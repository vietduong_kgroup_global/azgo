# Changelog #

All notable changes to this project will be documented in this file.

## [Unreleased] ##

## 2021-05-25 ##

### Add ###

- Create this repo and add README
- Add file ```before_start.sh```: run it after machine restarted
- Add file ```clear_project.sh```: run it when build project failed. maybe it is failed because of cache
- Add ```docker-compose.yml```: all service of azgo in here
- Add ```import mysql.sh```: run it if this is the first time you run this on your machine. These script will install mysql8 and export current db in mysql of azgo, then import the default db (when we had it)
- Add ```install_docker.sh```: run it if this is the first time you run this project. This file will install docker for you.
- Add ```azgo.sql```:default azgo db
- Add 3 git subtree 

```bash
git subtree add --prefix azgo-admin https://bitbucket.org/vietduong_kgroup_global/azgo-admin.git master --squash
```

```bash
git subtree add --prefix azgo-api https://bitbucket.org/vietduong_kgroup_global/azgo-api.git master --squash
```

```bash
git subtree add --prefix azgo-socketio https://bitbucket.org/vietduong_kgroup_global/azgo-socketio.git master --squash
```
