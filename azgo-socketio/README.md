# AZGO Socket IO #

## Installation ##

```bash
npm install
```

## Build ##

```bash
npm run build
```

## Start ##

```bash
npm run start
```

## Access ECR ##

aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 505145921145.dkr.ecr.ap-southeast-1.amazonaws.com

```create tag in repository
docker tag azgo/azgo-socket:0.0.1  505145921145.dkr.ecr.ap-southeast-1.amazonaws.com/azgo-socket:0.0.1

```push image  in repository
docker push 505145921145.dkr.ecr.ap-southeast-1.amazonaws.com/azgo-socket:0.0.1
```

## Dockerized ##

### Run ###

```bash
sh start_project.sh
```

### Port output ###

> Socket: 8300 -> 8300
> Redis: 6379 -> 8301, 6479 -> 8302
