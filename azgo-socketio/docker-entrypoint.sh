#!/bin/bash
set -e

### Start app
if [ -f pm2.json ];
then
    pm2 start pm2.json
else
    pm2 start npm -- start -i 4 --name bg-job --log-date-format 'YYYY-MM-DD HH:mm:ss.SSS'
fi

pm2 log

exec "$@"
