import {Controller} from '@nestjs/common';
import {RedisService} from '@azgo-module/core/dist/background-utils/Redis/redis.service';
import config from '../config';

@Controller()
export class DemoController<T = any> {
    private io;
    private redisService;

    constructor(
    ) {
        this.redisService = RedisService.getInstance(config.redis);
    }

    listen(io) {
        console.log('Start listen demo event');
        this.io = io;
        this.redisService.listenEventDemo(this.handleRedisDemoEvent.bind(this));
    }

    private handleRedisDemoEvent(channel, redis_data) {
        console.log('handleRedisDemoEvent chanel:', channel);
        redis_data = JSON.parse(redis_data);
        console.log('handleRedisDemoEvent data:', redis_data);
        this.io.in(redis_data.data.room).emit(channel, redis_data.data);
    }
}
