import {Module} from '@nestjs/common';
import {DemoController} from './demo.controller';

@Module({
    controllers: [DemoController],
    imports: [],
    providers: [],
})
export class DemoModule {

}
