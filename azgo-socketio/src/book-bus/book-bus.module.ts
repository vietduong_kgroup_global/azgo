import { Module } from '@nestjs/common';
import { BookBusController } from './book-bus.controller';

@Module({
  controllers: [BookBusController]
})
export class BookBusModule {}
