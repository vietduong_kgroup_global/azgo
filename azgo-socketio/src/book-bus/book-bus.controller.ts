import { Controller } from '@nestjs/common';
import {RedisService} from '@azgo-module/core/dist/background-utils/Redis/redis.service';
import config from '../config';

@Controller('book-bus')
export class BookBusController <T = any>  {
    private io;
    private redisService;

    constructor(
    ) {
        this.redisService = RedisService.getInstance(config.redis);
    }

    listen(io) {
        console.log('Start listen book bus event');
        this.io = io;
        this.redisService.listenEventBookBus(this.handleRedisBookBusEvent.bind(this));
    }

    private handleRedisBookBusEvent(channel, redis_data) {
        console.log('handleRedisBookBusEvent chanel:', channel);
        redis_data = JSON.parse(redis_data);
        console.log('handleRedisBookBusEvent data:', redis_data);
        this.io.in(redis_data.data.room).emit(channel, redis_data.data);
    }
}
