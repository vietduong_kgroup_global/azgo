import { Test, TestingModule } from '@nestjs/testing';
import { BookBusController } from './book-bus.controller';

describe('BookBus Controller', () => {
  let controller: BookBusController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BookBusController],
    }).compile();

    controller = module.get<BookBusController>(BookBusController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
