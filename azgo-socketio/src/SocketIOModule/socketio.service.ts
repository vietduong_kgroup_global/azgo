import config from '../config';
const SocketIO = require('socket.io');

export class SocketioService
{
    /*private static server;*/
    private static io;

    /**
     * Create server
     */
    public static initServer()
    {
        if (typeof(this.io) === 'undefined') {
            /*this.server = require('http').createServer();*/
            // this.io = new SocketIO(config.port, {
            //     path: '/socket',
            // });
            this.io = new SocketIO(config.port);
            /*this.server.listen(Config.port);*/

            console.log('Listening on port ' + config.port);

            return true;
        }
        else {
            console.log('Server has been initialized on port ' + config.port);
        }

        return false;
    }

    /**
     * Get current IO instance
     */
    public static getIO()
    {
        return this.io;
    }

    public static stopServer()
    {
        this.io.close();
        this.io = undefined;
        console.log('Server stopped');
    }
}
