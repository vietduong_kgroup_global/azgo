import {Controller} from '@nestjs/common';
import {SocketioService} from './socketio.service';
import {RedisService} from '@azgo-module/core/dist/background-utils/Redis/redis.service';
import config from '../config';

@Controller()
export class SocketioController {
    private redisService;
    constructor() {
        this.redisService = RedisService.getInstance(config.redis);

    }

    public start()
    {
        console.log('Server is starting...');

        if (SocketioService.initServer()) {
            console.log('Server started');
        }
        else {
            console.log('Can not start server');
        }

        return SocketioService.getIO();
    }

    public stop()
    {
        SocketioService.stopServer();
    }


}
