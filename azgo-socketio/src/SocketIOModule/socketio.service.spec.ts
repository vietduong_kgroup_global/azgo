import {SocketioService} from "./socketio.service";

const SocketIO = require('socket.io');
const Config = require('../config');
const isPortReachable = require('is-port-reachable');

console.log = function() {};

describe('SocketIOService', async () => {

    describe('getIO', async () => {
        it('Should undefined when not started', async () => {
            expect(SocketioService.getIO()).toBeUndefined();
        });

        it('Must be an instance of SocketIO after started', async () => {
            await SocketioService.initServer();
            expect(SocketioService.getIO().constructor).toBe((await new SocketIO()).constructor);
            await SocketioService.stopServer();
        });

        it('Must be undefined after stopped', async () => {
            await SocketioService.initServer();
            await SocketioService.stopServer();
            expect(SocketioService.getIO()).toBeUndefined();
        });
    });

    describe('initServer', async () => {
        it('Port ' + Config.port + ' must be free when server is not started', async () => {
            expect(await isPortReachable(Config.port)).toBe(false);
        });

        it('Could start server', async () => {
            const result = await SocketioService.initServer();
            expect(result).toBe(true);
            await SocketioService.stopServer();
        });

        it('Port ' + Config.port + ' must be used after server started', async () => {
            await SocketioService.initServer();
            expect(await isPortReachable(Config.port)).toBe(true);
            await SocketioService.stopServer();
        });
    });
});