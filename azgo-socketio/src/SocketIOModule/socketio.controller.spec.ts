import {SocketioController} from "./socketio.controller";
import {SocketioService} from "./socketio.service";

const SocketIO = require('socket.io');

console.log = function() {};

describe('SocketIOController', () => {
    let socketIOController: SocketioController;

    beforeEach(async () => {
        socketIOController = new SocketioController();
    });

    describe('start', () => {
        it('Should return an instance of socket.io', async() => {
            const result = await socketIOController.start();
            expect(result).toBe(SocketioService.getIO());
            await socketIOController.stop();
        });
    });
});