import {Module} from '@nestjs/common';
import {SocketioController} from './socketio.controller';

@Module({
    controllers: [SocketioController],
    imports: [],
    providers: [],
})
export class SocketioModule {

}
