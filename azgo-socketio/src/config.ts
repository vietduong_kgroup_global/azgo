require('dotenv').config();
export default {
    port: process.env.ENV_SOCKETIO_PORT || 8300,
    redis: {
        port: process.env.ENV_REDIS_PORT || 6379,
        host: process.env.ENV_REDIS_HOST || '0.0.0.0',
        password: process.env.ENV_REDIS_PASSWORD || null,
    },
};
