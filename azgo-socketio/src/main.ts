import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {SocketioModule} from './SocketIOModule/socketio.module';
import {SocketioController} from './SocketIOModule/socketio.controller';
import {DemoController} from './Demo/demo.controller';
import {DemoModule} from './Demo/demo.module';
import {BikeCarModule} from './BikeCar/bikecar.module';
import {BikeCarController} from './BikeCar/bikecar.controller';
import {BookBusModule} from './book-bus/book-bus.module';
import {BookBusController} from './book-bus/book-bus.controller';
import {VanBagacModule} from './van-bagac/van-bagac.module';
import {VanBagacController} from './van-bagac/van-bagac.controller';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    const ctrlSocketIO = app.select(SocketioModule).get(SocketioController);

    const io = ctrlSocketIO.start();

    io.sockets.on('connection', (socket) => {
        // check socket user_id
        // if (socket.handshake.query.user_id) {
        //     socket.join('user_' + socket.handshake.query.user_id );
        //     socket.join('online');
        // } else {
            socket.on('join', (room) => {
                socket.join(room);
                console.log(room)
                // socket.join('online');
            });
        // }
    });


    // RedisService.setConfig(Config.redis);
    const ctrlDemo = app.select(DemoModule).get(DemoController);
    await ctrlDemo.listen(io);
    // DemoService.listen(io);
    const ctrlBikeCar = app.select(BikeCarModule).get(BikeCarController);
    await ctrlBikeCar.listen(io);

    // DemoService.listen(io);
    const ctrlVanBagac = app.select(VanBagacModule).get(VanBagacController);
    await ctrlVanBagac.listen(io);

    const ctrlBooBus = app.select(BookBusModule).get(BookBusController);
    await ctrlBooBus.listen(io);
}

bootstrap();
