import {Module, HttpModule} from '@nestjs/common';
import {SocketioModule} from './SocketIOModule/socketio.module';
import {DemoModule} from './Demo/demo.module';
import {BikeCarModule} from './BikeCar/bikecar.module';
import { BookBusModule } from './book-bus/book-bus.module';
import { VanBagacModule } from './van-bagac/van-bagac.module';

@Module({
    controllers: [],
    imports: [
        SocketioModule,
        HttpModule,
        DemoModule,
        BikeCarModule,
        BookBusModule,
        VanBagacModule,
    ],
    providers: [],
})
export class AppModule {

}
