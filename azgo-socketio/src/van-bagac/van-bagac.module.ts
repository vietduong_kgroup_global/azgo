import { Module } from '@nestjs/common';
import { VanBagacController } from './van-bagac.controller';

@Module({
  controllers: [VanBagacController]
})
export class VanBagacModule {}
