import { Controller } from '@nestjs/common';
import {RedisService} from '@azgo-module/core/dist/background-utils/Redis/redis.service';
import config from '../config';

@Controller('van-bagac')
export class VanBagacController {
    private io;
    private redisService;

    constructor(
    ) {
        this.redisService = RedisService.getInstance(config.redis);
    }

    listen(io) {
        console.log('Start listen van bagac event');
        this.io = io;
        this.redisService.listenEventVanBagac(this.handleRedisBagacEvent.bind(this));
    }

    private handleRedisBagacEvent(channel, redis_data) {
        console.log('handleRedisBagacEvent chanel:', channel);
        redis_data = JSON.parse(redis_data);
        console.log('handleRedisBagacEvent data:', redis_data);
        this.io.in(redis_data.data.room).emit(channel, redis_data.data);
    }
}
