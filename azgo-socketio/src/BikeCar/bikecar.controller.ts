import {Controller} from '@nestjs/common';
import {RedisService} from '@azgo-module/core/dist/background-utils/Redis/redis.service';
import config from '../config';

@Controller()
export class BikeCarController<T = any> {
    private io;
    private redisService;

    constructor(
    ) {
        this.redisService = RedisService.getInstance(config.redis);
    }

    listen(io) {
        console.log('Start listen bike car event');
        this.io = io;
        this.redisService.listenEventBikeCar(this.handleRedisBikeCarEvent.bind(this));
    }

    private handleRedisBikeCarEvent(channel, redis_data) {
        console.log('handleRedisBikeCarEvent chanel:', channel);
        redis_data = JSON.parse(redis_data);
        console.log('handleRedisBikeCarEvent data:', redis_data);
        this.io.in(redis_data.data.room).emit(channel, redis_data.data);
    }
}
