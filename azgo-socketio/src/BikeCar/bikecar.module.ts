import {Module} from '@nestjs/common';
import {BikeCarController} from './bikecar.controller';

@Module({
    controllers: [BikeCarController],
})
export class BikeCarModule {

}
