#!/bin/bash
set -e

API_WORKDIR=$(pwd)

CORE_SRC=/home/tannguyen/Projects/rest-framework/nestjs-core/dist

# ================
CORE_MODULE=$(pwd)/node_modules/@azgo-module/core/dist
# ================

echo 'Building core ...'
cd $CORE_SRC

npm run build
echo 'Built core successfully'

# Remove old dist folder

cd $API_WORKDIR

rm -rf $CORE_MODULE/*

cp -R $CORE_SRC/* $CORE_MODULE/.

# Change config hostUserModuleAPI in Core
sed -i "s/localhost:8200/plantsmart.bclocal.top:82/g" /home/tannguyen/Projects/rest-framework/nestjs-socketIO/node_modules/@vitop-module/core/dist/config.js

echo 'DONE'
