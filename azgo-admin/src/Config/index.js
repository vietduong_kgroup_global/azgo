
console.log('App Core',process.env.REACT_APP_CORE)
export const Config = {
  DEV_URL: {
    IMAGE: process.env.REACT_APP_IMAGE || 'http://54.255.252.122:8200',
    CORE: process.env.REACT_APP_CORE || 'http://54.255.252.122:8100/api/v1',
    FILE_SERVICE: process.env.REACT_APP_FILE_SERVICE || 'https://dev-azgo-asia.s3-ap-southeast-1.amazonaws.com/',
    UPLOAD_IMAGE: 'http://localhost:8200/api/v1/upload-file/images',
    GOOGLE_MAP_API_KEY: process.env.REACT_APP_GOOGLE_MAP_API_KEY ||
      'https://maps.googleapis.com/maps/api/js?key=' +
      'AIzaSyAKrUyzNuIGi1PvvDwhRl_RsGW_V12bpZg' +
      '&language=vi&v=3.exp&libraries=geometry,drawing,places',
  }, 
  // DEV_URL: {
  //   IMAGE: 'http://118.69.226.194:9200',
  //   CORE: 'http://118.69.226.194:9200/api/v1',
  //   UPLOAD_IMAGE: 'http://118.69.226.194:9200/api/v1/upload-file/images',
  //   FILE_SERVICE: 'https://plantsmart.s3.ap-southeast-1.amazonaws.com/',
  //   GOOGLE_MAP_API_KEY:
  //     'https://maps.googleapis.com/maps/api/js?key=' +
  //     'AIzaSyAKrUyzNuIGi1PvvDwhRl_RsGW_V12bpZg' +
  //     '&language=vi&v=3.exp&libraries=geometry,drawing,places',
  // },
}
export const adminVersion = "0.0.4" 

