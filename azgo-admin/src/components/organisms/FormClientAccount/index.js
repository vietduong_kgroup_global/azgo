import React, { useEffect, useState } from 'react'
import {
  Form,
  Input,
  Button,
  Select,
  DatePicker,
  Upload,
  Icon,
  Modal,
  message,
} from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import moment from 'moment'
import * as config from 'Utils/enum'
import 'Stores/ClientManagement/Reducers'
import 'Stores/ClientManagement/Sagas'
import { ClientManagementActions } from 'Stores/ClientManagement/Actions'
import { getToken } from 'Utils/token'
import { Config } from 'Config'
import UploadImage from 'components/molecules/UploadImg'
import {_parsePhoneNumber} from 'Utils/helper'
const FormClientAccountContainer = ({
  form,
  id,
  item,
  createItem,
  editItem,
  changeBlockUser,
}) => {
  const { getFieldDecorator, getFieldsValue, setFieldsValue } = form
  const { Option } = Select
  const [loading, setLoading] = useState(false)
  const [imageUrl, setImgUrl] = useState('')
  const [previewVisible, setPreviewVisible] = useState(false)
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }

      if (id) {
        editItem({
          ...getFieldsValue(),
          birthday: moment(form.getFieldsValue().birthday).format('DD/MM/YYYY'),
          type: config.MANAGEMENT_TYPE_USER.CUSTOMER,
          id,
          status: item.get('status'),
          changeBlockUser: changeBlockUser,
        })
      } else {
        createItem({
          ...getFieldsValue(),
          roles: [config.MANAGEMENT_TYPE_USER.CUSTOMER],
          birthday: moment(form.getFieldsValue().birthday).format('DD/MM/YYYY'),
          type: config.MANAGEMENT_TYPE_USER.CUSTOMER,
        })
      }
    })
  }
  const propsHeaderUpload = {
    name: 'images[]',
    action: `${Config.DEV_URL.UPLOAD_IMAGE}?type_image=client`,
    headers: {
      Authorization: 'bearer ' + getToken(),
    },
  }
  const dateFormat = ['DD/MM/YYYY']
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  }
  const compareToFirstPassword = (rule, value, callback) => {
    if (value && value !== form.getFieldValue('password')) {
      callback('Mật khẩu không trùng khớp!')
    } else {
      callback()
    }
  }
  const validatePhone = (rule, value, callback) => { 
    let phone = form.getFieldValue('phone')
    let countryCode = form.getFieldValue('countryCode') 
    if (phone && countryCode) {
      let result = _parsePhoneNumber(countryCode, phone)
      if(result && result.isValid)  callback()
      else callback('Số điện thoại không hợp lệ')
    } else {
      callback('') 
    }
  }
   
  const normFile = e => {
    if (Array.isArray(e)) {
      return e
    }
    return e && e.fileList
  }
  const getBase64 = (img, callback) => {
    const reader = new FileReader()
    reader.addEventListener('load', () => callback(reader.result))
    reader.readAsDataURL(img)
  }
  const handlePreviewImg = () => {
    if (imageUrl.length > 0) {
      setPreviewVisible(true)
    }
  }
  const handleChange = value => {
    if(!value) {
      setImgUrl(null)
      setFieldsValue({
        imageProfile: null,
      })
    }
    else {
      setImgUrl(value.file.response.data[0].address)
      let imageProfile = {
        address: value.file.response.data[0].address,
        filePath: value.file.response.data[0].file_path,
        fileName: value.file.response.data[0].file_name,
      }
      setFieldsValue({
        imageProfile: imageProfile,
      })
    }   
  }
  const prefixSelector = getFieldDecorator('countryCode', {
    initialValue: '84',
  })(
    <Select style={{ width: 70 }}>
      <Option value="84">+84</Option>
    </Select>
  )
  useEffect(() => {
    // let filePath = item.getIn(['customerProfile', 'imageProfile', 'filePath'])
    // let fileName = item.getIn(['customerProfile', 'imageProfile', 'fileName'])
    // let imagePath = ''
    // if (filePath && fileName) {
    //   imagePath = filePath + '/' + fileName
    // } 
    let imageUrl = ''
    if (item.getIn(['customerProfile', 'imageProfile', 'address'])) {
      imageUrl = item.getIn(['customerProfile', 'imageProfile', 'address'])
      setImgUrl(imageUrl)
    } else {
      setImgUrl('')
    }
  }, [item])
  return (
    <>
      <Form
        id="frmClientAccount"
        {...formItemLayout}
        className="input-floating form-ant-custom">
        <Form.Item label="E-mail">
          {getFieldDecorator('email', {
            rules: [
              {
                pattern: new RegExp("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"),
                message: 'Email không đúng định dạng!',
              },
              {
                max: 50,
                message: 'Trường không hơn lớn 50 ký tự',
              },
            ],
            initialValue: id && item.get('email'),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Số điện thoại">
          {getFieldDecorator('phone', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                // pattern: new RegExp("^([0-9]{9})$"),
                validator: validatePhone,
                // message: 'Phone không đúng định dạng, bắt buộc 9 số và bỏ số 0 đầu!',
              }, 
            ],
            initialValue: id && item.get('phone'),
          })(<Input type="number" disabled={!id ? false : true} addonBefore={prefixSelector} style={{ width: '100%' }} />)}
        </Form.Item>
        {!id ? (
          <Form.Item label="Mật khẩu" hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Trường bắt buộc!',
                },
                {
                  // pattern: new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,200}$"),
                  message: 'Trường tối thiểu 6 ký tự, 1 ký tự hoa, 1 ký tự thường và 1 số, tối đa 200 ký tự',
                },
              ],
            })(<Input.Password />)}
          </Form.Item>
        ) : (
          ""
          // <Form.Item label="Mật khẩu" hasFeedback>
          //   {getFieldDecorator('password', {
          //     rules: [
          //       {
          //         // pattern: new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,200}$"),
          //         message: 'Trường tối thiểu 6 ký tự, 1 ký tự hoa, 1 ký tự thường và 1 số, tối đa 200 ký tự',
          //       },
          //     ],
          //   })(<Input.Password />)}
          // </Form.Item>
        )}

        {!id ? (
          <Form.Item label="Xác nhận mật khẩu" hasFeedback>
            {getFieldDecorator('confirm', {
              rules: [
                {
                  validator: compareToFirstPassword,
                },
                {
                  required: true,
                  message: 'Trường bắt buộc!',
                },
              ],
            })(<Input.Password onPaste={e => e.preventDefault()} />)}
          </Form.Item>
        ) : (
          ""

          // <Form.Item label="Xác nhận mật khẩu" hasFeedback>
          //   {getFieldDecorator('confirm', {
          //     rules: [
          //       {
          //         validator: compareToFirstPassword,
          //       },
          //     ],
          //   })(<Input.Password onPaste={e => e.preventDefault()} />)}
          // </Form.Item>
        )}
        <Form.Item label="Họ">
          {getFieldDecorator('lastName', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                min: 1,
                message: 'Trường không được ít hơn 1 ký tự!',
              },
              {
                max: 10,
                message: 'Trường không được quá 10 ký tự!',
              },
            ],
            initialValue: id && item.getIn(['customerProfile', 'lastName']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Tên">
          {getFieldDecorator('firstName', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                min: 1,
                message: 'Trường không được ít hơn 1 ký tự!',
              },
              {
                max: 30,
                message: 'Trường không được quá 30 ký tự!',
              },
            ],
            initialValue: id && item.getIn(['customerProfile', 'firstName']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Ngày sinh">
          {getFieldDecorator('birthday', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue:
              id && item.getIn(['customerProfile', 'birthday'])
                ? moment(item.getIn(['customerProfile', 'birthday']))
                : null,
          })(<DatePicker format={dateFormat} placeholder="Chọn" />)}
        </Form.Item>
        <Form.Item label="Giới tính">
          {getFieldDecorator('gender', {
            rules: [{ required: true, message: 'Trường bắt buộc!' }],
            initialValue: id && item.getIn(['customerProfile', 'gender']),
          })(
            <Select placeholder="Chọn">
              {config.MANAGEMENT_GENDER_ARR.map(item => (
                <Select.Option value={item.key} key={`gender_${item.key}`}>
                  {item.label}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Số chứng minh nhân dân">
          {getFieldDecorator('cmnd', {
            rules: [
              {
                required: false,
                message: 'Trường bắt buộc!',
              },
              {
                pattern: new RegExp("^([A-Za-z0-9]{9,15})"),
                message: 'Trường chứa từ 9 - 15 ký tự không bỏ dấu',
              },
            ],
            initialValue: id && item.getIn(['customerProfile', 'cmnd']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Địa chỉ">
          {getFieldDecorator('address', {
            rules: [
              {
                required: false,
                message: 'Trường bắt buộc!',
              },
              {
                min: 5,
                message: 'Trường không được nhỏ hơn 5 ký tự!',
              },
              {
                max: 255,
                message: 'Trường không được lớn hơn 255 ký tự!',
              },
            ],
            initialValue: id && item.getIn(['customerProfile', 'address']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Ảnh đại diện" className="ant-form-item-load">
            <UploadImage
              imageProp={imageUrl}
              handleParent={handleChange}
              // setImgUrl={setImageAvatar}
              type_image_props='client'
              fileNameProp={item.getIn(['customerProfile', 'imageProfile', 'fileName'])}
            />
          
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('imageProfile')(<Input />)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Xác nhận
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(ClientManagementActions.createItemRequest(values)),
  editItem: values => dispatch(ClientManagementActions.editItemRequest(values)),
})

FormClientAccountContainer.propTypes = {
  form: PropTypes.any,
  id: PropTypes.string,
  item: PropTypes.object,
  createItem: PropTypes.func,
  editItem: PropTypes.func,
  changeBlockUser: PropTypes.any,
}
const FormClientAccount = Form.create()(FormClientAccountContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormClientAccount)
