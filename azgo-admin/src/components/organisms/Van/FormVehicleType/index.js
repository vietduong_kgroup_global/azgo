import React, { useEffect } from 'react'
import { Form, Input, Button, Select } from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import 'Stores/Van/VehicleTypeManagement/Reducers'
import 'Stores/Van/VehicleTypeManagement/Sagas'
import { VehicleTypeManagementActions } from 'Stores/Van/VehicleTypeManagement/Actions'

const FormVehicleTypeContainer = ({ form, id, item, createItem, editItem }) => {
  const { getFieldDecorator, getFieldsValue } = form
  const { Option } = Select
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      if (id) {
        editItem({
          ...getFieldsValue(),
          id: parseInt(id, 10),
          totalSeat: 1,
          pricePerKm: parseInt(form.getFieldsValue().pricePerKm, 10),
          basePrice: parseInt(form.getFieldsValue().basePrice, 10),
        })
      } else {
        createItem({
          ...getFieldsValue(),
          totalSeat: 1,
          pricePerKm: parseInt(form.getFieldsValue().pricePerKm, 10),
          basePrice: parseInt(form.getFieldsValue().basePrice, 10),
        })
      }
    })
  }
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  }
  return (
    <>
      <Form
        id="frmVehicleType"
        {...formItemLayout}
        className="input-floating form-ant-custom">
        <Form.Item label="Loại xe">
          {getFieldDecorator('vehicleGroupId', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('vehicleGroupId'),
          })(
            <Select placeholder="Chọn loại xe">
              <Option value={4}>Xe tải</Option>
              <Option value={5}>Bagac</Option>
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Tên loại xe">
          {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('name'),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Giá gốc(<1Km)">
          {getFieldDecorator('basePrice', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('basePrice'),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Giá/Km">
          {getFieldDecorator('pricePerKm', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('pricePerKm'),
          })(<Input />)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Xác nhận
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(VehicleTypeManagementActions.createItemRequest(values)),
  editItem: values =>
    dispatch(VehicleTypeManagementActions.editItemRequest(values)),
})

FormVehicleTypeContainer.propTypes = {
  form: PropTypes.any,
  id: PropTypes.string,
  item: PropTypes.object,
  createItem: PropTypes.func,
  editItem: PropTypes.func,
}
const FormVehicleType = Form.create()(FormVehicleTypeContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormVehicleType)
