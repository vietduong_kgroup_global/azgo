import React, { useEffect, useState } from 'react'
import { Form, Input, Button, Select, DatePicker, Row, Col } from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import moment from 'moment'
import * as config from 'Utils/enum'
import { Config } from 'Config'
import 'Stores/Van/DriverManagement/Reducers'
import 'Stores/Van/DriverManagement/Sagas'
import { DriverManagementActions } from 'Stores/Van/DriverManagement/Actions'
import UploadImage from 'components/molecules/UploadImage'
import {_parsePhoneNumber} from 'Utils/helper'

const FormDriverAccountContainer = ({
  form,
  item,
  id,
  editItem,
  createItem,
  changeBlockUser,
}) => {
  const { getFieldDecorator, getFieldsValue, setFieldsValue } = form
  const { Option } = Select
  const [imageAvatar, setImageAvatar] = useState('')
  const [imageFrontCmnd, setImageFrontCmnd] = useState('')
  const [imageBackCmnd, setImageBackCmnd] = useState('')
  const [frontDrivingLicense, setFrontDrivingLicense] = useState('')
  const [backDrivingLicense, setBackDrivingLicense] = useState('')
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      if (id) {
        editItem({
          ...getFieldsValue(),
          birthday: moment(form.getFieldsValue().birthday).format('DD/MM/YYYY'),
          type: config.MANAGEMENT_TYPE_USER.DRIVER_VAN_BAGAC,
          id,
          status: item.get('status'),
          changeBlockUser: changeBlockUser,
        })
      } else {
        createItem({
          ...getFieldsValue(),
          birthday: moment(form.getFieldsValue().birthday).format('DD/MM/YYYY'),
          type: config.MANAGEMENT_TYPE_USER.DRIVER_VAN_BAGAC,
        })
      }
    })
  }
  const dateFormat = ['DD/MM/YYYY']
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 7 },
    },
  }
  const formItemLayoutUpload = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 20 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 20,
        offset: 4,
      },
    },
  }
  const compareToFirstPassword = (rule, value, callback) => {
    if (value && value !== form.getFieldValue('password')) {
      callback('Mật khẩu không trùng khớp!')
    } else {
      callback()
    }
  }
  const prefixSelector = getFieldDecorator('countryCode', {
    initialValue: '84',
  })(
    <Select style={{ width: 70 }}>
      <Option value="84">+84</Option>
    </Select>
  )
  const callbackHandleAvatar = value => {
    setFieldsValue({
      imageProfile: value,
    })
  }
  const callbackHandleFrontCmnd = value => {
    setFieldsValue({
      frontOfCmnd: value,
    })
  }
  const callbackHandleBackCmnd = value => {
    setFieldsValue({
      backOfCmnd: value,
    })
  }
  const callbackHandlefrontDrivingLicense = value => {
    setFieldsValue({
      frontDrivingLicense: value,
    })
  }
  const callbackHandleBackDrivingLicense = value => {
    setFieldsValue({
      backDrivingLicense: value,
    })
  }
  const validatePhone = (rule, value, callback) => { 
    let phone = form.getFieldValue('phone')
    let countryCode = form.getFieldValue('countryCode')
    if (phone && countryCode) {
      let result = _parsePhoneNumber(countryCode, phone)
      if(result && result.isValid)  callback()
      else callback('Số điện thoại không hợp lệ')
    } else {
      callback('Số điện thoại không hợp lệ')
    }
  }
  useEffect(() => {
    if (item.getIn(['driverProfile', 'imageProfile'])) {
      setImageAvatar(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['driverProfile', 'imageProfile', 'filePath']) +
          '/' +
          item.getIn(['driverProfile', 'imageProfile', 'fileName'])
      )
    }
    if (item.getIn(['driverProfile', 'frontOfCmnd'])) {
      setImageFrontCmnd(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['driverProfile', 'frontOfCmnd', 'filePath']) +
          '/' +
          item.getIn(['driverProfile', 'frontOfCmnd', 'fileName'])
      )
    }
    if (item.getIn(['driverProfile', 'backOfCmnd'])) {
      setImageBackCmnd(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['driverProfile', 'backOfCmnd', 'filePath']) +
          '/' +
          item.getIn(['driverProfile', 'backOfCmnd', 'fileName'])
      )
    }
    if (item.getIn(['driverProfile', 'frontDrivingLicense'])) {
      setFrontDrivingLicense(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['driverProfile', 'frontDrivingLicense', 'filePath']) +
          '/' +
          item.getIn(['driverProfile', 'frontDrivingLicense', 'fileName'])
      )
    }
    if (item.getIn(['driverProfile', 'backDrivingLicense'])) {
      setBackDrivingLicense(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['driverProfile', 'backDrivingLicense', 'filePath']) +
          '/' +
          item.getIn(['driverProfile', 'backDrivingLicense', 'fileName'])
      )
    }
    return () => {
      setImageAvatar('')
      setImageFrontCmnd('')
      setImageBackCmnd('')
      setFrontDrivingLicense('')
      setBackDrivingLicense('')
    }
  }, [item])
  return (
    <>
      <Form
        id="frmDriverAccount"
        {...formItemLayout}
        className="input-floating form-ant-custom-upload">
        <Form.Item label="Số điện thoại">
          {getFieldDecorator('phone', {
            rules: [
              {
                // pattern: new RegExp("^([0-9]{9})$"),
                validator: validatePhone,
                // message: 'Phone không đúng định dạng, bắt buộc 9 số và bỏ số 0 đầu!',
              },
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('phone'),
          })(<Input disabled={!id ? false : true} addonBefore={prefixSelector} style={{ width: '100%' }} />)}
        </Form.Item>
        {!id ? (
          <Form.Item label="Mật khẩu" hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Trường bắt buộc!',
                },
                {
                  pattern: new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,200}$"),
                  message: 'Trường tối thiểu 6 ký tự, 1 ký tự hoa, 1 ký tự thường và 1 số, tối đa 200 ký tự',
                },
              ],
            })(<Input.Password />)}
          </Form.Item>
        ) : (
          <Form.Item label="Mật khẩu" hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  pattern: new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,200}$"),
                  message: 'Trường tối thiểu 6 ký tự, 1 ký tự hoa, 1 ký tự thường và 1 số, tối đa 200 ký tự',
                },
              ],
            })(<Input.Password />)}
          </Form.Item>
        )}

        {!id ? (
          <Form.Item label="Xác nhận mật khẩu" hasFeedback>
            {getFieldDecorator('confirm', {
              rules: [
                {
                  validator: compareToFirstPassword,
                },
                {
                  required: true,
                  message: 'Trường bắt buộc!',
                },
              ],
            })(<Input.Password onPaste={e => e.preventDefault()} />)}
          </Form.Item>
        ) : (
          <Form.Item label="Xác nhận mật khẩu" hasFeedback>
            {getFieldDecorator('confirm', {
              rules: [
                {
                  validator: compareToFirstPassword,
                },
              ],
            })(<Input.Password onPaste={e => e.preventDefault()} />)}
          </Form.Item>
        )}
        <Form.Item label="Họ tài xế">
          {getFieldDecorator('lastName', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                min: 1,
                message: 'Trường không được ít hơn 1 ký tự!',
              },
              {
                max: 10,
                message: 'Trường không được quá 10 ký tự!',
              },
            ],
            initialValue: id && item.getIn(['driverProfile', 'lastName']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Tên tài xế">
          {getFieldDecorator('firstName', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                min: 1,
                message: 'Trường không được ít hơn 1 ký tự!',
              },
              {
                max: 30,
                message: 'Trường không được quá 30 ký tự!',
              },
            ],
            initialValue: id && item.getIn(['driverProfile', 'firstName']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Ngày sinh">
          {getFieldDecorator('birthday', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue:
              id && item.getIn(['driverProfile', 'birthday'])
                ? moment(item.getIn(['driverProfile', 'birthday']))
                : null,
          })(<DatePicker format={dateFormat} placeholder="Chọn" />)}
        </Form.Item>
        <Form.Item label="Giới tính">
          {getFieldDecorator('gender', {
            rules: [{ required: true, message: 'Trường bắt buộc!' }],
            initialValue: id && item.getIn(['driverProfile', 'gender']),
          })(
            <Select placeholder="Chọn">
              {config.MANAGEMENT_GENDER_ARR.map(item => (
                <Select.Option value={item.key} key={`gender_${item.key}`}>
                  {item.label}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Số chứng minh nhân dân">
          {getFieldDecorator('cmnd', {
            rules: [
              {
                required: false,
                message: 'Trường bắt buộc!',
              },
              {
                pattern: new RegExp("^([A-Za-z0-9]{9,15})"),
                message: 'Trường chứa từ 9 - 15 ký tự không bỏ dấu',
              },
            ],
            initialValue: id && item.getIn(['driverProfile', 'cmnd']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="E-mail">
          {getFieldDecorator('email', {
            rules: [
              {
                pattern: new RegExp("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"),
                message: 'Email không đúng định dạng!',
              },
              {
                max: 50,
                message: 'Trường không hơn lớn 50 ký tự',
              },
            ],
            initialValue: id && item.get('email'),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Địa chỉ">
          {getFieldDecorator('address', {
            rules: [
              {
                min: 5,
                message: 'Trường không được nhỏ hơn 5 ký tự!',
              },
              {
                max: 255,
                message: 'Trường không được lớn hơn 255 ký tự!',
              },
            ],
            initialValue: id && item.getIn(['driverProfile', 'address']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Ảnh đại diện" className="ant-form-item-load">
          <UploadImage
            imageProp={imageAvatar}
            handleParent={callbackHandleAvatar}
          />
        </Form.Item>
        <Form.Item
          label="Giấy CMND"
          className="ant-form-item-load"
          {...formItemLayoutUpload}>
          <Row>
            <Col span={8}>
              <UploadImage
                imageProp={imageFrontCmnd}
                handleParent={callbackHandleFrontCmnd}
              />
            </Col>
            <Col span={8}>
              <UploadImage
                imageProp={imageBackCmnd}
                handleParent={callbackHandleBackCmnd}
              />
            </Col>
          </Row>
        </Form.Item>
        <Form.Item
          label="Giấy phép lái xe"
          className="ant-form-item-load"
          {...formItemLayoutUpload}>
          <Row>
            <Col span={8}>
              <UploadImage
                imageProp={frontDrivingLicense}
                handleParent={callbackHandlefrontDrivingLicense}
              />
            </Col>
            <Col span={8}>
              <UploadImage
                imageProp={backDrivingLicense}
                handleParent={callbackHandleBackDrivingLicense}
              />
            </Col>
          </Row>
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('imageProfile', {
            initialValue:
              id && item.getIn(['driverProfile', 'imageProfile'])
                ? item.getIn(['driverProfile', 'imageProfile']).toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('frontOfCmnd', {
            initialValue:
              id && item.getIn(['driverProfile', 'frontOfCmnd'])
                ? item.getIn(['driverProfile', 'frontOfCmnd']).toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('backOfCmnd', {
            initialValue:
              id && item.getIn(['driverProfile', 'backOfCmnd'])
                ? item.getIn(['driverProfile', 'backOfCmnd']).toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('frontDrivingLicense', {
            initialValue:
              id && item.getIn(['driverProfile', 'frontDrivingLicense'])
                ? item.getIn(['driverProfile', 'frontDrivingLicense']).toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('backDrivingLicense', {
            initialValue:
              id && item.getIn(['driverProfile', 'backDrivingLicense'])
                ? item.getIn(['driverProfile', 'backDrivingLicense']).toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Xác nhận
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  editItem: values => dispatch(DriverManagementActions.editItemRequest(values)),
  createItem: values =>
    dispatch(DriverManagementActions.createItemRequest(values)),
})

FormDriverAccountContainer.propTypes = {
  id: PropTypes.any,
  form: PropTypes.any,
  item: PropTypes.object,
  editItem: PropTypes.func,
  createItem: PropTypes.func,
  changeBlockUser: PropTypes.any,
}
const FormDriverAccount = Form.create()(FormDriverAccountContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormDriverAccount)
