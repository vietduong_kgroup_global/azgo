import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { Popconfirm } from 'antd'
import { Card, CardHeader, CardBody, Row, Col } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import { compose } from 'redux'
import { VehicleTypeManagementActions } from 'Stores/Van/VehicleTypeManagement/Actions'
import { connect } from 'react-redux'
import { formatPrice } from 'Utils/helper'

const CardVehicleType = ({ dataSource, deleteItem }) => {
  const Style = {
    cursor: 'pointer',
    marginLeft: '15px',
  }
  const handleFormatPrice = data => {
    if (typeof data === 'string') return data
    return formatPrice(data)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  return (
    <>
      <Row>
        {dataSource.map((item, idx) => (
          <Col xs="12" sm="6" md="4" key={`vehicle_type_${item.get('id')}`}>
            <Card className="border-info">
              <CardHeader>
                <strong>{idx + 1}</strong>
                <div className="card-header-actions">
                  <Link
                    to={'/van/vehicle-type/' + item.get('id')}
                    style={Style}>
                    <i className="fa fa-pencil" />
                  </Link>
                  <Popconfirm
                    placement="bottom"
                    onConfirm={() => handleConfirmDelete(item.get('id'))}
                    title="Bạn có muốn xóa?"
                    okText="Có"
                    cancelText="Không">
                    <span style={Style}>
                      <i className="fa fa-trash" />
                    </span>
                  </Popconfirm>
                </div>
              </CardHeader>
              <CardBody>
                <h5>Tên loại xe: {item.get('name')}</h5>
                <div>
                  <strong>{'Giá gốc(<1Km)'}</strong>:{' '}
                  {handleFormatPrice(item.get('basePrice'))} VNĐ
                </div>
                <div>
                  <strong>Giá mỗi Km</strong>:{' '}
                  {handleFormatPrice(item.get('pricePerKm'))} VNĐ
                </div>
              </CardBody>
            </Card>
          </Col>
        ))}
      </Row>
    </>
  )
}

CardVehicleType.propTypes = {
  dataSource: PropTypes.any,
  deleteItem: PropTypes.func,
}
const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({
  deleteItem: id =>
    dispatch(VehicleTypeManagementActions.deleteItemRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(CardVehicleType)
