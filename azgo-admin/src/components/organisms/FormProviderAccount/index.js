import React, { useEffect, useState } from 'react'
import { Form, Input, Button, Select, Upload, Icon, Modal, message } from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import 'Stores/ProviderManagement/Reducers'
import 'Stores/ProviderManagement/Sagas'
import { ProviderManagementActions } from 'Stores/ProviderManagement/Actions'
import * as config from 'Utils/enum'
import { getToken } from 'Utils/token'
import { Config } from 'Config'
import {_parsePhoneNumber} from 'Utils/helper'

const FormProviderAccountContainer = ({
  form,
  item,
  id,
  editItem,
  createItem,
  changeBlockUser,
}) => {
  const { getFieldDecorator, getFieldsValue, setFieldsValue } = form
  const { Option } = Select
  const [loading, setLoading] = useState(false)
  const [imageUrl, setImgUrl] = useState('')
  const [previewVisible, setPreviewVisible] = useState(false)
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      if (id) {
        editItem({
          ...getFieldsValue(),
          type: config.MANAGEMENT_TYPE_USER.PROVIDER,
          id,
          status: item.get('status'),
          changeBlockUser: changeBlockUser,
        })
      } else {
        createItem({
          ...getFieldsValue(),
          type: config.MANAGEMENT_TYPE_USER.PROVIDER,
        })
      }
    })
  }
  const propsHeaderUpload = {
    name: 'images[]',
    action: Config.DEV_URL.UPLOAD_IMAGE,
    headers: {
      Authorization: 'bearer ' + getToken(),
    },
  }
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  }
  const compareToFirstPassword = (rule, value, callback) => {
    if (value && value !== form.getFieldValue('password')) {
      callback('Mật khẩu không trùng khớp!')
    } else {
      callback()
    }
  }
  const normFile = e => {
    if (Array.isArray(e)) {
      return e
    }
    return e && e.fileList
  }
  const getBase64 = (img, callback) => {
    const reader = new FileReader()
    reader.addEventListener('load', () => callback(reader.result))
    reader.readAsDataURL(img)
  }
  const handlePreviewImg = () => {
    if (imageUrl.length > 0) {
      setPreviewVisible(true)
    }
  }
  const handleChange = info => {
    if (info.file.status === 'uploading') {
      setLoading(true)
      return
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl => {
        setImgUrl(imageUrl)
        setLoading(false)
      })
      // set lai state imageProfile
      let imageProfile = {
        address: info.file.response.data[0].address,
        filePath: info.file.response.data[0].file_path,
        fileName: info.file.response.data[0].file_name,
      }
      setFieldsValue({
        imageProfile: imageProfile,
      })
    } else if (info.file.status === 'error') {
      setLoading(false)
      message.error('Upload failed')
      try {
        setFieldsValue({
          imageProfile: item.getIn(['providersInfo', 'imageProfile']).toJS(),
        })
      } catch (e) {
        console.log(e)
      }
    }
  }
  const prefixSelector = getFieldDecorator('countryCode', {
    initialValue: '84',
  })(
    <Select style={{ width: 70 }}>
      <Option value="84">+84</Option>
    </Select>
  )
  const validatePhone = (rule, value, callback) => { 
    let phone = form.getFieldValue('phone')
    let countryCode = form.getFieldValue('countryCode')
    if (phone && countryCode) {
      let result = _parsePhoneNumber(countryCode, phone)
      if(result && result.isValid)  callback()
      else callback('Số điện thoại không hợp lệ')
    } else {
      callback()
    }
  }
  useEffect(() => {
    let filePath = item.getIn(['providersInfo', 'imageProfile', 'filePath'])
    let fileName = item.getIn(['providersInfo', 'imageProfile', 'fileName'])
    let imagePath = ''
    if (filePath && fileName) {
      imagePath = filePath + '/' + fileName
    }
    let imageUrl = ''
    if (imagePath !== '') {
      imageUrl = Config.DEV_URL.IMAGE + '/' + imagePath
      setImgUrl(imageUrl)
    } else {
      setImgUrl('')
    }
  }, [item])
  return (
    <>
      <Form
        id="frmProviderAccount"
        {...formItemLayout}
        className="input-floating form-ant-custom">
        <Form.Item label="Số điện thoại">
          {getFieldDecorator('phone', {
            initialValue: id && item.get('phone'),
            rules: [
              {
                // pattern: new RegExp("^([0-9]{9})$"),
                validator: validatePhone,
                // message: 'Phone không đúng định dạng, bắt buộc 9 số và bỏ số 0 đầu!',
              },
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(<Input type="number" disabled={!id ? false : true} addonBefore={prefixSelector} style={{ width: '100%' }} />)}
        </Form.Item>
        {!id ? (
          <Form.Item label="Mật khẩu" hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Trường bắt buộc!',
                },
                {
                  pattern: new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,200}$"),
                  message: 'Trường tối thiểu 6 ký tự, 1 ký tự hoa, 1 ký tự thường và 1 số, tối đa 200 ký tự',
                },
              ],
            })(<Input.Password />)}
          </Form.Item>
        ) : (
          <Form.Item label="Mật khẩu" hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  pattern: new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,200}$"),
                  message: 'Trường tối thiểu 6 ký tự, 1 ký tự hoa, 1 ký tự thường và 1 số, tối đa 200 ký tự',
                },
              ],
            })(<Input.Password />)}
          </Form.Item>
        )}

        {!id ? (
          <Form.Item label="Xác nhận mật khẩu" hasFeedback>
            {getFieldDecorator('confirm', {
              rules: [
                {
                  validator: compareToFirstPassword,
                },
                {
                  required: true,
                  message: 'Trường bắt buộc!',
                },
              ],
            })(<Input.Password onPaste={e => e.preventDefault()} />)}
          </Form.Item>
        ) : (
          <Form.Item label="Xác nhận mật khẩu" hasFeedback>
            {getFieldDecorator('confirm', {
              rules: [
                {
                  validator: compareToFirstPassword,
                },
              ],
            })(<Input.Password onPaste={e => e.preventDefault()} />)}
          </Form.Item>
        )}
        <Form.Item label="Tên nhà xe">
          {getFieldDecorator('providerName', {
            initialValue: id && item.getIn(['providersInfo', 'providerName']),
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                min: 3,
                message: 'Trường không được ít hơn 3 ký tự!',
              },
              {
                max: 50,
                message: 'Trường không được quá 50 ký tự!',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="E-mail">
          {getFieldDecorator('email', {
            initialValue: id && item.get('email'),
            rules: [
              {
                pattern: new RegExp("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"),
                message: 'Email không đúng định dạng!',
              },
              {
                max: 50,
                message: 'Trường không hơn lớn 50 ký tự',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Địa chỉ">
          {getFieldDecorator('address', {
            initialValue: id && item.getIn(['providersInfo', 'address']),
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                min: 3,
                message: 'Trường không được ít hơn 3 ký tự!',
              },
              {
                max: 255,
                message: 'Trường không được quá 255 ký tự!',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Người đại diện">
          {getFieldDecorator('representative', {
            initialValue: id && item.getIn(['providersInfo', 'representative']),
            rules: [
              {
                min: 3,
                message: 'Trường không được ít hơn 3 ký tự!',
              },
              {
                max: 50,
                message: 'Trường không được quá 50 ký tự!',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Số điện thoại người đại diện">
          {getFieldDecorator('phoneRepresentative', {
            initialValue:
              id && item.getIn(['providersInfo', 'phoneRepresentative']),
            rules: [
              {
                // pattern: new RegExp("^([0-9]{9})$"),
                validator: validatePhone,
                // message: 'Phone không đúng định dạng, bắt buộc 9 số và bỏ số 0 đầu!',
              },
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(<Input type="number" addonBefore={prefixSelector} style={{ width: '100%' }} />)}
        </Form.Item>
        <Form.Item label="Ảnh đại diện" className="ant-form-item-load">
          {getFieldDecorator('upload', {
            valuePropName: 'fileList',
            getValueFromEvent: normFile,
          })(
            <>
              <div className="row-upload-img">
                <div className="col-upload-img">
                  <div className="frame-img">
                    {imageUrl && imageUrl !== '' ? (
                      <img
                        src={imageUrl}
                        onClick={handlePreviewImg}
                        alt="avatar"
                      />
                    ) : (
                      <Icon type="picture" />
                    )}
                  </div>
                </div>
                <div className="col-upload-img-button">
                  <Upload
                    name="avatar"
                    className="avatar-uploader"
                    {...propsHeaderUpload}
                    showUploadList={false}
                    onChange={handleChange}>
                    {imageUrl && imageUrl !== '' ? (
                      <Button>
                        <Icon type="edit" /> Thay đổi
                        {loading && (
                          <Icon className="loading-image" type="loading" />
                        )}
                      </Button>
                    ) : (
                      <Button>
                        <Icon type="upload" /> Tải hình lên
                        {loading && (
                          <Icon className="loading-image" type="loading" />
                        )}
                      </Button>
                    )}
                  </Upload>
                </div>
              </div>
              <Modal
                visible={previewVisible}
                footer={null}
                onCancel={() => setPreviewVisible(false)}>
                <div className="modal-image">
                  <img alt="example" src={imageUrl} />
                </div>
              </Modal>
            </>
          )}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('imageProfile')(<Input />)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Xác nhận
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(ProviderManagementActions.createItemRequest(values)),
  editItem: values =>
    dispatch(ProviderManagementActions.editItemRequest(values)),
})

FormProviderAccountContainer.propTypes = {
  item: PropTypes.object,
  form: PropTypes.any,
  id: PropTypes.string,
  createItem: PropTypes.func,
  editItem: PropTypes.func,
  changeBlockUser: PropTypes.any,
}
const FormProviderAccount = Form.create()(FormProviderAccountContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormProviderAccount)
