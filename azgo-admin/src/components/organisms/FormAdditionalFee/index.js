import React, { useEffect, useState } from 'react'
import {
  Form,
  Input,
  Button,
  Select
} from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'

import 'Stores/Intercity/AreaManagement/Reducers'
import 'Stores/Intercity/AreaManagement/Sagas'
import { AreaManagementActions } from 'Stores/Intercity/AreaManagement/Actions'
import { AreaManagementSelectors } from 'Stores/Intercity/AreaManagement/Selectors'
import 'Stores/AdditionalFeeManagement/Reducers'
import 'Stores/AdditionalFeeManagement/Sagas'
import { AdditionalFeeManagementSelectors } from 'Stores/AdditionalFeeManagement/Selectors'
import { AdditionalFeeManagementActions } from 'Stores/AdditionalFeeManagement/Actions'
import * as config from 'Utils/enum'

const { TextArea } = Input;
const FormFee = ({
  form,
  id,
  item,
  createItem,
  editItem,
  deleteItem,
  itemsArea,
  getListItemsArea,
  setFilterArea,
  filterArea
}) => {
  const {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue,
    resetFields,
    validateFields
  } = form;
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 7 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 20,
        offset: 4,
      },
    },
  }
  const [typeAdditionalFee, setTypeAdditionalFee] = useState('flat')
  
  const onChangeTypeAdditionalFee = (value) => {
    setTypeAdditionalFee(value)
  }
  const handleSubmit = () => {
    validateFields(['name'], { force: true }, (err, values) => {
      if (err) {
        return false
      }
      let percent, flat;
      let fields = getFieldsValue();
      if(typeAdditionalFee === 'percent'){
        percent = fields.percent? Number(fields.percent/100) : 0;
        flat = null;
      }
      else{
        flat = fields.flat? fields.flat : 0;
        percent = null;
      }
      if (id) {
        editItem({
          ...getFieldsValue(),
          type: typeAdditionalFee,
          percent: percent,
          flat: flat,
          uuid: id,
        })
      } else {
        createItem({
          ...getFieldsValue(),
          type: typeAdditionalFee,
          percent: percent,
          flat: flat,
        })
      }
    })
  }
  const handleDelete = () => {
    deleteItem(id)
  }
  useEffect(() => {
    if(id && item.get('flat') ){
      console.log(" item.get('flat'): ",  item.get('flat'))
      setTypeAdditionalFee(config.MANAGEMENT_TYPE_ADDITIONAL_FEE.FLAT)
    }
    else if(id && item.get('percent')) {
      console.log(" item.get('percent'): ",  item.get('percent'))
      setTypeAdditionalFee(config.MANAGEMENT_TYPE_ADDITIONAL_FEE.PERCENT)
    }
  }, [item])
  useEffect(() => {
    getListItemsArea(filterArea)
  }, [filterArea])
  useEffect(() => {
    setFilterArea({
      per_page: 100,
      page: 1,
      keyword: '',
    })
  }, [])
  return (
    <>
      <Form
        id="frmVehicle"
        {...formItemLayout}
        className="input-floating form-ant-custom-upload">
        <Form.Item label="Tên phí">
          {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('name'),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Mã phí phụ thu">
          {getFieldDecorator('code', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('code'),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Loại phí">
          <Select
            onChange={onChangeTypeAdditionalFee}
            placeholder="Chọn loại phí"
            defaultValue={typeAdditionalFee}
            value={typeAdditionalFee}
          >
            {config.MANAGEMENT_TYPE_ADDITIONAL_FEE_ARR.map(item => (
              <Select.Option value={item.key} key={`type_additional_fee_${item.key}`}>
                {item.label}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        {typeAdditionalFee === config.MANAGEMENT_TYPE_ADDITIONAL_FEE.PERCENT && 
          <Form.Item label="Phần trăm (%)">
            {getFieldDecorator('percent', {
              rules: [
                {
                  required: true,
                  message: 'Trường bắt buộc!',
                },
              ],
              initialValue: id && item.get('percent')*100,
            })(<Input type="number" min={1} max={100}/>)}
          </Form.Item>
        }
        {typeAdditionalFee === config.MANAGEMENT_TYPE_ADDITIONAL_FEE.FLAT &&
          <Form.Item label="Số tiền">
            {getFieldDecorator('flat', {
              rules: [
                {
                  required: true,
                  message: 'Trường bắt buộc!',
                },
              ],
              initialValue: id && item.get('flat'),
            })(<Input type="number" min={1} />)}
          </Form.Item>
        }
        <Form.Item label="Khu vực">
          {getFieldDecorator('areaUuid', {
            rules: [
            ],
            initialValue: id && item.get('areaUuid'),
          })(
            <Select placeholder="Vui lòng chọn khu vực">
              <Select.Option
                  value={null}
                  key={`area_uuid_no_set`}>
                  Chọn khu vực
              </Select.Option>
              {itemsArea.toJS().map(item => (
                <Select.Option
                  value={item.uuid}
                  key={`area_uuid_${item.uuid}`}>
                  {item.name}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit} className="confirm_update_button">
            {id ? 'Xác nhận' : 'Tạo mới'}
          </Button>
          {id && <Button type="danger"  onClick={handleDelete}>
            Xoá
          </Button>
          }
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({
  itemsArea: AreaManagementSelectors.getItems(state),
  filterArea: AreaManagementSelectors.getFilter(state),
  item: AdditionalFeeManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(AdditionalFeeManagementActions.createItemRequest(values)),
  editItem: values =>
    dispatch(AdditionalFeeManagementActions.editItemRequest(values)),
  deleteItem: values =>
    dispatch(AdditionalFeeManagementActions.deleteItemRequest(values)),
  setFilterArea: filter => dispatch(AreaManagementActions.setFilter(filter)),
  getListItemsArea: values =>
    dispatch(AreaManagementActions.getItemsRequest(values)),
})

FormFee.propTypes = {
  form: PropTypes.any,
  id: PropTypes.string,
  item: PropTypes.object,
  createItem: PropTypes.func,
  editItem: PropTypes.func,
  deleteItem: PropTypes.func,
  itemsVehicleType: PropTypes.object,
  itemsBrandVehicle: PropTypes.object,
  itemsDriver: PropTypes.object,
}
const FormAdditionalFee = Form.create()(FormFee)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormAdditionalFee)
