import React, { useEffect, useState } from 'react'
import { Form, Input, Button, Select, InputNumber } from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import 'Stores/Vehicle/VehicleTypeManagement/Reducers'
import 'Stores/Vehicle/VehicleTypeManagement/Sagas'
import * as configsEnum from 'Utils/enum'
import './styles.scss'
import { VehicleTypeManagementActions } from 'Stores/Vehicle/VehicleTypeManagement/Actions'
// import InputNumber from 'components/organisms/InputNumber'
const FormVehicleTypeContainer = ({
  form,
  id,
  item,
  createItem,
  editItem,
  clearItems,
}) => {
  const { getFieldDecorator, getFieldsValue, setFieldsValue } = form
  const { Option } = Select
  const [displayField, setDisPlayField] = useState(false)
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      if (id) {
        editItem({
          ...getFieldsValue(),
          id: parseInt(id, 10),
          totalSeat: parseInt(form.getFieldsValue().totalSeat, 10),
          pricePerKm: parseInt(form.getFieldsValue().pricePerKm, 10),
          basePrice: parseInt(form.getFieldsValue().basePrice, 10),
        })
      } else {
        createItem({
          ...getFieldsValue(),
          totalSeat: parseInt(form.getFieldsValue().totalSeat, 10),
          pricePerKm: parseInt(form.getFieldsValue().pricePerKm, 10),
          basePrice: parseInt(form.getFieldsValue().basePrice, 10),
        })
      }
    })
  }
  const onChangeValueForm = (name, value) => {
    setFieldsValue({
      [name]: value,
    })
  }
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  }
  const handleChangeVehicleGroup = value => {
    if (value === configsEnum.VEHICLE_TYPE.BIKE) {
      setDisPlayField(false)
      setFieldsValue({
        totalSeat: 2,
      })
    } else {
      setDisPlayField(true)
      setFieldsValue({
        totalSeat: null,
      })
    }
  }
  useEffect(() => {
    if (id && item.get('vehicleGroupId') === configsEnum.VEHICLE_TYPE.BIKE) {
      setFieldsValue({
        totalSeat: 2,
      })
      setDisPlayField(false)
    } else {
      setDisPlayField(true)
    }
  }, [item])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  return (
    <>
      <Form
        id="frmVehicleType"
        {...formItemLayout}
        className="input-floating form-ant-custom">
        <Form.Item label="Loại xe">
          {getFieldDecorator('vehicleGroupId', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('vehicleGroupId'),
          })(
            <Select
              placeholder="Chọn loại xe"
              onChange={value => handleChangeVehicleGroup(value)}>
              {/* <Option value={configsEnum.VEHICLE_TYPE.BUS}>Xe bus</Option> */}
              <Option value={configsEnum.VEHICLE_TYPE.BIKE}>Xe máy</Option>
              <Option value={configsEnum.VEHICLE_TYPE.CAR}>Ô tô</Option>
              {/* <Option value={configsEnum.VEHICLE_TYPE.VAN}>Xe tải</Option>
              <Option value={configsEnum.VEHICLE_TYPE.BAGAC}>Bagac</Option> */}
              <Option value={configsEnum.VEHICLE_TYPE.TOUR}>Tour</Option>
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Tên loại xe">
          {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('name'),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Giá gốc(<1Km)">
          {getFieldDecorator('basePrice', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('basePrice'),
          })( 
          <InputNumber 
            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            parser={value => value.replace(/\$\s?|(,*)/g, '')}  
            className="input-number-transition"
            />
          )}
        </Form.Item>
        <Form.Item label="Giá/Km">
          {getFieldDecorator('pricePerKm', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('pricePerKm'),
          })(<InputNumber 
              formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              parser={value => value.replace(/\$\s?|(,*)/g, '')} 
          />)}
        </Form.Item>
        <Form.Item label="Số lượng ghế">
          {getFieldDecorator('totalSeat', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('totalSeat'),
          })(<InputNumber type="number" min={1} />)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Xác nhận
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(VehicleTypeManagementActions.createItemRequest(values)),
  editItem: values =>
    dispatch(VehicleTypeManagementActions.editItemRequest(values)),
  clearItems: () => dispatch(VehicleTypeManagementActions.clearItems()),
})

FormVehicleTypeContainer.propTypes = {
  form: PropTypes.any,
  id: PropTypes.string,
  item: PropTypes.object,
  createItem: PropTypes.func,
  editItem: PropTypes.func,
  clearItems: PropTypes.func,
}
const FormVehicleType = Form.create()(FormVehicleTypeContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormVehicleType)
