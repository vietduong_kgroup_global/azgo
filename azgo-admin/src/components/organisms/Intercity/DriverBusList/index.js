import React, { useEffect } from 'react'
import 'Stores/DriverManagement/Reducers'
import 'Stores/DriverManagement/Sagas'
import { DriverManagementSelectors } from 'Stores/DriverManagement/Selectors'
import { DriverManagementActions } from 'Stores/DriverManagement/Actions'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import DriverManagementContainer from 'containers/Intercity/DriverManagement'
import { Card, CardBody, CardHeader } from 'reactstrap'

const DriverBusList = ({ filter, setFilter, idProvider }) => {
  useEffect(() => {
    if (idProvider) {
      setFilter({
        provider_id: idProvider,
      })
    }
  }, [idProvider])
  return (
    <Card>
      <CardHeader>
        <strong>Danh sách tài xế</strong>
      </CardHeader>
      <CardBody style={{ padding: '0' }}>
        <DriverManagementContainer
          filter={filter}
          setFilter={setFilter}
          idProvider={idProvider}
        />
      </CardBody>
    </Card>
  )
}

const mapStateToProps = state => ({
  filter: DriverManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(DriverManagementActions.setFilter(filter)),
})

DriverBusList.propTypes = {
  idProvider: PropTypes.number,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
}

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(DriverBusList)
