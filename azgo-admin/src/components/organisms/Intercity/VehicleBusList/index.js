import React, { useEffect, useState } from 'react'
import 'Stores/Intercity/VehicleManagement/Reducers'
import 'Stores/Intercity/VehicleManagement/Sagas'
import { VehicleManagementActions } from 'Stores/Intercity/VehicleManagement/Actions'
import { VehicleManagementSelectors } from 'Stores/Intercity/VehicleManagement/Selectors'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Button, Card, CardBody, CardHeader } from 'reactstrap'
import VehicleManagementContainer from 'containers/Intercity/VehicleManagement'

const VehicleBusList = ({ filter, setFilter, idProvider }) => {
  const [visible, setVisible] = useState(false)
  const showModal = () => {
    setVisible(true)
  }
  useEffect(() => {
    if (idProvider) {
      setFilter({
        provider_id: idProvider,
      })
    }
    return () => {
      setFilter({
        provider_id: null,
      })
    }
  }, [idProvider])
  return (
    <Card>
      <CardHeader>
        <strong>Danh sách xe</strong>
      </CardHeader>
      <CardBody style={{ padding: '0' }}>
        <VehicleManagementContainer
          filter={filter}
          setFilter={setFilter}
          visible={visible}
          setVisible={setVisible}
          idProvider={idProvider}
        />
      </CardBody>
    </Card>
  )
}

const mapStateToProps = state => ({
  filterVehicle: VehicleManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(VehicleManagementActions.setFilter(filter)),
})

VehicleBusList.propTypes = {
  idProvider: PropTypes.number,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
}

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(VehicleBusList)
