import React, {useEffect, useContext, useState} from 'react'
import { Form, Input, Button, Select } from 'antd'
import { UserContext } from 'Contexts/UserData'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import 'Stores/ProviderManagement/Reducers'
import 'Stores/ProviderManagement/Sagas'
import 'Stores/Intercity/VehicleTypeManagement/Reducers'
import 'Stores/Intercity/VehicleTypeManagement/Sagas'
import { ProviderManagementSelectors } from 'Stores/ProviderManagement/Selectors'
import { ProviderManagementActions } from 'Stores/ProviderManagement/Actions'
import { VehicleTypeManagementActions } from 'Stores/Intercity/VehicleTypeManagement/Actions'
import { VehicleTypeManagementSelectors } from 'Stores/Intercity/VehicleTypeManagement/Selectors'
import { UserSelectors } from 'Stores/User/Selectors'
import * as config from 'Utils/enum'

const FormVehicleTypeContainer = ({
  form,
  id,
  item,
  getItemsListProvider,
  getItemsProvider,
  editItem,
  createItem,
  getItem,
  providerDetail,
  itemUser,
  visible,
  setVisible,
}) => {
  const UserInfo = useContext(UserContext)
  const { getFieldDecorator, getFieldsValue, resetFields } = form
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      if (id) {
        editItem({
          ...getFieldsValue(),
          totalSeat: parseInt(form.getFieldsValue().totalSeat, 10),
          pricePerKm: 1,
          basePrice: 1,
          providerId:
            itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
              ? itemUser.getIn(['providersInfo', 'id'])
              : form.getFieldsValue().providerId,
          id,
        })
      } else {
        createItem({
          ...getFieldsValue(),
          totalSeat: parseInt(form.getFieldsValue().totalSeat, 10),
          pricePerKm: 1,
          basePrice: 1,
          providerId:
            itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
              ? itemUser.getIn(['providersInfo', 'id'])
              : form.getFieldsValue().providerId,
        })
      }
    })
  }
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  }
  useEffect(() => {
    if (id) {
      getItem(id)
    } else {
      resetFields()
    }
  }, [id])
  useEffect(() => {
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.ADMIN) {
      getItemsProvider()
    }
  }, [])
  return (
    <>
      <Form
        id="frmVehicleType"
        {...formItemLayout}
        className="input-floating form-ant-custom">
        <Form.Item label="Loại xe">
          {getFieldDecorator('name', {
            initialValue: id && item.get('name'),
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                min: 1,
                message: 'Trường không được ít hơn 1 ký tự!',
              },
              {
                max: 30,
                message: 'Trường không được quá 30 ký tự!',
              },
            ],
          })(<Input />)}
        </Form.Item>
        {providerDetail &&
          itemUser.get('type') !== config.MANAGEMENT_TYPE_USER.PROVIDER && (
            <Form.Item label="Nhà xe">
              {getFieldDecorator('providerId', {
                initialValue: providerDetail,
                rules: [{ required: true, message: 'Trường bắt buộc!' }],
              })(
                <Select placeholder="Vui lòng chọn nhà xe" disabled>
                  {getItemsListProvider.map(item => (
                    <Select.Option
                      value={item.get('id')}
                      key={`provider_${item.get('id')}`}>
                      {item.get('providerName')}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
          )}
        {!providerDetail &&
          itemUser.get('type') !== config.MANAGEMENT_TYPE_USER.PROVIDER && (
            <Form.Item label="Nhà xe">
              {getFieldDecorator('providerId', {
                initialValue: id && item.get('providerId'),
                rules: [{ required: true, message: 'Trường bắt buộc!' }],
              })(
                <Select placeholder="Vui lòng chọn nhà xe">
                  {getItemsListProvider.map(item => (
                    <Select.Option
                      value={item.get('id')}
                      key={`provider_${item.get('id')}`}>
                      {item.get('providerName')}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
          )}
        <Form.Item label="Số lượng ghế">
          {getFieldDecorator('totalSeat', {
            initialValue: id && item.get('totalSeat'),
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(<Input type="number" min={1} max={100} />)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Xác nhận
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({
  getItemsListProvider: ProviderManagementSelectors.getItemsList(state),
  item: VehicleTypeManagementSelectors.getItem(state),
  itemUser: UserSelectors.getUserData(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id =>
    dispatch(VehicleTypeManagementActions.getItemDetailRequest(id)),
  getItemsProvider: values =>
    dispatch(ProviderManagementActions.getItemsListRequest(values)),
  editItem: values =>
    dispatch(VehicleTypeManagementActions.editItemRequest(values)),
  createItem: values =>
    dispatch(VehicleTypeManagementActions.createItemRequest(values)),
})

FormVehicleTypeContainer.propTypes = {
  id: PropTypes.any,
  item: PropTypes.object,
  getItem: PropTypes.func,
  form: PropTypes.any,
  getItemsProvider: PropTypes.func,
  getItemsListProvider: PropTypes.object,
  editItem: PropTypes.func,
  createItem: PropTypes.func,
  providerDetail: PropTypes.number,
  itemUser: PropTypes.object,
  visible: PropTypes.any,
  setVisible: PropTypes.func,
}
const FormCarCategory = Form.create()(FormVehicleTypeContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormCarCategory)
