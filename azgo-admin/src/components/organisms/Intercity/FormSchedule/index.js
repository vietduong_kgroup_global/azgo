import React, { useEffect, useState, useContext } from 'react'
import { UserContext } from 'Contexts/UserData'
import { Form, Button, Select, Icon, DatePicker, TimePicker, Input } from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import moment from 'moment'
import 'Stores/ProviderManagement/Reducers'
import 'Stores/ProviderManagement/Sagas'
import 'Stores/DriverManagement/Reducers'
import 'Stores/DriverManagement/Sagas'
import 'Stores/Intercity/VehicleTypeManagement/Reducers'
import 'Stores/Intercity/VehicleTypeManagement/Sagas'
import 'Stores/Intercity/VehicleManagement/Reducers'
import 'Stores/Intercity/VehicleManagement/Sagas'
import 'Stores/Intercity/RouteManagement/Reducers'
import 'Stores/Intercity/RouteManagement/Sagas'
import { ProviderManagementSelectors } from 'Stores/ProviderManagement/Selectors'
import { ProviderManagementActions } from 'Stores/ProviderManagement/Actions'
import { DriverManagementSelectors } from 'Stores/DriverManagement/Selectors'
import { DriverManagementActions } from 'Stores/DriverManagement/Actions'
import { VehicleTypeManagementActions } from 'Stores/Intercity/VehicleTypeManagement/Actions'
import { VehicleTypeManagementSelectors } from 'Stores/Intercity/VehicleTypeManagement/Selectors'
import { VehicleManagementActions } from 'Stores/Intercity/VehicleManagement/Actions'
import { VehicleManagementSelectors } from 'Stores/Intercity/VehicleManagement/Selectors'
import { RouteManagementActions } from 'Stores/Intercity/RouteManagement/Actions'
import { RouteManagementSelectors } from 'Stores/Intercity/RouteManagement/Selectors'
import { ScheduleManagementActions } from 'Stores/Intercity/ScheduleManagement/Actions'
import { UserSelectors } from 'Stores/User/Selectors'
import * as config from 'Utils/enum'

let idSchedule = 0
const FormScheduleContainer = ({
  form,
  id,
  item,
  editItem,
  createItem,
  getItemsProvider,
  getItemsListProvider,
  getItemsVehicleType,
  itemsVehicleType,
  getItemLicensePlates,
  itemLicensePlates,
  fetchItemsRouteOfProvider,
  getItemsRouteOfProvider,
  fetchItemsDriverOfProvider,
  getItemsDriverOfProvider,
  itemUser,
}) => {
  const {
    getFieldDecorator,
    getFieldValue,
    getFieldsValue,
    resetFields,
    setFieldsValue,
  } = form
  const UserInfo = useContext(UserContext)
  const [providerId, setProviderId] = useState(null)
  const [disabledFieldForm, setDisabledFieldForm] = useState(false)
  const [fieldDate, setFieldDate] = useState('')
  const [fieldTime, setFieldTime] = useState('')
  const [duration, setDuration] = useState(null)
  const [endHour, setEndHour] = useState(null)
  const [current, setCurrent] = useState(false)
  const [disabledTime, setDisabledTime] = useState(false)
  getFieldDecorator('keys', { initialValue: [] })
  const keys = getFieldValue('keys')
  const dateFormat = 'DD-MM-YYYY'
  const formatTime = 'HH:mm'
  const { Option } = Select
  const remove = k => {
    const keys = form.getFieldValue('keys')
    if (keys.length === 0) {
      return
    }
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    })
  }
  const add = () => {
    const keys = form.getFieldValue('keys')
    const nextKeys = keys.concat(idSchedule++)
    form.setFieldsValue({
      keys: nextKeys,
    })
  }
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      if (id) {
        editItem({
          ...getFieldsValue(),
          startDate: fieldDate + ' ' + fieldTime + ':00',
          startHour: fieldTime,
          id: parseInt(id, 10),
          providerId:
            itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
              ? itemUser.getIn(['providersInfo', 'id'])
              : parseInt(form.getFieldsValue().providerId, 10),
          editSchedule: 1,
        })
      } else {
        createItem({
          ...getFieldsValue(),
          startDate: fieldDate + ' ' + fieldTime + ':00',
          startHour: fieldTime,
          providerId:
            itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
              ? itemUser.getIn(['providersInfo', 'id'])
              : parseInt(form.getFieldsValue().providerId, 10),
        })
      }
    })
  }
  const handleChangeSelectProvider = value => {
    setDisabledFieldForm(true)
    setProviderId(value)
    resetFields('vehicleType')
    resetFields('vehicleId')
    resetFields('routeId')
    resetFields('userId')
    setFieldsValue({
      vehicleType: null,
      vehicleId: null,
      routeId: null,
      userId: null,
    })
  }
  const handleChangeVehicleType = value => {
    setDisabledFieldForm(false)
    getItemLicensePlates(providerId, value)
    resetFields('vehicleId')
    setFieldsValue({
      vehicleId: null,
    })
  }
  const onChangeDate = (date, dateString) => {
    if (date) {
      let newDate = moment(date._d).format('YYYY-MM-DD')
      setFieldDate(newDate)
      if (date._d.toDateString() === new Date().toDateString()) {
        setCurrent(true)
      } else {
        setCurrent(false)
      }
    }
    setDisabledTime(true)
  }
  const onChangeTime = (time, timeString) => {
    setFieldTime(timeString)
  }
  const disabledDate = current => {
    // Can not select days before today and today
    return current && current < moment().startOf('day')
  }

  const range = (start, end) => {
    const result = []
    for (let i = start; i < end; i++) {
      result.push(i)
    }
    return result
  }

  const disabledHours = () => {
    if (current) {
      let h = new Date().getHours()
      const hours = range(0, 60)
      hours.splice(h, 24 - h)
      return hours
    }
  }
  const disabledMinutes = h => {
    let newh = new Date().getHours()
    if (current) {
      if (h === newh) {
        let m = new Date().getMinutes()
        return range(0, m)
      }
    }
  }
  const handleChangeRoute = value => {
    if (getItemsRouteOfProvider.size > 0) {
      const resultFind = getItemsRouteOfProvider
        .toJS()
        .find(el => el.id === value)
      if (resultFind) {
        setDuration(resultFind.duration)
      }
    }
  }
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  }
  const formItems = keys.map((k, index) => (
    <Form.Item {...tailFormItemLayout} key={k}>
      {getFieldDecorator(`names[${k}]`, {
        validateTrigger: ['onChange', 'onBlur'],
        rules: [
          {
            required: true,
            whitespace: true,
            message: 'Vui lòng điền thông tin.',
          },
        ],
      })(
        <>
          <DatePicker
            placeholder="Chọn ngày chạy"
            format={dateFormat}
            style={{ width: 'calc(60% - 20px)' }}
          />
          <TimePicker
            placeholder="Chọn giờ"
            format={formatTime}
            style={{
              width: 'calc(40% - 20px)',
              marginLeft: '20px',
              marginRight: '6px',
            }}
          />
        </>
      )}
      {keys.length > 0 ? (
        <Icon
          className="dynamic-delete-button"
          type="minus-circle-o"
          onClick={() => remove(k)}
        />
      ) : null}
    </Form.Item>
  ))
  useEffect(() => {
    if (fieldDate && fieldTime && duration) {
      let dateMili = Date.parse(`${fieldDate} ${fieldTime}`)
      let time = duration
      let timeParts = time.split(':')
      let distanceMili = +timeParts[0] * (60000 * 60) + +timeParts[1] * 60000
      let endDateMili = dateMili + distanceMili
      var day = moment(new Date(endDateMili)).format('HH:mm DD-MM-YYYY')
      setEndHour(day)
      form.setFieldsValue({
        endHour: day,
      })
    }
    return () => {
      setEndHour(null)
    }
  }, [fieldDate, fieldTime, duration, endHour])
  useEffect(() => {
    if (providerId) {
      getItemsVehicleType(providerId)
      fetchItemsRouteOfProvider(providerId)
      fetchItemsDriverOfProvider(providerId)
    }
  }, [providerId])
  useEffect(() => {
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.ADMIN) {
      getItemsProvider()
    }
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER) {
      setProviderId(itemUser.getIn(['providersInfo', 'id']))
    }
    return () => {
      resetFields()
    }
  }, [])
  useEffect(() => {
    if (item && item.size > 0) {
      if (item.getIn(['provider', 'id'])) {
        setProviderId(item.getIn(['provider', 'id']))
        getItemsVehicleType(item.getIn(['provider', 'id']))
        if (item.getIn(['vehicle', 'vehicleTypeId'])) {
          getItemLicensePlates(
            item.getIn(['provider', 'id']),
            item.getIn(['vehicle', 'vehicleTypeId'])
          )
        }
      }
      fetchItemsRouteOfProvider(item.getIn(['provider', 'id']))
      fetchItemsDriverOfProvider(item.getIn(['provider', 'id']))
      setFieldDate(moment(item.get('startDate')).format('YYYY-MM-DD'))
      setFieldTime(item.get('startHour'))
      setDuration(item.getIn(['route', 'duration']))
      setEndHour(item.get('endHour'))
    }
    return () => {
      setEndHour(null)
      setDuration(null)
      setFieldDate('')
      setFieldTime('')
      resetFields()
    }
  }, [item])
  return (
    <>
      <Form
        id="frmSchedule"
        {...formItemLayout}
        className="input-floating form-ant-custom">
        {itemUser.get('type') !== config.MANAGEMENT_TYPE_USER.PROVIDER && (
          <Form.Item label="Nhà xe">
            {getFieldDecorator('providerId', {
              rules: [{ required: true, message: 'Trường bắt buộc!' }],
              initialValue: id && item.getIn(['provider', 'id']),
            })(
              <Select
                placeholder="Vui lòng chọn nhà xe"
                onChange={handleChangeSelectProvider}>
                {getItemsListProvider.map(item => (
                  <Select.Option
                    value={item.get('id')}
                    key={`provider_${item.get('id')}`}>
                    {item.get('providerName')}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        )}
        <Form.Item label="Loại xe">
          {getFieldDecorator('vehicleType', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.getIn(['vehicle', 'vehicleTypeId']),
          })(
            <Select
              placeholder="Vui lòng chọn loại xe"
              onChange={handleChangeVehicleType}>
              {itemsVehicleType.map(item => (
                <Select.Option
                  value={item.get('id')}
                  key={`vehicle_type_${item.get('id')}`}>
                  {item.get('name')}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Biển số xe">
          {getFieldDecorator('vehicleId', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.getIn(['vehicle', 'id']),
          })(
            <Select placeholder="Vui lòng chọn xe" disabled={disabledFieldForm}>
              {itemLicensePlates &&
                itemLicensePlates.size > 0 &&
                itemLicensePlates.map((item, idx) => (
                  <Select.Option
                    value={item.get('id')}
                    key={`license_plates_${idx}`}>
                    {item.get('licensePlates')}
                  </Select.Option>
                ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Tuyến xe">
          {getFieldDecorator('routeId', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.getIn(['route', 'id']),
          })(
            <Select
              placeholder="Vui lòng chọn tuyến xe"
              onChange={item => handleChangeRoute(item)}>
              {getItemsRouteOfProvider &&
                getItemsRouteOfProvider.map(item => (
                  <Select.Option
                    value={item.get('id')}
                    key={`route_provider_${item.get('id')}`}>
                    {item.get('name')}
                  </Select.Option>
                ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Tài xế">
          {getFieldDecorator('userId', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('userId'),
          })(
            <Select placeholder="Vui lòng chọn tài xế">
              {getItemsDriverOfProvider &&
                getItemsDriverOfProvider.map(item => (
                  <Select.Option
                    value={item.get('userId')}
                    key={`user_${item.get('userId')}`}>
                    {item.get('fullName')}
                  </Select.Option>
                ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Phụ xe">
          {getFieldDecorator('subDriver', {
            initialValue: id && item.get('subDriver'),
            rules: [{ required: false, message: 'Trường bắt buộc!' }],
          })(<Input type="text" />)}
        </Form.Item>
        {item && item.size > 0 && (
          <Form.Item label="Giờ chạy xe">
            {getFieldDecorator('timeStart', {
              rules: [{ required: false, message: 'Trường bắt buộc!' }],
            })(
              <>
                <DatePicker
                  placeholder="Chọn ngày chạy"
                  defaultValue={
                    item.get('startDate')
                      ? moment(
                          `${moment(item.get('startDate')).format(
                            'DD-MM-YYYY'
                          )}`,
                          dateFormat
                        )
                      : null
                  }
                  format={dateFormat}
                  style={{ width: 'calc(60% - 20px)' }}
                  onChange={onChangeDate}
                  disabledDate={disabledDate}
                  allowClear={false}
                />
                <TimePicker
                  placeholder="Chọn giờ"
                  defaultValue={
                    item.get('startHour')
                      ? moment(`${item.get('startHour')}`, formatTime)
                      : null
                  }
                  format={formatTime}
                  style={{ width: '40%', marginLeft: '20px' }}
                  onChange={onChangeTime}
                  disabledHours={disabledHours}
                  disabledMinutes={disabledMinutes}
                  disabled={!disabledTime}
                  allowClear={false}
                />
              </>
            )}
          </Form.Item>
        )}
        {!id && (
          <>
            <Form.Item label="Ngày chạy xe">
              {getFieldDecorator('date-picker', {
                rules: [{ required: true, message: 'Trường bắt buộc!' }],
              })(
                <DatePicker
                  placeholder="Chọn ngày chạy"
                  format={dateFormat}
                  onChange={onChangeDate}
                  disabledDate={disabledDate}
                />
              )}
            </Form.Item>
            <Form.Item label="Giờ chạy xe">
              {getFieldDecorator('time-picker', {
                rules: [{ required: true, message: 'Trường bắt buộc!' }],
              })(
                <TimePicker
                  placeholder="Chọn giờ"
                  format={formatTime}
                  disabledHours={disabledHours}
                  disabledMinutes={disabledMinutes}
                  onChange={onChangeTime}
                  disabled={!disabledTime}
                />
              )}
            </Form.Item>
          </>
        )}
        <Form.Item label="Thời gian chạy">
          <Input value={duration} disabled />
        </Form.Item>
        <Form.Item label="Giờ kết thúc">
          {getFieldDecorator('endHour', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: endHour,
          })(<Input disabled />)}
        </Form.Item>
        {/* {formItems} */}
        {/* <Form.Item {...formItemLayoutWithOutLabel}> */}
        {/*  <Button type="dashed" onClick={add} style={{ width: '200px' }}> */}
        {/*    <Icon type="plus" /> Thêm thời gian chạy xe */}
        {/*  </Button> */}
        {/* </Form.Item> */}
        {item.get('status') === 1 || !id ? (
          <Form.Item {...tailFormItemLayout}>
            <Button
              type="primary"
              htmlType="submit"
              onClick={() => handleSubmit()}>
              Xác nhận
            </Button>
          </Form.Item>
        ) : null}
      </Form>
    </>
  )
}

const mapStateToProps = state => ({
  getItemsListProvider: ProviderManagementSelectors.getItemsList(state),
  getItemsDriverOfProvider: DriverManagementSelectors.getItemsOfProvider(state),
  itemsVehicleType: VehicleTypeManagementSelectors.getItemsVehicleType(state),
  itemLicensePlates: VehicleManagementSelectors.getItemLicensePlates(state),
  getItemsRouteOfProvider: RouteManagementSelectors.getItemsProvider(state),
  itemUser: UserSelectors.getUserData(state),
})

const mapDispatchToProps = dispatch => ({
  getItemsProvider: values =>
    dispatch(ProviderManagementActions.getItemsListRequest(values)),
  fetchItemsRouteOfProvider: values =>
    dispatch(RouteManagementActions.getItemsRequestProvider(values)),
  fetchItemsDriverOfProvider: values =>
    dispatch(DriverManagementActions.getItemsRequestOfProvider(values)),
  getItemsVehicleType: values =>
    dispatch(VehicleTypeManagementActions.getItemsVehicleTypeRequest(values)),
  getItemLicensePlates: (providerId, vehicleTypeId) =>
    dispatch(
      VehicleManagementActions.getItemLicensePlatesRequest(
        providerId,
        vehicleTypeId
      )
    ),
  editItem: values =>
    dispatch(ScheduleManagementActions.editItemRequest(values)),
  createItem: values =>
    dispatch(ScheduleManagementActions.createItemRequest(values)),
})

FormScheduleContainer.propTypes = {
  id: PropTypes.any,
  item: PropTypes.object,
  form: PropTypes.any,
  editItem: PropTypes.func,
  createItem: PropTypes.func,
  getItemsListProvider: PropTypes.object,
  getItemsProvider: PropTypes.func,
  getItemsVehicleType: PropTypes.func,
  itemsVehicleType: PropTypes.object,
  getItemLicensePlates: PropTypes.func,
  itemLicensePlates: PropTypes.object,
  fetchItemsRouteOfProvider: PropTypes.func,
  getItemsRouteOfProvider: PropTypes.object,
  fetchItemsDriverOfProvider: PropTypes.func,
  getItemsDriverOfProvider: PropTypes.object,
  itemUser: PropTypes.object,
}
const FormSchedule = Form.create()(FormScheduleContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormSchedule)
