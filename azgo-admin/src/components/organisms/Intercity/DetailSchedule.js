import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Button, ListGroup, ListGroupItem } from 'reactstrap'
import moment from 'moment'
import * as configsEnum from 'Utils/enum'
import { formatPrice } from 'Utils/helper'

const DetailSchedule = ({ itemList }) => {
  const [status, setStatus] = useState({})
  const handleFormatPrice = data => {
    if (typeof data === 'string') return data
    return formatPrice(data)
  }
  useEffect(() => {
    const result = configsEnum.MANAGEMENT_STATUS_SCHEDULE.find(item => {
      return item.key === itemList.get('status')
    })
    setStatus(result)
  }, [itemList])
  return (
    <>
      <div>
        <ListGroup style={{ paddingRight: '20px' }}>
          <ListGroupItem className="justify-content-between">
            Mã chuyến
            <div className="float-right">{itemList && itemList.get('id')}</div>
          </ListGroupItem>
          <ListGroupItem className="justify-content-between">
            Tuyến
            <div className="float-right">
              {itemList && itemList.getIn(['route', 'name'])}
            </div>
          </ListGroupItem>
          <ListGroupItem className="justify-content-between">
            Nhà xe
            <div className="float-right">
              {itemList && itemList.getIn(['provider', 'providerName'])}
            </div>
          </ListGroupItem>
          <ListGroupItem className="justify-content-between">
            Điểm đi
            <div className="float-right">
              {itemList && itemList.getIn(['route', 'startPoint', 'name'])}
            </div>
          </ListGroupItem>
          <ListGroupItem className="justify-content-between">
            Tài xế
            <div className="float-right">
              {itemList && itemList.getIn(['user', 'fullName'])}
            </div>
          </ListGroupItem>
          <ListGroupItem className="justify-content-between">
            Phụ xe
            <div className="float-right">
              {itemList && itemList.get('subDriver')}
            </div>
          </ListGroupItem>
          <ListGroupItem className="justify-content-between">
            Điểm đến
            <div className="float-right">
              {itemList && itemList.getIn(['route', 'endPoint', 'name'])}
            </div>
          </ListGroupItem>
          <ListGroupItem className="justify-content-between">
            Ngày chạy
            <div className="float-right">
              {itemList &&
                moment(itemList.get('startDate')).format('DD-MM-YYYY')}
            </div>
          </ListGroupItem>
          <ListGroupItem className="justify-content-between">
            Giờ
            <div className="float-right">
              {itemList && itemList.get('startHour')}
            </div>
          </ListGroupItem>
          <ListGroupItem className="justify-content-between">
            Xe
            <div className="float-right">
              {itemList && itemList.getIn(['vehicle', 'licensePlates'])}
            </div>
          </ListGroupItem>
          <ListGroupItem className="justify-content-between">
            Số ghế
            <div className="float-right">
              {itemList &&
                itemList.getIn(['vehicle', 'seatPattern', 'totalSeat'])}
            </div>
          </ListGroupItem>
          <ListGroupItem className="justify-content-between">
            Ghế trống
            <div className="float-right">
              {itemList && itemList.get('availableSeat')}
            </div>
          </ListGroupItem>
          <ListGroupItem className="justify-content-between">
            Giá vé
            <div className="float-right">
              {handleFormatPrice(
                itemList && itemList.getIn(['route', 'basePrice'])
              )}{' '}
              VNĐ
            </div>
          </ListGroupItem>
          <ListGroupItem className="justify-content-between">
            Trạng thái
            <div className="float-right">
              <Button color={status && status.color} size="sm">
                {status && status.label}
              </Button>
            </div>
          </ListGroupItem>
        </ListGroup>
      </div>
    </>
  )
}

DetailSchedule.propTypes = {
  itemList: PropTypes.object,
}
const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(DetailSchedule)
