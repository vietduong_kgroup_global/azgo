import React, { useEffect, useState, useContext } from 'react'
import { Form, Input, Button, Select, InputNumber, message } from 'antd'
import { Col, Row, Card, CardBody, CardHeader } from 'reactstrap'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import 'Stores/ProviderManagement/Reducers'
import 'Stores/ProviderManagement/Sagas'
import 'Stores/Intercity/SeatMapManagement/Reducers'
import 'Stores/Intercity/SeatMapManagement/Sagas'
import { SeatMapManagementActions } from 'Stores/Intercity/SeatMapManagement/Actions'
import './styles.scss'
import styles from './styles.module.scss'
import { ProviderManagementActions } from 'Stores/ProviderManagement/Actions'
import { ProviderManagementSelectors } from 'Stores/ProviderManagement/Selectors'
import { UserSelectors } from 'Stores/User/Selectors'
import * as config from 'Utils/enum'
import { UserContext } from 'Contexts/UserData'

const FormSeatMapContainer = ({
  form,
  id,
  item,
  createItem,
  editItem,
  getItemsProvider,
  getItemsListProvider,
  itemUser,
}) => {
  const UserInfo = useContext(UserContext)
  const { getFieldDecorator, getFieldsValue } = form
  const [totalSeats, setTotalSeats] = useState(0)
  const [showButton, setshowButton] = useState(true)
  const [seatMap, setSeatMap] = useState({
    rows: 0,
    cols: 0,
    f1: [],
    f2: [],
  })
  const handleChangeSeatOfHorizontal = () => {
    setshowButton(false)
  }
  const handleChangeSeatOfVertical = () => {
    setshowButton(false)
  }
  const handleChangeFloors = () => {
    setshowButton(false)
  }
  const { Option } = Select
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      } else {
        setshowButton(true)
        if (id) {
          setTotalSeats(0)
          renderSeatMap(
            getFieldsValue().maxSeatOfVertical, // column
            getFieldsValue().maxSeatOfHorizontal, // row
            getFieldsValue().floors // floor
          )
        } else {
          setTotalSeats(0)
          renderSeatMap(
            getFieldsValue().maxSeatOfVertical, // column
            getFieldsValue().maxSeatOfHorizontal, // row
            getFieldsValue().floors // floor
          )
        }
      }
    })
  }
  const submitForm = () => {
    form.validateFields(err => {
      if (err) {
        message.error('Tên ghế không được để trống')
        return false
      } else {
        let valueForm = getFieldsValue()
        let arr = []
        let arrFind = []
        Object.keys(valueForm).map(function(key) {
          arr.push({ id: key, seatName: valueForm[key] })
          if (typeof valueForm[key] === 'string') {
            arrFind.push(valueForm[key])
          }
          return arr, arrFind
        })
        const uniqueArray = [...new Set(arrFind)]
        if (uniqueArray.length !== arrFind.length) {
          message.error('Tên ghế không được trùng nhau!')
        } else {
          let arrFloor1 = seatMap.f1
          let arrFloor2 = seatMap.f2
          for (let i = 0; i < arrFloor1.length; i++) {
            for (let j = 0; j < arrFloor1[i].length; j++) {
              let result = arr.find(item => {
                return parseInt(item.id, 10) === arrFloor1[i][j].id
              })
              if (result && parseInt(result.id, 10) === arrFloor1[i][j].id) {
                arrFloor1[i][j].seatName = result.seatName
              }
            }
          }
          setSeatMap({
            ...seatMap,
            f1: arrFloor1,
          })
          if (seatMap.f2.length > 0) {
            for (let i = 0; i < arrFloor2.length; i++) {
              for (let j = 0; j < arrFloor2[i].length; j++) {
                let result = arr.find(item => {
                  return parseInt(item.id, 10) === arrFloor2[i][j].id
                })
                if (result && parseInt(result.id, 10) === arrFloor2[i][j].id) {
                  arrFloor2[i][j].seatName = result.seatName
                }
              }
            }
            setSeatMap({
              ...seatMap,
              f2: arrFloor2,
            })
          }
          setTimeout(function() {
            if (id) {
              editItem({
                ...getFieldsValue(),
                seatMap: seatMap,
                floor: getFieldsValue().floors,
                totalSeat: totalSeats,
                id: parseInt(id, 10),
                providerId:
                  itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
                    ? itemUser.getIn(['providersInfo', 'id'])
                    : form.getFieldsValue().providerId,
              })
            } else {
              createItem({
                ...getFieldsValue(),
                seatMap: seatMap,
                floor: getFieldsValue().floors,
                totalSeat: totalSeats,
                providerId:
                  itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
                    ? itemUser.getIn(['providersInfo', 'id'])
                    : form.getFieldsValue().providerId,
              })
            }
          }, 500)
        }
      }
    })
  }
  const renderSeatMap = (rows, cols, floors) => {
    let arrFloor1 = []
    let arrFloor2 = []
    for (let i = 1; i <= rows; i++) {
      let colArr = []
      for (let j = 1; j <= cols; j++) {
        let colItem = {
          id: parseInt('1' + i + '' + j, 10),
          number: i, // => rows
          name: '1' + i + '' + j,
          seatName: i + '' + j,
          isSelected: false,
          isReserved: false,
          status: false,
          isWaiting: false,
        }
        colArr.push(colItem);

      }
      arrFloor1.push(colArr)
    } // floor 1
    for (let i = 1; i <= rows; i++) {
      let colArr = []
      for (let j = 1; j <= cols; j++) {
        let colItem = {
          id: parseInt('2' + i + '' + j, 10),
          number: i,
          name: '2' + i + '' + j,
          seatName: '2' + i + '' + j,
          isSelected: false,
          isReserved: false,
          status: false,
          isWaiting: false,
        }
        colArr.push(colItem)
      }
      arrFloor2.push(colArr)
    } // floor 2
    if (floors === 2) {
      setSeatMap({
        rows: rows,
        cols: cols,
        f1: arrFloor1 || [],
        f2: arrFloor2 || [],
      })
    } else {
      setSeatMap({
        rows: rows,
        cols: cols,
        f1: arrFloor1 || [],
        f2: [],
      })
    }
  }
  const showSeat = item => {
    if (item.isSelected === true) {
      setTotalSeats(totalSeats - 1)
    } else {
      setTotalSeats(totalSeats + 1)
    }
    let floor = parseInt(item.id.toString().slice(0, 1), 10)
    let arrFloor
    if (floor === 1) {
      arrFloor = seatMap.f1
    } else {
      arrFloor = seatMap.f2
    }
    for (let i = 0; i < arrFloor.length; i++) {
      for (let j = 0; j < arrFloor[i].length; j++) {
        if (arrFloor[i][j].id === item.id) {
          arrFloor[i][j].isSelected = !arrFloor[i][j].isSelected
        }
      }
    }
    if (floor === 1) {
      setSeatMap({
        ...seatMap,
        f1: arrFloor,
      })
    } else {
      setSeatMap({
        ...seatMap,
        f2: arrFloor,
      })
    }
  }
  useEffect(() => {
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.ADMIN) {
      getItemsProvider()
    }
  }, [])
  // edit
  useEffect(() => {
    if (item && item.size > 0) {
      let seatmap = item.get('seatMap')
      let col = item.get('maxSeatOfHorizontal')
      let row = item.get('maxSeatOfVertical')
      setTotalSeats(item.get('totalSeat'))
      setSeatMap({
        rows: row,
        cols: col,
        f1: item.getIn(['seatMap', 'f1']) ? seatmap.toJS().f1 : [],
        f2: item.getIn(['seatMap', 'f2']) ? seatmap.toJS().f2 : [],
      })
    }
    return () => {
      setTotalSeats(0)
      setSeatMap({
        rows: 0,
        cols: 0,
        f1: [],
        f2: [],
      })
    }
  }, [item])

  return (
    <>
      <Row>
        <Col xs="12" sm="3">
          <Card
            style={{ height: '100%' }}
            className={id ? `card-accent-warning` : `card-accent-success`}>
            <CardHeader>
              <strong>Thông tin tạo sơ đồ ghế</strong>
            </CardHeader>
            <CardBody style={{ background: '#fafafa' }}>
              <Form id="frmRenderSeatMap">
                <Form.Item label="Tên sơ đồ">
                  {getFieldDecorator('name', {
                    initialValue: id && item.get('name'),
                    rules: [
                      {
                        required: true,
                        message: 'Trường bắt buộc!',
                      },
                      {
                        min: 1,
                        message: 'Trường không được ít hơn 1 ký tự!',
                      },
                      {
                        max: 30,
                        message: 'Trường không được quá 30 ký tự!',
                      },
                    ],
                  })(<Input />)}
                </Form.Item>
                {itemUser.get('type') !==
                  config.MANAGEMENT_TYPE_USER.PROVIDER && (
                  <Form.Item label="Nhà xe">
                    {getFieldDecorator('providerId', {
                      initialValue: id && item.getIn(['provider', 'id']),
                      rules: [{ required: true, message: 'Trường bắt buộc!' }],
                    })(
                      <Select placeholder="Vui lòng chọn nhà xe">
                        {getItemsListProvider.map(item => (
                          <Select.Option
                            value={item.get('id')}
                            key={`provider_${item.get('id')}`}>
                            {item.get('providerName')}
                          </Select.Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                )}
                <Form.Item label="Số lượng ghế nhiều nhất theo hàng ngang">
                  {getFieldDecorator('maxSeatOfHorizontal', {
                    initialValue: id && item.get('maxSeatOfHorizontal'),
                    rules: [
                      {
                        required: true,
                        message: 'Trường bắt buộc!',
                      },
                    ],
                  })(
                    <InputNumber
                      min={1}
                      max={20}
                      onChange={handleChangeSeatOfHorizontal}
                    />
                  )}
                </Form.Item>
                <Form.Item label="Số lượng ghế nhiều nhất theo hàng dọc">
                  {getFieldDecorator('maxSeatOfVertical', {
                    initialValue: id && item.get('maxSeatOfVertical'),
                    rules: [
                      {
                        required: true,
                        message: 'Trường bắt buộc!',
                      },
                    ],
                  })(
                    <InputNumber
                      min={1}
                      max={20}
                      onChange={handleChangeSeatOfVertical}
                    />
                  )}
                </Form.Item>
                <Form.Item label="Số tầng">
                  {getFieldDecorator('floors', {
                    initialValue: id && item.get('floor'),
                    rules: [
                      {
                        required: true,
                        message: 'Trường bắt buộc!',
                      },
                    ],
                  })(
                    <Select
                      placeholder="Vui lòng chọn số tầng"
                      onChange={handleChangeFloors}>
                      <Option value={1}>1</Option>
                      <Option value={2}>2</Option>
                      {/* <Option value={3}>3</Option> */}
                    </Select>
                  )}
                </Form.Item>
                <Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                    onClick={handleSubmit}>
                    Tạo
                  </Button>
                </Form.Item>
              </Form>
            </CardBody>
          </Card>
        </Col>
        <Col xs="12" sm="9">
          <Card
            style={{ height: '100%' }}
            className={id ? `card-accent-warning` : `card-accent-success`}>
            <CardHeader>
              <strong>Số chỗ: {totalSeats}</strong>
              {showButton && (
                <div className="card-header-actions">
                  <Button
                    type="primary"
                    htmlType="submit"
                    size="small"
                    onClick={() => submitForm()}>
                    Lưu sơ đồ
                  </Button>
                </div>
              )}
            </CardHeader>
            <CardBody
              style={{ maxHeight: 'calc(100vh - 215px)', overflow: 'auto' }}>
              <div className="seat-map">
                {seatMap.f1.length > 0 && (
                  <>
                    <div className={styles.seatMap}>
                      {seatMap.f2.length > 0 && (
                        <div className={styles.seatMapTitle}>Tầng 1</div>
                      )}
                      {seatMap.f1.map((row, index) => (
                        <div key={'row_' + index} className={styles.rowSeat}>
                          <span className={styles.rowIndex}>{index + 1}</span>
                          {row.map((item, index) => (
                            <div
                              key={'col_' + index}
                              className={
                                styles.seat +
                                ' ' +
                                `${item.isSelected ? styles.selected : ''}`
                              }>
                              <div
                                className={styles.click}
                                onClick={() => showSeat(item)}
                              />
                              {item.isSelected ? (
                                <Form.Item className="frmSeatMap">
                                  {getFieldDecorator(`${item.id}`, {
                                    initialValue: item.seatName
                                      ? item.seatName
                                      : item.name,
                                    rules: [
                                      {
                                        required: true,
                                        message: false,
                                      },
                                    ],
                                  })(<Input />)}
                                </Form.Item>
                              ) : null}
                            </div>
                          ))}
                        </div>
                      ))}
                    </div>
                  </>
                )}
                {seatMap.f2.length > 0 && (
                  <>
                    <div className={styles.seatMap}>
                      <div className={styles.seatMapTitle}>Tầng 2</div>
                      {seatMap.f2.map((row, index) => (
                        <div key={'row_' + index} className={styles.rowSeat}>
                          <span className={styles.rowIndex}>{index + 1}</span>
                          {row.map((item, index) => (
                            <div
                              key={'col_' + index}
                              className={
                                styles.seat +
                                ' ' +
                                `${item.isSelected ? styles.selected : ''}`
                              }>
                              <div
                                className={styles.click}
                                onClick={() => showSeat(item)}
                              />
                              {item.isSelected ? (
                                <Form.Item className="frmSeatMap">
                                  {getFieldDecorator(`${item.id}`, {
                                    initialValue: item.seatName
                                      ? item.seatName
                                      : item.name,
                                    rules: [
                                      {
                                        required: true,
                                        message: false,
                                      },
                                    ],
                                  })(<Input />)}
                                </Form.Item>
                              ) : null}
                            </div>
                          ))}
                        </div>
                      ))}
                    </div>
                  </>
                )}
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </>
  )
}

const mapStateToProps = state => ({
  getItemsListProvider: ProviderManagementSelectors.getItemsList(state),
  itemUser: UserSelectors.getUserData(state),
})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(SeatMapManagementActions.createItemRequest(values)),
  getItemsProvider: values =>
    dispatch(ProviderManagementActions.getItemsListRequest(values)),
  editItem: values =>
    dispatch(SeatMapManagementActions.editItemRequest(values)),
})

FormSeatMapContainer.propTypes = {
  id: PropTypes.any,
  item: PropTypes.object,
  form: PropTypes.any,
  createItem: PropTypes.func,
  editItem: PropTypes.func,
  getItemsListProvider: PropTypes.object,
  getItemsProvider: PropTypes.func,
  itemUser: PropTypes.object,
}
const FormSeatMap = Form.create()(FormSeatMapContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormSeatMap)
