import React, { useEffect, useState } from 'react'
import { Card, CardBody, CardHeader } from 'reactstrap'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import './styles.scss'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { message, Modal, Form, Tabs, Row, Col, Button } from 'antd'
import TicketInfo from 'components/molecules/TicketInfo'
import FormTicketAdmin from 'components/organisms/Intercity/FormTicketAdmin'
import FormTicketAdminSpecify from 'components/organisms/Intercity/FormTicketAdminSpecify'
import DetailSchedule from 'components/organisms/Intercity/DetailSchedule'
import { UserSelectors } from 'Stores/User/Selectors'
import { formatPhone } from 'Utils/helper'
import * as config from 'Utils/enum'
import { fromJS } from 'immutable'

const FormTicketContainer = ({ id, itemList, showPopUp, itemUser }) => {
  const { TabPane } = Tabs
  const [seatMap, setSeatMap] = useState({
    f1: [],
    f2: [],
  })
  const [visible, setVisible] = useState(false)
  const [arrSeat, setArrSeat] = useState([])
  const [idTicket, setIdTicket] = useState(null)
  const [statusSchedule, setStatusSchedule] = useState({})
  useEffect(() => {
    if (itemList && itemList.size > 0) {
      let floor1 = itemList.getIn(['seatMap', 'f1'])
        ? itemList.getIn(['seatMap', 'f1']).toJS()
        : null
      let floor2 = itemList.getIn(['seatMap', 'f2'])
        ? itemList.getIn(['seatMap', 'f2']).toJS()
        : []
      let ticket = itemList.get('tickets') ? itemList.get('tickets').toJS() : []
      let arrFloor1 = []
      let arrFloor2 = []
      for (let i = 0; i < floor1.length; i++) {
        let colArr = []
        for (let j = 0; j < floor1[i].length; j++) {
          let colItem
          const result = ticket.find(item => {
            return item.seatName === floor1[i][j].seatName
          })
          if (result && result.seatName === floor1[i][j].seatName) {
            colItem = {
              id: floor1[i][j].id,
              number: floor1[i][j].number,
              name: floor1[i][j].name,
              seatName: floor1[i][j].seatName,
              isSelected: floor1[i][j].isSelected,
              isReserved: floor1[i][j].isReserved,
              status: floor1[i][j].status,
              isWaiting: floor1[i][j].isWaiting,
              ticketId: result.id,
              phone: result.user.phone,
              countryCode: result.user.countryCode,
              nameTicket: result.user.customerProfile.firstName
                ? result.user.customerProfile.firstName
                : result.user.customerProfile.fullName,
            }
          } else {
            colItem = {
              id: floor1[i][j].id,
              number: floor1[i][j].number,
              name: floor1[i][j].name,
              seatName: floor1[i][j].seatName,
              isSelected: floor1[i][j].isSelected,
              isReserved: floor1[i][j].isReserved,
              status: floor1[i][j].status,
              isWaiting: floor1[i][j].isWaiting,
            }
          }
          colArr.push(colItem)
        }
        arrFloor1.push(colArr)
      }
      for (let i = 0; i < floor2.length; i++) {
        let colArr = []
        for (let j = 0; j < floor2[i].length; j++) {
          let colItem
          const result = ticket.find(item => {
            return item.seatName === floor2[i][j].seatName
          })
          if (result && result.seatName === floor2[i][j].seatName) {
            colItem = {
              id: floor2[i][j].id,
              number: floor2[i][j].number,
              name: floor2[i][j].name,
              seatName: floor2[i][j].seatName,
              isSelected: floor2[i][j].isSelected,
              isReserved: floor2[i][j].isReserved,
              status: floor2[i][j].status,
              isWaiting: floor1[i][j].isWaiting,
              ticketId: result.id,
              phone: result.user.phone,
              countryCode: result.user.countryCode,
              nameTicket: result.user.customerProfile.firstName
                ? result.user.customerProfile.firstName
                : result.user.customerProfile.fullName,
            }
          } else {
            colItem = {
              id: floor2[i][j].id,
              number: floor2[i][j].number,
              name: floor2[i][j].name,
              seatName: floor2[i][j].seatName,
              isSelected: floor2[i][j].isSelected,
              isReserved: floor2[i][j].isReserved,
              status: floor2[i][j].status,
              isWaiting: floor2[i][j].isWaiting,
            }
          }
          colArr.push(colItem)
        }
        arrFloor2.push(colArr)
      }
      setSeatMap({
        f1: arrFloor1 || [],
        f2: arrFloor2 || [],
      })
    }
    const result = config.MANAGEMENT_STATUS_SCHEDULE.find(item => {
      return item.key === itemList.get('status')
    })
    setStatusSchedule(result)
  }, [itemList])
  const selectSeat = item => {
    if (!item.ticketId && itemList.get('status') === 1) {
      // start set arr
      const findMovies = selectedClass => {
        return selectedClass.name === item.name
      }
      let index = arrSeat.findIndex(findMovies)

      if (index === -1) {
        arrSeat.push({ seatName: item.seatName, name: item.name })
      } else {
        arrSeat.splice(index, 1)
      }
      // end set arr

      let floor = parseInt(item.id.toString().slice(0, 1), 10)
      let arrFloor
      if (floor === 1) {
        arrFloor = seatMap.f1
      } else {
        arrFloor = seatMap.f2
      }
      for (let i = 0; i < arrFloor.length; i++) {
        for (let j = 0; j < arrFloor[i].length; j++) {
          if (arrFloor[i][j].id === item.id) {
            arrFloor[i][j].isReserved = !arrFloor[i][j].isReserved
          }
        }
      }
      if (floor === 1) {
        setSeatMap({
          ...seatMap,
          f1: arrFloor,
        })
      } else {
        setSeatMap({
          ...seatMap,
          f2: arrFloor,
        })
      }
    }
    if (item.ticketId) {
      setIdTicket(item.ticketId)
      setVisible(true)
    }
    if (itemList.get('status') !== 1) {
      if (!item.ticketId) {
        message.warning(
          `Chuyến xe '${statusSchedule &&
            statusSchedule.label}' nên không thể đặt vé`
        )
      }
    }
  }
  const showPopupTicket = () => {
    setVisible(true)
  }
  const handleCancel = () => {
    setVisible(false)
    setIdTicket(null)
  }
  const callbackTab = key => {
    console.log(key)
  }
  const callbackHandlerFunction = () => {
    handleCancel()
  }
  useEffect(() => {
    if (showPopUp) {
      setArrSeat([])
      handleCancel()
    }
  }, [showPopUp])

  return (
    <>
      <Card
        className={id ? `card-accent-warning` : `card-accent-success`}
        style={{ height: 'auto' }}>
        <CardHeader>
          <strong>Sơ đồ vé</strong>
        </CardHeader>
        <CardBody
          style={{ maxHeight: 'calc(100vh - 175px)', overflow: 'auto' }}>
          <Row>
            <Col span={8}>
              <DetailSchedule itemList={itemList} />
            </Col>
            <Col span={16}>
              {itemList.get('status') === 1 && (
                <div className="selected-seat-ticket">
                  <div className="selected-seat-ticket-inner">
                    <div className="label">Ghế được chọn:</div>
                    <div className="arr-selected-seat">
                      {arrSeat.map(item => (
                        <span key={item.name}>{item.seatName}</span>
                      ))}
                    </div>
                  </div>
                  {arrSeat.length > 0 && (
                    <Button
                      type="primary"
                      size="small"
                      onClick={showPopupTicket}>
                      Đặt vé
                    </Button>
                  )}
                </div>
              )}
              <div className="content-seatmap">
                <div className="direction-seat">
                  <div className="row-direction-seat">
                    <span className="label">Chưa đặt</span>
                    <span className="seat_ct selected" />
                  </div>
                  <div className="row-direction-seat">
                    <span className="label">Đã đặt</span>
                    <span className="seat_ct selected status" />
                  </div>
                  {itemList.get('status') === 1 && (
                    <div className="row-direction-seat">
                      <span className="label">Đang chọn</span>
                      <span className="seat_ct selected selectedTicket" />
                    </div>
                  )}
                </div>
                <div className="content-seatmap-inner">
                  {seatMap.f1.length > 0 && (
                    <>
                      <div className="seat-map">
                        {seatMap.f2.length > 0 && (
                          <div className="seat-map-title">Tầng 1</div>
                        )}
                        {seatMap.f1.map((row, index) => (
                          <div key={'row_' + index} className="row_seat">
                            <span className="row-index">{index + 1}</span>
                            {row.map((item, index) => {
                              return (
                                <div
                                  key={'col_' + index}
                                  className={
                                    `seat_ct` +
                                    `${item.isSelected ? ` selected` : ''}` +
                                    `${item.status ? ` status` : ''}` +
                                    `${
                                      item.isReserved ? ` selectedTicket` : ''
                                    }`
                                  }
                                  onClick={() => selectSeat(item)}>
                                  <div>
                                    <strong>
                                      {item.seatName
                                        ? item.seatName
                                        : item.name.toString().slice(1, 3)}
                                    </strong>
                                  </div>
                                  {item.phone && (
                                    <div style={{ fontSize: '12px' }}>
                                      {formatPhone(
                                        item.countryCode,
                                        item.phone
                                      )}
                                    </div>
                                  )}
                                  {item.nameTicket && (
                                    <span>{item.nameTicket}</span>
                                  )}
                                </div>
                              )
                            })}
                          </div>
                        ))}
                      </div>
                    </>
                  )}
                  {seatMap.f2.length > 0 && (
                    <>
                      <div className="seat-map">
                        <div className="seat-map-title">Tầng 2</div>
                        {seatMap.f2.map((row, index) => (
                          <div key={'row_' + index} className="row_seat">
                            <span className="row-index">{index + 1}</span>
                            {row.map((item, index) => (
                              <div
                                key={'col_' + index}
                                className={
                                  `seat_ct` +
                                  `${item.isSelected ? ` selected` : ''}` +
                                  `${item.status ? ` status` : ''}` +
                                  `${item.isReserved ? ` selectedTicket` : ''}`
                                }
                                onClick={() => selectSeat(item)}>
                                <div>
                                  <strong>
                                    {item.seatName
                                      ? item.seatName
                                      : item.name.toString().slice(1, 3)}
                                  </strong>
                                </div>
                                {item.phone && (
                                  <div style={{ fontSize: '12px' }}>
                                    {formatPhone(item.countryCode, item.phone)}
                                  </div>
                                )}
                                {item.nameTicket && (
                                  <span>{item.nameTicket}</span>
                                )}
                              </div>
                            ))}
                          </div>
                        ))}
                      </div>
                    </>
                  )}
                </div>
              </div>
            </Col>
          </Row>
        </CardBody>
      </Card>
      <TicketInfo
        handleClickParent={callbackHandlerFunction}
        itemList={itemList}
        idTicket={idTicket > 0 ? idTicket : null}
      />
      <Modal
        title="Chọn khách hàng để đặt vé"
        visible={visible && !idTicket && itemList.get('status') === 1}
        centered
        onCancel={() => handleCancel()}
        className="ant-modal-custom"
        footer={null}>
        {itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER && (
          <FormTicketAdmin
            itemList={itemList}
            arrSeat={arrSeat}
            visible={visible}
          />
        )}
        {itemUser.get('type') === config.MANAGEMENT_TYPE_USER.ADMIN && (
          <Tabs defaultActiveKey="1" onChange={callbackTab}>
            <TabPane tab="Khách hàng có sẵn" key="1">
              <FormTicketAdminSpecify
                itemList={itemList}
                arrSeat={arrSeat}
                visible={visible}
              />
            </TabPane>
            <TabPane tab="Khách hàng mới" key="2">
              <FormTicketAdmin
                itemList={itemList}
                arrSeat={arrSeat}
                visible={visible}
              />
            </TabPane>
          </Tabs>
        )}
      </Modal>
    </>
  )
}
const mapStateToProps = state => ({
  showPopUp: LoadingSelectors.getShowPopUp(state),
  itemUser: UserSelectors.getUserData(state),
})
const mapDispatchToProps = dispatch => ({})
FormTicketContainer.propTypes = {
  id: PropTypes.any,
  itemList: PropTypes.object,
  showPopUp: PropTypes.bool,
  itemUser: PropTypes.object,
}
const FormTicket = Form.create()(FormTicketContainer)
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormTicket)
