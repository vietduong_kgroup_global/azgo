import React, { useEffect, useState, useContext } from 'react'
import { UserContext } from 'Contexts/UserData'
import {
  Form,
  Input,
  Button,
  Select,
  Icon,
  Upload,
  message,
  Modal,
  Row,
  Col,
} from 'antd'
import PropTypes from 'prop-types'
import 'Stores/ProviderManagement/Reducers'
import 'Stores/ProviderManagement/Sagas'
import 'Stores/Intercity/VehicleManagement/Reducers'
import 'Stores/Intercity/VehicleManagement/Sagas'
import 'Stores/Intercity/VehicleTypeManagement/Reducers'
import 'Stores/Intercity/VehicleTypeManagement/Sagas'
import 'Stores/Intercity/SeatMapManagement/Reducers'
import 'Stores/Intercity/SeatMapManagement/Sagas'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { ProviderManagementActions } from 'Stores/ProviderManagement/Actions'
import { ProviderManagementSelectors } from 'Stores/ProviderManagement/Selectors'
import { VehicleTypeManagementActions } from 'Stores/Intercity/VehicleTypeManagement/Actions'
import { VehicleTypeManagementSelectors } from 'Stores/Intercity/VehicleTypeManagement/Selectors'
import { VehicleManagementActions } from 'Stores/Intercity/VehicleManagement/Actions'
import { VehicleManagementSelectors } from 'Stores/Intercity/VehicleManagement/Selectors'
import { SeatMapManagementActions } from 'Stores/Intercity/SeatMapManagement/Actions'
import { SeatMapManagementSelectors } from 'Stores/Intercity/SeatMapManagement/Selectors'
import { UserSelectors } from 'Stores/User/Selectors'
import * as config from 'Utils/enum'
import { Config } from '../../../../Config'
import { getToken } from '../../../../Utils/token'

const FormVehicleContainer = ({
  form,
  id,
  item,
  getItem,
  itemsListProvider,
  getItemsProvider,
  itemsVehicleType,
  itemsSeatMap,
  getItemsVehicleType,
  getItemsSeatMap,
  editItem,
  createItem,
  filter,
  setFilter,
  providerDetail,
  itemUser,
}) => {
  const {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue,
    resetFields,
  } = form
  const [loading, setLoading] = useState('')
  const [imageAvatarUrl, setImageAvatarUrl] = useState([])
  const [imageFrontCarInsurance, setImageFrontCarInsurance] = useState('')
  const [imageBackCarInsurance, setImageBackCarInsurance] = useState('')
  const [imageFrontCertificates, setImageFrontCertificates] = useState('')
  const [imageBackCertificates, setImageBackCertificates] = useState('')
  const [imageAvatarUrlPopup, setImageAvatarUrlPopup] = useState('')
  const [previewVisible, setPreviewVisible] = useState(false)
  const [idProvider, setIdProvider] = useState(null)
  const [reset, setReset] = useState(true)
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      if (id) {
        editItem({
          ...getFieldsValue(),
          seatPatternId: parseInt(form.getFieldsValue().seatPatternId, 10),
          providerId:
            itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
              ? itemUser.getIn(['providersInfo', 'id'])
              : form.getFieldsValue().providerId,
          id,
        })
      } else {
        createItem({
          ...getFieldsValue(),
          seatPatternId: parseInt(form.getFieldsValue().seatPatternId, 10),
          providerId:
            itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
              ? itemUser.getIn(['providersInfo', 'id'])
              : form.getFieldsValue().providerId,
        })
      }
    })
  }

  const propsHeaderUpload = {
    name: 'images[]',
    action: Config.DEV_URL.UPLOAD_IMAGE,
    headers: {
      Authorization: 'bearer ' + getToken(),
    },
  }
  const getBase64 = (img, callback) => {
    const reader = new FileReader()
    reader.addEventListener('load', () => callback(reader.result))
    reader.readAsDataURL(img)
  }

  const handleChangeSelectProvider = value => {
    setIdProvider(value)
    setReset(false)
    resetFields('vehicleTypeId')
    resetFields('seatPatternId')
  }
  const handlePreviewImg = (value, idx) => {
    switch (value) {
      case 'driverImg': {
        for (let i = 0; i < imageAvatarUrl.length; i++) {
          if (idx === i) {
            setImageAvatarUrlPopup(
              Config.DEV_URL.IMAGE +
                '/' +
                imageAvatarUrl[i].filePath +
                '/' +
                imageAvatarUrl[i].fileName
            )
          }
        }
        break
      }
      case 'frontCertificates': {
        setImageAvatarUrlPopup(imageFrontCertificates)
        break
      }
      case 'backCertificates': {
        setImageAvatarUrlPopup(imageBackCertificates)
        break
      }
      case 'frontInsurance': {
        setImageAvatarUrlPopup(imageFrontCarInsurance)
        break
      }
      default: {
        setImageAvatarUrlPopup(imageBackCarInsurance)
        break
      }
    }
    setPreviewVisible(true)
  }
  const handleChange = (info, value) => {
    if (info.file.status === 'uploading') {
      switch (value) {
        case 'driverImg': {
          setLoading('driverImg')
          break
        }
        case 'frontCertificates': {
          setLoading('frontCertificates')
          break
        }
        case 'backCertificates': {
          setLoading('backCertificates')
          break
        }
        case 'frontInsurance': {
          setLoading('frontInsurance')
          break
        }
        default: {
          setLoading('backInsurance')
          break
        }
      }
    }

    if (info.file.status === 'done') {
      getBase64(info.file.originFileObj, imageUrl => {
        setLoading('')
        let imageProfile = {
          filePath: info.file.response.data[0].file_path,
          fileName: info.file.response.data[0].file_name,
        }
        switch (value) {
          case 'driverImg': {
            let img =
              Config.DEV_URL.IMAGE +
              '/' +
              imageProfile.filePath +
              '/' +
              imageProfile.fileName
            if (img) {
              imageAvatarUrl.push(imageProfile)
              setFieldsValue({
                images: imageAvatarUrl,
              })
            }
            break
          }
          case 'frontCertificates': {
            setImageFrontCertificates(imageUrl)
            setFieldsValue({
              frontRegistrationCertificates: imageProfile,
            })
            break
          }
          case 'backCertificates': {
            setImageBackCertificates(imageUrl)
            setFieldsValue({
              backRegistrationCertificates: imageProfile,
            })
            break
          }
          case 'frontInsurance': {
            setImageFrontCarInsurance(imageUrl)
            setFieldsValue({
              frontCarInsurance: imageProfile,
            })
            break
          }
          default: {
            setImageBackCarInsurance(imageUrl)
            setFieldsValue({
              backCarInsurance: imageProfile,
            })
            break
          }
        }
      })
    } else if (info.file.status === 'error') {
      setLoading(false)
      message.error('Upload failed')
    }
  }
  const handleRemoveField = index => {
    imageAvatarUrl.splice(index, 1)
    setFieldsValue({
      images: imageAvatarUrl,
    })
  }
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 7 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 20,
        offset: 4,
      },
    },
  }
  const formItemLayoutUpload = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 20 },
    },
  }
  useEffect(() => {
    if (item) {
      setIdProvider(item.getIn(['provider', 'id']))
    }
    setReset(true)
  }, [item])
  useEffect(() => {
    if (providerDetail) {
      setIdProvider(providerDetail)
    }
  }, [providerDetail])
  useEffect(() => {
    if (idProvider) {
      setFilter({
        provider_id: idProvider,
      })
      getItemsVehicleType(idProvider)
    }
  }, [idProvider])
  useEffect(() => {
    if (filter.get('provider_id') !== '') {
      getItemsSeatMap(filter)
    }
  }, [filter])
  useEffect(() => {
    if (id) {
      getItem(id)
    }
    return () => {
      if (
        providerDetail ||
        itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
      ) {
        resetFields()
      } else {
        resetFields()
        setIdProvider(null)
      }
    }
  }, [id])
  useEffect(() => {
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.ADMIN) {
      getItemsProvider()
    }
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER) {
      setIdProvider(itemUser.getIn(['providersInfo', 'id']))
    }
  }, [itemUser])
  useEffect(() => {
    return () => {
      if (
        providerDetail ||
        itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
      ) {
        resetFields()
      } else {
        resetFields()
        setIdProvider(null)
      }
    }
  }, [])
  useEffect(() => {
    if (item.get('images')) {
      setImageAvatarUrl(...[item.get('images').toJS()])
    }
    if (item.get('frontRegistrationCertificates')) {
      setImageFrontCertificates(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['frontRegistrationCertificates', 'filePath']) +
          '/' +
          item.getIn(['frontRegistrationCertificates', 'fileName'])
      )
    }
    if (item.get('backRegistrationCertificates')) {
      setImageBackCertificates(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['backRegistrationCertificates', 'filePath']) +
          '/' +
          item.getIn(['backRegistrationCertificates', 'fileName'])
      )
    }
    if (item.get('frontCarInsurance')) {
      setImageFrontCarInsurance(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['frontCarInsurance', 'filePath']) +
          '/' +
          item.getIn(['frontCarInsurance', 'fileName'])
      )
    }
    if (item.get('backCarInsurance')) {
      setImageBackCarInsurance(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['backCarInsurance', 'filePath']) +
          '/' +
          item.getIn(['backCarInsurance', 'fileName'])
      )
    }
    getItemsVehicleType(item.getIn(['vehicleType', 'vehicleGroupId']))
    return () => {
      setImageAvatarUrl([])
      setImageFrontCertificates('')
      setImageBackCertificates('')
      setImageFrontCarInsurance('')
      setImageBackCarInsurance('')
    }
  }, [item])
  return (
    <>
      <Form
        id="frmVehicleBus"
        {...formItemLayout}
        className="input-floating form-ant-custom-upload">
        <Form.Item label="Biển số xe">
          {getFieldDecorator('licensePlates', {
            initialValue: id && item.get('licensePlates'),
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                min: 1,
                message: 'Trường không được ít hơn 1 ký tự!',
              },
              {
                max: 10,
                message: 'Trường không được quá 10 ký tự!',
              },
            ],
          })(<Input />)}
        </Form.Item>
        {providerDetail &&
          itemUser.get('type') !== config.MANAGEMENT_TYPE_USER.PROVIDER && (
            <Form.Item label="Nhà xe">
              {getFieldDecorator('providerId', {
                initialValue: providerDetail,
                rules: [{ required: true, message: 'Trường bắt buộc!' }],
              })(
                <Select
                  placeholder="Vui lòng chọn nhà xe"
                  onChange={handleChangeSelectProvider}
                  disabled>
                  {itemsListProvider &&
                    itemsListProvider.map(item => (
                      <Select.Option
                        value={item.get('id')}
                        key={`provider_${item.get('id')}`}>
                        {item.get('providerName')}
                      </Select.Option>
                    ))}
                </Select>
              )}
            </Form.Item>
          )}
        {!providerDetail &&
          itemUser.get('type') !== config.MANAGEMENT_TYPE_USER.PROVIDER && (
            <Form.Item label="Nhà xe">
              {getFieldDecorator('providerId', {
                initialValue: id && item.get('providerId'),
                rules: [{ required: true, message: 'Trường bắt buộc!' }],
              })(
                <Select
                  placeholder="Vui lòng chọn nhà xe"
                  onChange={handleChangeSelectProvider}>
                  {itemsListProvider.map(item => (
                    <Select.Option
                      value={item.get('id')}
                      key={`provider_${item.get('id')}`}>
                      {item.get('providerName')}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
          )}
        <Form.Item label="Loại xe">
          {getFieldDecorator('vehicleTypeId', {
            initialValue: reset ? id && item.get('vehicleTypeId') : null,
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(
            <Select placeholder="Vui lòng chọn loại xe">
              {idProvider &&
                itemsVehicleType.map(item => (
                  <Select.Option
                    value={item.get('id')}
                    key={`vehicle_type_${item.get('id')}`}>
                    {item.get('name')}
                  </Select.Option>
                ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Sơ đồ ngồi">
          {getFieldDecorator('seatPatternId', {
            initialValue: id && reset && item.getIn(['seatPattern', 'id']),
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(
            <Select placeholder="Vui lòng sơ đồ ghế">
              {idProvider &&
                itemsSeatMap.map(item => (
                  <Select.Option
                    value={item.get('id')}
                    key={`seat_map_${item.get('id')}`}>
                    {item.get('name')}
                  </Select.Option>
                ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item
          label="Hình ảnh xe"
          className="ant-form-item-load"
          {...formItemLayoutUpload}>
          <div className="row-upload-img">
            <div className="col-upload-img col-upload-img-arr">
              {imageAvatarUrl &&
                imageAvatarUrl.map((item, idx) => (
                  <div className="frame-img" key={idx}>
                    <Icon
                      type="delete"
                      onClick={() => handleRemoveField(idx)}
                    />
                    <img
                      src={
                        Config.DEV_URL.IMAGE +
                        '/' +
                        item.filePath +
                        '/' +
                        item.fileName
                      }
                      onClick={() => handlePreviewImg('driverImg', idx)}
                      alt="driverImg"
                    />
                  </div>
                ))}
            </div>
            {imageAvatarUrl.length < 5 ? (
              <div className="col-upload-img-button">
                <Upload
                  name="avatar"
                  className="avatar-uploader"
                  {...propsHeaderUpload}
                  showUploadList={false}
                  onChange={e => handleChange(e, 'driverImg')}>
                  <Button>
                    <Icon type="upload" /> Tải hình
                    {loading === 'driverImg' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                </Upload>
              </div>
            ) : null}
          </div>
        </Form.Item>
        <Form.Item
          label="Giấy tờ xe"
          className="ant-form-item-load"
          {...formItemLayoutUpload}>
          <Row>
            <Col span={8}>
              <div className="frame-img frame-img frame-img-big">
                <div className="frame-img-inner">
                  {imageFrontCertificates && imageFrontCertificates !== '' ? (
                    <img
                      src={imageFrontCertificates}
                      onClick={() => handlePreviewImg('frontCertificates')}
                      alt="image"
                    />
                  ) : (
                    <Icon type="picture" />
                  )}
                </div>
              </div>
              <Upload
                name="avatar"
                className="avatar-uploader"
                {...propsHeaderUpload}
                showUploadList={false}
                onChange={e => handleChange(e, 'frontCertificates')}>
                {imageFrontCertificates && imageFrontCertificates !== '' ? (
                  <Button>
                    <Icon type="edit" /> Thay đổi hình giấy tờ xe mặt trước
                    {loading === 'frontCertificates' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                ) : (
                  <Button>
                    <Icon type="upload" /> Tải hình giấy tờ xe mặt trước
                    {loading === 'frontCertificates' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                )}
              </Upload>
            </Col>
            <Col span={8}>
              <div className="frame-img frame-img-big">
                <div className="frame-img-inner">
                  {imageBackCertificates && imageBackCertificates !== '' ? (
                    <img
                      src={imageBackCertificates}
                      onClick={() => handlePreviewImg('backCertificates')}
                      alt="backInsurance"
                    />
                  ) : (
                    <Icon type="picture" />
                  )}
                </div>
              </div>
              <Upload
                name="avatar"
                className="avatar-uploader"
                {...propsHeaderUpload}
                showUploadList={false}
                onChange={e => handleChange(e, 'backCertificates')}>
                {imageBackCertificates && imageBackCertificates !== '' ? (
                  <Button>
                    <Icon type="edit" /> Thay đổi hình giấy tờ xe mặt sau
                    {loading === 'backCertificates' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                ) : (
                  <Button>
                    <Icon type="upload" /> Tải hình giấy tờ xe mặt sau
                    {loading === 'backCertificates' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                )}
              </Upload>
            </Col>
          </Row>
        </Form.Item>
        <Form.Item
          label="Bảo hiểm xe"
          className="ant-form-item-load"
          {...formItemLayoutUpload}>
          <Row>
            <Col span={8}>
              <div className="frame-img frame-img-big">
                <div className="frame-img-inner">
                  {imageFrontCarInsurance && imageFrontCarInsurance !== '' ? (
                    <img
                      src={imageFrontCarInsurance}
                      onClick={() => handlePreviewImg('frontInsurance')}
                      alt="image"
                    />
                  ) : (
                    <Icon type="picture" />
                  )}
                </div>
              </div>
              <Upload
                name="avatar"
                className="avatar-uploader"
                {...propsHeaderUpload}
                showUploadList={false}
                onChange={e => handleChange(e, 'frontInsurance')}>
                {imageFrontCarInsurance && imageFrontCarInsurance !== '' ? (
                  <Button>
                    <Icon type="edit" /> Thay đổi hình bảo hiểm xe mặt trước
                    {loading === 'frontInsurance' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                ) : (
                  <Button>
                    <Icon type="upload" /> Tải hình bảo hiểm xe mặt trước
                    {loading === 'frontInsurance' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                )}
              </Upload>
            </Col>
            <Col span={8}>
              <div className="frame-img frame-img-big">
                <div className="frame-img-inner">
                  {imageBackCarInsurance && imageBackCarInsurance !== '' ? (
                    <img
                      src={imageBackCarInsurance}
                      onClick={() => handlePreviewImg('backInsurance')}
                      alt="backInsurance"
                    />
                  ) : (
                    <Icon type="picture" />
                  )}
                </div>
              </div>
              <Upload
                name="avatar"
                className="avatar-uploader"
                {...propsHeaderUpload}
                showUploadList={false}
                onChange={e => handleChange(e, 'backInsurance')}>
                {imageBackCarInsurance && imageBackCarInsurance !== '' ? (
                  <Button>
                    <Icon type="edit" /> Thay đổi hình bảo hiểm xe mặt sau
                    {loading === 'backInsurance' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                ) : (
                  <Button>
                    <Icon type="upload" /> Tải hình bảo hiểm xe mặt sau
                    {loading === 'backInsurance' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                )}
              </Upload>
            </Col>
          </Row>
        </Form.Item>
        <Modal
          visible={previewVisible}
          footer={null}
          onCancel={() => setPreviewVisible(false)}>
          <div className="modal-image">
            <img alt="image" src={imageAvatarUrlPopup} />
          </div>
        </Modal>
        <Form.Item hidden>
          {getFieldDecorator('images', {
            initialValue:
              id && item.get('images') ? item.get('images').toJS() : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('frontRegistrationCertificates', {
            initialValue:
              id && item.get('frontRegistrationCertificates')
                ? item.get('frontRegistrationCertificates').toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('backRegistrationCertificates', {
            initialValue:
              id && item.get('backRegistrationCertificates')
                ? item.get('backRegistrationCertificates').toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('frontCarInsurance', {
            initialValue:
              id && item.get('frontCarInsurance')
                ? item.get('frontCarInsurance').toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('backCarInsurance', {
            initialValue:
              id && item.get('backCarInsurance')
                ? item.get('backCarInsurance').toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Xác nhận
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({
  item: VehicleManagementSelectors.getItem(state),
  itemsListProvider: ProviderManagementSelectors.getItemsList(state),
  itemsVehicleType: VehicleTypeManagementSelectors.getItemsVehicleType(state),
  itemsSeatMap: SeatMapManagementSelectors.getItems(state),
  filter: SeatMapManagementSelectors.getFilter(state),
  itemUser: UserSelectors.getUserData(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(VehicleManagementActions.getItemDetailRequest(id)),
  getItemsSeatMap: filter =>
    dispatch(SeatMapManagementActions.getItemsRequest(filter)),
  getItemsProvider: values =>
    dispatch(ProviderManagementActions.getItemsListRequest(values)),
  getItemsVehicleType: values =>
    dispatch(VehicleTypeManagementActions.getItemsVehicleTypeRequest(values)),
  editItem: values =>
    dispatch(VehicleManagementActions.editItemRequest(values)),
  createItem: values =>
    dispatch(VehicleManagementActions.createItemRequest(values)),
  setFilter: filter => dispatch(SeatMapManagementActions.setFilter(filter)),
  clearFilter: () => dispatch(SeatMapManagementActions.clearFilter()),
})

FormVehicleContainer.propTypes = {
  id: PropTypes.any,
  item: PropTypes.object,
  getItem: PropTypes.func,
  form: PropTypes.any,
  getItemsProvider: PropTypes.func,
  getItemsSeatMap: PropTypes.func,
  itemsListProvider: PropTypes.object,
  getItemsVehicleType: PropTypes.func,
  itemsVehicleType: PropTypes.object,
  itemsSeatMap: PropTypes.object,
  editItem: PropTypes.func,
  createItem: PropTypes.func,
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
  providerDetail: PropTypes.number,
  itemUser: PropTypes.object,
}
const FormVehicle = Form.create()(FormVehicleContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormVehicle)
