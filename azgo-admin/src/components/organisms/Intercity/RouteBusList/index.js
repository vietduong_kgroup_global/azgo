import React, { useEffect } from 'react'
import 'Stores/Intercity/RouteManagement/Reducers'
import 'Stores/Intercity/RouteManagement/Sagas'
import { RouteManagementActions } from 'Stores/Intercity/RouteManagement/Actions'
import { RouteManagementSelectors } from 'Stores/Intercity/RouteManagement/Selectors'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Card, CardBody, CardHeader } from 'reactstrap'
import RouterContainer from 'containers/Intercity/Route'

const RouteBusList = ({ filter, setFilter, idProvider }) => {
  useEffect(() => {
    if (idProvider) {
      setFilter({
        provider_id: idProvider,
      })
    }
  }, [idProvider])
  return (
    <Card>
      <CardHeader>
        <strong>Danh sách tuyến</strong>
      </CardHeader>
      <CardBody style={{ padding: '0' }}>
        <RouterContainer
          filter={filter}
          setFilter={setFilter}
          idProvider={idProvider}
        />
      </CardBody>
    </Card>
  )
}

const mapStateToProps = state => ({
  filter: RouteManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(RouteManagementActions.setFilter(filter)),
})

RouteBusList.propTypes = {
  idProvider: PropTypes.number,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
}

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(RouteBusList)
