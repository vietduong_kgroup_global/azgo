import React, { useEffect} from 'react'
import 'Stores/Intercity/SeatMapManagement/Reducers'
import 'Stores/Intercity/SeatMapManagement/Sagas'
import { SeatMapManagementActions } from 'Stores/Intercity/SeatMapManagement/Actions'
import { SeatMapManagementSelectors } from 'Stores/Intercity/SeatMapManagement/Selectors'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Card, CardBody, CardHeader } from 'reactstrap'
import SeatMapContainer from 'containers/Intercity/SeatMap'

const SeatMapBusList = ({ filter, setFilter, idProvider }) => {
  useEffect(() => {
    if (idProvider) {
      setFilter({
        provider_id: idProvider,
      })
    }
    return () => {
      setFilter({
        provider_id: null,
      })
    }
  }, [idProvider])
  return (
    <Card>
      <CardHeader>
        <strong>Danh sách mẫu ghế</strong>
      </CardHeader>
      <CardBody style={{ padding: '0' }}>
        <SeatMapContainer
          filter={filter}
          setFilter={setFilter}
          idProvider={idProvider}
        />
      </CardBody>
    </Card>
  )
}

const mapStateToProps = state => ({
  filter: SeatMapManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(SeatMapManagementActions.setFilter(filter)),
})

SeatMapBusList.propTypes = {
  idProvider: PropTypes.number,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
}

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(SeatMapBusList)
