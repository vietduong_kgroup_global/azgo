import React, { useEffect, useState } from 'react'
import 'Stores/Intercity/VehicleTypeManagement/Reducers'
import 'Stores/Intercity/VehicleTypeManagement/Sagas'
import { VehicleTypeManagementActions } from 'Stores/Intercity/VehicleTypeManagement/Actions'
import { VehicleTypeManagementSelectors } from 'Stores/Intercity/VehicleTypeManagement/Selectors'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Button, Card, CardBody, CardHeader } from 'reactstrap'
import VehicleTypeManagementContainer from 'containers/Intercity/VehicleTypeManagement'

const DriverBusList = ({ filter, setFilter, idProvider }) => {
  const [visible, setVisible] = useState(false)
  const showModal = () => {
    setVisible(true)
  }
  useEffect(() => {
    if (idProvider) {
      setFilter({
        provider_id: idProvider,
      })
    }
    return () => {
      setFilter({
        provider_id: null,
      })
    }
  }, [idProvider])
  return (
    <Card>
      <CardHeader>
        <strong>Danh sách loại xe</strong>
        <Button
          color="success"
          size="sm"
          onClick={showModal}
          style={{ float: 'right' }}>
          <i className="fa fa-plus-square" />
          &nbsp;Thêm mới
        </Button>
      </CardHeader>
      <CardBody style={{ padding: '0' }}>
        <VehicleTypeManagementContainer
          filter={filter}
          setFilter={setFilter}
          visible={visible}
          setVisible={setVisible}
          idProvider={idProvider}
        />
      </CardBody>
    </Card>
  )
}

const mapStateToProps = state => ({
  filter: VehicleTypeManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(VehicleTypeManagementActions.setFilter(filter)),
})

DriverBusList.propTypes = {
  idProvider: PropTypes.number,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
}

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(DriverBusList)
