import React, { useEffect } from 'react'
import 'Stores/Intercity/ScheduleManagement/Reducers'
import 'Stores/Intercity/ScheduleManagement/Sagas'
import { ScheduleManagementSelectors } from 'Stores/Intercity/ScheduleManagement/Selectors'
import { ScheduleManagementActions } from 'Stores/Intercity/ScheduleManagement/Actions'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import SchedulerContainer from 'containers/Intercity/Schedule'
import { Card, CardBody, CardHeader } from 'reactstrap'

const VehicleBusList = ({
  filterSchedule,
  setFilterSchedule,
  itemProvider,
}) => {
  useEffect(() => {
    if (itemProvider) {
      setFilterSchedule({
        provider_id: itemProvider,
      })
    }
    return () => {
      setFilterSchedule({
        provider_id: null,
      })
    }
  }, [itemProvider])
  return (
    <Card>
      <CardHeader>
        <strong>Danh sách chyến xe</strong>
      </CardHeader>
      <CardBody style={{ padding: '0' }}>
        <SchedulerContainer
          filter={filterSchedule}
          setFilter={setFilterSchedule}
          providerDetail={itemProvider}
        />
      </CardBody>
    </Card>
  )
}

const mapStateToProps = state => ({
  filterSchedule: ScheduleManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilterSchedule: filter =>
    dispatch(ScheduleManagementActions.setFilter(filter)),
})

VehicleBusList.propTypes = {
  itemProvider: PropTypes.number,
  filterSchedule: PropTypes.object,
  setFilterSchedule: PropTypes.func,
}

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(VehicleBusList)
