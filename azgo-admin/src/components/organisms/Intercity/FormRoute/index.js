import React, { useEffect, useState, useContext } from 'react'
import { message, Form, Input, Button, Select, InputNumber } from 'antd'
import { UserContext } from 'Contexts/UserData'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import 'Stores/ProviderManagement/Reducers'
import 'Stores/ProviderManagement/Sagas'
import 'Stores/Intercity/AreaManagement/Reducers'
import 'Stores/Intercity/AreaManagement/Sagas'
import { ProviderManagementActions } from 'Stores/ProviderManagement/Actions'
import { ProviderManagementSelectors } from 'Stores/ProviderManagement/Selectors'
import { AreaManagementActions } from 'Stores/Intercity/AreaManagement/Actions'
import { AreaManagementSelectors } from 'Stores/Intercity/AreaManagement/Selectors'
import { RouteManagementActions } from 'Stores/Intercity/RouteManagement/Actions'
import BusRouteMap from 'components/molecules/BusRouteMap'
import { UserSelectors } from 'Stores/User/Selectors'
import styles from './styles.module.scss'
import * as config from 'Utils/enum'

const FormRouteContainer = ({
  form,
  id,
  item,
  editItem,
  createItem,
  getItemsProvider,
  getItemsListProvider,
  fetchItemsArea,
  getItemsArea,
  itemUser,
}) => {
  const UserInfo = useContext(UserContext)
  const { getFieldDecorator, getFieldsValue } = form
  const [location, setLocation] = useState(null)
  const [hours, setHours] = useState(null)
  const [minute, setMinute] = useState(null)
  const [startPoint, setStartPoint] = useState(null)
  const [endPoint, setEndPoint] = useState(null)
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      if (location.stations.length === 0) {
        message.error('Vui lòng nhập điểm đón chính')
      } else {
        if (location.destinations.length === 0) {
          message.error('Vui lòng nhập điểm đến chính')
        } else {
          if (id) {
            editItem({
              ...getFieldsValue(),
              id: parseInt(id, 10),
              router: location,
              duration: `${form.getFieldsValue().hours}:${
                form.getFieldsValue().minute
              }`,
              providerId:
                itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
                  ? itemUser.getIn(['providersInfo', 'id'])
                  : form.getFieldsValue().providerId,
            })
          } else {
            createItem({
              ...getFieldsValue(),
              router: location, // get props location tu cpn bus route map
              duration: `${form.getFieldsValue().hours}:${
                form.getFieldsValue().minute
              }`,
              providerId:
                itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
                  ? itemUser.getIn(['providersInfo', 'id'])
                  : form.getFieldsValue().providerId,
            })
          }
        }
      }
    })

  }
  const formItemGroup = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 24 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 24 },
    },
  }
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  }
  const handleStartPoint = value => {
    if (getItemsArea.size > 0) {
      const resultFind = getItemsArea.toJS().find(el => el.id === value)
      if (resultFind) {
        setStartPoint(resultFind)
      }
    }
  }
  const handleEndPoint = value => {
    if (getItemsArea.size > 0) {
      const resultFind = getItemsArea.toJS().find(el => el.id === value)
      if (resultFind) {
        setEndPoint(resultFind)
      }
    }
  }
  const callbackHandlerFunction = locationChild => {
    setLocation(locationChild)
  }
  // edit
  useEffect(() => {
    if (item.get('router')) {
      setLocation(item.get('router').toJS())
    }
    if (item.get('duration')) {
      let string = item.get('duration').toString()
      let index = string.indexOf(':')
      let hours = string.slice(0, index)
      let minute = string.slice(index + 1)
      setHours(hours)
      setMinute(minute)
    }
    if (item.size > 0) {
      setStartPoint(item.get('startPoint').toJS())
      setEndPoint(item.get('endPoint').toJS())
    }
    return () => {
      setHours(null)
      setMinute(null)
      setStartPoint(null)
      setEndPoint(null)
    }
  }, [item])
  useEffect(() => {
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.ADMIN) {
      getItemsProvider()
    }
  }, [itemUser])
  useEffect(() => {
    fetchItemsArea()
  }, [])
  return (
    <>
      <Form
        id="frmRoute"
        {...formItemLayout}
        className="input-floating form-ant-custom">
        <Form.Item label="Mã tuyến">
          {getFieldDecorator('routeCode', {
            initialValue: id && item.get('routeCode'),
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                min: 1,
                message: 'Trường không được ít hơn 1 ký tự!',
              },
              {
                max: 30,
                message: 'Trường không được quá 30 ký tự!',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Tên tuyến">
          {getFieldDecorator('name', {
            initialValue: id && item.get('name'),
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                min: 1,
                message: 'Trường không được ít hơn 1 ký tự!',
              },
              {
                max: 100,
                message: 'Trường không được quá 100 ký tự!',
              },
            ],
          })(<Input />)}
        </Form.Item>
        {itemUser.get('type') !== config.MANAGEMENT_TYPE_USER.PROVIDER && (
          <Form.Item label="Nhà xe">
            {getFieldDecorator('providerId', {
              initialValue: id && item.getIn(['provider', 'id']),
              rules: [{ required: true, message: 'Trường bắt buộc!' }],
            })(
              <Select placeholder="Vui lòng chọn nhà xe">
                {getItemsListProvider.toJS().map(item => (
                  <Select.Option value={item.id} key={`provider_${item.id}`}>
                    {item.providerName}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        )}
        <Form.Item label="Điểm đi">
          {getFieldDecorator('startPoint', {
            initialValue: id && item.getIn(['startPoint', 'id']),
            rules: [{ required: true, message: 'Trường bắt buộc!' }],
          })(
            <Select
              placeholder="Vui lòng chọn điểm đi"
              onChange={item => handleStartPoint(item)}>
              {getItemsArea.map(item => (
                <Select.Option
                  value={item.get('id')}
                  key={`start_point_${item.get('id')}`}>
                  {item.get('name')}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Điểm đến">
          {getFieldDecorator('endPoint', {
            initialValue: id && item.getIn(['endPoint', 'id']),
            rules: [{ required: true, message: 'Trường bắt buộc!' }],
          })(
            <Select
              placeholder="Vui lòng chọn điểm đến"
              onChange={item => handleEndPoint(item)}>
              {getItemsArea.map(item => (
                <Select.Option
                  value={item.get('id')}
                  key={`end_point_${item.get('id')}`}>
                  {item.get('name')}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <div className={styles.formGroup}>
          <div
            className={`${styles.formGroupLabel}` + ` ant-form-item-required`}>
            Khoảng thời gian (24)
          </div>
          <div className={styles.formGroupBody}>
            <Form.Item className={styles.formGroupItem} {...formItemGroup}>
              {getFieldDecorator('hours', {
                initialValue: id && hours ? hours : null,
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
              })(<InputNumber min={0} max={23} />)}
            </Form.Item>
            <span className="ml-1 mr-3">giờ</span>
            <Form.Item className={styles.formGroupItem} {...formItemGroup}>
              {getFieldDecorator('minute', {
                initialValue: id && minute ? minute : null,
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
              })(<InputNumber min={0} max={60} />)}
            </Form.Item>
            <span className="ml-1">phút</span>
          </div>
        </div>
        <Form.Item label="Khoảng cách">
          {getFieldDecorator('distance', {
            initialValue: id && item.get('distance'),
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(<InputNumber type="number" min={1} />)}
        </Form.Item>
        <Form.Item label="Giá gốc">
          {getFieldDecorator('basePrice', {
            initialValue: id && item.get('basePrice'),
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(<InputNumber type="number" />)}
        </Form.Item>
        <Form.Item label="Giá/Km">
          {getFieldDecorator('pricePerKm', {
            initialValue: id && item.get('pricePerKm'),
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(<InputNumber type="number" />)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Xác nhận
          </Button>
        </Form.Item>
      </Form>
      <hr />
      <BusRouteMap
        item={item}
        handlePropLocation={callbackHandlerFunction}
        startPoint={startPoint}
        endPoint={endPoint}
      />
    </>
  )
}

const mapStateToProps = state => ({
  getItemsListProvider: ProviderManagementSelectors.getItemsList(state),
  getItemsArea: AreaManagementSelectors.getItems(state),
  itemUser: UserSelectors.getUserData(state),
})

const mapDispatchToProps = dispatch => ({
  getItemsProvider: values =>
    dispatch(ProviderManagementActions.getItemsListRequest(values)),
  fetchItemsArea: () => dispatch(AreaManagementActions.getItemsRequest()),
  editItem: values => dispatch(RouteManagementActions.editItemRequest(values)),
  createItem: values =>
    dispatch(RouteManagementActions.createItemRequest(values)),
})

FormRouteContainer.propTypes = {
  id: PropTypes.any,
  item: PropTypes.object,
  getItemsProvider: PropTypes.func,
  getItemsListProvider: PropTypes.object,
  form: PropTypes.any,
  editItem: PropTypes.func,
  createItem: PropTypes.func,
  fetchItemsArea: PropTypes.func,
  getItemsArea: PropTypes.object,
  itemUser: PropTypes.object,
}
const FormRoute = Form.create()(FormRouteContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormRoute)
