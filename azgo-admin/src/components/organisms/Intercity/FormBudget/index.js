import React from 'react'
import {Form, Input, Button, Col, Row, InputNumber} from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { RouteManagementActions } from 'Stores/Intercity/RouteManagement/Actions'

const FormBudgetContainer = ({ form, id, item, editItem }) => {
  const { getFieldDecorator, getFieldsValue } = form
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      if (id) {
        editItem({
          ...getFieldsValue(),
          id: parseInt(id, 10),
          routeCode: id && item.get('routeCode'),
          name: id && item.get('name'),
          distance: id && item.get('distance'),
          startPoint: id && item.get('startPoint'),
          endPoint: id && item.get('endPoint'),
          providerId: id && item.get('providerId'),
          router: id && item.get('router').toJS(),
          duration: id && item.get('duration'),
          budget: true,
        })
      }
    })
  }
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  }
  return (
    <>
      <div
        style={{ maxWidth: '600px', fontWeight: 'bold', marginBottom: '30px' }}>
        <Row>
          <Col xs={{ span: 24 }} sm={{ span: 8 }}>
            Mã tuyến:
          </Col>
          <Col xs={{ span: 24 }} sm={{ span: 8 }}>
            {id && item.get('routeCode')}
          </Col>
        </Row>
        <Row>
          <Col xs={{ span: 24 }} sm={{ span: 8 }}>
            Tuyến:
          </Col>
          <Col xs={{ span: 24 }} sm={{ span: 8 }}>
            {id && item.get('name')}
          </Col>
        </Row>
        <Row style={{ maxWidth: '600px' }}>
          <Col xs={{ span: 24 }} sm={{ span: 8 }}>
            Nhà xe:
          </Col>
          <Col xs={{ span: 24 }} sm={{ span: 8 }}>
            {id && item.getIn(['provider', 'providerName'])}
          </Col>
        </Row>
      </div>
      <Form
        id="frmRoute"
        {...formItemLayout}
        className="input-floating form-ant-custom">
        <Form.Item label="Giá gốc">
          {getFieldDecorator('basePrice', {
            initialValue: id && item.get('basePrice'),
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                pattern: new RegExp("^(:?(?=[1-9]{1})([0-9]{0,8}))$"),
                message: 'Trường bắt buộc từ 1 - 10,000,000!',
              },
            ],
          })(<InputNumber type="number" />)}
        </Form.Item>
        <Form.Item label="Giá/Km">
          {getFieldDecorator('pricePerKm', {
            initialValue: id && item.get('pricePerKm'),
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                pattern: new RegExp("^(:?(?=[1-9]{1})([0-9]{0,8}))$"),
                message: 'Trường bắt buộc từ 1 - 10,000,000!',
              },
            ],
          })(<InputNumber type="number" />)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Xác nhận
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  editItem: values => dispatch(RouteManagementActions.editItemRequest(values)),
})

FormBudgetContainer.propTypes = {
  id: PropTypes.any,
  item: PropTypes.object,
  form: PropTypes.any,
  editItem: PropTypes.func,
}
const FormBudget = Form.create()(FormBudgetContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormBudget)
