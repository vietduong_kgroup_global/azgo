import React, { useEffect, useState } from 'react'
import { Form, Input, Select, Table, Radio, Row, Col } from 'antd'
import { Button } from 'reactstrap'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import 'Stores/ClientManagement/Reducers'
import 'Stores/ClientManagement/Sagas'
import { ClientManagementSelectors } from 'Stores/ClientManagement/Selectors'
import { ClientManagementActions } from 'Stores/ClientManagement/Actions'
import 'Stores/Intercity/TicketManagement/Reducers'
import 'Stores/Intercity/TicketManagement/Sagas'
import { TicketManagementActions } from 'Stores/Intercity/TicketManagement/Actions'
import * as config from 'Utils/enum'
import FormSearch from 'components/molecules/FormSearch'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { formatPhone } from 'Utils/helper'

const FormTicketSpecify = ({
  form,
  itemList,
  createItem,
  getItemsListUser,
  setFilter,
  clearItems,
  itemsListUser,
  filter,
  totalCount,
  showLoadingList,
  arrSeat,
  visible,
}) => {
  const { getFieldDecorator, getFieldsValue, resetFields } = form
  const [infoSeat, setInfoSeat] = useState([])
  const [displaySubmit, setDisplaySubmit] = useState(false)
  const [selectionType, setSelectionType] = useState('radio')
  const [userIdTicket, setUserIdTicket] = useState({})
  const [floor, setFloor] = useState(null)
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      createItem({
        ...getFieldsValue(),
        scheduleId: itemList.get('id'),
        phone: userIdTicket.phone,
        fullName: userIdTicket.customerProfile.fullName,
        countryCode: userIdTicket.countryCode,
        price: itemList.getIn(['route', 'basePrice']),
        seatName: infoSeat,
        floor: floor,
        paymentStatus: 2,
      })
    })
  }
  const columns = [
    {
      title: 'Họ và tên',
      dataIndex: 'customerProfile.fullName',
      key: 'name',
      width: 100,
    },
    {
      title: 'Số điện thoại',
      render: value => {
        return formatPhone(value.countryCode, value.phone)
      },
    },
    {
      title: 'Địa chỉ',
      dataIndex: 'customerProfile.address',
      key: 'address',
      width: 200,
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItemsListUser(filter)
  }
  const handleSearch = value => {
    setFilter({
      keyword: value,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      setUserIdTicket(selectedRows[0])
    },
    getCheckboxProps: record => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name,
    }),
  }
  useEffect(() => {
    getItemsListUser(filter)
  }, [filter])

  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  useEffect(() => {
    if (itemList.getIn(['seatMap', 'f2']).size > 0) {
      setFloor(2)
    } else {
      setFloor(1)
    }
  }, [itemList])

  useEffect(() => {
    if (userIdTicket.phone) {
      setDisplaySubmit(true)
    } else {
      setDisplaySubmit(false)
    }
  }, [userIdTicket])
  useEffect(() => {
    if (visible) {
      let arr = []
      for (let i = 0; i < arrSeat.length; i++) {
        arr.push(arrSeat[i].name)
      }
      setInfoSeat(arr)
    }
    return () => {
      clearItems()
      setFilter({
        keyword: '',
        page: 1,
      })
      setPagination({
        ...pagination,
        current: 1,
      })
      resetFields()
      setUserIdTicket({})
      setInfoSeat([])
    }
  }, [visible])
  useEffect(() => {
    getItemsListUser(filter)
  }, [])
  return (
    <>
      <Row gutter={16}>
        <Col className="gutter-row" span={12}>
          {infoSeat ? <FormSearch handleSearch={handleSearch} /> : null}
          <Radio.Group
            onChange={({ target: { value } }) => {
              setSelectionType(value)
            }}
            value={selectionType}>
            {/* <Radio value="checkbox">Checkbox</Radio> */}
            {/* <Radio value="radio">radio</Radio> */}
          </Radio.Group>
          <Table
            dataSource={itemsListUser ? itemsListUser.toJS() : null}
            style={{ marginTop: '20px' }}
            columns={columns}
            pagination={pagination}
            rowKey="userId"
            loading={showLoadingList}
            onChange={handleTableChange}
            rowSelection={
              infoSeat
                ? {
                    type: selectionType,
                    ...rowSelection,
                  }
                : null
            }
            footer={() =>
              totalCount && totalCount !== 0
                ? 'Hiển thị ' + itemsListUser.toJS().length + '/' + totalCount
                : '0'
            }
          />
        </Col>
        <Col className="gutter-row" span={12}>
          <Form id="frmTicketAdmin" className="input-floating">
            <Form.Item label="Điểm đón">
              {getFieldDecorator('pickupPoint', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
              })(
                <Radio.Group>
                  {itemList.getIn(['route', 'router', 'stations']).map(item => (
                    <Radio
                      value={item.get('address')}
                      key={`address_${item.get('lat')}`}>
                      <Button size="sm" className="btn-html5 btn-brand mr-1">
                        Điểm đón chính
                      </Button>
                      {item.get('address')}
                    </Radio>
                  ))}
                 {/* {itemList
                    .getIn(['route', 'router', 'additionalStation'])
                    .map(item => (
                      <Radio
                        value={item.get('address')}
                        key={`address_${item.get('lat')}`}>
                        <Button size="sm" className="btn-vine btn-brand mr-1">
                          Điểm đón phụ
                        </Button>
                        {item.get('address')}
                      </Radio>
                    ))}*/}
                </Radio.Group>
              )}
            </Form.Item>
            <Form.Item label="Điểm đến">
              {getFieldDecorator('destination', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
              })(
                <Radio.Group>
                  {itemList
                    .getIn(['route', 'router', 'destinations'])
                    .map(item => (
                      <Radio
                        value={item.get('address')}
                        key={`address_${item.get('lat')}`}>
                        <Button size="sm" className="btn-github btn-brand mr-1">
                          Điểm đến phụ
                        </Button>
                        {item.get('address')}
                      </Radio>
                    ))}
                </Radio.Group>
              )}
            </Form.Item>
            <Form.Item label="Thông tin">
              {getFieldDecorator('luggageInfo', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
              })(<Input.TextArea />)}
            </Form.Item>
            <Form.Item>
              {displaySubmit && (
                <Button color="primary" onClick={handleSubmit}>
                  Xác nhận
                </Button>
              )}
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </>
  )
}

const mapStateToProps = state => ({
  itemsListUser: ClientManagementSelectors.getItems(state),
  filter: ClientManagementSelectors.getFilter(state),
  totalCount: ClientManagementSelectors.getTotalCount(state),
  showLoadingList: LoadingSelectors.getLoadingList(state),
})

const mapDispatchToProps = dispatch => ({
  createItem: values => {
    dispatch(TicketManagementActions.createItemRequest(values))
  },

  getItemsListUser: filter =>
    dispatch(ClientManagementActions.getItemsRequest(filter)),
  setFilter: filter => dispatch(ClientManagementActions.setFilter(filter)),
  clearItems: () => dispatch(ClientManagementActions.clearItems()),
})

FormTicketSpecify.propTypes = {
  form: PropTypes.any,
  itemList: PropTypes.object,
  createItem: PropTypes.func,
  setFilter: PropTypes.func,
  getItemsListUser: PropTypes.func,
  clearItems: PropTypes.func,
  itemsListUser: PropTypes.object,
  filter: PropTypes.object,
  totalCount: PropTypes.number,
  showLoadingList: PropTypes.bool,
  arrSeat: PropTypes.array,
  visible: PropTypes.bool,
}
const FormTicketAdminSpecify = Form.create()(FormTicketSpecify)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormTicketAdminSpecify)
