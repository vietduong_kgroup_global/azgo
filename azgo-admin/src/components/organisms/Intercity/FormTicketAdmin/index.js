import React, { useEffect, useState } from 'react'
import { Form, Input, Select, Radio } from 'antd'
import { Button } from 'reactstrap'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import './styles.scss'
import { TicketManagementActions } from 'Stores/Intercity/TicketManagement/Actions'
import 'Stores/Intercity/TicketManagement/Reducers'
import 'Stores/Intercity/TicketManagement/Sagas'

const FormTicket = ({ form, itemList, createItem, visible, arrSeat }) => {
  const { getFieldDecorator, getFieldsValue, resetFields } = form
  const [infoSeat, setInfoSeat] = useState([])
  const { Option } = Select
  const [floor, setFloor] = useState(null)
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      createItem({
        ...getFieldsValue(),
        scheduleId: itemList.get('id'),
        price: itemList.getIn(['route', 'basePrice']),
        seatName: infoSeat,
        floor: floor,
        paymentStatus: 2,
      })
    })
  }
  const prefixSelector = getFieldDecorator('countryCode', {
    initialValue: '84',
  })(
    <Select style={{ width: 70 }}>
      <Option value="84">+84</Option>
    </Select>
  )
  useEffect(() => {
    return () => {
      resetFields()
    }
  }, [infoSeat])
  useEffect(() => {
    if (itemList.getIn(['seatMap', 'f2']).size > 0) {
      setFloor(2)
    } else {
      setFloor(1)
    }
  }, [itemList])
  useEffect(() => {
    if (visible) {
      let arr = []
      for (let i = 0; i < arrSeat.length; i++) {
        arr.push(arrSeat[i].name)
      }
      setInfoSeat(arr)
    }
  }, [visible])
  return (
    <>
      <Form id="frmTicketAdmin" className="input-floating">
        <Form.Item label="Số điện thoại">
          {getFieldDecorator('phone', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(<Input addonBefore={prefixSelector} style={{ width: '100%' }} />)}
        </Form.Item>
        <Form.Item label="Họ và tên">
          {getFieldDecorator('fullName', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Điểm đón">
          {getFieldDecorator('pickupPoint', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(
            <Radio.Group>
              {itemList.getIn(['route', 'router', 'stations']).map(item => (
                <Radio
                  value={item.get('address')}
                  key={`address_${item.get('lat')}`}>
                  <Button size="sm" className="btn-html5 btn-brand mr-1">
                    Điểm đón chính
                  </Button>
                  {item.get('address')}
                </Radio>
              ))}
             {/* {itemList
                .getIn(['route', 'router', 'additionalStation'])
                .map(item => (
                  <Radio
                    value={item.get('address')}
                    key={`address_${item.get('lat')}`}>
                    <Button size="sm" className="btn-vine btn-brand mr-1">
                      Điểm đón phụ
                    </Button>
                    {item.get('address')}
                  </Radio>
                ))}*/}
            </Radio.Group>
          )}
        </Form.Item>
        <Form.Item label="Điểm đến">
          {getFieldDecorator('destination', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(
            <Radio.Group>
              {itemList.getIn(['route', 'router', 'destinations']).map(item => (
                <Radio
                  value={item.get('address')}
                  key={`address_${item.get('lat')}`}>
                  <Button size="sm" className="btn-github btn-brand mr-1">
                    Điểm đến phụ
                  </Button>
                  {item.get('address')}
                </Radio>
              ))}
            </Radio.Group>
          )}
        </Form.Item>
        <Form.Item label="Thông tin">
          {getFieldDecorator('luggageInfo', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(<Input.TextArea />)}
        </Form.Item>
        <Form.Item>
          <Button color="primary" onClick={handleSubmit}>
            Xác nhận
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(TicketManagementActions.createItemRequest(values)),
})

FormTicket.propTypes = {
  form: PropTypes.any,
  itemList: PropTypes.object,
  createItem: PropTypes.func,
  arrSeat: PropTypes.array,
  visible: PropTypes.bool,
}
const FormTicketAdmin = Form.create()(FormTicket)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormTicketAdmin)
