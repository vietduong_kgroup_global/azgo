import React, { useEffect, useState } from 'react'
import { Form, DatePicker, Select,Row, Col, Input, Modal, Slider, InputNumber } from 'antd'
import { Button } from 'reactstrap'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import moment from 'moment'
import {formatDate} from 'Utils/helper'
import { OrderTourLogSelectors } from 'Stores/TourCar/OrderTourLog/Selectors'
import { OrderTourLogActions } from 'Stores/TourCar/OrderTourLog/Actions'
const { Option } = Select;
const FormFilterContainer = ({ form, handleClickParent, filter, getItems, setFilter }) => {
  const { getFieldDecorator, getFieldsValue, resetFields } = form
  
  const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 20 },
  }; 
  // const arraySeat = [{name: "Xe 4 chỗ", value: 4},{name: "Xe 7 chỗ", value: 7}, {name: "Xe 16 chỗ", value: 16}]
  const [isModalVisible, setIsModalVisible] = useState(false);
 
  const [fromDate, onChangefromDate] = useState('')
  const [toDate, onChangetoDate] = useState('')

  useEffect(() => { 
    onChangefromDate(filter.get('fromDate')) 
    onChangetoDate(filter.get('toDate')) 
    
  }, [filter])
 
 
  const handleCancel = () => {
    setFilter({ 
      fromDate: null,
      toDate: null,
    })
    resetFields()
    getItems(filter)
    setIsModalVisible(false);
  };

  const handleOk = () => {   
    setFilter({ 
      fromDate: formatDate(moment(fromDate).format('DD/MM/YYYY')) + ' 00:00:00Z',
      toDate: formatDate(moment(toDate).format('DD/MM/YYYY'))+ ' 23:59:59Z',
    })
    getItems(filter)
    setIsModalVisible(false);
  };
 
  const showModal = () => {
    setIsModalVisible(true);
  };
  const onChangeFromDate = (date, dateString) => {
    if (date) {  
      let newDate = moment(date).startOf('day')._d
      onChangefromDate(newDate) 
    } 
  }
  const onChangeToDate = (date, dateString) => {
    if (date) {   
      let newDate = moment(date).endOf('day')._d
      onChangetoDate(newDate) 
    } 
  }
  const dateFormat = ['DD/MM/YYYY'] 
  return (
    <>
    {filter.get('numberOfSeat')!= null || filter.get('fromPrice') != null  || filter.get('toPrice') != null || filter.get('fromDate')!= null || filter.get('toDate')!= null ? 
      <Button className = "btn_filter_active" color="primary" onClick={showModal}>
        <i className="fa fa-plus-square" />
              &nbsp;Filter
        </Button>
        :
      <Button className = "btn_filter" color="secondary" onClick={showModal}>
        <i className="fa fa-plus-square" />
                &nbsp;Filter
        </Button>
      }
    <Modal title="Filter Tour Order Logs" visible={isModalVisible}  onOk={handleOk} onCancel={handleCancel} cancelText="Reset" okText="OK"
      // footer={[
      //   <Button key="back" onClick={handleOk}>
      //     Huỷ filter
      //   </Button>,
      //   <Button key="submit" type="primary" onClick={handleCancel}>
      //     Filter
      //   </Button>,
      // ]}
    > 
        <>
        <Form {...layout} className="form_filter_table_container" name="control-ref"  >
        <Form.Item label="Từ ngày">
          {getFieldDecorator('fromDate', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(<DatePicker format={dateFormat} placeholder="Chọn"   onChange={onChangeFromDate}/>)}
        </Form.Item>
        <Form.Item label="đến ngày">
          {getFieldDecorator('toDate', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(<DatePicker format={dateFormat} placeholder="Chọn"  onChange={onChangeToDate}  />)}
        </Form.Item> 
          </Form> 
        </> 
    </Modal>
    </>
  
  )
}

const mapStateToProps = state => ({
  filter: OrderTourLogSelectors.getFilter(state),

})

const mapDispatchToProps = dispatch => ({
  getItems: filter => dispatch(OrderTourLogActions.getItemsRequest(filter)),
  setFilter: filter => dispatch(OrderTourLogActions.setFilter(filter)),

})

FormFilterContainer.propTypes = {
  form: PropTypes.any,
  handleClickParent: PropTypes.func,
}
const FormFilter = Form.create()(FormFilterContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormFilter)
