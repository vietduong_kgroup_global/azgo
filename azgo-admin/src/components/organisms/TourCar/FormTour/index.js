import React, { useEffect, useState } from 'react'
import {
  Form,
  Input,
  Button,
  Select,
  Icon,
  Upload,
  InputNumber,
  DatePicker,
  Row,
  Col,
  Divider,
  Modal,
  message,
  Checkbox
} from 'antd'
import moment from 'moment'
import {  ButtonGroup, Button as ButtonReactStrap } from 'reactstrap';
// import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
// Vehicle Type
import 'Stores/Vehicle/VehicleTypeManagement/Reducers'
import 'Stores/Vehicle/VehicleTypeManagement/Sagas'
import { VehicleTypeManagementSelectors } from 'Stores/Vehicle/VehicleTypeManagement/Selectors'
import { VehicleTypeManagementActions } from 'Stores/Vehicle/VehicleTypeManagement/Actions'

// Additional Fee
import 'Stores/AdditionalFeeManagement/Reducers'
import 'Stores/AdditionalFeeManagement/Sagas'
import { AdditionalFeeManagementSelectors } from 'Stores/AdditionalFeeManagement/Selectors'
import { AdditionalFeeManagementActions } from 'Stores/AdditionalFeeManagement/Actions'
// Area
import 'Stores/Intercity/AreaManagement/Reducers'
import 'Stores/Intercity/AreaManagement/Sagas'
import { AreaManagementActions } from 'Stores/Intercity/AreaManagement/Actions'
import { AreaManagementSelectors } from 'Stores/Intercity/AreaManagement/Selectors'
// Tour
import 'Stores/TourCar/TourManagement/Reducers'
import 'Stores/TourCar/TourManagement/Sagas'
import { TourManagementActions } from 'Stores/TourCar/TourManagement/Actions'
import { TourManagementSelectors } from 'Stores/TourCar/TourManagement/Selectors'

import { Config } from 'Config'
import { getToken } from 'Utils/token'
import * as config from 'Utils/enum'
import FilterTagsCompany from 'components/organisms/TourCar/FilterTagsCompany'
import FormatPrice from 'components/organisms/FormatPrice'

const { TextArea } = Input;
const FormTour = ({
  form,
  id,
  item,
  createItem,
  editItem,
  deleteItem,
  itemsArea,
  getListItemsArea,
  filterArea,
  setFilterArea,
  itemsAdditionalFee,
  getListItemsAdditionalFee,
  filterAdditionalFee,
  setFilterAdditionalFee,
  itemsVehicleType,
  getListItemsVehicleType,
  filterVehicleType,
  setFilterVehicleType,
}) => {
  const {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue,
    resetFields,
    validateFields
  } = form
  const { RangePicker } = DatePicker;
  const dateFormat = ['DD/MM/YYYY']
  const [loading, setLoading] = useState('')
  const [image, setImage] = useState((id && item.get('image') && item.get('image').toJS()) || "")
  const [gallery, setGallery] = useState((id && item.get('gallery') && item.get('gallery').toJS()) || [])
  const [fieldsStation, setFieldsStation] = useState(id && item.get('stations') && item.get('stations').toJS() || [])
  const [fieldsAdditionalFee, setFieldsAdditionalFee] = useState(id && item.get('additionalFee') && item.get('additionalFee').toJS() || [])
  const [checked, setChecked] = useState(id && item.get('isRoundTrip') || false)
  const [checked_is_from_airport, setChecked_is_from_airport] = useState(id && item.get('isFromAirport') || false)
  const [checked_hide_mobile, setChecked_hide_mobile] = useState(id && item.get('hideMobile') || false)
   
  const [type, setType] = useState(id && item.get('type') || 'fix_price')
  const [previewVisible, setPreviewVisible] = useState(false)
  const [previewImage, setPreviewImage] = useState("")
  const [resetField, setResetField] = useState(true)
  const [listCompany, setListCompany] = useState([]);
  const cdleSubmitgeTagsFilterCompany = tags => {
    setListCompany(tags)
  }
  const changeTypeTour = (type) => {
    setType(type)
  }
  const handleSubmit = () => {
    validateFields(['name', 'time', 'distanceRate', 'timeRate', 'tourPrice', 'numberOfSeat', 'tourPrice', 'description', 'areaUuid', 'extraTimePrice'], { force: true }, (err, values) => {
      if (err) {
        return false
      }
      const field = { ...getFieldsValue() }
      field.hideMobile = !!field.hideMobile ? 1 : 0;
      if (id) {
        editItem({
          ...field,
          additionalFee: fieldsAdditionalFee,
          stations: fieldsStation,
          image: image,
          gallery: gallery,
          isRoundTrip: checked,
          company: listCompany,
          type: type,
          uuid: id,
        })
      } else {
        createItem({
          ...field,
          additionalFee: fieldsAdditionalFee,
          stations: fieldsStation,
          image: image,
          gallery: gallery,
          isRoundTrip: checked,
          type: type,
          company: listCompany,
        })
      }
    })
  }
  const handleDelete = () => {
    deleteItem(id)
  }
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 7 },
    },
  }
  const formAreaLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 10 },
    },
  }
  const formItemLayoutUpload = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 20 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 20,
        offset: 4,
      },
    },
  }
  const propsHeaderUpload = {
    name: 'images[]',
    action: `${Config.DEV_URL.UPLOAD_IMAGE}?type_image=tour`,
    headers: {
      Authorization: 'bearer ' + getToken(),
    },
  }
  const onChangeCheckox = (e) => {
    setChecked(e.target.checked)
  }
  const onChangeCheckox_is_from_airport = (e) => {
    setChecked_is_from_airport(e.target.checked)
  }
  const onChangeCheckox_hide_mobile = (e) => {
    setChecked_hide_mobile(e.target.checked)
  }
  const getBase64 = (img, callback) => {
    const reader = new FileReader()
    reader.addEventListener('load', () => callback(reader.result))
    reader.readAsDataURL(img)
  }
  const onFinish = values => {
    console.log('Received values of form:', values);
  };
  useEffect(() => {
    setImage(id && item.get('image') ? item.get('image').toJS() : "")
    setGallery(id && item.get('gallery') ? item.get('gallery').toJS() : [])
    setFieldsStation(id && item.get('stations') ? item.get('stations').toJS() : [])
    setFieldsAdditionalFee(id && item.get('additionalFee') && item.get('additionalFee').toJS() || [])
    setChecked(id && item.get('isRoundTrip') ? item.get('isRoundTrip') : false)
    setChecked_is_from_airport(id && item.get('isFromAirport') ? item.get('isFromAirport') : 0)
    setChecked_hide_mobile(id && item.get('hideMobile') ? item.get('hideMobile') : 0)
    setListCompany(id && item.get('company') ? item.get('company').toJS() : [])
    setType(id && item.get('type') ? item.get('type') : 'fix_price')
  }, [item])

  const handlePreviewImg = (url) => {
    setPreviewImage(url)
    setPreviewVisible(true)
  }
  const handleChange = (info, value) => {
    if (info.file.status === 'uploading') {
      switch (value) {
        case 'image': {
          setLoading('image')
          break
        }
        case 'gallery': {
          setLoading('gallery')
          break
        }
      }
    }

    if (info.file.status === 'done') {
      getBase64(info.file.originFileObj, imageUrl => {
        setLoading('')
        let imageProfile = info.file.response.data[0].address;
        switch (value) {
          case 'gallery': {
            gallery.push({ url: imageProfile, name: info.file.name })
            setFieldsValue({ gallery: gallery })
            setGallery(gallery)
            break
          }
          case 'image': {
            setFieldsValue({ image: { url: imageProfile, name: info.file.name } })
            setImage({ url: imageProfile, name: info.file.name })
            break
          }
          default: {
            break
          }
        }
        forceUpdate()
      })
    } else if (info.file.status === 'error') {
      setLoading(false)
      message.error('Upload failed')
    }
  }
  const [, updateState] = React.useState();
  const forceUpdate = React.useCallback(() => updateState({}), []);
  const addStation = () => {
    fieldsStation.push({
      "label": ["name", "lat", "lng", "time"],
    })
    forceUpdate()

  }
  const removeStation = (index) => {
    fieldsStation.splice(index, 1)
    forceUpdate()
  }
  const onChangeInputStation = (e, index) => {
    fieldsStation[index][e.target.name] = e.target.value
    setFieldsStation(fieldsStation)
    forceUpdate()
  }
  const addAdditionalFee = () => {
    fieldsAdditionalFee.push({
      additionalFee: null,
      start: null,
      end: null,
    })
    forceUpdate()

  }
  const removeAdditionalFee = (index) => {
    fieldsAdditionalFee.splice(index, 1)
    forceUpdate()
  }
  const onChangeAdditionalFee = ({...param}) => {
    if(param.name === 'start' || param.name === 'end'){
      fieldsAdditionalFee[param.index][param.name] = param.value._isAMomentObject ? param.value._d : null;
    }
    else if(param.name === 'additionalFee'){
      let fee = itemsAdditionalFee.toJS().find(item => item.uuid === param.value)
      fieldsAdditionalFee[param.index][param.name] = fee;
    }
    setFieldsAdditionalFee(fieldsAdditionalFee)
    forceUpdate()
  }
  const handleRemoveGallery = index => {
    gallery.splice(index, 1)
    setFieldsValue(gallery)
    forceUpdate()
  }
  const handleRemoveImage = () => {
    setImage("")
    forceUpdate()
  }
  useEffect(() => {
    getListItemsArea(filterArea)
  }, [filterArea])
  useEffect(() => {
    getListItemsAdditionalFee(filterAdditionalFee)
  }, [filterAdditionalFee])
  useEffect(() => {
    getListItemsVehicleType(filterVehicleType)
  }, [filterVehicleType])
  useEffect(() => {
    setFilterArea({
      per_page: 100,
      page: 1,
      keyword: '',
    })
    setFilterAdditionalFee({
      per_page: 100,
      page: 1,
      keyword: '',
    })
    setFilterVehicleType({
      per_page: 100,
      page: 1,
      keyword: '',
    })
  }, [])
  const changeTagsFilterCompany = tags => {
    setListCompany(tags)
  }
  return (
    <>
      <Form
        id="frmVehicle"
        {...formItemLayout}
        className="input-floating form-ant-custom-upload">
        <Form.Item label="Tên tour">
          {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('name'),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Số chỗ">
          {getFieldDecorator('numberOfSeat', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('numberOfSeat'),
          })(
            <InputNumber
              type="number"
              className="input-number-transition"
            />
          )}
        </Form.Item>
        <Divider orientation="left"><b>Thông tin tour</b></Divider>
        <Form.Item label="Kiểu tour">
          {getFieldDecorator('name', { 
            initialValue: id && item.get('name'),
          })(
            <ButtonGroup>
              <Button onClick = {() => changeTypeTour('fix_price')} className={type === 'fix_price' ? 'active_type_tour' : 'non_active_type_tour'}>Giá cố định</Button>
              <Button onClick = {() => changeTypeTour('distance_price')}  className={type === 'distance_price' ? 'active_type_tour' : 'non_active_type_tour'}>Giá theo khoảng cách</Button>
              <Button onClick = {() => changeTypeTour('time_price')} className={type === 'time_price' ? 'active_type_tour' : 'non_active_type_tour'}>Thuê xe</Button>
            </ButtonGroup>
          )}
        </Form.Item>  
        
        {type === 'distance_price' &&
        <Form.Item label="Giá tiền/km">
          {getFieldDecorator('distanceRate', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('distanceRate'),
          })(
            <InputNumber
              formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              parser={value => value.replace(/\$\s?|(,*)/g, '')}
              className="input-number-transition"
            />
          )}
        </Form.Item>
        } 
        {type === 'time_price'   &&
          <React.Fragment>
            <Form.Item label="Giá tiền/ngày">
              {getFieldDecorator('timeRate', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue: id && item.get('timeRate'),
              })(
                <InputNumber
                  formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={value => value.replace(/\$\s?|(,*)/g, '')}
                  className="input-number-transition"
                />
              )}
            </Form.Item>
            <Form.Item label="Loại xe">
              {getFieldDecorator('vehicleTypeId', {
                initialValue: id && item.get('vehicleTypeId'),
              })(
                <Select placeholder="Vui lòng chọn loại xe">
                  {itemsVehicleType.toJS().map(item => (
                    <Select.Option
                      value={item.id}
                      key={`vehicle_type_${item.id}`}>
                      {item.name}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
          </React.Fragment>
        }
        {type === 'fix_price' &&
        <Form.Item label="Giá tour">
          {getFieldDecorator('tourPrice', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('tourPrice'),
          })(
            <InputNumber
              formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              parser={value => value.replace(/\$\s?|(,*)/g, '')}
              className="input-number-transition"
            />
          )}
        </Form.Item> 
        }
        {type === 'distance_price' || type === 'fix_price' &&
        <Form.Item label="Thời gian (phút)">
          {getFieldDecorator('time', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('time'),
          })(
            <InputNumber
              type="number"
              className="input-number-transition"
            />
          )}
        </Form.Item>
        }  
        {type !== 'time_price'  && type !== 'distance_price' &&
          <Form.Item label="Giá thêm giờ (Số tiền/h)">
            {getFieldDecorator('extraTimePrice', {
              initialValue: id && item.get('extraTimePrice'),
            })(
              <InputNumber
                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                parser={value => value.replace(/\$\s?|(,*)/g, '')}
                className="input-number-transition"
              />
            )}
          </Form.Item>
        }
        <Form.Item label="Mô tả">
          {getFieldDecorator('description', {
            initialValue: id && item.get('description'),
          })(<TextArea rows={4} />)}
        </Form.Item>
        <Form.Item label="Khu vực">
          {getFieldDecorator('areaUuid', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('areaUuid'),
          })(
            <Select placeholder="Vui lòng chọn khu vực">
              {itemsArea.toJS().map(item => (
                <Select.Option
                  value={item.uuid}
                  key={`vehicle_brand_${item.uuid}`}>
                  {item.name}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Company">
          {getFieldDecorator('company', {
            rules: [
            ],
            initialValue: id && item.get('company'),
          })(
            <FilterTagsCompany tags={listCompany} inputTitle={"Enter to add"} buttonTitle={"Thêm company"} changeTags={changeTagsFilterCompany} />
          )}
        </Form.Item>
        {type !== 'time_price'  && type !== 'distance_price' && 
        <Form.Item label="Địa điểm" {...formAreaLayout} >
          <Form name="dynamic_form_nest_item" onFinish={onFinish} autoComplete="off">
            {fieldsStation.map((field, index) => (
              <div style={{ display: 'flex', marginBottom: 8, flexDirection: 'column', padding: '10px', backgroundColor: 'aliceblue' }} align="baseline">
                <div>{'Trạm thứ ' + (index + 1)}</div>
                <Form.Item
                  {...field}
                  label={'Tên tour'}
                  name={'name'}
                  fieldKey={[field.fieldKey, 'name']}
                  rules={[{ required: true, message: 'Missing name' }]}
                >
                  <Input placeholder="Name" value={field.name} name={'name'} onChange={(e) => onChangeInputStation(e, index)} />
                </Form.Item>
                <Form.Item
                  {...field}
                  label={'Kinh độ'}
                  name={'lat'}
                  fieldKey={[field.fieldKey, 'lat']}
                  rules={[{ required: true, message: 'Missing lat' }]}
                >
                  <Input placeholder="Lat" value={field.lat} name={'lat'} onChange={(e) => onChangeInputStation(e, index)}/>
                </Form.Item>
                <Form.Item
                  {...field}
                  label={'Vĩ độ'}
                  name={'Lng'}
                  fieldKey={[field.fieldKey, 'Lng']}
                  rules={[{ required: true, message: 'Missing Lng' }]}
                >
                  <Input placeholder="Lng" value={field.lng} name={'lng'} onChange={(e) => onChangeInputStation(e, index)}/>
                </Form.Item>
                <Form.Item
                  {...field}
                  label={'Thời gian'}
                  name={'time'}
                  index={index}
                  fieldKey={[field.fieldKey, 'time']}
                  rules={[{ required: true, message: 'Missing time' }]}
                >
                  <Input placeholder="time" name={'time'} value={field.time} onChange={(e) => onChangeInputStation(e, index)}/>
                </Form.Item>
                <Icon type="minus-circle" onClick={() => removeStation(index)} />
              </div>
            ))}

            <Form.Item>
              <Button type="dashed" onClick={() => addStation()} block icon={
                <Icon type="plus" />
              }>
                Thêm trạm mới
                    </Button>
            </Form.Item>
          </Form>
        </Form.Item>
        }
        {type !== 'time_price' && type !== 'distance_price' && 
        <Form.Item
          name="agreement"
          valuePropName="checked"
          rules={[
          ]}
          {...tailFormItemLayout}
        >
          {getFieldDecorator('isRoundTrip', {
            initialValue: id && item.get('isRoundTrip'),
          })(
            <Checkbox checked={checked} onChange={onChangeCheckox}>
              Khứ hồi
              </Checkbox>
          )}

        </Form.Item>
        }
        {type !== 'distance_price' && type !== 'time_price'  &&
          <Form.Item
            name="agreement"
            valuePropName="checked"
            rules={[
            ]}
            {...tailFormItemLayout}
          >
            {getFieldDecorator('is_from_airport', {
              initialValue: id && item.get('is_from_airport'),
            })(
              <Checkbox checked={checked_is_from_airport} onChange={onChangeCheckox_is_from_airport}>
                Chuyến từ sân bay
                </Checkbox>
            )}

          </Form.Item>
        }
        <Form.Item
          name="agreement"
          valuePropName="checked"
          rules={[
            // {
            //   validator: (_, value) =>
            //     value ? Promise.resolve() : Promise.reject('Should accept agreement'),
            // },
          ]}
          {...tailFormItemLayout}
        >
          {getFieldDecorator('hideMobile', {
            initialValue: id && item.get('hideMobile'),
          })(
            <Checkbox checked={checked_hide_mobile} onChange={onChangeCheckox_hide_mobile}>
              Ẩn trên mobile
              </Checkbox>
          )}

        </Form.Item>
        
        <Divider orientation="left"><b>Phí phụ thu</b></Divider>
        <Form.Item label="Phí phụ thu" {...formAreaLayout} >
          <Form name="dynamic_form_nest_item" onFinish={onFinish} autoComplete="off">
            {fieldsAdditionalFee.map((field, index) => (
              <div style={{ display: 'flex', marginBottom: 8, flexDirection: 'column', padding: '10px', backgroundColor: 'aliceblue' }} align="baseline">
                <Form.Item
                  {...field}
                  label={'Tên phí'}
                  name='fee'
                  fieldKey={[field.fieldKey, 'name']}
                  rules={[{ required: true, message: 'Missing name' }]}
                >
                  <Select
                    name='fee'
                    placeholder="Vui lòng chọn phí phụ thu"
                    value={field.additionalFee? field.additionalFee.uuid: null}
                    onChange={(e) => onChangeAdditionalFee({value: e, name: 'additionalFee', index})}
                  >
                    <Select.Option
                      value={null}
                      key={`addiitional_fee_null`}
                    >
                      -- Chọn --
                    </Select.Option>
                    {itemsAdditionalFee.toJS().map(item => (
                      <Select.Option
                        value={item.uuid}
                        key={`addiitional_fee_${item.uuid}`}
                      >
                        {item.name} - 
                        {item.type === config.MANAGEMENT_TYPE_ADDITIONAL_FEE.FLAT ?
                          <FormatPrice 
                            value={item.flat}
                            renderText={(value) => 
                              <span> {value} VNĐ</span>
                            }
                          />
                        : (item.percent ? ` ${item.percent*100}%` : 0)
                        }
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
                <Form.Item
                  {...field}
                  label={'Bắt đầu áp dụng phí'}
                  name='start'
                  fieldKey={[field.fieldKey, 'start']}
                  rules={[{ required: true, message: 'Missing start' }]}
                >
                  <DatePicker value={field.start? moment(field.start) : null} name='start' format={dateFormat} placeholder="Chọn" onChange={(e) => onChangeAdditionalFee({value: e, name: 'start', index})}/>
                </Form.Item>
                <Form.Item
                  {...field}
                  label={'Kết thúc áp dụng phí'}
                  name='end'
                  fieldKey={[field.fieldKey, 'end']}
                  rules={[{ required: true, message: 'Missing end' }]}
                >
                  <DatePicker value={field.end? moment(field.end) : null} name='end' format={dateFormat} placeholder="Chọn" onChange={(e) => onChangeAdditionalFee({value: e, name: 'end', index})}/>
                </Form.Item>
                <Icon type="minus-circle" onClick={() => removeAdditionalFee(index)} />
              </div>
            ))}
            <Form.Item>
              <Button 
                type="dashed"
                onClick={() => addAdditionalFee()}
                icon={<Icon type="plus"/>}
                block
              >
                Thêm phí phụ thu mới
              </Button>
            </Form.Item>
          </Form>
        </Form.Item>

        <Divider orientation="left"><b>Hình ảnh</b></Divider>
        <Form.Item
          label={type !== 'time_price' ? "Ảnh bìa tour" : "Ảnh xe"}
          className="ant-form-item-load"
          {...formItemLayoutUpload}>
          <div className="row-upload-img">
            {image && image.url &&
              <div className="col-upload-img col-upload-img-arr">
                <div className="frame-img" >
                  <Icon
                    type="delete"
                    onClick={() => handleRemoveImage()}
                  />
                  <img
                    src={image.url}
                    onClick={() => handlePreviewImg(image.url)}
                    alt="driverImg"
                  />
                </div>
              </div>
            }
            <div className="col-upload-img-button">
              <Upload
                accept=".png, .jpg, .jpeg"
                name="avatar"
                className="avatar-uploader"
                {...propsHeaderUpload}
                showUploadList={false}
                onChange={e => handleChange(e, 'image')}>
                <Button>
                  <Icon type="upload" /> Tải hình
                    {loading === 'image' && (
                    <Icon className="loading-image" type="loading" />
                  )}
                </Button>
              </Upload>
            </div>
          </div>
        </Form.Item>
        <Form.Item
          label="Thư viện ảnh"
          className="ant-form-item-load"
          {...formItemLayoutUpload}>
          <div className="row-upload-img">
            <div className="col-upload-img col-upload-img-arr">
              {(gallery || []).map((galleryItem, idx) => {
                if (galleryItem && galleryItem.url) return (
                  <div className="frame-img" key={idx}>
                    <Icon
                      type="delete"
                      onClick={() => handleRemoveGallery(idx)}
                    />
                    <img
                      src={galleryItem.url}
                      onClick={() => handlePreviewImg(galleryItem.url)}
                      alt="gallery"
                    />
                  </div>
                )
              })}
            </div>
            <div className="col-upload-img-button">
              <Upload
                accept=".png, .jpg, .jpeg"
                name="avatar"
                className="avatar-uploader"
                {...propsHeaderUpload}
                showUploadList={false}
                onChange={e => handleChange(e, 'gallery')}>
                <Button>
                  <Icon type="upload" /> Tải hình
                    {loading === 'gallery' && (
                    <Icon className="loading-image" type="loading" />
                  )}
                </Button>
              </Upload>
            </div>
          </div>
        </Form.Item>
        <Modal
          visible={previewVisible}
          footer={null}
          onCancel={() => setPreviewVisible(false)}>
          <div className="modal-image">
            <img alt="image" src={previewImage} />
          </div>
        </Modal>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit} className="confirm_update_button">
            {id ? 'Xác nhận' : 'Tạo mới'}
          </Button>
          {id && <Button type="danger"  onClick={handleDelete}>
            Xoá tour
          </Button>
          }
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({
  itemsAdditionalFee: AdditionalFeeManagementSelectors.getItems(state),
  filterAdditionalFee: AdditionalFeeManagementSelectors.getFilter(state),
  itemsVehicleType: VehicleTypeManagementSelectors.getItems(state),
  filterVehicleType: VehicleTypeManagementSelectors.getFilter(state),
  itemsArea: AreaManagementSelectors.getItems(state),
  filterArea: AreaManagementSelectors.getFilter(state),
  item: TourManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(TourManagementActions.createItemRequest(values)),
  editItem: values =>
    dispatch(TourManagementActions.editItemRequest(values)),
  deleteItem: values =>
    dispatch(TourManagementActions.deleteItemRequest(values)),
  getListItemsArea: values =>
    dispatch(AreaManagementActions.getItemsRequest(values)),
  setFilterArea: filter => dispatch(AreaManagementActions.setFilter(filter)),
  getListItemsAdditionalFee: values =>
    dispatch(AdditionalFeeManagementActions.getItemsRequest(values)),
  setFilterAdditionalFee: filter => dispatch(AdditionalFeeManagementActions.setFilter(filter)),
  getListItemsVehicleType: values =>
    dispatch(VehicleTypeManagementActions.getItemsRequest(values)),
  setFilterVehicleType: filter => dispatch(VehicleTypeManagementActions.setFilter(filter)),
})

FormTour.propTypes = {
  form: PropTypes.any,
  id: PropTypes.string,
  item: PropTypes.object,
  createItem: PropTypes.func,
  editItem: PropTypes.func,
  deleteItem: PropTypes.func,
  itemsVehicleType: PropTypes.object,
  itemsBrandVehicle: PropTypes.object,
  itemsDriver: PropTypes.object,
}
const FormVehicle = Form.create()(FormTour)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormVehicle)
