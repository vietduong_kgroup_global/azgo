import { Tag, Input } from 'antd';
import { TweenOneGroup } from 'rc-tween-one';
import { PlusOutlined } from '@ant-design/icons';
import React, { useEffect, useState } from 'react'

class FilterTagsCompany extends React.Component {
  state = {
    tags: [],
    inputVisible: false,
    inputValue: '',
  };
  componentDidMount() {
    this.setState({ tags: this.props.tags || [] });

  }
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.tags && nextProps.tags !== prevState.tags) {
      return { tags: nextProps.tags };
    }
    return null;
  }
  handleClose = removedTag => {
    const tags = this.state.tags.filter(tag => tag !== removedTag);
    this.setState({ tags });
    this.props.changeTags(tags)
  };

  showInput = () => {
    this.setState({ inputVisible: true }, () => this.input.focus());
  };

  handleInputChange = e => {
    this.setState({ inputValue: e.target.value });
  };

  handleInputConfirm = () => {
    let { inputValue = "" } = this.state;
    inputValue = inputValue.toLocaleLowerCase()
    let { tags } = this.state;
    if (inputValue && tags.indexOf(inputValue) === -1) {
      tags = [...tags, inputValue];
    }
    this.setState({
      tags,
      inputVisible: false,
      inputValue: '',
    });
    this.props.changeTags(tags)
  };

  saveInputRef = input => {
    this.input = input;
  };

  forMap = tag => {
    const tagElem = (
      <Tag
        closable
        onClose={e => {
          e.preventDefault();
          this.handleClose(tag);
        }}
      >
        {tag}
      </Tag>
    );
    return (
      <span key={tag} style={{ display: 'inline-block' }}>
        {tagElem}
      </span>
    );
  };

  render() {
    const { buttonTitle, inputTitle, styleContainer = {} } = this.props
    const { tags, inputVisible, inputValue } = this.state;
    const tagChild = tags.map(this.forMap);
    return (
      <>
        <div style={styleContainer}>
          <TweenOneGroup
            enter={{
              scale: 0.8,
              opacity: 0,
              type: 'from',
              duration: 100,
              onComplete: e => {
                e.target.style = '';
              },
            }}
            leave={{ opacity: 0, width: 0, scale: 0, duration: 200 }}
            appear={false}
          >
            {tagChild}
          </TweenOneGroup>
        </div>
        {inputVisible && (
          <Input
            ref={this.saveInputRef}
            type="text"
            size="big"
            style={{ marginLeft: 0 }}
            value={inputValue}
            onChange={this.handleInputChange}
            onBlur={this.handleInputConfirm}
            onPressEnter={this.handleInputConfirm}
            placeholder={inputTitle ? inputTitle : "Enter to Filter"}
          />
        )}
        {!inputVisible && (
          <Tag onClick={this.showInput} className="site-tag-plus" style={{ background: '#fff', borderStyle: 'dashed', marginLeft: 0, cursor: 'pointer', height: '32px', maxWidth: 'fit-content' }} >
            <PlusOutlined /> {buttonTitle ? buttonTitle : 'Filter Company'}
          </Tag>
        )}
      </>
    );
  }
}
export default FilterTagsCompany
// ReactDOM.render(<FilterTagsCompany />, mountNode);