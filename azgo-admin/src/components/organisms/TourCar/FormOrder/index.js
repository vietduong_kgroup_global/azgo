import React, { useEffect, useState } from 'react'
import {
  Form,
  Input,
  Button,
  Select,
  Icon,
  Upload,
  Row,
  Checkbox,
  Divider,
  message,
  Col,
  Modal,
  AutoComplete,
  InputNumber,
  DatePicker,
} from 'antd' 
import { Link, withRouter } from 'react-router-dom'
import FilterTagsCompany from 'components/organisms/TourCar/FilterTagsCompany'
import './styles.scss';
// import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons'; 
import moment from 'moment'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import FormatPrice from 'components/organisms/FormatPrice'

import 'Stores/Intercity/AreaManagement/Reducers'
import 'Stores/Intercity/AreaManagement/Sagas'
import { AreaManagementActions } from 'Stores/Intercity/AreaManagement/Actions'
import { AreaManagementSelectors } from 'Stores/Intercity/AreaManagement/Selectors'

import 'Stores/DriverManagement/Reducers'
import 'Stores/DriverManagement/Sagas'
import { DriverManagementActions } from 'Stores/DriverManagement/Actions'
import { DriverManagementSelectors } from 'Stores/DriverManagement/Selectors'

import 'Stores/ClientManagement/Reducers'
import 'Stores/ClientManagement/Sagas'
import { ClientManagementSelectors } from 'Stores/ClientManagement/Selectors'
import { ClientManagementActions } from 'Stores/ClientManagement/Actions'

import 'Stores/TourCar/TourManagement/Reducers'
import 'Stores/TourCar/TourManagement/Sagas'
import { TourManagementActions } from 'Stores/TourCar/TourManagement/Actions'
import { TourManagementSelectors } from 'Stores/TourCar/TourManagement/Selectors'

import 'Stores/TourCar/TripManagement/Reducers'
import 'Stores/TourCar/TripManagement/Sagas'
import { TripManagementActions } from 'Stores/TourCar/TripManagement/Actions'
import { TripManagementSelectors } from 'Stores/TourCar/TripManagement/Selectors'

import { _parsePhoneNumber } from 'Utils/helper'
import DriverList from 'components/organisms/TourCar/FormOrder/driverList'
import * as config from 'Utils/enum'
import * as errorCode from 'Utils/errorCode'
import { UserSelectors } from 'Stores/User/Selectors'

const { TextArea } = Input;
const FormTour = ({
  form,
  id,
  item,
  createItem,
  editItem,
  deleteItem,
  listCustomer,
  filterCustomer,
  setFilterCustomer,
  filterTour,
  setFilterTour,
  listTour,
  getListDriver,
  getListCustomer,
  getListTour,
  userInfo,
  history,
  itemsArea,
  getListItemsArea,
  filterArea,
  checkFeeWalletOrder,
  statusFeeWallet
}) => {
  const {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue,
    resetFields,
    validateFields
  } = form;
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 10 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 20,
        offset: 4,
      },
    },
  }
  let company = userInfo && userInfo.get('adminProfile') && userInfo.get('adminProfile').toJS().company || ''
  const dateFormat = ['DD-MM-YYYY HH:mm'];
  const { Option } = Select;

  const [checkedAutoSendSms, setCheckedAutoSendSms] = useState(id && item.get('autoSendSms') || false)
  const [checkedAutoSendNotification, setCheckedAutoSendNotification] = useState(id && item.get('autoSendNotification') || false)
  const [checkedIsPaid, setCheckedIsPaid] = useState(id && item.get('isPaid') || false)
  const [isFromAirport, setIsFromAirport] = useState(id && item.get('isRoundTrip') || false)
  const [distanceRate, setDistanceRate] = useState(0)
  const [tourInfo, setTourInfo] = useState({})
  const [driverSelect, setDriverSelect] = useState({})
  const [customerId, setCustomerId] = useState("")
  const [optionsCustomer, setOptionsCustomer] = useState([]);
  const [optionsTour, setOptionsTour] = useState([]);
  const [visibleDriverList, setVisibleDriverList] = useState(false);
  const [area, onAreaChange] = useState([])
  const [, updateState] = React.useState();
  const forceUpdate = React.useCallback(() => updateState({}), []);
  const handleOpenDriverList = () => {
    setVisibleDriverList(true)
  }
  
  const onChangeCheckedAutoSendNotification = (e) => {
    setCheckedAutoSendNotification(e.target.checked)
  }
  
  const onChangeCheckedAutoSendSms = (e) => {
    setCheckedAutoSendSms(e.target.checked)
  }
  
  const onChangeCheckedIsPaid = (e) => {
    setCheckedIsPaid(e.target.checked)
  }

  const onChangeAreaChange = (area) => {
    onAreaChange(area)
    setFilterTour({ area: JSON.stringify(area) })
  }
  
  const onChangeIsFromAirport = (e) => {
    setIsFromAirport(e.target.checked ? 1 : 0)
    setFilterTour({ isFromAirport: e.target.checked ? 1 : undefined })
  }
  
  const validatePhone = (rule, value, callback) => {
    let phone = form.getFieldValue('customerMobile')
    let countryCode = form.getFieldValue('countryCode')
    if (phone && countryCode) {
      let result = _parsePhoneNumber(countryCode, phone)
      if (result && result.isValid) callback()
      else callback('Số điện thoại không hợp lệ')
    } else {
      callback()
    }
  } 
   
  const handleSubmit = () => { 
    validateFields(['driverId', 'customerId', 'customerMobile', 'customerFirstName', 'customerLastName', 'tourId', 'startTime', 'status', 'longPickLocation', 'latPickLocation'], { force: true }, (err, values) => {
      const { startTime, customerMobile } = getFieldsValue()
      if (err) {
        return false
      }
      let field = { ...getFieldsValue() }
      field.driverId = driverSelect ? driverSelect.id : null
      let tourDetail = listTour.toJS().find((e) => e.id === field.tourId)
      if (tourDetail && tourDetail.tourPrice && field.extraTime && tourDetail.extraTimePrice) {
        field.extraMoney = field.extraTime * tourDetail.extraTimePrice
      }
      delete field.countryCode
      delete field.tourName
      let findCus = listCustomer.toJS().find(item => item.phone === customerMobile);

      field.customerId = findCus ? customerId : null
      field.driverId = field.driverId || null
      field.autoSendNotification = !!checkedAutoSendNotification ? 1 : 0;
      field.autoSendSms = !!checkedAutoSendSms ? 1 : 0;
      field.isPaid = !!checkedIsPaid ? 1 : 0;
      const startTimeObj = startTime._is_a_moment_object || startTime._isAMomentObject ? startTime._d : moment()._d
      const startTimeMilisecond = moment(startTimeObj).startOf('day').valueOf()
      if (id) {
        editItem({
          ...field,
          startTime: startTimeObj,
          startTimeMilisecond: startTimeMilisecond,
          // startTime: startTime._is_a_moment_object || startTime._isAMomentObject ? startTime._d : moment(),
          uuid: id,
        })
      } else {
        createItem({
          ...field,
          startTime: startTimeObj,
          startTimeMilisecond: startTimeMilisecond,
        })
      }
    })
  }
  const handleDelete = () => {
    deleteItem(id)
  }

  const changeTagsFilterCompany = tags => {
    setFilterTour({ company: JSON.stringify(tags) })
  }

  const calcPricePerDistance = () => {
    let distance = getFieldsValue().distance || 0
    let rate = distanceRate || 0;
    setFieldsValue({ orderPrice: distance * rate })
  }

  const selectDriver = (driver) => { 
    setVisibleDriverList(false)
    setDriverSelect(driver)
    setFieldsValue({ driverId: driver.driverProfile.fullName })
    if(driver && driver.id) checkFeeWalletOrderFunc(driver.id) 
  }

  const onChangeDriver = (e) => {
    setDriverSelect({})
    setFieldsValue({ driverId: null })
  }

  const checkFeeWalletOrderFunc = (driverId) => { 
    let field = { ...getFieldsValue() } 
    field.driverId = driverId
    checkFeeWalletOrder({...field})
  }
  const handleInfoTour = () => {
    if(tourInfo && tourInfo.uuid) { 
      // history.push('/admin/tour/tourManagement/' + tourInfo.uuid)
      const win = window.open('/admin/tour/tourManagement/' + tourInfo.uuid, "_blank");
      win.focus();
    } 
  }

  const onSearchTour = (searchText) => {
    setFilterTour({ keyword: searchText })
  };

  const onSelectTour = (data) => { 
    let tourInfo = listTour.toJS().find(item => item.id === Number(data));
    if (!!tourInfo) {
      setFieldsValue({
        orderPrice: tourInfo.tourPrice,
        tourId: String(tourInfo.id)
      });
      setDistanceRate(tourInfo.distanceRate)
      setTourInfo(tourInfo)
    }
  };

  const onSearchCustomer = (searchText) => {
    setFilterCustomer({ keyword: searchText })
  };

  const onSelectCustomer = (data) => {
    let customer = listCustomer.toJS().find(item => item.phone === data);
    if (!!customer) {
      setFieldsValue({
        customerMobile: customer.phone,
        customerFirstName: customer.customerProfile ? customer.customerProfile.firstName : "",
        customerLastName: customer.customerProfile ? customer.customerProfile.lastName : "",
        customerGender: customer.customerProfile ? customer.customerProfile.gender : 0,
      });
      setCustomerId(customer.customerProfile ? customer.customerProfile.userId : null)
    }
  };

  useEffect(() => {
    setDistanceRate(id && item.get('tourProfile') ? item.getIn(['tourProfile', 'distanceRate']) : 0)
    setCheckedAutoSendSms(id && item.get('autoSendSms') ? item.get('autoSendSms') : false)
    setCheckedAutoSendNotification(id && item.get('autoSendNotification') ? item.get('autoSendNotification') : false)
    setCheckedIsPaid(id && item.get('isPaid') ? item.get('isPaid') : false)
    setTourInfo(id && item.get('tourProfile') && item.get('tourProfile').toJS())
    setCustomerId(id && item.get('customerId') ? item.get('customerId') : null)
    setDriverSelect(id && item.get('driverTour') && item.get('driverTour').toJS())
    if (id) {
      setFieldsValue({
        customerMobile: item.get('customerMobile') || '',
        customerFirstName: item.get('customerFirstName') || '',
        customerLastName: item.get('customerLastName') || '',
        customerGender: item.get('customerGender') || 0, 
        tourId:  String(item.get('tourId')) || ''
      });
      if(item.get('customerTour') && item.getIn(['customerTour', 'customerProfile'])){
        setFieldsValue({
          customerMobile: !item.get('customerMobile') ? item.getIn(['customerTour', 'phone']) : item.get('customerMobile'),
          customerFirstName: !item.get('customerFirstName') ? item.getIn(['customerTour', 'customerProfile', 'firstName']) : item.get('customerFirstName'),
          customerLastName: !item.get('customerLastName') ? item.getIn(['customerTour', 'customerProfile', 'lastName']) : item.get('customerLastName'),
          customerGender: !item.get('customerGender') ? item.getIn(['customerTour', 'customerProfile', 'gender']) : item.get('customerGender') ,
        });
      }
    }
    if (id && getFieldsValue().customerMobile) {
      setFilterCustomer({
        keyword: getFieldsValue().customerMobile,
      })
    }
  }, [item])
   
  useEffect(() => {
    // Xoá tour khi thay đổi filter
    setFieldsValue({ 
      tourId: ""
    });
    setTourInfo({})
  }, [
    filterTour.toJS().area,
    filterTour.toJS().isFromAirport,
    filterTour.toJS().company,
  ])
   
  useEffect(() => {
    getListTour(filterTour)
  }, [filterTour])

  useEffect(() => { 
    if (listTour.toJS().length === 1) {
      let tourInfo = listTour.toJS()[0]
      setFieldsValue({
        orderPrice: tourInfo.tourPrice,
        tourName: tourInfo.id
      });
      setDistanceRate(tourInfo.distanceRate)
    }
    let options = listTour.toJS().map(item => {
      return (
        <AutoComplete.Option key={item.id} label={item.name} value={String(item.id)}>
          <span className="certain-search-item-count">{`${item.name} - ${item.areaInfo && item.areaInfo.name}`}</span>
        </AutoComplete.Option>
      )
    }) 
    setOptionsTour(options)
  }, [listTour])

  useEffect(() => {
    getListCustomer(filterCustomer)
  }, [filterCustomer])
  
  useEffect(() => {
    let options = listCustomer.toJS().map(item => {
      return (
        <AutoComplete.Option key={item.phone} value={item.phone}>
          <b>{item.customerProfile ? item.customerProfile.fullName : ""} </b>
          <span className="certain-search-item-count">{item.phone}</span>
        </AutoComplete.Option>
      )
    })
    setOptionsCustomer(options)
  }, [listCustomer])

  useEffect(() => {
    setFilterTour({
      per_page: 100,
      page: 1,
      keyword: '',
    })
    setFilterCustomer({
      per_page: 10,
      page: 1,
      keyword: '',
    })
    getListCustomer()
    getListDriver()
    getListItemsArea(filterArea)
    if(driverSelect && driverSelect.id) checkFeeWalletOrderFunc(driverSelect.id )
  }, []) 
  return (
    <>
      <Modal
        title="Chọn tài xế"
        centered
        visible={visibleDriverList}
        onCancel={() => setVisibleDriverList(false)}
        width={1000}
        zIndex={1020}
        footer={[
          <Button key="back" onClick={() => setVisibleDriverList(false)}>
            Huỷ
          </Button>,
        ]}
      >
        <DriverList tourInfo={tourInfo} selectDriver={selectDriver} />
      </Modal>
      <Form
        id="formOrderTour"
        {...formItemLayout}
        className="input-floating form-ant-custom-upload">
        <Form.Item label="Công ty">
          <span className="ant-form-text">{id && item.get('company')}</span>
        </Form.Item>
        <Form.Item label="Tên tour">
          {getFieldDecorator('tourId', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('tourId'),
          })( 
                <AutoComplete
                  dataSource={optionsTour}
                  onSelect={onSelectTour}
                  onSearch={onSearchTour}
                  optionLabelProp="label" 
                >
                  <Input
                    style={{ width: '100%' }}
                    addonAfter={tourInfo && tourInfo.uuid && !company && 
                      <div className="info_tour" onClick={()=> handleInfoTour(tourInfo)}>
                        {tourInfo && tourInfo.areaInfo &&
                          <span>{tourInfo.areaInfo.name}</span>
                        }
                        <Icon
                          type="info-circle"
                          className="icon_info_tour" 
                        />
                      </div>
                    }
                  />
                </AutoComplete> 
          )}
          <Checkbox checked={isFromAirport} onChange={onChangeIsFromAirport}>
            Từ sân bay
          </Checkbox>
          {company ?
            null
            :
            <FilterTagsCompany changeTags={changeTagsFilterCompany}  inputTitle = "Enter company" />
          } 
          <div style={{marginTop: '16px'}}>
            <Select
                  mode="multiple"
                  placeholder="Filter tour theo 1 hoặc nhiều khu vực"
                  onChange={onChangeAreaChange}
                  allowClear
                  value={area}
                >
                  {itemsArea.toJS().map((e) => {
                    return(
                    <Option value={e.uuid}>{e.name}</Option> 
                    )
                  })
                  }
              </Select>
          </div>
        </Form.Item>
        {tourInfo && tourInfo.type === 'fix_price' && 
          <Form.Item
            label="Thời gian thêm" >
            {getFieldDecorator('extraTime', {
              initialValue: id && item.get('extraTime'),
            })(
              <InputNumber
                type="number"
                className="input-number-transition"
              />
            )}
          </Form.Item>
        }
        {tourInfo && tourInfo.type === 'time_price' && 
          <Form.Item
            label="Số ngày thuê" >
            {getFieldDecorator('time', {
              initialValue: id && item.get('time'),
            })(
              <InputNumber
                type="number"
                className="input-number-transition"
              />
            )}
          </Form.Item>
        }
        <Form.Item
          label="Tài xế tour"
          validateStatus={
            (statusFeeWallet && statusFeeWallet.toJS().error === errorCode.ERROR_TOUR_ORDER_DRIVER_DONT_HAVE_ENOUGHT_FEE_WALLET && driverSelect && driverSelect.id) ?
            "warning" 
            : ""
          }
          help={
            (statusFeeWallet && statusFeeWallet.toJS().error === errorCode.ERROR_TOUR_ORDER_DRIVER_DONT_HAVE_ENOUGHT_FEE_WALLET && driverSelect && driverSelect.id) ?
            "Tài xế không đủ tiền để nhận đơn hàng này"
            : ""
          }
        >
          {getFieldDecorator('driverId', {
            initialValue: driverSelect && driverSelect.driverProfile && driverSelect.driverProfile.fullName,
          })(
            <Input onChange={onChangeDriver} allowClear={true} onClick={handleOpenDriverList} value={(driverSelect && driverSelect.driverProfile && driverSelect.driverProfile.fullName)} />
          )}
        </Form.Item>
        <Form.Item
          name="agreement"
          valuePropName="checked"
          {...tailFormItemLayout}
        >
          {getFieldDecorator('isPaid', {
            initialValue: id && item.get('isPaid'),
          })(
            <Checkbox checked={checkedIsPaid} onChange={onChangeCheckedIsPaid}>
              Đã thanh toán
          </Checkbox>
          )}
        </Form.Item>
        <Divider orientation="left"><b>Thông tin khách hàng</b></Divider>
        <Form.Item label="Điện thoại khách hàng">
          {getFieldDecorator('customerMobile', {
            rules: [
              {
                validator: validatePhone,
              },
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('customerMobile'),
          })(
            <AutoComplete
              dataSource={optionsCustomer}
              onSelect={onSelectCustomer}
              onSearch={onSearchCustomer}
              optionLabelProp="value"
            >
              <Input style={{ width: '100%' }} />
            </AutoComplete>
          )}
        </Form.Item>
        <Form.Item
          label="Họ khách hàng" >
          {getFieldDecorator('customerLastName', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('customerLastName'),
          })(
            <Input />
          )}
        </Form.Item>
        <Form.Item
          label="Tên khách hàng" >
          {getFieldDecorator('customerFirstName', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('customerFirstName'),
          })(
            <Input />
          )}
        </Form.Item>
        <Form.Item label="Giới tính khách hàng">
          {getFieldDecorator('customerGender', {
            initialValue: id && item.get('customerGender'),
          })(
            <Select placeholder="Chọn">
              {config.MANAGEMENT_GENDER_ARR.map(item => (
                <Select.Option value={item.key} key={`gender_${item.key}`}>
                  {item.label}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item
          label="Mã số chuyến bay" >
          {getFieldDecorator('flyNumber', {
            initialValue: id && item.get('flyNumber'),
          })(
            <Input />
          )}
        </Form.Item>
        <Divider orientation="left"><b>Thông tin đơn hàng</b></Divider>
        <Form.Item label="Trạng thái order">
          {getFieldDecorator('status', {
            initialValue: id ? item.get('status') ? item.get('status') : 1 : 1,
          })(
            <Select placeholder="Vui lòng chọn">
              {config.MANAGEMENT_STATUS_TOUR_ARR.map(e => (
                <Select.Option
                  value={e.value}
                  key={`vehicle_brand_${e.key}`}>
                  {e.label}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>        
        {getFieldsValue().status === config.MANAGEMENT_STATUS_TOUR.CANCELED_CUSTOMER &&
          <Form.Item label="Lý do khách hàng huỷ">
            {getFieldDecorator('customerCancelReason', {
              initialValue: id && item.get('customerCancelReason'),
            })(<TextArea rows={4} />)}
          </Form.Item>
        }
        {getFieldsValue().status === config.MANAGEMENT_STATUS_TOUR.CANCELED_DRIVER &&
          <Form.Item label="Lý do tài xế huỷ">
            {getFieldDecorator('driverCancelReason', {
              initialValue: id && item.get('driverCancelReason'),
            })(<TextArea rows={4} />)}
          </Form.Item>
        }
        <Form.Item
          label="Mã giao dịch ngân hàng" >
          {getFieldDecorator('paymentCode', {
            initialValue: id && item.get('paymentCode'),
          })(
            <Input />
          )}
        </Form.Item>
        <Form.Item
          label="Mã liên kết" >
          {getFieldDecorator('externalCode', {
            initialValue: id && item.get('externalCode'),
          })(
            <Input />
          )}
          <span>Dành cho các công ty đối tác của AZGO</span>
        </Form.Item>
        <Form.Item
          label="Mã giảm giá" >
          {getFieldDecorator('promotionCode', {
            initialValue: id && item.get('promotionCode'),
          })(
            <Input />
          )}
          {/* <p className="text-calc-distance">
              <span
                onClick={checkCoupon}
                className="btn-calc-price-per-distance"
              >
                Kiểm tra mã
              </span>
            </p> */}
        </Form.Item>
        {/* <div className="container_red_form_item"> */}
          <Form.Item
            label="Giá chuyến đi"
          >
            {getFieldDecorator('orderPrice', {
              initialValue: id && item.get('orderPrice'),
            })(
              <InputNumber
                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                parser={value => value.replace(/\$\s?|(,*)/g, '')}
                className="input-number-transition"
              />
            )}
          </Form.Item>
        {/* </div> */}
        {tourInfo && tourInfo.type === 'distance_price' && 
          <Form.Item
            label="Quãng đường đi (km)"
          >
            {getFieldDecorator('distance', {
              initialValue: id && item.get('distance'),
            })(
              <InputNumber
                type="number"
                className="input-number-transition"
              />
            )}
            {distanceRate !== null &&
              <FormatPrice
                value={distanceRate}
                thousandSeparator={'.'}
                decimalSeparator={','}
                displayType={'text'}
                renderText={(value) =>
                  <p className="text-calc-distance">
                    Giá tiền tính theo km là {value}VNĐ/km
                    <span
                      onClick={calcPricePerDistance}
                      className="btn-calc-price-per-distance"
                    >
                      Tính lại giá xe theo km
                    </span>
                  </p>
                }
              />
            }
          </Form.Item>
        }
        <Form.Item
          name="agreement"
          valuePropName="checked"
          {...tailFormItemLayout}
        >
          {getFieldDecorator('autoSendSms', {
            initialValue: id && item.get('autoSendSms'),
          })(
            <Checkbox checked={checkedAutoSendSms} onChange={onChangeCheckedAutoSendSms}>
              Tự động gửi tin nhắn
          </Checkbox>
          )}
        </Form.Item>
        <Form.Item
          name="agreement"
          valuePropName="checked"
          {...tailFormItemLayout}
        >
          {getFieldDecorator('autoSendNotification', {
            initialValue: id && item.get('autoSendNotification'),
          })(
            <Checkbox checked={checkedAutoSendNotification} onChange={onChangeCheckedAutoSendNotification}>
              Tự động gửi thông báo
          </Checkbox>
          )}
        </Form.Item>
        <Divider orientation="left"><b>Thời gian - địa điểm</b></Divider>
        <Form.Item label="Khởi hành lúc">
          {getFieldDecorator('startTime', {
            initialValue:
              id ? (item.get('startTime') ? moment(item.get('startTime')) : moment()) : moment(),
          })(<DatePicker format={dateFormat} placeholder="Chọn" />)}
        </Form.Item>
        <Form.Item
          label="Địa chỉ đón"
        >
          {getFieldDecorator('addressPick', {
            rules: [
              {
                required: tourInfo && tourInfo.type === 'fix_price',
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('addressPick'),
          })(
            <Input />
          )}
        </Form.Item>
        <Form.Item
          label="Kinh độ(điểm đón)"
        >
          {getFieldDecorator('latPickLocation', {
            rules: [
              {
                required: tourInfo && tourInfo.type === 'fix_price',
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('latPickLocation'),
          })(
            <Input type="number" />
          )}
        </Form.Item>
        <Form.Item
          label="Vĩ độ(điểm đón)"
        >
          {getFieldDecorator('longPickLocation', {
            rules: [
              {
                required: tourInfo && tourInfo.type === 'fix_price',
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('longPickLocation'),
          })(
            <Input type="number" />
          )}
        </Form.Item>
        <Form.Item
          label="Địa chỉ đến"
        >
          {getFieldDecorator('addressDestination', {
            initialValue: id && item.get('addressDestination'),
          })(
            <Input />
          )}
        </Form.Item>
        <Form.Item
          label="Kinh độ(điểm đến)"
        >
          {getFieldDecorator('latDestinationLocation', {
            initialValue: id && item.get('latDestinationLocation'),
          })(
            <Input type="number" />
          )}
        </Form.Item>
        <Form.Item
          label="Vĩ độ(điểm đến)"
        >
          {getFieldDecorator('longDestinationLocation', {
            initialValue: id && item.get('longDestinationLocation'),
          })(
            <Input type="number" />
          )}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit} className="confirm_update_button">
            {id ? 'Xác nhận' : 'Tạo mới'}
          </Button>
          {id && !company ? <Button type="secondary" htmlType="delete" onClick={handleDelete}>
            Xoá order
          </Button> : ""
          }
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({
  userInfo: UserSelectors.getUserData(state),
  item: TripManagementSelectors.getItem(state),
  listCustomer: ClientManagementSelectors.getItems(state),
  listTour: TourManagementSelectors.getItems(state),
  filterArea: AreaManagementSelectors.getFilter(state),
  itemsArea: AreaManagementSelectors.getItems(state),
  filterCustomer: ClientManagementSelectors.getFilter(state),
  filterTour: TourManagementSelectors.getFilter(state),
  statusFeeWallet: TripManagementSelectors.getStatusFeeWallet(state),
})

const mapDispatchToProps = dispatch => ({
  createItem: values => dispatch(TripManagementActions.createItemRequest(values)),
  editItem: values => dispatch(TripManagementActions.editItemRequest(values)),
  getListDriver: values => dispatch(DriverManagementActions.getItemsRequest(values)),
  getListCustomer: filter => dispatch(ClientManagementActions.getItemsRequest(filter)),
  getListTour: filter => dispatch(TourManagementActions.getItemsRequest(filter)),
  deleteItem: filter => dispatch(TripManagementActions.deleteItemRequest(filter)),
  setFilterCustomer: filter => dispatch(ClientManagementActions.setFilter(filter)),
  setFilterTour: filter => dispatch(TourManagementActions.setFilter(filter)),
  getListItemsArea: values =>  dispatch(AreaManagementActions.getItemsRequest(values)),
  checkFeeWalletOrder: values => dispatch(TripManagementActions.checkFeeWalletOrder(values)), 

})

FormTour.propTypes = {
  form: PropTypes.any,
  id: PropTypes.string,
  item: PropTypes.object,
  createItem: PropTypes.func,
  editItem: PropTypes.func,
  checkFeeWalletOrder: PropTypes.func,
  deleteItem: PropTypes.func,
  itemsVehicleType: PropTypes.object,
  itemsBrandVehicle: PropTypes.object,
  itemsDriver: PropTypes.object,
  history: PropTypes.any,
  statusFeeWallet: PropTypes.any, 
}
const FormVehicle = Form.create()(FormTour)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(FormVehicle)








