import NumberFormat from 'react-number-format';
import React, { useEffect, useState } from 'react'
const FormatPrice = ({
  value = 0,
  renderText,
  decimalSeparator = '.',
  thousandSeparator = ',',
  displayType = 'text',
}) => {

  return (
    <NumberFormat
      value={value}
      thousandSeparator={thousandSeparator}
      decimalSeparator={decimalSeparator}
      displayType={displayType}
      renderText={renderText}
    />
  );
}

export default FormatPrice