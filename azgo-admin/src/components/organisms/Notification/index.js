import React, { useEffect } from 'react'
import { message } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import 'Stores/Notification/Reducers'
import { NotificationSelectors } from 'Stores/Notification/Selectors'
import { NotificationActions } from 'Stores/Notification/Actions'
import PropTypes from 'prop-types'
import { TYPE_MESSAGE } from 'Utils/enum'
const Notification = ({ notifyInfo, clearNotifyRequest, isShowNotify }) => {
  useEffect(() => {
    if (isShowNotify && notifyInfo.type === TYPE_MESSAGE.SUCCESS) {
      message.success(notifyInfo.message)
      clearNotifyRequest()
    }
    if (isShowNotify && notifyInfo.type === TYPE_MESSAGE.ERROR) {
      message.error(notifyInfo.message)
      clearNotifyRequest()
    }
    if (isShowNotify && notifyInfo.type === TYPE_MESSAGE.WARNING) {
      message.warning(notifyInfo.message)
      clearNotifyRequest()
    }
  })
  return <></>
}
const mapStateToProps = state => ({
  notifyInfo: NotificationSelectors.getNotifyInfo(state),
  isShowNotify: NotificationSelectors.getIsShowNotify(state),
})

const mapDispatchToProps = dispatch => ({
  clearNotifyRequest: () => dispatch(NotificationActions.clearNotifyRequest()),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)
Notification.propTypes = {
  notifyInfo: PropTypes.object,
  clearNotifyRequest: PropTypes.func,
  isShowNotify: PropTypes.bool,
}
export default compose(withConnect)(Notification)
