import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Form, Input, Button, Select, DatePicker, Row, Col, Checkbox, Typography, AutoComplete } from 'antd'
import PropTypes from 'prop-types'
import moment from 'moment'

import 'Stores/DriverManagement/Reducers'
import 'Stores/DriverManagement/Sagas'
import { DriverManagementActions } from 'Stores/DriverManagement/Actions'

import 'Stores/AccountManagement/Reducers'
import 'Stores/AccountManagement/Sagas'
import { AccountManagementActions } from 'Stores/AccountManagement/Actions'
 
import 'Stores/ProviderManagement/Reducers'
import 'Stores/ProviderManagement/Sagas'
import { ProviderManagementActions } from 'Stores/ProviderManagement/Actions'
import { ProviderManagementSelectors } from 'Stores/ProviderManagement/Selectors'

import 'Stores/BankManagement/Reducers'
import 'Stores/BankManagement/Sagas'
import { BankManagementActions } from 'Stores/BankManagement/Actions'
import { BankManagementSelectors } from 'Stores/BankManagement/Selectors'

import 'Stores/Intercity/AreaManagement/Reducers'
import 'Stores/Intercity/AreaManagement/Sagas'
import { AreaManagementActions } from 'Stores/Intercity/AreaManagement/Actions'
import { AreaManagementSelectors } from 'Stores/Intercity/AreaManagement/Selectors'

import { UserSelectors } from 'Stores/User/Selectors'
import { AccountManagementSelectors } from 'Stores/AccountManagement/Selectors'
 
import UploadImage from 'components/molecules/UploadImg'
import {_parsePhoneNumber} from 'Utils/helper'
import * as config from 'Utils/enum'

const { Text, Link } = Typography;
const FormDriverAccountContainer = ({
  form,
  item,
  getItemsProviderOfDriver,
  getItemsOfDriver,
  getAccount,
  id,
  editItem,
  createItem,
  changeBlockUser,
  itemUser,
  account,
  bankList,
  getItemsOfBank,
  itemsArea,
  getListItemsArea,
  setFilterArea,
  filterArea,
}) => {
  const { getFieldDecorator, getFieldsValue, setFieldsValue } = form
  const { Option } = Select
  const [imageAvatar, setImageAvatar] = useState('')
  const [imageFrontCmnd, setImageFrontCmnd] = useState('')
  const [imageBackCmnd, setImageBackCmnd] = useState('')
  const [frontDrivingLicense, setFrontDrivingLicense] = useState('')
  const [backDrivingLicense, setBackDrivingLicense] = useState('')
  const [checked, setChecked] = useState(false)
  const [checkedBikeCar, setCheckedBikeCar] = useState(true)
  const [checkedActive, setCheckedActive] = useState(0)
  const [bankOptions, setBankOptions] = useState([])
  useEffect(() => {
    setBankOptions(bankList.toJS().map((e) => {return {text: `${e.vnName} (${e.shortName})`, value:e.bankCode} }))
  }, [bankList])
  const onChangeCheckox = (e) => {
    setChecked(e.target.checked)
    setFieldsValue({
      DRIVER_TOUR: e.target.checked, 
    })
  }
  const onChangeCheckoxBikeCar = (e) => {
    setCheckedBikeCar(e.target.checked)
    setFieldsValue({ 
      DRIVER_BIKE_CAR: e.target.checked,
    })
  }
  const onChangeCheckboxActive= (e) => {
    setCheckedActive(e.target.checked)
  }
  const handleSubmit = () => { 
    form.validateFields(async err => {
      if (err) {
        return false
      } 
      let fields = { ...getFieldsValue()}
      let roles = [];
      for (const key in config.MANAGEMENT_TYPE_USER) {
        if(!!getFieldsValue()[key]) roles.push(config.MANAGEMENT_TYPE_USER[key])
      }
      if (id) {
        editItem({
          ...fields,
          areaUuid: fields.areaUuid || null,
          roles: roles,
          birthday: moment(form.getFieldsValue().birthday).format('DD/MM/YYYY'),
          type: roles[0] || config.MANAGEMENT_TYPE_USER.DRIVER_BIKE_CAR,
          id,
          status: item.get('status'),
          changeBlockUser: changeBlockUser,
          providerId:
            itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
              ? itemUser.getIn(['providersInfo', 'id'])
              : form.getFieldsValue().providerId,
          user_uuid: item.get('userId'), 
          isActive: checkedActive
        })  
      } else {
        createItem({
          ...fields,
          roles: roles,
          areaUuid: fields.areaUuid || null,
          birthday: moment(form.getFieldsValue().birthday).format('DD/MM/YYYY'),
          type: roles[0] || config.MANAGEMENT_TYPE_USER.DRIVER_BIKE_CAR,
          providerId:
            itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
              ? itemUser.getIn(['providersInfo', 'id'])
              : form.getFieldsValue().providerId, 
          user_uuid: item.get('userId'), 
          isActive: checkedActive
        })
      }
    })
  }
  const dateFormat = ['DD/MM/YYYY']
  const formAccount = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 18 },
    },
  }
  const formItemLayoutUpload = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 24 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 24 },
    },
  }
  const tailFormCheckboxLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 24,
        offset: 0,
        // push: 2
      },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 24,
        // offset: 4,
        push: 12
      },
    },
  }
  const compareToFirstPassword = (rule, value, callback) => {
    if (value && value !== form.getFieldValue('password')) {
      callback('Mật khẩu không trùng khớp!')
    } else {
      callback()
    }
  }
  const prefixSelector = getFieldDecorator('countryCode', {
    initialValue: '84',
  })(
    <Select style={{ width: 70 }}>
      <Option value="84">+84</Option>
    </Select>
  )
  const callbackHandleAvatar = value => {
    if(!value) {
      setImageAvatar(null)
      // set lai state imageProfile
      let imageProfile = null
      setFieldsValue({
        imageProfile: null,
      })
    }
    else {
      setImageAvatar(value.file.response.data[0].address)
      // set lai state imageProfile
      let imageProfile = {
        address: value.file.response.data[0].address,
        filePath: value.file.response.data[0].file_path,
        fileName: value.file.response.data[0].file_name,
      }
      setFieldsValue({
        imageProfile: imageProfile,
      })
    }  
  }
  const callbackHandleFrontCmnd = value => {
    if(!value) {
      setImageFrontCmnd(null)
      // set lai state imageProfile
      let imageProfile = null
      setFieldsValue({
        frontOfCmnd: null,
      })
    }
    else {
      setImageFrontCmnd(value.file.response.data[0].address)
      // set lai state imageProfile
      let imageProfile = {
        address: value.file.response.data[0].address,
        filePath: value.file.response.data[0].file_path,
        fileName: value.file.response.data[0].file_name,
      }
      setFieldsValue({
        frontOfCmnd: imageProfile,
      })
    }
  }
  const callbackHandleBackCmnd = value => {
    if(!value) {
      setImageBackCmnd(null) 
      setFieldsValue({
        backOfCmnd: null,
      })
    }
    else {
      setImageBackCmnd(value.file.response.data[0].address)
      // set lai state imageProfile
      let imageProfile = {
        address: value.file.response.data[0].address,
        filePath: value.file.response.data[0].file_path,
        fileName: value.file.response.data[0].file_name,
      }
      setFieldsValue({
        backOfCmnd: imageProfile,
      })
    }
  }
  const callbackHandlefrontDrivingLicense = value => {
    if(!value) {
      setFrontDrivingLicense(null) 
      setFieldsValue({
        frontDrivingLicense: null,
      })
    }
    else {
      setFrontDrivingLicense(value.file.response.data[0].address)
      // set lai state imageProfile
      let imageProfile = {
        address: value.file.response.data[0].address,
        filePath: value.file.response.data[0].file_path,
        fileName: value.file.response.data[0].file_name,
      }
      setFieldsValue({
        frontDrivingLicense: imageProfile,
      })
    }
      
  }
  const callbackHandleBackDrivingLicense = value => {
    if(!value) {
      setBackDrivingLicense(null) 
      setFieldsValue({
        backDrivingLicense: null,
      })
    }
    else {
      setBackDrivingLicense(value.file.response.data[0].address)
      // set lai state imageProfile
      let imageProfile = {
        address: value.file.response.data[0].address,
        filePath: value.file.response.data[0].file_path,
        fileName: value.file.response.data[0].file_name,
      }
      setFieldsValue({
        backDrivingLicense: imageProfile,
      })
    }
  }
  useEffect(() => {
    getListItemsArea(filterArea)
  }, [filterArea])
  useEffect(() => {
    setFilterArea({
      per_page: 100,
      page: 1,
      keyword: '',
    })
  }, [])
  useEffect(() => { 
    getAccount({user_uuid: item.get('userId')})
    getItemsOfBank()
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.ADMIN) {
      getItemsOfDriver()
      // getAccount(item.get('userId'))
    }
  }, [item])
  useEffect(() => {
    if(account) { 
      setCheckedActive(account.get('isActive'))
      setFieldsValue({
        accountBankBranch: account.get('accountBankBranch') || '',
        accountBankNumber: account.get('accountBankNumber') || '',
        accountBankUserName: account.get('accountBankUserName') || '',
        accountBankName: account.get('accountBankName') || '',
        accountBankCode: account.get('accountBankCode') || '',
        isActive: account.get('isActive') || '',
      })
    } 
  }, [account])
  useEffect(() => {
    if(!!item.get("roles")){
      let roles = item.get("roles").toJS();
      roles = Array.isArray(roles) ? roles : []
      setCheckedBikeCar(roles.indexOf(config.MANAGEMENT_TYPE_USER.DRIVER_BIKE_CAR) !== -1)
      setChecked(roles.indexOf(config.MANAGEMENT_TYPE_USER.DRIVER_TOUR) !== -1)
      setFieldsValue({
        DRIVER_TOUR: roles.indexOf(config.MANAGEMENT_TYPE_USER.DRIVER_TOUR) !== -1,
        DRIVER_BIKE_CAR: roles.indexOf(config.MANAGEMENT_TYPE_USER.DRIVER_BIKE_CAR) !== -1,
      })
    }
    else {
      setFieldsValue({
        DRIVER_TOUR:  false,
        DRIVER_BIKE_CAR: true,
      })
    }
    if (item.getIn(['driverProfile', 'imageProfile', "address"])) {
      setImageAvatar(item.getIn(['driverProfile', 'imageProfile', "address"]))
    }
    if (item.getIn(['driverProfile', 'frontOfCmnd', "address"])) {
      setImageFrontCmnd(item.getIn(['driverProfile', 'frontOfCmnd', "address"]))
    }
    if (item.getIn(['driverProfile', 'backOfCmnd', "address"])) {
      setImageBackCmnd(item.getIn(['driverProfile', 'backOfCmnd', "address"]))
    }
    if (item.getIn(['driverProfile', 'frontDrivingLicense', "address"])) {
      setFrontDrivingLicense(item.getIn(['driverProfile', 'frontDrivingLicense', "address"]))
    }
    if (item.getIn(['driverProfile', 'backDrivingLicense', "address"])) {
      setBackDrivingLicense(item.getIn(['driverProfile', 'backDrivingLicense', "address"]))
    }
    return () => {
      setImageAvatar('')
      setImageFrontCmnd('')
      setImageBackCmnd('')
      setFrontDrivingLicense('')
      setBackDrivingLicense('')
    }
  }, [item])
  const validatePhone = (rule, value, callback) => { 
    let phone = form.getFieldValue('phone')
    let countryCode = form.getFieldValue('countryCode')
    if (phone && countryCode) {
      let result = _parsePhoneNumber(countryCode, phone)
      if(result && result.isValid)  callback()
      else callback('Số điện thoại không hợp lệ')
    } else {
      callback()
    }
  }  
  const onSearch = (searchText) => {
    let newOptions = bankList.toJS().filter((e) => {if(e && e.shortName && e.shortName.match(new RegExp(searchText, "i")) || (e && e.vnName && e.vnName.match(new RegExp(searchText, "i")))) return e})
    setBankOptions(newOptions.map((e) => {return {text: `${e.vnName} (${e.shortName})`, value:e.bankCode}}))
  } 
  const onSelect = (value) => {
    let optionSelected = bankList.toJS().find((e) =>  e.bankCode == value)
    setFieldsValue({
      accountBankCode: optionSelected.bankCode,
      accountBankName: optionSelected.shortName,
    })
  } 
  return (
    <>
    <Row gutter={[16, 16]}> 
      <Col xs={{ span: 24, offset: 0 }} md={{ span: 4, offset: 0 }}>
        <div className="section_driver_detail">Thông tin tài khoản</div>
        <UploadImage
          imageProp={imageAvatar}
          handleParent={callbackHandleAvatar}
          type_image_props='driver'
          fileNameProp={item && item.toJS() && item.toJS().driverProfile && item.toJS().driverProfile.imageProfile && item.toJS().driverProfile.imageProfile.fileName}
        />
      </Col>
      <Col xs={{ span: 24, offset: 0 }} md={{ span: 10, offset: 0 }}>
      <div className="section_driver_detail"></div>
      <Form
        id="frmDriverAccount"
        {...formItemLayout}
        className="input-floating form-ant-custom-upload">
        <Form.Item label="Số ĐT" >
          {getFieldDecorator('phone', {
            rules: [
              {
                // pattern: new RegExp("^([0-9]{9})$"),
                validator: validatePhone,
                // message: 'Phone không đúng định dạng, bắt buộc 9 số và bỏ số 0 đầu!',
              },
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('phone'),
          })(<Input type="number" disabled={!id ? false : true} addonBefore={prefixSelector} style={{ width: '100%' }} />)}
        </Form.Item>
        {!id ? (
          <Form.Item label="Mật khẩu" hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Trường bắt buộc!',
                },
                {
                  // pattern: new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,200}$"),
                  message: 'Trường tối thiểu 6 ký tự, 1 ký tự hoa, 1 ký tự thường và 1 số, tối đa 200 ký tự',
                },
              ],
            })(<Input.Password />)}
          </Form.Item>
        ) : ""}
        {!id ? (
          <Form.Item label="Xác nhận" hasFeedback extra="Xác nhận lại mật khẩu đã nhập">
            {getFieldDecorator('confirm', {
              rules: [
                {
                  validator: compareToFirstPassword,
                },
                {
                  required: true,
                  message: 'Trường bắt buộc!',
                },
              ],
            })(<Input.Password onPaste={e => e.preventDefault()} />)}
          </Form.Item>
        ) : ""}
        <Form.Item label="Họ tài xế">
          {getFieldDecorator('lastName', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                min: 1,
                message: 'Trường không được ít hơn 1 ký tự!',
              },
              {
                max: 10,
                message: 'Trường không được quá 10 ký tự!',
              },
            ],
            initialValue: id && item.getIn(['driverProfile', 'lastName']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Tên tài xế">
          {getFieldDecorator('firstName', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                min: 1,
                message: 'Trường không được ít hơn 1 ký tự!',
              },
              {
                max: 30,
                message: 'Trường không được quá 30 ký tự!',
              },
            ],
            initialValue: id && item.getIn(['driverProfile', 'firstName']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Ngày sinh">
          {getFieldDecorator('birthday', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue:
              id && item.getIn(['driverProfile', 'birthday'])
                ? moment(item.getIn(['driverProfile', 'birthday']))
                : null,
          })(<DatePicker format={dateFormat} placeholder="Chọn" />)}
        </Form.Item>
        <Form.Item label="Giới tính">
          {getFieldDecorator('gender', {
            rules: [{ required: true, message: 'Trường bắt buộc!' }],
            initialValue: id && item.getIn(['driverProfile', 'gender']),
          })(
            <Select placeholder="Chọn">
              {config.MANAGEMENT_GENDER_ARR.map(item => (
                <Select.Option value={item.key} key={`gender_${item.key}`}>
                  {item.label}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Số CMND">
          {getFieldDecorator('cmnd', {
            rules: [
              {
                required: false,
                message: 'Trường bắt buộc!',
              },
              {
                pattern: new RegExp("^([A-Za-z0-9]{9,15})"),
                message: 'Trường chứa từ 9 - 15 ký tự không bỏ dấu',
              },
            ],
            initialValue: id && item.getIn(['driverProfile', 'cmnd']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Khu vực">
          {getFieldDecorator('areaUuid', {
            rules: [
            ],
            initialValue: id && item.getIn(['driverProfile','areaUuid']),
          })(
            <Select placeholder="Vui lòng chọn khu vực" allowClear>
              <Select.Option
                  value={null}
                  key={`area_uuid_no_set`}>
                  Chọn khu vực
              </Select.Option>
              {itemsArea.toJS().map(item => (
                <Select.Option
                  value={item.uuid}
                  key={`area_uuid_${item.uuid}`}>
                  {item.name}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="E-mail">
          {getFieldDecorator('email', {
            rules: [
              {
                pattern: new RegExp("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"),
                message: 'Email không đúng định dạng!',
              },
              {
                max: 50,
                message: 'Trường không hơn lớn 50 ký tự',
              },
            ],
            initialValue: id && item.getIn(['driverProfile','email']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Địa chỉ">
          {getFieldDecorator('address', {
            rules: [
              {
                required: false,
                message: 'Trường bắt buộc!',
              },
              {
                min: 5,
                message: 'Trường không được nhỏ hơn 5 ký tự!',
              },
              {
                max: 255,
                message: 'Trường không được lớn hơn 255 ký tự!',
              },
            ],
            initialValue: id && item.getIn(['driverProfile', 'address']),
          })(<Input />)}
        </Form.Item>
        <Row>
          <Col span={12}>
            <Form.Item
              valuePropName="checked"
              {...tailFormCheckboxLayout}
            >
              {getFieldDecorator('DRIVER_TOUR', {
                initialValue: id && item.get('DRIVER_TOUR'),
              })( 
              <Checkbox checked={checked} onChange={onChangeCheckox}>
                Tài xế tour
              </Checkbox>
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              valuePropName="checked"
              {...tailFormCheckboxLayout}
            >
              {getFieldDecorator('DRIVER_BIKE_CAR', {
                initialValue: id && item.get('DRIVER_BIKE_CAR'),
              })( 
              <Checkbox checked={checkedBikeCar} onChange={onChangeCheckoxBikeCar}>
                Tài xế bike car
              </Checkbox>
              )}
            </Form.Item>
          </Col> 
        </Row>
        <Form.Item hidden>
          {getFieldDecorator('imageProfile', {
            initialValue:
              id && item.getIn(['driverProfile', 'imageProfile'])
                ? item.getIn(['driverProfile', 'imageProfile']).toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('frontOfCmnd', {
            initialValue:
              id && item.getIn(['driverProfile', 'frontOfCmnd'])
                ? item.getIn(['driverProfile', 'frontOfCmnd']).toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('backOfCmnd', {
            initialValue:
              id && item.getIn(['driverProfile', 'backOfCmnd'])
                ? item.getIn(['driverProfile', 'backOfCmnd']).toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('frontDrivingLicense', {
            initialValue:
              id && item.getIn(['driverProfile', 'frontDrivingLicense'])
                ? item.getIn(['driverProfile', 'frontDrivingLicense']).toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('backDrivingLicense', {
            initialValue:
              id && item.getIn(['driverProfile', 'backDrivingLicense'])
                ? item.getIn(['driverProfile', 'backDrivingLicense']).toJS()
                : null,
          })(<Input />)}
        </Form.Item> 
      </Form>
      </Col>
      <Col xs={{ span: 24, offset: 0 }} md={{ span: 10, offset: 0 }}>
      <div className="section_driver_detail">Giấy CMND</div>
      <Form.Item
          className="ant-form-item-load"
          >
          <Row gutter={8}>
            <Col span={12}>
              <Text type="secondary">Mặt trước</Text>
              <UploadImage
                imageProp={imageFrontCmnd}
                handleParent={callbackHandleFrontCmnd}
                // setImgUrl={setImageFrontCmnd}
                type_image_props='driver'
                fileNameProp={item && item.toJS() && item.toJS().driverProfile && item.toJS().driverProfile.frontOfCmnd && item.toJS().driverProfile.frontOfCmnd.fileName}
                />
            </Col>
            <Col span={12}>
            <Text type="secondary">Mặt sau</Text>
              <UploadImage
                imageProp={imageBackCmnd}
                handleParent={callbackHandleBackCmnd}
                // setImgUrl={setImageBackCmnd}
                type_image_props='driver'
                fileNameProp={item && item.toJS() && item.toJS().driverProfile && item.toJS().driverProfile.backOfCmnd && item.toJS().driverProfile.backOfCmnd.fileName}
            />
            </Col> 
          </Row>
        </Form.Item>
      <div className="section_driver_detail">Giấy phép lái xe</div>
        <Form.Item
          className="ant-form-item-load"
          >
             <Row  gutter={8}>
            <Col span={12}>
            <Text type="secondary">Mặt trước</Text>
              <UploadImage
                imageProp={frontDrivingLicense}
                handleParent={callbackHandlefrontDrivingLicense}
                type_image_props='driver'
                fileNameProp={item && item.toJS() && item.toJS().driverProfile && item.toJS().driverProfile.frontDrivingLicense && item.toJS().driverProfile.frontDrivingLicense.fileName}
            />
            </Col>
            <Col span={12}>
            <Text type="secondary">Mặt sau</Text> <UploadImage
                imageProp={backDrivingLicense}
                handleParent={callbackHandleBackDrivingLicense}
                type_image_props='driver'
                fileNameProp={item && item.toJS() && item.toJS().driverProfile && item.toJS().driverProfile.backDrivingLicense && item.toJS().driverProfile.backDrivingLicense.fileName}
              />
            </Col>
          </Row>
        </Form.Item>
        
      </Col>
    </Row>
    <Row>
        <Col  xs={{ span: 24, offset: 0 }} sm={{ span: 24, offset: 0 }} md={{ span: 12, offset: 0 }}>
            <div className="section_driver_detail">Thông tin tài khoản ngân hàng</div>
              <Form
                id="formAccount"
                {...formAccount}
                className="input-floating form-ant-custom-upload">

                <Form.Item label="Tên ngân hàng" labelAlign="left">
                {getFieldDecorator('accountBankCode', {
                    // rules: [
                    //   {
                    //     required: true,
                    //     message: 'Trường bắt buộc!',
                    //   }, 
                    // ],
                    // initialValue: item && item.get('accountBankBranch'),
                  })(
                    <AutoComplete
                      dataSource={bankOptions}
                      onSelect={onSelect}
                      onSearch={onSearch}
                      placeholder="Chọn một ngân hàng"
                      style={{ width: '100%' }}
                    />
                  )} 
                </Form.Item>
                <Form.Item labelAlign="left" hidden = {true}>
                {getFieldDecorator('accountBankName', {
                    // rules: [
                    //   {
                    //     required: true,
                    //     message: 'Trường bắt buộc!',
                    //   }, 
                    // ],
                    // initialValue: item && item.get('accountBankBranch'),
                  })(
                     <Input/>
                  )} 
                </Form.Item>
                <Form.Item label="Chi nhánh"  labelAlign="left">
                  {getFieldDecorator('accountBankBranch', {
                    // rules: [
                    //   {
                    //     required: true,
                    //     message: 'Trường bắt buộc!',
                    //   }, 
                    // ],
                    // initialValue: item && item.get('accountBankBranch'),
                  })(
                    <Input/>
                  )}
                </Form.Item>
                <Form.Item label="Số tài khoản"  labelAlign="left">
                  {getFieldDecorator('accountBankNumber', {
                  //  rules: [
                  //   {
                  //     required: true,
                  //     message: 'Trường bắt buộc!',
                  //   }, 
                  // ],
                    // initialValue:  item && item.get('accountBankNumber'),
                  })(<Input />)}
                </Form.Item>
                <Form.Item label="Tên tài khoản"  labelAlign="left">
                  {getFieldDecorator('accountBankUserName', {
                  //  rules: [
                  //   {
                  //     required: false,
                  //     message: 'Trường bắt buộc!',
                  //   }, 
                  // ],
                    // initialValue: item && item.get('accountBankUserName'),
                  })(<Input />)}
                </Form.Item>
                <Form.Item
                  valuePropName="checked"
                  {...tailFormCheckboxLayout}
                >
                  {getFieldDecorator('isActive', {
                    initialValue: id && item.get('DRIVER_TOUR'),
                  })( 
                  <Checkbox checked={checkedActive} onChange={onChangeCheckboxActive}>
                    Xác minh
                  </Checkbox>
                  )}
                </Form.Item>
              </Form>
        </Col> 
    </Row>
    <Row>
        <Col span={24}>
          <Form.Item {...tailFormItemLayout}>
            <Button className="btn_confirm_form_driver" type="primary" htmlType="submit" onClick={handleSubmit}>
              Xác nhận
            </Button>
          </Form.Item>
          </Col> 
        </Row>
      </>
  )
}

const mapStateToProps = state => ({
  getItemsProviderOfDriver: ProviderManagementSelectors.getItemsList(state),
  itemUser: UserSelectors.getUserData(state),
  account: AccountManagementSelectors.getAccount(state),
  bankList: BankManagementSelectors.getAllItem(state),
  itemsArea: AreaManagementSelectors.getItems(state),
  filterArea: AreaManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  getItemsOfDriver: values =>
    dispatch(ProviderManagementActions.getItemsListRequest(values)),
  getAccount: values => dispatch(AccountManagementActions.getAccount(values)),
  editItem: values => dispatch(DriverManagementActions.editItemRequest(values)),
  createItem: values =>
    dispatch(DriverManagementActions.createItemRequest(values)),
  getItemsOfBank: values => dispatch(BankManagementActions.getAllItemRequest(values)),
  setFilterArea: filter => dispatch(AreaManagementActions.setFilter(filter)),
  getListItemsArea: values =>
    dispatch(AreaManagementActions.getItemsRequest(values)),
})

FormDriverAccountContainer.propTypes = {
  id: PropTypes.any,
  form: PropTypes.any,
  account: PropTypes.any,
  bankList: PropTypes.any,
  item: PropTypes.object,
  getItemsOfDriver: PropTypes.func,
  getItemsOfBank: PropTypes.func,
  getAccount: PropTypes.func,
  getItemsProviderOfDriver: PropTypes.object,
  editItem: PropTypes.func,
  createItem: PropTypes.func,
  changeBlockUser: PropTypes.any,
  itemUser: PropTypes.object,
}
const FormDriverAccount = Form.create()(FormDriverAccountContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormDriverAccount) 
