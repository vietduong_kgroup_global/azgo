import React, { useEffect, useState } from 'react'
import {
  Form,
  Input,
  Button,
  Select,
  DatePicker,
  Upload,
  Icon,
  Modal,
  message,
  Checkbox, 
  InputNumber
} from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import moment from 'moment'
import * as config from 'Utils/enum'
import 'Stores/WalletManagement/Reducers'
import 'Stores/WalletManagement/Sagas'
import { WalletManagementActions } from 'Stores/WalletManagement/Actions'
import { getToken } from 'Utils/token'
import { Config } from 'Config'
import {_parsePhoneNumber} from 'Utils/helper' 
const { TextArea } = Input;
const FormWalletContainer = ({
  form,
  id,
  item,
  createItem,
  editItem,
  changeBlockUser,
  callbackUpdate,
  typeForm
}) => {  
  const { getFieldDecorator, getFieldsValue, setFieldsValue, resetFields } = form
  const { Option } = Select
  const [loading, setLoading] = useState(false) 
  const [checked, setChecked] = useState(id && item.get('status') || 2)
  const [previewVisible, setPreviewVisible] = useState(false)
  const onChangeCheckox = (e) => { 
    setChecked(e.target.checked ? 1 : 0)
  }
  const handleSubmit = () => {
    form.validateFields(async(err) => {
      if (err) {
        return false
      }

      let data = {...getFieldsValue()}  
      if (id) {
        await editItem({
          ...data, 
          feeWallet: Number(data.feeWallet),
          cashWallet: Number(data.cashWallet),
          type: typeForm,
          id: id,
        }, (resultEdit) => {
          if(resultEdit.status) {
            callbackUpdate()
            resetFields()
          }
        })  
      }
    })
  } 
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  } 
    
  useEffect(() => {
    if(id) setChecked(item.get('status') || 0)
    // let filePath = item.getIn(['customerProfile', 'imageProfile', 'filePath'])
    // let fileName = item.getIn(['customerProfile', 'imageProfile', 'fileName'])
    // let imagePath = ''
    // if (filePath && fileName) {
    //   imagePath = filePath + '/' + fileName
    // } 
    // let imageUrl = ''
    // if (item.getIn(['customerProfile', 'imageProfile', 'address'])) {
    //   imageUrl = item.getIn(['customerProfile', 'imageProfile', 'address'])
    //   setImgUrl(imageUrl)
    // } else {
    //   setImgUrl('')
    // }
  }, [item]) 
  return (
    <>
      <Form
        id="frmWalletAccount"
        {...formItemLayout}
        className="input-floating form-ant-custom form_wallet">
        {/* <Form.Item label="Cash wallet">
          {getFieldDecorator('cashWallet', {
            rules: [ ],
            initialValue: id && item.get('cashWallet'),
          })(<Input />)}
        </Form.Item>  */}
        {typeForm === 'feeWallet' ? 
          <Form.Item label="Số tiền">
            {getFieldDecorator('feeWallet', {
              rules: [
                {
                  required: true,
                  message: 'Trường bắt buộc!',
                },  
              ],
            })(
            <InputNumber
              formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              parser={value => value.replace(/\$\s?|(,*)/g, '')}
              // onChange={onChange} 
            />)}
            <span>Nhập số tiền lớn hơn 0(thêm vào) và nhỏ hơn 0(nếu trừ đi) số tiền đang có </span>
          </Form.Item>  
        :
          <Form.Item label="Số tiền">
          {getFieldDecorator('cashWallet', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
                // type: "number",
              },  
            ],
            // initialValue: 0,
          })(
            <InputNumber
              formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              parser={value => value.replace(/\$\s?|(,*)/g, '')} 
            />
          )}
          <span>Nhập số tiền lớn hơn 0(thêm vào) và nhỏ hơn 0(nếu trừ đi) số tiền đang có </span>
        </Form.Item>  
        }
        <Form.Item label="Lý do thay đổi">
          {getFieldDecorator('content', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
                // type: "number",
              },  
            ], 
            initialValue: id && item.get('content'),
          })(<TextArea  rows={4}/>)}
        </Form.Item> 
        <Form.Item label="Mã đơn hàng">
          {getFieldDecorator('orderCode', {
            initialValue: id && item.get('orderCode'),
          })(<Input/>)}
        </Form.Item> 
        <Form.Item label="Mã giao dịch">
          {getFieldDecorator('paymentCode', {
            initialValue: id && item.get('paymentCode'),
          })(<Input/>)}
        </Form.Item> 
        <Form.Item
                name="agreement"
                valuePropName="checked"
                rules={[
                  // {
                  //   validator: (_, value) =>
                  //     value ? Promise.resolve() : Promise.reject('Should accept agreement'),
                  // },
                ]}
                {...tailFormItemLayout}
              >
              {getFieldDecorator('status', {
                initialValue: id && item.get('status'),
              })( 
              <Checkbox checked={checked} onChange={onChangeCheckox}>
                Kích hoạt
              </Checkbox>
              )}
              
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Cập nhật
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({  

})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(WalletManagementActions.createItemRequest(values)),
  editItem: (values, callback) => dispatch(WalletManagementActions.editItemRequest(values, callback)),
})

FormWalletContainer.propTypes = {
  form: PropTypes.any,
  id: PropTypes.string,
  item: PropTypes.object,
  createItem: PropTypes.func,
  callbackUpdate: PropTypes.func,
  editItem: PropTypes.func,
  changeBlockUser: PropTypes.any,
}
const FormWalletAccount = Form.create()(FormWalletContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormWalletAccount)
