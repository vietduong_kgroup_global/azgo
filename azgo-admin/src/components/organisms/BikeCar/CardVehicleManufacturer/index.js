import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { Popconfirm } from 'antd'
import { Card, CardHeader, CardBody, Row, Col } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import { compose } from 'redux'
import { BrandVehicleManagementActions } from 'Stores/BrandVehicleManagement/Actions'
import { connect } from 'react-redux'

const CardVehicleManufacturer = ({ dataSource, deleteItem }) => {
  const Style = {
    cursor: 'pointer',
    marginLeft: '15px',
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  return (
    <>
      <Row>
        {dataSource.toJS().map((item, idx) => (
          <Col xs="12" sm="6" md="4" key={`vehicle_manufacturer_${item.id}`}>
            <Card className="border-info">
              <CardHeader>
                <strong>{idx + 1}</strong>
                <div className="card-header-actions">
                  <Link
                    to={'/admin/bike-car/vehicle-manufacturer/' + item.id}
                    style={Style}>
                    <i className="fa fa-pencil" />
                  </Link>
                  <Popconfirm
                    placement="bottom"
                    onConfirm={() => handleConfirmDelete(item.id)}
                    title="Bạn có muốn xóa?"
                    okText="Có"
                    cancelText="Không">
                    <span style={Style}>
                      <i className="fa fa-trash" />
                    </span>
                  </Popconfirm>
                </div>
              </CardHeader>
              <CardBody>{item.name}</CardBody>
            </Card>
          </Col>
        ))}
      </Row>
    </>
  )
}

CardVehicleManufacturer.propTypes = {
  dataSource: PropTypes.any,
  deleteItem: PropTypes.func,
}
const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({
  deleteItem: id =>
    dispatch(BrandVehicleManagementActions.deleteItemRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(CardVehicleManufacturer)
