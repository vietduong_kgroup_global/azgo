import React, { useEffect, useState } from 'react'
import {
  Form,
  Input,
  Button,
  Select,
  Icon,
  Upload,
  message,
  Modal,
  Row,
  Col,
} from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import 'Stores/BikeCar/VehicleManagement/Reducers'
import 'Stores/BikeCar/VehicleManagement/Sagas'
import 'Stores/BikeCar/VehicleTypeManagement/Reducers'
import 'Stores/BikeCar/VehicleTypeManagement/Sagas'
import 'Stores/BrandVehicleManagement/Reducers'
import 'Stores/BrandVehicleManagement/Sagas'
import 'Stores/BikeCar/DriverManagement/Reducers'
import 'Stores/BikeCar/DriverManagement/Sagas'
import { VehicleManagementActions } from 'Stores/BikeCar/VehicleManagement/Actions'
import { VehicleTypeManagementSelectors } from 'Stores/BikeCar/VehicleTypeManagement/Selectors'
import { VehicleTypeManagementActions } from 'Stores/BikeCar/VehicleTypeManagement/Actions'
import { BrandVehicleManagementSelectors } from 'Stores/BrandVehicleManagement/Selectors'
import { BrandVehicleManagementActions } from 'Stores/BrandVehicleManagement/Actions'
import { DriverManagementSelectors } from 'Stores/BikeCar/DriverManagement/Selectors'
import { DriverManagementActions } from 'Stores/BikeCar/DriverManagement/Actions'
import { fromJS } from 'immutable'
import * as configsEnum from 'Utils/enum'
import { Config } from 'Config'
import { getToken } from 'Utils/token'

const FormVehicleContainer = ({
  form,
  id,
  item,
  createItem,
  editItem,
  itemsVehicleType,
  itemsBrandVehicle,
  getItemsVehicleType,
  getItemsBrandVehicle,
  itemsDriver,
  getItemsWithoutRequest,
}) => {
  const {
    getFieldDecorator,
    getFieldsValue,
    setFieldsValue,
    resetFields,
  } = form
  const { Option } = Select
  const [loading, setLoading] = useState('')
  const [driverId, setDriverId] = useState(null)
  const [imageAvatarUrl, setImageAvatarUrl] = useState([])
  const [imageFrontCarInsurance, setImageFrontCarInsurance] = useState('')
  const [imageBackCarInsurance, setImageBackCarInsurance] = useState('')
  const [imageFrontCertificates, setImageFrontCertificates] = useState('')
  const [imageBackCertificates, setImageBackCertificates] = useState('')
  const [imageAvatarUrlPopup, setImageAvatarUrlPopup] = useState('')
  const [previewVisible, setPreviewVisible] = useState(false)
  const [filter, setFilter] = useState({
    per_page: 1000,
    page: 1,
    keyword: '',
  })
  const [resetField, setResetField] = useState(true)
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      if (id) {
        editItem({
          ...getFieldsValue(),
          driverId: driverId,
          id: parseInt(id, 10),
        })
      } else {
        createItem({
          ...getFieldsValue(),
        })
      }
    })
  }
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 7 },
    },
  }
  const formItemLayoutUpload = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 20 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 20,
        offset: 4,
      },
    },
  }
  const propsHeaderUpload = {
    name: 'images[]',
    action: Config.DEV_URL.UPLOAD_IMAGE,
    headers: {
      Authorization: 'bearer ' + getToken(),
    },
  }
  const getBase64 = (img, callback) => {
    const reader = new FileReader()
    reader.addEventListener('load', () => callback(reader.result))
    reader.readAsDataURL(img)
  }
  const handleDriver = value => {
    setDriverId(value)
  }
  const handleTypeVehicle = value => {
    resetFields('vehicleTypeId')
    setResetField(false)
    getItemsVehicleType(value)
  }
  const handlePreviewImg = (value, idx) => {
    switch (value) {
      case 'driverImg': {
        for (let i = 0; i < imageAvatarUrl.length; i++) {
          if (idx === i) {
            setImageAvatarUrlPopup(
              Config.DEV_URL.IMAGE +
                '/' +
                imageAvatarUrl[i].filePath +
                '/' +
                imageAvatarUrl[i].fileName
            )
          }
        }
        break
      }
      case 'frontCertificates': {
        setImageAvatarUrlPopup(imageFrontCertificates)
        break
      }
      case 'backCertificates': {
        setImageAvatarUrlPopup(imageBackCertificates)
        break
      }
      case 'frontInsurance': {
        setImageAvatarUrlPopup(imageFrontCarInsurance)
        break
      }
      default: {
        setImageAvatarUrlPopup(imageBackCarInsurance)
        break
      }
    }
    setPreviewVisible(true)
  }
  const handleChange = (info, value) => {
    if (info.file.status === 'uploading') {
      switch (value) {
        case 'driverImg': {
          setLoading('driverImg')
          break
        }
        case 'frontCertificates': {
          setLoading('frontCertificates')
          break
        }
        case 'backCertificates': {
          setLoading('backCertificates')
          break
        }
        case 'frontInsurance': {
          setLoading('frontInsurance')
          break
        }
        default: {
          setLoading('backInsurance')
          break
        }
      }
    }

    if (info.file.status === 'done') {
      getBase64(info.file.originFileObj, imageUrl => {
        setLoading('')
        let imageProfile = {
          filePath: info.file.response.data[0].file_path,
          fileName: info.file.response.data[0].file_name,
        }
        switch (value) {
          case 'driverImg': {
            let img =
              Config.DEV_URL.IMAGE +
              '/' +
              imageProfile.filePath +
              '/' +
              imageProfile.fileName
            if (img) {
              imageAvatarUrl.push(imageProfile)
              setFieldsValue({
                images: imageAvatarUrl,
              })
            }
            break
          }
          case 'frontCertificates': {
            setImageFrontCertificates(imageUrl)
            setFieldsValue({
              frontRegistrationCertificates: imageProfile,
            })
            break
          }
          case 'backCertificates': {
            setImageBackCertificates(imageUrl)
            setFieldsValue({
              backRegistrationCertificates: imageProfile,
            })
            break
          }
          case 'frontInsurance': {
            setImageFrontCarInsurance(imageUrl)
            setFieldsValue({
              frontCarInsurance: imageProfile,
            })
            break
          }
          default: {
            setImageBackCarInsurance(imageUrl)
            setFieldsValue({
              backCarInsurance: imageProfile,
            })
            break
          }
        }
      })
    } else if (info.file.status === 'error') {
      setLoading(false)
      message.error('Upload failed')
    }
  }

  const handleRemoveField = index => {
    imageAvatarUrl.splice(index, 1)
    setFieldsValue({
      images: imageAvatarUrl,
    })
  }
  useEffect(() => {
    if (item.get('images')) {
      setImageAvatarUrl(...[item.get('images').toJS()])
    }
    if (item.get('frontRegistrationCertificates')) {
      setImageFrontCertificates(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['frontRegistrationCertificates', 'filePath']) +
          '/' +
          item.getIn(['frontRegistrationCertificates', 'fileName'])
      )
    }
    if (item.get('backRegistrationCertificates')) {
      setImageBackCertificates(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['backRegistrationCertificates', 'filePath']) +
          '/' +
          item.getIn(['backRegistrationCertificates', 'fileName'])
      )
    }
    if (item.get('frontCarInsurance')) {
      setImageFrontCarInsurance(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['frontCarInsurance', 'filePath']) +
          '/' +
          item.getIn(['frontCarInsurance', 'fileName'])
      )
    }
    if (item.get('backCarInsurance')) {
      setImageBackCarInsurance(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['backCarInsurance', 'filePath']) +
          '/' +
          item.getIn(['backCarInsurance', 'fileName'])
      )
    }
    getItemsVehicleType(item.getIn(['vehicleType', 'vehicleGroupId']))
    if (item.size > 0) {
      setDriverId(item.getIn(['users', '0', 'driverProfile', 'userId']))
    }
    return () => {
      setImageAvatarUrl([])
      setImageFrontCertificates('')
      setImageBackCertificates('')
      setImageFrontCarInsurance('')
      setImageBackCarInsurance('')
    }
  }, [item])
  useEffect(() => {
    getItemsBrandVehicle(fromJS(filter))
  }, [filter])
  useEffect(() => {
    getItemsWithoutRequest()
  }, [])
  return (
    <>
      <Form
        id="frmVehicle"
        {...formItemLayout}
        className="input-floating form-ant-custom-upload">
        <Form.Item label="Loại xe">
          {getFieldDecorator('vehicleGroupId', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.getIn(['vehicleType', 'vehicleGroupId']),
          })(
            <Select
              placeholder="Chọn loại xe"
              onChange={handleTypeVehicle}
              disabled={!!id}>
              <Option value={configsEnum.VEHICLE_TYPE.BIKE}>Xe máy</Option>
              <Option value={configsEnum.VEHICLE_TYPE.CAR}>Ô tô</Option>
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Tên loại xe">
          {getFieldDecorator('vehicleTypeId', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue:
              resetField === true
                ? id && item.getIn(['vehicleType', 'id'])
                : null,
          })(
            <Select placeholder="Vui lòng chọn loại xe">
              {itemsVehicleType &&
                itemsVehicleType.map(item => (
                  <Select.Option
                    value={item.get('id')}
                    key={`vehicle_type_${item.get('id')}`}>
                    {item.get('name')}
                  </Select.Option>
                ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Hãng xe">
          {getFieldDecorator('vehicleBrandId', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.getIn(['vehicleBrand', 'id']),
          })(
            <Select placeholder="Vui lòng chọn hãng xe">
              {itemsBrandVehicle.map(item => (
                <Select.Option
                  value={item.get('id')}
                  key={`vehicle_brand_${item.get('id')}`}>
                  {item.get('name')}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Biển số xe">
          {getFieldDecorator('licensePlates', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('licensePlates'),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Tài xế">
          {getFieldDecorator('driverId', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue:
              id && item.getIn(['users', '0', 'driverProfile', 'fullName']),
          })(
            <Select
              placeholder="Vui lòng chọn tài xế"
              onChange={value => handleDriver(value)}>
              {itemsDriver.map(item => (
                <Select.Option
                  value={item.getIn(['driverProfile', 'userId'])}
                  key={`driver_${item.get('userId')}`}>
                  {item.getIn(['driverProfile', 'fullName'])}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item
          label="Hình ảnh xe"
          className="ant-form-item-load"
          {...formItemLayoutUpload}>
          <div className="row-upload-img">
            <div className="col-upload-img col-upload-img-arr">
              {imageAvatarUrl &&
                imageAvatarUrl.map((item, idx) => (
                  <div className="frame-img" key={idx}>
                    <Icon
                      type="delete"
                      onClick={() => handleRemoveField(idx)}
                    />
                    <img
                      src={
                        Config.DEV_URL.IMAGE +
                        '/' +
                        item.filePath +
                        '/' +
                        item.fileName
                      }
                      onClick={() => handlePreviewImg('driverImg', idx)}
                      alt="driverImg"
                    />
                  </div>
                ))}
            </div>
            {imageAvatarUrl.length < 5 ? (
              <div className="col-upload-img-button">
                <Upload
                  name="avatar"
                  className="avatar-uploader"
                  {...propsHeaderUpload}
                  showUploadList={false}
                  onChange={e => handleChange(e, 'driverImg')}>
                  <Button>
                    <Icon type="upload" /> Tải hình
                    {loading === 'driverImg' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                </Upload>
              </div>
            ) : null}
          </div>
        </Form.Item>
        <Form.Item
          label="Giấy tờ xe"
          className="ant-form-item-load"
          {...formItemLayoutUpload}>
          <Row>
            <Col span={8}>
              <div className="frame-img frame-img frame-img-big">
                <div className="frame-img-inner">
                  {imageFrontCertificates && imageFrontCertificates !== '' ? (
                    <img
                      src={imageFrontCertificates}
                      onClick={() => handlePreviewImg('frontCertificates')}
                      alt="image"
                    />
                  ) : (
                    <Icon type="picture" />
                  )}
                </div>
              </div>
              <Upload
                name="avatar"
                className="avatar-uploader"
                {...propsHeaderUpload}
                showUploadList={false}
                onChange={e => handleChange(e, 'frontCertificates')}>
                {imageFrontCertificates && imageFrontCertificates !== '' ? (
                  <Button>
                    <Icon type="edit" /> Thay đổi hình giấy tờ xe mặt trước
                    {loading === 'frontCertificates' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                ) : (
                  <Button>
                    <Icon type="upload" /> Tải hình giấy tờ xe mặt trước
                    {loading === 'frontCertificates' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                )}
              </Upload>
            </Col>
            <Col span={8}>
              <div className="frame-img frame-img-big">
                <div className="frame-img-inner">
                  {imageBackCertificates && imageBackCertificates !== '' ? (
                    <img
                      src={imageBackCertificates}
                      onClick={() => handlePreviewImg('backCertificates')}
                      alt="backInsurance"
                    />
                  ) : (
                    <Icon type="picture" />
                  )}
                </div>
              </div>
              <Upload
                name="avatar"
                className="avatar-uploader"
                {...propsHeaderUpload}
                showUploadList={false}
                onChange={e => handleChange(e, 'backCertificates')}>
                {imageBackCertificates && imageBackCertificates !== '' ? (
                  <Button>
                    <Icon type="edit" /> Thay đổi hình giấy tờ xe mặt sau
                    {loading === 'backCertificates' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                ) : (
                  <Button>
                    <Icon type="upload" /> Tải hình giấy tờ xe mặt sau
                    {loading === 'backCertificates' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                )}
              </Upload>
            </Col>
          </Row>
        </Form.Item>
        <Form.Item
          label="Bảo hiểm xe"
          className="ant-form-item-load"
          {...formItemLayoutUpload}>
          <Row>
            <Col span={8}>
              <div className="frame-img frame-img-big">
                <div className="frame-img-inner">
                  {imageFrontCarInsurance && imageFrontCarInsurance !== '' ? (
                    <img
                      src={imageFrontCarInsurance}
                      onClick={() => handlePreviewImg('frontInsurance')}
                      alt="image"
                    />
                  ) : (
                    <Icon type="picture" />
                  )}
                </div>
              </div>
              <Upload
                name="avatar"
                className="avatar-uploader"
                {...propsHeaderUpload}
                showUploadList={false}
                onChange={e => handleChange(e, 'frontInsurance')}>
                {imageFrontCarInsurance && imageFrontCarInsurance !== '' ? (
                  <Button>
                    <Icon type="edit" /> Thay đổi hình bảo hiểm xe mặt trước
                    {loading === 'frontInsurance' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                ) : (
                  <Button>
                    <Icon type="upload" /> Tải hình bảo hiểm xe mặt trước
                    {loading === 'frontInsurance' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                )}
              </Upload>
            </Col>
            <Col span={8}>
              <div className="frame-img frame-img-big">
                <div className="frame-img-inner">
                  {imageBackCarInsurance && imageBackCarInsurance !== '' ? (
                    <img
                      src={imageBackCarInsurance}
                      onClick={() => handlePreviewImg('backInsurance')}
                      alt="backInsurance"
                    />
                  ) : (
                    <Icon type="picture" />
                  )}
                </div>
              </div>
              <Upload
                name="avatar"
                className="avatar-uploader"
                {...propsHeaderUpload}
                showUploadList={false}
                onChange={e => handleChange(e, 'backInsurance')}>
                {imageBackCarInsurance && imageBackCarInsurance !== '' ? (
                  <Button>
                    <Icon type="edit" /> Thay đổi hình bảo hiểm xe mặt sau
                    {loading === 'backInsurance' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                ) : (
                  <Button>
                    <Icon type="upload" /> Tải hình bảo hiểm xe mặt sau
                    {loading === 'backInsurance' && (
                      <Icon className="loading-image" type="loading" />
                    )}
                  </Button>
                )}
              </Upload>
            </Col>
          </Row>
        </Form.Item>
        <Modal
          visible={previewVisible}
          footer={null}
          onCancel={() => setPreviewVisible(false)}>
          <div className="modal-image">
            <img alt="image" src={imageAvatarUrlPopup} />
          </div>
        </Modal>
        <Form.Item hidden>
          {getFieldDecorator('images', {
            initialValue:
              id && item.get('images') ? item.get('images').toJS() : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('frontRegistrationCertificates', {
            initialValue:
              id && item.get('frontRegistrationCertificates')
                ? item.get('frontRegistrationCertificates').toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('backRegistrationCertificates', {
            initialValue:
              id && item.get('backRegistrationCertificates')
                ? item.get('backRegistrationCertificates').toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('frontCarInsurance', {
            initialValue:
              id && item.get('frontCarInsurance')
                ? item.get('frontCarInsurance').toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('backCarInsurance', {
            initialValue:
              id && item.get('backCarInsurance')
                ? item.get('backCarInsurance').toJS()
                : null,
          })(<Input />)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Xác nhận
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({
  itemsVehicleType: VehicleTypeManagementSelectors.itemsByVehicleGroup(state),
  itemsBrandVehicle: BrandVehicleManagementSelectors.getItems(state),
  itemsDriver: DriverManagementSelectors.getItemsWithout(state),
})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(VehicleManagementActions.createItemRequest(values)),
  editItem: values =>
    dispatch(VehicleManagementActions.editItemRequest(values)),
  getItemsVehicleType: value =>
    dispatch(VehicleTypeManagementActions.getItemsRequestByVehicleGroup(value)),
  getItemsBrandVehicle: filter =>
    dispatch(BrandVehicleManagementActions.getItemsRequest(filter)),
  getItemsWithoutRequest: values =>
    dispatch(DriverManagementActions.getItemsWithoutRequest(values)),
})

FormVehicleContainer.propTypes = {
  form: PropTypes.any,
  id: PropTypes.string,
  item: PropTypes.object,
  createItem: PropTypes.func,
  editItem: PropTypes.func,
  itemsVehicleType: PropTypes.object,
  itemsBrandVehicle: PropTypes.object,
  itemsDriver: PropTypes.object,
  getItemsVehicleType: PropTypes.func,
  getItemsBrandVehicle: PropTypes.func,
  getItemsWithoutRequest: PropTypes.func,
}
const FormVehicle = Form.create()(FormVehicleContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormVehicle)
