import React, { useEffect } from 'react'
import { Form, Input, Button } from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import 'Stores/BrandVehicleManagement/Reducers'
import 'Stores/BrandVehicleManagement/Sagas'
import { BrandVehicleManagementActions } from 'Stores/BrandVehicleManagement/Actions'

const FormVehicleManufacturerContainer = ({
  form,
  id,
  item,
  createItem,
  editItem,
}) => {
  const { getFieldDecorator, getFieldsValue } = form
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      if (id) {
        editItem({
          ...getFieldsValue(),
          id: parseInt(id, 10),
        })
      } else {
        createItem({
          ...getFieldsValue(),
        })
      }
    })
  }
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  }
  return (
    <>
      <Form
        id="frmVehicleManufacturer"
        {...formItemLayout}
        className="input-floating form-ant-custom">
        <Form.Item label="Hãng xe">
          {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('name'),
          })(<Input />)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Xác nhận
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(BrandVehicleManagementActions.createItemRequest(values)),
  editItem: values =>
    dispatch(BrandVehicleManagementActions.editItemRequest(values)),
})

FormVehicleManufacturerContainer.propTypes = {
  form: PropTypes.any,
  id: PropTypes.string,
  item: PropTypes.object,
  createItem: PropTypes.func,
  editItem: PropTypes.func,
}
const FormVehicleManufacturer = Form.create()(FormVehicleManufacturerContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormVehicleManufacturer)
