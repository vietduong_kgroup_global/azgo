import React, { useEffect, useState } from 'react'
import { Form, Select,Row, Col, Modal, InputNumber, Dropdown, Button, Menu, Slider} from 'antd'
// import { Button } from 'reactstrap'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { TourManagementSelectors } from 'Stores/TourCar/TourManagement/Selectors'
import { TourManagementActions } from 'Stores/TourCar/TourManagement/Actions'
import FormatPrice from 'components/organisms/FormatPrice'

import 'Stores/Intercity/AreaManagement/Reducers'
import 'Stores/Intercity/AreaManagement/Sagas'
import { AreaManagementSelectors } from 'Stores/Intercity/AreaManagement/Selectors'
import { AreaManagementActions } from 'Stores/Intercity/AreaManagement/Actions' 
const { Option } = Select;
const FormFilterContainer = ({ 
  form,
  handleClickParent,
  filter,
  getItems,
  setFilter,
  itemsArea,
  getListItemsArea,
  setFilterArea,
  filterArea
}) => {
  const { getFieldDecorator, getFieldsValue, resetFields } = form
  const minTime = 0
  const maxTime = 10000000
  const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 20 },
  }; 
  // const arraySeat = [{name: "Xe 4 chỗ", value: 4},{name: "Xe 7 chỗ", value: 7}, {name: "Xe 16 chỗ", value: 16}]
  const [isModalVisible, setIsModalVisible] = useState(false); 
  const [numberOfSeat, onNumberOfSeatChange] = useState()
  const [area, onAreaChange] = useState([])
  const [company, onCompanyChange] = useState([])
  const [type, onTypeChange] = useState()
  const [fromPrice, onChangeFriceFrom] = useState()
  const [toPrice, onChangeToPrice] = useState()
  const [fromTime, onChangefromTime] = useState()
  const [toTime, onChangetoTime] = useState()
  // useEffect(() => {
  //   handleOk()
  // }, [numberOfSeat, area, type, fromPrice, toPrice, fromTime, toTime])
  useEffect(() => {
    console.log(filter.get('company'))
    console.log(filter.get('area'))
    // onNumberOfSeatChange(filter.get('numberOfSeat'))
    // onAreaChange(filter.get('area') ? filter.get('area').toJS() : [])
    // onCompanyChange(filter.get('company') ? filter.get('company').toJS() : [])
    // onTypeChange(filter.get('type'))
    // onChangeFriceFrom(filter.get('fromPrice'))
    // onChangeToPrice(filter.get('toPrice'))
    // onChangefromTime(filter.get('fromTime')) 
    // onChangetoTime(filter.get('toTime')) 
  }, [filter])
  const handleNumberOfSeatChange = (value) => {  
    onNumberOfSeatChange(value) 
    handleOk({numberOfSeat: value});
  }
  const handleAreaChange = (value) => { 
    onAreaChange(value) 
    handleOk({area: JSON.stringify(value)});
  }
  const handleCompanyChange = (value) => { 
    onCompanyChange(value)
    handleOk({company:  JSON.stringify(value)});
  }
  const handleTypeChange = (value) => { 
    if(!value) {
        onChangefromTime() 
        onChangetoTime() 
    }
    onTypeChange(value) 
    handleOk({type: value});
  } 
const handleChangeTime = (value) => {
  onChangefromTime(value[0])
  onChangetoTime(value[1]) 
  // handleOk({fromPrice: value[0], toPrice: value[1]})
}
const handleAfterChangeTime = (value) => {
  onChangefromTime(value[0])
  onChangetoTime(value[1]) 
  handleOk({fromTime: value[0], toTime: value[1]})
}
 
  useEffect(() => {
    getListItemsArea(filterArea)
  }, [filterArea])

  useEffect(() => {
    setFilterArea({
      per_page: 100,
      page: 1,
      keyword: '',
    })
  }, [])

  const handleFriceFrom = (value) => { 
    onChangeFriceFrom(value[0])
    onChangeToPrice(value[1]) 
    // handleOk({fromTime: value[0], toTime: value[1]})
  }
  const handleAfterFriceFrom = (value) => { 
    onChangeFriceFrom(value[0])
    onChangeToPrice(value[1]) 
    handleOk({fromPrice: value[0], toPrice: value[1]})
  }
   

  const handleOk = (value = {}) => {   
    setFilter({
      numberOfSeat: value.hasOwnProperty('numberOfSeat') ? value.numberOfSeat : numberOfSeat,
      area: value.area || JSON.stringify(area),
      company: value.company || JSON.stringify(company),
      type: value.hasOwnProperty('type') ? value.type : type,
      fromPrice: value.fromPrice || fromPrice,
      toPrice: value.toPrice || toPrice,
      fromTime: value.fromTime || fromTime,
      toTime: value.toTime || toTime,
    })
    getItems(filter)
    setIsModalVisible(false);
  }; 
  const menuTime = (
    <div className="menu-filter-price-tour">
      <Slider
        min={0}
        max= {1000}
        range
        step={10}
        defaultValue={[0, 1000]}
        onChange={(value) => handleChangeTime(value)}
        onAfterChange={handleAfterChangeTime}
      />
       <div className="menu-filter-price-tour-range">
          <div>{fromTime && <div><FormatPrice value={`${fromTime}`}/>Phút</div>}</div>
          <div>{toTime && <div><FormatPrice value={`${toTime}`}/>Phút</div>}</div>
        </div> 
    </div> 
  );
  const menuPrice = ( 
    <div className="menu-filter-price-tour">
        <Slider
          min={minTime}
          max= {maxTime}
          range
          step={10}
          defaultValue={[minTime, maxTime]}
          onChange={(value) => handleFriceFrom(value)}
          onAfterChange={handleAfterFriceFrom}
        />
        <div className="menu-filter-price-tour-range">
          <div>{fromPrice && <div><FormatPrice value={`${fromPrice}`}/>VND</div>}</div>
          <div>{toPrice && <div><FormatPrice value={`${toPrice}`}/>VND</div>}</div>
        </div> 
     {/* <Row gutter={10}>   
        <Col span={10}>
          <InputNumber
            style={{ margin: '0 16px' }}
            value={fromPrice}
            onChange={handleFriceFrom}
          />
        </Col> 
        <Col span={1}>-</Col> 
        <Col span={10}>
          <InputNumber
            style={{ margin: '0 16px' }}
            value={toPrice}
            onChange={onChangeToPrice}
          />
        </Col>
      </Row>  */}
    </div>
  );
  console.log(company) 
  return (
    <>
        <>
        <Form {...layout} className="" name="control-ref"  >
        <Row gutter={16} >
          <Col className="gutter-row" xs={{span: 24}} sm={{span: 12}} md={{span: 3}} lg={{span: 3}} xxl={{span: 3}}>
            <Select allowClear  mode="tags" style={{ width: '100%' }} placeholder="Công ty" onChange={handleCompanyChange}>
                {company.map((e, i) => {
                  return( 
                  <Option key = {i} value={e}>{e}</Option> 
                  )
                })
                }
            </Select>
          </Col>
          <Col className="gutter-row" xs={{span: 24}} sm={{span: 12}} md={{span: 5}} lg={{span: 5}} xxl={{span: 5}}>
            <Select
                  style={{width: '100%'}}
                  mode="multiple"
                  placeholder="Chọn nhiều khu vực"
                  onChange={handleAreaChange}
                  allowClear
                  value={area}
                >
                  {itemsArea.toJS().map((e) => {
                    return(
                    <Option value={e.uuid}>{e.name}</Option> 
                    )
                  })
                  }
              </Select>
          </Col>
          <Col className="gutter-row" xs={{span: 24}} sm={{span: 12}} md={{span: 3}} lg={{span: 3}} xxl={{span: 3}}>
            <Select style={{width: '100%'}}
                  placeholder="Loại tour"
                  onChange={handleTypeChange}
                  allowClear
                  value={type}
                >
                  <Option value={'fix_price'}>Giá cố định</Option>
                  <Option value={'distance_price'}>Giá theo km</Option>
                  <Option value={'time_price'}>Giá theo ngày</Option>
            </Select>
          </Col>
          <Col className="gutter-row" xs={{span: 24}} sm={{span: 12}} md={{span: 4}} lg={{span: 4}} xxl={{span: 4}}>
            <Dropdown  disabled = {type === 'fix_price' ? false : true} overlay={menuTime} placement="bottomCenter" arrow>
              <Button  style={{width: '100%'}}>Thời gian {fromTime >= 0 && toTime >= 0 && `${fromTime} - ${toTime}`}</Button>
            </Dropdown>
          </Col>
          <Col className="gutter-row" xs={{span: 24}} sm={{span: 12}} md={{span: 3}} lg={{span: 3}} xxl={{span: 3}}>
            <Select
                style={{width: '100%'}}
                placeholder="Số chỗ"
                onChange={handleNumberOfSeatChange}
                allowClear
                value={numberOfSeat}
              >
                <Option value={4}>Xe 4 chỗ</Option>
                <Option value={7}>Xe 7 chỗ</Option>
                <Option value={16}>Xe 16 chỗ</Option>
              </Select> 
          </Col>
          <Col className="gutter-row" xs={{span: 24}} sm={{span: 12}} md={{span: 6}} lg={{span: 6}} xxl={{span: 6}}>
            <Dropdown overlay={menuPrice} placement="bottomCenter" arrow>
              <Button  style={{width: '100%'}}><div>Giá tour {fromPrice >=0 && toPrice >0 && (<span><FormatPrice value={fromPrice}/>-<FormatPrice value={toPrice}/></span>)}</div></Button>
            </Dropdown>
          </Col>
        </Row>

            {/* <Form.Item name="numberOfSeat" label="Số chỗ" rules={[{ required: true }]}>
              <Select
                placeholder="Chọn 1 mục"
                onChange={onNumberOfSeatChange}
                allowClear
                value={numberOfSeat}
              >
                <Option value={4}>Xe 4 chỗ</Option>
                <Option value={7}>Xe 7 chỗ</Option>
                <Option value={16}>Xe 16 chỗ</Option>
              </Select> 
            </Form.Item> 
            <Form.Item name="area" label="Khu vực" rules={[{ required: true }]}>
            <Row gutter={10}>  
              <Col span={21}> 
              <Select
                mode="multiple"
                placeholder="Chọn nhiều khu vực"
                onChange={onAreaChange}
                allowClear
                value={area}
              >
                {itemsArea.toJS().map((e) => {
                  return(
                  <Option value={e.uuid}>{e.name}</Option> 
                  )
                })
                }
              </Select>
              </Col>
            </Row> 
            </Form.Item> 
            <Form.Item name="type" label="Loại tour" rules={[{ required: true }]}>
            <Row gutter={10}>  
              <Col span={21}> 
              <Select
                placeholder="Chọn 1 mục"
                onChange={onTypeChange}
                allowClear
                value={type}
              >
                <Option value={'fix_price'}>Giá cố định</Option>
                <Option value={'distance_price'}>Giá theo km</Option>
                <Option value={'time_price'}>Giá theo ngày</Option>
              </Select>
              </Col>
            </Row> 
            </Form.Item>
            <Form.Item name="price" label="Giá" >
            <Row gutter={10}>   
              <Col span={10}>
                <InputNumber
                  style={{ margin: '0 16px' }}
                  value={fromPrice}
                  onChange={handleFriceFrom}
                />
              </Col> 
              <Col span={1}>-</Col> 
              <Col span={10}>
                <InputNumber
                  style={{ margin: '0 16px' }}
                  value={toPrice}
                  onChange={onChangeToPrice}
                />
              </Col>
            </Row>
            </Form.Item> 
            <Form.Item label="Thời gian" >
            <Row gutter={10}>   
              <Col span={10}>
              <InputNumber
                  style={{ margin: '0 16px' }}
                  value={fromTime}
                  onChange={onChangefromTime}
                />  
              </Col> 
              <Col span={1}>-</Col> 
              <Col span={10}>
              <InputNumber
                  style={{ margin: '0 16px' }}
                  value={toTime}
                  onChange={onChangetoTime}
              ></InputNumber>
              </Col>
            </Row>
            </Form.Item>  */}
          </Form> 
        </>  
    </>
  
  )
}

const mapStateToProps = state => ({
  filter: TourManagementSelectors.getFilter(state),
  itemsArea: AreaManagementSelectors.getItems(state),
  filterArea: AreaManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  getItems: filter => dispatch(TourManagementActions.getItemsRequest(filter)),
  setFilter: filter => dispatch(TourManagementActions.setFilter(filter)),
  setFilterArea: filter => dispatch(AreaManagementActions.setFilter(filter)),
  getListItemsArea: values =>
    dispatch(AreaManagementActions.getItemsRequest(values)),
})

FormFilterContainer.propTypes = {
  form: PropTypes.any,
  handleClickParent: PropTypes.func,
}
const FormFilter = Form.create()(FormFilterContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormFilter)
