import React, { useEffect, useState } from 'react'
import { Form, Input, Button, Select } from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'

import 'Stores/Intercity/AreaManagement/Reducers'
import 'Stores/Intercity/AreaManagement/Sagas'
import { AreaManagementActions } from 'Stores/Intercity/AreaManagement/Actions'
import { AreaManagementSelectors } from 'Stores/Intercity/AreaManagement/Selectors'

import 'Stores/InternalManagement/Reducers'
import 'Stores/InternalManagement/Sagas'
import { InternalManagementActions } from 'Stores/InternalManagement/Actions'

import UploadImage from 'components/molecules/UploadImg'
import './styles.scss'
import * as config from 'Utils/enum'
import { _parsePhoneNumber } from 'Utils/helper'

const FormInternalAccountContainer = ({
  form,
  id,
  item,
  createItem,
  editItem,
  itemsArea,
  getListItemsArea,
  setFilterArea,
  filterArea,
  changeBlockUser,
}) => {
  const { getFieldDecorator, getFieldsValue, setFieldsValue } = form;
  const { Option } = Select;
  const [imageUrl, setImgUrl] = useState('');

  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      let fieldsValue = {...getFieldsValue()}
      if (id) {
        editItem({
          ...fieldsValue,
          areaUuid: fieldsValue.areaUuid || null,
          type: fieldsValue.type,
          role: fieldsValue.type,
          id,
          status: item.get('status'),
          changeBlockUser: changeBlockUser,
        })
      } else {
        createItem({
          ...fieldsValue,
          areaUuid: fieldsValue.areaUuid || null,
          type: fieldsValue.type,
          role: fieldsValue.type,
          roles: [config.MANAGEMENT_TYPE_USER.ADMIN],
         })
      }
    })
  }  
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  }
  const compareToFirstPassword = (rule, value, callback) => {
    if (value && value !== form.getFieldValue('password')) {
      callback('Mật khẩu không trùng khớp!')
    } else {
      callback()
    }
  }
  const normFile = e => {
    if (Array.isArray(e)) {
      return e
    }
    return e && e.fileList
  }
  const callbackHandleImageProfile = value => { 
    if(!value) {
      setImgUrl(null) 
      setFieldsValue({
        imageProfile: null,
      })
    }
    else {
      setImgUrl(value.file.response.data[0].address)
      // set lai state imageProfile
      let imageProfile = {
        address: value.file.response.data[0].address,
        filePath: value.file.response.data[0].file_path,
        fileName: value.file.response.data[0].file_name,
      }
      setFieldsValue({
        imageProfile: imageProfile,
      })
    }
  }
  const prefixSelector = getFieldDecorator('countryCode', {
    initialValue: '84',
  })(
    <Select style={{ width: 70 }}>
      <Option value="84">+84</Option>
    </Select>
  )
  const validatePhone = (rule, value, callback) => {
    let phone = form.getFieldValue('phone')
    let countryCode = form.getFieldValue('countryCode')
    if (phone && countryCode) {
      let result = _parsePhoneNumber(countryCode, phone)
      if (result && result.isValid) callback()
      else callback('Số điện thoại không hợp lệ')
    } else {
      callback()
    }
  }
  useEffect(() => {
    let filePath = item.getIn(['adminProfile', 'imageProfile', 'filePath'])
    let fileName = item.getIn(['adminProfile', 'imageProfile', 'fileName'])
    let imagePath = ''
    if (filePath && fileName) {
      imagePath = filePath + '/' + fileName
    }
    let imageUrl = ''
    if (imagePath !== '') {
      imageUrl = item.getIn(['adminProfile', 'imageProfile', 'address'])
      setImgUrl(imageUrl)
    } else {
      setImgUrl('')
    }
    //Set rolefor user
    if(!!item.get("type")){
      setFieldsValue({
        type: item.get("type"), // lấy roles đầu tiên trong ảng roles
      })
    }
    else {
      setFieldsValue({
        type: config.ACCOUNT_INITIAL_ROLE.ADMIN
      })
    }
  }, [item])
  useEffect(() => {
    getListItemsArea(filterArea)
  }, [filterArea])
  useEffect(() => {
    setFilterArea({
      per_page: 100,
      page: 1,
      keyword: '',
    })
  }, [])
  return (
    <>
      <Form
        id="frmInternalAccount"
        {...formItemLayout}
        className="input-floating form-ant-custom">
        <Form.Item label="E-mail">
          {getFieldDecorator('email', {
            rules: [
              {
                pattern: new RegExp("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"),
                message: 'Email không đúng định dạng!',
              },
              {
                max: 50,
                message: 'Trường không hơn lớn 50 ký tự',
              },
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.get('email'),
          })(<Input disabled={!id ? false : true} />)}
        </Form.Item>
        {!id ? (
          <Form.Item label="Mật khẩu" hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Trường bắt buộc!',
                },
              ],
            })(<Input.Password />)}
          </Form.Item>
        ) : ""}
        {!id ? (
          <Form.Item label="Xác nhận mật khẩu" hasFeedback>
            {getFieldDecorator('confirm', {
              rules: [
                {
                  validator: compareToFirstPassword,
                },
                {
                  required: true,
                  message: 'Trường bắt buộc!',
                },
              ],
            })(<Input.Password onPaste={e => e.preventDefault()} />)}
          </Form.Item>
        ) : ""}
        <Form.Item label="Họ">
          {getFieldDecorator('lastName', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                min: 1,
                message: 'Trường không được ít hơn 1 ký tự!',
              },
              {
                max: 10,
                message: 'Trường không được quá 10 ký tự!',
              },
            ],
            initialValue: id && item.getIn(['adminProfile', 'lastName']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Tên">
          {getFieldDecorator('firstName', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
              {
                min: 1,
                message: 'Trường không được ít hơn 1 ký tự!',
              },
              {
                max: 30,
                message: 'Trường không được quá 30 ký tự!',
              },
            ],
            initialValue: id && item.getIn(['adminProfile', 'firstName']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Số điện thoại">
          {
          getFieldDecorator('phone', {
            rules: [
              {
                validator: validatePhone,
              },
            ],
            initialValue: id && item.getIn(['adminProfile', 'phone']), 
          })
          (<Input type="number"  addonBefore={prefixSelector} style={{ width: '100%' }} />)}
        </Form.Item>
        <Form.Item label="Giới tính">
          {getFieldDecorator('gender', {
            initialValue: id && item.getIn(['adminProfile', 'gender']),
          })(
            <Select placeholder="Chọn">
              {config.MANAGEMENT_GENDER_ARR.map(item => (
                <Select.Option value={item.key} key={`gender_${item.key}`}>
                  {item.label}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Company">
          {getFieldDecorator('company', { 
             rules: [
              {
                pattern: new RegExp("^[a-z0-9]+$"), 
                message: 'Chỉ nhập ký tự thường hoặc số và không nhập khoảng trắng', 
              }, 
            ],
            initialValue: id && item.getIn(['adminProfile','company']),
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Khu vực">
          {getFieldDecorator('areaUuid', {
            rules: [
              {
                required: getFieldsValue().type === config.ACCOUNT_INITIAL_ROLE.REGION_MANAGER,
                message: 'Trường bắt buộc!',
              },
            ],
            initialValue: id && item.getIn(['adminProfile','areaUuid']),
          })(
            <Select
              placeholder="Vui lòng chọn khu vực"
              allowClear
            >
              <Select.Option
                  value={null}
                  key={`area_uuid_no_set`}>
                  Chọn khu vực
              </Select.Option>
              {itemsArea.toJS().map(item => (
                <Select.Option
                  value={item.uuid}
                  key={`area_uuid_${item.uuid}`}>
                  {item.name}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Quyền">
          {getFieldDecorator('type', {
            rules: [{ required: true, message: 'Trường bắt buộc!' }],
            initialValue:
              id ? item.get('type')
              : config.ACCOUNT_INITIAL_ROLE.ADMIN,
          })(
            <Select placeholder="Chọn">
              {config.ACCOUNT_INITIAL_ROLE_ARR.map(item => (
                <Select.Option value={item.key} key={`gender_${item.key}`}>
                  {item.label}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Ảnh đại diện" className="ant-form-item-load">
          {getFieldDecorator('upload', {
            valuePropName: 'fileList',
            getValueFromEvent: normFile,
          })(
            <>
              <UploadImage
                imageProp={imageUrl}
                handleParent={callbackHandleImageProfile} 
                type_image_props='internal_account'
                fileNameProp={item.getIn(['adminProfile', 'imageProfile', 'fileName'])}
              />
            </>
          )}
        </Form.Item>
        <Form.Item hidden>
          {getFieldDecorator('imageProfile')(<Input />)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Xác nhận
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({
  itemsArea: AreaManagementSelectors.getItems(state),
  filterArea: AreaManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(InternalManagementActions.createItemRequest(values)),
  editItem: values =>
    dispatch(InternalManagementActions.editItemRequest(values)),
  setFilterArea: filter => dispatch(AreaManagementActions.setFilter(filter)),
  getListItemsArea: values =>
    dispatch(AreaManagementActions.getItemsRequest(values)),
})

FormInternalAccountContainer.propTypes = {
  form: PropTypes.any,
  id: PropTypes.string,
  item: PropTypes.object,
  createItem: PropTypes.func,
  editItem: PropTypes.func,
  changeBlockUser: PropTypes.any,
}

const FormInternalAccount = Form.create()(FormInternalAccountContainer)
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormInternalAccount)
