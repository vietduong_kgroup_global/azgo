import React, { useEffect } from 'react'
import { Form, Button, DatePicker } from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import moment from 'moment'

const FormFilterContainer = ({ form, handleClickParent }) => {
  const { getFieldDecorator, getFieldsValue, resetFields } = form
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      } else {
        handleClickParent(
          moment(form.getFieldsValue().fromDate).format('DD/MM/YYYY'),
          moment(form.getFieldsValue().endDate).format('DD/MM/YYYY')
        )
      }
    })
  }
  const handleReset = () => {
    handleClickParent(null, null)
    resetFields()
  }
  const dateFormat = ['DD/MM/YYYY']
  return (
    <>
      <Form
        id="frmFilter"
        className="input-floating"
        layout="inline"
        style={{ marginBottom: '20px' }}>
        <Form.Item label="Từ ngày">
          {getFieldDecorator('fromDate', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(<DatePicker format={dateFormat} placeholder="Chọn" />)}
        </Form.Item>
        <Form.Item label="đến ngày">
          {getFieldDecorator('endDate', {
            rules: [
              {
                required: true,
                message: 'Trường bắt buộc!',
              },
            ],
          })(<DatePicker format={dateFormat} placeholder="Chọn" />)}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Xác nhận
          </Button>
        </Form.Item>
        <Form.Item>
          <Button htmlType="submit" onClick={handleReset}>
            Reset
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

FormFilterContainer.propTypes = {
  form: PropTypes.any,
  handleClickParent: PropTypes.func,
}
const FormFilter = Form.create()(FormFilterContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormFilter)
