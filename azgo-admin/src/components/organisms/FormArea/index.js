import React, { Component, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import Autocomplete from 'react-google-autocomplete'
import { Radio, Form, Input } from 'antd'
import { Button, Row, Col } from 'reactstrap'
import { connect } from 'react-redux'
import styles from './styles.module.scss'
import 'Stores/Intercity/AreaManagement/Reducers'
import 'Stores/Intercity/AreaManagement/Sagas'
import { AreaManagementActions } from 'Stores/Intercity/AreaManagement/Actions'
import { AreaManagementSelectors } from 'Stores/Intercity/AreaManagement/Selectors'
import UploadImage from 'components/molecules/UploadImg'

const Area = ({ createItem, visible, form, item, editItem, id }) => {
  const { getFieldDecorator, getFieldsValue, resetFields, setFieldsValue } = form
  const [imageAvatar, setImageAvatar] = useState('')
  // const [location, setLocation] = useState(null)
  const [valueTypeArea, setValueTypeArea] = useState(1)
  // const initMap = () => {
  //   const google = window.google
  //   var map = new google.maps.Map(document.getElementById('map'), {
  //     center: location,
  //     zoom: 8,
  //   })
  //   if (location) {
  //     map.setCenter(location)
  //   } else {
  //     map.setCenter({
  //       lat: 10.8230989,
  //       lng: 106.6296638,
  //     })
  //   }
  //   var marker = new google.maps.Marker({
  //     map: map,
  //     anchorPoint: new google.maps.Point(0, -29),
  //   })
  //   marker.setPosition(location)
  //   marker.setVisible(true)
  // }
  // useEffect(() => {
  //   initMap()
  // }, [location])
  const imageLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 24,
        offset: 0,
        // push: 2
      },
    },
  }
  useEffect(() => {
    if (visible === false) {
      // setLocation(null)
      resetFields()
    }
  }, [visible])
  useEffect(() => {
    if (item) {
      setValueTypeArea(item.get('type')) 
      setImageAvatar(item.getIn(['image', 'address'])) 
      // setLocation({
      //   lat: item.coordinates.lat,
      //   lng: item.coordinates.long,
      // })
    }
  }, [item])
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      if (id) {
        editItem({
              ...getFieldsValue(),
              uuid: id,
              // coordinates: {
              //   lat: getFieldsValue().lat,
              //   long: getFieldsValue().long,
              // },
        })
      } else {
        createItem({
          ...getFieldsValue(),
          // coordinates: {
          //   lat: getFieldsValue().lat,
          //   long: getFieldsValue().long,
          // },
        })
      }
    })
  }
  const onChangeRadioTypeArea = e => {
    setValueTypeArea(e.target.value)
  }
  const callbackHandleAvatar = value => {
    if(!value) {
      setImageAvatar(null) 
      setFieldsValue({
        image: null,
      })
    }
    else {
      setImageAvatar(value.file.response.data[0].address)
      // set lai state imageProfile
      let imageProfile = {
        address: value.file.response.data[0].address,
        filePath: value.file.response.data[0].file_path,
        fileName: value.file.response.data[0].file_name,
      }
      setFieldsValue({
        image: imageProfile,
      })
    }  
  }
  return (
    <>
      <Form id="frmArea">
        <Row>
          <Col col="6">
            <Form.Item>
              {getFieldDecorator('type', {
                initialValue: valueTypeArea,
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
              })(
                <Radio.Group onChange={e => onChangeRadioTypeArea(e)}>
                  <Radio value={1}>Tỉnh/thành phố</Radio>
                  <Radio value={2}>Quận/huyện/thành phố</Radio>
                </Radio.Group>
              )}
            </Form.Item>
            <Form.Item label="Tên địa điểm">
              {getFieldDecorator('name', {
                initialValue: id && item.get('name'),
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Ảnh đại diện" className="ant-form-item-load"  {...imageLayout}>
              <Row>
              <Col span={8} sm={4}>
                <UploadImage
                  imageProp={imageAvatar}
                  handleParent={callbackHandleAvatar}
                  // setImgUrl={setImageAvatar}
                  type_image_props='area'
                  fileNameProp={item && item.toJS() && item.toJS().image && item.toJS().image.fileName}
                />
                </Col>
              </Row>
            </Form.Item>
            <Form.Item hidden>
              {getFieldDecorator('image')(<Input />)}
            </Form.Item>
            {/* <div className="mb-2">
              <div>Tìm tọa độ</div>
              {visible && (
                <Autocomplete
                  style={{ width: '100%' }}
                  types={[]}
                  onPlaceSelected={place => {
                    if (place) {
                      setLocation({
                        lat: place.geometry.location.lat(),
                        lng: place.geometry.location.lng(),
                      })
                    }
                  }}
                  componentRestrictions={{ country: 'vn' }}
                />
              )}
            </div> */}
            {/* <Row>
              <Col col="6">
                <Form.Item label="Lat">
                  {getFieldDecorator('lat', {
                    initialValue: location && location.lat,
                    rules: [
                      {
                        required: true,
                        message: 'Trường bắt buộc!',
                      },
                    ],
                  })(<Input />)}
                </Form.Item>
              </Col>
              <Col col="6">
                <Form.Item label="Long">
                  {getFieldDecorator('long', {
                    initialValue: location && location.lng,
                    rules: [
                      {
                        required: true,
                        message: 'Trường bắt buộc!',
                      },
                    ],
                  })(<Input />)}
                </Form.Item>
              </Col>
            </Row> */}
          </Col>
          {/* <Col col="6">
            <div id="map" className={styles.mapArea} />
          </Col> */}
        </Row>
        <Form.Item>
          <Button color="primary" onClick={() => handleSubmit()}>
            Xác nhận
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}

Area.propTypes = {
  createItem: PropTypes.func,
  visible: PropTypes.bool,
  form: PropTypes.any,
  item: PropTypes.object,
  editItem: PropTypes.func,
}

const mapStateToProps = state => ({
  getItemsArea: AreaManagementSelectors.getItems(state),
})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(AreaManagementActions.createItemRequest(values)),
  editItem: values => dispatch(AreaManagementActions.editItemRequest(values)),
})

const FormArea = Form.create()(Area)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(FormArea)
