import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Table } from 'antd'

const TableList = ({
  columns,
  dataSource,
  showLoading,
  pagination,
  handleTableChange,
  totalCount,
  onRow,
  expandedRowRender
}) => {
  const [listData, setListData] = useState([])
  useEffect(() => {
    if (dataSource && dataSource.size > 0) {
      setListData([])
      let arr = []
      for (let i = 0; i < dataSource.toJS().length; i++) {
        let item = {
          ...dataSource.toJS()[i],
          key: i + 1,
        }
        arr.push(item)
      }
      setListData(arr)
    } else {
      setListData([])
    }
  }, [dataSource])
  return (
    <>
      <Table
        onRow={onRow}
        columns={columns}
        rowKey="key"
        dataSource={listData.length > 0 ? listData : null}
        pagination={pagination}
        loading={showLoading}
        onChange={handleTableChange}
        footer={() =>
          totalCount && totalCount !== 0
            ? 'Hiển thị ' + dataSource.size + '/' + totalCount
            : '0'
        }
        expandedRowRender={expandedRowRender}
        locale={{ emptyText: 'Không có kết quả tìm kiếm' }}
      />
    </>
  )
}

TableList.propTypes = {
  columns: PropTypes.array,
  dataSource: PropTypes.any,
  showLoading: PropTypes.bool,
  pagination: PropTypes.object,
  handleTableChange: PropTypes.func,
  onRow :  PropTypes.func,
  totalCount: PropTypes.number,
}

export default TableList
