import React from 'react'
import PropTypes from 'prop-types'

const TitlePage = ({ data, name, id }) => {
  return (
    <>
      <h1 className="title-page">
        {data}
        <span style={{ color: '#20a8d8' }}> {name}</span>
      </h1>
    </>
  )
}

TitlePage.propTypes = {
  data: PropTypes.string,
  name: PropTypes.any,
  id: PropTypes.string,
}

export default TitlePage
