import React, { useEffect, useState } from 'react'
import { Upload, message, Icon } from 'antd'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { compose } from 'redux'

const UploadImage = () => {
  const state = {
    loading: false,
  }
  const [loading, setloading] = useState(false)
  const { imageUrl } = state
  const uploadButton = (
    <div>
      <Icon type={loading ? 'loading' : 'plus'} />
      <div className="ant-upload-text">Upload</div>
    </div>
  )

  return (
    <>
      <Upload
        name="avatar"
        listType="picture-card"
        className="avatar-uploader"
        showUploadList={false}
        action="https://www.mocky.io/v2/5cc8019d300000980a055e76">
        {imageUrl ? (
          <img src={imageUrl} alt="avatar" style={{ width: '100%' }} />
        ) : (
          uploadButton
        )}
      </Upload>
    </>
  )
}

UploadImage.propTypes = {}

export default UploadImage
