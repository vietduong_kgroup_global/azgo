import React, { Component, Suspense, useEffect } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import * as router from 'react-router-dom'
import { Container } from 'reactstrap'
import routes from 'routes'
import PropTypes from 'prop-types'
import {
  getTokenData,
  getPermissions,
  removePermissions,
  removeToken,
} from 'Utils/token'
import 'Stores/User/Reducers'
import * as config from 'Utils/enum'
import {
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react'

// sidebar nav config
import navigation from './_nav'
import navigationRegionManager from './_navRegionManager'
import navigationAccountant from './_navAccountant'
import navigationOperator from './_navOperator'
 
import navigationCompany from './_navCompany'
// routes config
import { changeLocation } from 'Utils/changeLocation'
import moment from 'moment'
import { UserSelectors } from 'Stores/User/Selectors'
import { connect } from 'react-redux'
import { compose } from 'redux' 
import * as filePackage from '../../../../package.json'
const providerRoutes = routes.filter(
  routes => routes.role === config.MANAGEMENT_TYPE_USER.PROVIDER
)
const DefaultHeader = React.lazy(() => import('./DefaultHeader'))
class DefaultLayout extends Component {
  constructor(props) {
    super(props)
    this.state = {
      roles: null,
      tokenUser: null,
    }
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  )

  signOut(e) {
    e.preventDefault()
    this.props.history.push('/login')
  }

  componentWillMount() {
    // Check if token exist
    let validToken = false
    let token = getTokenData()
    if (token) {
      let endDate = moment.unix(token.exp)
      if (moment().isBefore(endDate)) {
        validToken = true
      } 
      this.setState({
        tokenUser: token,
      })
    }
    if (!validToken) {
      changeLocation('/login')
    }
    this.fetch_info()
  }
  fetch_info() {
    setTimeout(() => {
      if (localStorage.length > 0) {
        this.setState({
          roles: JSON.parse(localStorage.getItem('permissions')),
        })
      }
    }, 500)
  }; 
  render() {
  let {userInfo} = this.props 
  return (
      <div className="app">
        {this.state.roles !== null && (
          <>
            <AppHeader fixed>
              <DefaultHeader
                onLogout={e => this.signOut(e)}
                userId={this.state.tokenUser}
              />
            </AppHeader>
            <div className="app-body">
              <AppSidebar fixed display="lg">
                <AppSidebarHeader />
                <AppSidebarForm />
                <Suspense>
                  {(userInfo && userInfo.get('adminProfile') && userInfo.get('adminProfile').toJS().company) ?
                   <AppSidebarNav
                    navConfig={navigationCompany}
                    {...this.props}
                    router={router}
                  />
                  : 
                  (userInfo && userInfo.get('type') && userInfo.get('type') === config.MANAGEMENT_TYPE_USER.REGION_MANAGER) ?
                    <AppSidebarNav
                      navConfig={navigationRegionManager}
                      {...this.props}
                      router={router}
                    />
                  :
                    (userInfo && userInfo.get('type') && userInfo.get('type')===9) ?
                    <AppSidebarNav
                      navConfig={navigationAccountant}
                      {...this.props}
                      router={router}
                    />
                  :
                    (userInfo && userInfo.get('type') && userInfo.get('type')===10) ?
                    <AppSidebarNav
                      navConfig={navigationOperator}
                      {...this.props}
                      router={router}
                    />
                  :  
                    <AppSidebarNav
                      navConfig={navigation}
                      {...this.props}
                      router={router}
                    /> 
                  } 
                 
                     <div className="sidebar_version">Admin version {filePackage && filePackage.version}</div>
                    <div className="sidebar_version">API version {this.props.apiVersion}</div>
                </Suspense>
                <AppSidebarFooter />
                <AppSidebarMinimizer />
              </AppSidebar>
              <main className="main">
                <Container fluid>
                  <Suspense fallback={this.loading()}>
                    <Switch>
                      {(userInfo && userInfo.get('adminProfile') && userInfo.get('adminProfile').toJS().company) ?
                      providerRoutes.map((route, idx) => {
                        return (
                          <Route
                            key={idx}
                            path={route.path}
                            exact={route.exact}
                            name={route.name}
                            render={props => <route.component {...props} />}
                          />
                        )
                      })
                      :this.state.roles === config.MANAGEMENT_TYPE_USER.PROVIDER
                        ? providerRoutes.map((route, idx) => {
                            return (
                              <Route
                                key={idx}
                                path={route.path}
                                exact={route.exact}
                                name={route.name}
                                render={props => <route.component {...props} />}
                              />
                            )
                          })
                        : routes.map((route, idx) => {
                            return (
                              <Route
                                key={idx}
                                path={route.path}
                                exact={route.exact}
                                name={route.name}
                                render={props => <route.component {...props} />}
                              />
                            )
                          })}
                      {this.state.roles ===
                        config.MANAGEMENT_TYPE_USER.PROVIDER && (
                        <Route exact path="/">
                          <Redirect to="/dashboard-provider" />
                        </Route>
                      )}
                      <Route path="*">
                        <Redirect to="/404" />
                      </Route>
                    </Switch>
                  </Suspense>
                </Container>
              </main>
            </div>
          </>
        )}
      </div>
    )
  }
}

DefaultLayout.propTypes = {
  history: PropTypes.object,
  logout: PropTypes.func,
}
 

const mapStateToProps = state => ({ 
  userInfo: UserSelectors.getUserData(state),
  apiVersion: UserSelectors.getAPIVersion(state),
})

const mapDispatchToProps = dispatch => ({ 
  // getAPIVersion: id => dispatch(UserActions.fetchApiVersionInfo()), 
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect)(DefaultLayout)
