import React, { Component, useEffect, useState } from 'react'
import { Link, NavLink, withRouter } from 'react-router-dom'
import * as router from 'react-router-dom'
import {
  UncontrolledDropdown,
  DropdownToggle,
  Nav,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap'
import PropTypes from 'prop-types'

import {
  AppBreadcrumb2 as AppBreadcrumb,
  AppNavbarBrand,
  AppSidebarToggler,
} from '@coreui/react'
import logo from 'assets/img/brand/logo.svg'
import sygnet from 'assets/img/brand/sygnet.svg'
import routes from 'routes'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { UserSelectors } from 'Stores/User/Selectors'
import { UserActions } from 'Stores/User/Actions'
import { Config } from 'Config'
import * as config from 'Utils/enum'

const DefaultHeader = ({ item, userId, getItem, getAPIVersion, apiVersion, history }) => {
  const [imgAvatar, setImgAvatar] = useState('')
  useEffect(() => {
    getAPIVersion()
    if (userId) {
      getItem(userId.user_id)
    }
  }, [userId])
  const handleOpenDetail = () => {
    if (item.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER) {
      history.push({
        pathname: '/provider-detail',
      })
    } else {
      history.push({
        pathname: `/internal-management/detail/${item.get('user_id')}`,
      })
    }
  }
  useEffect(() => {
    if (item.getIn(['adminProfile', 'image_profile'])) {
      let img =
        Config.DEV_URL.IMAGE +
        '/' +
        item.getIn(['adminProfile', 'image_profile', 'file_path']) +
        '/' +
        item.getIn(['adminProfile', 'image_profile', 'file_name'])
      setImgAvatar(img)
    }
    if (item.getIn(['customerProfile', 'image_profile'])) {
      let img =
        Config.DEV_URL.IMAGE +
        '/' +
        item.getIn(['customerProfile', 'image_profile', 'file_path']) +
        '/' +
        item.getIn(['customerProfile', 'image_profile', 'file_name'])
      setImgAvatar(img)
    }
    if (item.getIn(['providersInfo', 'image_profile'])) {
      let img =
        Config.DEV_URL.IMAGE +
        '/' +
        item.getIn(['providersInfo', 'image_profile', 'file_path']) +
        '/' +
        item.getIn(['providersInfo', 'image_profile', 'file_name'])
      setImgAvatar(img)
    }
  }, [item]) 
  return (
    <>
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: 'Azgo Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'Azgo Logo' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />
        <AppBreadcrumb appRoutes={routes} router={router} />
        <Nav className="ml-auto" navbar>
          {item.getIn(['customerProfile', 'first_name'])}
          {item.getIn(['adminProfile', 'first_name'])}
          {item.getIn(['providersInfo', 'provider_name'])}
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              {imgAvatar && <img src={imgAvatar} className="img-avatar" />}
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem>
                <i className="fa fa-user" />{' '}
                <span onClick={() => handleOpenDetail()}>
                  Thông tin tài khoản
                </span>
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-lock" /> <a href="/logout">Đăng xuất</a>
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
      </React.Fragment>
    </>
  )
}

DefaultHeader.propTypes = {
  item: PropTypes.object,
  userId: PropTypes.any,
  getItem: PropTypes.func,
  history: PropTypes.any,
}

const mapStateToProps = state => ({
  item: UserSelectors.getUserData(state),
  // apiVersion: UserSelectors.getAPIVersion(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(UserActions.fetchUserInfo(id)),
  getAPIVersion: id => dispatch(UserActions.fetchApiVersionInfo()), 
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(DefaultHeader)
