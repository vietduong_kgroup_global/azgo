import logo from 'assets/img/icon_tour.svg'
export default {
  items: [
    {
      name: 'Tài khoản',
      icon: 'fa fa-user',
      children: [
        {
          name: 'Tài xế xe',
          url: '/admin/driver',
        },
        {
          name: 'Quản lý ví',
          url: '/admin/wallet',
        }, 
      ],
    },
    {
      name: 'Phương tiện',
      icon: 'fa fa-car',
      children: [
        {
          name: 'Quản lý xe',
          url: '/admin/vehicle/vehicle',
        },
        {
          name: 'Quản lý loại xe',
          url: '/admin/vehicle/vehicle-type',
        },
        {
          name: 'Quản lý hãng xe',
          url: '/admin/vehicle/vehicle-manufacturer',
        },
        // {
        //   name: 'Quản lý chuyến xe',
        //   url: '/admin/bike-car/trip-management',
        // },
      ],
    },
    {
      name: 'Tour',
      icon:  { img: { src: logo} },
      children: [
        {
          name: 'Quản lý tour',
          url: '/admin/tour/tourManagement',
        },
        {
          name: 'Quản lý đơn hàng',
          url: '/admin/tour/orderManagement',
        },
        {
          name: 'Lịch sử đơn hàng',
          url: '/admin/tour/orderTourLog',
        },
        {
          name: 'Quản lý khu vực',
          url: '/admin/tour/areaManagement',
        },
      ],
    },
    {
      name: 'Version',
      url: '/admin/version',
      icon: 'fa fa-sign-out',
    },
    {
      name: 'Đăng xuất',
      url: '/logout',
      icon: 'fa fa-sign-out',
    },
  ],
}
