import logo from 'assets/img/icon_tour.svg'
export default {
  items: [
    {
      name: 'Tour',
      icon:  { img: { src: logo} },
      children: [
        // {
        //   name: 'Quản lý tour',
        //   url: '/admin/tour/tourManagement',
        // },
        {
          name: 'Quản lý đơn hàng',
          url: '/admin/tour/orderManagement',
        },
        {
          name: 'Lịch sử đơn hàng',
          url: '/admin/tour/orderTourLog',
        }, 
      ],
    },
    {
      name: 'Đăng xuất',
      url: '/logout',
      icon: 'fa fa-sign-out',
    },
  ]
}
