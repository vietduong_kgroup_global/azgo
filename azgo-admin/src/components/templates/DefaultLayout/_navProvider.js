export default {
  items: [
    {
      name: 'Dashboard',
      url: '/admin/dashboard-provider',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
      },
    },
    {
      name: 'Thông tin chi tiết',
      url: '/admin/provider-detail',
      icon: 'fa fa-vcard',
    },
    {
      name: 'Danh sách Tài xế',
      url: '/admin/driver',
    },
    {
      name: 'Danh sách tài xế',
      url: '/admin/driver/bus',
      icon: 'fa fa-user-o',
    },
    {
      name: 'Quản lý tuyến',
      url: '/admin/bus/route',
      icon: 'fa fa-vcard',
    },
    {
      name: 'Quản lý loại xe',
      url: '/admin/bus/vehicle-type',
      icon: 'fa fa-sitemap',
    },
    {
      name: 'Quản lý xe',
      url: '/admin/bus/vehicle',
      icon: 'fa fa-bus',
    },
    {
      name: 'Quản lý chi phí',
      url: '/admin/bus/budget',
      icon: 'fa fa-dollar',
    },
    {
      name: 'Quản lý mẫu ghế',
      url: '/admin/bus/seat-map',
      icon: 'fa fa-th',
    },
    {
      name: 'Xếp lịch chạy',
      url: '/bus/schedule',
      icon: 'fa fa-retweet',
    },
    {
      name: 'Đăng xuất',
      url: '/logout',
      icon: 'fa fa-sign-out',
    },
  ],
}
