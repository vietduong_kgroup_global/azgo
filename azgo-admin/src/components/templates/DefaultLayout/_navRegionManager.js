import logo from 'assets/img/icon_tour.svg'
export default {
  items: [
    {
      name: 'Tài khoản',
      icon: 'fa fa-user',
      children: [
        {
          name: 'Nội bộ',
          url: '/admin/internal-management',
        },
        {
          name: 'Tài xế xe',
          url: '/admin/driver',
        },
        {
          name: 'Quản lý ví',
          url: '/admin/wallet',
        },
      ],
    },
    {
      name: 'Tour',
      icon:  { img: { src: logo} },
      children: [
        {
          name: 'Quản lý tour',
          url: '/admin/tour/tourManagement',
        },
        {
          name: 'Quản lý đơn hàng',
          url: '/admin/tour/orderManagement',
        },
        {
          name: 'Lịch sử đơn hàng',
          url: '/admin/tour/orderTourLog',
        },
      ],
    },
    {
      name: 'Version',
      url: '/admin/version',
      icon: 'fa fa-sign-out',
    },
    {
      name: 'Đăng xuất',
      url: '/logout',
      icon: 'fa fa-sign-out',
    },
  ],
}
