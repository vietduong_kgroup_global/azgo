import logo from 'assets/img/icon_tour.svg'
export default {
  items: [
    {
      name: 'Kế toán',
      icon: 'fa fa-pencil',
      children: [
        {
          name: 'Doanh thu đơn hàng',
          url: '/admin/report/revenue',
        }, 
        {
          name: 'Số dư tài xế',
          url: '/admin/report/feeWallet',
        }, 
      ],
    },
    {
      name: 'Version',
      url: '/admin/version',
      icon: 'fa fa-sign-out',
    },
    {
      name: 'Đăng xuất',
      url: '/logout',
      icon: 'fa fa-sign-out',
    },
  ],
}
