import logo from 'assets/img/icon_tour.svg'
export default {
  items: [
    {
      name: 'Dashboard',
      url: '/admin',
      icon: 'icon-speedometer',
    },
    {
      name: 'Tài khoản',
      icon: 'fa fa-user',
      children: [
        {
          name: 'Nội bộ',
          url: '/admin/internal-management',
        },
        {
          name: 'Người dùng',
          url: '/admin/user-management',
        },
        {
          name: 'Khách hàng',
          url: '/admin/clients-management',
        },
        // {
        //   name: 'Nhà xe',
        //   url: '/admin/provider-management',
        // }, 
        {
          name: 'Tài xế xe',
          url: '/admin/driver',
        },
        {
          name: 'Quản lý ví',
          url: '/admin/wallet',
        },
        // {
        //   name: 'Tài xế xe liên tỉnh',
        //   url: '/admin/driver/bus',
        // },
        // {
        //   name: 'Tài xế xe bike/car',
        //   url: '/admin/driver/bike-car',
        // },
        // {
        //   name: 'Tài xế xe van/bagac',
        //   url: '/admin/driver/van',
        // },
      ],
    },
    // {
    //   name: 'Xe liên tỉnh',
    //   icon: 'fa fa-bus',
    //   children: [
    //     {
    //       name: 'Quản lý địa điểm',
    //       url: '/admin/bus/area',
    //     },
    //     {
    //       name: 'Quản lý tuyến',
    //       url: '/admin/bus/route',
    //     },
    //     {
    //       name: 'Quản lý loại xe',
    //       url: '/admin/bus/vehicle-type',
    //     },
    //     {
    //       name: 'Quản lý xe',
    //       url: '/admin/bus/vehicle',
    //     },
    //     {
    //       name: 'Quản lý chi phí',
    //       url: '/admin/bus/budget',
    //     },
    //     {
    //       name: 'Quản lý mẫu ghế',
    //       url: '/admin/bus/seat-map',
    //     },
    //     {
    //       name: 'Xếp lịch chạy',
    //       url: '/admin/bus/schedule',
    //     },
    //   ],
    // },
    {
      name: 'Phương tiện',
      icon: 'fa fa-car',
      children: [
        {
          name: 'Quản lý xe',
          url: '/admin/vehicle/vehicle',
        },
        {
          name: 'Quản lý loại xe',
          url: '/admin/vehicle/vehicle-type',
        },
        {
          name: 'Quản lý hãng xe',
          url: '/admin/vehicle/vehicle-manufacturer',
        },
        // {
        //   name: 'Quản lý chuyến xe',
        //   url: '/admin/bike-car/trip-management',
        // },
      ],
    },
    {
      name: 'Bike/car',
      icon: 'fa fa-motorcycle',
      children: [
        // {
        //   name: 'Quản lý xe',
        //   url: '/admin/bike-car/vehicle',
        // },
        // {
        //   name: 'Quản lý loại xe',
        //   url: '/admin/bike-car/vehicle-type',
        // },
        // {
        //   name: 'Quản lý hãng xe',
        //   url: '/admin/bike-car/vehicle-manufacturer',
        // },
        {
          name: 'Quản lý chuyến xe',
          url: '/admin/bike-car/trip-management',
        },
      ],
    },
    // {
    //   name: 'Van/Ba gác',
    //   icon: 'fa fa-truck',
    //   children: [
    //     {
    //       name: 'Quản lý xe',
    //       url: '/admin/van/vehicle',
    //     },
    //     {
    //       name: 'Quản lý loại xe',
    //       url: '/admin/van/vehicle-type',
    //     },
    //     {
    //       name: 'Quản lý hãng xe',
    //       url: '/admin/van/vehicle-manufacturer',
    //     },
    //     {
    //       name: 'Quản lý chuyến xe',
    //       url: '/admin/van/trip-management',
    //     },
    //     {
    //       name: 'Quản lý dịch vụ',
    //       url: '/admin/van/other-service',
    //     },
    //   ],
    // },
    {
      name: 'Tour',
      icon:  { img: { src: logo} },
      children: [
        {
          name: 'Quản lý tour',
          url: '/admin/tour/tourManagement',
        },
        {
          name: 'Quản lý đơn hàng',
          url: '/admin/tour/orderManagement',
        },
        {
          name: 'Lịch sử đơn hàng',
          url: '/admin/tour/orderTourLog',
        },
        {
          name: 'Quản lý khu vực',
          url: '/admin/tour/areaManagement',
        },
        // {
        //   name: 'Quản lý xe',
        //   url: '/admin/tour-car/vehicle',
        // },
        // {
        //   name: 'Quản lý loại xe',
        //   url: '/admin/tour-car/vehicle-type',
        // },
        // {
        //   name: 'Quản lý hãng xe',
        //   url: '/admin/tour-car/vehicle-manufacturer',
        // },
        // {
        //   name: 'Quản lý chuyến xe',
        //   url: '/admin/tour-car/trip-management',
        // },
        // {
        //   name: 'Quản lý dịch vụ',
        //   url: '/admin/tour-car/other-service',
        // },
      ],
    },
    {
      name: 'Quản lý doanh thu',
      icon: 'fa fa-dollar',
      children: [
        {
          name: 'Phí dịch vụ',
          url: '/admin/service-charge',
        },
        {
          name: 'Phí phụ thu',
          url: '/admin/additional-fee',
        },
        {
          name: 'Xe liên tỉnh',
          url: '/admin/revenue-management/provider',
        },
        {
          name: 'Bike/Car',
          url: '/admin/revenue-management/bike-car',
        },
        {
          name: 'Van/bagac',
          url: '/admin/revenue-management/van-bagac',
        },
      ],
    },
    {
      name: 'Quản lý nội dung',
      icon: 'fa fa-pencil',
      children: [
        {
          name: 'Quản lý banner',
          url: '/admin/content-management/banner',
        },
        {
          name: 'Danh sách ngân hàng',
          url: '/admin/content-management/bank',
        },
        {
          name: 'Quản lý khuyến mãi',
          url: '/admin/content-management/promotion',
        },
        {
          name: 'Hotline và policy',
          url: '/admin/content-management/policy',
        },
        {
          name: 'Cài đặt hệ thống',
          url: '/admin/content-management/settingSystem',
        },
      ],
    },
    {
      name: 'Kế toán',
      icon: 'fa fa-pencil',
      children: [
        {
          name: 'Doanh thu đơn hàng',
          url: '/admin/report/revenue',
        }, 
        {
          name: 'Số dư tài xế',
          url: '/admin/report/feeWallet',
        }, 
      ],
    },
    {
      name: 'Version',
      url: '/admin/version',
      icon: 'fa fa-sign-out',
    },
    {
      name: 'Đăng xuất',
      url: '/logout',
      icon: 'fa fa-sign-out',
    },
  ],
}
