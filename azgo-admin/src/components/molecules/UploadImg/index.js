import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Config } from 'Config'
import { getToken } from 'Utils/token'
import { Button, Icon, message, Modal, Upload } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'  
import SrcDefautImg from 'assets/img/default-image.png';
import { DriverManagementActions } from 'Stores/DriverManagement/Actions' 
const UploadImage = ({ fileNameProp, imageProp, handleParent, setImgUrl, type_image_props, deleteImg, editItem}) => {
  const [loading, setLoading] = useState(false)
  const [imageUrl, setImageUrl] = useState('')
  const [previewVisible, setPreviewVisible] = useState(false)
  const propsHeaderUpload = {
    name: 'images[]',
    action: `${Config.DEV_URL.UPLOAD_IMAGE}`,//?type_image=${type_image_props}`,
    headers: {
      Authorization: 'bearer ' + getToken(),
    },
  }
  const getBase64 = (img, callback) => {
    const reader = new FileReader()
    reader.addEventListener('load', () => callback(reader.result))
    reader.readAsDataURL(img)
  }
  const handlePreviewImg = () => {
    setPreviewVisible(true)
  }
  const deleteImgFunc = () => {
    if(imageUrl && !fileNameProp) {
      setImageUrl('') 
      handleParent(null) 
    }
    else if(fileNameProp) {
      deleteImg(fileNameProp)
      setImageUrl('') 
      handleParent(null) 
    }
    // deleteImg()
  } 
  const handleChange = (info, value) => {
    if (info.file.status === 'uploading') {
      setLoading(true)
      return
    }
    if (info.file.status === 'done') {
      setLoading(false)
      // Get this url from response in real world.
      // getBase64(info.file.originFileObj, imageUrl => {
      //   setImgUrl(imageUrl)
      //   setLoading(false)
      // })
      console.log(info)
      handleParent(info) 
    } else if (info.file.status === 'error') {
      setLoading(false)
      message.error('Upload failed')
    }
  }
  useEffect(() => {
    if (imageProp !== '') {
      setImageUrl(imageProp)
    }
    return () => {
      setImageUrl('')
    }
  }, [imageProp])
  return (
    <>
      <div className="frame-img">
      {imageUrl && imageUrl !== '' && <Icon
            type="delete"
            className="icon_delete_upload"
            onClick={() => deleteImgFunc()}
          />}
        <div className="frame-img-inner">
          {imageUrl && imageUrl !== '' ? (
          <> 
                <img
                  src={imageUrl}
                  onClick={() => handlePreviewImg()}
                  alt="image"
                />
            </>
          ) : (
            <img
              src={SrcDefautImg} 
              alt="image"
              style={{width: '100%', height: '100%'}}
            />
            // <Icon type="picture" className="icon_picture_upload"/>
          )}
        </div>
      </div>
      <div className="container_button_action_upload"> 
        <Upload
          name="avatar"
          accept=".png, .jpg, .jpeg"
          className="avatar-uploader"
          {...propsHeaderUpload}
          showUploadList={false}
          onChange={e => handleChange(e)}>
          {imageUrl && imageUrl !== '' ? (
            <Button>
              <Icon type="edit" /> Thay đổi
              {loading && <Icon className="loading-image" type="loading" />}
            </Button>
          ) : (
            <Button>
              <Icon type="upload" /> Tải hình
              {loading && <Icon className="loading-image" type="loading" />}
            </Button>
          )} 
        </Upload>
        {/* <Button onClick={() => deleteImgFunc()} className="button_delete_upload"> Xoá</Button> */}
      </div>

      <Modal
        visible={previewVisible}
        footer={null}
        onCancel={() => setPreviewVisible(false)}>
        <div className="modal-image">
          <img alt="example" src={imageUrl} />
        </div>
      </Modal>
    </>
  )
}

UploadImage.propTypes = {
  imageProp: PropTypes.string,
  handleParent: PropTypes.func,
  deleteImg: PropTypes.func,
}
const mapStateToProps = state => ({ 
})

const mapDispatchToProps = dispatch => ({ 
  editItem: values => dispatch(DriverManagementActions.editItemRequest(values)),
  deleteImg: values =>
    dispatch(DriverManagementActions.deleteImgRequest(values)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(UploadImage)