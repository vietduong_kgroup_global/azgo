import React from 'react'
import PropTypes from 'prop-types'
import { Input } from 'antd'

const FormSearch = ({ handleSearch }) => {
  const Style = {
    maxWidth: '250px',
  }
  const { Search } = Input
  return (
    <>
      <Search
        style={Style}
        placeholder="Tìm kiếm"
        onSearch={value => handleSearch(value)}
        enterButton
      />
    </>
  )
}

FormSearch.propTypes = {
  handleSearch: PropTypes.func,
}

export default FormSearch
