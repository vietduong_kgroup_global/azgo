import React from 'react'
import PropTypes from 'prop-types'
import './styles.scss'

const ItemInfo = ({ label, content }) => {
  return (
    <>
      <div className="item-list-row">
        <div className="item-list-col item-list-label">{label}</div>
        <div className="item-list-col item-list-content">{content}</div>
      </div>
    </>
  )
}

ItemInfo.propTypes = {
  label: PropTypes.string,
  content: PropTypes.any,
}

export default ItemInfo
