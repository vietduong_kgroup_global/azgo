import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import './styles.scss'
import moment from 'moment'
import { Button, message, Modal, Popconfirm } from 'antd'
import 'Stores/Intercity/TicketManagement/Reducers'
import 'Stores/Intercity/TicketManagement/Sagas'
import { TicketManagementActions } from 'Stores/Intercity/TicketManagement/Actions'
import { TicketManagementSelectors } from 'Stores/Intercity/TicketManagement/Selectors'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { withRouter } from 'react-router-dom'
import { formatPhone } from 'Utils/helper'
import { connect } from 'react-redux'
import { compose } from 'redux'

const TicketInfo = ({
  idTicket,
  itemTicket,
  itemList,
  deleteTicket,
  detailTicket,
  clearItems,
  handleClickParent,
  showPopUp,
}) => {
  const [visible, setVisible] = useState(false)
  const confirm = () => {
    let values = {
      idTicket: idTicket,
      scheduleId: itemList.get('id'),
    }
    if (values) {
      deleteTicket(values)
    }
  }
  const showModal = () => {
    setVisible(true)
  }
  const handleCancel = () => {
    setVisible(false)
    handleClickParent()
  }
  useEffect(() => {
    if (idTicket) {
      detailTicket(idTicket)
    }
  }, [idTicket])
  useEffect(() => {
    if (itemTicket.size > 0) {
      showModal()
    }
  }, [itemTicket])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    if (showPopUp) {
      handleCancel()
    }
  }, [showPopUp])
  return (
    <>
      <Modal
        title="CHI TIẾT THÔNG TIN VÉ"
        visible={visible}
        onCancel={handleCancel}
        footer={null}>
        <div className="row-info-ticket">
          <div className="info-ticket-label">Id:</div>
          {itemTicket && itemTicket.get('id')}
        </div>
        <div className="row-info-ticket">
          <div className="info-ticket-label">Họ và tên:</div>
          {itemTicket &&
            itemTicket.getIn(['user', 'customerProfile', 'fullName'])}
        </div>
        <div className="row-info-ticket">
          <div className="info-ticket-label">Email:</div>
          {itemTicket && itemTicket.getIn(['user', 'email'])}
        </div>
        <div className="row-info-ticket">
          <div className="info-ticket-label">Số điện thoại:</div>
          {formatPhone(
            itemTicket && itemTicket.getIn(['user', 'countryCode']),
            itemTicket && itemTicket.getIn(['user', 'phone'])
          )}
        </div>
        <div className="row-info-ticket">
          <div className="info-ticket-label">Thời gian đặt:</div>
          {moment(new Date(itemTicket.get('createdAt'))).format('HH:mm')}
        </div>
        <div className="row-info-ticket">
          <div className="info-ticket-label">Ngày đặt:</div>
          {moment(new Date(itemTicket.get('createdAt'))).format('DD-MM-YYYY')}
        </div>
        <div className="row-info-ticket">
          <div className="info-ticket-label">Điểm đón:</div>
          {itemTicket && itemTicket.get('pickupPoint')}
        </div>
        <div className="row-info-ticket">
          <div className="info-ticket-label">Điểm đến:</div>
          {itemTicket && itemTicket.get('destination')}
        </div>
        {itemList.get('status') === 1 && (
          <div style={{ textAlign: 'right', marginTop: '10px' }}>
            <Popconfirm
              title="Bạn có chắc chắn muốn hủy vé?"
              onConfirm={() => confirm()}
              okText="Có"
              cancelText="Không">
              <Button type="danger">Hủy vé</Button>
            </Popconfirm>
          </div>
        )}
      </Modal>
    </>
  )
}

TicketInfo.propTypes = {
  itemTicket: PropTypes.object,
  idTicket: PropTypes.number,
  deleteTicket: PropTypes.func,
  detailTicket: PropTypes.func,
  itemList: PropTypes.object,
  clearItems: PropTypes.func,
  handleClickParent: PropTypes.func,
  showPopUp: PropTypes.bool,
}

const mapStateToProps = state => ({
  itemTicket: TicketManagementSelectors.getItem(state),
  showPopUp: LoadingSelectors.getShowPopUp(state),
})

const mapDispatchToProps = dispatch => ({
  detailTicket: values =>
    dispatch(TicketManagementActions.getItemDetailRequest(values)),
  deleteTicket: values =>
    dispatch(TicketManagementActions.deleteItemRequest(values)),
  clearItems: () => dispatch(TicketManagementActions.clearItems()),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(TicketInfo)
