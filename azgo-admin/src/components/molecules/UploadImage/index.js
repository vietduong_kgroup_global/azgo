import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Config } from 'Config'
import { getToken } from 'Utils/token'
import { Button, Icon, message, Modal, Upload } from 'antd'

const UploadImage = ({ imageProp, handleParent }) => {
  const [loading, setLoading] = useState(false)
  const [imageUrl, setImageUrl] = useState('')
  const [imageFieldValue, setImageFieldValue] = useState(null)
  const [previewVisible, setPreviewVisible] = useState(false)
  const propsHeaderUpload = {
    name: 'images[]',
    action: Config.DEV_URL.UPLOAD_IMAGE,
    headers: {
      Authorization: 'bearer ' + getToken(),
    },
  }
  const getBase64 = (img, callback) => {
    const reader = new FileReader()
    reader.addEventListener('load', () => callback(reader.result))
    reader.readAsDataURL(img)
  }
  const handlePreviewImg = () => {
    setPreviewVisible(true)
  }
  const handleChange = (info, value) => {
    if (info.file.status === 'uploading') {
      setLoading(true)
    } else {
      setLoading(false)
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl => {
        setLoading('')
        let imageProfile = {
          filePath: info.file.response.data[0].file_path,
          fileName: info.file.response.data[0].file_name,
        }
        setImageUrl(imageUrl)
        setImageFieldValue(imageProfile)
      })
    } else if (info.file.status === 'error') {
      setLoading(false)
      message.error('Upload failed')
    }
  }
  useEffect(() => {
    if (imageFieldValue) {
      handleParent(imageFieldValue)
    }
  }, [imageFieldValue])
  useEffect(() => {
    if (imageProp !== '') {
      setImageUrl(imageProp)
    }
    return () => {
      setImageUrl('')
    }
  }, [imageProp])
  return (
    <>
      <div className="frame-img frame-img-big">
        <div className="frame-img-inner">
          {imageUrl && imageUrl !== '' ? (
            <img
              src={imageUrl}
              onClick={() => handlePreviewImg()}
              alt="image"
            />
          ) : (
            <Icon type="picture" />
          )}
        </div>
      </div>
      <Upload
        name="avatar"
        className="avatar-uploader"
        {...propsHeaderUpload}
        showUploadList={false}
        onChange={e => handleChange(e)}>
        {imageUrl && imageUrl !== '' ? (
          <Button>
            <Icon type="edit" /> Thay đổi hình
            {loading && <Icon className="loading-image" type="loading" />}
          </Button>
        ) : (
          <Button>
            <Icon type="upload" /> Tải hình lên
            {loading && <Icon className="loading-image" type="loading" />}
          </Button>
        )}
      </Upload>
      <Modal
        visible={previewVisible}
        footer={null}
        onCancel={() => setPreviewVisible(false)}>
        <div className="modal-image">
          <img alt="example" src={imageUrl} />
        </div>
      </Modal>
    </>
  )
}

UploadImage.propTypes = {
  imageProp: PropTypes.string,
  handleParent: PropTypes.func,
}

export default UploadImage
