import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { message } from 'antd'
import { Card, CardBody, CardHeader, Col, Row, Button } from 'reactstrap'
import Autocomplete from 'react-google-autocomplete'
import { connect } from 'react-redux'
import styles from './styles.module.scss'
import marker1 from 'assets/img/marker1.png'
import marker2 from 'assets/img/marker2.png'
import marker3 from 'assets/img/marker3.png'

const BusRouteMap = ({ item, handlePropLocation, startPoint, endPoint }) => {
  const [location, setLocation] = useState(null)
  const [stations, setStations] = useState([])
  const [additionalStation, setAdditionalStation] = useState([])
  const [destinations, setDestinations] = useState([])
  const [waypts, setWaypts] = useState([])
  const [origin, setOrigin] = useState(null)
  const [destinationEnd, setDestinationEnd] = useState(null)
  const [stationsRoute, setStationsRoute] = useState([])
  const [additionalStationRoute, setAdditionalStationRoute] = useState([])
  const [destinationsRoute, setDestinationsRoute] = useState([])
  const [shelters, setShelters] = useState({
    stations: [],
    additional_station: [],
    destinations: [],
  })
  const google = window.google
  const initMap = () => {
    var map = new google.maps.Map(document.getElementById('mapRoute'), {
      zoom: 5,
      center: { lat: 16.1780618, lng: 107.2195423 },
    })
    // map
    let directionsService = new google.maps.DirectionsService()
    let directionsRenderer = new google.maps.DirectionsRenderer()
    directionsRenderer.setMap(map)
    function calculateAndDisplayRoute(directionsService, directionsRenderer) {
      directionsService.route(
        {
          origin: origin || null,
          destination: destinationEnd || null,
          // waypoints: waypts,
          optimizeWaypoints: true,
          travelMode: 'DRIVING',
        },
        function(response, status) {
          if (status === 'OK') {
            directionsRenderer.setDirections(response)
          } else {
            message.warning('Định tuyến không thành công ' + status)
          }
        }
      )
    }
    if (origin !== null && destinationEnd !== null) {
      if (origin.lat !== null && destinationEnd.lat !== null) {
        calculateAndDisplayRoute(directionsService, directionsRenderer)
      }
    }

    //  marker
    const marker = (arr, icon) => {
      let infowindow = new google.maps.InfoWindow()
      if (arr.length > 0) {
        for (let i = 0; i < arr.length; i++) {
          let marker = new google.maps.Marker({
            position: new google.maps.LatLng(arr[i][1], arr[i][2]),
            map: map,
          })
          marker.setIcon({
            url: icon,
            scaledSize: new google.maps.Size(30, 44),
          })
          google.maps.event.addListener(
            marker,
            'click',
            (function(marker, i) {
              return function() {
                infowindow.setContent(arr[i][0])
                infowindow.open(map, marker)
              }
            })(marker, i)
          )
        }
      }
    }
    if (stationsRoute.length > 0) {
      marker(stationsRoute, marker1)
    }
    if (additionalStationRoute.length > 0) {
      marker(additionalStationRoute, marker2)
    }
    if (destinationsRoute.length > 0) {
      marker(destinationsRoute, marker3)
    }
  }
  const addLocation = () => {
    if (location) {
      switch (location.type) {
        case 'stations': {
          const values = [...stations]
          values.push({
            address: location.address,
            lat: location.lat,
            lng: location.lng,
            long: location.lng,
            flag: false,
          })
          setStations([...values])
          break
        }
        case 'additionalStation': {
          const values = [...additionalStation]
          values.push({
            address: location.address,
            lat: location.lat,
            lng: location.lng,
            long: location.lng,
            flag: false,
          })
          setAdditionalStation([...values])
          break
        }
        default: {
          const values = [...destinations]
          values.push({
            address: location.address,
            lat: location.lat,
            lng: location.lng,
            long: location.lng,
            flag: false,
          })
          setDestinations([...values])
          break
        }
      }
    }
  }
  const handleRemoveField = (i, value) => {
    switch (value) {
      case 'stations': {
        const values = [...stations]
        values.splice(i, 1)
        setStations([...values])
        break
      }
      case 'additionalStation': {
        const values = [...additionalStation]
        values.splice(i, 1)
        setAdditionalStation([...values])
        break
      }
      default: {
        const values = [...destinations]
        values.splice(i, 1)
        setDestinations([...values])
        break
      }
    }
  }
  const handleCheckLocation = () => {
    let listAdditionalStation = []
    if (additionalStation.length > 0) {
      for (let i = 0; i < additionalStation.length; i++) {
        let item = {
          id: i + 1,
          ...additionalStation[i],
          long: additionalStation[i].lng,
        }
        listAdditionalStation.push(item)
      }
    }
    setShelters({
      stations: [...stations],
      additional_station: [...listAdditionalStation],
      destinations: [...destinations],
    })
  }
  useEffect(() => {
    handleCheckLocation()
  }, [stations, additionalStation, destinations])
  useEffect(() => {
    handlePropLocation(shelters)
  }, [shelters])
  useEffect(() => {
    if (item && item.size > 0) {
      setStations(
        item.getIn(['router', 'stations']) &&
          item.getIn(['router', 'stations']).toJS()
      )
      setAdditionalStation(
        item.getIn(['router', 'additionalStation']) &&
          item.getIn(['router', 'additionalStation']).toJS()
      )
      setDestinations(
        item.getIn(['router', 'destinations']) &&
          item.getIn(['router', 'destinations']).toJS()
      )
    }
    return () => {
      setStations([])
      setAdditionalStation([])
      setDestinations([])
      setOrigin(null)
      setDestinationEnd(null)
    }
  }, [item])
  useEffect(() => {
    setOrigin({
      lat: startPoint ? parseFloat(startPoint.coordinates.lat) : null,
      lng: startPoint ? parseFloat(startPoint.coordinates.long) : null,
    })
    setDestinationEnd({
      lat: endPoint ? parseFloat(endPoint.coordinates.lat) : null,
      lng: endPoint ? parseFloat(endPoint.coordinates.long) : null,
    })
  }, [startPoint, endPoint])
  useEffect(() => {
    if (stations.length > 0) {
      let valueStationsRoute = []
      // const valueWaypts = []
      for (let i = 0; i < stations.length; i++) {
        // valueWaypts.push({ location: stations[i].address })
        valueStationsRoute.push([
          stations[i].address,
          stations[i].lat,
          stations[i].lng,
        ])
        setStationsRoute([...valueStationsRoute])
        // setWaypts(...[valueWaypts])
      }
    }
    return () => {
      setStationsRoute([])
    }
  }, [stations])
  useEffect(() => {
    if (additionalStation.length > 0) {
      let value = []
      for (let i = 0; i < additionalStation.length; i++) {
        value.push([
          additionalStation[i].address,
          additionalStation[i].lat,
          additionalStation[i].lng,
        ])
        setAdditionalStationRoute([...value])
      }
    }
    return () => {
      setAdditionalStationRoute([])
    }
  }, [additionalStation])
  useEffect(() => {
    if (destinations.length > 0) {
      let value = []
      for (let i = 0; i < destinations.length; i++) {
        value.push([
          destinations[i].address,
          destinations[i].lat,
          destinations[i].lng,
        ])
        setDestinationsRoute([...value])
      }
    }
    return () => {
      setDestinationsRoute([])
    }
  }, [destinations])
  useEffect(() => {
    if (origin && destinationEnd) {
      initMap()
    }
  }, [origin, destinationEnd])
  useEffect(() => {
    initMap()
  }, [stationsRoute, additionalStationRoute, destinationsRoute])
  useEffect(() => {
    initMap()
  }, [])
  return (
    <>
      <Card>
        <CardHeader className={styles.cardHeader}>
          <strong>Danh sách địa điểm</strong>
        </CardHeader>
        <CardBody style={{ background: '#fafafa', padding: '10px' }}>
          <Row>
            <Col xs="12" sm="3">
              <div className={styles.cardLocation}>
                <div className={styles.cardLocationTitle}>
                  <span className="mr-1">Điểm đón chính</span>
                  <img src={marker1} width="10" />
                </div>
                <div>
                  {stations.map((item, idx) => (
                    <div key={idx} className="mb-1">
                      <strong style={{ marginRight: '10px' }}>
                        {idx + 1}.
                      </strong>
                      {item.address}
                      <i
                        className={`${styles.trashFiled}` + ` fa fa-trash ml-1`}
                        onClick={() => handleRemoveField(idx, 'stations')}
                      />
                    </div>
                  ))}
                  <div className={`${styles.addField} mt-2`}>
                    <Autocomplete
                      style={{ width: '100%' }}
                      types={[]}
                      fields={[
                        'name',
                        'address_component',
                        'formatted_address',
                        'geometry',
                      ]}
                      onPlaceSelected={place => {
                        let index = place.formatted_address.indexOf(place.name)
                        if (place.address_components) {
                          if (index === -1) {
                            setLocation({
                              address:
                                place.name + ', ' + place.formatted_address,
                              lat: place.geometry.location.lat(),
                              lng: place.geometry.location.lng(),
                              long: place.geometry.location.lng(),
                              type: 'stations',
                            })
                          } else {
                            setLocation({
                              address: place.formatted_address,
                              lat: place.geometry.location.lat(),
                              lng: place.geometry.location.lng(),
                              long: place.geometry.location.lng(),
                              type: 'stations',
                            })
                          }
                        }
                      }}
                      componentRestrictions={{ country: 'vn' }}
                    />
                    <Button
                      color="success"
                      size="sm"
                      className="ml-1"
                      onClick={addLocation}>
                      Thêm
                    </Button>
                  </div>
                </div>
              </div>
              <div className={styles.cardLocation}>
                <div className={styles.cardLocationTitle}>
                  <span className="mr-1">Điểm đón phụ</span>
                  <img src={marker2} width="10" />
                </div>
                <div>
                  {additionalStation.map((item, idx) => (
                    <div key={idx} className="mb-1">
                      <strong style={{ marginRight: '10px' }}>
                        {idx + 1}.
                      </strong>
                      {item.address}
                      <i
                        className={`${styles.trashFiled}` + ` fa fa-trash ml-1`}
                        onClick={() =>
                          handleRemoveField(idx, 'additionalStation')
                        }
                      />
                    </div>
                  ))}
                  <div className={`${styles.addField} mt-2`}>
                    <Autocomplete
                      style={{ width: '100%' }}
                      types={[]}
                      fields={[
                        'name',
                        'address_component',
                        'formatted_address',
                        'geometry',
                      ]}
                      onPlaceSelected={place => {
                        if (place.address_components) {
                          let index = place.formatted_address.indexOf(
                            place.name
                          )
                          if (index === -1) {
                            setLocation({
                              address:
                                place.name + ', ' + place.formatted_address,
                              lat: place.geometry.location.lat(),
                              lng: place.geometry.location.lng(),
                              long: place.geometry.location.lng(),
                              type: 'additionalStation',
                            })
                          } else {
                            setLocation({
                              address: place.formatted_address,
                              lat: place.geometry.location.lat(),
                              lng: place.geometry.location.lng(),
                              long: place.geometry.location.lng(),
                              type: 'additionalStation',
                            })
                          }
                        }
                      }}
                      componentRestrictions={{ country: 'vn' }}
                    />
                    <Button
                      color="success"
                      size="sm"
                      className="ml-1"
                      onClick={addLocation}>
                      Thêm
                    </Button>
                  </div>
                </div>
              </div>
              <div className={styles.cardLocation}>
                <div className={styles.cardLocationTitle}>
                  <span className="mr-1">Điểm đến phụ</span>
                  <img src={marker3} width="10" />
                </div>
                <div>
                  {destinations.map((item, idx) => (
                    <div key={idx} className="mb-1">
                      <strong style={{ marginRight: '10px' }}>
                        {idx + 1}.
                      </strong>
                      {item.address}
                      <i
                        className={`${styles.trashFiled}` + ` fa fa-trash ml-1`}
                        onClick={() => handleRemoveField(idx, 'destinations')}
                      />
                    </div>
                  ))}
                  <div className={`${styles.addField} mt-2`}>
                    <Autocomplete
                      style={{ width: '100%' }}
                      types={[]}
                      fields={[
                        'name',
                        'address_component',
                        'formatted_address',
                        'geometry',
                      ]}
                      onPlaceSelected={place => {
                        if (place.address_components) {
                          let index = place.formatted_address.indexOf(
                            place.name
                          )
                          if (index === -1) {
                            setLocation({
                              address:
                                place.name + ', ' + place.formatted_address,
                              lat: place.geometry.location.lat(),
                              lng: place.geometry.location.lng(),
                              long: place.geometry.location.lng(),
                              type: 'destinations',
                            })
                          } else {
                            setLocation({
                              address: place.formatted_address,
                              lat: place.geometry.location.lat(),
                              lng: place.geometry.location.lng(),
                              long: place.geometry.location.lng(),
                              type: 'destinations',
                            })
                          }
                        }
                      }}
                      componentRestrictions={{ country: 'vn' }}
                    />
                    <Button
                      color="success"
                      size="sm"
                      className="ml-1"
                      onClick={addLocation}>
                      Thêm
                    </Button>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs="12" sm="9">
              <div id="mapRoute" className={styles.mapRoute} />
            </Col>
          </Row>
        </CardBody>
      </Card>
    </>
  )
}

BusRouteMap.propTypes = {
  item: PropTypes.object,
  handlePropLocation: PropTypes.func,
  startPoint: PropTypes.object,
  endPoint: PropTypes.object,
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(BusRouteMap)
