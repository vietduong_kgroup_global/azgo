import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles.module.scss'
import { Card, CardBody } from 'reactstrap'

const CardDashboard = ({ classColor, number, text, icon }) => {
  return (
    <>
      <Card className={styles.card}>
        <CardBody
          className={`clearfix p-3 card-body ${styles.cardBodyDashboard}`}>
          <i
            className={`fa ${icon} p-3 mr-3 float-left bg-${classColor} ${styles.icon}`}
          />
          <div>
            <div className={`h5 mb-0 text-${classColor}`}>{number}</div>
            <div className="text-muted text-uppercase font-weight-bold font-xs">
              {text}
            </div>
          </div>
        </CardBody>
      </Card>
    </>
  )
}

CardDashboard.propTypes = {
  classColor: PropTypes.string,
  number: PropTypes.any,
  text: PropTypes.string,
  icon: PropTypes.any,
}

export default CardDashboard
