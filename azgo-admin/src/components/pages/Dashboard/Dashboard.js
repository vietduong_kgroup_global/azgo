import React, { useEffect, useState } from 'react'
import { Line } from 'react-chartjs-2'
import { Card, CardHeader, CardBody, CardTitle, Col, Row } from 'reactstrap'
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips'
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import CardDashboard from 'components/molecules/CardDashboard'
import { Link, withRouter } from 'react-router-dom'
import 'Stores/Dashboard/Reducers'
import 'Stores/Dashboard/Sagas'
import { DashboardManagementSelectors } from 'Stores/Dashboard/Selectors'
import { DashboardManagementActions } from 'Stores/Dashboard/Actions'
import styles from './styles.module.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import * as config from 'Utils/enum'
import { formatPrice } from 'Utils/helper'
import moment from 'moment'
import { Select } from 'antd'

const brandInfo = getStyle('--info')
const mainChartOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
    intersect: true,
    mode: 'index',
    position: 'nearest',
    callbacks: {
      labelColor: function(tooltipItem, chart) {
        return {
          backgroundColor:
            chart.data.datasets[tooltipItem.datasetIndex].borderColor,
        }
      },
    },
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          drawOnChartArea: true,
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          // maxTicksLimit: 5,
          // stepSize: Math.ceil(250 / 5),
          // max: 250,
        },
      },
    ],
  },
  elements: {
    point: {
      radius: 2,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
}

const Dashboard = ({
  // bus
  getItemAmountCustomerBus,
  itemAmountCustomerBus,
  getItemAmountScheduleBus,
  itemAmountScheduleBus,
  getItemAmountScheduleDoneBus,
  itemAmountScheduleDoneBus,
  getItemRevenueBus,
  itemRevenueBus,
  // bike
  getItemAmountOrderBike,
  itemAmountOrderBike,
  getItemAmountOrderMonthBike,
  itemAmountOrderMonthBike,
  getItemAmountOrderMonthDoneBike,
  itemAmountOrderMonthDoneBike,
  getItemRevenueBike,
  itemRevenueBike,
  // van
  getItemAmountOrderVan,
  itemAmountOrderVan,
  getItemAmountOrderMonthVan,
  itemAmountOrderMonthVan,
  getItemAmountOrderMonthDoneVan,
  itemAmountOrderMonthDoneVan,
  getItemRevenueVan,
  itemRevenueVan,
  // line chart
  getItemsLineChart,
  itemsLineChart,
  filter,
  setFilter,
}) => {
  const dataMonth = []
  for (let i = 1; i <= 12; i++) {
    dataMonth.push({
      name: `Tháng ${i}`,
      value: i,
    })
  }
  const dataYear = []
  var year = new Date().getFullYear()
  for (let i = 2020; i <= year; i++) {
    dataYear.push({
      name: `${i}`,
      value: i,
    })
  }
  const handleChangeSelectYear = value => {
    setFilter({
      ...filter,
      year: value,
    })
  }
  const handleChangeSelectMonth = value => {
    if (value) {
      setFilter({
        ...filter,
        month: value,
      })
    } else {
      setFilter({
        ...filter,
        month: null,
      })
    }
  }
  const [lineChartLabel, setLineChartLabel] = useState([])
  const [lineChartValue, setLineChartValue] = useState([])
  const [mainChart, setMainChart] = useState({
    labels: [],
    datasets: [
      {
        label: 'Doanh thu',
        backgroundColor: hexToRgba(brandInfo, 10),
        borderColor: brandInfo,
        pointHoverBackgroundColor: '#fff',
        borderWidth: 2,
        data: [],
      },
    ],
  })
  const [filterAmountCustomerBus, setFilterAmountCustomerBus] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
  })
  const [filterAmountScheduleBus, setFilterAmountScheduleBus] = useState({
    from_date: '',
    to_date: '',
    is_in_month: true,
    status: null,
  })
  const [
    filterAmountScheduleDoneBus,
    setFilterAmountScheduleDoneBus,
  ] = useState({
    from_date: '',
    to_date: '',
    is_in_month: true,
    status: config.MANAGEMENT_STATUS_SCHEDULE_BUS.DONE,
  })
  const [filterRevenueBus, setFilterRevenueBus] = useState({
    from_date: '',
    to_date: '',
    is_in_month: true,
  })
  // bike
  const [filterAmountOrderBike, setFilterAmountOrderBike] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
  })
  const [filterAmountOrderMonthBike, setFilterAmountOrderMonthBike] = useState({
    from_date: '',
    to_date: '',
    is_in_month: true,
  })
  const [
    filterAmountOrderMonthDoneBike,
    setFilterAmountOrderMonthDoneBike,
  ] = useState({
    from_date: '',
    to_date: '',
    is_in_month: true,
    status: config.MANAGEMENT_STATUS_TRIP.COMPLETED,
  })
  const [filterRevenueBike, setFilterRevenueBike] = useState({
    from_date: '',
    to_date: '',
    is_in_month: true,
  })
  // van
  const [filterAmountOrderVan, setFilterAmountOrderVan] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
  })
  const [filterAmountOrderMonthVan, setFilterAmountOrderMonthVan] = useState({
    from_date: '',
    to_date: '',
    is_in_month: true,
  })
  const [
    filterAmountOrderMonthDoneVan,
    setFilterAmountOrderMonthDoneVan,
  ] = useState({
    from_date: '',
    to_date: '',
    is_in_month: true,
    status: config.MANAGEMENT_STATUS_VAN_TRIP.COMPLETED,
  })
  const [filterRevenueVan, setFilterRevenueVan] = useState({
    from_date: '',
    to_date: '',
    is_in_month: true,
  })
  const handleFormatPrice = data => {
    if (typeof data === 'string') return data
    return formatPrice(data)
  }
  useEffect(() => {
    // bus
    getItemAmountCustomerBus(filterAmountCustomerBus)
    getItemAmountScheduleBus(filterAmountScheduleBus)
    getItemAmountScheduleDoneBus(filterAmountScheduleDoneBus)
    getItemRevenueBus(filterRevenueBus)
    // bike
    getItemAmountOrderBike(filterAmountOrderBike)
    getItemAmountOrderMonthBike(filterAmountOrderMonthBike)
    getItemAmountOrderMonthDoneBike(filterAmountOrderMonthDoneBike)
    getItemRevenueBike(filterRevenueBike)
    // van
    getItemAmountOrderVan(filterAmountOrderVan)
    getItemAmountOrderMonthVan(filterAmountOrderMonthVan)
    getItemAmountOrderMonthDoneVan(filterAmountOrderMonthDoneVan)
    getItemRevenueVan(filterRevenueVan)
    // line chart
    setFilter({
      month: null,
      year: year,
    })
  }, [])
  useEffect(() => {
    getItemsLineChart(filter)
  }, [filter])
  useEffect(() => {
    if (itemsLineChart) {
      if (!filter.get('month')) {
        setLineChartLabel([])
        setLineChartValue([])
        for (let i = 1; i <= 12; i++) {
          lineChartLabel.push(`Tháng ${i}`)
          lineChartValue.push(0)
        }
        for (let j = 0; j < itemsLineChart.toJS().length; j++) {
          lineChartValue.splice(
            itemsLineChart.toJS()[j].month - 1,
            1,
            itemsLineChart.toJS()[j].value
          )
        }
      } else {
        let month = `${filter.get('year')}-${filter.get('month')}`
        let dayOfMonth = moment(month, 'YYYY-MM').daysInMonth()
        setLineChartLabel([])
        setLineChartValue([])
        for (let i = 1; i <= dayOfMonth; i++) {
          lineChartLabel.push(`${i}`)
          lineChartValue.push(0)
        }
        for (let j = 0; j < itemsLineChart.toJS().length; j++) {
          lineChartValue.splice(
            itemsLineChart.toJS()[j].day - 1,
            1,
            itemsLineChart.toJS()[j].value
          )
        }
      }
    }
    setMainChart({
      labels: lineChartLabel,
      datasets: [
        {
          label: 'Doanh thu',
          backgroundColor: hexToRgba(brandInfo, 10),
          borderColor: brandInfo,
          pointHoverBackgroundColor: '#fff',
          borderWidth: 2,
          data: lineChartValue,
        },
      ],
    })
  }, [itemsLineChart])
  return (
    <div className="animated fadeIn">
      <Card className={styles.card}>
        <CardHeader className={styles.cardHeader}>
          <Link to="/revenue-management/provider" className={styles.link}>
            <strong>XE LIÊN TỈNH </strong>
            <i className="fa fa-bus" />
          </Link>
        </CardHeader>
        <CardBody className={styles.cardBody}>
          <Row>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'success'}
                number={handleFormatPrice(itemAmountCustomerBus)}
                text={'Số lượng khách'}
                icon={'fa-user-plus'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'info'}
                number={handleFormatPrice(itemAmountScheduleBus)}
                text={'Tổng số chuyến trong tháng'}
                icon={'fa-exchange'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'warning'}
                number={handleFormatPrice(itemAmountScheduleDoneBus)}
                text={'Số chuyến hoàn thành trong tháng'}
                icon={'fa-check'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'danger'}
                number={handleFormatPrice(itemRevenueBus)}
                text={'Doanh thu trong tháng'}
                icon={'fa-dollar'}
              />
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Card className={styles.card}>
        <CardHeader className={styles.cardHeader}>
          <Link to="/revenue-management/bike-car" className={styles.link}>
            <strong>BIKE/CAR </strong>
            <i className="fa fa-motorcycle" />
          </Link>
        </CardHeader>
        <CardBody className={styles.cardBody}>
          <Row>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'success'}
                number={handleFormatPrice(itemAmountOrderBike)}
                text={'Số lượng khách'}
                icon={'fa-user-plus'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'info'}
                number={handleFormatPrice(itemAmountOrderMonthBike)}
                text={'Tổng số chuyến trong tháng'}
                icon={'fa-exchange'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'warning'}
                number={handleFormatPrice(itemAmountOrderMonthDoneBike)}
                text={'Số chuyến hoàn thành trong tháng'}
                icon={'fa-check'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'danger'}
                number={handleFormatPrice(itemRevenueBike)}
                text={'Doanh thu trong tháng'}
                icon={'fa-dollar'}
              />
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Card className={styles.card}>
        <CardHeader className={styles.cardHeader}>
          <Link to="/revenue-management/van-bagac" className={styles.link}>
            <strong>VAN/BAGAC </strong>
            <i className="fa fa-truck" />
          </Link>
        </CardHeader>
        <CardBody className={styles.cardBody}>
          <Row>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'success'}
                number={handleFormatPrice(itemAmountOrderVan)}
                text={'Số lượng khách'}
                icon={'fa-user-plus'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'info'}
                number={handleFormatPrice(itemAmountOrderMonthVan)}
                text={'Tổng số chuyến trong tháng'}
                icon={'fa-exchange'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'warning'}
                number={handleFormatPrice(itemAmountOrderMonthDoneVan)}
                text={'Số chuyến hoàn thành trong tháng'}
                icon={'fa-check'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'danger'}
                number={handleFormatPrice(itemRevenueVan)}
                text={'Doanh thu trong tháng'}
                icon={'fa-dollar'}
              />
            </Col>
          </Row>
        </CardBody>
      </Card>
      {/* <Card className={styles.card}>
        <CardHeader className={styles.cardHeader}>
          <Link to="/revenue-management/van-bagac" className={styles.link}>
            <strong>TOUR CAR</strong>
            <i className="fa fa-truck" />
          </Link>
        </CardHeader>
        <CardBody className={styles.cardBody}>
          <Row>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'success'}
                number={handleFormatPrice(itemAmountOrderVan)}
                text={'Số lượng khách'}
                icon={'fa-user-plus'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'info'}
                number={handleFormatPrice(itemAmountOrderMonthVan)}
                text={'Tổng số chuyến trong tháng'}
                icon={'fa-exchange'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'warning'}
                number={handleFormatPrice(itemAmountOrderMonthDoneVan)}
                text={'Số chuyến hoàn thành trong tháng'}
                icon={'fa-check'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'danger'}
                number={handleFormatPrice(itemRevenueVan)}
                text={'Doanh thu trong tháng'}
                icon={'fa-dollar'}
              />
            </Col>
          </Row>
        </CardBody>
      </Card> */}
      <Row>
        <Col>
          <Card>
            <CardBody>
              <Row>
                <Col sm="5">
                  <CardTitle className="mb-0">DOANH THU TỔNG</CardTitle>
                  <div className="small text-muted">
                    {filter.get('month') ? (
                      <span>Tháng {filter.get('month')} - </span>
                    ) : null}
                    Năm {filter.get('year')}
                  </div>
                </Col>
                <Col sm="7" className="d-none d-sm-inline-block">
                  <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <Select
                      placeholder="Chọn tháng"
                      style={{ width: '100px', marginRight: '10px' }}
                      defaultValue=""
                      onChange={handleChangeSelectMonth}>
                      <Select.Option value=""> None</Select.Option>
                      {dataMonth.map(item => (
                        <Select.Option value={item.value} key={item.value}>
                          {item.name}
                        </Select.Option>
                      ))}
                    </Select>
                    <Select
                      placeholder="Chọn năm"
                      style={{ width: '100px' }}
                      defaultValue={year}
                      onChange={handleChangeSelectYear}>
                      {dataYear.map(item => (
                        <Select.Option value={item.value} key={item.value}>
                          {item.name}
                        </Select.Option>
                      ))}
                    </Select>
                  </div>
                </Col>
              </Row>
              <div
                className="chart-wrapper"
                style={{ height: 300 + 'px', marginTop: 40 + 'px' }}>
                <Line data={mainChart} options={mainChartOpts} height={300} />
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

const mapStateToProps = state => ({
  // bus
  itemAmountCustomerBus: DashboardManagementSelectors.getItemAmountCustomerBus(
    state
  ),
  itemAmountScheduleBus: DashboardManagementSelectors.getItemAmountScheduleBus(
    state
  ),
  itemAmountScheduleDoneBus: DashboardManagementSelectors.getItemAmountScheduleDoneBus(
    state
  ),
  itemRevenueBus: DashboardManagementSelectors.getItemRevenueBus(state),
  // bike
  itemAmountOrderBike: DashboardManagementSelectors.getItemAmountOrderBike(
    state
  ),
  itemAmountOrderMonthBike: DashboardManagementSelectors.getItemAmountOrderMonthBike(
    state
  ),
  itemAmountOrderMonthDoneBike: DashboardManagementSelectors.getItemAmountOrderMonthDoneBike(
    state
  ),
  itemRevenueBike: DashboardManagementSelectors.getItemRevenueBike(state),
  //  van
  itemAmountOrderVan: DashboardManagementSelectors.getItemAmountOrderVan(state),
  itemAmountOrderMonthVan: DashboardManagementSelectors.getItemAmountOrderMonthVan(
    state
  ),
  itemAmountOrderMonthDoneVan: DashboardManagementSelectors.getItemAmountOrderMonthDoneVan(
    state
  ),
  itemRevenueVan: DashboardManagementSelectors.getItemRevenueVan(state),
  // line chart
  itemsLineChart: DashboardManagementSelectors.getItemsLineChart(state),
  filter: DashboardManagementSelectors.getFilter(state),
})

Dashboard.propTypes = {
  // bus
  getItemAmountCustomerBus: PropTypes.func,
  getItemAmountScheduleBus: PropTypes.func,
  getItemAmountScheduleDoneBus: PropTypes.func,
  getItemRevenueBus: PropTypes.func,
  itemAmountCustomerBus: PropTypes.any,
  itemAmountScheduleBus: PropTypes.any,
  itemAmountScheduleDoneBus: PropTypes.any,
  itemRevenueBus: PropTypes.any,
  // bike
  getItemAmountOrderBike: PropTypes.func,
  getItemAmountOrderMonthBike: PropTypes.func,
  getItemAmountOrderMonthDoneBike: PropTypes.func,
  getItemRevenueBike: PropTypes.func,
  itemAmountOrderBike: PropTypes.any,
  itemAmountOrderMonthBike: PropTypes.any,
  itemAmountOrderMonthDoneBike: PropTypes.any,
  itemRevenueBike: PropTypes.any,
  // van
  getItemAmountOrderVan: PropTypes.func,
  getItemAmountOrderMonthVan: PropTypes.func,
  getItemAmountOrderMonthDoneVan: PropTypes.func,
  getItemRevenueVan: PropTypes.func,
  itemAmountOrderVan: PropTypes.any,
  itemAmountOrderMonthVan: PropTypes.any,
  itemAmountOrderMonthDoneVan: PropTypes.any,
  itemRevenueVan: PropTypes.any,
  // line chart
  getItemsLineChart: PropTypes.func,
  itemsLineChart: PropTypes.object,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
}

const mapDispatchToProps = dispatch => ({
  getItemAmountCustomerBus: values =>
    dispatch(DashboardManagementActions.getItemAmountCustomerBus(values)),
  getItemAmountScheduleBus: values =>
    dispatch(DashboardManagementActions.getItemAmountScheduleBus(values)),
  getItemAmountScheduleDoneBus: values =>
    dispatch(DashboardManagementActions.getItemAmountScheduleDoneBus(values)),
  getItemRevenueBus: values =>
    dispatch(DashboardManagementActions.getItemRevenueBus(values)),
  // bike
  getItemAmountOrderBike: values =>
    dispatch(DashboardManagementActions.getItemAmountOrderBike(values)),
  getItemAmountOrderMonthBike: values =>
    dispatch(DashboardManagementActions.getItemAmountOrderMonthBike(values)),
  getItemAmountOrderMonthDoneBike: values =>
    dispatch(
      DashboardManagementActions.getItemAmountOrderMonthDoneBike(values)
    ),
  getItemRevenueBike: values =>
    dispatch(DashboardManagementActions.getItemRevenueBike(values)),
  // van
  getItemAmountOrderVan: values =>
    dispatch(DashboardManagementActions.getItemAmountOrderVan(values)),
  getItemAmountOrderMonthVan: values =>
    dispatch(DashboardManagementActions.getItemAmountOrderMonthVan(values)),
  getItemAmountOrderMonthDoneVan: values =>
    dispatch(DashboardManagementActions.getItemAmountOrderMonthDoneVan(values)),
  getItemRevenueVan: values =>
    dispatch(DashboardManagementActions.getItemRevenueVan(values)),
  // line chart
  getItemsLineChart: values =>
    dispatch(DashboardManagementActions.getItemsLineChart(values)),
  setFilter: filter => dispatch(DashboardManagementActions.setFilter(filter)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(Dashboard)
