import React, { Component, useEffect, useState } from 'react'
import { Card, CardHeader, CardBody, Col, Row } from 'reactstrap'
import CardDashboard from 'components/molecules/CardDashboard'
import FormFilter from 'components/organisms/FormFilter'
import 'Stores/Intercity/RevenueManagement/Reducers'
import 'Stores/Intercity/RevenueManagement/Sagas'
import { RevenueManagementSelectors } from 'Stores/Intercity/RevenueManagement/Selectors'
import { RevenueManagementActions } from 'Stores/Intercity/RevenueManagement/Actions'
import 'Stores/Dashboard/Reducers'
import 'Stores/Dashboard/Sagas'
import { DashboardManagementSelectors } from 'Stores/Dashboard/Selectors'
import { DashboardManagementActions } from 'Stores/Dashboard/Actions'
import 'Stores/ProviderManagement/Reducers'
import 'Stores/ProviderManagement/Sagas'
import { ProviderManagementActions } from 'Stores/ProviderManagement/Actions'
import { ProviderManagementSelectors } from 'Stores/ProviderManagement/Selectors'
import styles from './styles.module.scss'
import { Select } from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import * as config from 'Utils/enum'
import { formatPrice } from 'Utils/helper'
import TableList from 'components/organisms/TableList'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import moment from 'moment'

const columns = [
  {
    title: 'Xe',
    dataIndex: 'licensePlates',
  },
  {
    title: 'Loại xe',
    dataIndex: 'vehicleTypeName',
  },
  {
    title: 'Ngày chạy',
    dataIndex: 'startDate',
    render: text => <div>{moment(text).format('DD-MM-YYYY HH:mm')}</div>,
  },
  {
    title: 'Tuyến',
    dataIndex: 'routeName',
  },
  {
    title: 'Tổng tiền (đ)',
    dataIndex: 'totalPrice',
    render: value => <div>{formatPrice(value)}</div>,
  },
  {
    title: 'Tổng tiền AZGO (đ)',
    dataIndex: 'totalPriceAzgo',
    render: value => <div>{formatPrice(value)}</div>,
  },
]

const RevenueProvider = ({
  getItemAmountCustomerBus,
  itemAmountCustomerBus,
  getItemAmountScheduleBus,
  itemAmountScheduleBus,
  getItemAmountScheduleDoneBus,
  itemAmountScheduleDoneBus,
  getItemRevenueBus,
  itemRevenueBus,
  getItemsRevenueListBus,
  itemsRevenueListBus,
  filter,
  setFilter,
  totalCount,
  showLoading,
  getItemsProvider,
  itemsListProvider,
}) => {
  const [fromDate, setFromDate] = useState('')
  const [endDate, setEndDate] = useState('')
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const [filterAmountCustomerBus, setFilterAmountCustomerBus] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
  })
  const [filterAmountScheduleBus, setFilterAmountScheduleBus] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
    status: null,
  })
  const [
    filterAmountScheduleDoneBus,
    setFilterAmountScheduleDoneBus,
  ] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
    status: config.MANAGEMENT_STATUS_SCHEDULE_BUS.DONE,
  })
  const [filterRevenueBus, setFilterRevenueBus] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
  })
  const callbackHandlerFunction = (fromDate, endDate) => {
    setFromDate(fromDate)
    setEndDate(endDate)
  }
  const handleFormatPrice = data => {
    if (typeof data === 'string') return data
    return formatPrice(data)
  }
  const handleTableChange = pagination => {
    setFilter({
      ...filter.toJS(),
      page: pagination.current,
    })
    getItemsRevenueListBus(filter)
  }
  const handleChangeSelectProvider = value => {
    setFilter({
      ...filter.toJS(),
      page: 1,
      provider_id: value,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }
  useEffect(() => {
    setFilterAmountCustomerBus({
      ...filterAmountCustomerBus,
      from_date: fromDate,
      to_date: endDate,
    })
    setFilterAmountScheduleBus({
      ...filterAmountScheduleBus,
      from_date: fromDate,
      to_date: endDate,
    })
    setFilterAmountScheduleDoneBus({
      ...filterAmountScheduleDoneBus,
      from_date: fromDate,
      to_date: endDate,
    })
    setFilterRevenueBus({
      ...filterRevenueBus,
      from_date: fromDate,
      to_date: endDate,
    })
    setFilter({
      ...filter.toJS(),
      from_date: fromDate,
      to_date: endDate,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [fromDate, endDate])
  useEffect(() => {
    getItemAmountCustomerBus(filterAmountCustomerBus)
  }, [filterAmountCustomerBus])
  useEffect(() => {
    getItemAmountScheduleBus(filterAmountScheduleBus)
  }, [filterAmountScheduleBus])
  useEffect(() => {
    getItemAmountScheduleDoneBus(filterAmountScheduleDoneBus)
  }, [filterAmountScheduleDoneBus])
  useEffect(() => {
    getItemRevenueBus(filterRevenueBus)
  }, [filterRevenueBus])
  useEffect(() => {
    getItemsRevenueListBus(filter)
  }, [filter])
  useEffect(() => {
    getItemsProvider()
    return () => {
      setFromDate('')
      setEndDate('')
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  return (
    <div className="animated fadeIn">
      <Card className={styles.cardFilter}>
        <CardHeader>
          <strong>DOANH THU XE LIÊN TỈNH</strong>
        </CardHeader>
        <CardBody className={styles.cardBodyFilter}>
          <FormFilter handleClickParent={callbackHandlerFunction} />
          <Row>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'success'}
                number={handleFormatPrice(itemAmountCustomerBus)}
                text={'Số lượng khách'}
                icon={'fa-user-plus'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'info'}
                number={handleFormatPrice(itemAmountScheduleBus)}
                text={'Tổng số chuyến'}
                icon={'fa-exchange'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'warning'}
                number={handleFormatPrice(itemAmountScheduleDoneBus)}
                text={'Số chuyến hoàn thành'}
                icon={'fa-check'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'danger'}
                number={handleFormatPrice(itemRevenueBus)}
                text={'Doanh thu'}
                icon={'fa-dollar'}
              />
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Row>
        <Col xs="12" sm="12" lg="12">
          <Card>
            <CardHeader className={styles.cardHeaderSearch}>
              <strong>DOANH THU THEO TỪNG XE</strong>
              <Select
                placeholder="Chọn nhà xe"
                style={{ width: '150px' }}
                defaultValue=""
                onChange={handleChangeSelectProvider}>
                <Select.Option value=""> Tất cả</Select.Option>
                {itemsListProvider.map(item => (
                  <Select.Option
                    value={item.get('id')}
                    key={`provider_${item.get('id')}`}>
                    {item.get('providerName')}
                  </Select.Option>
                ))}
              </Select>
            </CardHeader>
            <CardBody>
              <TableList
                columns={columns}
                dataSource={itemsRevenueListBus}
                totalCount={totalCount}
                showLoading={showLoading}
                pagination={pagination}
                handleTableChange={handleTableChange}
              />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

RevenueProvider.propTypes = {
  getItemAmountCustomerBus: PropTypes.func,
  getItemAmountScheduleBus: PropTypes.func,
  getItemAmountScheduleDoneBus: PropTypes.func,
  getItemRevenueBus: PropTypes.func,
  getItemsRevenueListBus: PropTypes.func,
  itemAmountCustomerBus: PropTypes.any,
  itemAmountScheduleBus: PropTypes.any,
  itemAmountScheduleDoneBus: PropTypes.any,
  itemRevenueBus: PropTypes.any,
  itemsRevenueListBus: PropTypes.object,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
  totalCount: PropTypes.number,
  showLoading: PropTypes.bool,
  getItemsProvider: PropTypes.func,
  itemsListProvider: PropTypes.object,
}
const mapStateToProps = state => ({
  itemAmountCustomerBus: DashboardManagementSelectors.getItemAmountCustomerBus(
    state
  ),
  itemAmountScheduleBus: DashboardManagementSelectors.getItemAmountScheduleBus(
    state
  ),
  itemAmountScheduleDoneBus: DashboardManagementSelectors.getItemAmountScheduleDoneBus(
    state
  ),
  itemRevenueBus: DashboardManagementSelectors.getItemRevenueBus(state),
  itemsRevenueListBus: RevenueManagementSelectors.getItems(state),
  filter: RevenueManagementSelectors.getFilter(state),
  totalCount: RevenueManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
  itemsListProvider: ProviderManagementSelectors.getItemsList(state),
})

const mapDispatchToProps = dispatch => ({
  getItemAmountCustomerBus: values =>
    dispatch(DashboardManagementActions.getItemAmountCustomerBus(values)),
  getItemAmountScheduleBus: values =>
    dispatch(DashboardManagementActions.getItemAmountScheduleBus(values)),
  getItemAmountScheduleDoneBus: values =>
    dispatch(DashboardManagementActions.getItemAmountScheduleDoneBus(values)),
  getItemRevenueBus: values =>
    dispatch(DashboardManagementActions.getItemRevenueBus(values)),
  getItemsRevenueListBus: values =>
    dispatch(RevenueManagementActions.getItemsRequest(values)),
  setFilter: filter => dispatch(RevenueManagementActions.setFilter(filter)),
  getItemsProvider: values =>
    dispatch(ProviderManagementActions.getItemsListRequest(values)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(RevenueProvider)
