import React, { Component, useEffect, useState } from 'react'
import { Line } from 'react-chartjs-2'
import { Card, CardHeader, CardBody, CardTitle, Col, Row } from 'reactstrap'
import { Select, Table } from 'antd'
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips'
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import { withRouter } from 'react-router-dom'
import CardDashboard from 'components/molecules/CardDashboard'
import 'Stores/Dashboard/Reducers'
import 'Stores/Dashboard/Sagas'
import { DashboardManagementSelectors } from 'Stores/Dashboard/Selectors'
import { DashboardManagementActions } from 'Stores/Dashboard/Actions'
import { formatPrice } from 'Utils/helper'
import TableList from 'components/organisms/TableList'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import moment from 'moment'
import styles from './styles.module.scss'
import FormFilter from 'components/organisms/FormFilter'
import { LoadingSelectors } from 'Stores/Loading/Selectors'

const brandInfo = getStyle('--info')

const mainChartOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
    intersect: true,
    mode: 'index',
    position: 'nearest',
    callbacks: {
      labelColor: function(tooltipItem, chart) {
        return {
          backgroundColor:
            chart.data.datasets[tooltipItem.datasetIndex].borderColor,
        }
      },
    },
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          drawOnChartArea: true,
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          // maxTicksLimit: 5,
          // stepSize: Math.ceil(250 / 5),
          // max: 250,
        },
      },
    ],
  },
  elements: {
    point: {
      radius: 2,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
}

const columns = [
  {
    title: 'Mã chuyến',
    dataIndex: 'route.routeCode',
  },
  {
    title: 'Tên chuyến',
    dataIndex: 'route.name',
  },
  {
    title: 'Xe',
    dataIndex: 'vehicle.licensePlates',
  },
]

const columnsRevenue = [
  {
    title: 'Xe',
    dataIndex: 'licensePlates',
  },
  {
    title: 'Loại xe',
    dataIndex: 'vehicleTypeName',
  },
  {
    title: 'Ngày chạy',
    dataIndex: 'startDate',
    render: text => <div>{moment(text).format('DD-MM-YYYY HH:mm')}</div>,
  },
  {
    title: 'Tổng tiền (đ)',
    dataIndex: 'totalPrice',
    render: value => <div>{formatPrice(value)}</div>,
  },
  {
    title: 'Tổng tiền AZGO (đ)',
    dataIndex: 'totalPriceAzgo',
    render: value => <div>{formatPrice(value)}</div>,
  },
]

const DashboardProvider = ({
  getItemAmountCustomerBus,
  itemAmountCustomerBus,
  getItemAmountScheduleBus,
  itemAmountScheduleBus,
  getItemRevenueBus,
  itemRevenueBus,
  getItemRatingProvider,
  itemRatingProvider,
  // line chart
  getItemsLineChart,
  itemsLineChart,
  filter,
  setFilter,

  // provider
  getItemsVehicleScheduleNearest,
  itemsVehicleScheduleNearest,
  getItemsVehicleScheduleNearestNot,
  itemsVehicleScheduleNearestNot,
  getItemsRevenue,
  itemsRevenue,
  totalCount,
  showLoading,
}) => {
  const dataMonth = []
  for (let i = 1; i <= 12; i++) {
    dataMonth.push({
      name: `Tháng ${i}`,
      value: i,
    })
  }
  const dataYear = []
  var year = new Date().getFullYear()
  for (let i = 2020; i <= year; i++) {
    dataYear.push({
      name: `${i}`,
      value: i,
    })
  }
  const handleChangeSelectYear = value => {
    setFilter({
      ...filter,
      year: value,
    })
  }
  const handleChangeSelectMonth = value => {
    if (value) {
      setFilter({
        ...filter,
        month: value,
      })
    } else {
      setFilter({
        ...filter,
        month: null,
      })
    }
  }
  const [lineChartLabel, setLineChartLabel] = useState([])
  const [lineChartValue, setLineChartValue] = useState([])
  const [fromDate, setFromDate] = useState('')
  const [endDate, setEndDate] = useState('')
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const [mainChart, setMainChart] = useState({
    labels: [],
    datasets: [
      {
        label: 'Doanh thu',
        backgroundColor: hexToRgba(brandInfo, 10),
        borderColor: brandInfo,
        pointHoverBackgroundColor: '#fff',
        borderWidth: 2,
        data: [],
      },
    ],
  })
  const callbackHandlerFunction = (fromDate, endDate) => {
    setFromDate(fromDate)
    setEndDate(endDate)
  }
  const handleFormatPrice = data => {
    if (typeof data === 'string') return data
    return formatPrice(data)
  }
  const handleTableChange = pagination => {
    setFilterRevenue({
      ...filterRevenue,
      page: pagination.current,
    })
    getItemsRevenue(filterRevenue)
  }
  const [filterAmountCustomerBus, setFilterAmountCustomerBus] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
  })
  const [filterAmountScheduleBus, setFilterAmountScheduleBus] = useState({
    from_date: '',
    to_date: '',
    is_in_month: true,
    status: null,
  })
  const [filterRevenueBus, setFilterRevenueBus] = useState({
    from_date: '',
    to_date: '',
    is_in_month: true,
  })
  const [
    filterVehicleScheduleNearest,
    setFilterVehicleScheduleNearest,
  ] = useState({
    limit: 5,
    status: 2,
  })
  const [
    filterVehicleScheduleNearestNot,
    setFilterVehicleScheduleNearestNot,
  ] = useState({
    limit: 5,
    status: 1,
  })
  const [filterRevenue, setFilterRevenue] = useState({
    per_page: 10,
    page: 1,
    provider_id: '',
    is_in_month: false,
    to_date: '',
    from_date: '',
  })
  useEffect(() => {
    setFilterRevenue({
      ...filterRevenue,
      from_date: fromDate,
      to_date: endDate,
      page: 1,
    })
  }, [fromDate, endDate])
  useEffect(() => {
    getItemsRevenue(filterRevenue)
  }, [filterRevenue])
  useEffect(() => {
    getItemsRevenue(filterRevenue)
    return () => {
      setFromDate('')
      setEndDate('')
    }
  }, [])
  useEffect(() => {
    getItemAmountCustomerBus(filterAmountCustomerBus)
    getItemAmountScheduleBus(filterAmountScheduleBus)
    getItemRevenueBus(filterRevenueBus)
    getItemRatingProvider()
    // line chart
    setFilter({
      month: null,
      year: year,
    })

    // provider
    getItemsVehicleScheduleNearest(filterVehicleScheduleNearest)
    getItemsVehicleScheduleNearestNot(filterVehicleScheduleNearestNot)
  }, [])
  useEffect(() => {
    getItemsLineChart(filter)
  }, [filter])
  useEffect(() => {
    if (itemsLineChart) {
      if (!filter.get('month')) {
        setLineChartLabel([])
        setLineChartValue([])
        for (let i = 1; i <= 12; i++) {
          lineChartLabel.push(`Tháng ${i}`)
          lineChartValue.push(0)
        }
        for (let j = 0; j < itemsLineChart.toJS().length; j++) {
          lineChartValue.splice(
            itemsLineChart.toJS()[j].month - 1,
            1,
            itemsLineChart.toJS()[j].value
          )
        }
      } else {
        let month = `${filter.get('year')}-${filter.get('month')}`
        let dayOfMonth = moment(month, 'YYYY-MM').daysInMonth()
        setLineChartLabel([])
        setLineChartValue([])
        for (let i = 1; i <= dayOfMonth; i++) {
          lineChartLabel.push(`${i}`)
          lineChartValue.push(0)
        }
        for (let j = 0; j < itemsLineChart.toJS().length; j++) {
          lineChartValue.splice(
            itemsLineChart.toJS()[j].day - 1,
            1,
            itemsLineChart.toJS()[j].value
          )
        }
      }
    }
    setMainChart({
      labels: lineChartLabel,
      datasets: [
        {
          label: 'Doanh thu',
          backgroundColor: hexToRgba(brandInfo, 10),
          borderColor: brandInfo,
          pointHoverBackgroundColor: '#fff',
          borderWidth: 2,
          data: lineChartValue,
        },
      ],
    })
  }, [itemsLineChart])
  return (
    <div className="animated fadeIn">
      <Row>
        <Col xs="12" sm="6" lg="3">
          <CardDashboard
            classColor={'success'}
            number={handleFormatPrice(itemAmountCustomerBus)}
            text={'Số lượng khách'}
            icon={'fa-user-plus'}
          />
        </Col>
        <Col xs="12" sm="6" lg="3">
          <CardDashboard
            classColor={'info'}
            number={handleFormatPrice(itemAmountScheduleBus)}
            text={'Tổng số chuyến trong tháng'}
            icon={'fa-exchange'}
          />
        </Col>
        <Col xs="12" sm="6" lg="3">
          <CardDashboard
            classColor={'danger'}
            number={handleFormatPrice(itemRevenueBus)}
            text={'Doanh thu trong tháng'}
            icon={'fa-dollar'}
          />
        </Col>
        <Col xs="12" sm="6" lg="3">
          <CardDashboard
            classColor={'warning'}
            number={itemRatingProvider ? `${itemRatingProvider}/5` : `0/5`}
            text={'Đánh giá'}
            icon={'fa-star'}
          />
        </Col>
      </Row>
      <Row>
        <Col>
          <Card>
            <CardBody>
              <Row>
                <Col sm="5">
                  <CardTitle className="mb-0">DOANH THU TỔNG</CardTitle>
                  <div className="small text-muted">
                    {filter.get('month') ? (
                      <span>Tháng {filter.get('month')} - </span>
                    ) : null}
                    Năm {filter.get('year')}
                  </div>
                </Col>
                <Col sm="7" className="d-none d-sm-inline-block">
                  <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <Select
                      placeholder="Chọn tháng"
                      style={{ width: '100px', marginRight: '10px' }}
                      defaultValue=""
                      onChange={handleChangeSelectMonth}>
                      <Select.Option value=""> None</Select.Option>
                      {dataMonth.map(item => (
                        <Select.Option value={item.value} key={item.value}>
                          {item.name}
                        </Select.Option>
                      ))}
                    </Select>
                    <Select
                      placeholder="Chọn năm"
                      style={{ width: '100px' }}
                      defaultValue={year}
                      onChange={handleChangeSelectYear}>
                      {dataYear.map(item => (
                        <Select.Option value={item.value} key={item.value}>
                          {item.name}
                        </Select.Option>
                      ))}
                    </Select>
                  </div>
                </Col>
              </Row>
              <div
                className="chart-wrapper"
                style={{ height: 300 + 'px', marginTop: 40 + 'px' }}>
                <Line data={mainChart} options={mainChartOpts} height={300} />
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col xs="12" sm="12" lg="6">
          <Card>
            <CardHeader>5 chuyến xe đang chạy gần nhất</CardHeader>
            <CardBody>
              <Table
                columns={columns}
                rowKey={record => record.createdAt}
                dataSource={
                  itemsVehicleScheduleNearest
                    ? itemsVehicleScheduleNearest.toJS()
                    : null
                }
                pagination={false}
              />
            </CardBody>
          </Card>
        </Col>
        <Col xs="12" sm="12" lg="6">
          <Card>
            <CardHeader>5 chuyến xe sắp chạy gần nhất</CardHeader>
            <CardBody>
              <Table
                columns={columns}
                rowKey={record => record.createdAt}
                dataSource={
                  itemsVehicleScheduleNearestNot
                    ? itemsVehicleScheduleNearestNot.toJS()
                    : null
                }
                pagination={false}
              />
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col xs="12" sm="12" lg="12">
          <Card>
            <CardHeader className={styles.cardHeaderSearch}>
              <strong>DOANH THU THEO TỪNG XE</strong>
            </CardHeader>
            <CardBody>
              <FormFilter handleClickParent={callbackHandlerFunction} />
              <TableList
                columns={columnsRevenue}
                dataSource={itemsRevenue}
                totalCount={totalCount}
                showLoading={showLoading}
                pagination={pagination}
                handleTableChange={handleTableChange}
              />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}
const mapStateToProps = state => ({
  itemAmountCustomerBus: DashboardManagementSelectors.getItemAmountCustomerBus(
    state
  ),
  itemAmountScheduleBus: DashboardManagementSelectors.getItemAmountScheduleBus(
    state
  ),
  itemRevenueBus: DashboardManagementSelectors.getItemRevenueBus(state),
  itemRatingProvider: DashboardManagementSelectors.getItemRatingProvider(state),
  // line chart
  itemsLineChart: DashboardManagementSelectors.getItemsLineChartProvider(state),
  filter: DashboardManagementSelectors.getFilter(state),

  // provider
  itemsVehicleScheduleNearest: DashboardManagementSelectors.getItemsVehicleScheduleNearest(
    state
  ),
  itemsVehicleScheduleNearestNot: DashboardManagementSelectors.getItemsVehicleScheduleNearestNot(
    state
  ),
  itemsRevenue: DashboardManagementSelectors.getItemsRevenue(state),
  totalCount: DashboardManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})
DashboardProvider.propTypes = {
  getItemAmountCustomerBus: PropTypes.func,
  itemAmountCustomerBus: PropTypes.any,
  getItemAmountScheduleBus: PropTypes.func,
  itemAmountScheduleBus: PropTypes.any,
  getItemRevenueBus: PropTypes.func,
  itemRevenueBus: PropTypes.any,
  getItemRatingProvider: PropTypes.func,
  itemRatingProvider: PropTypes.any,
  // line chart
  getItemsLineChart: PropTypes.func,
  itemsLineChart: PropTypes.object,
  filter: PropTypes.object,
  setFilter: PropTypes.func,

  // provider
  getItemsVehicleScheduleNearest: PropTypes.func,
  itemsVehicleScheduleNearest: PropTypes.object,
  getItemsVehicleScheduleNearestNot: PropTypes.func,
  itemsVehicleScheduleNearestNot: PropTypes.object,
  getItemsRevenue: PropTypes.func,
  itemsRevenue: PropTypes.object,
  totalCount: PropTypes.number,
  showLoading: PropTypes.bool,
}
const mapDispatchToProps = dispatch => ({
  getItemAmountCustomerBus: values =>
    dispatch(DashboardManagementActions.getItemAmountCustomerBus(values)),
  getItemAmountScheduleBus: values =>
    dispatch(DashboardManagementActions.getItemAmountScheduleBus(values)),
  getItemRevenueBus: values =>
    dispatch(DashboardManagementActions.getItemRevenueBus(values)),
  getItemRatingProvider: () =>
    dispatch(DashboardManagementActions.getItemRatingProvider()),
  // line chart
  getItemsLineChart: values =>
    dispatch(DashboardManagementActions.getItemsLineChartProvider(values)),
  setFilter: filter => dispatch(DashboardManagementActions.setFilter(filter)),

  //  provider
  getItemsVehicleScheduleNearest: values =>
    dispatch(DashboardManagementActions.getItemsVehicleScheduleNearest(values)),
  getItemsVehicleScheduleNearestNot: values =>
    dispatch(
      DashboardManagementActions.getItemsVehicleScheduleNearestNot(values)
    ),
  getItemsRevenue: values =>
    dispatch(DashboardManagementActions.getItemsRevenueRequest(values)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(DashboardProvider)
