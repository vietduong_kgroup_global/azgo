import React, { Component, useEffect, useState } from 'react'
import { Card, CardHeader, CardBody, Col, Row } from 'reactstrap'
import CardDashboard from 'components/molecules/CardDashboard'
import FormFilter from 'components/organisms/FormFilter'
import 'Stores/Dashboard/Reducers'
import 'Stores/Dashboard/Sagas'
import { DashboardManagementSelectors } from 'Stores/Dashboard/Selectors'
import { DashboardManagementActions } from 'Stores/Dashboard/Actions'
import 'Stores/BikeCar/RevenueManagement/Reducers'
import 'Stores/BikeCar/RevenueManagement/Sagas'
import { RevenueManagementBikeSelectors } from 'Stores/BikeCar/RevenueManagement/Selectors'
import { RevenueManagementBikeActions } from 'Stores/BikeCar/RevenueManagement/Actions'
import styles from './styles.module.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { formatPrice } from 'Utils/helper'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import TableList from 'components/organisms/TableList'
import moment from 'moment'
import * as config from 'Utils/enum'

const columns = [
  {
    title: 'Xe',
    dataIndex: 'licensePlates',
  },
  {
    title: 'Loại xe',
    dataIndex: 'vehicleTypeName',
  },
  {
    title: 'Doanh thu (đ)',
    dataIndex: 'totalPrice',
    render: value => <div>{formatPrice(value)}</div>,
  },
  {
    title: 'Doanh thu AZGO (đ)',
    dataIndex: 'totalPriceAzgo',
    render: value => <div>{formatPrice(value)}</div>,
  },
  {
    title: 'Ngày tạo',
    dataIndex: 'createdAt',
    render: value => <div>{moment(value).format('DD-MM-YYYY HH:mm')}</div>,
  },
]
const RevenueBikeCar = ({
  getItemAmountOrderBike,
  itemAmountOrderBike,
  getItemAmountOrderMonthBike,
  itemAmountOrderMonthBike,
  getItemAmountOrderMonthDoneBike,
  itemAmountOrderMonthDoneBike,
  getItemRevenueBike,
  itemRevenueBike,
  getItemsRevenueListBike,
  itemsRevenueListBike,
  filter,
  setFilter,
  showLoading,
  totalCount,
}) => {
  const [fromDate, setFromDate] = useState('')
  const [endDate, setEndDate] = useState('')
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const [filterAmountOrderBike, setFilterAmountOrderBike] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
  })
  const [filterAmountOrderMonthBike, setFilterAmountOrderMonthBike] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
  })
  const [
    filterAmountOrderMonthDoneBike,
    setFilterAmountOrderMonthDoneBike,
  ] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
    status: config.MANAGEMENT_STATUS_TRIP.COMPLETED,
  })
  const [filterRevenueBike, setFilterRevenueBike] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
  })
  const callbackHandlerFunction = (fromDate, endDate) => {
    setFromDate(fromDate)
    setEndDate(endDate)
  }
  const handleFormatPrice = data => {
    if (typeof data === 'string') return data
    return formatPrice(data)
  }
  const handleTableChange = pagination => {
    setFilter({
      ...filter.toJS(),
      page: pagination.current,
    })
    getItemsRevenueListBike(filter)
  }
  useEffect(() => {
    setFilterAmountOrderBike({
      ...filterAmountOrderBike,
      from_date: fromDate,
      to_date: endDate,
    })
    setFilterAmountOrderMonthBike({
      ...filterAmountOrderMonthBike,
      from_date: fromDate,
      to_date: endDate,
    })
    setFilterAmountOrderMonthDoneBike({
      ...filterAmountOrderMonthDoneBike,
      from_date: fromDate,
      to_date: endDate,
    })
    setFilterRevenueBike({
      ...filterRevenueBike,
      from_date: fromDate,
      to_date: endDate,
    })
    setFilter({
      ...filter.toJS(),
      from_date: fromDate,
      to_date: endDate,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [fromDate, endDate])
  useEffect(() => {
    getItemAmountOrderBike(filterAmountOrderBike)
    getItemAmountOrderMonthBike(filterAmountOrderMonthBike)
    getItemAmountOrderMonthDoneBike(filterAmountOrderMonthDoneBike)
    getItemRevenueBike(filterRevenueBike)
    getItemsRevenueListBike(filter)
  }, [
    filterAmountOrderBike,
    filterAmountOrderMonthBike,
    filterAmountOrderMonthDoneBike,
    filterRevenueBike,
    filter,
  ])

  useEffect(() => {
    getItemsRevenueListBike(filter)
    return () => {
      setFromDate('')
      setEndDate('')
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  return (
    <div className="animated fadeIn">
      <Card className={styles.cardFilter}>
        <CardHeader>
          <strong>DOANH THU BIKE/CAR</strong>
        </CardHeader>
        <CardBody className={styles.cardBodyFilter}>
          <FormFilter handleClickParent={callbackHandlerFunction} />
          <Row>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'success'}
                number={handleFormatPrice(itemAmountOrderBike)}
                text={'Số lượng khách'}
                icon={'fa-user-plus'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'info'}
                number={handleFormatPrice(itemAmountOrderMonthBike)}
                text={'Tổng số chuyến'}
                icon={'fa-exchange'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'warning'}
                number={handleFormatPrice(itemAmountOrderMonthDoneBike)}
                text={'Số chuyến hoàn thành'}
                icon={'fa-check'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'danger'}
                number={handleFormatPrice(itemRevenueBike)}
                text={'Doanh thu'}
                icon={'fa-dollar'}
              />
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Row>
        <Col xs="12" sm="12" lg="12">
          <Card>
            <CardHeader className={styles.cardHeaderSearch}>
              <strong>DOANH THU THEO TỪNG XE</strong>
            </CardHeader>
            <CardBody>
              <TableList
                columns={columns}
                dataSource={itemsRevenueListBike}
                totalCount={totalCount}
                showLoading={showLoading}
                pagination={pagination}
                handleTableChange={handleTableChange}
              />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

const mapStateToProps = state => ({
  itemAmountOrderBike: DashboardManagementSelectors.getItemAmountOrderBike(
    state
  ),
  itemAmountOrderMonthBike: DashboardManagementSelectors.getItemAmountOrderMonthBike(
    state
  ),
  itemAmountOrderMonthDoneBike: DashboardManagementSelectors.getItemAmountOrderMonthDoneBike(
    state
  ),
  itemRevenueBike: DashboardManagementSelectors.getItemRevenueBike(state),
  itemsRevenueListBike: RevenueManagementBikeSelectors.getItems(state),
  filter: RevenueManagementBikeSelectors.getFilter(state),
  showLoading: LoadingSelectors.getLoadingList(state),
  totalCount: RevenueManagementBikeSelectors.getTotalCount(state),
})

const mapDispatchToProps = dispatch => ({
  getItemAmountOrderBike: values =>
    dispatch(DashboardManagementActions.getItemAmountOrderBike(values)),
  getItemAmountOrderMonthBike: values =>
    dispatch(DashboardManagementActions.getItemAmountOrderMonthBike(values)),
  getItemAmountOrderMonthDoneBike: values =>
    dispatch(
      DashboardManagementActions.getItemAmountOrderMonthDoneBike(values)
    ),
  getItemRevenueBike: values =>
    dispatch(DashboardManagementActions.getItemRevenueBike(values)),
  getItemsRevenueListBike: values =>
    dispatch(RevenueManagementBikeActions.getItemsRequest(values)),
  setFilter: filter => dispatch(RevenueManagementBikeActions.setFilter(filter)),
})

RevenueBikeCar.propTypes = {
  getItemAmountOrderBike: PropTypes.func,
  getItemAmountOrderMonthBike: PropTypes.func,
  getItemAmountOrderMonthDoneBike: PropTypes.func,
  getItemRevenueBike: PropTypes.func,
  itemAmountOrderBike: PropTypes.any,
  itemAmountOrderMonthBike: PropTypes.any,
  itemAmountOrderMonthDoneBike: PropTypes.any,
  itemRevenueBike: PropTypes.any,
  itemsRevenueListBike: PropTypes.object,
  getItemsRevenueListBike: PropTypes.func,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
  totalCount: PropTypes.number,
  showLoading: PropTypes.bool,
}

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(RevenueBikeCar)
