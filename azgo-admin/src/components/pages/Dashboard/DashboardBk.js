import React, { Component, useState } from 'react'
import { Line } from 'react-chartjs-2'
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Progress,
  Row,
  Table,
} from 'reactstrap'
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips'
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'

const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandDanger = getStyle('--danger')

// Random Numbers
function random(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

var elements = 27
var data1 = []
var data2 = []
var data3 = []

for (var i = 0; i <= elements; i++) {
  data1.push(random(50, 200))
  data2.push(random(80, 100))
  data3.push(65)
}

const mainChart = {
  labels: [
    'Mo',
    'Tu',
    'We',
    'Th',
    'Fr',
    'Sa',
    'Su',
    'Mo',
    'Tu',
    'We',
    'Th',
    'Fr',
    'Sa',
    'Su',
    'Mo',
    'Tu',
    'We',
    'Th',
    'Fr',
    'Sa',
    'Su',
    'Mo',
    'Tu',
    'We',
    'Th',
    'Fr',
    'Sa',
    'Su',
  ],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: hexToRgba(brandInfo, 10),
      borderColor: brandInfo,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 2,
      data: data1,
    },
    {
      label: 'My Second dataset',
      backgroundColor: 'transparent',
      borderColor: brandSuccess,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 2,
      data: data2,
    },
    {
      label: 'My Third dataset',
      backgroundColor: 'transparent',
      borderColor: brandDanger,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 1,
      borderDash: [8, 5],
      data: data3,
    },
  ],
}

const mainChartOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
    intersect: true,
    mode: 'index',
    position: 'nearest',
    callbacks: {
      labelColor: function(tooltipItem, chart) {
        return {
          backgroundColor:
            chart.data.datasets[tooltipItem.datasetIndex].borderColor,
        }
      },
    },
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          drawOnChartArea: false,
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 5,
          stepSize: Math.ceil(250 / 5),
          max: 250,
        },
      },
    ],
  },
  elements: {
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
}

const Dashboard = () => {
  const [radioSelected, setRadioSelected] = useState(2)
  const onRadioBtnClick = radioSelected => {
    setRadioSelected(radioSelected)
  }
  return (
    <div className="animated fadeIn">
      <Row>
        <Col xs="12" sm="6" lg="3">
          <Card>
            <CardBody className="clearfix p-3 card-body">
              <i className="fa fa-user-plus bg-success p-3 mr-3 float-left" />
              <div className="h5 mb-0 text-success mt-2">10</div>
              <div className="text-muted text-uppercase font-weight-bold font-xs">
                User mới
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col xs="12" sm="6" lg="3">
          <Card>
            <CardBody className="clearfix p-3 card-body">
              <i className="fa fa-exchange bg-info p-3 mr-3 float-left" />
              <div className="h5 mb-0 text-info mt-2">583</div>
              <div className="text-muted text-uppercase font-weight-bold font-xs">
                Số chuyến
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col xs="12" sm="6" lg="3">
          <Card>
            <CardBody className="clearfix p-3 card-body">
              <i className="fa fa-motorcycle bg-warning p-3 mr-3 float-left" />
              <div className="h5 mb-0 text-warning mt-2">1576</div>
              <div className="text-muted text-uppercase font-weight-bold font-xs">
                Tổng tài xế
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col xs="12" sm="6" lg="3">
          <Card>
            <CardBody className="clearfix p-3 card-body">
              <i className="fa fa-dollar bg-danger p-3 mr-3 float-left" />
              <div className="h5 mb-0 text-danger mt-2">$1.999,50</div>
              <div className="text-muted text-uppercase font-weight-bold font-xs">
                Chi phí
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col>
          <Card>
            <CardBody>
              <Row>
                <Col sm="5">
                  <CardTitle className="mb-0">Traffic</CardTitle>
                  <div className="small text-muted">November 2019</div>
                </Col>
                <Col sm="7" className="d-none d-sm-inline-block">
                  <ButtonToolbar
                    className="float-right"
                    aria-label="Toolbar with button groups">
                    <ButtonGroup className="mr-3" aria-label="First group">
                      <Button
                        color="outline-secondary"
                        onClick={() => onRadioBtnClick(1)}
                        active={radioSelected === 1}>
                        Day
                      </Button>
                      <Button
                        color="outline-secondary"
                        onClick={() => onRadioBtnClick(2)}
                        active={radioSelected === 2}>
                        Month
                      </Button>
                      <Button
                        color="outline-secondary"
                        onClick={() => onRadioBtnClick(3)}
                        active={radioSelected === 3}>
                        Year
                      </Button>
                    </ButtonGroup>
                  </ButtonToolbar>
                </Col>
              </Row>
              <div
                className="chart-wrapper"
                style={{ height: 300 + 'px', marginTop: 40 + 'px' }}>
                <Line data={mainChart} options={mainChartOpts} height={300} />
              </div>
            </CardBody>
            <CardFooter>
              <Row className="text-center">
                <Col sm={12} md className="mb-sm-2 mb-0">
                  <div className="text-muted">Visits</div>
                  <strong>29.703 Users (40%)</strong>
                  <Progress
                    className="progress-xs mt-2"
                    color="success"
                    value="40"
                  />
                </Col>
                <Col sm={12} md className="mb-sm-2 mb-0 d-md-down-none">
                  <div className="text-muted">Unique</div>
                  <strong>24.093 Users (20%)</strong>
                  <Progress
                    className="progress-xs mt-2"
                    color="info"
                    value="20"
                  />
                </Col>
                <Col sm={12} md className="mb-sm-2 mb-0">
                  <div className="text-muted">Pageviews</div>
                  <strong>78.706 Views (60%)</strong>
                  <Progress
                    className="progress-xs mt-2"
                    color="warning"
                    value="60"
                  />
                </Col>
                <Col sm={12} md className="mb-sm-2 mb-0">
                  <div className="text-muted">New Users</div>
                  <strong>22.123 Users (80%)</strong>
                  <Progress
                    className="progress-xs mt-2"
                    color="danger"
                    value="80"
                  />
                </Col>
                <Col sm={12} md className="mb-sm-2 mb-0 d-md-down-none">
                  <div className="text-muted">Bounce Rate</div>
                  <strong>Average Rate (40.15%)</strong>
                  <Progress
                    className="progress-xs mt-2"
                    color="primary"
                    value="40"
                  />
                </Col>
              </Row>
            </CardFooter>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col>
          <Card>
            <CardHeader>User</CardHeader>
            <CardBody>
              <Table
                hover
                responsive
                className="table-outline mb-0 d-none d-sm-table">
                <thead className="thead-light">
                  <tr>
                    <th className="text-center">
                      <i className="icon-people" />
                    </th>
                    <th>User</th>
                    <th className="text-center">Country</th>
                    <th>Usage</th>
                    <th className="text-center">Payment Method</th>
                    <th>Activity</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="text-center">
                      <div className="avatar">
                        <img
                          src={'assets/img/avatars/1.jpg'}
                          className="img-avatar"
                          alt="admin@bootstrapmaster.com"
                        />
                        <span className="avatar-status badge-success" />
                      </div>
                    </td>
                    <td>
                      <div>Yiorgos Avraamu</div>
                      <div className="small text-muted">
                        <span>New</span> | Registered: Jan 1, 2015
                      </div>
                    </td>
                    <td className="text-center">
                      <i
                        className="flag-icon flag-icon-us h4 mb-0"
                        title="us"
                        id="us"
                      />
                    </td>
                    <td>
                      <div className="clearfix">
                        <div className="float-left">
                          <strong>50%</strong>
                        </div>
                        <div className="float-right">
                          <small className="text-muted">
                            Jun 11, 2015 - Jul 10, 2015
                          </small>
                        </div>
                      </div>
                      <Progress
                        className="progress-xs"
                        color="success"
                        value="50"
                      />
                    </td>
                    <td className="text-center">
                      <i
                        className="fa fa-cc-mastercard"
                        style={{ fontSize: 24 + 'px' }}
                      />
                    </td>
                    <td>
                      <div className="small text-muted">Last login</div>
                      <strong>10 sec ago</strong>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div className="avatar">
                        <img
                          src={'assets/img/avatars/2.jpg'}
                          className="img-avatar"
                          alt="admin@bootstrapmaster.com"
                        />
                        <span className="avatar-status badge-danger" />
                      </div>
                    </td>
                    <td>
                      <div>Avram Tarasios</div>
                      <div className="small text-muted">
                        <span>Recurring</span> | Registered: Jan 1, 2015
                      </div>
                    </td>
                    <td className="text-center">
                      <i
                        className="flag-icon flag-icon-br h4 mb-0"
                        title="br"
                        id="br"
                      />
                    </td>
                    <td>
                      <div className="clearfix">
                        <div className="float-left">
                          <strong>10%</strong>
                        </div>
                        <div className="float-right">
                          <small className="text-muted">
                            Jun 11, 2015 - Jul 10, 2015
                          </small>
                        </div>
                      </div>
                      <Progress
                        className="progress-xs"
                        color="info"
                        value="10"
                      />
                    </td>
                    <td className="text-center">
                      <i
                        className="fa fa-cc-visa"
                        style={{ fontSize: 24 + 'px' }}
                      />
                    </td>
                    <td>
                      <div className="small text-muted">Last login</div>
                      <strong>5 minutes ago</strong>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div className="avatar">
                        <img
                          src={'assets/img/avatars/3.jpg'}
                          className="img-avatar"
                          alt="admin@bootstrapmaster.com"
                        />
                        <span className="avatar-status badge-warning" />
                      </div>
                    </td>
                    <td>
                      <div>Quintin Ed</div>
                      <div className="small text-muted">
                        <span>New</span> | Registered: Jan 1, 2015
                      </div>
                    </td>
                    <td className="text-center">
                      <i
                        className="flag-icon flag-icon-in h4 mb-0"
                        title="in"
                        id="in"
                      />
                    </td>
                    <td>
                      <div className="clearfix">
                        <div className="float-left">
                          <strong>74%</strong>
                        </div>
                        <div className="float-right">
                          <small className="text-muted">
                            Jun 11, 2015 - Jul 10, 2015
                          </small>
                        </div>
                      </div>
                      <Progress
                        className="progress-xs"
                        color="warning"
                        value="74"
                      />
                    </td>
                    <td className="text-center">
                      <i
                        className="fa fa-cc-stripe"
                        style={{ fontSize: 24 + 'px' }}
                      />
                    </td>
                    <td>
                      <div className="small text-muted">Last login</div>
                      <strong>1 hour ago</strong>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div className="avatar">
                        <img
                          src={'assets/img/avatars/4.jpg'}
                          className="img-avatar"
                          alt="admin@bootstrapmaster.com"
                        />
                        <span className="avatar-status badge-secondary" />
                      </div>
                    </td>
                    <td>
                      <div>Enéas Kwadwo</div>
                      <div className="small text-muted">
                        <span>New</span> | Registered: Jan 1, 2015
                      </div>
                    </td>
                    <td className="text-center">
                      <i
                        className="flag-icon flag-icon-fr h4 mb-0"
                        title="fr"
                        id="fr"
                      />
                    </td>
                    <td>
                      <div className="clearfix">
                        <div className="float-left">
                          <strong>98%</strong>
                        </div>
                        <div className="float-right">
                          <small className="text-muted">
                            Jun 11, 2015 - Jul 10, 2015
                          </small>
                        </div>
                      </div>
                      <Progress
                        className="progress-xs"
                        color="danger"
                        value="98"
                      />
                    </td>
                    <td className="text-center">
                      <i
                        className="fa fa-paypal"
                        style={{ fontSize: 24 + 'px' }}
                      />
                    </td>
                    <td>
                      <div className="small text-muted">Last login</div>
                      <strong>Last month</strong>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div className="avatar">
                        <img
                          src={'assets/img/avatars/5.jpg'}
                          className="img-avatar"
                          alt="admin@bootstrapmaster.com"
                        />
                        <span className="avatar-status badge-success" />
                      </div>
                    </td>
                    <td>
                      <div>Agapetus Tadeáš</div>
                      <div className="small text-muted">
                        <span>New</span> | Registered: Jan 1, 2015
                      </div>
                    </td>
                    <td className="text-center">
                      <i
                        className="flag-icon flag-icon-es h4 mb-0"
                        title="es"
                        id="es"
                      />
                    </td>
                    <td>
                      <div className="clearfix">
                        <div className="float-left">
                          <strong>22%</strong>
                        </div>
                        <div className="float-right">
                          <small className="text-muted">
                            Jun 11, 2015 - Jul 10, 2015
                          </small>
                        </div>
                      </div>
                      <Progress
                        className="progress-xs"
                        color="info"
                        value="22"
                      />
                    </td>
                    <td className="text-center">
                      <i
                        className="fa fa-google-wallet"
                        style={{ fontSize: 24 + 'px' }}
                      />
                    </td>
                    <td>
                      <div className="small text-muted">Last login</div>
                      <strong>Last week</strong>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div className="avatar">
                        <img
                          src={'assets/img/avatars/6.jpg'}
                          className="img-avatar"
                          alt="admin@bootstrapmaster.com"
                        />
                        <span className="avatar-status badge-danger" />
                      </div>
                    </td>
                    <td>
                      <div>Friderik Dávid</div>
                      <div className="small text-muted">
                        <span>New</span> | Registered: Jan 1, 2015
                      </div>
                    </td>
                    <td className="text-center">
                      <i
                        className="flag-icon flag-icon-pl h4 mb-0"
                        title="pl"
                        id="pl"
                      />
                    </td>
                    <td>
                      <div className="clearfix">
                        <div className="float-left">
                          <strong>43%</strong>
                        </div>
                        <div className="float-right">
                          <small className="text-muted">
                            Jun 11, 2015 - Jul 10, 2015
                          </small>
                        </div>
                      </div>
                      <Progress
                        className="progress-xs"
                        color="success"
                        value="43"
                      />
                    </td>
                    <td className="text-center">
                      <i
                        className="fa fa-cc-amex"
                        style={{ fontSize: 24 + 'px' }}
                      />
                    </td>
                    <td>
                      <div className="small text-muted">Last login</div>
                      <strong>Yesterday</strong>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default Dashboard
