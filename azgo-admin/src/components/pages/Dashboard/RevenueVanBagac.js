import React, { Component, useEffect, useState } from 'react'
import { Card, CardHeader, CardBody, Col, Row } from 'reactstrap'
import CardDashboard from 'components/molecules/CardDashboard'
import FormFilter from 'components/organisms/FormFilter'
import 'Stores/Dashboard/Reducers'
import 'Stores/Dashboard/Sagas'
import { DashboardManagementSelectors } from 'Stores/Dashboard/Selectors'
import { DashboardManagementActions } from 'Stores/Dashboard/Actions'
import 'Stores/Van/RevenueManagement/Reducers'
import 'Stores/Van/RevenueManagement/Sagas'
import { RevenueManagementVanSelectors } from 'Stores/Van/RevenueManagement/Selectors'
import { RevenueManagementVanActions } from 'Stores/Van/RevenueManagement/Actions'
import styles from './styles.module.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import * as config from 'Utils/enum'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import TableList from 'components/organisms/TableList'
import { formatPrice } from 'Utils/helper'
import moment from 'moment'

const columns = [
  {
    title: 'Xe',
    dataIndex: 'licensePlates',
  },
  {
    title: 'Loại xe',
    dataIndex: 'vehicleTypeName',
  },
  {
    title: 'Doanh thu (đ)',
    dataIndex: 'totalPrice',
    render: value => <div>{formatPrice(value)}</div>,
  },
  {
    title: 'Doanh thu AZGO (đ)',
    dataIndex: 'totalPriceAzgo',
    render: value => <div>{formatPrice(value)}</div>,
  },
  {
    title: 'Ngày tạo',
    dataIndex: 'createdAt',
    render: value => <div>{moment(value).format('DD-MM-YYYY HH:mm')}</div>,
  },
]

const data = []
for (let i = 0; i < 5; i++) {
  data.push({
    key: i,
    vehicleId: '77H3-54645',
    vehicleTypeId: 'Xe máy',
    date: '21-03-2020',
    payment: '2.000.000',
  })
}
const RevenueVanBagac = ({
  getItemAmountOrderVan,
  itemAmountOrderVan,
  getItemAmountOrderMonthVan,
  itemAmountOrderMonthVan,
  getItemAmountOrderMonthDoneVan,
  itemAmountOrderMonthDoneVan,
  getItemRevenueVan,
  itemRevenueVan,
  getItemsRevenueListVan,
  itemsRevenueListVan,
  filter,
  setFilter,
  showLoading,
  totalCount,
}) => {
  const [fromDate, setFromDate] = useState('')
  const [endDate, setEndDate] = useState('')
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const [filterAmountOrderVan, setFilterAmountOrderVan] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
  })
  const [filterAmountOrderMonthVan, setFilterAmountOrderMonthVan] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
  })
  const [
    filterAmountOrderMonthDoneVan,
    setFilterAmountOrderMonthDoneVan,
  ] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
    status: config.MANAGEMENT_STATUS_VAN_TRIP.COMPLETED,
  })
  const [filterRevenueVan, setFilterRevenueVan] = useState({
    from_date: '',
    to_date: '',
    is_in_month: false,
  })
  const callbackHandlerFunction = (fromDate, endDate) => {
    setFromDate(fromDate)
    setEndDate(endDate)
  }
  const handleFormatPrice = data => {
    if (typeof data === 'string') return data
    return formatPrice(data)
  }
  const handleTableChange = pagination => {
    setFilter({
      ...filter.toJS(),
      page: pagination.current,
    })
    getItemsRevenueListVan(filter)
  }
  useEffect(() => {
    setFilterAmountOrderVan({
      ...filterAmountOrderVan,
      from_date: fromDate,
      to_date: endDate,
    })
    setFilterAmountOrderMonthVan({
      ...filterAmountOrderMonthVan,
      from_date: fromDate,
      to_date: endDate,
    })
    setFilterAmountOrderMonthDoneVan({
      ...filterAmountOrderMonthDoneVan,
      from_date: fromDate,
      to_date: endDate,
    })
    setFilterRevenueVan({
      ...filterRevenueVan,
      from_date: fromDate,
      to_date: endDate,
    })
    setFilter({
      ...filter.toJS(),
      from_date: fromDate,
      to_date: endDate,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [fromDate, endDate])
  useEffect(() => {
    getItemAmountOrderVan(filterAmountOrderVan)
    getItemAmountOrderMonthVan(filterAmountOrderMonthVan)
    getItemAmountOrderMonthDoneVan(filterAmountOrderMonthDoneVan)
    getItemRevenueVan(filterRevenueVan)
    getItemsRevenueListVan(filter)
  }, [
    filterAmountOrderVan,
    filterAmountOrderMonthVan,
    filterAmountOrderMonthDoneVan,
    filterRevenueVan,
    filter,
  ])
  useEffect(() => {
    return () => {
      setFromDate('')
      setEndDate('')
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  return (
    <div className="animated fadeIn">
      <Card className={styles.cardFilter}>
        <CardHeader>
          <strong>DOANH THU VAN/BAGAC</strong>
        </CardHeader>
        <CardBody className={styles.cardBodyFilter}>
          <FormFilter handleClickParent={callbackHandlerFunction} />
          <Row>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'success'}
                number={handleFormatPrice(itemAmountOrderVan)}
                text={'Số lượng khách'}
                icon={'fa-user-plus'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'info'}
                number={handleFormatPrice(itemAmountOrderMonthVan)}
                text={'Tổng số chuyến'}
                icon={'fa-exchange'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'warning'}
                number={handleFormatPrice(itemAmountOrderMonthDoneVan)}
                text={'Số chuyến hoàn thành'}
                icon={'fa-check'}
              />
            </Col>
            <Col xs="12" sm="6" lg="3">
              <CardDashboard
                classColor={'danger'}
                number={handleFormatPrice(itemRevenueVan)}
                text={'Doanh thu'}
                icon={'fa-dollar'}
              />
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Row>
        <Col xs="12" sm="12" lg="12">
          <Card>
            <CardHeader className={styles.cardHeaderSearch}>
              <strong>DOANH THU THEO TỪNG XE</strong>
            </CardHeader>
            <CardBody>
              <TableList
                columns={columns}
                dataSource={itemsRevenueListVan}
                totalCount={totalCount}
                showLoading={showLoading}
                pagination={pagination}
                handleTableChange={handleTableChange}
              />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

const mapStateToProps = state => ({
  itemAmountOrderVan: DashboardManagementSelectors.getItemAmountOrderVan(state),
  itemAmountOrderMonthVan: DashboardManagementSelectors.getItemAmountOrderMonthVan(
    state
  ),
  itemAmountOrderMonthDoneVan: DashboardManagementSelectors.getItemAmountOrderMonthDoneVan(
    state
  ),
  itemRevenueVan: DashboardManagementSelectors.getItemRevenueVan(state),
  itemsRevenueListVan: RevenueManagementVanSelectors.getItems(state),
  filter: RevenueManagementVanSelectors.getFilter(state),
  showLoading: LoadingSelectors.getLoadingList(state),
  totalCount: RevenueManagementVanSelectors.getTotalCount(state),
})

const mapDispatchToProps = dispatch => ({
  getItemAmountOrderVan: values =>
    dispatch(DashboardManagementActions.getItemAmountOrderVan(values)),
  getItemAmountOrderMonthVan: values =>
    dispatch(DashboardManagementActions.getItemAmountOrderMonthVan(values)),
  getItemAmountOrderMonthDoneVan: values =>
    dispatch(DashboardManagementActions.getItemAmountOrderMonthDoneVan(values)),
  getItemRevenueVan: values =>
    dispatch(DashboardManagementActions.getItemRevenueVan(values)),
  getItemsRevenueListVan: values =>
    dispatch(RevenueManagementVanActions.getItemsRequest(values)),
  setFilter: filter => dispatch(RevenueManagementVanActions.setFilter(filter)),
})

RevenueVanBagac.propTypes = {
  getItemAmountOrderVan: PropTypes.func,
  getItemAmountOrderMonthVan: PropTypes.func,
  getItemAmountOrderMonthDoneVan: PropTypes.func,
  getItemRevenueVan: PropTypes.func,
  itemAmountOrderVan: PropTypes.any,
  itemAmountOrderMonthVan: PropTypes.any,
  itemAmountOrderMonthDoneVan: PropTypes.any,
  itemRevenueVan: PropTypes.any,
  itemsRevenueListVan: PropTypes.object,
  getItemsRevenueListVan: PropTypes.func,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
  totalCount: PropTypes.number,
  showLoading: PropTypes.bool,
}

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(RevenueVanBagac)
