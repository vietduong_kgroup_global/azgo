import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import TableTourManagementContainer from 'containers/TourCar/TourManagement'
import FormSearch from 'components/molecules/FormSearch'
import { Link, withRouter } from 'react-router-dom'
import { Button } from 'reactstrap'
import 'Stores/TourCar/TourManagement/Reducers'
import 'Stores/TourCar/TourManagement/Sagas'
import { UserSelectors } from 'Stores/User/Selectors'
import { TourManagementSelectors } from 'Stores/TourCar/TourManagement/Selectors'
import { TourManagementActions } from 'Stores/TourCar/TourManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import FormFilterContainer from '../../../organisms/FormFilterTable'
const DriverManagement = ({ filter, setFilter, userInfo }) => {
  let company = userInfo && userInfo.get('adminProfile') && userInfo.get('adminProfile').toJS().company  || ''
  const titlePage = `Danh sách tour` 
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  } 
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        {!company && <div>
          {/* <FormFilterContainer/> */}
          <Link to="/admin/tour/tourManagement/create">
            <Button color="success">
              <i className="fa fa-plus-square" />
              &nbsp;Thêm mới
            </Button>
          </Link>
        </div>
        } 

      </div>  
      <div style={{marginBottom: '16px'}}>
        <FormFilterContainer/> 
      </div>  

      <TableTourManagementContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
      />
    </div>
  )
}

DriverManagement.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  userInfo: UserSelectors.getUserData(state),
  filter: TourManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(TourManagementActions.setFilter(filter)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(DriverManagement)
