import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import DriverAccount from 'components/organisms/Van/FormDriverAccount'
import { Link, useParams } from 'react-router-dom'
import { DriverManagementSelectors } from 'Stores/Van/DriverManagement/Selectors'
import { DriverManagementActions } from 'Stores/Van/DriverManagement/Actions'
import PropTypes from 'prop-types'
import { Form, Switch } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Card, CardBody, CardHeader } from 'reactstrap'

const CreateUpdateDriverAccountContainer = ({ getItem, item }) => {
  let nameDriver = item.getIn(['driverProfile', 'fullName'])
    ? item.getIn(['driverProfile', 'fullName'])
    : ''
  let { id } = useParams()
  const [changeBlockUser, setChangeBlockUser] = useState(false)
  const onChange = checked => {
    setChangeBlockUser(true)
  }
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={
              id
                ? 'Chỉnh sửa tài khoản tài xế van/bagac'
                : 'Thêm tài khoản tài xế van/bagac mới'
            }
            name={
              id ? (
                <Link to={`/driver/van/detail/${id}`}>{nameDriver}</Link>
              ) : null
            }
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
          <div className="card-header-actions">
            {item.size > 0 && item.get('status') === 1 ? (
              <Switch onChange={onChange} defaultChecked />
            ) : null}
            {item.size > 0 && item.get('status') === 0 ? (
              <Switch onChange={onChange} defaultChecked={false} />
            ) : null}
          </div>
        </CardHeader>
        <CardBody>
          <DriverAccount
            id={id || null}
            item={item}
            getItem={getItem}
            changeBlockUser={changeBlockUser}
          />
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: DriverManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(DriverManagementActions.getItemDetailRequest(id)),
})

CreateUpdateDriverAccountContainer.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}
const CreateUpdateDriverAccount = Form.create()(
  CreateUpdateDriverAccountContainer
)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateDriverAccount)
