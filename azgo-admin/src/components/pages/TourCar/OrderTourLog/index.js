import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import TableOrderTourLogContainer from 'containers/TourCar/OrderTourLog'
import FormSearch from 'components/molecules/FormSearch'
import { Link, withRouter } from 'react-router-dom'
import { Button } from 'reactstrap'
import 'Stores/TourCar/OrderTourLog/Reducers'
import 'Stores/TourCar/OrderTourLog/Sagas'
import { OrderTourLogSelectors } from 'Stores/TourCar/OrderTourLog/Selectors'
import { OrderTourLogActions } from 'Stores/TourCar/OrderTourLog/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import FormFilterContainer from '../../../organisms/TourCar/FormFilterTourOrderLog'
import { UserSelectors } from 'Stores/User/Selectors'
const DriverManagement = ({ filter, setFilter, userInfo }) => {
  let company = userInfo && userInfo.get('adminProfile') && userInfo.get('adminProfile').toJS().company || ''
  const titlePage = `Lịch sử đơn hàng ${company}`
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <div>
          <FormFilterContainer/> 
        </div>
      </div> 
      <TableOrderTourLogContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
      />
    </div>
  )
}

DriverManagement.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  userInfo: UserSelectors.getUserData(state),
  filter: OrderTourLogSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(OrderTourLogActions.setFilter(filter)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(DriverManagement)
