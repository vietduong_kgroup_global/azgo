import React, { useEffect, useState } from 'react'
import {
  Card,
  CardBody,
  CardHeader,
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  Button,
} from 'reactstrap'
import styles from './style.module.css' 
import 'Stores/TourCar/TripManagement/Reducers'
import 'Stores/TourCar/TripManagement/Sagas'
import 'Stores/DriverManagement/Reducers'
import 'Stores/DriverManagement/Sagas'
import { Link } from 'react-router-dom'
import FormatPrice from 'components/organisms/FormatPrice'
import { useParams, withRouter } from 'react-router-dom'
import { TripManagementSelectors } from 'Stores/TourCar/TripManagement/Selectors'
import { TripManagementActions } from 'Stores/TourCar/TripManagement/Actions'

import { DriverManagementActions } from 'Stores/DriverManagement/Actions'
import { DriverManagementSelectors } from 'Stores/DriverManagement/Selectors'

import * as config from 'Utils/enum' 
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import moment from 'moment'
import TitlePage from 'components/atoms/TitlePage/'
import * as configsEnum from 'Utils/enum'
import { Config } from 'Config'
import { formatPrice, formatPhone, formatDateTime } from 'Utils/helper'
import { Modal } from 'antd'
import { SeatManagementService } from 'Services/SeatManagementService'

const TripDetail = ({ getItem, item, listDriver, getListDriver }) => {
  const [status, setStatus] = useState({}) 
  const [imageUrl, setImageUrl] = useState([])
  const [imageUrlPopup, setImageUrlPopup] = useState('')
  const [previewVisible, setPreviewVisible] = useState(false)
  const [genderCustomer, setGenderCustmer] = useState("")
  const titlePage = 'THÔNG TIN ĐƠN HÀNG '+ item.get('orderCode')
  let { id } = useParams()
  const style = {
    display: 'flex',
    flexWrap: 'wrap',
  }
  const handlePreviewImg = idx => {
    for (let i = 0; i < imageUrl.length; i++) {
      if (idx === i) {
        setImageUrlPopup(
          Config.DEV_URL.IMAGE +
            '/' +
            imageUrl[i].filePath +
            '/' +
            imageUrl[i].fileName
        )
      }
    }
    setPreviewVisible(true)
  }
  const handleFormatPrice = data => {
    if (typeof data === 'string') return data
    return formatPrice(data)
  }
  useEffect(() => {
    if (id) {
      getItem(id)
      getListDriver()
    }
  }, [])
  useEffect(() => {
    const result = configsEnum.MANAGEMENT_STATUS_TOUR_ARR.find(itemTrip => {
      return itemTrip.value === item.get('status')
    })
   
    if (item.get('images')) {
      setImageUrl(...[item.get('images').toJS()])
    }
    let genderCustomer = 0;
    if(item && item.get('customerGender')){
      genderCustomer = item.get('customerGender')
    }
    else if(item && item.getIn(['customerTour']) && item.getIn(['customerTour', 'customerProfile'])){
      genderCustomer = item.getIn(['customerTour', 'customerProfile', 'gender'])
    }
    let gender = config.MANAGEMENT_GENDER_ARR.find((e) => e.key == genderCustomer)
    if(gender && gender.label) setGenderCustmer(gender.label)
    setStatus(result) 
    return () => {
      setImageUrl([])
    }
  }, [item])  
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <div style={{ width: '100%' }}>
          <Row style={{ marginBottom: '20px' }}>
            <Col sm="12" xl="6">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Thông tin đơn hàng</strong>
                </CardHeader>
                <CardBody>
                  <ListGroup>
                    <ListGroupItem className="justify-content-between">
                      Mã
                      <div className="float-right">
                        <strong>{item && item.get('orderCode')}</strong>
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Công ty
                      <div className="float-right">
                         {item && item.get('company')} 
                      </div>
                    </ListGroupItem>
                    {/* <ListGroupItem className="justify-content-between">
                      Điểm đi
                      <div className="float-right">
                        {item &&
                          item.getIn(['routeInfo', 'startPoint', 'address'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Điểm đến
                      <div className="float-right">
                        {item &&
                          item.getIn(['routeInfo', 'endPoint', 'address'])}
                      </div>
                    </ListGroupItem> */}
                    <ListGroupItem className="justify-content-between">
                      Ngày khởi hành
                      <div className="float-right">
                        {item &&
                          formatDateTime(item.get('startTime'))
                          // moment(new Date(item.get('startTime'))).format(
                          //   'DD-MM-YYYY'
                          // )
                          }
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Giờ khởi hành
                      <div className="float-right">
                        {item &&  new Date(item.get('startTime')).getHours() + 'h'+new Date(item.get('startTime')).getMinutes()}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Trạng thái
                      <div className="float-right">
                        <Button color={status && status.color} size="sm">
                          {status && status.label}
                        </Button>
                      </div>
                    </ListGroupItem>
                  </ListGroup>
                </CardBody>
              </Card>
            </Col>
            <Col sm="12" xl="6">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Thông tin tài xế</strong>
                </CardHeader>
                <CardBody>
                  <ListGroup>
                    <ListGroupItem className="justify-content-between"> 
                      Họ và tên 
                      <div className="float-right">
                        {/* <Link to={"/admin/driver/detail/" + item.getIn(['driverTour', 'driverProfile', 'fullName'])} style={{ marginLeft: '5px' }}> */}
                          {item && item.getIn(['driverTour', 'driverProfile', 'fullName'])}
                        {/* </Link> */}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Xe
                      <div className="float-right">
                        {item && item.getIn(['driverTour','vehicles'])  && item.getIn(['driverTour','vehicles']).map((e) => {
                          if(e && e.toJS() && e.toJS().licensePlates)
                          return(
                          <div>{e.toJS().licensePlates}</div>
                          )})
                          }
                      </div>
                    </ListGroupItem>
                    {/* <ListGroupItem className="justify-content-between">
                      Loại xe
                      <div className="float-right">
                        {item && item.getIn(['service', 'name'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Hãng xe
                      <div className="float-right">
                        {item &&
                          item.getIn([
                            'driverTour',
                            'vehicles',
                            '0',
                            'vehicleBrand',
                            'name',
                          ])}
                      </div>
                    </ListGroupItem> */}
                    <ListGroupItem className="justify-content-between">
                      Số điện thoại
                      <div className="float-right">
                        {item && formatPhone(item.getIn([ 'driverTour', 'countryCode']), item.getIn([ 'driverTour', 'phone']))}
                      </div>
                    </ListGroupItem>
                    {/* <ListGroupItem className="justify-content-between">
                      Đánh giá
                      <div className="float-right">
                        {item && item.get('ratingDriver')}/5
                      </div>
                    </ListGroupItem> */}
                  </ListGroup>
                </CardBody>
              </Card>
            </Col>
            <Col sm="12" xl="6">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Thông tin khách hàng</strong>
                </CardHeader>
                <CardBody>
                  <ListGroup>
                    <ListGroupItem className="justify-content-between">
                      Họ và tên
                      <div className="float-right">
                        {(item && (item.get('customerLastName') || item.get('customerFirstName')))
                        ?  (item.get('customerLastName') + ' ' +  item.get('customerFirstName'))
                        : (item && item.getIn(['customerTour']) && item.getIn(['customerTour', 'customerProfile']))
                        ?    item.getIn(['customerTour', 'customerProfile', 'fullName'])
                        : ''
                        }
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      SDT
                      <div className="float-right">
                        {(item && item.get('customerMobile'))
                        ?  item.get('customerMobile')
                        : (item && item.getIn(['customerTour']))
                        ? item.getIn(['customerTour', 'phone'])
                        : ''
                        }
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Giới tính
                      <div className="float-right">
                        {item &&
                            genderCustomer}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      CMND
                      <div className="float-right">
                        {(item && item.getIn(['customerTour']) && item.getIn(['customerTour', 'customerProfile']))
                        ?  item.getIn(['customerTour', 'customerProfile', 'cmnd'])
                        : ''
                        }
                        {/* {item && item.getIn(['customerTour', 'customerProfile', 'cmnd'])} */}
                      </div>
                    </ListGroupItem>
                  </ListGroup>
                </CardBody>
              </Card>
            </Col>
            <Col sm="12" xl="6">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Thông tin TOUR</strong>
                </CardHeader>
                <CardBody>
                  <ListGroup>
                    <ListGroupItem className="justify-content-between">
                      Tên
                      <div className="float-right">
                        {item && item.getIn(['tourProfile', 'name'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Số chỗ ngồi
                      <div className="float-right">
                        {item &&
                          item.getIn([
                            'tourProfile',
                            'numberOfSeat',
                          ])}
                      </div>
                    </ListGroupItem>
                    {item && item.getIn(['tourProfile', 'type']) == 'fix_price' &&
                      <ListGroupItem className="justify-content-between">
                        Thời gian
                        <div className="float-right">
                          {item && item.getIn(['tourProfile', 'time']) + 'phút'}
                        </div>
                      </ListGroupItem>
                    }
                    <ListGroupItem className="justify-content-between">
                      Giá tour
                      <div className="float-right">
                        {item && item.getIn(['tourProfile', 'type']) == 'fix_price' ?
                        <FormatPrice value={item && item.getIn(['tourProfile', 'tourPrice'])} />
                        : item && item.getIn(['tourProfile', 'type']) == 'distance_price' ?
                        <div><FormatPrice value={item && (item.getIn(['tourProfile', 'distanceRate']))} />VND/km</div>
                        :
                        <div><FormatPrice value={item && (item.getIn(['tourProfile', 'timeRate']))} />VND/ngày</div>
                        }
                      </div>
                    </ListGroupItem>
                  </ListGroup>
                </CardBody>
              </Card>
            </Col>
            {(item && !!item.get('additionalFee')) && 
              <Col sm="12" xl="6">
                <Card className={styles.card}>
                  <CardHeader>
                    <i className="fa fa-align-justify" />
                    <strong>Phí phụ thu</strong>
                  </CardHeader>
                  <CardBody>
                    <ListGroup>
                      <ListGroupItem className="justify-content-between">
                        Tên phí
                        <div className="float-right">
                          <strong>{item.getIn(['additionalFee', 'name']) || ''}</strong>
                        </div>
                      </ListGroupItem>
                      <ListGroupItem className="justify-content-between">
                        Giá trị
                        <div className="float-right">
                          {/* <strong> */}
                            {(item.getIn(['additionalFee', 'type']) === config.MANAGEMENT_TYPE_ADDITIONAL_FEE.FLAT)?
                              <FormatPrice 
                                value={item.getIn(['additionalFee', 'flat'])}
                                renderText={(value) => 
                                  <div>{value} VNĐ</div>
                                }
                              />
                            : (item.getIn(['additionalFee', 'percent']) ? `${item.getIn(['additionalFee', 'percent']) * 100}%` : 0)
                            }
                          {/* </strong> */}
                        </div>
                      </ListGroupItem>
                      <ListGroupItem className="justify-content-between">
                        Ngày áp dụng
                        <div className="float-right">
                          {item.getIn(['additionalFee', 'start']) ? 
                            moment(item.getIn(['additionalFee', 'start'])).format("DD/MM/YYYY")
                            : '' 
                          }
                        </div>
                      </ListGroupItem>
                      <ListGroupItem className="justify-content-between">
                        Ngày kết thúc
                        <div className="float-right">
                          {item.getIn(['additionalFee', 'end']) ? 
                            moment(item.getIn(['additionalFee', 'end'])).format("DD/MM/YYYY")
                            : '' 
                          }
                        </div>
                      </ListGroupItem>
                    </ListGroup>
                  </CardBody>
                </Card>
              </Col>
            }
          </Row>  
        </div>
      </div>
    </div>
  )
}

TripDetail.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  // listDriver = listDriver.toJS(),
  listDriver: DriverManagementSelectors.getItems(state),
  item: TripManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getListDriver: values => dispatch(DriverManagementActions.getItemsRequest(values)),  
  getItem: id => dispatch(TripManagementActions.getItemDetailRequest(id)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(TripDetail)
