import React, { useEffect } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormTour from 'components/organisms/TourCar/FormTour'
import { useParams } from 'react-router-dom'
import { Card, CardBody, CardHeader } from 'reactstrap'
import PropTypes from 'prop-types'
import { TourManagementSelectors } from 'Stores/TourCar/TourManagement/Selectors'
import { TourManagementActions } from 'Stores/TourCar/TourManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'

const CreateUpdateTour = ({ getItem, item }) => {
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, []) 
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={
              id ? 'Chỉnh sửa thông tin tour' : 'Thêm tour mới'
            }
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
        </CardHeader>
        <CardBody>
          <FormTour id={id || null} item={item} />
        </CardBody>
      </Card>
    </div>
  )
}
CreateUpdateTour.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  item: TourManagementSelectors.getItem(state),  
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(TourManagementActions.getItemDetailRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateTour)
