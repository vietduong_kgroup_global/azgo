import React, { useEffect } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormOrder from 'components/organisms/TourCar/FormOrder'
import { useParams } from 'react-router-dom'
import { Card, CardBody, CardHeader } from 'reactstrap'
import PropTypes from 'prop-types'
import { TripManagementSelectors } from 'Stores/TourCar/TripManagement/Selectors'
import { TripManagementActions } from 'Stores/TourCar/TripManagement/Actions'
import { ClientManagementSelectors } from 'Stores/ClientManagement/Selectors'
import { ClientManagementActions } from 'Stores/ClientManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'

const CreateUpdateOrderTour = ({ getItem, item, filterCustomer, setFilterCustomer }) => {
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])  
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={
              id ? `Chỉnh sửa thông tin order ${item.toJS() && item.toJS().orderCode}` : 'Thêm order mới'
            }
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
        </CardHeader>
        <CardBody>
          <FormOrder
            id={id || null}
            item={item}
            // filterCustomer={filterCustomer}
            // setFilterCustomer={setFilterCustomer}
          />
        </CardBody>
      </Card>
    </div>
  )
}
CreateUpdateOrderTour.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  item: TripManagementSelectors.getItem(state),  
  // filterCustomer: ClientManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(TripManagementActions.getItemDetailRequest(id)),
  // setFilterCustomer: filter => dispatch(ClientManagementActions.setFilter(filter)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateOrderTour)
