import React, { useEffect, useState } from 'react'
import {
  Card,
  CardBody,
  CardHeader,
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  Button,
} from 'reactstrap'
import styles from './style.module.css' 
import 'Stores/TourCar/OrderTourLog/Reducers'
import 'Stores/TourCar/OrderTourLog/Sagas'
import 'Stores/DriverManagement/Reducers'
import 'Stores/DriverManagement/Sagas'
import { Link } from 'react-router-dom'

import { useParams, withRouter } from 'react-router-dom'
import { OrderTourLogSelectors } from 'Stores/TourCar/OrderTourLog/Selectors'
import { OrderTourLogActions } from 'Stores/TourCar/OrderTourLog/Actions'

import { DriverManagementActions } from 'Stores/DriverManagement/Actions'
import { DriverManagementSelectors } from 'Stores/DriverManagement/Selectors'

import * as config from 'Utils/enum' 
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import moment from 'moment'
import TitlePage from 'components/atoms/TitlePage/'
import * as configsEnum from 'Utils/enum'
import { Config } from 'Config'
import { formatPrice, formatPhone, formatDateTime } from 'Utils/helper'
import { Modal } from 'antd'
import { SeatManagementService } from 'Services/SeatManagementService'

const TripDetail = ({ getItem, item, listDriver, getListDriver }) => {
  const [status, setStatus] = useState({}) 
  const [imageUrl, setImageUrl] = useState([])
  const [imageUrlPopup, setImageUrlPopup] = useState('')
  const [previewVisible, setPreviewVisible] = useState(false)
  const [driverAccount, setDriverAccount] = useState({}) 
  const [genderCustomer, setGenderCustmer] = useState("") 
  const titlePage = 'CHI TIẾT LỊCH SỬ ĐƠN HÀNG'
  let { id } = useParams()
  const style = {
    display: 'flex',
    flexWrap: 'wrap',
  }
  const handlePreviewImg = idx => {
    for (let i = 0; i < imageUrl.length; i++) {
      if (idx === i) {
        setImageUrlPopup(
          Config.DEV_URL.IMAGE +
            '/' +
            imageUrl[i].filePath +
            '/' +
            imageUrl[i].fileName
        )
      }
    }
    setPreviewVisible(true)
  }
  const handleFormatPrice = data => {
    if (typeof data === 'string') return data
    return formatPrice(data)
  }
  useEffect(() => {
    if (id) {
      getItem(id)
      getListDriver()
    }
  }, [])
  useEffect(() => {
    const result = configsEnum.MANAGEMENT_STATUS_TOUR_ARR.find(itemTrip => {
      return itemTrip.value === item.getIn(['tourOrderProfile', 'status'])
    })
   
    if (item.get('images')) {
      setImageUrl(...[item.get('images').toJS()])
    }
    let gender = config.MANAGEMENT_GENDER_ARR.find((e) => e.key == item.getIn([
      'customerProfile',
      'gender',
    ]))
    if(gender && gender.label) setGenderCustmer(gender.label)
    setStatus(result) 
    return () => {
      setImageUrl([])
    }
  }, [item]) 
  useEffect(() => {
    setDriverAccount(listDriver.toJS().find((e) => e.id === item.getIn(['driverProfile','userId'])) || {})
  }, [listDriver, item])  
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <div style={{ width: '100%' }}>
          <Row style={{ marginBottom: '20px' }}>
            <Col sm="12" xl="6">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Chi tiết đơn hàng</strong>
                </CardHeader>
                <CardBody>
                  <ListGroup>
                    <ListGroupItem className="justify-content-between">
                      Mã
                      <div className="float-right">
                        <Link to={"/admin/tour/orderManagement/detail/" + item.getIn(['tourOrderProfile', 'uuid'])} style={{ marginLeft: '5px' }}>
                        <strong>{item && item.getIn(['tourOrderProfile', 'orderCode'])}</strong>
                        </Link>
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Trạng thái mới nhất
                      <div className="float-right">
                        <Button color={status && status.color} size="sm">
                          {status && status.label}
                        </Button>
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Người thay đổi
                      <div className="float-right"> 
                        <strong>{item && item.getIn(['userProfile', 'email'])}</strong> 
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Ngày thay đổi
                      <div className="float-right"> 
                        <strong>{item && formatDateTime(item.get('createdAt'))}</strong> 
                      </div>
                    </ListGroupItem>
                  </ListGroup>
                </CardBody>
              </Card>
            </Col> </Row>  </div>
      </div>
    </div>
  )
}

TripDetail.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  // listDriver = listDriver.toJS(),
  listDriver: DriverManagementSelectors.getItems(state),
  item: OrderTourLogSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getListDriver: values => dispatch(DriverManagementActions.getItemsRequest(values)),  
  getItem: id => dispatch(OrderTourLogActions.getItemDetailRequest(id)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(TripDetail)
