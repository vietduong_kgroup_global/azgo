import React, { useState } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Link, withRouter } from 'react-router-dom'
import { Button } from 'reactstrap'
import { Select } from 'antd';

import TitlePage from 'components/atoms/TitlePage/'
import TripsManagementContainer from 'containers/TourCar/TripManagement'
import FormSearch from 'components/molecules/FormSearch'
import { UserSelectors } from 'Stores/User/Selectors'
import * as configsEnum from 'Utils/enum'
import './styles.scss'
import FilterTagsCompany from 'components/organisms/TourCar/FilterTagsCompany'
// import Chips, { Chip } from 'react-chips'
const TripsManagement = props => {
  const { Option } = Select;
  let userInfo = props.userInfo
  let company = userInfo && userInfo.get('adminProfile') && userInfo.get('adminProfile').toJS().company || ''
  const titlePage = `Danh sách Order ${company}`
  const [search, setSearch] = useState();
  const [chips, setChips] = useState([]);
  const [listStatus, setListStatus] = useState(`[]`);
  const [listCompany, setListCompany] = useState(`[]`);
  
  
  const handleSearch = value => {
    setSearch(value)
  }
  const changeTagsFilterCompany = tags => {  
    setListCompany(JSON.stringify(tags))
  }
  const handleChange = (value) => {
    setListStatus(JSON.stringify(value))
  }

  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <div className="trip--contain-filter-search">
          <FormSearch handleSearch={handleSearch} />
          <Select
            mode="multiple"
            allowClear
            className="trip--filter-status"
            placeholder="Trạng thái đơn hàng"
            onChange={handleChange}
          >
            {configsEnum.MANAGEMENT_STATUS_TOUR_ARR.map((status, index) => {
              return (
                <Option key={`${status.key}-${index}`} value={status.value}>{status.label}</Option>
              )
            })}
          </Select>
          {!company && <FilterTagsCompany changeTags = {changeTagsFilterCompany}/>}
          {/* <Chips
            value={chips}
            onChange={onChangeChips}
            suggestions={["Your", "Data", "Here"]}
         /> */}
        </div>
        <Link to="/admin/tour/orderManagement/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới order
          </Button>
        </Link>
      </div>
      <TripsManagementContainer
        search={search}
        listStatus={listStatus}
        listCompany={listCompany}
      />
    </div>
  )
}
const mapStateToProps = state => ({
  userInfo: UserSelectors.getUserData(state),

})

const mapDispatchToProps = dispatch => ({

})

TripsManagement.propTypes = {}
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(TripsManagement)
