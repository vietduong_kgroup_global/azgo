import React, { useEffect } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormAdditionalFee from 'components/organisms/FormAdditionalFee'
import { useParams } from 'react-router-dom'
import { Card, CardBody, CardHeader } from 'reactstrap'
import PropTypes from 'prop-types'
import { AdditionalFeeManagementSelectors } from 'Stores/AdditionalFeeManagement/Selectors'
import { AdditionalFeeManagementActions } from 'Stores/AdditionalFeeManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'

const CreateUpdateTour = ({ getItem, item }) => {
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={
              id ? 'Chỉnh sửa thông tin phí phụ thu' : 'Thêm phí phụ thu mới'
            }
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
        </CardHeader>
        <CardBody>
          <FormAdditionalFee id={id || null} item={item} />
        </CardBody>
      </Card>
    </div>
  )
}
CreateUpdateTour.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  item: AdditionalFeeManagementSelectors.getItem(state),  
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(AdditionalFeeManagementActions.getItemDetailRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateTour)
