import React, { useState } from 'react'
import { Button } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types'

import TitlePage from 'components/atoms/TitlePage/'
import TableFeeManagementContainer from 'containers/AdditionalFee/TableFeeManagement'
import FormSearch from 'components/molecules/FormSearch'
import 'Stores/AdditionalFeeManagement/Reducers'
import 'Stores/AdditionalFeeManagement/Sagas'
import { AdditionalFeeManagementSelectors } from 'Stores/AdditionalFeeManagement/Selectors'
import { AdditionalFeeManagementActions } from 'Stores/AdditionalFeeManagement/Actions'
const FeeManagement = ({ filter, setFilter }) => {
  const titlePage = `Danh sách phí phụ thu` 
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  } 
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch}/>
        <div>
          <Link to="/admin/additional-fee/create">
            <Button color="success">
              <i className="fa fa-plus-square" />
              &nbsp;Thêm mới
            </Button>
          </Link>
        </div>
      </div> 
      <TableFeeManagementContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
      />
    </div>
  )
}

FeeManagement.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  filter: AdditionalFeeManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(AdditionalFeeManagementActions.setFilter(filter)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(FeeManagement)
