import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormSearch from 'components/molecules/FormSearch'
import TableClientsManagementContainer from 'containers/ClientsManagement/TableClientsManagement'
import 'antd/dist/antd.css'
import PropTypes from 'prop-types'
import { Button } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import 'Stores/ClientManagement/Reducers'
import 'Stores/ClientManagement/Sagas'
import { ClientManagementSelectors } from 'Stores/ClientManagement/Selectors'
import { ClientManagementActions } from 'Stores/ClientManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'

const ClientsManagement = ({ filter, setFilter }) => {
  const titlePage = 'Danh sách khách hàng'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Link to="/admin/clients-management/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <TableClientsManagementContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
      />
    </div>
  )
}

const mapStateToProps = state => ({
  filter: ClientManagementSelectors.getFilter(state),
})

ClientsManagement.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
}

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(ClientManagementActions.setFilter(filter)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(ClientsManagement)
