import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormClientAccount from 'components/organisms/FormClientAccount'
import { Link, useParams } from 'react-router-dom'
import { ClientManagementSelectors } from 'Stores/ClientManagement/Selectors'
import { ClientManagementActions } from 'Stores/ClientManagement/Actions'
import PropTypes from 'prop-types'
import { Form, Switch } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Card, CardBody, CardHeader } from 'reactstrap'

const CreateUpdateClientAccountContainer = ({ getItem, item }) => {
  let nameClient = item.getIn(['customerProfile', 'fullName'])
    ? item.getIn(['customerProfile', 'fullName'])
    : ''
  let { id } = useParams()
  const [changeBlockUser, setChangeBlockUser] = useState(false)
  const onChange = checked => {
    setChangeBlockUser(true)
  }
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={
              id
                ? 'Chỉnh sửa tài khoản khách hàng'
                : 'Thêm tài khoản khách hàng mới'
            }
            name={
              id ? (
                <Link to={`/admin/clients-management/detail/${id}`}>
                  {nameClient}
                </Link>
              ) : null
            }
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
          <div className="card-header-actions">
            {item.size > 0 && item.get('status') === 1 ? (
              <Switch onChange={onChange} defaultChecked />
            ) : null}
            {item.size > 0 && item.get('status') === 0 ? (
              <Switch onChange={onChange} defaultChecked={false} />
            ) : null}
          </div>
        </CardHeader>
        <CardBody>
          <FormClientAccount
            id={id || null}
            item={item}
            getItem={getItem}
            changeBlockUser={changeBlockUser}
          />
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: ClientManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(ClientManagementActions.getItemDetailRequest(id)),
})

CreateUpdateClientAccountContainer.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}
const CreateUpdateClientAccount = Form.create()(
  CreateUpdateClientAccountContainer
)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateClientAccount)
