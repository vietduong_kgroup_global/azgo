import React, { useEffect, useState } from 'react'
import {
  Card,
  CardBody,
  CardHeader,
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  Button, 
} from 'reactstrap'
import { 
  Input, 
  Form
} from 'antd'
import classnames from 'classnames';
import styles from './style.module.css' 
import 'Stores/WalletManagement/Reducers'
import 'Stores/WalletManagement/Sagas' 
import { Link } from 'react-router-dom'
import FormWallet from 'components/organisms/FormWallet'
import { useParams, withRouter } from 'react-router-dom'
import { WalletManagementSelectors } from 'Stores/WalletManagement/Selectors'
import { WalletManagementActions } from 'Stores/WalletManagement/Actions'
import TableWalletHistoryContainer from 'containers/Driver/TableWalletHistoryContainer' 
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux' 
import TitlePage from 'components/atoms/TitlePage/' 
import {formatDateTime, formatPhone} from 'Utils/helper'  
import FormatPrice from 'components/organisms/FormatPrice'

import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
const TripDetail = ({ getItem, item, listWallet, getListWallet, filter, setFilter }) => {
 let phoneNumber = formatPhone(item && item.getIn(['usersEntity', 'countryCode']), item && item.getIn(['usersEntity', 'phone']))
  const [imageUrl, setImageUrl] = useState([])  
  const [search, setSearch] = useState()  
  const [activeTab, setActiveTab] = useState('1'); 
  const toggle = tab => {
    if(activeTab !== tab) setActiveTab(tab);
  }
  const titlePage = `CHI TIẾT TÀI KHOẢN VÍ ${item.getIn(['usersEntity','driverProfile','fullName']) || ""} - ${phoneNumber}`
  let { id } = useParams()
  
  useEffect(() => {
    if (id) {
      getItem(id)
      getListWallet()
    }
  }, [])
  useEffect(() => {
  
  }, [item])  
  const callbackUpdate = () => {
    getItem(id) 
  } 
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      {item.get('createdAt')?
      <div className="header-main-content">
        <div style={{ width: '100%' }}>
          <Row style={{ marginBottom: '20px' }}>
            <Col sm="12" xl="12">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Chi tiết Tài khoản ví</strong>
                </CardHeader>
                <CardBody>
                <ListGroup> 
                    <ListGroupItem className="justify-content-between">
                      Ngày tạo ví
                      <div className="float-right"> 
                        <strong>{item && formatDateTime(item.get('createdAt'))}</strong> 
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Ngày cập nhật ví
                      <div className="float-right"> 
                        <strong>{item && formatDateTime(item.get('updatedAt'))}</strong> 
                      </div>
                    </ListGroupItem>
                    {/* <ListGroupItem className="justify-content-between">
                      SDT tài xế
                      <div className="float-right"> 
                        <strong>{phoneNumber}</strong> 
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Ho tên tài xế
                      <div className="float-right"> 
                        <strong>{item && item.getIn(['usersEntity','driverProfile','fullName'])}</strong> 
                      </div>
                    </ListGroupItem> */}
                    {/* <ListGroupItem className="justify-content-between"> 
                      Fee Wallet
                        <div className="float-right"> 
                          <strong>{item && 
                          <mat 
                            value = {item.get('feeWallet')}
                            thousandSeparator={'.'}
                            decimalSeparator={','}
                            displayType={'text'}
                          />} VND</strong> 
                        </div> 
                     
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between"> 
                    Cash Wallet
                     <div className="float-right"> 
                       <strong>{item && 
                       <NumberFormat 
                         value = {item.get('cashWallet')}
                         thousandSeparator={'.'}
                         decimalSeparator={','}
                         displayType={'text'}
                       />} VND</strong> 
                     </div> 
                  
                 </ListGroupItem> */}
              </ListGroup>
              <div>
                <Nav tabs>
                  <NavItem className = {styles.width_navitem}>
                    <NavLink
                      className={classnames({ active: activeTab === '1' })}
                      onClick={() => { toggle('1'); }}
                    >
                      Tài khoản thanh toán:  
                     <div className="float-right"> 
                      <strong>{item && 
                          <FormatPrice 
                            value = {item.get('feeWallet')}
                          />} VND</strong> </div>
                    </NavLink>
                  </NavItem>
                  <NavItem className = {styles.width_navitem}>
                    <NavLink
                      className={classnames({ active: activeTab === '2' })}
                      onClick={() => { toggle('2'); }}
                    >
                      Tài khoản doanh thu: 
                     <div className="float-right"> 
                      <strong>{item && 
                       <FormatPrice 
                         value = {item.get('cashWallet')}
                       />} VND</strong>  </div>
                    </NavLink>
                  </NavItem>
                </Nav>
              <TabContent activeTab={activeTab}>
                <TabPane tabId="1">
                  <div className={styles.title_update}>Cập Nhật Tài khoản thanh toán</div>
                  <FormWallet id={id || null} item={item}  callbackUpdate = {callbackUpdate} typeForm = "feeWallet"/>  
                </TabPane>
                <TabPane tabId="2">
                  <div  className={styles.title_update}>Cập Nhật Tài khoản doanh thu</div>
                  <FormWallet id={id || null} item={item}  callbackUpdate = {callbackUpdate} typeForm = "cashWallet"/>   
                </TabPane>
              </TabContent>
            </div>  
             
                </CardBody>
              </Card>
            </Col> </Row>  </div>
      </div>
      :   <div>{'Chưa tạo ví'}</div>}
      <div className="header-main-content">
        <div style={{ width: '100%' }}>
          <Row style={{ marginBottom: '20px' }}>
            <Col sm="12" xl="12">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Lịch sử ví</strong>
                </CardHeader>
                <CardBody>
                <TableWalletHistoryContainer
                  search={search}
                  filter={filter}
                  setFilter={setFilter}
                  item={item}
                  id={id || null}
                />
                </CardBody>
              </Card>
            </Col> </Row>  </div>
      </div>
    </div>
  )
}

TripDetail.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
  getItem: PropTypes.func, 
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  // listWallet = listWallet.toJS(),
  filter: WalletManagementSelectors.getFilter(state),
  listWallet: WalletManagementSelectors.getItems(state),
  item: WalletManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(WalletManagementActions.setFilter(filter)),
  getListWallet: values => dispatch(WalletManagementActions.getItemsRequest(values)),  
  getItem: id => dispatch(WalletManagementActions.getItemDetailRequest(id)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(TripDetail)
