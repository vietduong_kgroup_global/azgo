import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import ItemInfo from 'components/molecules/ItemInfo'
import { useParams, withRouter } from 'react-router-dom'
import 'Stores/BikeCar/DriverManagement/Reducers'
import 'Stores/BikeCar/DriverManagement/Sagas'
import { DriverManagementSelectors } from 'Stores/BikeCar/DriverManagement/Selectors'
import { DriverManagementActions } from 'Stores/BikeCar/DriverManagement/Actions'
import PropTypes from 'prop-types'
import { Form, Icon, Modal, Button, Col, Row, Typography  } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Card, CardBody, CardHeader} from 'reactstrap'
import * as configsEnum from 'Utils/enum'
import { formatPhone } from 'Utils/helper'
import { Config } from 'Config'
import moment from 'moment'
import { AccountManagementSelectors } from 'Stores/AccountManagement/Selectors'
import 'Stores/AccountManagement/Reducers'
import 'Stores/AccountManagement/Sagas'
import { AccountManagementActions } from 'Stores/AccountManagement/Actions'
const { Text, Link } = Typography;
const DriverAccountDetailContainer = ({ getItem, item, history, account, getAccount }) => {
  const [gender, setGender] = useState({})
  const [previewVisible, setPreviewVisible] = useState(false)
  const [imageUrlPopup, setImageUrlPopup] = useState('')
  const [imageAvatar, setImageAvatar] = useState('')
  const [imageFrontCmnd, setImageFrontCmnd] = useState('')
  const [imageBackCmnd, setImageBackCmnd] = useState('')
  const [frontDrivingLicense, setFrontDrivingLicense] = useState('')
  const [backDrivingLicense, setBackDrivingLicense] = useState('')
  const [phone, setPhone] = useState()
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
      getAccount({user_uuid: id})
    }
  }, [])
  const handleOpenUpdate = () => {
    history.push('/admin/driver/edit/' + item.get('userId'))
  }
  const handlePreviewImg = value => {
    switch (value) {
      case 'imageAvatar': {
        setImageUrlPopup(imageAvatar)
        break
      }
      case 'imageFrontCmnd': {
        setImageUrlPopup(imageFrontCmnd)
        break
      }
      case 'frontDrivingLicense': {
        setImageUrlPopup(frontDrivingLicense)
        break
      }
      case 'backDrivingLicense': {
        setImageUrlPopup(backDrivingLicense)
        break
      }
      default: {
        setImageUrlPopup(imageBackCmnd)
        break
      }
    }
    setPreviewVisible(true)
  }
  useEffect(() => {
    const result = configsEnum.MANAGEMENT_GENDER_ARR.find(itemGender => {
      return itemGender.key === item.getIn(['driverProfile', 'gender'])
    })
    setGender(result)
    if (item.getIn(['driverProfile', 'imageProfile', 'address'])) {
      setImageAvatar(item.getIn(['driverProfile', 'imageProfile', 'address']))
    }
    if (item.getIn(['driverProfile', 'frontOfCmnd', 'address'])) {
      setImageFrontCmnd(item.getIn(['driverProfile', 'frontOfCmnd', 'address']))
    }
    if (item.getIn(['driverProfile', 'backOfCmnd', 'address'])) {
      setImageBackCmnd(item.getIn(['driverProfile', 'backOfCmnd', 'address']))
    }
    if (item.getIn(['driverProfile', 'frontDrivingLicense', 'address'])) {
      setFrontDrivingLicense(item.getIn(['driverProfile', 'frontDrivingLicense', 'address']))
    }
    if (item.getIn(['driverProfile', 'backDrivingLicense', 'address'])) {
      setBackDrivingLicense(item.getIn(['driverProfile', 'backDrivingLicense', 'address']))
    }
    if (item.size > 0) {
      setPhone(formatPhone(item.get('countryCode'), item.get('phone')))
    }
  }, [item]) 
  const handleLinkWallet = () => {
    history.push('/admin/wallet/detail/' + item.get('userId'))
  }
  return (
    <div className="animated fadeIn">
      <Card className="card-accent-primary">
        <CardHeader>
          <TitlePage
            data="Chi tiết tài khoản tài xế"
            name={item.getIn(['driverProfile', 'fullName'])}
          />
          <i
            className="fa fa-pencil"
            onClick={() => handleOpenUpdate()}
            style={{ cursor: 'pointer' }}
          />
        </CardHeader>
        <CardBody>
         <Row gutter={[16, 16]}> 
            <Col xs={{ span: 24, offset: 0 }} md={{ span: 4, offset: 0 }}>
            <div className="section_driver_detail">Thông tin tài khoản</div>
            <div className="detail-user-image">
              {item.getIn(['driverProfile', 'imageProfile', 'fileName']) ? (
                <div className="detail-user-image-inner">
                  <img
                    src={imageAvatar}
                    alt="avatar"
                    onClick={() => handlePreviewImg('imageAvatar')}
                  />
                </div>
              ) : (
                <div className="bg-avatar" />
              )}
            </div>
            </Col> 
            <Col xs={{ span: 24, offset: 0 }} md={{ span: 10, offset: 0 }}>
            <div className="section_driver_detail"></div>
            <div className="detail-user-info">
              <ItemInfo label="Email:" content={item.get('email')} />
              <ItemInfo
                label="Họ:"
                content={item.getIn(['driverProfile', 'lastName'])}
              />
              <ItemInfo
                label="Tên:"
                content={item.getIn(['driverProfile', 'firstName'])}
              />
              <ItemInfo label="Số điện thoại:" content={phone} />
              <ItemInfo
                label="Ngày sinh:"
                content={moment(
                  new Date(item.getIn(['driverProfile', 'birthday']))
                ).format('DD-MM-YYYY')}
              />
              <ItemInfo
                label="Giới tính:"
                content={gender ? gender.label : null}
              />
              <ItemInfo
                label="Số CMND:"
                content={item.getIn(['driverProfile', 'cmnd'])}
              />
              <ItemInfo
                label="Địa chỉ:"
                content={item.getIn(['driverProfile', 'address'])}
              />
              <ItemInfo
                label="Khu vực:"
                content={item.getIn(['driverProfile', 'areaInfo']) && item.getIn(['driverProfile', 'areaInfo', "name"])}
              />
               <Row style={{ marginTop: '20px', marginBottom: '10px' }}>
                <Col xs="12" sm="12" md="12">
                <Button color="success" onClick={() => handleLinkWallet()}>
                 Chi tiết ví tài xế
                </Button>
                </Col>
              </Row>  <Modal
                visible={previewVisible}
                footer={null}
                onCancel={() => setPreviewVisible(false)}>
                <div className="modal-image">
                  <img alt="example" src={imageUrlPopup} />
                </div>
              </Modal>
            </div>
            </Col> 
            <Col xs={{ span: 24, offset: 0 }} md={{ span: 10, offset: 0 }}>
              <div className="section_driver_detail">Giấy CMND</div>
              <Row  gutter={8}>
              <Col span={12}>
                  <div className="frame-img">
                    <div className="frame-img-inner">
                      {imageFrontCmnd ? (
                        <img
                          src={imageFrontCmnd}
                          alt="front CMND"
                          onClick={() => handlePreviewImg('imageFrontCmnd')}
                        />
                      ) : (
                        <Icon type="picture" />
                      )}
                    </div>
                  </div>
                </Col>
              <Col span={12}>
                  <div className="frame-img">
                    <div className="frame-img-inner">
                      {imageBackCmnd ? (
                        <img
                          src={imageBackCmnd}
                          alt="back CMND"
                          onClick={() => handlePreviewImg('imageBackCmnd')}
                        />
                      ) : (
                        <Icon type="picture" />
                      )}
                    </div>
                  </div>
                </Col>
              </Row>
              <div className="section_driver_detail">Giấy phép lái xe</div>
              <Row gutter={8}> 
              <Col span={12}>
                  <div className="frame-img">
                    <div className="frame-img-inner">
                      {frontDrivingLicense ? (
                        <img
                          src={frontDrivingLicense}
                          alt="frontDrivingLicense"
                          onClick={() =>
                            handlePreviewImg('frontDrivingLicense')
                          }
                        />
                      ) : (
                        <Icon type="picture" />
                      )}
                    </div>
                  </div>
                </Col>
                <Col span={12}>
                  <div className="frame-img">
                    <div className="frame-img-inner">
                      {backDrivingLicense ? (
                        <img
                          src={backDrivingLicense}
                          alt="backDrivingLicense"
                          onClick={() => handlePreviewImg('backDrivingLicense')}
                        />
                      ) : (
                        <Icon type="picture" />
                      )}
                    </div>
                  </div>
                </Col>
              </Row>
             
            </Col> 
         </Row> 
         <Row gutter={[16, 16]}> 
         <Col xs={{ span: 24, offset: 0 }} md={{ span: 24, offset: 0 }}>
          <div className="section_driver_detail">Thông tin tài khoản ngân hàng</div>
            <ItemInfo
              label="Tên ngân hàng"
              content={account && account.getIn(['bankInfo','vnName'])}
            />
             <ItemInfo
              label="Chi nhánh"
              content={account && account.get('accountBankBranch')}
             
            />
            <ItemInfo
              label="Số TK"
              content={account && account.get('accountBankNumber')}
              
             
            />
            <ItemInfo
              label="Tên TK"
              content={account && account.get('accountBankUserName')}
               
            />
             {account && account.get('isActive') ? <div style={{color: 'rgb(24 172 230)', display: 'flex', alignItems: 'center'}}><Icon style={{marginRight: '4px'}} type="check-circle" /><Text style={{color: 'rgb(24 172 230)'}}> Tài khoản đã được xác minh</Text></div>:
             <Text type="warning">Tài khoản chưa được xác minh</Text>
              }
         </Col>
         </Row> 
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  account: AccountManagementSelectors.getAccount(state),
  item: DriverManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getAccount: values => dispatch(AccountManagementActions.getAccount(values)),
  getItem: id => dispatch(DriverManagementActions.getItemDetailRequest(id)),
})

DriverAccountDetailContainer.propTypes = {
  getItem: PropTypes.func,
  getAccount: PropTypes.func,
  account: PropTypes.any,
  item: PropTypes.object,
  history: PropTypes.any,
}
const DriverAccountDetail = Form.create()(DriverAccountDetailContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(DriverAccountDetail)
