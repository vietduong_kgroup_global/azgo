import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import TableWalletManagementContainer from 'containers/Driver/TableWalletManagementContainer'
import FormSearch from 'components/molecules/FormSearch'
import { Link, withRouter } from 'react-router-dom'
import { Button } from 'reactstrap'
import 'Stores/WalletManagement/Reducers'
import 'Stores/WalletManagement/Sagas'
import { WalletManagementSelectors } from 'Stores/WalletManagement/Selectors'
import { WalletManagementActions } from 'Stores/WalletManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types'

const WalletManagement = ({ filter, setFilter }) => {
  const titlePage = 'Danh sách tài khoản ví'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        {/* <Link to="/admin/driver/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link> */}
      </div>
      <TableWalletManagementContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
      />
    </div>
  )
}

WalletManagement.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  filter: WalletManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(WalletManagementActions.setFilter(filter)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(WalletManagement)
