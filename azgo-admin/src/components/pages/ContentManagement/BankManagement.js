import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import TableBankManagementContainer from 'containers/Bank/BankManagement'
import FormSearch from 'components/molecules/FormSearch'
import { Link, withRouter } from 'react-router-dom'
import { Button } from 'reactstrap'
import 'Stores/BankManagement/Reducers'
import 'Stores/BankManagement/Sagas'
import { BankManagementSelectors } from 'Stores/BankManagement/Selectors'
import { BankManagementActions } from 'Stores/BankManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
// import {_findPhoneNumbersInText} from 'Utils/helper'
import PropTypes from 'prop-types'

const BankManagement = ({ filter, setFilter }) => {
  const titlePage = 'Danh sách Ngân hàng'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    // if(_findPhoneNumbersInText(value)) value = _findPhoneNumbersInText(value)
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Link to="/admin/content-management/bank/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <TableBankManagementContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
      />
    </div>
  )
}

BankManagement.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  filter: BankManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(BankManagementActions.setFilter(filter)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(BankManagement)


// import React, { useEffect, useState } from 'react'
// import TitlePage from 'components/atoms/TitlePage/'
// import { Button } from 'reactstrap'
// import { Link, withRouter } from 'react-router-dom'
// import { Table, Popconfirm } from 'antd'
// import 'Stores/BankManagement/Reducers'
// import 'Stores/BankManagement/Sagas'
// import { BankManagementSelectors } from 'Stores/BankManagement/Selectors'
// import { BankManagementActions } from 'Stores/BankManagement/Actions'
// import { connect } from 'react-redux'
// import { compose } from 'redux'
// import PropTypes from 'prop-types'
// import * as configsEnum from 'Utils/enum'
// import { Config } from 'Config'
// import { LoadingSelectors } from 'Stores/Loading/Selectors'
// import TableBankManagementContainer from 'containers/BankContainer'

// const BankManagement = ({
//   getItems,
//   items,
//   clearItems,
//   history,
//   showLoading,
//   deleteItem
// }) => {
//   const titlePage = 'Danh sách Ngân hàng'
//   const handleOpenUpdate = id => { 
//     history.push('/admin/content-management/bank/edit/' + id)
//   }
//   const Style = {
//     maxWidth: '100px',
//     width: '100px',
//     display: 'flex',
//     alignItems: 'center',
//     padding: '5px',
//     justifyContent: 'center',
//     border: '1px solid #e8e8e8',
//   }
//   const handleConfirmDelete = id => {
//     deleteItem(id)
//   }
//   console.log(items && items.toJS())
//   const columns = [
//     {
//       title: 'Tên Tiếng Anh',
//       key: 'enName',
//       dataIndex: 'enName',
//     },
//     {
//       title: 'Tên Tiếng Việt',
//       key: 'vnName',
//       dataIndex: 'vnName',
//     },
//     {
//       title: 'Short name',
//       key: 'shortName',
//       dataIndex: 'shortName',
//     },
//     {
//       title: 'Mã ngân hàng',
//       key: 'bankCode',
//       dataIndex: 'bankCode',
//     },
//     {
//       title: 'Chức năng',
//       width: 140,
//       dataIndex: 'function',
//       render: (key, value) => (
//         <>
//           <Button
//             color="light"
//             size="sm"
//             className="btn-sm-ct"
//             style={{ marginRight: '5px' }}
//             onClick={() =>  handleOpenUpdate(value.uuid)}>
//             <i className="fa fa-pencil" />
//             &nbsp;Sửa
//           </Button>
//           <Popconfirm
//             placement="top"
//             onConfirm={() => handleConfirmDelete(value.uuid)}
//             title={
//               'Bạn có chắc chắn muốn xóa ngân hàng ' +
//               `${value.shortName ? value.shortName : ''}`
//             }
//             okText="Có"
//             cancelText="Không">
//             <Button color="danger" size="sm" className="btn-sm-ct">
//               <i className="fa fa-trash-o" />
//               &nbsp;Xóa
//             </Button>
//           </Popconfirm>
//         </>
//       ),
//     },
//   ]
//   useEffect(() => {
//     getItems()
//     return () => {
//       clearItems()
//     }
//   }, [])
//   return (
//     <div className="animated fadeIn">
//       <TitlePage data={titlePage} />
//       <div className="header-main-content">
//         <Link to="/admin/content-management/bank/create">
//           <Button color="success">
//             <i className="fa fa-plus-square" />
//             &nbsp;Thêm mới
//           </Button>
//         </Link>
//       </div>
//       <TableBankManagementContainer
//         search={search}
//         filter={filter}
//         setFilter={setFilter}
//         // rowKey={record => record.id}
//         // columns={columns}
//         // pagination={false}
//         // loading={showLoading}
//         // dataSource={items && items.size > 0 ? items.toJS() : null}
//       />
//     </div>
//   )
// }

// BankManagement.propTypes = {
//   items: PropTypes.object,
//   getItems: PropTypes.func,
//   clearItems: PropTypes.func,
//   history: PropTypes.any,
//   showLoading: PropTypes.bool,
// }

// const mapStateToProps = state => ({
//   showLoading: LoadingSelectors.getLoadingList(state),
//   items: BankManagementSelectors.getItems(state),
// })

// const mapDispatchToProps = dispatch => ({
//   getItems: () => dispatch(BankManagementActions.getItemsRequest()),
//   clearItems: () => dispatch(BankManagementActions.clearItems()),
//   deleteItem:  (values) => dispatch(BankManagementActions.deleteItemRequest(values)),
// })
// const withConnect = connect(mapStateToProps, mapDispatchToProps)
// export default compose(withConnect, withRouter)(BankManagement)