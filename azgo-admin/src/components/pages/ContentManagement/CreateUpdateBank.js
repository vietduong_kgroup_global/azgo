import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Link, useParams } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Button, Form, Input, Checkbox } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Config } from 'Config'
import { Card, CardBody, CardHeader } from 'reactstrap'
import UploadImage from 'components/molecules/UploadImage'
import 'Stores/BankManagement/Reducers'
import 'Stores/BankManagement/Sagas'
import { BankManagementSelectors } from 'Stores/BankManagement/Selectors'
import { BankManagementActions } from 'Stores/BankManagement/Actions'

const CreateUpdateBankContainer = ({
  form,
  createItem,
  getItem,
  item,
  editItem,
}) => {
  let name = item.get('bank') ? item.get('bank') : ''
  const { getFieldDecorator, getFieldsValue, setFieldsValue } = form
  let { id } = useParams()
  const [imageView, setImageView] = useState('')
  const [checked, setChecked] = useState(false)
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  }
  const callbackHandleImage = value => {
    setFieldsValue({
      image: value,
    })
  }
  const onChange = e => {
    setChecked(e.target.checked)
  }
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }

      if (id) {
        editItem({
          ...getFieldsValue(),
          napasSupported: form.getFieldsValue().napasSupported === true ? true : false,
          uuid: id,
        })
      } else {
        createItem({
          ...getFieldsValue(),
          napasSupported: form.getFieldsValue().napasSupported === true ? true : false,
        })
      }
    })
  }
  useEffect(() => {
    if (item.get('napasSupported')) {
      setChecked(true)
    } else {
      setChecked(false)
    }
  }, [item])
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={id ? 'Chỉnh sửa ngân hàng' : 'Thêm ngân hàng mới'}
            name={name}
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
        </CardHeader>
        <CardBody>
          <Form
            id="frmBank"
            {...formItemLayout}
            className="input-floating form-ant-custom">
            <Form.Item label="Tên Tiếng Việt">
              {getFieldDecorator('vnName', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue: id && item.get('vnName'),
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Tên Tiếng Anh">
              {getFieldDecorator('enName', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue: id && item.get('enName'),
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Tên Viết tắt">
              {getFieldDecorator('shortName', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue: id && item.get('shortName'),
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Mã ngân hàng">
              {getFieldDecorator('bankCode', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue: id && item.get('bankCode'),
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Hỗ trợ Napas">
              {getFieldDecorator('napasSupported', {
                rules: [
                  {
                    required: false,
                  },
                ],
                initialValue: checked,
              })(
                <Checkbox checked={checked} onChange={onChange}>
                  Hỗ trợ Napas
                </Checkbox>
              )}
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
              <Button type="primary" htmlType="submit" onClick={handleSubmit}>
                Xác nhận
              </Button>
            </Form.Item>
          </Form>
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: BankManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(BankManagementActions.createItemRequest(values)),
  editItem: values => dispatch(BankManagementActions.editItemRequest(values)),
  getItem: id => dispatch(BankManagementActions.getItemDetailRequest(id)),
})

CreateUpdateBankContainer.propTypes = {
  getItem: PropTypes.func,
  createItem: PropTypes.func,
  editItem: PropTypes.func,
  form: PropTypes.any,
  item: PropTypes.object,
}
const CreateUpdateBank = Form.create()(CreateUpdateBankContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateBank)