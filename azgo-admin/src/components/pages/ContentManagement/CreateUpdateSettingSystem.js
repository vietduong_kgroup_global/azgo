import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { useParams } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Button, Form, Input, Row, Col, Checkbox, Typography } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Card, CardBody, CardHeader } from 'reactstrap'
import 'Stores/ContentManagement/SettingSystemManagement/Reducers'
import 'Stores/ContentManagement/SettingSystemManagement/Sagas'
import { SettingSystemManagementSelectors } from 'Stores/ContentManagement/SettingSystemManagement/Selectors'
import { SettingSystemManagementActions } from 'Stores/ContentManagement/SettingSystemManagement/Actions'
import CKEditor from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
const { Text } = Typography;
const CreateUpdateSettingSystemContainer = ({ form, getItem, item, editItem, createItem }) => {
  const { getFieldDecorator, getFieldsValue } = form
  const formItemLayout = {
    labelCol: {
      xs: { span: 10 },
      sm: { span: 10 },
    },
    wrapperCol: {
      xs: { span: 14 },
      sm: { span: 14 },
    },
  }
  const tailFormItemLayout = {
    labelCol: {
      xs: { span: 24},
      sm: { span: 24},
    },
    wrapperCol: {
      xs: { span: 24, offset: 12  },
      sm: { span: 24 , offset: 12 },
    },
  }
  const CheckboxItemLayout = {
    labelCol: {
      xs: { span: 24},
      sm: { span: 24},
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 24 },
    },
  }
   
  const handleSubmit = () => {
    let fields = form.getFieldsValue()
    form.validateFields(err => {
      if (err) {
        return false
      }
      if(item && item.get('id')) {
        editItem({ 
          tax:  Number(fields.tax),
          vat:  Number(fields.vat),
          hoursAllowCancel:  Number(fields.hoursAllowCancel),
          minimumPercentOrder:  Number(fields.minimumPercentOrder),
          remindTime:  Number(fields.remindTime),
          appleDriverVersion:  fields.appleDriverVersion,
          appleDriverForceUpdate:  fields.appleDriverForceUpdate,
          googleDriverVersion:  fields.googleDriverVersion,
          googleDriverForceUpdate:  fields.googleDriverForceUpdate,
          appleCustomerVersion:  fields.appleCustomerVersion,
          appleCustomerForceUpdate:  fields.appleCustomerForceUpdate,
          googleCustomerVersion:  fields.googleCustomerVersion,
          googleCustomerForceUpdate:  fields.googleCustomerForceUpdate,
          id: item.get('id'),
        })
      } 
      else 
      createItem({
        tax:  Number(fields.tax),
        vat:  Number(fields.vat),
        hoursAllowCancel: Number(fields.hoursAllowCancel),
        minimumPercentOrder:  Number(fields.minimumPercentOrder),
        apple:  Number(fields.remindTime),
        appleDriverVersion:  fields.appleDriverVersion,
        appleDriverForceUpdate:  fields.appleDriverForceUpdate,
        googleDriverVersion:  fields.googleDriverVersion,
        googleDriverForceUpdate:  fields.googleDriverForceUpdate,
        appleCustomerVersion:  fields.appleCustomerVersion,
        appleCustomerForceUpdate:  fields.appleCustomerForceUpdate,
        googleCustomerVersion:  fields.googleCustomerVersion,
        googleCustomerForceUpdate:  fields.googleCustomerForceUpdate,
       
      })
    })
  }
  const onChangeCheckBox = () => {

  }
  console.log(item && item.get('appleDriverForceUpdate'))
  useEffect(() => {
    getItem()
  }, []) 
  return (
    <div className="animated fadeIn">
      <Card className="card-accent-warning">
        <CardHeader>
          <TitlePage data="Chỉnh sửa cài đặt hệ thống" />
          <i className="fa fa-pencil" />
        </CardHeader>
        <CardBody>
          <Form
            id="frmPolicy"
            {...formItemLayout}
            className="input-floating form-ant-custom-policy">
            <Form.Item label="Thuế giá trị gia tăng">
              {getFieldDecorator('vat', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue: item && item.get('vat'),
              })(<Input type="number" />)}
            </Form.Item>
            <Form.Item label="Thuế Thu nhập cá nhân">
              {getFieldDecorator('tax', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue: item && item.get('tax'),
              })(<Input type="number" />)}
            </Form.Item>
            <Form.Item label="Số Giờ cho phép tài xế huỷ">
              {getFieldDecorator('hoursAllowCancel', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue: item && item.get('hoursAllowCancel'),
              })(<Input type="number" />)}
            </Form.Item> 
            <Form.Item label="Số Giờ nhắc nhở" extra="Số Giờ nhắc nhở khi đơn hàng chưa được tài xế xác nhận">
              {getFieldDecorator('remindTime', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue: item && item.get('remindTime'),
              })(<Input type="number" />)}
            </Form.Item> 
            <Form.Item label="% Tài khoản thanh toán thấp nhất"  extra={`Phần trăm mà số tiền trong ví không được nhỏ hơn so với giá trị đơn hàng`}>
              {getFieldDecorator('minimumPercentOrder', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue: item && item.get('minimumPercentOrder'),
              })(<Input type="number" />)}
            </Form.Item> 
            <Row gutter={[16, 24]}>
              <Col span={24}>
                <Text strong>App Driver</Text>
              </Col>
            </Row>
            <Row gutter={[16, 24]}>
              <Col span={14} align = "middle" justify = "center">
                <Form.Item label="Version Apple">
                  {getFieldDecorator('appleDriverVersion', {
                    rules: [
                      {
                        required: false,
                        message: 'Trường bắt buộc!',
                      },
                    ],
                    initialValue: item && item.get('appleDriverVersion'),
                  })(<Input placeholder = ''/>)}
                </Form.Item> 
              </Col>
              <Col span={6}>
                <Form.Item
                  name="agreement"
                  valuePropName="checked"
                  {...CheckboxItemLayout}
                >
                  {getFieldDecorator('appleDriverForceUpdate', {
                    initialValue: item && item.get('appleDriverForceUpdate'),
                    valuePropName: 'checked'
                  })(
                    <Checkbox >Force Update</Checkbox>
                  )}
                </Form.Item>
               </Col>
              <Col span={4}>  <Button type="default">Send Notify</Button></Col>
            </Row>
            <Row gutter={[16, 24]}>
              <Col span={14} align = "middle" justify = "center">
                <Form.Item label="Version Google">
                  {getFieldDecorator('googleDriverVersion', {
                    rules: [
                      {
                        required: false,
                        message: 'Trường bắt buộc!',
                      },
                    ],
                    initialValue: item && item.get('googleDriverVersion'),
                  })(<Input placeholder = ''/>)}
                </Form.Item> 
              </Col>
              <Col span={6}>
                <Form.Item
                  name="agreement"
                  valuePropName="checked"
                  {...CheckboxItemLayout}
                >
                  {getFieldDecorator('googleDriverForceUpdate', {
                    initialValue: item && item.get('googleDriverForceUpdate'),
                    valuePropName: 'checked'
                  })(
                    <Checkbox>Force Update</Checkbox>
                  )}
                </Form.Item>
               </Col>
              <Col span={4}>  <Button type="default">Send Notify</Button></Col>
            </Row>
            
            <Row gutter={[16, 24]}>
              <Col span={24}>
                <Text strong>App Customer</Text>
              </Col>
            </Row> 
            <Row gutter={[16, 24]}>
              <Col span={14} align = "middle" justify = "center">
                <Form.Item label="Version Apple">
                  {getFieldDecorator('appleCustomerVersion', {
                    rules: [
                      {
                        required: false,
                        message: 'Trường bắt buộc!',
                      },
                    ],
                    initialValue: item && item.get('appleCustomerVersion'),
                  })(<Input placeholder = ''/>)}
                </Form.Item> 
              </Col>
              <Col span={6}>
                <Form.Item
                  name="agreement"
                  valuePropName="checked"
                  {...CheckboxItemLayout}
                >
                  {getFieldDecorator('appleCustomerForceUpdate', {
                    initialValue: item && item.get('appleCustomerForceUpdate'),
                    valuePropName: 'checked'
                  })(
                    <Checkbox onChange={onChangeCheckBox}>Force Update</Checkbox>
                  )}
                </Form.Item>
               </Col>
              <Col span={4}>  <Button type="default">Send Notify</Button></Col>
            </Row>
            <Row gutter={[16, 24]}>
              <Col span={14} align = "middle" justify = "center">
                <Form.Item label="Version Google">
                  {getFieldDecorator('googleCustomerVersion', {
                    rules: [
                      {
                        required: false,
                        message: 'Trường bắt buộc!',
                      },
                    ],
                    initialValue: item && item.get('googleCustomerVersion'),
                  })(<Input placeholder = ''/>)}
                </Form.Item> 
              </Col>
              <Col span={6}>
                <Form.Item
                  name="agreement"
                  valuePropName="checked"
                  {...CheckboxItemLayout}
                >
                  {getFieldDecorator('googleCustomerForceUpdate', {
                    initialValue: item && item.get('googleCustomerForceUpdate'),
                    valuePropName: 'checked'
                  })(
                    <Checkbox onChange={onChangeCheckBox}>Force Update</Checkbox>
                  )}
                </Form.Item>
               </Col>
              <Col span={4}>  <Button type="default">Send Notify</Button></Col>
            </Row>
            <Form.Item {...tailFormItemLayout}>
              <Button type="primary" htmlType="submit" onClick={handleSubmit}>
                Xác nhận
              </Button>
            </Form.Item>
          </Form>
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: SettingSystemManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  editItem: values => dispatch(SettingSystemManagementActions.editItemRequest(values)),
  createItem: values => dispatch(SettingSystemManagementActions.createItemRequest(values)),
  getItem: () => dispatch(SettingSystemManagementActions.getItemDetailRequest()),
})

CreateUpdateSettingSystemContainer.propTypes = {
  getItem: PropTypes.func,
  editItem: PropTypes.func,
  createItem: PropTypes.func,
  form: PropTypes.any,
  item: PropTypes.object,
}
const CreateUpdatePolicy = Form.create()(CreateUpdateSettingSystemContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdatePolicy)