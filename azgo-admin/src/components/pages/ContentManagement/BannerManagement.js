import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Button } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import { Table } from 'antd'
import 'Stores/ContentManagement/BannerManagement/Reducers'
import 'Stores/ContentManagement/BannerManagement/Sagas'
import { BannerManagementSelectors } from 'Stores/ContentManagement/BannerManagement/Selectors'
import { BannerManagementActions } from 'Stores/ContentManagement/BannerManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import * as configsEnum from 'Utils/enum'
import { Config } from 'Config'
import { LoadingSelectors } from 'Stores/Loading/Selectors'

const BannerManagement = ({
  getItems,
  items,
  clearItems,
  history,
  showLoading,
}) => {
  const titlePage = 'Danh sách Banner'
  const handleOpenUpdate = id => {
    history.push('/admin/content-management/banner/edit/' + id)
  }
  const Style = {
    maxWidth: '100px',
    width: '100px',
    display: 'flex',
    alignItems: 'center',
    padding: '5px',
    justifyContent: 'center',
    border: '1px solid #e8e8e8',
  }
  const columns = [
    {
      title: 'Tên',
      key: 'banner',
      dataIndex: 'banner',
    },
    {
      title: 'Hình ảnh',
      dataIndex: 'image',
      render: (key, value) => {
        return (
          <div style={Style}>
            {value.image && (
              <img
                src={value.image.address}
              />
            )}
          </div>
        )
      },
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      render: value => {
        const result = configsEnum.BANNER_STATUS_ARR.find(item => {
          return item.key === value
        })
        if (!result) return ''
        return (
          <span className={`btn-status btn-status-${result.label}`}>
            {result.label}
          </span>
        )
      },
    },
    {
      title: 'Chức năng',
      width: 140,
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Button
            color="light"
            size="sm"
            className="btn-sm-ct"
            style={{ marginRight: '5px' }}
            onClick={() => handleOpenUpdate(value.id)}>
            <i className="fa fa-pencil" />
            &nbsp;Sửa
          </Button>
        </>
      ),
    },
  ]
  useEffect(() => {
    getItems()
    return () => {
      clearItems()
    }
  }, [])
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <Link to="/admin/content-management/banner/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <Table
        rowKey={record => record.id}
        columns={columns}
        pagination={false}
        loading={showLoading}
        dataSource={items && items.size > 0 ? items.toJS() : null}
      />
    </div>
  )
}

BannerManagement.propTypes = {
  items: PropTypes.object,
  getItems: PropTypes.func,
  clearItems: PropTypes.func,
  history: PropTypes.any,
  showLoading: PropTypes.bool,
}

const mapStateToProps = state => ({
  showLoading: LoadingSelectors.getLoadingList(state),
  items: BannerManagementSelectors.getItems(state),
})

const mapDispatchToProps = dispatch => ({
  getItems: () => dispatch(BannerManagementActions.getItemsRequest()),
  clearItems: () => dispatch(BannerManagementActions.clearItems()),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(BannerManagement)