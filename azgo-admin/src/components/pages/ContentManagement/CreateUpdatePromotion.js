import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Link, useParams } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Button, Form, Input, Checkbox, Select } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Card, CardBody, CardHeader } from 'reactstrap'
import 'Stores/ContentManagement/PromotionManagement/Reducers'
import 'Stores/ContentManagement/PromotionManagement/Sagas'
import { PromotionManagementSelectors } from 'Stores/ContentManagement/PromotionManagement/Selectors'
import { PromotionManagementActions } from 'Stores/ContentManagement/PromotionManagement/Actions'
import * as config from 'Utils/enum'

const CreateUpdatePromotionContainer = ({
  form,
  createItem,
  getItem,
  item,
  editItem,
}) => {
  let name = item.get('banner') ? item.get('banner') : ''
  const { getFieldDecorator, getFieldsValue, setFieldsValue } = form
  let { id } = useParams()
  const [typePromotion, setTypePromotion] = useState(0)
  const [checkAutoGenerate, setCheckAutoGenerate] = useState(false)
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  }
  const onChangeTypePromotion = (value) => {
    setTypePromotion(value)
  }
  const onChangeCheckox = (e) => {
    setCheckAutoGenerate(e.target.checked);
    form.validateFields([])
  }
  const handleSubmit = () => {
    let validateArr = [];
    if(!checkAutoGenerate) validateArr.push('coupon')
    if(checkAutoGenerate) validateArr.push('quantity')
    if(typePromotion === config.MANAGEMENT_TYPE_PROMOTION.PERCENT) validateArr.push('percentDiscount')
    if(typePromotion === config.MANAGEMENT_TYPE_PROMOTION.VALUE) validateArr.push('priceDiscount')
    // form.validateFields(err => {
    form.validateFields(validateArr, { force: true }, (err, values) => {
      if (err) {
        return false
      }
      let field = { ...getFieldsValue()};

      if(!checkAutoGenerate){
        field.quantity = null;
      }
      else{
        field.quantity = parseInt(form.getFieldsValue().quantity, 10);
      }
      if(typePromotion === config.MANAGEMENT_TYPE_PROMOTION.PERCENT){
        field.percentDiscount = parseInt(form.getFieldsValue().percentDiscount, 10);
        field.priceDiscount = null;
      }
      else{
        field.priceDiscount = parseInt(form.getFieldsValue().priceDiscount, 10);
        field.percentDiscount = null;
      }
      if (id) {
        editItem({
          ...field,
          uuid: id,
        })
      } else {
        createItem({
          ...field,
        })
      }
    })
  }
  useEffect(() => {
    if(id && item.get('percentDiscount') ){
    setTypePromotion(config.MANAGEMENT_TYPE_PROMOTION.PERCENT)
    }
    else if(id && item.get('priceDiscount')) {
      setTypePromotion(config.MANAGEMENT_TYPE_PROMOTION.VALUE)
    }
  }, [item])
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={id ? 'Chỉnh sửa khuyến mãi' : 'Thêm khuyến mãi mới'}
            name={name}
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
        </CardHeader>
        <CardBody>
          <Form
            id="frmPromotion"
            {...formItemLayout}
            className="input-floating form-ant-custom">
            <Form.Item label="Mã khuyến mãi">
              {getFieldDecorator('coupon', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue: id && item.get('coupon'),
              })(<Input disabled={checkAutoGenerate}/>)}
            <Checkbox checked={checkAutoGenerate} onChange={onChangeCheckox}>
              Tạo tự động
            </Checkbox>
            </Form.Item>
            <Form.Item label="Công ty">
              {getFieldDecorator('company', {
                initialValue: id && item.get('company'),
              })(<Input/>)}
            </Form.Item>
            <Form.Item label="Loại giảm giá">
              <Select
                onChange={onChangeTypePromotion}
                placeholder="Chọn"
                value={typePromotion}
              >
                {config.MANAGEMENT_TYPE_PROMOTION_ARR.map(item => (
                  <Select.Option value={item.key} key={`type_promotion_${item.key}`}>
                    {item.label}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
            {typePromotion === config.MANAGEMENT_TYPE_PROMOTION.PERCENT && 
              <Form.Item label="Giảm giá %">
                {getFieldDecorator('percentDiscount', {
                  rules: [
                    {
                      required: true,
                      message: 'Trường bắt buộc!',
                    },
                  ],
                  initialValue: id && item.get('percentDiscount'),
                })(<Input type="number" min={1} />)}
              </Form.Item>
            }
            {typePromotion === config.MANAGEMENT_TYPE_PROMOTION.VALUE &&
              <Form.Item label="Giảm giá theo số tiền">
                {getFieldDecorator('priceDiscount', {
                  rules: [
                    {
                      required: true,
                      message: 'Trường bắt buộc!',
                    },
                  ],
                  initialValue: id && item.get('priceDiscount'),
                })(<Input type="number" min={1} />)}
              </Form.Item>
            }
            {checkAutoGenerate &&
              <Form.Item label="Số lượng">
                {getFieldDecorator('quantity', {
                  rules: [
                    {
                      required: true,
                      message: 'Trường bắt buộc!',
                    },
                  ],
                  initialValue: id && item.get('quantity'),
                })(<Input type="number" min={1} />)}
              </Form.Item>
            }
            <Form.Item {...tailFormItemLayout}>
              <Button type="primary" htmlType="submit" onClick={handleSubmit}>
                Xác nhận
              </Button>
            </Form.Item>
          </Form>
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: PromotionManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(PromotionManagementActions.createItemRequest(values)),
  editItem: values =>
    dispatch(PromotionManagementActions.editItemRequest(values)),
  getItem: id => dispatch(PromotionManagementActions.getItemDetailRequest(id)),
})

CreateUpdatePromotionContainer.propTypes = {
  getItem: PropTypes.func,
  createItem: PropTypes.func,
  editItem: PropTypes.func,
  form: PropTypes.any,
  item: PropTypes.object,
}
const CreateUpdatePromotion = Form.create()(CreateUpdatePromotionContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdatePromotion)
