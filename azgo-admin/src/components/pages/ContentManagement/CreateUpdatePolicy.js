import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { useParams } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Button, Form, Input } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Card, CardBody, CardHeader } from 'reactstrap'
import 'Stores/ContentManagement/PolicyManagement/Reducers'
import 'Stores/ContentManagement/PolicyManagement/Sagas'
import { PolicyManagementSelectors } from 'Stores/ContentManagement/PolicyManagement/Selectors'
import { PolicyManagementActions } from 'Stores/ContentManagement/PolicyManagement/Actions'
import CKEditor from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'

const CreateUpdatePolicyContainer = ({ form, getItem, item, editItem, createItem }) => {
  const { getFieldDecorator, getFieldsValue } = form
  const formItemLayout = {
    labelCol: {
      xs: { span: 4 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 20 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 4,
        offset: 0,
      },
      sm: {
        span: 20,
        offset: 0,
      },
    },
  }
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      if(item.get('id')) {
        editItem({
          policy: form.getFieldsValue().policy,
          hotline: {
            report_problem: form.getFieldsValue().reportProblem,
            help_center: form.getFieldsValue().helpCenter,
          },
          id: item.get('id'),
        })
      } 
      else 
      createItem({
        policy: form.getFieldsValue().policy,
        hotline: {
          report_problem: form.getFieldsValue().reportProblem,
          help_center: form.getFieldsValue().helpCenter,
        }, 
      })
    })
  }

  useEffect(() => {
    getItem()
  }, [])

  return (
    <div className="animated fadeIn">
      <Card className="card-accent-warning">
        <CardHeader>
          <TitlePage data="Chỉnh sửa Hotline and policy" />
          <i className="fa fa-pencil" />
        </CardHeader>
        <CardBody>
          <Form
            id="frmPolicy"
            {...formItemLayout}
            className="input-floating form-ant-custom-policy">
            <Form.Item label="SĐT báo cáo sự cố">
              {getFieldDecorator('reportProblem', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue: item.getIn(['hotline', 'reportProblem']),
              })(<Input type="number" />)}
            </Form.Item>
            <Form.Item label="SĐT tổng đài trợ giúp">
              {getFieldDecorator('helpCenter', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue: item.getIn(['hotline', 'helpCenter']),
              })(<Input type="number" />)}
            </Form.Item>
            <Form.Item {...tailFormItemLayout} label="Policy">
              {getFieldDecorator('policy', {
                valuePropName: 'data',
                getValueFromEvent: (event, editor) => {
                  const data = editor.getData()
                  return data
                },
                rules: [{ required: true, message: 'Please enter the policy' }],
                initialValue: item.get('policy'),
              })(<CKEditor editor={ClassicEditor} />)}
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
              <Button type="primary" htmlType="submit" onClick={handleSubmit}>
                Xác nhận
              </Button>
            </Form.Item>
          </Form>
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: PolicyManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  editItem: values => dispatch(PolicyManagementActions.editItemRequest(values)),
  createItem: values => dispatch(PolicyManagementActions.createItemRequest(values)),
  getItem: () => dispatch(PolicyManagementActions.getItemDetailRequest()),
})

CreateUpdatePolicyContainer.propTypes = {
  getItem: PropTypes.func,
  editItem: PropTypes.func,
  createItem: PropTypes.func,
  form: PropTypes.any,
  item: PropTypes.object,
}
const CreateUpdatePolicy = Form.create()(CreateUpdatePolicyContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdatePolicy)