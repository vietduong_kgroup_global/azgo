import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Link, useParams } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Button, Form, Input, Checkbox } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Config } from 'Config'
import { Card, CardBody, CardHeader } from 'reactstrap'
import UploadImage from 'components/molecules/UploadImage'
import 'Stores/ContentManagement/BannerManagement/Reducers'
import 'Stores/ContentManagement/BannerManagement/Sagas'
import { BannerManagementSelectors } from 'Stores/ContentManagement/BannerManagement/Selectors'
import { BannerManagementActions } from 'Stores/ContentManagement/BannerManagement/Actions'

const CreateUpdateBannerContainer = ({
  form,
  createItem,
  getItem,
  item,
  editItem,
}) => {
  let name = item.get('banner') ? item.get('banner') : ''
  const { getFieldDecorator, getFieldsValue, setFieldsValue } = form
  let { id } = useParams()
  const [imageView, setImageView] = useState('')
  const [checked, setChecked] = useState(false)
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  }
  const callbackHandleImage = value => {
    setFieldsValue({
      image: value,
    })
  }
  const onChange = e => {
    setChecked(e.target.checked)
  }
  const handleSubmit = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }

      if (id) {
        editItem({
          ...getFieldsValue(),
          status: form.getFieldsValue().status === true ? 1 : 0,
          id: parseInt(id, 10),
        })
      } else {
        createItem({
          ...getFieldsValue(),
          status: form.getFieldsValue().status === true ? 1 : 0,
        })
      }
    })
  }
  useEffect(() => {
    if (item.size > 0) {
      setImageView( item.getIn(['image', 'address']))
    }
    if (item.get('status') === 1) {
      setChecked(true)
    } else {
      setChecked(false)
    }
    return () => {
      setImageView(null)
    }
  }, [item])
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={id ? 'Chỉnh sửa banner' : 'Thêm banner mới'}
            name={name}
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
        </CardHeader>
        <CardBody>
          <Form
            id="frmBanner"
            {...formItemLayout}
            className="input-floating form-ant-custom">
            <Form.Item label="Tên">
              {getFieldDecorator('banner', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue: id && item.get('banner'),
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Hình ảnh" className="ant-form-item-load">
              {getFieldDecorator('image', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
                initialValue:
                  id && item.get('image') ? item.get('image').toJS() : null,
              })(
                <>
                  <UploadImage
                    imageProp={imageView}
                    handleParent={callbackHandleImage}
                  />
                </>
              )}
            </Form.Item>
            <Form.Item label="Trạng thái">
              {getFieldDecorator('status', {
                rules: [
                  {
                    required: false,
                  },
                ],
                initialValue: checked,
              })(
                <Checkbox checked={checked} onChange={onChange}>
                  Hiển thị
                </Checkbox>
              )}
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
              <Button type="primary" htmlType="submit" onClick={handleSubmit}>
                Xác nhận
              </Button>
            </Form.Item>
          </Form>
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: BannerManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  createItem: values =>
    dispatch(BannerManagementActions.createItemRequest(values)),
  editItem: values => dispatch(BannerManagementActions.editItemRequest(values)),
  getItem: id => dispatch(BannerManagementActions.getItemDetailRequest(id)),
})

CreateUpdateBannerContainer.propTypes = {
  getItem: PropTypes.func,
  createItem: PropTypes.func,
  editItem: PropTypes.func,
  form: PropTypes.any,
  item: PropTypes.object,
}
const CreateUpdateBanner = Form.create()(CreateUpdateBannerContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateBanner)