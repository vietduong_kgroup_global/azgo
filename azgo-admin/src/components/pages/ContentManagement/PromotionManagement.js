import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Button } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import { Popconfirm, Tag } from 'antd'
import FormSearch from 'components/molecules/FormSearch'
import 'Stores/ContentManagement/PromotionManagement/Reducers'
import 'Stores/ContentManagement/PromotionManagement/Sagas'
import { PromotionManagementSelectors } from 'Stores/ContentManagement/PromotionManagement/Selectors'
import { PromotionManagementActions } from 'Stores/ContentManagement/PromotionManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import TableList from 'components/organisms/TableList'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { formatPrice } from 'Utils/helper'

const PromotionManagement = ({
  getItems,
  items,
  clearItems,
  history,
  showLoading,
  filter,
  setFilter,
  totalCount,
  deleteItem,
}) => {
  const titlePage = 'Danh sách khuyến mãi'
  const [search, setSearch] = useState()
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const handleSearch = value => {
    setSearch(value)
  }
  const handleOpenUpdate = uuid => {
    history.push('/admin/content-management/promotion/edit/' + uuid)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  const columns = [
    {
      title: 'Mã khuyến mãi',
      key: 'coupon',
      // dataIndex: 'coupon',
      render: (key, value) => (
        <>
          {value.coupon.toUpperCase()}
          {value.company && <div>
            <Tag color="red">{value.company}</Tag>
          </div>}
        </>
      ),
    },
    {
      title: 'Giảm giá %',
      key: 'percentDiscount',
      dataIndex: 'percentDiscount',
    },
    {
      title: 'Giảm giá theo số tiền',
      dataIndex: 'priceDiscount',
      render: value => <div>{formatPrice(value)}</div>,
    },
    {
      title: 'Số lần sử dụng còn lại',
      key: 'count',
      dataIndex: 'count',
    },
    {
      title: 'Chức năng',
      width: 140,
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Button
            color="light"
            size="sm"
            className="btn-sm-ct"
            style={{ marginRight: '5px' }}
            onClick={() => handleOpenUpdate(value.uuid)}>
            <i className="fa fa-pencil" />
            &nbsp;Sửa
          </Button>
          <Popconfirm
            placement="top"
            onConfirm={() => handleConfirmDelete(value.uuid)}
            title="Bạn có chắc chắn muốn xóa ?"
            okText="Có"
            cancelText="Không">
            <Button color="danger" size="sm" className="btn-sm-ct">
              <i className="fa fa-trash-o" />
              &nbsp;Xóa
            </Button>
          </Popconfirm>
        </>
      ),
    },
  ]
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter)
  }, [filter])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    getItems()
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Link to="/admin/content-management/promotion/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <TableList
        columns={columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </div>
  )
}

PromotionManagement.propTypes = {
  items: PropTypes.object,
  getItems: PropTypes.func,
  clearItems: PropTypes.func,
  history: PropTypes.any,
  showLoading: PropTypes.bool,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
  totalCount: PropTypes.number,
  deleteItem: PropTypes.func,
}

const mapStateToProps = state => ({
  showLoading: LoadingSelectors.getLoadingList(state),
  items: PromotionManagementSelectors.getItems(state),
  filter: PromotionManagementSelectors.getFilter(state),
  totalCount: PromotionManagementSelectors.getTotalCount(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(PromotionManagementActions.setFilter(filter)),
  getItems: filter =>
    dispatch(PromotionManagementActions.getItemsRequest(filter)),
  deleteItem: id => dispatch(PromotionManagementActions.deleteItemRequest(id)),
  clearItems: () => dispatch(PromotionManagementActions.clearItems()),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(PromotionManagement)
