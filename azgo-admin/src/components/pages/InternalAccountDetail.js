import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import ItemInfo from 'components/molecules/ItemInfo'
import { useParams } from 'react-router-dom'
import 'Stores/InternalManagement/Reducers'
import 'Stores/InternalManagement/Sagas'
import { InternalManagementSelectors } from 'Stores/InternalManagement/Selectors'
import { InternalManagementActions } from 'Stores/InternalManagement/Actions'
import PropTypes from 'prop-types'
import { Form, Modal } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Card, CardBody, CardHeader } from 'reactstrap'
import * as configsEnum from 'Utils/enum'
import { formatPhone } from 'Utils/helper'
import { Config } from 'Config'

const InternalAccountDetailContainer = ({ getItem, item, history }) => {
  const [gender, setGender] = useState({})
  const [previewVisible, setPreviewVisible] = useState(false)
  const [imageAvatar, setImageAvatar] = useState('')
  const [adminType, setAdminType] = useState({})
  const [phone, setPhone] = useState()
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  const handleOpenUpdate = () => {
    history.push('/admin/internal-management/edit/' + item.get('userId'))
  }
  const handlePreviewImg = () => {
    setPreviewVisible(true)
  }
  useEffect(() => {
    const result = configsEnum.MANAGEMENT_GENDER_ARR.find(itemGender => {
      return itemGender.key === item.getIn(['adminProfile', 'gender'])
    })
    const resultType = configsEnum.ACCOUNT_INITIAL_ROLE_ARR.find(itemType => {
      return itemType.key === item.get('type')
    })
    setGender(result)
    setAdminType(resultType)
    if (item.getIn(['adminProfile', 'imageProfile', 'address'])) {
      setImageAvatar(item.getIn(['adminProfile', 'imageProfile', "address"]))
      // setImageAvatar(
      //   Config.DEV_URL.IMAGE +
      //   '/' +
      //   item.getIn(['adminProfile', 'imageProfile', 'filePath']) +
      //   '/' +
      //   item.getIn(['adminProfile', 'imageProfile', 'fileName'])
      // )
    }
    if (item.size > 0) {
      setPhone(formatPhone(item.get('countryCode'), item.get('phone')))
    }
  }, [item])
  return (
    <div className="animated fadeIn">
      <Card className="card-accent-primary">
        <CardHeader>
          <TitlePage
            data="Thông tin chi tiết tài khoản nội bộ"
            name={item.getIn(['adminProfile', 'fullName'])}
          />
          <i
            className="fa fa-pencil"
            onClick={() => handleOpenUpdate()}
            style={{ cursor: 'pointer' }}
          />
        </CardHeader>
        <CardBody>
          <div className="detail-user">
            <div className="detail-user-image">
              {imageAvatar ? (
                <div className="detail-user-image-inner">
                  <img
                    src={imageAvatar}
                    alt="avatar"
                    onClick={() => handlePreviewImg()}
                  />
                </div>
              ) : (
                  <div className="bg-avatar" />
                )}
            </div>
            <div className="detail-user-info">
              <ItemInfo label="Email:" content={item.get('email')} />
              <ItemInfo
                label="Họ:"
                content={item.getIn(['adminProfile', 'lastName'])}
              />
              <ItemInfo
                label="Tên:"
                content={item.getIn(['adminProfile', 'firstName'])}
              />
              <ItemInfo label="Số điện thoại:" content={phone} />
              <ItemInfo
                label="Giới tính:"
                content={gender ? gender.label : null}
              />
              <ItemInfo
                label="Khu vực:"
                content={item.getIn(['adminProfile', 'areaInfo']) && item.getIn(['adminProfile', 'areaInfo', "name"])}
              />
              <ItemInfo label="Quyền:" content={adminType ? adminType.label : null} />
            </div>
          </div>
          <Modal
            visible={previewVisible}
            footer={null}
            onCancel={() => setPreviewVisible(false)}>
            <div className="modal-image">
              <img alt="example" src={imageAvatar} />
            </div>
          </Modal>
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: InternalManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(InternalManagementActions.getItemDetailRequest(id)),
})

InternalAccountDetailContainer.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
  history: PropTypes.any,
}
const InternalAccountDetail = Form.create()(InternalAccountDetailContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(InternalAccountDetail)
