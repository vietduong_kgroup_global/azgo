import React, { useEffect, useState } from 'react'
import {
  Card,
  CardBody,
  CardHeader,
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  Button,
} from 'reactstrap'
import styles from './style.module.css'
import 'Stores/Van/TripManagement/Reducers'
import 'Stores/Van/TripManagement/Sagas'
import { useParams, withRouter } from 'react-router-dom'
import { TripManagementSelectors } from 'Stores/Van/TripManagement/Selectors'
import { TripManagementActions } from 'Stores/Van/TripManagement/Actions'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import moment from 'moment'
import TitlePage from 'components/atoms/TitlePage/'
import * as configsEnum from 'Utils/enum'
import { Config } from 'Config'
import { formatPrice, formatPhone } from 'Utils/helper'
import { Modal } from 'antd'

const TripDetail = ({ getItem, item }) => {
  const [status, setStatus] = useState({})
  const [statusPayment, setStatusPayment] = useState({})
  const [statusMethod, setStatusMethod] = useState({})
  const [imageUrl, setImageUrl] = useState([])
  const [imageUrlPopup, setImageUrlPopup] = useState('')
  const [previewVisible, setPreviewVisible] = useState(false)
  const titlePage = 'Thông tin chuyến xe'
  let { id } = useParams()
  const style = {
    display: 'flex',
    flexWrap: 'wrap',
  }
  const handlePreviewImg = idx => {
    for (let i = 0; i < imageUrl.length; i++) {
      if (idx === i) {
        setImageUrlPopup(
          Config.DEV_URL.IMAGE +
            '/' +
            imageUrl[i].filePath +
            '/' +
            imageUrl[i].fileName
        )
      }
    }
    setPreviewVisible(true)
  }
  const handleFormatPrice = data => {
    if (typeof data === 'string') return data
    return formatPrice(data)
  }
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  useEffect(() => {
    const result = configsEnum.MANAGEMENT_STATUS_VAN_TRIP_ARR.find(itemTrip => {
      return itemTrip.key === item.get('status')
    })
    const resultPayment = configsEnum.PAYMENT_STATUS_ARR.find(itemPayment => {
      return itemPayment.key === item.get('paymentStatus')
    })
    const resultMethod = configsEnum.PAYMENT_METHOD_ARR.find(itemMethod => {
      return itemMethod.key === item.get('paymentMethod')
    })
    if (item.get('images')) {
      setImageUrl(...[item.get('images').toJS()])
    }
    setStatus(result)
    setStatusPayment(resultPayment)
    setStatusMethod(resultMethod)
    return () => {
      setImageUrl([])
    }
  }, [item])
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <div style={{ width: '100%' }}>
          <Row style={{ marginBottom: '20px' }}>
            <Col sm="12" xl="6">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Thông tin chuyến xe</strong>
                </CardHeader>
                <CardBody>
                  <ListGroup>
                    <ListGroupItem className="justify-content-between">
                      Mã chuyến
                      <div className="float-right">
                        <strong>{item && item.get('id')}</strong>
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Điểm đi
                      <div className="float-right">
                        {item &&
                          item.getIn(['routeInfo', 'startPoint', 'address'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Điểm đến
                      <div className="float-right">
                        {item &&
                          item.getIn(['routeInfo', 'endPoint', 'address'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Ngày
                      <div className="float-right">
                        {item &&
                          moment(new Date(item.get('createdAt'))).format(
                            'DD-MM-YYYY'
                          )}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Giờ
                      <div className="float-right">
                        {item && item.get('startHour')}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Trạng thái
                      <div className="float-right">
                        <Button color={status && status.color} size="sm">
                          {status && status.label}
                        </Button>
                      </div>
                    </ListGroupItem>
                  </ListGroup>
                </CardBody>
              </Card>
            </Col>
            <Col sm="12" xl="6">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Thông tin tài xế</strong>
                </CardHeader>
                <CardBody>
                  <ListGroup>
                    <ListGroupItem className="justify-content-between">
                      Họ và tên
                      <div className="float-right">
                        {item && item.getIn(['driverProfile', 'fullName'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Xe
                      <div className="float-right">
                        {item &&
                          item.getIn([
                            'driverProfile',
                            'users',
                            'vehicles',
                            '0',
                            'licensePlates',
                          ])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Loại xe
                      <div className="float-right">
                        {item && item.getIn(['service', 'name'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Hãng xe
                      <div className="float-right">
                        {item &&
                          item.getIn([
                            'driverProfile',
                            'users',
                            'vehicles',
                            '0',
                            'vehicleBrand',
                            'name',
                          ])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Số điện thoại
                      <div className="float-right">
                        {formatPhone(
                          item.getIn(['driverProfile', 'users', 'countryCode']),
                          item.getIn(['driverProfile', 'users', 'phone'])
                        )}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Đánh giá
                      <div className="float-right">
                        {item && item.get('ratingDriver')}/5
                      </div>
                    </ListGroupItem>
                  </ListGroup>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row style={{ marginBottom: '20px' }}>
            <Col sm="12" xl="6">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Thông tin hàng hóa</strong>
                </CardHeader>
                <CardBody>
                  <ListGroup style={{ marginBottom: '10px' }}>
                    <ListGroupItem className="justify-content-between">
                      Thông tin hàng hóa
                      <div className="float-right">
                        {item && item.get('luggageInfo')}
                      </div>
                    </ListGroupItem>
                  </ListGroup>
                  <ListGroup>
                    <ListGroupItem className="justify-content-between">
                      Hình ảnh
                      <div className="float-right">
                        <div
                          className="col-upload-img col-upload-img-arr"
                          style={style}>
                          {imageUrl &&
                            imageUrl.map((item, idx) => (
                              <div className="frame-img" key={idx}>
                                <img
                                  onClick={() => handlePreviewImg(idx)}
                                  src={
                                    `${Config.DEV_URL.IMAGE}` +
                                    '/' +
                                    `${item.filePath}` +
                                    '/' +
                                    `${item.fileName}`
                                  }
                                  alt="Img"
                                />
                              </div>
                            ))}
                        </div>
                      </div>
                      <Modal
                        visible={previewVisible}
                        footer={null}
                        onCancel={() => setPreviewVisible(false)}>
                        <div className="modal-image">
                          <img alt="image" src={imageUrlPopup} />
                        </div>
                      </Modal>
                    </ListGroupItem>
                  </ListGroup>
                </CardBody>
              </Card>
            </Col>
            <Col sm="12" xl="6">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Thông tin giao hàng</strong>
                </CardHeader>
                <CardBody>
                  <ListGroup style={{ marginBottom: '20px' }}>
                    <ListGroupItem active tag="a" href="#" action>
                      Thông tin người gởi
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Họ tên
                      <div className="float-right">
                        {item && item.getIn(['customerProfile', 'fullName'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Số điện thoại
                      <div className="float-right">
                        {formatPhone(
                          item.getIn([
                            'customerProfile',
                            'users',
                            'countryCode',
                          ]),
                          item.getIn(['customerProfile', 'users', 'phone'])
                        )}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Địa chỉ
                      <div className="float-right">
                        {item && item.getIn(['customerProfile', 'address'])}
                      </div>
                    </ListGroupItem>
                  </ListGroup>
                  <ListGroup>
                    <ListGroupItem active tag="a" href="#" action>
                      Thông tin người nhận
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Họ tên
                      <div className="float-right">
                        {item && item.getIn(['receiverInfo', 'name'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Số điện thoại
                      <div className="float-right">
                        {item && item.getIn(['receiverInfo', 'phone'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Địa chỉ
                      <div className="float-right">
                        {item && item.getIn(['receiverInfo', 'address'])}
                      </div>
                    </ListGroupItem>
                  </ListGroup>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col sm="12" xl="6">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Thông tin cuốc phí</strong>
                </CardHeader>
                <CardBody>
                  <ListGroup>
                    <ListGroupItem className="justify-content-between">
                      Cuốc phí
                      <div className="float-right">
                        {handleFormatPrice(item && item.get('price'))}
                      </div>
                    </ListGroupItem>
                    {item.get('serviceOthers') &&
                      item.get('serviceOthers').map((item, idx) => (
                        <ListGroupItem
                          className="justify-content-between"
                          key={idx}>
                          {item.get('name')}
                          <div className="float-right">
                            {handleFormatPrice(item.get('price'))}
                          </div>
                        </ListGroupItem>
                      ))}
                    <ListGroupItem className="justify-content-between">
                      Mã khuyến mãi
                      <div className="float-right">0</div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Tiền khuyến mãi
                      <div className="float-right">0</div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Tổng tiền
                      <div className="float-right">
                        <div className="float-right">
                          {handleFormatPrice(item && item.get('price'))}
                        </div>
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Trạng thái thanh toán
                      <div className="float-right">
                        <Button
                          color={statusPayment && statusPayment.color}
                          size="sm">
                          {statusPayment && statusPayment.label}
                        </Button>
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Phương thức thanh toán
                      <div className="float-right">
                        {statusMethod && statusMethod.label}
                      </div>
                    </ListGroupItem>
                  </ListGroup>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  )
}

TripDetail.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  item: TripManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(TripManagementActions.getItemDetailRequest(id)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(TripDetail)
