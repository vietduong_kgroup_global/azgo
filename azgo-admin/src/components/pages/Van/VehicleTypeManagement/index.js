import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Button } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import VehicleTypeContainer from 'containers/Van/VehicleTypeManagement'
import FormSearch from 'components/molecules/FormSearch'
import 'Stores/Van/VehicleTypeManagement/Reducers'
import 'Stores/Van/VehicleTypeManagement/Sagas'
import { VehicleTypeManagementSelectors } from 'Stores/Van/VehicleTypeManagement/Selectors'
import { VehicleTypeManagementActions } from 'Stores/Van/VehicleTypeManagement/Actions'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'

const VehicleType = ({ filter, setFilter }) => {
  const titlePage = 'Danh sách loại xe van/bagac'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Link to="/van/vehicle-type/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <VehicleTypeContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
      />
    </div>
  )
}

VehicleType.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  filter: VehicleTypeManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(VehicleTypeManagementActions.setFilter(filter)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(VehicleType)
