import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import TableDriverManagementContainer from 'containers/Van/DriverManagement'
import FormSearch from 'components/molecules/FormSearch'
import { Link, withRouter } from 'react-router-dom'
import { Button } from 'reactstrap'
import 'Stores/Van/DriverManagement/Reducers'
import 'Stores/Van/DriverManagement/Sagas'
import { DriverManagementSelectors } from 'Stores/Van/DriverManagement/Selectors'
import { DriverManagementActions } from 'Stores/Van/DriverManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types'

const DriverManagement = ({ filter, setFilter }) => {
  const titlePage = 'Danh sách tài xế xe van/bagac'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Link to="/driver/van/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <TableDriverManagementContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
      />
    </div>
  )
}

DriverManagement.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  filter: DriverManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(DriverManagementActions.setFilter(filter)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(DriverManagement)
