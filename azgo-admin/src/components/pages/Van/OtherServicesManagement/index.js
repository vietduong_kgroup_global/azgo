import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Button, Card, CardBody, CardHeader, Row, Col } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import 'Stores/Van/OtherServicesManagement/Reducers'
import 'Stores/Van/OtherServicesManagement/Sagas'
import { OtherServicesManagementSelectors } from 'Stores/Van/OtherServicesManagement/Selectors'
import { OtherServicesManagementActions } from 'Stores/Van/OtherServicesManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { Form, Input, InputNumber, Modal, Popconfirm } from 'antd'
import styles from './styles.module.scss'
import { formatPrice } from 'Utils/helper'

const OtherServices = ({
  items,
  getItems,
  createItem,
  deleteItem,
  editItem,
  form,
  showLoading,
}) => {
  const { getFieldDecorator, getFieldsValue, setFieldsValue } = form
  const [visible, setVisible] = useState(false)
  const [idUpdate, setIdUpdate] = useState(null)
  const titlePage = 'Danh sách dịch vụ'
  const showModal = () => {
    setVisible(true)
    setIdUpdate(null)
    setFieldsValue({
      nameCreate: null,
      priceCreate: null,
    })
  }

  const handleOkUpdate = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      editItem({
        name: form.getFieldsValue().name,
        price: form.getFieldsValue().price,
        id: idUpdate,
      })
    })
  }

  const handleOkCreate = () => {
    form.validateFields(err => {
      if (err) {
        return false
      }
      createItem({
        name: form.getFieldsValue().nameCreate,
        price: form.getFieldsValue().priceCreate,
      })
    })
  }

  const Style = {
    cursor: 'pointer',
    marginLeft: '15px',
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }

  const handleCancel = () => {
    setVisible(false)
    setIdUpdate(null)
    setFieldsValue({
      nameCreate: null,
      priceCreate: null,
    })
  }
  const handleUpdate = item => {
    setIdUpdate(item.get('id'))
    // if (idUpdate === item.get('id')) {
    //   setIdUpdate(null)
    //   setFieldsValue({
    //     name: null,
    //     price: null,
    //   })
    // }
  }
  const handleFormatPrice = data => {
    if (typeof data === 'string') return data
    return formatPrice(data)
  }
  useEffect(() => {
    getItems()
  }, [])
  useEffect(() => {
    if (!showLoading) {
      setVisible(false)
      setIdUpdate(null)
    }
  }, [showLoading])
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <Button color="success" onClick={showModal}>
          <i className="fa fa-plus-square" />
          &nbsp;Thêm mới
        </Button>
      </div>
      <Row>
        {items &&
          items.map((item, idx) => (
            <Col xs="12" sm="6" md="4" key={`other_service_${item.get('id')}`}>
              <Card className="border-info">
                <CardHeader>
                  <strong>{idx + 1}.</strong>
                  <div className={styles.cardHeaderActions}>
                    <i
                      style={{ cursor: 'pointer' }}
                      className="fa fa-pencil"
                      onClick={() => handleUpdate(item)}
                    />
                    <Popconfirm
                      placement="bottom"
                      onConfirm={() => handleConfirmDelete(item.get('id'))}
                      title="Bạn có muốn xóa?"
                      okText="Có"
                      cancelText="Không">
                      <span style={Style}>
                        <i className="fa fa-trash" />
                      </span>
                    </Popconfirm>
                  </div>
                </CardHeader>
                <CardBody>
                  <Form id="frmOtherServices" className="input-floating">
                    <div className={styles.fieldForm}>
                      <span className={styles.label}>Tên dịch vụ:</span>
                      {idUpdate === item.get('id') ? (
                        <Form.Item className={styles.fieldFormItem}>
                          {getFieldDecorator('name', {
                            initialValue: item.get('name'),
                            rules: [
                              {
                                required: true,
                                message: 'Trường bắt buộc!',
                              },
                            ],
                          })(<Input />)}
                        </Form.Item>
                      ) : (
                        <strong>{item.get('name')}</strong>
                      )}
                    </div>
                    <div className={styles.fieldForm}>
                      <span className={styles.label}>Giá:</span>
                      {idUpdate === item.get('id') ? (
                        <Form.Item className={styles.fieldFormItem}>
                          {getFieldDecorator('price', {
                            initialValue: item.get('price'),
                            rules: [
                              {
                                required: true,
                                message: 'Trường bắt buộc!',
                              },
                            ],
                          })(
                            <InputNumber
                              min={1}
                              className={styles.inputNumber}
                            />
                          )}
                        </Form.Item>
                      ) : (
                        <strong>{handleFormatPrice(item.get('price'))}</strong>
                      )}
                      <span style={{ marginLeft: '5px' }}>VNĐ</span>
                    </div>
                  </Form>
                  {idUpdate === item.get('id') ? (
                    <div className={styles.bottomCard}>
                      <Button
                        style={{ marginRight: '10px' }}
                        color="primary"
                        size="sm"
                        type="primary"
                        onClick={handleOkUpdate}>
                        Lưu
                      </Button>
                      <Button
                        size="sm"
                        color="danger"
                        type="primary"
                        onClick={handleCancel}>
                        Hủy
                      </Button>
                    </div>
                  ) : null}
                </CardBody>
              </Card>
            </Col>
          ))}
      </Row>
      {!idUpdate && visible && (
        <Modal
          title="THÊM DỊCH VỤ"
          visible={visible}
          footer={[
            <Button color="primary" type="primary" onClick={handleOkCreate}>
              Lưu
            </Button>,
          ]}
          onCancel={handleCancel}>
          <Form
            id="frmOtherServicesModal"
            className="input-floating"
            layout="inline">
            <Form.Item label="Tên dịch vụ">
              {getFieldDecorator('nameCreate', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Giá">
              {getFieldDecorator('priceCreate', {
                rules: [
                  {
                    required: true,
                    message: 'Trường bắt buộc!',
                  },
                ],
              })(<InputNumber min={1} className={styles.inputNumber} />)}
            </Form.Item>
          </Form>
        </Modal>
      )}
    </div>
  )
}

OtherServices.propTypes = {
  form: PropTypes.any,
  items: PropTypes.object,
  getItems: PropTypes.func,
  createItem: PropTypes.func,
  deleteItem: PropTypes.func,
  editItem: PropTypes.func,
  showLoading: PropTypes.bool.isRequired,
}

const mapStateToProps = state => ({
  items: OtherServicesManagementSelectors.getItems(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

const mapDispatchToProps = dispatch => ({
  getItems: () => dispatch(OtherServicesManagementActions.getItemsRequest()),
  clearItems: () => dispatch(OtherServicesManagementActions.clearItems()),
  createItem: values =>
    dispatch(OtherServicesManagementActions.createItemRequest(values)),
  editItem: values =>
    dispatch(OtherServicesManagementActions.editItemRequest(values)),
  deleteItem: id =>
    dispatch(OtherServicesManagementActions.deleteItemRequest(id)),
})
const OtherServicesPage = Form.create()(OtherServices)
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(OtherServicesPage)
