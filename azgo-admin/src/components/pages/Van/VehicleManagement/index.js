import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Button } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import 'Stores/Van/VehicleManagement/Reducers'
import 'Stores/Van/VehicleManagement/Sagas'
import VehicleManagementContainer from 'containers/Van/VehicleManagement'
import FormSearch from 'components/molecules/FormSearch'
import PropTypes from 'prop-types'
import { VehicleManagementSelectors } from 'Stores/Van/VehicleManagement/Selectors'
import { VehicleManagementActions } from 'Stores/Van/VehicleManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'

const VehicleManagement = ({ filter, setFilter }) => {
  const titlePage = 'Danh sách xe Van/Bagac'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Link to="/van/vehicle/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <VehicleManagementContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
      />
    </div>
  )
}

VehicleManagement.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  filter: VehicleManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(VehicleManagementActions.setFilter(filter)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(VehicleManagement)
