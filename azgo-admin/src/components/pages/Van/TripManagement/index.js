import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import TripsManagementContainer from 'containers/Van/TripManagement'
import FormSearch from 'components/molecules/FormSearch'

const TripsManagement = props => {
  const titlePage = 'Danh sách chuyến xe'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }

  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
      </div>
      <TripsManagementContainer search={search} />
    </div>
  )
}

TripsManagement.propTypes = {}

export default TripsManagement
