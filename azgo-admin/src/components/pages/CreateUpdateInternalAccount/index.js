import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import InternalAccount from 'components/organisms/FormInternalAccount'
import { Link, useParams } from 'react-router-dom'
import { InternalManagementSelectors } from 'Stores/InternalManagement/Selectors'
import { InternalManagementActions } from 'Stores/InternalManagement/Actions'
import PropTypes from 'prop-types'
import { Form, Switch } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Card, CardBody, CardHeader } from 'reactstrap'

const CreateUpdateInternalAccountContainer = ({ getItem, item }) => {
  let nameAdmin = item.getIn(['adminProfile', 'fullName'])
    ? item.getIn(['adminProfile', 'fullName'])
    : ''
  let { id } = useParams()
  const [changeBlockUser, setChangeBlockUser] = useState(false)
  const onChange = checked => {
    setChangeBlockUser(true)
  }
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={
              id ? 'Chỉnh sửa tài khoản nội bộ' : 'Thêm tài khoản nội bộ mới'
            }
            name={
              id ? (
                <Link to={`/admin/internal-management/detail/${id}`}>{nameAdmin}</Link>
              ) : null
            }
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
          <div className="card-header-actions">
            {item.size > 0 && item.get('status') === 1 ? (
              <Switch onChange={onChange} defaultChecked />
            ) : null}
            {item.size > 0 && item.get('status') === 0 ? (
              <Switch onChange={onChange} defaultChecked={false} />
            ) : null}
          </div>
        </CardHeader>
        <CardBody>
          <InternalAccount
            id={id || null}
            item={item}
            getItem={getItem}
            changeBlockUser={changeBlockUser}
          />
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: InternalManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(InternalManagementActions.getItemDetailRequest(id)),
})

CreateUpdateInternalAccountContainer.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}
const CreateUpdateInternalAccount = Form.create()(
  CreateUpdateInternalAccountContainer
)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateInternalAccount)
