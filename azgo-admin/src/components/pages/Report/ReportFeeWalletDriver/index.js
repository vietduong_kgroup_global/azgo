import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/' 
import { Link, withRouter } from 'react-router-dom'
import ReportWalletDriver from 'containers/Report/ReportWalletDriver'
import 'Stores/InternalManagement/Reducers'
import 'Stores/InternalManagement/Sagas' 
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import { ReportSelectors } from 'Stores/Report/Selectors'
// import ReactExport from 'react-data-export';
import { changeFormatDate } from 'Utils/helper'
// const ExcelFile = ReactExport.ExcelFile;
// const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
 
const InternalManagement = ({ filter }) => {
  let filterFrom = filter && filter.toJS() && filter && filter.toJS().from
  let filterTo = filter && filter.toJS() && filter && filter.toJS().to
  let from = filterFrom ? changeFormatDate(filterFrom.split(" ")[0]) : ""
  let to = filterTo ? changeFormatDate(filterTo.split(" ")[0]) : ""
  const titlePage = (from && to) ? `BẢNG TỔNG HỢP SỐ DƯ VÍ TÀI XẾ TỪ NGÀY ${from} ĐẾN NGÀY ${to}` :  `BẢNG TỔNG HỢP SỐ DƯ TÀI XẾ`
  // const [search, setSearch] = useState()
  // const handleSearch = value => {
  //   setSearch(value)
  // }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} /> 
      {/* <ExcelFile element={<button>Export Excel</button>}>
          <ExcelSheet dataSet={multiDataSet} name="Organization"/>
      </ExcelFile> */}
      <ReportWalletDriver  
      />
    </div>
  )
}

InternalManagement.propTypes = {
  filter: PropTypes.object.isRequired, 
}

const mapStateToProps = state => ({
  filter: ReportSelectors.getFilterDriverWallet(state), 
})

const mapDispatchToProps = dispatch => ({ 
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(InternalManagement)

 