import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/' 
import { Link, withRouter } from 'react-router-dom'
import ReportRevenueOrder from 'containers/Report/ReportRevenueOrder'
import 'Stores/InternalManagement/Reducers'
import 'Stores/InternalManagement/Sagas' 
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import { ReportSelectors } from 'Stores/Report/Selectors'
// import ReactExport from 'react-data-export';
import { changeFormatDate, changeFormatObjectDate } from 'Utils/helper'
// const ExcelFile = ReactExport.ExcelFile;
// const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
 
const InternalManagement = ({ filter }) => {
  let filterFrom = filter && filter.toJS() && filter && filter.toJS().from
  let filterTo = filter && filter.toJS() && filter && filter.toJS().to 
  let from = filterFrom ? changeFormatDate(filterFrom.split(" ")[0]) : ""
  let to = filterTo ? changeFormatDate(filterTo.split(" ")[0]) : ""
  const titlePage = (from && to) ? `BÁO CÁO CHI TIẾT CHUYẾN XE VÀ TIỀN THU CHUYẾN XE TỪ NGÀY ${from} ĐẾN NGÀY ${to}` :  `BÁO CÁO CHI TIẾT CHUYẾN XE VÀ TIỀN THU CHUYẾN XE`

  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} /> 
      {/* <ExcelFile element={<button>Export Excel</button>}>
          <ExcelSheet dataSet={multiDataSet} name="Organization"/>
      </ExcelFile> */}
      <ReportRevenueOrder  
      />
    </div>
  )
}

InternalManagement.propTypes = {
  filter: PropTypes.object.isRequired, 
}

const mapStateToProps = state => ({
    filter: ReportSelectors.getFilterRevenueOrder(state), 
})

const mapDispatchToProps = dispatch => ({ 
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(InternalManagement)

 