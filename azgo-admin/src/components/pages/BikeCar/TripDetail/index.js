import React, { useEffect, useState } from 'react'
import {
  Card,
  CardBody,
  CardHeader,
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  Button,
} from 'reactstrap'
import styles from './style.module.css'
import TitlePage from 'components/atoms/TitlePage/'
import 'Stores/BikeCar/TripManagement/Reducers'
import 'Stores/BikeCar/TripManagement/Sagas'
import { useParams, withRouter } from 'react-router-dom'
import { TripManagementSelectors } from 'Stores/BikeCar/TripManagement/Selectors'
import { TripManagementActions } from 'Stores/BikeCar/TripManagement/Actions'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { formatPrice, formatPhone } from 'Utils/helper'
import moment from 'moment'
import * as configsEnum from 'Utils/enum'

const TripDetail = ({ getItem, item }) => {
  let { id } = useParams()
  const [status, setStatus] = useState({})
  const [statusPayment, setStatusPayment] = useState({})
  const [time, setTime] = useState('')
  const handleFormatPrice = data => {
    if (typeof data === 'string') return data
    return formatPrice(data)
  }
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  useEffect(() => {
    const result = configsEnum.MANAGEMENT_STATUS_TRIP_ARR.find(itemTrip => {
      return itemTrip.key === item.get('status')
    })
    const resultPayment = configsEnum.PAYMENT_STATUS_ARR.find(itemPayment => {
      return itemPayment.key === item.get('paymentStatus')
    })
    setStatus(result)
    setStatusPayment(resultPayment)
    let newTime = new Date(item.get('createdAt'))
    let parseTime = moment(newTime).format('HH:mm')
    setTime(parseTime)
  }, [item])
  const titlePage = 'Thông tin chuyến xe'
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <div style={{ width: '100%' }}>
          <Row style={{ marginBottom: '20px' }}>
            <Col sm="12" xl="6">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Thông tin chuyến xe</strong>
                </CardHeader>
                <CardBody>
                  <ListGroup>
                    <ListGroupItem className="justify-content-between">
                      Mã chuyến
                      <div className="float-right">
                        <strong>{item && item.get('id')}</strong>
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Điểm đi
                      <div className="float-right">
                        {item &&
                          item.getIn(['routeInfo', 'startPosition', 'address'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Điểm đến
                      <div className="float-right">
                        {item &&
                          item.getIn(['routeInfo', 'endPosition', 'address'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Ngày
                      <div className="float-right">
                        {item &&
                          moment(new Date(item.get('createdAt'))).format(
                            'DD-MM-YYYY'
                          )}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Giờ
                      <div className="float-right">{time}</div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Trạng thái
                      <div className="float-right">
                        <Button color={status && status.color} size="sm">
                          {status && status.label}
                        </Button>
                      </div>
                    </ListGroupItem>
                  </ListGroup>
                </CardBody>
              </Card>
            </Col>
            <Col sm="12" xl="6">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Thông tin tài xế</strong>
                </CardHeader>
                <CardBody>
                  <ListGroup>
                    <ListGroupItem className="justify-content-between">
                      Họ và tên
                      <div className="float-right">
                        {item && item.getIn(['driverProfile', 'fullName'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Xe
                      <div className="float-right">
                        {item &&
                          item.getIn([
                            'driverProfile',
                            'users',
                            'vehicles',
                            '0',
                            'licensePlates',
                          ])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Loại xe
                      <div className="float-right">
                        {item &&
                          item.getIn([
                            'driverProfile',
                            'users',
                            'vehicles',
                            '0',
                            'vehicleType',
                            'name',
                          ])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Hãng xe
                      <div className="float-right">
                        {item &&
                          item.getIn([
                            'driverProfile',
                            'users',
                            'vehicles',
                            '0',
                            'vehicleBrand',
                            'name',
                          ])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Số điện thoại
                      <div className="float-right">
                        {formatPhone(
                          item.getIn(['driverProfile', 'users', 'countryCode']),
                          item.getIn(['driverProfile', 'users', 'phone'])
                        )}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Đánh giá
                      <div className="float-right">
                        {item && item.get('ratingDriver')}/5
                      </div>
                    </ListGroupItem>
                  </ListGroup>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col sm="12" xl="6">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Thông tin cuốc phí</strong>
                </CardHeader>
                <CardBody>
                  <ListGroup>
                    <ListGroupItem className="justify-content-between">
                      Cuốc phí
                      <div className="float-right">
                        {handleFormatPrice(
                          item && item.getIn(['routeInfo', 'price'])
                        )}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Mã khuyến mãi
                      <div className="float-right">0</div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Tiền khuyến mãi
                      <div className="float-right">0</div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Tổng tiền
                      <div className="float-right">
                        {handleFormatPrice(item && item.get('price'))}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Trạng thái thanh toán
                      <div className="float-right">
                        <Button
                          color={statusPayment && statusPayment.color}
                          size="sm">
                          {statusPayment && statusPayment.label}
                        </Button>
                      </div>
                    </ListGroupItem>
                  </ListGroup>
                </CardBody>
              </Card>
            </Col>
            <Col sm="12" xl="6">
              <Card className={styles.card}>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Thông tin khách hàng</strong>
                </CardHeader>
                <CardBody>
                  <ListGroup>
                    <ListGroupItem className="justify-content-between">
                      Họ tên
                      <div className="float-right">
                        {item && item.getIn(['customerProfile', 'fullName'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Số điện thoại
                      <div className="float-right">
                        {formatPhone(
                          item.getIn([
                            'customerProfile',
                            'users',
                            'countryCode',
                          ]),
                          item.getIn(['customerProfile', 'users', 'phone'])
                        )}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Email
                      <div className="float-right">
                        {item &&
                          item.getIn(['customerProfile', 'users', 'email'])}
                      </div>
                    </ListGroupItem>
                    <ListGroupItem className="justify-content-between">
                      Địa chỉ
                      <div className="float-right">
                        {item && item.getIn(['customerProfile', 'address'])}
                      </div>
                    </ListGroupItem>
                  </ListGroup>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  )
}

TripDetail.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  item: TripManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(TripManagementActions.getItemDetailRequest(id)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(TripDetail)
