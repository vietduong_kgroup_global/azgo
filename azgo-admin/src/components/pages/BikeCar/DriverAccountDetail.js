import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import ItemInfo from 'components/molecules/ItemInfo'
import { useParams } from 'react-router-dom'
import 'Stores/BikeCar/DriverManagement/Reducers'
import 'Stores/BikeCar/DriverManagement/Sagas'
import { DriverManagementSelectors } from 'Stores/BikeCar/DriverManagement/Selectors'
import { DriverManagementActions } from 'Stores/BikeCar/DriverManagement/Actions'
import PropTypes from 'prop-types'
import { Form, Icon, Modal } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Card, CardBody, CardHeader, Col, Row } from 'reactstrap'
import * as configsEnum from 'Utils/enum'
import { formatPhone } from 'Utils/helper'
import { Config } from 'Config'
import moment from 'moment'

const DriverAccountDetailContainer = ({ getItem, item, history }) => {
  const [gender, setGender] = useState({})
  const [previewVisible, setPreviewVisible] = useState(false)
  const [imageUrlPopup, setImageUrlPopup] = useState('')
  const [imageAvatar, setImageAvatar] = useState('')
  const [imageFrontCmnd, setImageFrontCmnd] = useState('')
  const [imageBackCmnd, setImageBackCmnd] = useState('')
  const [frontDrivingLicense, setFrontDrivingLicense] = useState('')
  const [backDrivingLicense, setBackDrivingLicense] = useState('')
  const [phone, setPhone] = useState()
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  const handleOpenUpdate = () => {
    history.push('/admin/driver/bike-car/edit/' + item.get('userId'))
  }
  const handlePreviewImg = value => {
    switch (value) {
      case 'imageAvatar': {
        setImageUrlPopup(imageAvatar)
        break
      }
      case 'imageFrontCmnd': {
        setImageUrlPopup(imageFrontCmnd)
        break
      }
      case 'frontDrivingLicense': {
        setImageUrlPopup(frontDrivingLicense)
        break
      }
      case 'backDrivingLicense': {
        setImageUrlPopup(backDrivingLicense)
        break
      }
      default: {
        setImageUrlPopup(imageBackCmnd)
        break
      }
    }
    setPreviewVisible(true)
  }
  useEffect(() => {
    const result = configsEnum.MANAGEMENT_GENDER_ARR.find(itemGender => {
      return itemGender.key === item.getIn(['driverProfile', 'gender'])
    })
    setGender(result)
    if (item.getIn(['driverProfile', 'imageProfile', 'fileName'])) {
      setImageAvatar(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['driverProfile', 'imageProfile', 'filePath']) +
          '/' +
          item.getIn(['driverProfile', 'imageProfile', 'fileName'])
      )
    }
    if (item.getIn(['driverProfile', 'frontOfCmnd', 'fileName'])) {
      setImageFrontCmnd(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['driverProfile', 'frontOfCmnd', 'filePath']) +
          '/' +
          item.getIn(['driverProfile', 'frontOfCmnd', 'fileName'])
      )
    }
    if (item.getIn(['driverProfile', 'backOfCmnd', 'fileName'])) {
      setImageBackCmnd(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['driverProfile', 'backOfCmnd', 'filePath']) +
          '/' +
          item.getIn(['driverProfile', 'backOfCmnd', 'fileName'])
      )
    }
    if (item.getIn(['driverProfile', 'frontDrivingLicense', 'fileName'])) {
      setFrontDrivingLicense(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['driverProfile', 'frontDrivingLicense', 'filePath']) +
          '/' +
          item.getIn(['driverProfile', 'frontDrivingLicense', 'fileName'])
      )
    }
    if (item.getIn(['driverProfile', 'backDrivingLicense', 'fileName'])) {
      setBackDrivingLicense(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['driverProfile', 'backDrivingLicense', 'filePath']) +
          '/' +
          item.getIn(['driverProfile', 'backDrivingLicense', 'fileName'])
      )
    }
    if (item.size > 0) {
      setPhone(formatPhone(item.get('countryCode'), item.get('phone')))
    }
  }, [item])
  return (
    <div className="animated fadeIn">
      <Card className="card-accent-primary">
        <CardHeader>
          <TitlePage
            data="Chi tiết tài khoản tài xế bike/car"
            name={item.getIn(['driverProfile', 'fullName'])}
          />
          <i
            className="fa fa-pencil"
            onClick={() => handleOpenUpdate()}
            style={{ cursor: 'pointer' }}
          />
        </CardHeader>
        <CardBody>
          <div className="detail-user">
            <div className="detail-user-image">
              {item.getIn(['driverProfile', 'imageProfile', 'fileName']) ? (
                <div className="detail-user-image-inner">
                  <img
                    src={imageAvatar}
                    alt="avatar"
                    onClick={() => handlePreviewImg('imageAvatar')}
                  />
                </div>
              ) : (
                <div className="bg-avatar" />
              )}
            </div>
            <div className="detail-user-info">
              <ItemInfo label="Email:" content={item.get('email')} />
              <ItemInfo
                label="Họ:"
                content={item.getIn(['driverProfile', 'lastName'])}
              />
              <ItemInfo
                label="Tên:"
                content={item.getIn(['driverProfile', 'firstName'])}
              />
              <ItemInfo label="Số điện thoại:" content={phone} />
              <ItemInfo
                label="Ngày sinh:"
                content={moment(
                  new Date(item.getIn(['driverProfile', 'birthday']))
                ).format('DD-MM-YYYY')}
              />
              <ItemInfo
                label="Giới tính:"
                content={gender ? gender.label : null}
              />
              <ItemInfo
                label="Số CMND:"
                content={item.getIn(['driverProfile', 'cmnd'])}
              />
              <ItemInfo
                label="Địa chỉ:"
                content={item.getIn(['driverProfile', 'address'])}
              />
              <Row style={{ marginTop: '20px', marginBottom: '10px' }}>
                <Col xs="12" sm="6" md="6">
                  <strong>Giấy CMND</strong>
                </Col>
              </Row>
              <Row>
                <Col xs="12" sm="6" md="6">
                  <div className="frame-img frame-img-big">
                    <div className="frame-img-inner">
                      {imageFrontCmnd ? (
                        <img
                          src={imageFrontCmnd}
                          alt="front CMND"
                          onClick={() => handlePreviewImg('imageFrontCmnd')}
                        />
                      ) : (
                        <Icon type="picture" />
                      )}
                    </div>
                  </div>
                </Col>
                <Col xs="12" sm="6" md="6">
                  <div className="frame-img frame-img-big">
                    <div className="frame-img-inner">
                      {imageBackCmnd ? (
                        <img
                          src={imageBackCmnd}
                          alt="back CMND"
                          onClick={() => handlePreviewImg('imageBackCmnd')}
                        />
                      ) : (
                        <Icon type="picture" />
                      )}
                    </div>
                  </div>
                </Col>
              </Row>
              <Row style={{ marginTop: '20px', marginBottom: '10px' }}>
                <Col xs="12" sm="6" md="6">
                  <strong>Giấy phép lái xe</strong>
                </Col>
              </Row>
              <Row>
                <Col xs="12" sm="6" md="6">
                  <div className="frame-img frame-img-big">
                    <div className="frame-img-inner">
                      {frontDrivingLicense ? (
                        <img
                          src={frontDrivingLicense}
                          alt="frontDrivingLicense"
                          onClick={() =>
                            handlePreviewImg('frontDrivingLicense')
                          }
                        />
                      ) : (
                        <Icon type="picture" />
                      )}
                    </div>
                  </div>
                </Col>
                <Col xs="12" sm="6" md="6">
                  <div className="frame-img frame-img-big">
                    <div className="frame-img-inner">
                      {backDrivingLicense ? (
                        <img
                          src={backDrivingLicense}
                          alt="backDrivingLicense"
                          onClick={() => handlePreviewImg('backDrivingLicense')}
                        />
                      ) : (
                        <Icon type="picture" />
                      )}
                    </div>
                  </div>
                </Col>
              </Row>
              <Modal
                visible={previewVisible}
                footer={null}
                onCancel={() => setPreviewVisible(false)}>
                <div className="modal-image">
                  <img alt="example" src={imageUrlPopup} />
                </div>
              </Modal>
            </div>
          </div>
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: DriverManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(DriverManagementActions.getItemDetailRequest(id)),
})

DriverAccountDetailContainer.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
  history: PropTypes.any,
}
const DriverAccountDetail = Form.create()(DriverAccountDetailContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(DriverAccountDetail)
