import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import TripManagementContainer from 'containers/BikeCar/TripManagement'
import FormSearch from 'components/molecules/FormSearch'

const TripManagementVan = props => {
  const titlePage = 'Danh sách chuyến xe'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }

  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
      </div>
      <TripManagementContainer search={search} />
    </div>
  )
}

TripManagementVan.propTypes = {}

export default TripManagementVan
