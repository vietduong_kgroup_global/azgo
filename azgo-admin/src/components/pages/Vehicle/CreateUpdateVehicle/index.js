import React, { useEffect } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormVehicle from 'components/organisms/Vehicle/FormVehicle'
import { useParams } from 'react-router-dom'
import { Card, CardBody, CardHeader } from 'reactstrap'
import PropTypes from 'prop-types'
import { VehicleManagementSelectors } from 'Stores/Vehicle/VehicleManagement/Selectors'
import { VehicleManagementActions } from 'Stores/Vehicle/VehicleManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'

const CreateUpdateVehicle = ({ getItem, item }) => {
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={
              id ? 'Chỉnh sửa thông tin xe' : 'Thêm xe mới'
            }
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
        </CardHeader>
        <CardBody>
          <FormVehicle id={id || null} item={item} />
        </CardBody>
      </Card>
    </div>
  )
}
CreateUpdateVehicle.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  item: VehicleManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(VehicleManagementActions.getItemDetailRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateVehicle)
