import React, { useEffect } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormVehicleType from 'components/organisms/Vehicle/FormVehicleType'
import { useParams } from 'react-router-dom'
import { Card, CardBody, CardHeader } from 'reactstrap'
import { VehicleTypeManagementSelectors } from 'Stores/Vehicle/VehicleTypeManagement/Selectors'
import { VehicleTypeManagementActions } from 'Stores/Vehicle/VehicleTypeManagement/Actions'
import PropTypes from 'prop-types'
import { Form } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'

const CreateUpdateVehicleType = ({ getItem, item }) => {
  let name = item.get('name') ? item.get('name') : ''
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={id ? 'Chỉnh sửa thông tin loại xe' : 'Thêm loại xe mới'}
            name={id ? name : null}
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
        </CardHeader>
        <CardBody>
          <FormVehicleType id={id || null} item={item} getItem={getItem} />
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: VehicleTypeManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id =>
    dispatch(VehicleTypeManagementActions.getItemDetailRequest(id)),
})

CreateUpdateVehicleType.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}
const CreateUpdateVehicleTypePage = Form.create()(CreateUpdateVehicleType)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateVehicleTypePage)
