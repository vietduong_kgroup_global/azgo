import React, { useEffect } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormVehicleManufacturer from 'components/organisms/Vehicle/FormVehicleManufacturer'
import { useParams } from 'react-router-dom'
import { Card, CardBody, CardHeader } from 'reactstrap'
import { BrandVehicleManagementSelectors } from 'Stores/BrandVehicleManagement/Selectors'
import { BrandVehicleManagementActions } from 'Stores/BrandVehicleManagement/Actions'
import PropTypes from 'prop-types'
import { Form } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'

const CreateUpdateVehicleManufacturer = ({ getItem, item }) => {
  let name = item.get('name') ? item.get('name') : ''
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={id ? 'Chỉnh sửa thông tin hãng xe' : 'Thêm hãng xe mới'}
            name={id ? name : null}
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
        </CardHeader>
        <CardBody>
          <FormVehicleManufacturer
            id={id || null}
            item={item}
            getItem={getItem}
          />
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: BrandVehicleManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id =>
    dispatch(BrandVehicleManagementActions.getItemDetailRequest(id)),
})

CreateUpdateVehicleManufacturer.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}
const CreateUpdateVehicleManufacturerPage = Form.create()(
  CreateUpdateVehicleManufacturer
)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateVehicleManufacturerPage)
