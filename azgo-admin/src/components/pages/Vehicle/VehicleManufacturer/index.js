import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Button } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import VehicleManufacturerContainer from 'containers/Vehicle/VehicleManufacturer'
import FormSearch from 'components/molecules/FormSearch'
import 'Stores/BrandVehicleManagement/Reducers'
import 'Stores/BrandVehicleManagement/Sagas'
import { BrandVehicleManagementSelectors } from 'Stores/BrandVehicleManagement/Selectors'
import { BrandVehicleManagementActions } from 'Stores/BrandVehicleManagement/Actions'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'

const VehicleManufacturer = ({ filter, setFilter }) => {
  const titlePage = 'Danh sách hãng xe'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Link to="/admin/vehicle/vehicle-manufacturer/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <VehicleManufacturerContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
      />
    </div>
  )
}

VehicleManufacturer.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  filter: BrandVehicleManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter =>
    dispatch(BrandVehicleManagementActions.setFilter(filter)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(VehicleManufacturer)
