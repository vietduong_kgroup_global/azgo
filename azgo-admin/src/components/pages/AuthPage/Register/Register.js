import React, { Component } from 'react'
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap'
import logo from 'assets/img/brand/logo.svg'
import { Link } from 'react-router-dom'

const Register = () => {
  const StyleLogo = {
    maxWidth: '150px',
    margin: '0 auto 20px auto',
  }
  const StyleTittle = {
    marginBottom: '30px',
    textAlign: 'center',
    textTransform: 'uppercase',
    fontSize: '18px',
    fontWeight: 'bold',
  }
  return (
    <div className="app flex-row align-items-center">
      <Container>
        <Row className="justify-content-center">
          <Col md="9" lg="7" xl="6">
            <Card className="mx-4">
              <CardBody className="p-4">
                <Form>
                  <div style={StyleLogo}>
                    <img src={logo} />
                  </div>
                  <div style={StyleTittle}>Đăng ký tài khoản mới!</div>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-user" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      type="text"
                      placeholder="Tên đăng nhập"
                      autoComplete="username"
                    />
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>@</InputGroupText>
                    </InputGroupAddon>
                    <Input
                      type="text"
                      placeholder="Email"
                      autoComplete="email"
                    />
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-lock" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      type="password"
                      placeholder="Mật khẩu"
                      autoComplete="new-password"
                    />
                  </InputGroup>
                  <InputGroup className="mb-4">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-lock" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      type="password"
                      placeholder="Xác nhận mật khẩu"
                      autoComplete="new-password"
                    />
                  </InputGroup>
                  <Button color="success" block>
                    Xác nhận
                  </Button>
                </Form>
              </CardBody>
              <CardFooter className="p-4">
                <Row style={{ textAlign: 'center', marginBottom: '20px' }}>
                  <Col xs="12" sm="6">
                    <Button
                      size="lg"
                      className="btn-facebook btn-brand mr-1 mb-1">
                      <i className="fa fa-facebook" />
                      <span>Facebook</span>
                    </Button>
                  </Col>
                  <Col xs="12" sm="6">
                    <Button
                      size="lg"
                      className="btn-google-plus btn-brand mr-1 mb-1">
                      <i className="fa fa-google-plus" />
                      <span>Google+</span>
                    </Button>
                  </Col>
                </Row>
                <p style={{ textAlign: 'right' }}>
                  Đăng nhập nếu đã có tài khoản
                  <Link to="/login" style={{ marginLeft: '5px' }}>
                    Đăng nhập!
                  </Link>
                </p>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default Register
