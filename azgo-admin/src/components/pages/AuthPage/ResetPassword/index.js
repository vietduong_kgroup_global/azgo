import React, { Component } from 'react'
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from 'reactstrap'
import logo from 'assets/img/brand/logo.svg'
import { Link } from 'react-router-dom'

const ResetPassword = () => {
  const StyleLogo = {
    maxWidth: '150px',
    margin: '0 auto 20px auto',
  }
  const StyleTittle = {
    marginBottom: '30px',
    textAlign: 'center',
    fontSize: '16px',
  }
  return (
    <div className="app flex-row align-items-center">
      <Container>
        <Row className="justify-content-center">
          <Col md="9" lg="7" xl="6">
            <Card className="mx-4">
              <CardBody className="p-4">
                <Form>
                  <div style={StyleLogo}>
                    <img src={logo} />
                  </div>
                  <div style={StyleTittle}>
                    Đặt lại mật khẩu của bạn nếu quên mật khẩu!
                  </div>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-user" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      type="email"
                      placeholder="Nhập email"
                      autoComplete="Email"
                    />
                  </InputGroup>
                  <Button color="success" block>
                    Xác nhận
                  </Button>
                </Form>
              </CardBody>
              <CardFooter className="p-4">
                <p style={{ textAlign: 'right' }}>
                  Quay lại trang
                  <Link to="/login" style={{ marginLeft: '5px' }}>
                    Đăng nhập!
                  </Link>
                </p>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default ResetPassword
