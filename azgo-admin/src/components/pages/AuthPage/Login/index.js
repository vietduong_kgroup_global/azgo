import React, { Component, useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { withRouter, Link } from 'react-router-dom'
import 'Stores/User/Reducers'
import 'Stores/User/Sagas'
import { UserActions } from 'Stores/User/Actions'
import { Form, Icon, Input, Button } from 'antd'
import logo from 'assets/img/brand/logo.svg'
import background from 'assets/img/pexels-photo-434450.jpeg'
import * as filePackage from '../../../../../package.json'
import { UserSelectors } from 'Stores/User/Selectors'

import { Card, CardBody, CardGroup, Col, Container, Row } from 'reactstrap'

const LoginForm = ({ form, login, history , apiVersion}) => {
  const { getFieldDecorator, getFieldsValue } = form
  const StyleBg = {
    backgroundImage: `url(${background})`,
    backgroundSize: 'cover',
  }
  const StyleLogo = {
    maxWidth: '150px',
    margin: '0 auto 20px auto',
  }
  const StyleForm = {
    maxWidth: '450px',
  }
  const handleSubmit = e => {
    e.preventDefault()
    form.validateFields((err, values) => {
      if (!err) {
        login({
          ...getFieldsValue(),
          device_type: 'other',
        })
      }
    })
  }
  // useEffect(() => {
  //   getAPIVersion() 
  // }, [])
  return (
    <div style={StyleBg} className="app flex-row align-items-center">
      <Container>
        <Row>
          <Col md="12">
            <CardGroup>
              <Card className="p-4" style={StyleForm}>
                <CardBody>
                  <div style={StyleLogo}>
                    <img src={logo} />
                  </div>
                  <Form onSubmit={handleSubmit} className="login-form">
                    <Form.Item>
                      {getFieldDecorator('username', {
                        rules: [
                          {
                            required: true,
                            type: 'email',
                            message: 'Vui lòng điền email của bạn!',
                          },
                        ],
                      })(
                        <Input
                          prefix={
                            <Icon
                              type="user"
                              style={{ color: 'rgba(0,0,0,.25)' }}
                            />
                          }
                          placeholder="Email"
                        />
                      )}
                    </Form.Item>
                    <Form.Item>
                      {getFieldDecorator('password', {
                        rules: [
                          {
                            required: true,
                            message: 'Vui lòng điền mật khẩu!',
                          },
                        ],
                      })(
                        <Input
                          prefix={
                            <Icon
                              type="lock"
                              style={{ color: 'rgba(0,0,0,.25)' }}
                            />
                          }
                          type="password"
                          placeholder="Mật khẩu"
                        />
                      )}
                    </Form.Item>
                    <Form.Item>
                      <Button
                        type="primary"
                        htmlType="submit"
                        className="login-form-button">
                        Đăng nhập
                      </Button> 
                    </Form.Item>
                    <div>Admin version {filePackage && filePackage.version}</div>
                    {/* <div>API version {apiVersion}</div> */}
                    {/*<Link to="/reset-password">Quên mật khẩu!</Link>*/}
                  </Form>
                </CardBody>
              </Card>
            </CardGroup>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

const mapStateToProps = state => ({
  apiVersion: UserSelectors.getAPIVersion(state),

})

const mapDispatchToProps = dispatch => ({
  // getAPIVersion: id => dispatch(UserActions.fetchApiVersionInfo()), 
  login: data => dispatch(UserActions.userLogin(data)),
})

LoginForm.propTypes = {
  form: PropTypes.any,
  login: PropTypes.func,
  history: PropTypes.object,
}

const withConnect = connect(mapStateToProps, mapDispatchToProps)

const Login = Form.create()(LoginForm)

export default compose(withConnect, withRouter)(Login)
