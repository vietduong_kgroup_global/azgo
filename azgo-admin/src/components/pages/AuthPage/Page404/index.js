import React, { Component, useEffect } from 'react'
import { Col, Container, Row } from 'reactstrap'
import { connect } from 'react-redux'
import { compose } from 'redux'

const Page404 = ({}) => {
  return (
    <>
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <div className="clearfix">
                <h1 className="float-left display-3 mr-4">404</h1>
                <h4 className="pt-3">Oops! You're lost.</h4>
                <p className="text-muted float-left">
                  The page you are looking for was not found.
                </p>
              </div>
              <div style={{ textAlign: 'center' }}>
                <a href="/">Go Home</a>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  )
}

Page404.propTypes = {}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect)(Page404)
