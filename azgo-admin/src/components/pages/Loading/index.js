import React from 'react'

const Loading = () => {
  return <div className="animated fadeIn pt-1 text-center">LOADING......</div>
}

Loading.propTypes = {}

export default Loading
