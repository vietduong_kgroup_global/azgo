import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Button } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import TableInternalManagementContainer from 'containers/InternalAccount/TableInternalManagement'
import 'Stores/InternalManagement/Reducers'
import 'Stores/InternalManagement/Sagas'
import FormSearch from 'components/molecules/FormSearch'
import { InternalManagementSelectors } from 'Stores/InternalManagement/Selectors'
import { InternalManagementActions } from 'Stores/InternalManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types'

const InternalManagement = ({ filter, setFilter }) => {
  const titlePage = 'Danh sách nội bộ'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Link to="/admin/internal-management/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <TableInternalManagementContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
      />
    </div>
  )
}

InternalManagement.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  filter: InternalManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(InternalManagementActions.setFilter(filter)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(InternalManagement)
