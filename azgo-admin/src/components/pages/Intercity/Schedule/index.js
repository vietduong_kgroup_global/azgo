import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Button } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import SchedulerContainer from 'containers/Intercity/Schedule'
import FormSearch from 'components/molecules/FormSearch'
import PropTypes from 'prop-types'
import 'Stores/Intercity/ScheduleManagement/Reducers'
import 'Stores/Intercity/ScheduleManagement/Sagas'
import { ScheduleManagementSelectors } from 'Stores/Intercity/ScheduleManagement/Selectors'
import { ScheduleManagementActions } from 'Stores/Intercity/ScheduleManagement/Actions'
import { UserSelectors } from 'Stores/User/Selectors'
import { connect } from 'react-redux'
import { compose } from 'redux'
import * as config from 'Utils/enum'

const Schedule = ({ filter, setFilter, itemUser }) => {
  const titlePage = 'Danh sách lịch chạy'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  useEffect(() => {
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER) {
      setFilter({
        ...filter,
        provider_id: itemUser.getIn(['providersInfo', 'id']),
      })
    }
  }, [itemUser])
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Link to="/bus/schedule/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <SchedulerContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
        managementPage
      />
    </div>
  )
}

Schedule.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
  itemUser: PropTypes.object,
}

const mapStateToProps = state => ({
  filter: ScheduleManagementSelectors.getFilter(state),
  itemUser: UserSelectors.getUserData(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(ScheduleManagementActions.setFilter(filter)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(Schedule)
