import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Button } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import SeatMapContainer from 'containers/Intercity/SeatMap'
import FormSearch from 'components/molecules/FormSearch'
import { connect } from 'react-redux'
import { compose } from 'redux'

const SeatMap = ({}) => {
  const titlePage = 'Danh sách mẫu ghế'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Link to="/bus/seat-map/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <SeatMapContainer search={search} />
    </div>
  )
}

SeatMap.propTypes = {}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(SeatMap)
