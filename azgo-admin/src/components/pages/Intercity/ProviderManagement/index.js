import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormSearch from 'components/molecules/FormSearch'
import ProviderManagementContainer from 'containers/Intercity/ProviderManagement'
import { Button } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import { ProviderManagementSelectors } from 'Stores/ProviderManagement/Selectors'
import { ProviderManagementActions } from 'Stores/ProviderManagement/Actions'
import 'Stores/ProviderManagement/Reducers'
import 'Stores/ProviderManagement/Sagas'

const ProviderManagement = ({ filter, setFilter }) => {
  const titlePage = 'Danh sách nhà xe'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Link to="/provider-management/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <ProviderManagementContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
      />
    </div>
  )
}

ProviderManagement.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  filter: ProviderManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(ProviderManagementActions.setFilter(filter)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(ProviderManagement)
