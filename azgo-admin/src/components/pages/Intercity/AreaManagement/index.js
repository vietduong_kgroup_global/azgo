import React, {useEffect, useState} from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Button } from 'reactstrap'
import AreaManagementContainer from 'containers/Intercity/AreaManagement'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import FormSearch from 'components/molecules/FormSearch'
import { AreaManagementSelectors } from 'Stores/Intercity/AreaManagement/Selectors'
import { AreaManagementActions } from 'Stores/Intercity/AreaManagement/Actions'
import { Link, withRouter } from 'react-router-dom'

const AreaManagement = ({ filter, setFilter }) => {
  const titlePage = 'Danh sách địa điểm'
  const [visible, setVisible] = useState(false)
  const showModal = () => {
    setVisible(true)
  }
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Link to="/admin/tour/areaManagement/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <AreaManagementContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
        visible={visible}
        setVisible={setVisible}
      />
    </div>
  )
}

AreaManagement.propTypes = {
  filter: PropTypes.object,
  setFilter: PropTypes.func,
}

const mapStateToProps = state => ({
  filter: AreaManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(AreaManagementActions.setFilter(filter)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(AreaManagement)
