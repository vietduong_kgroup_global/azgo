import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { withRouter } from 'react-router-dom'
import BudgetContainer from 'containers/Intercity/Budget'
import FormSearch from 'components/molecules/FormSearch'
import { connect } from 'react-redux'
import { compose } from 'redux'

const Budget = ({}) => {
  const titlePage = 'Quản lý chi phí tuyến'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
      </div>
      <BudgetContainer search={search} />
    </div>
  )
}

Budget.propTypes = {}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(Budget)
