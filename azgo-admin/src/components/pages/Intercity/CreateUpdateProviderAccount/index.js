import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormProviderAccount from 'components/organisms/FormProviderAccount'
import { Link, useParams } from 'react-router-dom'
import { ProviderManagementSelectors } from 'Stores/ProviderManagement/Selectors'
import { ProviderManagementActions } from 'Stores/ProviderManagement/Actions'
import PropTypes from 'prop-types'
import { Form, Switch } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { UserSelectors } from 'Stores/User/Selectors'
import { Card, CardBody, CardHeader } from 'reactstrap'
import * as config from 'Utils/enum'

const CreateUpdateProviderAccountContainer = ({
  getItem,
  item,
  itemUser,
  history,
}) => {
  let nameProvider = item.getIn(['providersInfo', 'providerName'])
    ? item.getIn(['providersInfo', 'providerName'])
    : ''
  let { id } = useParams()
  const [changeBlockUser, setChangeBlockUser] = useState(false)
  const onChange = checked => {
    setChangeBlockUser(true)
  }
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  useEffect(() => {
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER) {
      if (itemUser.get('user_id') !== id) {
        history.push(`/404`)
      }
    }
  }, [itemUser, id])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={
              id ? 'Chỉnh sửa tài khoản nhà xe' : 'Thêm tài khoản nhà xe mới'
            }
            name={
              id ? (
                <Link to={`/provider-management/detail/${id}`}>
                  {nameProvider}
                </Link>
              ) : null
            }
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
          <div className="card-header-actions">
            {item.size > 0 && item.get('status') === 1 ? (
              <Switch onChange={onChange} defaultChecked />
            ) : null}
            {item.size > 0 && item.get('status') === 0 ? (
              <Switch onChange={onChange} defaultChecked={false} />
            ) : null}
          </div>
        </CardHeader>
        <CardBody>
          <FormProviderAccount
            id={id || null}
            item={item}
            getItem={getItem}
            changeBlockUser={changeBlockUser}
          />
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: ProviderManagementSelectors.getItem(state),
  itemUser: UserSelectors.getUserData(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(ProviderManagementActions.getItemDetailRequest(id)),
})

CreateUpdateProviderAccountContainer.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
  itemUser: PropTypes.object,
  history: PropTypes.any,
}
const CreateUpdateProviderAccount = Form.create()(
  CreateUpdateProviderAccountContainer
)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateProviderAccount)
