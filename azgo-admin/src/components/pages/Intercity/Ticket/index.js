import React, { useEffect } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { useParams } from 'react-router-dom'
import FormTicketContainer from 'components/organisms/Intercity/FormTicket'
import 'Stores/Intercity/ScheduleManagement/Reducers'
import 'Stores/Intercity/ScheduleManagement/Sagas'
import { ScheduleManagementSelectors } from 'Stores/Intercity/ScheduleManagement/Selectors'
import { ScheduleManagementActions } from 'Stores/Intercity/ScheduleManagement/Actions'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
const Ticket = ({ getItem, item }) => {
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  const titlePage = 'Danh sách vé xe'
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <FormTicketContainer id={id || null} itemList={item} />
    </div>
  )
}

Ticket.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  item: ScheduleManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => {
    dispatch(ScheduleManagementActions.getItemDetailRequest(id))
  },
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(Ticket)
