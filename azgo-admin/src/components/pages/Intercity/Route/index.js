import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Button } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import RouterContainer from 'containers/Intercity/Route'
import FormSearch from 'components/molecules/FormSearch'
import { connect } from 'react-redux'
import { compose } from 'redux'

const Route = ({}) => {
  const titlePage = 'Danh sách tuyến'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Link to="/admin/bus/route/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <RouterContainer search={search} managementPage />
    </div>
  )
}

Route.propTypes = {}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(Route)
