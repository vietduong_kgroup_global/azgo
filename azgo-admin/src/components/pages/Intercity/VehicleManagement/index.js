import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import { Button } from 'reactstrap'
import { withRouter } from 'react-router-dom'
import 'Stores/Intercity/VehicleManagement/Reducers'
import 'Stores/Intercity/VehicleManagement/Sagas'
import VehicleManagementContainer from 'containers/Intercity/VehicleManagement'
import FormSearch from 'components/molecules/FormSearch'
import PropTypes from 'prop-types'
import { VehicleManagementSelectors } from 'Stores/Intercity/VehicleManagement/Selectors'
import { VehicleManagementActions } from 'Stores/Intercity/VehicleManagement/Actions'
import { UserSelectors } from 'Stores/User/Selectors'
import { connect } from 'react-redux'
import { compose } from 'redux'
import * as config from 'Utils/enum'

const VehicleManagement = ({ filter, setFilter, itemUser }) => {
  const titlePage = 'Danh sách xe'
  const [search, setSearch] = useState()
  const [visible, setVisible] = useState(false)
  const handleSearch = value => {
    setSearch(value)
  }
  const showModal = () => {
    setVisible(true)
  }
  useEffect(() => {
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER) {
      setFilter({
        ...filter,
        provider_id: itemUser.getIn(['providersInfo', 'id']),
      })
    }
  }, [itemUser])
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Button color="success" onClick={showModal}>
          <i className="fa fa-plus-square" />
          &nbsp;Thêm mới
        </Button>
      </div>
      <VehicleManagementContainer
        search={search}
        filter={filter}
        setFilter={setFilter}
        visible={visible}
        setVisible={setVisible}
        managementPage
      />
    </div>
  )
}

VehicleManagement.propTypes = {
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
  itemUser: PropTypes.object,
}

const mapStateToProps = state => ({
  filter: VehicleManagementSelectors.getFilter(state),
  itemUser: UserSelectors.getUserData(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(VehicleManagementActions.setFilter(filter)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(VehicleManagement)
