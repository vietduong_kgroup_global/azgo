


import React, { useEffect } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormArea from 'components/organisms/FormArea'
import { useParams } from 'react-router-dom'
import { Card, CardBody, CardHeader } from 'reactstrap'
import { connect } from 'react-redux'
import { compose } from 'redux'
import 'Stores/Intercity/AreaManagement/Reducers'
import 'Stores/Intercity/AreaManagement/Sagas'
import { AreaManagementSelectors } from 'Stores/Intercity/AreaManagement/Selectors'
import { AreaManagementActions } from 'Stores/Intercity/AreaManagement/Actions'
import PropTypes from 'prop-types'

const CreateUpdateArea = ({ getItem, item }) => {
  let nameRoute = item.toJS().name ? item.toJS().name : ''
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [id])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={id ? 'Chỉnh sửa khu vực' : 'Thêm khu vực'}
            name={id ? nameRoute : null}
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
        </CardHeader>
        <CardBody>
          <FormArea id={id || null} item={item} />
        </CardBody>
      </Card>
    </div>
  )
}

CreateUpdateArea.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  item: AreaManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(AreaManagementActions.getItemDetailRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateArea)
