import React, { useEffect } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormRoute from 'components/organisms/Intercity/FormRoute'
import { useParams } from 'react-router-dom'
import { Card, CardBody, CardHeader } from 'reactstrap'
import { connect } from 'react-redux'
import { compose } from 'redux'
import 'Stores/Intercity/RouteManagement/Reducers'
import 'Stores/Intercity/RouteManagement/Sagas'
import { RouteManagementSelectors } from 'Stores/Intercity/RouteManagement/Selectors'
import { RouteManagementActions } from 'Stores/Intercity/RouteManagement/Actions'
import PropTypes from 'prop-types'

const CreateUpdateRoute = ({ getItem, item }) => {
  let nameRoute = item.toJS().name ? item.toJS().name : ''
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [id])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={id ? 'Chỉnh sửa tuyến xe' : 'Thêm tuyến mới'}
            name={id ? nameRoute : null}
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
        </CardHeader>
        <CardBody>
          <FormRoute id={id || null} item={item} />
        </CardBody>
      </Card>
    </div>
  )
}

CreateUpdateRoute.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  item: RouteManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(RouteManagementActions.getItemDetailRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateRoute)
