import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import ItemInfo from 'components/molecules/ItemInfo'
import { Link, useParams } from 'react-router-dom'
import RouteBusList from 'components/organisms/Intercity/RouteBusList'
import DriverBusList from 'components/organisms/Intercity/DriverBusList'
import VehicleTypeBusList from 'components/organisms/Intercity/VehicleTypeBusList'
import VehicleBusList from 'components/organisms/Intercity/VehicleBusList'
import SeatMapBusList from 'components/organisms/Intercity/SeatMapBusList'
import ScheduleBusList from 'components/organisms/Intercity/ScheduleBusList'
import 'Stores/ProviderManagement/Reducers'
import 'Stores/ProviderManagement/Sagas'
import { ProviderManagementSelectors } from 'Stores/ProviderManagement/Selectors'
import { ProviderManagementActions } from 'Stores/ProviderManagement/Actions'
import { UserSelectors } from 'Stores/User/Selectors'
import PropTypes from 'prop-types'
import { Form, Modal, Table } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Card, CardBody, CardHeader, Col, Row } from 'reactstrap'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { Config } from 'Config'
import * as config from 'Utils/enum'
import { formatPhone } from 'Utils/helper'

const ProviderDetailContainer = ({
  getItem,
  item,
  history,
  showLoading,
  itemUser,
}) => {
  let { id } = useParams()
  const [idProvider, setIdProvider] = useState(null)
  const [previewVisible, setPreviewVisible] = useState(false)
  const [imageAvatar, setImageAvatar] = useState('')
  const [phone, setPhone] = useState()
  const handlePreviewImg = () => {
    setPreviewVisible(true)
  }
  const handleOpenUpdate = () => {
    history.push('/provider-management/edit/' + item.get('userId'))
  }
  useEffect(() => {
    if (id) {
      getItem(id)
    }
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER) {
      getItem(itemUser.get('user_id'))
    }
    return () => {
      setIdProvider(null)
    }
  }, [itemUser])
  useEffect(() => {
    if (item.getIn(['providersInfo', 'id'])) {
      setIdProvider(item.getIn(['providersInfo', 'id']))
    }
  }, [item])
  useEffect(() => {
    if (item.getIn(['providersInfo', 'imageProfile', 'fileName'])) {
      setImageAvatar(
        Config.DEV_URL.IMAGE +
          '/' +
          item.getIn(['providersInfo', 'imageProfile', 'filePath']) +
          '/' +
          item.getIn(['providersInfo', 'imageProfile', 'fileName'])
      )
    }
    if (item.size > 0) {
      setPhone(formatPhone(item.get('countryCode'), item.get('phone')))
    }
  }, [item])
  return (
    <div className="animated fadeIn">
      <Card className="card-accent-primary">
        <CardHeader>
          <TitlePage
            data="Thông tin nhà xe"
            name={item.getIn(['providersInfo', 'providerName'])}
          />
          <i
            className="fa fa-pencil"
            onClick={() => handleOpenUpdate()}
            style={{ cursor: 'pointer' }}
          />
        </CardHeader>
        <CardBody>
          <Row>
            <Col sm="12" xl="6">
              <div className="detail-user" style={{ marginBottom: '20px' }}>
                <div className="detail-user-image">
                  {imageAvatar ? (
                    <div className="detail-user-image-inner">
                      <img
                        src={imageAvatar}
                        alt="avatar"
                        onClick={() => handlePreviewImg()}
                      />
                    </div>
                  ) : (
                    <div className="bg-avatar" />
                  )}
                </div>
                <div className="detail-user-info">
                  <ItemInfo
                    label="Tên nhà xe:"
                    content={item.getIn(['providersInfo', 'providerName'])}
                  />
                  <ItemInfo label="Email:" content={item.get('email')} />
                  <ItemInfo label="Số điện thoại:" content={phone} />
                  <ItemInfo
                    label="Địa chỉ:"
                    content={item.getIn(['providersInfo', 'address'])}
                  />
                  <ItemInfo
                    label="Người đại diện:"
                    content={item.getIn(['providersInfo', 'representative'])}
                  />
                  <ItemInfo
                    label="SĐT người đại diên:"
                    content={item.getIn([
                      'providersInfo',
                      'phoneRepresentative',
                    ])}
                  />
                </div>
              </div>
            </Col>
            <Col sm="12" xl="6">
              <RouteBusList idProvider={idProvider} />
            </Col>
          </Row>
          <Row>
            <Col sm="12" xl="6">
              <DriverBusList idProvider={idProvider} />
            </Col>
            <Col sm="12" xl="6">
              <VehicleBusList idProvider={idProvider} />
            </Col>
          </Row>
          <Row>
            <Col sm="12" xl="6">
              <VehicleTypeBusList idProvider={idProvider} />
            </Col>
            <Col sm="12" xl="6">
              <SeatMapBusList idProvider={idProvider} />
            </Col>
          </Row>
          <Row>
            <Col sm="12" xl="12">
              <ScheduleBusList itemProvider={idProvider} />
            </Col>
          </Row>
          <Modal
            visible={previewVisible}
            footer={null}
            onCancel={() => setPreviewVisible(false)}>
            <div className="modal-image">
              <img alt="example" src={imageAvatar} />
            </div>
          </Modal>
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: ProviderManagementSelectors.getItem(state),
  showLoading: LoadingSelectors.getLoadingList(state),
  itemUser: UserSelectors.getUserData(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(ProviderManagementActions.getItemDetailRequest(id)),
})

ProviderDetailContainer.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
  history: PropTypes.any,
  showLoading: PropTypes.bool,
  itemUser: PropTypes.object,
}
const ProviderDetail = Form.create()(ProviderDetailContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(ProviderDetail)
