import React, { useEffect } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormSeatMap from 'components/organisms/Intercity/FormSeatMap'
import { useParams } from 'react-router-dom'
import PropTypes from 'prop-types'
import { SeatMapManagementSelectors } from 'Stores/Intercity/SeatMapManagement/Selectors'
import { SeatMapManagementActions } from 'Stores/Intercity/SeatMapManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'

const CreateUpdateSeatMap = ({ getItem, item }) => {
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [id])
  return (
    <div className="animated fadeIn">
      <TitlePage
        data={id ? 'Chỉnh sửa thông tin sơ đồ ngồi' : 'Thêm sơ đồ ngồi mới'}
      />
      <FormSeatMap id={id || null} item={item} />
    </div>
  )
}

CreateUpdateSeatMap.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  item: SeatMapManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(SeatMapManagementActions.getItemDetailRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateSeatMap)
