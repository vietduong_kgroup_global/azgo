import React, { useEffect } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormSchedule from 'components/organisms/Intercity/FormSchedule'
import { useParams } from 'react-router-dom'
import { Card, CardBody, CardHeader } from 'reactstrap'
import 'Stores/Intercity/ScheduleManagement/Reducers'
import 'Stores/Intercity/ScheduleManagement/Sagas'
import { ScheduleManagementSelectors } from 'Stores/Intercity/ScheduleManagement/Selectors'
import { ScheduleManagementActions } from 'Stores/Intercity/ScheduleManagement/Actions'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'

const CreateUpdateSchedule = ({ getItem, item }) => {
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data={
              id ? 'Chỉnh sửa thông tin lịch chạy xe' : 'Thêm lịch chạy xe mới'
            }
          />
          {id ? <i className="fa fa-pencil" /> : <i className="fa fa-file-o" />}
        </CardHeader>
        <CardBody>
          <FormSchedule id={id || null} item={item} />
        </CardBody>
      </Card>
    </div>
  )
}

CreateUpdateSchedule.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  item: ScheduleManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => {
    dispatch(ScheduleManagementActions.getItemDetailRequest(id))
  },
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateSchedule)
