import React, { useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import DriverManagementContainer from 'containers/Intercity/DriverManagement'
import FormSearch from 'components/molecules/FormSearch'
import { Link, withRouter } from 'react-router-dom'
import { Button } from 'reactstrap'
import { connect } from 'react-redux'
import { compose } from 'redux'

const DriverManagement = ({}) => {
  const titlePage = 'Danh sách tài xế xe liên tỉnh'
  const [search, setSearch] = useState()
  const handleSearch = value => {
    setSearch(value)
  }
  return (
    <div className="animated fadeIn">
      <TitlePage data={titlePage} />
      <div className="header-main-content">
        <FormSearch handleSearch={handleSearch} />
        <Link to="/driver/bus/create">
          <Button color="success">
            <i className="fa fa-plus-square" />
            &nbsp;Thêm mới
          </Button>
        </Link>
      </div>
      <DriverManagementContainer search={search} managementPage />
    </div>
  )
}

DriverManagement.propTypes = {}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(DriverManagement)
