import React, { useEffect } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import FormBudget from 'components/organisms/Intercity/FormBudget'
import { useParams } from 'react-router-dom'
import { Card, CardBody, CardHeader } from 'reactstrap'
import 'Stores/Intercity/RouteManagement/Reducers'
import 'Stores/Intercity/RouteManagement/Sagas'
import { RouteManagementSelectors } from 'Stores/Intercity/RouteManagement/Selectors'
import { RouteManagementActions } from 'Stores/Intercity/RouteManagement/Actions'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'

const CreateUpdateBudget = ({ getItem, item }) => {
  let nameRoute = item.toJS().name ? item.toJS().name : ''
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [id])
  return (
    <div className="animated fadeIn">
      <Card className={id ? `card-accent-warning` : `card-accent-success`}>
        <CardHeader>
          <TitlePage
            data="Chỉnh sửa thông tin chi phí tuyến"
            name={id ? nameRoute : null}
          />
          <i className="fa fa-pencil" />
        </CardHeader>
        <CardBody>
          <FormBudget id={id || null} item={item} />
        </CardBody>
      </Card>
    </div>
  )
}

CreateUpdateBudget.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
}

const mapStateToProps = state => ({
  item: RouteManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(RouteManagementActions.getItemDetailRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(CreateUpdateBudget)
