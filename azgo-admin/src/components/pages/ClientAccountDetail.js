import React, { useEffect, useState } from 'react'
import TitlePage from 'components/atoms/TitlePage/'
import ItemInfo from 'components/molecules/ItemInfo'
import { useParams } from 'react-router-dom'
import 'Stores/ClientManagement/Reducers'
import 'Stores/ClientManagement/Sagas'
import { ClientManagementSelectors } from 'Stores/ClientManagement/Selectors'
import { ClientManagementActions } from 'Stores/ClientManagement/Actions'
import PropTypes from 'prop-types'
import { Form, Modal } from 'antd'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Card, CardBody, CardHeader } from 'reactstrap'
import * as configsEnum from 'Utils/enum'
import { Config } from 'Config'
import moment from 'moment'
import { formatPhone } from 'Utils/helper'

const ClientAccountDetailContainer = ({ getItem, item, history }) => {
  const [gender, setGender] = useState({})
  const [previewVisible, setPreviewVisible] = useState(false)
  const [imageAvatar, setImageAvatar] = useState('')
  const [phone, setPhone] = useState()
  let { id } = useParams()
  useEffect(() => {
    if (id) {
      getItem(id)
    }
  }, [])
  const handleOpenUpdate = () => {
    history.push('/admin/clients-management/edit/' + item.get('userId'))
  }
  const handlePreviewImg = () => {
    setPreviewVisible(true)
  }
  useEffect(() => {
    const result = configsEnum.MANAGEMENT_GENDER_ARR.find(itemGender => {
      return itemGender.key === item.getIn(['customerProfile', 'gender'])
    })
    if (item.getIn(['customerProfile', 'imageProfile', 'address'])) {
      setImageAvatar(item.getIn(['customerProfile', 'imageProfile', 'address']))
    }
    if (item.size > 0) {
      setPhone(formatPhone(item.get('countryCode'), item.get('phone')))
    }
    setGender(result)
  }, [item])
  return (
    <div className="animated fadeIn">
      <Card className="card-accent-primary">
        <CardHeader>
          <TitlePage
            data="Thông tin chi tiết tài khoản khách hàng"
            name={item.getIn(['customerProfile', 'fullName'])}
          />
          <i
            className="fa fa-pencil"
            onClick={() => handleOpenUpdate()}
            style={{ cursor: 'pointer' }}
          />
        </CardHeader>
        <CardBody>
          <div className="detail-user">
            <div className="detail-user-image">
              {imageAvatar ? (
                <div className="detail-user-image-inner">
                  <img
                    src={imageAvatar}
                    alt="avatar"
                    onClick={() => handlePreviewImg()}
                  />
                </div>
              ) : (
                <div className="bg-avatar" />
              )}
            </div>
            <div className="detail-user-info">
              <ItemInfo label="Email:" content={item.get('email')} />
              <ItemInfo
                label="Họ:"
                content={item.getIn(['customerProfile', 'lastName'])}
              />
              <ItemInfo
                label="Tên:"
                content={item.getIn(['customerProfile', 'firstName'])}
              />
              <ItemInfo label="Số điện thoại:" content={phone} />
              <ItemInfo
                label="Ngày sinh:"
                content={moment(
                  new Date(item.getIn(['customerProfile', 'birthday']))
                ).format('DD-MM-YYYY')}
              />
              <ItemInfo
                label="Giới tính:"
                content={gender ? gender.label : null}
              />
              <ItemInfo
                label="Số CMND:"
                content={item.getIn(['customerProfile', 'cmnd'])}
              />
              <ItemInfo
                label="Địa chỉ:"
                content={item.getIn(['customerProfile', 'address'])}
              />
            </div>
          </div>
          <Modal
            visible={previewVisible}
            footer={null}
            onCancel={() => setPreviewVisible(false)}>
            <div className="modal-image">
              <img alt="example" src={imageAvatar} />
            </div>
          </Modal>
        </CardBody>
      </Card>
    </div>
  )
}

const mapStateToProps = state => ({
  item: ClientManagementSelectors.getItem(state),
})

const mapDispatchToProps = dispatch => ({
  getItem: id => dispatch(ClientManagementActions.getItemDetailRequest(id)),
})

ClientAccountDetailContainer.propTypes = {
  getItem: PropTypes.func,
  item: PropTypes.object,
  history: PropTypes.any,
}
const ClientAccountDetail = Form.create()(ClientAccountDetailContainer)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(ClientAccountDetail)
