import React, { Component, useEffect, useState } from 'react'

import 'Stores/ServiceChargeManagement/Reducers'
import 'Stores/ServiceChargeManagement/Sagas'
import { ServiceChargeManagementSelectors } from 'Stores/ServiceChargeManagement/Selectors'
import { ServiceChargeManagementActions } from 'Stores/ServiceChargeManagement/Actions'
import { Form, Input, InputNumber, Modal, Table } from 'antd'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { formatPrice } from 'Utils/helper'
import TitlePage from 'components/atoms/TitlePage'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { Button } from 'reactstrap'

const ServiceChargeForm = ({
  getItems,
  items,
  form,
  editItem,
  showPopUp,
  showLoading,
}) => {
  const {
    getFieldDecorator,
    getFieldsValue,
    resetFields,
    setFieldsValue,
  } = form
  const [visible, setVisible] = useState(false)
  const [id, setId] = useState(null)
  const [vehicleGroupId, setVehicleGroupId] = useState(null)
  const [rateOfCharge, setRateOfCharge] = useState(null)
  const [name, setName] = useState('')
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  }
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  }
  const columns = [
    {
      title: 'Id',
      dataIndex: 'vehicleGroupEntity.id',
    },
    {
      title: 'Loại xe',
      dataIndex: 'vehicleGroupEntity.name',
    },
    {
      title: 'Doanh thu App AZGO(tùy chỉnh)',
      dataIndex: 'rateOfCharge',
      render: value => {
        return <div>{value}%</div>
      },
    },
    {
      title: 'Chức năng',
      width: 140,
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Button
            color="light"
            size="sm"
            className="btn-sm-ct"
            onClick={() => showModal(value)}>
            <i className="fa fa-pencil" />
            &nbsp;Sửa
          </Button>
        </>
      ),
    },
  ]
  const showModal = value => {
    setVisible(true)
    setId(value.id)
    setVehicleGroupId(value.vehicleGroupId)
    setRateOfCharge(value.rateOfCharge)
    setName(value.vehicleGroupEntity.name)
    setFieldsValue({
      rateOfCharge: value.rateOfCharge,
    })
  }
  const handleSubmit = e => {
    // setVisible(false)
    form.validateFields(err => {
      if (err) {
        return false
      } else {
        editItem({
          ...getFieldsValue(),
          vehicleGroupId: vehicleGroupId,
          id: id,
        })
      }
    })
  }

  const handleCancel = e => {
    setVisible(false)
    setId(null)
    setVehicleGroupId(null)
    setRateOfCharge(null)
    setName('')
    resetFields()
  }
  useEffect(() => {
    if (showPopUp) {
      handleCancel()
    }
  }, [showPopUp])
  useEffect(() => {
    getItems()
    return () => {}
  }, [])
  return (
    <div className="animated fadeIn">
      <TitlePage data="Cài đặt phí dịch vụ" />
      <Table
        columns={columns}
        dataSource={items ? items.toJS() : null}
        pagination={false}
        rowKey="id"
        loading={showLoading}
      />
      <Modal
        title="Chỉnh sửa phí dịch vụ"
        visible={visible}
        footer={null}
        onCancel={handleCancel}>
        <Form
          id="frmServiceCharge"
          className="input-floating form-ant-custom"
          {...formItemLayout}>
          <Form.Item label="Tên dịch vụ">
            <div>
              <strong>{name}</strong>
            </div>
          </Form.Item>
          <Form.Item label="Phí dịch vụ">
            {getFieldDecorator('rateOfCharge', {
              initialValue: rateOfCharge,
              rules: [
                {
                  required: true,
                  message: 'Trường bắt buộc!',
                },
              ],
            })(<InputNumber min={0} />)}
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            <Button type="primary" onClick={handleSubmit}>
              Xác nhận
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  )
}

const mapStateToProps = state => ({
  items: ServiceChargeManagementSelectors.getItems(state),
  showLoading: LoadingSelectors.getLoadingList(state),
  showPopUp: LoadingSelectors.getShowPopUp(state),
})

const mapDispatchToProps = dispatch => ({
  getItems: () => dispatch(ServiceChargeManagementActions.getItemsRequest()),
  editItem: values =>
    dispatch(ServiceChargeManagementActions.editItemRequest(values)),
})

ServiceChargeForm.propTypes = {
  getItems: PropTypes.func,
  items: PropTypes.object,
  form: PropTypes.any,
  editItem: PropTypes.func,
  showPopUp: PropTypes.bool,
  showLoading: PropTypes.bool,
}

const ServiceCharge = Form.create()(ServiceChargeForm)

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect)(ServiceCharge)
