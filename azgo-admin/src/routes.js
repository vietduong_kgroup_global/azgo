import VehicleTypeManagement from 'components/pages/Intercity/VehicleTypeManagement'
import React from 'react'
import * as config from 'Utils/enum'

const Dashboard = React.lazy(() =>
  import('./components/pages/Dashboard/Dashboard')
)
const DashboardProvider = React.lazy(() =>
  import('./components/pages/Dashboard/DashboardProvider')
)
const InternalManagement = React.lazy(() =>
  import('./components/pages/InternalManagement')
)
const UserManagement = React.lazy(() =>
  import('./components/pages/UserManagement')
) 
const CreateUpdateInternalAccount = React.lazy(() =>
  import('./components/pages/CreateUpdateInternalAccount')
)
const InternalAccountDetail = React.lazy(() =>
  import('./components/pages/InternalAccountDetail.js')
)
const ClientsManagement = React.lazy(() =>
  import('./components/pages/ClientsManagement')
)
const ClientAccountDetail = React.lazy(() =>
  import('./components/pages/ClientAccountDetail.js')
)
const CreateUpdateClientAccount = React.lazy(() =>
  import('./components/pages/CreateUpdateClientAccount')
)
const ProviderManagement = React.lazy(() =>
  import('./components/pages/Intercity/ProviderManagement')
)
const CreateUpdateProviderAccount = React.lazy(() =>
  import('./components/pages/Intercity/CreateUpdateProviderAccount')
)
const ProviderDetail = React.lazy(() =>
  import('./components/pages/Intercity/ProviderDetail.js')
) 
const WalletManagement = React.lazy(() =>
  import('./components/pages/Driver/WalletManagement')
)
const WalletDetail = React.lazy(() =>
  import('./components/pages/Driver/WalletDetail')
)
const DriverManagement = React.lazy(() =>
  import('./components/pages/Driver/DriverManagement')
)
const CreateUpdateDriver = React.lazy(() =>
  import('./components/pages/Driver/CreateUpdateDriver')
)
const DriverDetail = React.lazy(() =>
  import('./components/pages/Driver/DriverAccountDetail')
)
const DriverBusManagement = React.lazy(() =>
  import('./components/pages/Intercity/DriverManagement')
)
const CreateUpdateDriverAccount = React.lazy(() =>
  import('./components/pages/Intercity/CreateUpdateDriverAccount')
)
const DriverAccountDetail = React.lazy(() =>
  import('./components/pages/Intercity/DriverAccountDetail')
)
// Vehicle General
const Vehicle = React.lazy(() =>
  import('./components/pages/Vehicle/VehicleManagement')
)
const CreateUpdateVehicle = React.lazy(() =>
  import('./components/pages/Vehicle/CreateUpdateVehicle')
)
const VehicleType = React.lazy(() =>
  import('./components/pages/Vehicle/VehicleTypeManagement')
)
const CreateUpdateVehicleType = React.lazy(() =>
  import('./components/pages/Vehicle/CreateUpdateVehicleType')
)
const VehicleManufacturer = React.lazy(() =>
  import('./components/pages/Vehicle/VehicleManufacturer')
)
const CreateUpdateVehicleManufacturer = React.lazy(() =>
  import('./components/pages/Vehicle/CreateUpdateVehicleManufacturer')
)
// bus
const Budget = React.lazy(() => import('./components/pages/Intercity/Budget'))
const CreateUpdateBudget = React.lazy(() =>
  import('./components/pages/Intercity/CreateUpdateBudget')
)
const VehicleBus = React.lazy(() =>
  import('./components/pages/Intercity/VehicleManagement')
)
const VehicleTypeBus = React.lazy(() =>
  import('./components/pages/Intercity/VehicleTypeManagement')
)
const Route = React.lazy(() => import('./components/pages/Intercity/Route'))
const CreateUpdateRoute = React.lazy(() =>
  import('./components/pages/Intercity/CreateUpdateRoute')
)
const Schedule = React.lazy(() =>
  import('./components/pages/Intercity/Schedule')
)
const CreateUpdateSchedule = React.lazy(() =>
  import('./components/pages/Intercity/CreateUpdateSchedule')
)
const SeatMap = React.lazy(() => import('./components/pages/Intercity/SeatMap'))
const Ticket = React.lazy(() => import('./components/pages/Intercity/Ticket'))

const CreateUpdateSeatMap = React.lazy(() =>
  import('./components/pages/Intercity/CreateUpdateSeatMap')
)
// bike car
const VehicleBikeCar = React.lazy(() =>
  import('./components/pages/BikeCar/VehicleManagement')
)
const CreateUpdateVehicleBikeCar = React.lazy(() =>
  import('./components/pages/BikeCar/CreateUpdateVehicle')
)
const VehicleTypeBikeCar = React.lazy(() =>
  import('./components/pages/BikeCar/VehicleTypeManagement')
)
const CreateUpdateVehicleTypeBikeCar = React.lazy(() =>
  import('./components/pages/BikeCar/CreateUpdateVehicleType')
)
const VehicleManufacturerBikeCar = React.lazy(() =>
  import('./components/pages/BikeCar/VehicleManufacturer')
)
const CreateUpdateVehicleManufacturerBikeCar = React.lazy(() =>
  import('./components/pages/BikeCar/CreateUpdateVehicleManufacturer')
)
const TripManagementBikeCar = React.lazy(() =>
  import('./components/pages/BikeCar/TripManagement')
)   
const TripDetailBikeCar = React.lazy(() =>
  import('./components/pages/BikeCar/TripDetail')
)
const DriverManagementBikeCar = React.lazy(() =>
  import('./components/pages/BikeCar/DriverManagement')
)
const CreateUpdateDriverBikeCar = React.lazy(() =>
  import('./components/pages/BikeCar/CreateUpdateDriverAccount')
)
const DriverBikeCarDetail = React.lazy(() =>
  import('./components/pages/BikeCar/DriverAccountDetail')
)
const AreaManagement = React.lazy(() =>
  import('./components/pages/Intercity/AreaManagement')
) 
const CreateUpdateArea = React.lazy(() =>
  import('./components/pages/Intercity/CreateUpdateArea')
) 
// Van
const VehicleVan = React.lazy(() =>
  import('./components/pages/Van/VehicleManagement')
)
const CreateUpdateVehicleVan = React.lazy(() =>
  import('./components/pages/Van/CreateUpdateVehicle')
)
const VehicleTypeVan = React.lazy(() =>
  import('./components/pages/Van/VehicleTypeManagement')
)
const CreateUpdateVehicleTypeVan = React.lazy(() =>
  import('./components/pages/Van/CreateUpdateVehicleType')
)
const VehicleManufacturerVan = React.lazy(() =>
  import('./components/pages/BikeCar/VehicleManufacturer')
)
const TripManagementVan = React.lazy(() =>
  import('./components/pages/Van/TripManagement')
)
const TripDetailVan = React.lazy(() =>
  import('./components/pages/Van/TripDetail')
)
const DriverManagementVan = React.lazy(() =>
  import('./components/pages/Van/DriverManagement')
)
const CreateUpdateDriverVan = React.lazy(() =>
  import('./components/pages/Van/CreateUpdateDriverAccount')
)
const DriverVanDetail = React.lazy(() =>
  import('./components/pages/Van/DriverAccountDetail')
)
const OtherServiceManagement = React.lazy(() =>
  import('./components/pages/Van/OtherServicesManagement')
)


// tour car
const TourManagement = React.lazy(() =>
  import('./components/pages/TourCar/TourManagement')
)
const CreateUpdateTour = React.lazy(() =>
  import('./components/pages/TourCar/CreateUpdateTour')
)
const VehicleTourCar = React.lazy(() =>
  import('./components/pages/TourCar/VehicleManagement')
)
const CreateUpdateVehicleTourCar = React.lazy(() =>
  import('./components/pages/TourCar/CreateUpdateVehicle')
)
const VehicleTypeTourCar = React.lazy(() =>
  import('./components/pages/TourCar/VehicleTypeManagement')
)
const CreateUpdateVehicleTypeTourCar = React.lazy(() =>
  import('./components/pages/TourCar/CreateUpdateVehicleType')
)
const VehicleManufacturerTourCar = React.lazy(() =>
  import('./components/pages/BikeCar/VehicleManufacturer')
)
const TripManagementTourCar = React.lazy(() =>
  import('./components/pages/TourCar/TripManagement')
)
const TripDetailTourCar = React.lazy(() =>
  import('./components/pages/TourCar/TripDetail')
)
const OrderTourLog = React.lazy(() =>
  import('./components/pages/TourCar/OrderTourLog')
)
const OrderTourLogDetail = React.lazy(() =>
  import('./components/pages/TourCar/OrderTourLogDetail')
)
const CreateUpdateOrderTour = React.lazy(() =>
  import('./components/pages/TourCar/CreateUpdateOrderTour')
)
const DriverManagementTourCar = React.lazy(() =>
  import('./components/pages/TourCar/DriverManagement')
)
 
const DriverTourCarDetail = React.lazy(() =>
  import('./components/pages/TourCar/DriverAccountDetail')
)
// const OtherServiceManagement = React.lazy(() =>
//   import('./components/pages/TourCar/OtherServicesManagement')
// )

// Service Charge
const ServiceCharge = React.lazy(() =>
  import('./components/pages/ServiceCharge')
)
// Additional Fee
const CreateUpdateFee = React.lazy(() =>
  import('./components/pages/AdditionalFee/CreateUpdateFee')
)
const FeeManagement = React.lazy(() =>
  import('./components/pages/AdditionalFee/FeeManagement')
)

// Revenue
const RevenueProviderManagement = React.lazy(() =>
  import('./components/pages/Dashboard/RevenueProvider')
)
const RevenueBikeCarManagement = React.lazy(() =>
  import('./components/pages/Dashboard/RevenueBikeCar')
)
const RevenueVanBagacManagement = React.lazy(() =>
  import('./components/pages/Dashboard/RevenueVanBagac')
)

// Content management
const CreateUpdateBanner = React.lazy(() =>
  import('./components/pages/ContentManagement/CreateUpdateBanner')
)
const CreateUpdateBank = React.lazy(() =>
  import('./components/pages/ContentManagement/CreateUpdateBank')
)
const BannerManagement = React.lazy(() =>
  import('./components/pages/ContentManagement/BannerManagement')
)
const BankManagement = React.lazy(() =>
  import('./components/pages/ContentManagement/BankManagement')
)
const PromotionManagement = React.lazy(() =>
  import('./components/pages/ContentManagement/PromotionManagement')
)
const CreateUpdatePromotion = React.lazy(() =>
  import('./components/pages/ContentManagement/CreateUpdatePromotion')
)
const CreateUpdatePolicy = React.lazy(() =>
  import('./components/pages/ContentManagement/CreateUpdatePolicy')
)
const CreateUpdateSettingSystem = React.lazy(() =>
  import('./components/pages/ContentManagement/CreateUpdateSettingSystem')
) 
const ReportFeeWalletDriver = React.lazy(() =>
  import('./components/pages/Report/ReportFeeWalletDriver')
) 
const ReportRevenueOrder = React.lazy(() =>
  import('./components/pages/Report/ReportRevenueOrder')
) 
 
const BuildVersion = React.lazy(() =>
  import('./components/pages/BuildVersion')
) 
const Page404 = React.lazy(() => import('./components/pages/AuthPage/Page404'))

const routes = [
  { path: '/admin', exact: true, name: 'Home', component: Dashboard },
  {
    path: '/404',
    exact: true,
    name: '404',
    component: Page404,
  //  role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  // {
  //   path: '/dashboard',
  //   exact: true,
  //   name: 'Dashboard',
  //   component: Dashboard,
  // },
  {
    path: '/admin/dashboard-provider',
    exact: true,
    name: 'Dashboard nhà xe',
    component: DashboardProvider,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/internal-management/',
    exact: true,
    name: 'Nội bộ',
    component: InternalManagement,
  },
  {
    path: '/admin/internal-management/create',
    exact: true,
    name: 'Thêm tài khoản nội bộ mới',
    component: CreateUpdateInternalAccount,
  },
  {
    path: '/admin/internal-management/edit/:id',
    name: 'Chỉnh sửa tài khoản nội bộ',
    component: CreateUpdateInternalAccount,
  },
  {
    path: '/admin/internal-management/detail/:id',
    name: 'Chi tiết tài khoản nội bộ',
    component: InternalAccountDetail,
  },
  {
    path: '/admin/user-management/',
    exact: true,
    name: 'Người dùng',
    component: UserManagement,
  },
  {
    path: '/admin/clients-management/',
    exact: true,
    name: 'Khách hàng',
    component: ClientsManagement,
  },
  {
    path: '/admin/clients-management/create',
    exact: true,
    name: 'Thêm tài khoản khách hàng mới',
    component: CreateUpdateClientAccount,
  },
  {
    path: '/admin/clients-management/edit/:id',
    name: 'Chỉnh sửa tài khoản khách hàng',
    component: CreateUpdateClientAccount,
  },
  {
    path: '/admin/clients-management/detail/:id',
    name: 'Chi tiết tài khoản khách hàng',
    component: ClientAccountDetail,
  },
  {
    path: '/admin/provider-management/',
    exact: true,
    name: 'Nhà xe',
    component: ProviderManagement,
  },
  {
    path: '/admin/provider-management/create',
    exact: true,
    name: 'Thêm tài khoản nhà xe mới',
    component: CreateUpdateProviderAccount,
  },
  {
    path: '/admin/provider-management/edit/:id',
    exact: true,
    name: 'Chỉnh sửa tài khoản nhà xe',
    component: CreateUpdateProviderAccount,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/provider-management/detail/:id',
    exact: true,
    name: 'Chi tiết tài khoản nhà xe',
    component: ProviderDetail,
  },
  {
    path: '/admin/provider-detail',
    exact: true,
    name: 'Chi tiết tài khoản nhà xe',
    component: ProviderDetail,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  }, 
  {
    path: '/admin/wallet',
    exact: true,
    name: 'Quản lý ví Tài xế',
    component: WalletManagement,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/wallet/detail/:id',
    exact: true,
    name: 'Chi tiết ví Tài xế',
    component: WalletDetail,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/driver',
    exact: true,
    name: 'Quản lý Tài xế',
    component: DriverManagement,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/driver/create',
    exact: true,
    name: 'Thêm tài khoản tài xế',
    component: CreateUpdateDriver,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/driver/edit/:id',
    exact: true,
    name: 'cập nhật tài khoản tài xế',
    component: CreateUpdateDriver,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/driver/detail/:id',
    exact: true,
    name: 'Thêm tài khoản tài xế',
    component: DriverDetail,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  // Vehicle General
  {
    path: '/admin/vehicle/vehicle',
    exact: true,
    name: 'Quản lý xe',
    component: Vehicle,
  },
  {
    path: '/admin/vehicle/vehicle/create',
    exact: true,
    name: 'Tạo mới xe',
    component: CreateUpdateVehicle,
  },
  {
    path: '/admin/vehicle/vehicle/edit/:id',
    exact: true,
    name: 'Chỉnh sửa thông tin xe',
    component: CreateUpdateVehicle,
  },
  {
    path: '/admin/vehicle/vehicle-type',
    exact: true,
    name: 'Quản lý loại xe',
    component: VehicleType,
  },
  {
    path: '/admin/vehicle/vehicle-type/create',
    exact: true,
    name: 'Tạo mới loại xe',
    component: CreateUpdateVehicleType,
  },
  {
    path: '/admin/vehicle/vehicle-type/:id',
    exact: true,
    name: 'Chỉnh sửa thông tin loại xe',
    component: CreateUpdateVehicleType,
  },
  {
    path: '/admin/vehicle/vehicle-manufacturer',
    exact: true,
    name: 'Quản lý hãng xe',
    component: VehicleManufacturer,
  },
  {
    path: '/admin/vehicle/vehicle-manufacturer/create',
    exact: true,
    name: 'Tạo mới hãng xe',
    component: CreateUpdateVehicleManufacturer,
  },
  {
    path: '/admin/vehicle/vehicle-manufacturer/:id',
    exact: true,
    name: 'Chỉnh sửa thông tin hãng xe',
    component: CreateUpdateVehicleManufacturer,
  },

// BUS
  {
    path: '/admin/driver/bus',
    exact: true,
    name: 'Tài xế',
    component: DriverBusManagement,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/driver/bus/create',
    exact: true,
    name: 'Thêm tài khoản tài xế liên tỉnh mới',
    component: CreateUpdateDriverAccount,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/driver/bus/edit/:id',
    exact: true,
    name: 'Chỉnh sửa tài khoản tài xế xe liên tỉnh',
    component: CreateUpdateDriverAccount,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/driver/bus/detail/:id',
    exact: true,
    name: 'Chi tiết tài khoản tài xế xe liên tỉnh',
    component: DriverAccountDetail,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  // bus
  {
    path: '/admin/bus/area/',
    exact: true,
    name: 'Địa điểm',
    component: AreaManagement,
  },
  {
    path: '/admin/bus/route/',
    exact: true,
    name: 'Tuyến',
    component: Route,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/bus/route/create',
    exact: true,
    name: 'Thêm tuyến xe mới',
    component: CreateUpdateRoute,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/bus/route/edit/:id',
    exact: true,
    name: 'Chỉnh sửa tuyến xe',
    component: CreateUpdateRoute,
  //  role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/bus/vehicle-type/',
    exact: true,
    name: 'Danh sách loại xe',
    component: VehicleTypeBus,
  //  role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/bus/vehicle/',
    exact: true,
    name: 'Xe',
    component: VehicleBus,
  //  role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/bus/budget/',
    exact: true,
    name: 'Chi phí',
    component: Budget,
  //  role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/bus/budget/:id',
    exact: true,
    name: 'Chỉnh sửa thông tin chi phí tuyến',
    component: CreateUpdateBudget,
  //  role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/bus/seat-map/',
    exact: true,
    name: 'Sơ đồ ghế ngồi',
    component: SeatMap,
  //  role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/bus/seat-map/create',
    exact: true,
    name: 'Tạo sơ đồ ghế ngồi',
    component: CreateUpdateSeatMap,
  //  role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/bus/seat-map/:id',
    exact: true,
    name: 'Chỉnh sửa sơ đồ ghế ngồi',
    component: CreateUpdateSeatMap,
  //  role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/bus/schedule/',
    exact: true,
    name: 'Xếp lịch chạy',
    component: Schedule,
  //  role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/bus/schedule/create',
    exact: true,
    name: 'Tạo mới lịch chạy',
    component: CreateUpdateSchedule,
  //  role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/bus/schedule/edit/:id',
    exact: true,
    name: 'Chỉnh sửa thông tin lịch chạy xe',
    component: CreateUpdateSchedule,
  //  role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/bus/schedule/ticket/:id',
    exact: true,
    name: 'Thông tin vé xe',
    component: Ticket,
  //  role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  // bike car
  {
    path: '/admin/bike-car/vehicle',
    exact: true,
    name: 'Quản lý xe',
    component: VehicleBikeCar,
  },
  {
    path: '/admin/bike-car/vehicle/create',
    exact: true,
    name: 'Tạo mới xe',
    component: CreateUpdateVehicleBikeCar,
  },
  {
    path: '/admin/bike-car/vehicle/edit/:id',
    exact: true,
    name: 'Chỉnh sửa thông tin xe',
    component: CreateUpdateVehicleBikeCar,
  },
  {
    path: '/admin/bike-car/vehicle-type',
    exact: true,
    name: 'Quản lý loại xe',
    component: VehicleTypeBikeCar,
  },
  {
    path: '/admin/bike-car/vehicle-type/create',
    exact: true,
    name: 'Tạo mới loại xe',
    component: CreateUpdateVehicleTypeBikeCar,
  },
  {
    path: '/admin/bike-car/vehicle-type/:id',
    exact: true,
    name: 'Chỉnh sửa thông tin loại xe',
    component: CreateUpdateVehicleTypeBikeCar,
  },
  {
    path: '/admin/bike-car/vehicle-manufacturer',
    exact: true,
    name: 'Quản lý hãng xe',
    component: VehicleManufacturerBikeCar,
  },
  {
    path: '/admin/bike-car/vehicle-manufacturer/create',
    exact: true,
    name: 'Tạo mới hãng xe',
    component: CreateUpdateVehicleManufacturerBikeCar,
  },
  {
    path: '/admin/bike-car/vehicle-manufacturer/:id',
    exact: true,
    name: 'Chỉnh sửa thông tin hãng xe',
    component: CreateUpdateVehicleManufacturerBikeCar,
  },
  {
    path: '/admin/bike-car/trip-management',
    exact: true,
    name: 'Quản lý chuyến xe',
    component: TripManagementBikeCar,
  },
  {
    path: '/admin/bike-car/trip-management/:id',
    exact: true,
    name: 'Thông tin chuyến xe',
    component: TripDetailBikeCar,
  },
  {
    path: '/admin/driver/bike-car',
    exact: true,
    name: 'Quản lý tài xế',
    component: DriverManagementBikeCar,
  },
  {
    path: '/admin/driver/bike-car/create',
    exact: true,
    name: 'Thêm tài xế bike/car mới',
    component: CreateUpdateDriverBikeCar,
  },
  {
    path: '/admin/driver/bike-car/edit/:id',
    exact: true,
    name: 'Chỉnh sửa tài xế bike/car',
    component: CreateUpdateDriverBikeCar,
  },
  {
    path: '/admin/driver/bike-car/detail/:id',
    exact: true,
    name: 'Chi tiết tài xế bike/car',
    component: DriverBikeCarDetail,
  },
  // Van
  {
    path: '/admin/van/vehicle',
    exact: true,
    name: 'Quản lý xe',
    component: VehicleVan,
  },
  {
    path: '/admin/van/vehicle/create',
    exact: true,
    name: 'Tạo mới xe',
    component: CreateUpdateVehicleVan,
  },
  {
    path: '/admin/van/vehicle/:id',
    exact: true,
    name: 'Chỉnh sửa thông tin xe',
    component: CreateUpdateVehicleVan,
  },
  {
    path: '/admin/van/vehicle-type',
    exact: true,
    name: 'Quản lý loại xe',
    component: VehicleTypeVan,
  },
  {
    path: '/admin/van/vehicle-type/create',
    exact: true,
    name: 'Tạo mới loại xe',
    component: CreateUpdateVehicleTypeVan,
  },
  {
    path: '/admin/van/vehicle-type/:id',
    exact: true,
    name: 'Chỉnh sửa thông tin loại xe',
    component: CreateUpdateVehicleTypeVan,
  },
  {
    path: '/admin/van/vehicle-manufacturer',
    exact: true,
    name: 'Quản lý hãng xe',
    component: VehicleManufacturerVan,
  },
  {
    path: '/admin/van/trip-management',
    exact: true,
    name: 'Quản lý chuyến xe',
    component: TripManagementVan,
  },
  {
    path: '/admin/van/trip-management/:id',
    exact: true,
    name: 'Thông tin chuyến xe',
    component: TripDetailVan,
  },
  {
    path: '/admin/driver/van',
    exact: true,
    name: 'Tài xế',
    component: DriverManagementVan,
  },
  {
    path: '/admin/driver/van/create',
    exact: true,
    name: 'Tạo tài xế mới',
    component: CreateUpdateDriverVan,
  },
  {
    path: '/admin/driver/van/edit/:id',
    exact: true,
    name: 'Chỉnh sửa thông tin tài xế',
    component: CreateUpdateDriverVan,
  },
  {
    path: '/admin/driver/van/detail/:id',
    exact: true,
    name: 'Chi tiết tài xế van/bagac',
    component: DriverVanDetail,
  },


  // tour car
  {
    path: '/admin/tour/tourManagement',
    exact: true,
    name: 'Quản lý tour',
    component: TourManagement,
    // role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/tour/tourManagement/create',
    exact: true,
    name: 'Tạo mới tour',
    component: CreateUpdateTour,
    role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/tour/tourManagement/:id',
    exact: true,
    name: 'Chỉnh sửa tour',
    component: CreateUpdateTour,
    role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/tour/orderManagement',
    exact: true,
    name: 'Quản lý order',
    component: TripManagementTourCar,
    role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/tour/orderManagement/create',
    exact: true,
    name: 'Thêm order tour',
    component: CreateUpdateOrderTour,
    role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/tour/orderManagement/edit/:id',
    exact: true,
    name: 'Chỉnh sửa order tour',
    component: CreateUpdateOrderTour,
    role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/tour/orderManagement/detail/:id',
    exact: true,
    name: 'Thông tin order',
    component: TripDetailTourCar,
    role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/tour/orderTourLog',
    exact: true,
    name: 'Lịch sử đơn hàng' ,
    component: OrderTourLog,
    role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/tour/orderTourLog/orderTourLogDetail/:id',
    exact: true,
    name: 'Chi tiết lịch sử đơn hàng',
    component: OrderTourLogDetail,
    role: config.MANAGEMENT_TYPE_USER.PROVIDER,
  },
  {
    path: '/admin/tour/areaManagement',
    exact: true,
    name: 'Quản lý khu vực',
    component: AreaManagement, 
  },
  {
    path: '/admin/tour/areaManagement/create',
    exact: true,
    name: 'Tạo khu vực',
    component: CreateUpdateArea, 
  },
  {
    path: '/admin/tour/areaManagement/:id',
    exact: true,
    name: 'Chỉnh sửa khu vực',
    component: CreateUpdateArea, 
  },
  // {
  //   path: '/admin/tour-car/vehicle',
  //   exact: true,
  //   name: 'Quản lý xe',
  //   component: VehicleTourCar,
  // },
  // {
  //   path: '/admin/tour-car/vehicle/create',
  //   exact: true,
  //   name: 'Tạo mới xe',
  //   component: CreateUpdateVehicleTourCar,
  // },
  // {
  //   path: '/admin/tour-car/vehicle/:id',
  //   exact: true,
  //   name: 'Chỉnh sửa thông tin xe',
  //   component: CreateUpdateVehicleTourCar,
  // },
  // {
  //   path: '/admin/tour-car/vehicle-type',
  //   exact: true,
  //   name: 'Quản lý loại xe',
  //   component: VehicleTypeTourCar,
  // },
  // {
  //   path: '/admin/tour-car/vehicle-type/create',
  //   exact: true,
  //   name: 'Tạo mới loại xe',
  //   component: CreateUpdateVehicleTypeTourCar,
  // },
  // {
  //   path: '/admin/tour-car/vehicle-type/:id',
  //   exact: true,
  //   name: 'Chỉnh sửa thông tin loại xe',
  //   component: CreateUpdateVehicleTypeTourCar,
  // },
  // {
  //   path: '/admin/tour-car/vehicle-manufacturer',
  //   exact: true,
  //   name: 'Quản lý hãng xe',
  //   component: VehicleManufacturerTourCar,
  // },
  // {
  //   path: '/admin/tour-car/trip-management',
  //   exact: true,
  //   name: 'Quản lý chuyến xe',
  //   component: TripManagementTourCar,
  // },
  // {
  //   path: '/admin/tour-car/trip-management/:id',
  //   exact: true,
  //   name: 'Thông tin chuyến xe',
  //   component: TripDetailTourCar,
  // },
  // {
  //   path: '/admin/driver/tour-car',
  //   exact: true,
  //   name: 'Tài xế',
  //   component: DriverManagementTourCar,
  // },
  // {
  //   path: '/admin/driver/tour-car/create',
  //   exact: true,
  //   name: 'Tạo tài xế mới',
  //   component: CreateUpdateDriverTourCar,
  // },
  // {
  //   path: '/admin/driver/tour-car/edit/:id',
  //   exact: true,
  //   name: 'Chỉnh sửa thông tin tài xế',
  //   component: CreateUpdateDriverTourCar,
  // },
  // {
  //   path: '/admin/driver/tour-car/detail/:id',
  //   exact: true,
  //   name: 'Chi tiết tài xế van/bagac',
  //   component: DriverTourCarDetail,
  // },





  {
    path: '/admin/van/other-service',
    exact: true,
    name: 'Quản lý dịch vụ',
    component: OtherServiceManagement,
  },
  {
    path: '/admin/revenue-management/provider',
    exact: true,
    name: 'Quản lý doanh thu xe liên tỉnh',
    component: RevenueProviderManagement,
  },
  {
    path: '/admin/revenue-management/bike-car',
    exact: true,
    name: 'Quản lý doanh thu xe bike/car',
    component: RevenueBikeCarManagement,
  },
  {
    path: '/admin/revenue-management/van-bagac',
    exact: true,
    name: 'Quản lý doanh thu xe van/bagac',
    component: RevenueVanBagacManagement,
  },
  {
    path: '/admin/service-charge',
    exact: true,
    name: 'Cài đăt phí dịch vụ',
    component: ServiceCharge,
  },
  {
    path: '/admin/additional-fee/create',
    exact: true,
    name: 'Tạo phí phụ thu mới',
    component: CreateUpdateFee,
  },
  {
    path: '/admin/additional-fee/:id',
    exact: true,
    name: 'Chỉnh sửa phí phụ thu',
    component: CreateUpdateFee,
  },
  {
    path: '/admin/additional-fee',
    exact: true,
    name: 'Cài đăt phí phụ thu',
    component: FeeManagement,
  },
  {
    path: '/admin/content-management/banner',
    exact: true,
    name: 'Danh sách banner',
    component: BannerManagement,
  },
  {
    path: '/admin/content-management/banner/create',
    exact: true,
    name: 'Tạo banner mới',
    component: CreateUpdateBanner,
  },
  {
    path: '/admin/content-management/banner/edit/:id',
    exact: true,
    name: 'Chỉnh sửa banner',
    component: CreateUpdateBanner,
  },
  {
    path: '/admin/content-management/bank',
    exact: true,
    name: 'Danh sách ngân hàng',
    component: BankManagement,
  },
  {
    path: '/admin/content-management/bank/create',
    exact: true,
    name: 'Tạo bank mới',
    component: CreateUpdateBank,
  },
  {
    path: '/admin/content-management/bank/edit/:id',
    exact: true,
    name: 'Chỉnh sửa ngân hàng',
    component: CreateUpdateBank,
  },
  {
    path: '/admin/content-management/promotion',
    exact: true,
    name: 'Danh sách khuyến mãi',
    component: PromotionManagement,
  },
  {
    path: '/admin/content-management/promotion/create',
    exact: true,
    name: 'Tạo khuyến mãi mới',
    component: CreateUpdatePromotion,
  },
  {
    path: '/admin/content-management/promotion/edit/:id',
    exact: true,
    name: 'Chỉnh sửa khuyến mãi',
    component: CreateUpdatePromotion,
  },
  {
    path: '/admin/content-management/policy',
    exact: true,
    name: 'Chỉnh sửa hotline và policy',
    component: CreateUpdatePolicy,
  },
  {
    path: '/admin/content-management/settingSystem',
    exact: true,
    name: 'Chỉnh sửa cài đặt hệ thống',
    component: CreateUpdateSettingSystem,
  },


  // report 
  {
    path: '/admin/report/feeWallet',
    exact: true,
    name: 'Số dư tài xế',
    component: ReportFeeWalletDriver,
  },
  {
    path: '/admin/report/revenue',
    exact: true,
    name: 'Doanh thu đơn hàng',
    component: ReportRevenueOrder,
  }, 
  {
    path: '/admin/version',
    exact: true,
    name: 'Build Version',
    component: BuildVersion,
  },
]

export default routes
