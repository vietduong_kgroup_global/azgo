import 'react-app-polyfill/ie9' // For IE 9-11 support
import 'react-app-polyfill/stable'
import './polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { createBrowserHistory } from 'history'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router/immutable'
import createStore from './Stores/CreateStore.js'
import { unregister } from './serviceWorker'
import './i18n'
import { UserContext } from 'Contexts/UserData'
import { getTokenData, getPermissions } from 'Utils/token'
import { ConfigProvider } from 'antd'
import vi_VN from 'antd/es/locale/vi_VN'
import 'moment/locale/vi'

const history = createBrowserHistory()

const auth = getTokenData()
const permissions = []

const userData = { auth, permissions }

async function init() {
  const store = createStore(history)
  const MOUNT_NODE = document.getElementById('root')
  const render = () =>
    ReactDOM.render(
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <UserContext.Provider value={userData}>
            <ConfigProvider locale={vi_VN}>
              <App />
            </ConfigProvider>
          </UserContext.Provider>
        </ConnectedRouter>
      </Provider>,
      MOUNT_NODE
    )
  if (module.hot) {
    module.hot.accept('./App', () => {
      ReactDOM.unmountComponentAtNode(MOUNT_NODE)
      render()
    })
  }
  render()
}

init()

unregister()
