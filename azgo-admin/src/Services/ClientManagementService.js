import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'
import * as config from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */

const getItemById = id => {
  return apiClient.get(`/users/${id}`).then(res => camelizeKeys(res.data))
}

const getListItems = filter => {
  return apiClient
    .get(`/users/customers`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

const createItem = values => {
  return apiClient
    .post(
      '/users/customer',
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const editItem = values => {
  return apiClient
    .put(
      `/users/${values.id}`,
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const deleteItem = id => {
  return apiClient.delete(`/users/${id}?user_role=${config.MANAGEMENT_TYPE_USER.CUSTOMER}`).then(res => camelizeKeys(res.data))
}

export const ClientManagementService = {
  getListItems,
  getItemById,
  editItem,
  createItem,
  deleteItem,
}
