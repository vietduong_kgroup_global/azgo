import { apiClientCore as apiClient } from './'
import { getToken } from 'Utils/token'

// import { camelizeKeys, decamelizeKeys } from 'humps'
/**
 * @method Get
 * @param {Number} id
 * @description: Get list role of a Users *
 */
// const getUserRoles = id =>
//   apiClient.get(`/api/core/users/${id}/roles`).then(res => res.data)
const requestResetPassword = data => {
  return apiClient.post(`/reset-password`, data).then(res => res.data)
}
const getUserRoles = id =>
  apiClient.get(`/api/core/users/${id}/roles`).then(res => res.data)
const createNewPassword = values => {
  const val = {
    password: values.password,
    password_confirmation: values.password_confirmation,
  }
  return apiClient
    .put(`/reset-password/change_password/${values.token}`, val)
    .then(res => res.data)
}
const checkTokenResetPassword = token => {
  return apiClient
    .get(`/reset-password/create-new-password/${token}`)
    .then(res => res.data)
}
export const AuthService = {
  requestResetPassword,
  getUserRoles,
  createNewPassword,
  checkTokenResetPassword,
}
