import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list
 */

// const getItemById = id => {
//   return apiClient.get(`/users/${id}`).then(res => camelizeKeys(res.data))
// }

// const getListItems = filter => {
//   return apiClient
//     .get(`/users/customers`, decamelizeKeys(filter))
//     .then(res => camelizeKeys(res.data))
// }

/**
 * @method Post
 * @description Create item
 */

const createItem = values => {
  // return apiClient
  //   .post(
  //     '/seat-pattern',
  //     decamelizeKeys({
  //       ...values,
  //       platform: PLATFORM_TYPE.WEB,
  //     })
  //   )
  //   .then(res => camelizeKeys(res.data))
  return null
}

// const editItem = values => {
//   return apiClient
//     .put(
//       `/users/${values.id}`,
//       decamelizeKeys({
//         ...values,
//         platform: PLATFORM_TYPE.WEB,
//       })
//     )
//     .then(res => camelizeKeys(res.data))
// }

// const deleteItem = id => {
//   return apiClient.delete(`/users/${id}`).then(res => camelizeKeys(res.data))
// }

export const SeatManagementService = {
  // getListItems,
  // getItemById,
  // editItem,
  createItem,
  // deleteItem,
}
