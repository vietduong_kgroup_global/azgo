import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */

const getListItems = () => {
  return apiClient.get(`/custom-charge`).then(res => camelizeKeys(res.data))
}
const editItem = values => {
  return apiClient
    .put(
      `/custom-charge/${values.id}`,
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}
export const ServiceChargeManagementService = {
  getListItems,
  editItem,
}
