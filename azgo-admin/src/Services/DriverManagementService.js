import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'
import * as config from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */
const getListItems = filter => {
  return apiClient
    .get(`/users/drivers`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

const getListItemsOfProvider = id => {
  return apiClient
    .get(`/driver/get-all-driver-by-provider/${id}`)
    .then(res => camelizeKeys(res.data))
}

const getItemById = id => {
  return apiClient.get(`/users/${id}`).then(res => camelizeKeys(res.data))
}

const createItem = values => {
  return apiClient
    .post(
      '/users/driver',
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const getItemLicensePlates = (id1, id2) => {
  return apiClient
    .get(`/vehicle/${id1}/${id2}`)
    .then(res => camelizeKeys(res.data))
}

const editItem = values => {
  return apiClient
    .put(
      `/users/${values.id}`,
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const deleteItem = id => {
  return apiClient.delete(`/users/${id}?user_role=${config.MANAGEMENT_TYPE_USER.DRIVER}`).then(res => camelizeKeys(res.data))
}

export const DriverManagementService = {
  getListItems,
  getListItemsOfProvider,
  getItemById,
  getItemLicensePlates,
  editItem,
  createItem,
  deleteItem,
}
