import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */
const getListItems = filter => {
  return apiClient
    .get(`/vehicle-brand`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

const getItemById = id => {
  return apiClient
    .get(`/vehicle-brand/${id}`)
    .then(res => camelizeKeys(res.data))
}

const createItem = values => {
  return apiClient
    .post(
      '/vehicle-brand',
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const editItem = values => {
  return apiClient
    .put(
      `/vehicle-brand/${values.id}`,
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const deleteItem = id => {
  return apiClient
    .delete(`/vehicle-brand/${id}`)
    .then(res => camelizeKeys(res.data))
}

export const BrandVehicleManagementService = {
  getListItems,
  getItemById,
  editItem,
  createItem,
  deleteItem,
}
