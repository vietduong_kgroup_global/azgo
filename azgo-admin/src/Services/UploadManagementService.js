import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */ 

const deleteImg = values => {
  return apiClient
  .post(
    `/upload-aws/deleteImg`,
    decamelizeKeys({
      ...values,
      platform: PLATFORM_TYPE.WEB,
    })
  ) 
    .then(res => camelizeKeys(res.data))
} 
export const UploadManagementService = { 
  deleteImg, 
}
