import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'

const getItemAmountCustomerBus = filter => {
  return apiClient
    .get(`/ticket/count-amount-customer`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}
const getItemAmountScheduleBus = filter => {
  return apiClient
    .get(`/dashboard/count-amount-schedule`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}
const getItemAmountScheduleDoneBus = filter => {
  return apiClient
    .get(`/dashboard/count-amount-schedule`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}
const getItemRevenueBus = filter => {
  return apiClient
    .get(`/dashboard/revenue-bus`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}
const getItemRatingProvider = () => {
  return apiClient
    .get(`/rating-schedule/get-avg-rating-by-provider`)
    .then(res => camelizeKeys(res.data))
}
// bike
const getItemAmountOrderBike = filter => {
  return apiClient
    .get(
      `/bike-car-customer/count-amount-customer-bike-car`,
      decamelizeKeys(filter)
    )
    .then(res => camelizeKeys(res.data))
}
const getItemAmountOrderMonthBike = filter => {
  return apiClient
    .get(
      `/bike-car-customer/count-order-bike-car-by-admin`,
      decamelizeKeys(filter)
    )
    .then(res => camelizeKeys(res.data))
}
const getItemAmountOrderMonthDoneBike = filter => {
  return apiClient
    .get(
      `/bike-car-customer/count-order-bike-car-by-admin`,
      decamelizeKeys(filter)
    )
    .then(res => camelizeKeys(res.data))
}
const getItemRevenueBike = filter => {
  return apiClient
    .get(`/bike-car-customer/revenue-bike-car`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}
// van
const getItemAmountOrderVan = filter => {
  return apiClient
    .get(
      `/van-bagac-orders/count-amount-customer-van-bagac/by-admin`,
      decamelizeKeys(filter)
    )
    .then(res => camelizeKeys(res.data))
}
const getItemAmountOrderMonthVan = filter => {
  return apiClient
    .get(
      `/van-bagac-orders/count-order-van-bagac/by-admin`,
      decamelizeKeys(filter)
    )
    .then(res => camelizeKeys(res.data))
}
const getItemAmountOrderMonthDoneVan = filter => {
  return apiClient
    .get(
      `/van-bagac-orders/count-order-van-bagac/by-admin`,
      decamelizeKeys(filter)
    )
    .then(res => camelizeKeys(res.data))
}
const getItemRevenueVan = filter => {
  return apiClient
    .get(`/van-bagac-orders/revenue-van-bagac/by-admin`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}
const getItemsLineChart = filter => {
  return apiClient
    .get(`/dashboard/line-chart-revenue-by-admin`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}
const getItemsLineChartProvider = filter => {
  return apiClient
    .get(
      `/dashboard/line-chart-revenue-bus-by-provider`,
      decamelizeKeys(filter)
    )
    .then(res => camelizeKeys(res.data))
}

// provider
const getItemsVehicleScheduleNearest = filter => {
  return apiClient
    .get(
      `/vehicle-schedule/get-vehicle-schedule/nearest`,
      decamelizeKeys(filter)
    )
    .then(res => camelizeKeys(res.data))
}

const getListItemsRevenue = filter => {
  return apiClient
    .get(`/ticket/revenue-per-vehicle-bus`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

export const DashboardManagementService = {
  getItemAmountCustomerBus,
  getItemAmountScheduleBus,
  getItemAmountScheduleDoneBus,
  getItemRevenueBus,
  getItemRatingProvider,
  getItemAmountOrderBike,
  getItemAmountOrderMonthBike,
  getItemAmountOrderMonthDoneBike,
  getItemRevenueBike,
  getItemAmountOrderVan,
  getItemAmountOrderMonthVan,
  getItemAmountOrderMonthDoneVan,
  getItemRevenueVan,
  getItemsLineChart,
  getItemsLineChartProvider,
  //  provider
  getItemsVehicleScheduleNearest,
  getListItemsRevenue,
}
