import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from '../index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */
const getListItems = filter => {
  return apiClient
    .get(`/tour/get-list-tour`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

// const getListWithoutItems = values => {
//   return apiClient
//     .get(
//       `/users/get-all-the-driver-van-bagac-list-without-vehicle`,
//       decamelizeKeys(values)
//     )
//     .then(res => camelizeKeys(res.data))
// }

const getItemById = id => { 
  return apiClient.get(`/tour/${id}`).then(res => camelizeKeys(res.data))
}

const createItem = values => {
  return apiClient
    .post(
      '/tour',
      decamelizeKeys({
        ...values,
        // platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const editItem = values => { 
  return apiClient
    .put(
      `/tour/${values.uuid}`,
      decamelizeKeys({
        ...values,
        // platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const deleteItem = id => {
  return apiClient.delete(`/tour/${id}`).then(res => camelizeKeys(res.data))
}

export const TourManagementService = {
  getListItems,
  getItemById,
  editItem,
  createItem,
  deleteItem,
  // getListWithoutItems,
}
