import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from '../index'

/**
 * @method Get
 * @description Get list plant
 */
const getListItems = filter => {
  return apiClient
    .get(
      `/tour-order/get-list`,
      decamelizeKeys(filter)
    )
    .then(res => camelizeKeys(res.data))
}

const getItemById = id => {
  return apiClient
    .get(`/tour-order/${id}`)
    .then(res => camelizeKeys(res.data))
}
const createItem = values => { 
  return apiClient
    .post(
      '/tour-order',
      decamelizeKeys({
        ...values,
        // platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const editItem = values => { 
  return apiClient
    .put(
      `/tour-order/${values.uuid}`,
      decamelizeKeys({
        ...values,
        // platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const deleteItem = id => {
  return apiClient.delete(`/tour-order/${id}`).then(res => camelizeKeys(res.data))
}

const checkFeeWalletOrder = values => { 
  return apiClient
    .post(
      `/tour-order/checkFeeWalletToReceiveOrder`,
      decamelizeKeys({
        ...values,
        // platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}
export const TripManagementService = {
  getListItems,
  getItemById,
  createItem,
  editItem,
  deleteItem,
  checkFeeWalletOrder
}
