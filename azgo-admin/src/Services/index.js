import { create } from 'apisauce'
import { Config } from 'Config'
import { getToken } from 'Utils/token'

export const apiClient = create({
  /**
   * Import the config from the App/Config/index.js file
   */
  baseURL: Config.DEV_URL.CORE,
  headers: {
    'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE',
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'bearer ' + getToken(),
  },
  timeout: 15000,
})

export const apiClientCore = create({
  /**
   * Import the config from the App/Config/index.js file
   */
  baseURL: Config.DEV_URL.CORE,
  headers: {
    'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE',
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'bearer ' + getToken(),
  },
  timeout: 15000,
})
