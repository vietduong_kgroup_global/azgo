import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */
const createItem = values => {
  return apiClient
    .post(
      '/additional-fee',
      decamelizeKeys({
        ...values,
        // platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}
const getAccountByAdmin = values => { 
  return apiClient
    .get(
      `/accounts/getAccount/${values.user_uuid}`,
      decamelizeKeys({
        ...values,
        // platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const editAccount = values => { 
  return apiClient
    .put(
      `/accounts/updateAccountByAdmin/${values.user_uuid}`,
      decamelizeKeys({
        ...values,
        // platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

// const deleteItem = uuid => {
//   return apiClient.delete(`/additional-fee/${uuid}`).then(res => camelizeKeys(res.data))
// }

export const AccountManagementService = {
  editAccount,
  createItem,
  getAccountByAdmin
  // deleteItem,
}
