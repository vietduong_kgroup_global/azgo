import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */
const getListItems = filter => {
  return apiClient
    .get(`/wallet`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

// const getListItemsOfProvider = id => {
//   return apiClient
//     .get(`/driver/get-all-driver-by-provider/${id}`)
//     .then(res => camelizeKeys(res.data))
// }

const getItemById = id => {
  return apiClient.get(`/wallet/${id}`).then(res => camelizeKeys(res.data))
}

// const createItem = values => {
//   return apiClient
//     .post(
//       '/users/driver',
//       decamelizeKeys({
//         ...values,
//         platform: PLATFORM_TYPE.WEB,
//       })
//     )
//     .then(res => camelizeKeys(res.data))
// }

// const getItemLicensePlates = (id1, id2) => {
//   return apiClient
//     .get(`/vehicle/${id1}/${id2}`)
//     .then(res => camelizeKeys(res.data))
// }

const editItem = values => {
  return apiClient
    .put(
      `/wallet/${values.id}`,
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}
const createItem = values => {
  return apiClient
    .post(
      `/wallet/create-wallet-for-driver`,
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}
 
// const deleteItem = id => {
//   return apiClient.delete(`/users/${id}`).then(res => camelizeKeys(res.data))
// }

export const WalletManagementService = {
  getListItems,
  // getListItemsOfProvider,
  getItemById,
  // getItemLicensePlates,
  editItem,
  createItem,
  // createItem,
  // deleteItem,
}
