import { camelizeKeys } from 'humps'
import { apiClient } from './index'

/**
 * @method Get
 * @description Get list plant
 */

const blockUser = id => {
  return apiClient.put(`/users/block/${id}`).then(res => camelizeKeys(res.data))
}

const unblockUser = id => {
  return apiClient
    .put(`/users/unblock/${id}`)
    .then(res => camelizeKeys(res.data))
}

export const BlockUserService = {
  blockUser,
  unblockUser,
}
