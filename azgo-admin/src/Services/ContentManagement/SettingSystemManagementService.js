import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from '../index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */
const getItem = () => {
  return apiClient.get(`/setting-system`).then(res => camelizeKeys(res.data))
}

const editItem = values => {
  return apiClient
    .put(
      `/setting-system/${values.id}`,
      decamelizeKeys({
        ...values,
      })
    )
    .then(res => camelizeKeys(res.data))
}
const createItem = values => {
  return apiClient
    .post(
      '/setting-system/',
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}
export const SettingSystemManagementService = {
  getItem,
  editItem,
  createItem
}
