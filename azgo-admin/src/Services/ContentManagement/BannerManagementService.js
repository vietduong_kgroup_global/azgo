import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from '../index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */
const getListItems = () => {
  return apiClient.get(`/banner-management`).then(res => camelizeKeys(res.data))
}

const getItemById = id => {
  return apiClient
    .get(`/banner-management/${id}`)
    .then(res => camelizeKeys(res.data))
}

const createItem = values => {
  return apiClient
    .post(
      '/banner-management/',
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const editItem = values => {
  return apiClient
    .put(
      `/banner-management/${values.id}`,
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

export const BannerManagementService = {
  getListItems,
  getItemById,
  editItem,
  createItem,
}
