import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from '../index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */
const getItem = () => {
  return apiClient.get(`/hotline-policy`).then(res => camelizeKeys(res.data))
}

const editItem = values => {
  return apiClient
    .put(
      `/hotline-policy/${values.id}`,
      decamelizeKeys({
        ...values,
      })
    )
    .then(res => camelizeKeys(res.data))
}
const createItem = values => {
  return apiClient
    .post(
      '/hotline-policy/',
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}
export const PolicyManagementService = {
  getItem,
  editItem,
  createItem
}
