import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list
 */
const getListItems = filter => {
  // return apiClient
  //   .get(`/users/providers`, decamelizeKeys(filter))
  //   .then(res => camelizeKeys(res.data))
  return {
    status: true,
    message: 'OK',
    data: [],
  }
}

const getItemById = id => {
  // return apiClient.get(`/users/${id}`).then(res => camelizeKeys(res.data))
  return {
    status: true,
    message: 'OK',
    data: [],
  }
}

const createItem = values => {
  // return apiClient
  //   .post(
  //     '/users/provider',
  //     decamelizeKeys({
  //       ...values,
  //       platform: PLATFORM_TYPE.WEB,
  //     })
  //   )
  //   .then(res => camelizeKeys(res.data))
  return {
    status: true,
    message: 'OK',
    data: [],
  }
}

const editItem = values => {
  // return apiClient
  //   .put(
  //     `/users/${values.id}`,
  //     decamelizeKeys({
  //       ...values,
  //       platform: PLATFORM_TYPE.WEB,
  //     })
  //   )
  //   .then(res => camelizeKeys(res.data))
  return {
    status: true,
    message: 'OK',
    data: [],
  }
}

export const TicketService = {
  getListItems,
  getItemById,
  editItem,
  createItem,
}
