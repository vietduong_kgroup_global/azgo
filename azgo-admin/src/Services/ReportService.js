import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'
import XLSX from "xlsx";
import FileDownload  from 'js-file-download';
import axios from 'axios'
/**
 * @method Get
 * @description Get list items
 */
// const exportExcel = payload => {
// //   return apiClient
// //     .get(`/users/administrators/`, decamelizeKeys(filter))
// //     .then(res => camelizeKeys(res.data))
// const ws = XLSX.utils.aoa_to_sheet(payload.exportData, {sheetStubs: true, cellStyles: true});
// const wb = XLSX.utils.book_new();
// var wscols = [
//   { wpx: 50},
//   { wpx: 50},
//   { wpx: 200}, 
//   { wpx: 200}, 
//   { wpx: 200}, 
//   { wpx: 200}, 
//   { wpx: 200}, 
//   { wpx: 200}, 
//   { wpx: 200}, 
//   { wpx: 200}, 
//   { wpx: 150}, 
//   { wpx: 150}, 
// ]
// var wsrows = [
//   {hpx: 30},
//   {hpx: 30 },
//   {hpx: 20 },
// ]
//   //  for (var i = 0; i < payload.header.length; i++) {  // columns length added
//   //    wscols.push({hpx: 30 })
//   //    console.log(payload.header[i])
//   //  } 
//    ws["!cols"] = wscols; 
//    ws["!rows"] = wsrows; 
//   //  ws['A2'].w = "3";  
//   //  ws["!margins"]={left:5,top:3}
//   //  ws['A2'].l = { Target:"#E2" }; /* link to cell E2 */
//   //  ws['A1'] = {left: 0.1} 

// ws["!merges"] = [
//   { s: { r: 0, c: 0 }, e: { r: 0, c: payload.length || 1 } }
// ];

// XLSX.utils.book_append_sheet(wb, ws, payload.sheet || "SheetJS");
// /* generate XLSX file and send to client */
// // XLSX.readFile(`${payload.name}.xlsx`, {cellStyles: true})
// XLSX.writeFile(wb, `${payload.name}.xlsx`);
// } 
const driverWalletReport = filter => { 
  return apiClient
  .get(`/users/driver_wallet_report`, decamelizeKeys(filter))
  .then((res) =>  camelizeKeys(res.data)) 
}
const exportDriverWalletReport = filter => {   
  return apiClient
    .get(`/users/export_driver_wallet_report`, decamelizeKeys(filter), {responseType: 'blob'})
    .then((res) => res)
}
const revenueOrderReport = filter => {   
  return apiClient
    .get(`/tour-order/revenue_order_report`, decamelizeKeys(filter))
    .then((res) =>  camelizeKeys(res.data))
}
const exportRevenueOrderReport = filter => {   
  return apiClient
    .get(`/tour-order/export_revenue_order_report`, decamelizeKeys(filter), {responseType: 'blob'})
    .then((res) => res)
}
 
export const ReportService = {
    // exportExcel,
    driverWalletReport,
    revenueOrderReport,
    exportRevenueOrderReport,
    exportDriverWalletReport
}
