import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'
import * as config from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */
const getListItems = filter => {
  return apiClient
    .get(`/users/providers`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

const getListItemsOfDriver = filter => {
  return apiClient
    .get(`/providers`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

const getItemById = id => {
  return apiClient.get(`/users/${id}`).then(res => camelizeKeys(res.data))
}

const createItem = values => {
  return apiClient
    .post(
      '/users/provider',
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const editItem = values => {
  return apiClient
    .put(
      `/users/${values.id}`,
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const deleteItem = id => {
  return apiClient.delete(`/users/${id}?user_role=${config.MANAGEMENT_TYPE_USER.PROVIDER}`).then(res => camelizeKeys(res.data))
}

export const ProviderManagementService = {
  getListItems,
  getListItemsOfDriver,
  getItemById,
  editItem,
  createItem,
  deleteItem,
}
