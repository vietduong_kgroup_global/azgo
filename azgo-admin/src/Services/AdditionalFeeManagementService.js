import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */
const getListItems = filter => {
  return apiClient
    .get(`/additional-fee`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

const getItemById = uuid => { 
  return apiClient.get(`/additional-fee/${uuid}`).then(res => camelizeKeys(res.data))
}

const createItem = values => {
  return apiClient
    .post(
      '/additional-fee',
      decamelizeKeys({
        ...values,
        // platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const editItem = values => { 
  return apiClient
    .put(
      `/additional-fee/${values.uuid}`,
      decamelizeKeys({
        ...values,
        // platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const deleteItem = uuid => {
  return apiClient.delete(`/additional-fee/${uuid}`).then(res => camelizeKeys(res.data))
}

export const AdditionalFeeManagementService = {
  getListItems,
  getItemById,
  editItem,
  createItem,
  deleteItem,
}
