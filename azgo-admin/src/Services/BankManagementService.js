import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */

const getListItems = filter => {
  return apiClient
    .get(`/bank/getAllByOffsetLimit`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}
const getAllItemRequest = filter => {
  return apiClient
    .get(`/bank/getAllBankByAdmin`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
} 
const createItem = values => {
  return apiClient
    .post(
      '/bank',
      decamelizeKeys({
        ...values,
        // platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const getItemById = id => {
  return apiClient.get(`/bank/${id}`).then(res => camelizeKeys(res.data))
}

const editItem = values => {
  return apiClient
    .put(
      `/bank/${values.uuid}`,
      decamelizeKeys({
        ...values,
        // platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const deleteItem = id => {
  return apiClient.delete(`/bank/${id}`).then(res => camelizeKeys(res.data))
}

export const BankManagementService = {
  getListItems,
  editItem,
  createItem,
  deleteItem,
  getItemById,
  getAllItemRequest
}
