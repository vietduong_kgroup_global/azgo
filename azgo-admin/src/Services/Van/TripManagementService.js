import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from '../index'

/**
 * @method Get
 * @description Get list plant
 */
const getListItems = filter => {
  return apiClient
    .get(
      `/van-bagac-orders/get-list-ticket-by-van-ba-gac`,
      decamelizeKeys(filter)
    )
    .then(res => camelizeKeys(res.data))
}

const getItemById = id => {
  return apiClient
    .get(`/van-bagac-orders/${id}`)
    .then(res => camelizeKeys(res.data))
}

export const TripManagementService = {
  getListItems,
  getItemById,
}
