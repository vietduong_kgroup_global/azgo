import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from '../index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */

const getItemById = id => {
  return apiClient
    .get(`/vehicle-schedule/${id}`)
    .then(res => camelizeKeys(res.data))
}

const getListItems = filter => {
  return apiClient
    .get(`/vehicle-schedule`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

const createItem = values => {
  return apiClient
    .post(
      '/vehicle-schedule',
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const editItem = values => {
  return apiClient
    .put(
      `/vehicle-schedule/${values.id}`,
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const deleteItem = id => {
  return apiClient
    .delete(`/vehicle-schedule/${id}`)
    .then(res => camelizeKeys(res.data))
}

export const ScheduleManagementService = {
  getListItems,
  getItemById,
  editItem,
  createItem,
  deleteItem,
}
