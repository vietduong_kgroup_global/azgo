import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from '../index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */

const getItemById = id => {
  return apiClient.get(`/routes/${id}`).then(res => camelizeKeys(res.data))
}

const getListItems = filter => {
  return apiClient
    .get(`/routes`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

const getListItemsProvider = id => {
  return apiClient
    .get(`/routes/get-route-by-provider/${id}`, decamelizeKeys(id))
    .then(res => camelizeKeys(res.data))
}

const createItem = values => {
  return apiClient
    .post(
      '/routes',
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const editItem = values => {
  return apiClient
    .put(
      `/routes/${values.id}`,
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const deleteItem = id => {
  return apiClient.delete(`/routes/${id}`).then(res => camelizeKeys(res.data))
}

export const RouteManagementService = {
  getListItems,
  getListItemsProvider,
  getItemById,
  editItem,
  createItem,
  deleteItem,
}
