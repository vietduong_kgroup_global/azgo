import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from '../index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */
const getListItems = filter => {
  return apiClient
    .get(`/vehicle/bus`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

const getItemById = id => {
  return apiClient.get(`/vehicle/${id}`).then(res => camelizeKeys(res.data))
}

const createItem = values => {
  return apiClient
    .post(
      '/vehicle/bus',
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const editItem = values => {
  return apiClient
    .put(
      `/vehicle/bus/${values.id}`,
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const deleteItem = id => {
  return apiClient.delete(`/vehicle/${id}`).then(res => camelizeKeys(res.data))
}

const getItemLicensePlates = (id1, id2) => {
  return apiClient
    .get(`/vehicle/${id1}/${id2}`)
    .then(res => camelizeKeys(res.data))
}

export const VehicleManagementService = {
  getListItems,
  getItemById,
  editItem,
  createItem,
  deleteItem,
  getItemLicensePlates,
}
