import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from '../index'

/**
 * @method Get
 * @description Get list plant
 */

const getListItems = filter => {
  return apiClient
    .get(
      `/vehicle-schedule/revenue/per-vehicle-schedule`,
      decamelizeKeys(filter)
    )
    .then(res => camelizeKeys(res.data))
}

export const RevenueManagementService = {
  getListItems,
}
