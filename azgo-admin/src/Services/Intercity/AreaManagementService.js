import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from '../index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */

const getListItems = filter => {
  return apiClient
    .get(`/area`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

const createItem = values => {
  return apiClient
    .post(
      '/area',
      decamelizeKeys({
        ...values,
        // platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const getItemById = id => {
  return apiClient.get(`/area/${id}`).then(res => camelizeKeys(res.data))
}

const editItem = values => {
  return apiClient
    .put(
      `/area/${values.uuid}`,
      decamelizeKeys({
        ...values,
        // platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const deleteItem = id => {
  return apiClient.delete(`/area/${id}`).then(res => camelizeKeys(res.data))
}

export const AreaManagementService = {
  getListItems,
  editItem,
  createItem,
  deleteItem,
  getItemById,
}
