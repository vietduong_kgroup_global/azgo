import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from '../index'
import { PLATFORM_TYPE } from 'Utils/enum'
import * as config from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */
const getListItems = filter => {
  return apiClient
    .get(`/users/drivers-bike-car`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

const getListWithoutItems = values => {
  return apiClient
    .get(
      `/users/get-all-the-driver-bike-car-list-without-vehicle`,
      decamelizeKeys(values)
    )
    .then(res => camelizeKeys(res.data))
}

const getItemById = id => {
  return apiClient.get(`/users/${id}`).then(res => camelizeKeys(res.data))
}

const createItem = values => {
  return apiClient
    .post(
      '/users/driver',
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    // .then(res => camelizeKeys(res.data))
    .then(res => console.log("res: ", res))
}

const editItem = values => {
  return apiClient
    .put(
      `/users/${values.id}`,
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const deleteItem = id => {
  return apiClient.delete(`/users/${id}?user_role=${config.MANAGEMENT_TYPE_USER.DRIVER_BIKE_CAR}`).then(res => camelizeKeys(res.data))
}

export const DriverManagementService = {
  getListItems,
  getItemById,
  editItem,
  createItem,
  deleteItem,
  getListWithoutItems,
}
