import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from '../index'

/**
 * @method Get
 * @description Get list plant
 */
const getListItems = filter => {
  return apiClient
    .get(
      `/bike-car-customer/get-list-ticket-by-bike-car`,
      decamelizeKeys(filter)
    )
    .then(res => camelizeKeys(res.data))
}

const getItemById = id => {
  return apiClient
    .get(`/bike-car-customer/detail-order-book-bike-car/${id}`)
    .then(res => camelizeKeys(res.data))
}

export const TripCarManagementService = {
  getListItems,
  getItemById,
}
