import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from '../index'
import { PLATFORM_TYPE } from 'Utils/enum'

/**
 * @method Get
 * @description Get list plant
 */
const getListItems = filter => {
  return apiClient
    .get(`/vehicle-types/vehicle`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

const getItemById = id => {
  return apiClient
    .get(`/vehicle-types/${id}`)
    .then(res => camelizeKeys(res.data))
}

const createItem = values => {
  return apiClient
    .post(
      '/vehicle-types/vehicle',
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const editItem = values => {
  return apiClient
    .put(
      `/vehicle-types/vehicle/${values.id}`,
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

const deleteItem = id => {
  return apiClient
    .delete(`/vehicle-types/${id}`)
    .then(res => camelizeKeys(res.data))
}

const getListItemsByVehicleGroup = id => {
  return apiClient
    .get(`/vehicle-types/find-by-vehicle-group/${id}`)
    .then(res => camelizeKeys(res.data))
}

export const VehicleTypeManagementService = {
  getListItems,
  getItemById,
  editItem,
  createItem,
  deleteItem,
  getListItemsByVehicleGroup,
}
