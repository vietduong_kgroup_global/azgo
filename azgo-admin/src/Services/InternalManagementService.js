import { camelizeKeys, decamelizeKeys } from 'humps'
import { apiClient } from './index'
import { PLATFORM_TYPE } from 'Utils/enum'
import * as config from 'Utils/enum'

/**
 * @method Get
 * @description Get list items
 */
const getListItems = filter => {
  return apiClient
    .get(`/users/administrators/`, decamelizeKeys(filter))
    .then(res => camelizeKeys(res.data))
}

/**
 * @method Get
 * @param {String} user id
 * @description Get item by id
 */
const getItemById = id => {
  return apiClient.get(`/users/${id}`).then(res => camelizeKeys(res.data))
}

/**
 * @method Post
 * @param {Object} name
 * @description Create new item
 */

const createItem = values => {
  return apiClient
    .post(
      '/users/administrator',
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

/**
 * @method Put
 * @param {String} userId
 * @description Edit item by id
 */

const editItem = values => {
  return apiClient
    .put(
      `/users/${values.id}`,
      decamelizeKeys({
        ...values,
        platform: PLATFORM_TYPE.WEB,
      })
    )
    .then(res => camelizeKeys(res.data))
}

/**
 * @method DELETE
 * @param{Number} userId
 * @description Delete plant
 */
const deleteItem = id => {
  return apiClient.delete(`/users/${id}?user_role=${config.MANAGEMENT_TYPE_USER.ADMIN}`).then(res => camelizeKeys(res.data))
}

export const InternalManagementService = {
  getListItems,
  getItemById,
  createItem,
  editItem,
  deleteItem,
}
