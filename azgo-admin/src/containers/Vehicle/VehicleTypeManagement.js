import React, { useEffect, useState } from 'react'
import CardVehicleType from 'components/organisms/Vehicle/CardVehicleTypeManagement'
import PropTypes from 'prop-types'
import { VehicleTypeManagementSelectors } from 'Stores/Vehicle/VehicleTypeManagement/Selectors'
import { VehicleTypeManagementActions } from 'Stores/Vehicle/VehicleTypeManagement/Actions'
import { withRouter } from 'react-router-dom'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Row, Col } from 'reactstrap'
import { Pagination } from 'antd';

const CardVehicleTypeContainer = ({
  items,
  history,
  filter,
  setFilter,
  getItems,
  totalCount,
  showLoading,
  clearItems,
  search,
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const onChangePage = (page, per_page) =>{
    setFilter({
      page: page,
    })
    setPagination({
      ...pagination,
      current: page,
    })
  }
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter)
  }, [getItems, clearItems, filter])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  return (
    <>
      <CardVehicleType dataSource={items} />
      {
        totalCount === 0 && (
          <div>Không có kết quả tìm kiếm</div>
        )
      }
      <Row  justify="end">
        <Col xs="12" sm="12" md="12">
          <Pagination onChange={onChangePage} defaultCurrent={1} total={totalCount} />
        </Col>
      </Row>
    </>
  )
}

CardVehicleTypeContainer.propTypes = {
  filter: PropTypes.object.isRequired,
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  history: PropTypes.any,
  setFilter: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  clearItems: PropTypes.func.isRequired,
  search: PropTypes.string,
}
const mapStateToProps = state => ({
  items: VehicleTypeManagementSelectors.getItems(state),
  totalCount: VehicleTypeManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})
const mapDispatchToProps = dispatch => ({
  getItems: filter =>
    dispatch(VehicleTypeManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(VehicleTypeManagementActions.clearItems()),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(CardVehicleTypeContainer)
