import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { Popconfirm, Select } from 'antd'
import { Link, withRouter } from 'react-router-dom'
import { AdditionalFeeManagementSelectors } from 'Stores/AdditionalFeeManagement/Selectors'
import { AdditionalFeeManagementActions } from 'Stores/AdditionalFeeManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import PropTypes from 'prop-types'
import { Button } from 'reactstrap'
import { UserSelectors } from 'Stores/User/Selectors'
import FormatPrice from 'components/organisms/FormatPrice'
import * as config from 'Utils/enum'
const TableFeeManagementContainer = ({
  items,
  getItems,
  history,
  filter,
  setFilter,
  clearItems,
  showLoading,
  totalCount,
  deleteItem,
  search,
  userInfo
}) => {
  let company = userInfo && userInfo.get('adminProfile') && userInfo.get('adminProfile').toJS().company  || ''
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter)
  }, [getItems, filter])
  useEffect(() => {
    setFilter({
      per_page: 10,
      page: 1,
      keyword: '',
      isFromAirport: null
    })
  }, [])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount, filter])
  const handleOpenUpdate = id => { 
    history.push('/admin/additional-fee/' + id)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  const columns = [
    {
      title: 'Mã phí',
      key: 'code',
      dataIndex: 'code'
    },
    {
      title: 'Tên phí',
      key: 'name',
      dataIndex: 'name'
    },
    {
      title: 'Kiểu giá trị',
      key: 'type',
      render: value => { 
        if(value.type === config.MANAGEMENT_TYPE_ADDITIONAL_FEE.FLAT) return 'Số tiền'
        return 'Phần trăm' 
      },
    },
    {
      title: 'Giá trị',
      key: 'time',
      render: value => { 
        if(value.type === config.MANAGEMENT_TYPE_ADDITIONAL_FEE.FLAT)
          return (
            <FormatPrice 
              value={value.flat}
              renderText={(value) => 
                <div>{value} VNĐ</div>
              }
            />
          )
        return value.percent ? `${value.percent*100}%` : 0
      },
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      width: 140,
      render: (key, value) => (
        company ? <></> : 
        <> 
          <Button
            color="light"
            size="sm"
            className="btn-sm-ct"
            style={{ marginRight: '5px' }}
            onClick={() =>  handleOpenUpdate(value.uuid)}>
            <i className="fa fa-pencil" />
            &nbsp;Sửa
          </Button>
          <Popconfirm
            placement="top"
            onConfirm={() => handleConfirmDelete(value.uuid)}
            title={
              'Bạn có chắc chắn muốn xóa phí ' +
              `${value.name ? value.name : ''}`
            }
            okText="Có"
            cancelText="Không">
            <Button color="danger" size="sm" className="btn-sm-ct">
              <i className="fa fa-trash-o" />
              &nbsp;Xóa
            </Button>
          </Popconfirm>
        </> 
      ),
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

const mapStateToProps = state => ({
  userInfo: UserSelectors.getUserData(state),
  items: AdditionalFeeManagementSelectors.getItems(state),
  totalCount: AdditionalFeeManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

TableFeeManagementContainer.propTypes = {
  filter: PropTypes.object.isRequired,
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  history: PropTypes.any,
  setFilter: PropTypes.func.isRequired,
  clearItems: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  deleteItem: PropTypes.func,
  search: PropTypes.string,
}

const mapDispatchToProps = dispatch => ({
  getItems: filter => dispatch(AdditionalFeeManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(AdditionalFeeManagementActions.clearItems()),
  deleteItem: id => dispatch(AdditionalFeeManagementActions.deleteItemRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(TableFeeManagementContainer)
