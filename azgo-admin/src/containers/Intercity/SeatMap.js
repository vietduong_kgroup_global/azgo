import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { withRouter } from 'react-router-dom'
import { Popconfirm, Select } from 'antd'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import 'Stores/Intercity/SeatMapManagement/Reducers'
import 'Stores/Intercity/SeatMapManagement/Sagas'
import { SeatMapManagementSelectors } from 'Stores/Intercity/SeatMapManagement/Selectors'
import { SeatMapManagementActions } from 'Stores/Intercity/SeatMapManagement/Actions'
import { UserSelectors } from 'Stores/User/Selectors'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import * as config from 'Utils/enum'

const SeatMapManagementContainer = ({
  items,
  history,
  filter,
  setFilter,
  getItems,
  totalCount,
  showLoading,
  deleteItem,
  clearItems,
  search,
  idProvider,
  itemUser,
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const handleOpenUpdate = id => {
    history.push('/bus/seat-map/' + id)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  const { Option } = Select
  const columns = [
    {
      title: 'Tên mẫu',
      dataIndex: 'name',
    },
    {
      title: 'Số lượng ghế',
      dataIndex: 'totalSeat',
    },
    {
      title: 'Nhà xe',
      dataIndex: 'provider.providerName',
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Select
            labelInValue
            style={{ width: 120 }}
            value={[]}
            placeholder="Chọn">
            <Option value="1" onClick={() => handleOpenUpdate(value.id)}>
              Sửa
            </Option>
            <Option value="2">
              <Popconfirm
                placement="left"
                onConfirm={() => handleConfirmDelete(value.id)}
                title="Bạn có muốn xóa?"
                okText="Có"
                cancelText="Không">
                <div>Xóa</div>
              </Popconfirm>
            </Option>
          </Select>
        </>
      ),
    },
  ]
  const columnsProvider = [
    {
      title: 'Tên mẫu',
      dataIndex: 'name',
    },
    {
      title: 'Số lượng ghế',
      dataIndex: 'totalSeat',
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Select
            labelInValue
            style={{ width: 120 }}
            value={[]}
            placeholder="Chọn">
            <Option value="1" onClick={() => handleOpenUpdate(value.id)}>
              Sửa
            </Option>
            <Option value="2">
              <Popconfirm
                placement="left"
                onConfirm={() => handleConfirmDelete(value.id)}
                title="Bạn có muốn xóa?"
                okText="Có"
                cancelText="Không">
                <div>Xóa</div>
              </Popconfirm>
            </Option>
          </Select>
        </>
      ),
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter)
  }, [filter])
  useEffect(() => {
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER) {
      setFilter({
        ...filter,
        provider_id: itemUser.getIn(['providersInfo', 'id']),
      })
    }
  }, [itemUser])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  return (
    <>
      <TableList
        columns={idProvider ? columnsProvider : columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

SeatMapManagementContainer.propTypes = {
  getItems: PropTypes.func,
  items: PropTypes.object,
  history: PropTypes.any,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
  totalCount: PropTypes.number,
  showLoading: PropTypes.bool,
  deleteItem: PropTypes.func,
  clearItems: PropTypes.func,
  search: PropTypes.string,
  idProvider: PropTypes.number,
  itemUser: PropTypes.object,
}

const mapStateToProps = state => ({
  items: SeatMapManagementSelectors.getItems(state),
  totalCount: SeatMapManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
  filter: SeatMapManagementSelectors.getFilter(state),
  itemUser: UserSelectors.getUserData(state),
})

const mapDispatchToProps = dispatch => ({
  getItems: filter =>
    dispatch(SeatMapManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(SeatMapManagementActions.clearItems()),
  deleteItem: id => dispatch(SeatMapManagementActions.deleteItemRequest(id)),
  setFilter: filter => dispatch(SeatMapManagementActions.setFilter(filter)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(SeatMapManagementContainer)
