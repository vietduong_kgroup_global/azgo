import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { withRouter } from 'react-router-dom'
import { Popconfirm, Select } from 'antd'
import { compose } from 'redux'
import { ScheduleManagementActions } from 'Stores/Intercity/ScheduleManagement/Actions'
import { ScheduleManagementSelectors } from 'Stores/Intercity/ScheduleManagement/Selectors'
import { Button } from 'reactstrap'
import moment from 'moment'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as configsEnum from 'Utils/enum'

const ScheduleContainer = ({
  items,
  history,
  getItems,
  filter,
  setFilter,
  clearItems,
  search,
  totalCount,
  showLoading,
  deleteItem,
  providerDetail,
  managementPage,
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  const handleOpenUpdate = id => {
    history.push('/bus/schedule/edit/' + id)
  }
  const handleOpenTicket = id => {
    history.push('/bus/schedule/ticket/' + id)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  const handleToVehicle = id => {
    history.push('/bus/vehicle/edit/' + id)
  }
  const handleToRoute = id => {
    history.push('/bus/route/edit/' + id)
  }
  const { Option } = Select
  const columns = [
    {
      title: 'Ngày',
      dataIndex: 'startDate',
      render: text => <div>{moment(text).format('DD-MM-YYYY')}</div>,
    },
    {
      title: 'Giờ khởi hành',
      dataIndex: 'startHour',
    },
    {
      title: 'Xe',
      dataIndex: 'vehicle',
      render: value => (
        <div onClick={() => handleToVehicle(value.id)}>
          {value ? value.licensePlates : null}
        </div>
      ),
    },
    {
      title: !providerDetail ? 'Nhà xe' : null,
      dataIndex: !providerDetail ? 'provider.providerName' : null,
    },
    {
      title: 'Tuyến',
      dataIndex: 'route',
      render: value => (
        <div onClick={() => handleToRoute(value.id)}>
          {value ? value.name : null}
        </div>
      ),
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      render: value => {
        const result = configsEnum.MANAGEMENT_STATUS_SCHEDULE.find(item => {
          return item.key === value
        })
        if (!result) return ''
        return (
          <Button size="sm" color={result.color}>
            {result.label}
          </Button>
        )
      },
    },
    {
      title: 'Số ghế trống',
      dataIndex: 'availableSeat',
    },
    {
      title: 'Sơ đồ vé',
      render: value => {
        return (
          <Button
            size="sm"
            color="success"
            onClick={() => handleOpenTicket(value.id)}>
            <i className="fa fa-th" />
            &nbsp; Sơ đồ vé
          </Button>
        )
      },
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value_) => (
        <>
          <Select
            labelInValue
            style={{ width: 120 }}
            value={[]}
            placeholder="Chọn">
            <Option value="1" onClick={() => handleOpenUpdate(value_.id)}>
              Sửa
            </Option>
            <Option value="2">
              <Popconfirm
                placement="left"
                onConfirm={() => handleConfirmDelete(value_.id)}
                title="Bạn có muốn xóa?"
                okText="Có"
                cancelText="Không">
                <div>Xóa</div>
              </Popconfirm>
            </Option>
          </Select>
        </>
      ),
    },
  ]
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    if (managementPage) {
      getItems(filter)
    }
    if (providerDetail) {
      getItems(filter)
    }
  }, [getItems, filter, clearItems])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

ScheduleContainer.propTypes = {
  items: PropTypes.object,
  history: PropTypes.any,
  getItems: PropTypes.func,
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
  clearItems: PropTypes.func.isRequired,
  search: PropTypes.string,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  deleteItem: PropTypes.func,
  providerDetail: PropTypes.number,
  managementPage: PropTypes.bool,
}

const mapStateToProps = state => ({
  items: ScheduleManagementSelectors.getItems(state),
  totalCount: ScheduleManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

const mapDispatchToProps = dispatch => ({
  getItems: filter =>
    dispatch(ScheduleManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(ScheduleManagementActions.clearItems()),
  deleteItem: id => dispatch(ScheduleManagementActions.deleteItemRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(ScheduleContainer)
