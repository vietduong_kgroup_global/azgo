import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { withRouter } from 'react-router-dom'
import { Modal, Popconfirm } from 'antd'
import { Button } from 'reactstrap'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { VehicleTypeManagementSelectors } from 'Stores/Intercity/VehicleTypeManagement/Selectors'
import { VehicleTypeManagementActions } from 'Stores/Intercity/VehicleTypeManagement/Actions'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import PropTypes from 'prop-types'
import FormVehicleType from 'components/organisms/Intercity/FormVehicleType'

const VehicleTypeManagementContainer = ({
  items,
  filter,
  setFilter,
  getItems,
  totalCount,
  showLoading,
  showPopUp,
  deleteItem,
  clearItems,
  search,
  visible,
  setVisible,
  idProvider,
  managementPage,
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const [id, setId] = useState(null)
  const showModal = id => {
    setVisible(true)
    setId(id)
  }

  const handleCancel = () => {
    setVisible(false)
    setId(null)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])

  useEffect(() => {
    if (idProvider || managementPage) {
      getItems(filter)
    }
  }, [filter])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  useEffect(() => {
    if (showPopUp) {
      handleCancel()
    }
  }, [showPopUp])
  const columns = [
    {
      title: 'Loại xe',
      dataIndex: 'name',
    },
    {
      title: 'Số ghế',
      dataIndex: 'totalSeat',
    },
    {
      title: 'Nhà xe',
      dataIndex: 'providersInfo.providerName',
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      width: 140,
      render: (key, value) => (
        <>
          <Button
            color="light"
            size="sm"
            className="btn-sm-ct"
            style={{ marginRight: '5px' }}
            onClick={() => showModal(value.id)}>
            <i className="fa fa-pencil" />
            &nbsp;Sửa
          </Button>
          {/*<Popconfirm*/}
          {/*  placement="top"*/}
          {/*  onConfirm={() => handleConfirmDelete(value.id)}*/}
          {/*  title={*/}
          {/*    'Bạn có chắc chắn muốn xóa loại xe ' +*/}
          {/*    `${value.name ? value.name : ''}`*/}
          {/*  }*/}
          {/*  okText="Có"*/}
          {/*  cancelText="Không">*/}
          {/*  <Button color="danger" size="sm" className="btn-sm-ct">*/}
          {/*    <i className="fa fa-trash-o" />*/}
          {/*    &nbsp;Xóa*/}
          {/*  </Button>*/}
          {/*</Popconfirm>*/}
        </>
      ),
    },
  ]
  const columnsProvider = [
    {
      title: 'Loại xe',
      dataIndex: 'name',
    },
    {
      title: 'Số ghế',
      dataIndex: 'totalSeat',
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      width: 140,
      render: (key, value) => (
        <>
          <Button
            color="light"
            size="sm"
            className="btn-sm-ct"
            style={{ marginRight: '5px' }}
            onClick={() => showModal(value.id)}>
            <i className="fa fa-pencil" />
            &nbsp;Sửa
          </Button>
          <Popconfirm
            placement="top"
            onConfirm={() => handleConfirmDelete(value.id)}
            title={
              'Bạn có chắc chắn muốn xóa loại xe ' +
              `${value.name ? value.name : ''}`
            }
            okText="Có"
            cancelText="Không">
            <Button color="danger" size="sm" className="btn-sm-ct">
              <i className="fa fa-trash-o" />
              &nbsp;Xóa
            </Button>
          </Popconfirm>
        </>
      ),
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  return (
    <>
      <TableList
        columns={idProvider ? columnsProvider : columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
      {visible && (
        <Modal
          title={id ? 'Chỉnh sửa loại xe' : 'Thêm loại xe'}
          visible={visible}
          footer={null}
          onCancel={handleCancel}>
          <FormVehicleType
            visible={visible}
            setVisible={setVisible}
            providerDetail={idProvider}
            id={id || null} />
        </Modal>
      )}
    </>
  )
}

const mapStateToProps = state => ({
  items: VehicleTypeManagementSelectors.getItems(state),
  filter: VehicleTypeManagementSelectors.getFilter(state),
  totalCount: VehicleTypeManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
  showPopUp: LoadingSelectors.getShowPopUp(state),
})

VehicleTypeManagementContainer.propTypes = {
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  deleteItem: PropTypes.func,
  clearItems: PropTypes.func.isRequired,
  search: PropTypes.string,
  visible: PropTypes.any,
  setVisible: PropTypes.func,
  showPopUp: PropTypes.any,
  idProvider: PropTypes.number,
  managementPage: PropTypes.bool,
}
const mapDispatchToProps = dispatch => ({
  getItems: filter =>
    dispatch(VehicleTypeManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(VehicleTypeManagementActions.clearItems()),
  deleteItem: id =>
    dispatch(VehicleTypeManagementActions.deleteItemRequest(id)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(VehicleTypeManagementContainer)
