import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { Button } from 'reactstrap'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'redux'
import 'Stores/Intercity/RouteManagement/Reducers'
import 'Stores/Intercity/RouteManagement/Sagas'
import { RouteManagementActions } from 'Stores/Intercity/RouteManagement/Actions'
import { RouteManagementSelectors } from 'Stores/Intercity/RouteManagement/Selectors'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { UserSelectors } from 'Stores/User/Selectors'
import PropTypes from 'prop-types'
import { formatPrice } from 'Utils/helper'
import * as config from 'Utils/enum'

const BudgetContainer = ({
  items,
  history,
  getItems,
  filter,
  setFilter,
  showLoading,
  totalCount,
  clearItems,
  search,
  itemUser,
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const handleOpenUpdate = id => {
    history.push('/bus/budget/' + id)
  }
  const columns = [
    {
      title: 'Tuyến',
      dataIndex: 'name',
    },
    {
      title: 'Nhà xe',
      dataIndex: 'provider.providerName',
    },
    {
      title: 'Giá gốc',
      dataIndex: 'basePrice',
      render: value => <div>{formatPrice(value)}</div>,
    },
    {
      title: 'Giá/Km',
      dataIndex: 'pricePerKm',
      render: value => <div>{formatPrice(value)}</div>,
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Button
            size="sm"
            color="primary"
            onClick={() => handleOpenUpdate(value.id)}>
            Sửa
          </Button>
        </>
      ),
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER) {
      setFilter({
        ...filter,
        provider_id: itemUser.getIn(['providersInfo', 'id']),
      })
    }
  }, [itemUser])
  useEffect(() => {
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER) {
      getItems(filter)
    } else {
      getItems(filter)
    }
  }, [filter])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  return (
    <>
      <TableList
        columns={columns}
        totalCount={totalCount}
        dataSource={items}
        pagination={pagination}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

BudgetContainer.propTypes = {
  items: PropTypes.object,
  getItems: PropTypes.func,
  filter: PropTypes.object.isRequired,
  history: PropTypes.any,
  setFilter: PropTypes.func.isRequired,
  showLoading: PropTypes.bool.isRequired,
  totalCount: PropTypes.number.isRequired,
  clearItems: PropTypes.func.isRequired,
  search: PropTypes.string,
  itemUser: PropTypes.object,
}

const mapStateToProps = state => ({
  items: RouteManagementSelectors.getItems(state),
  totalCount: RouteManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
  filter: RouteManagementSelectors.getFilter(state),
  itemUser: UserSelectors.getUserData(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(RouteManagementActions.setFilter(filter)),
  getItems: filter => dispatch(RouteManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(RouteManagementActions.clearItems()),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(BudgetContainer)
