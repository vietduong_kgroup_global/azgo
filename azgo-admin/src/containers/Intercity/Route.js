import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { Popconfirm, Select } from 'antd'
import 'Stores/Intercity/RouteManagement/Reducers'
import 'Stores/Intercity/RouteManagement/Sagas'
import { RouteManagementActions } from 'Stores/Intercity/RouteManagement/Actions'
import { RouteManagementSelectors } from 'Stores/Intercity/RouteManagement/Selectors'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { UserSelectors } from 'Stores/User/Selectors'
import PropTypes from 'prop-types'
import * as config from 'Utils/enum'
import { formatPrice } from 'Utils/helper'

const RouteContainer = ({
  items,
  history,
  getItems,
  filter,
  setFilter,
  showLoading,
  totalCount,
  deleteItem,
  clearItems,
  search,
  idProvider,
  managementPage,
  itemUser,
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const handleOpenUpdate = id => {
    history.push('/bus/route/edit/' + id)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  const { Option } = Select
  const columns = [
    {
      title: 'Tuyến',
      dataIndex: 'name',
    },
    {
      title: 'Nhà xe',
      dataIndex: 'provider.providerName',
    },
    {
      title: 'Điểm đi',
      dataIndex: 'startPoint.name',
    },
    {
      title: 'Điểm đến',
      dataIndex: 'endPoint.name',
    },
    {
      title: 'Điểm đón chính',
      dataIndex: 'mainStation',
    },
    {
      title: 'Điểm đón phụ',
      dataIndex: 'additionalStation',
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Select
            labelInValue
            style={{ width: 120 }}
            value={[]}
            placeholder="Chọn">
            <Option value="1" onClick={() => handleOpenUpdate(value.id)}>
              Sửa
            </Option>
            <Option value="2">
              <Popconfirm
                placement="left"
                onConfirm={() => handleConfirmDelete(value.id)}
                title={
                  'Bạn có chắc chắn muốn xóa ' +
                  `${value.name ? value.name : ''}`
                }
                okText="Có"
                cancelText="Không">
                <div>Xóa</div>
              </Popconfirm>
            </Option>
          </Select>
        </>
      ),
    },
  ]
  const columnsProvider = [
    {
      title: 'Tuyến',
      dataIndex: 'name',
    },
    {
      title: 'Điểm đi',
      dataIndex: 'startPoint.name',
    },
    {
      title: 'Điểm đến',
      dataIndex: 'endPoint.name',
    },
    {
      title: 'Giá vé',
      dataIndex: 'basePrice',
      render: value => <div>{formatPrice(value)}</div>,
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Select
            labelInValue
            style={{ width: 120 }}
            value={[]}
            placeholder="Chọn">
            <Option value="1" onClick={() => handleOpenUpdate(value.id)}>
              Sửa
            </Option>
            <Option value="2">
              <Popconfirm
                placement="left"
                onConfirm={() => handleConfirmDelete(value.id)}
                title={
                  'Bạn có chắc chắn muốn xóa ' +
                  `${value.name ? value.name : ''}`
                }
                okText="Có"
                cancelText="Không">
                <div>Xóa</div>
              </Popconfirm>
            </Option>
          </Select>
        </>
      ),
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    if (
      managementPage ||
      idProvider ||
      itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
    ) {
      getItems(filter)
    }
  }, [filter])
  useEffect(() => {
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER) {
      setFilter({
        ...filter,
        provider_id: itemUser.getIn(['providersInfo', 'id']),
      })
    }
  }, [itemUser])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  return (
    <>
      <TableList
        columns={idProvider ? columnsProvider : columns}
        totalCount={totalCount}
        dataSource={items}
        pagination={pagination}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

RouteContainer.propTypes = {
  items: PropTypes.object,
  getItems: PropTypes.func,
  filter: PropTypes.object,
  history: PropTypes.any,
  setFilter: PropTypes.func,
  showLoading: PropTypes.bool,
  totalCount: PropTypes.number,
  deleteItem: PropTypes.func,
  clearItems: PropTypes.func,
  search: PropTypes.string,
  idProvider: PropTypes.number,
  managementPage: PropTypes.bool,
  itemUser: PropTypes.object,
}

const mapStateToProps = state => ({
  items: RouteManagementSelectors.getItems(state),
  totalCount: RouteManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
  itemUser: UserSelectors.getUserData(state),
  filter: RouteManagementSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(RouteManagementActions.setFilter(filter)),
  getItems: filter => dispatch(RouteManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(RouteManagementActions.clearItems()),
  deleteItem: id => dispatch(RouteManagementActions.deleteItemRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(RouteContainer)
