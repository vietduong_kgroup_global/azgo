import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { Icon, Popconfirm, Select } from 'antd'
import { Link, withRouter } from 'react-router-dom'
import * as configsEnum from 'Utils/enum'
import PropTypes from 'prop-types'
import { ProviderManagementSelectors } from 'Stores/ProviderManagement/Selectors'
import { ProviderManagementActions } from 'Stores/ProviderManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { Config } from 'Config'
import { formatPhone } from 'Utils/helper'

const ProviderManagementContainer = ({
  items,
  getItems,
  history,
  filter,
  setFilter,
  clearItems,
  showLoading,
  totalCount,
  deleteItem,
  search,
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const StyleLogo = {
    maxWidth: '50px',
    width: '50px',
    display: 'flex',
    alignItems: 'center',
    padding: '5px',
    justifyContent: 'center',
    border: '1px solid #e8e8e8',
  }
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter)
  }, [getItems, clearItems, filter])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  const handleOpenUpdate = id => {
    history.push('/provider-management/edit/' + id)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  const { Option } = Select
  const columns = [
    {
      title: 'Hình ảnh',
      dataIndex: 'providersInfo.imageProfile',
      render: (key, value) => {
        return (
          <div style={StyleLogo}>
            {value.providersInfo.imageProfile && (
              <Link to={`/provider-management/detail/${value.userId}`}>
                <img
                  src={
                    Config.DEV_URL.IMAGE +
                    '/' +
                    value.providersInfo.imageProfile.filePath +
                    '/' +
                    value.providersInfo.imageProfile.fileName
                  }
                />
              </Link>
            )}
          </div>
        )
      },
    },
    {
      title: 'Nhà xe',
      render: value => {
        return (
          <Link to={`/provider-management/detail/${value.userId}`}>
            {value.providersInfo && value.providersInfo.providerName}
          </Link>
        )
      },
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'Tình trạng',
      dataIndex: 'status',
      render: value => {
        const result = configsEnum.MANAGEMENT_STATUS_ARR.find(item => {
          return item.key === value
        })
        if (!result) return ''
        return (
          <span className={`btn-status btn-status-${result.label}`}>
            {result.label}
          </span>
        )
      },
    },
    {
      title: 'Số lượng xe',
      dataIndex: 'providersInfo.totalVehicles',
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => {
        return (
          <>
            <Select
              labelInValue
              style={{ width: 120 }}
              value={[]}
              placeholder="Chọn">
              <Option value="1" onClick={() => handleOpenUpdate(value.userId)}>
                Sửa
              </Option>
              <Option value="2">
                <Popconfirm
                  placement="left"
                  onConfirm={() => handleConfirmDelete(value.userId)}
                  icon={
                    <Icon type="question-circle-o" style={{ color: 'red' }} />
                  }
                  title={
                    'Bạn có chắc chắn muốn xóa nhà xe ' +
                    value.providersInfo.providerName
                  }
                  okText="Có"
                  cancelText="Không">
                  <div>Xóa</div>
                </Popconfirm>
              </Option>
            </Select>
          </>
        )
      },
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

const mapStateToProps = state => ({
  items: ProviderManagementSelectors.getItems(state),
  totalCount: ProviderManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

ProviderManagementContainer.propTypes = {
  filter: PropTypes.object.isRequired,
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  history: PropTypes.any,
  setFilter: PropTypes.func.isRequired,
  clearItems: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  deleteItem: PropTypes.func,
  search: PropTypes.string,
}

const mapDispatchToProps = dispatch => ({
  getItems: filter =>
    dispatch(ProviderManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(ProviderManagementActions.clearItems()),
  deleteItem: id => dispatch(ProviderManagementActions.deleteItemRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(ProviderManagementContainer)
