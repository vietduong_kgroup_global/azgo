import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { Icon, Popconfirm, Select } from 'antd'
import { withRouter } from 'react-router-dom'
import 'Stores/DriverManagement/Reducers'
import 'Stores/DriverManagement/Sagas'
import { DriverManagementSelectors } from 'Stores/DriverManagement/Selectors'
import { DriverManagementActions } from 'Stores/DriverManagement/Actions'
import { UserSelectors } from 'Stores/User/Selectors'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import PropTypes from 'prop-types'
import * as config from 'Utils/enum'
import { formatPhone } from 'Utils/helper'

const DriverManagementContainer = ({
  items,
  getItems,
  history,
  filter,
  setFilter,
  clearItems,
  showLoading,
  totalCount,
  deleteItem,
  search,
  itemUser,
  idProvider,
  managementPage,
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const handleOpenDetail = id => {
    history.push(`/driver/bus/detail/` + id)
  }
  const handleOpenUpdate = id => {
    history.push('/driver/bus/edit/' + id)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  const { Option } = Select
  const columns = [
    {
      title: 'Họ và tên',
      key: 'fullName',
      render: value => {
        return (
          <div
            onClick={() => handleOpenDetail(value.userId)}
            className="link-list">
            {value.driverProfile.fullName}
          </div>
        )
      },
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'Tình trạng',
      dataIndex: 'status',
      render: value => {
        const result = config.MANAGEMENT_STATUS_ARR.find(item => {
          return item.key === value
        })
        if (!result) return ''
        return (
          <span className={`btn-status btn-status-${result.label}`}>
            {result.label}
          </span>
        )
      },
    },
    {
      title: 'Nhà xe',
      dataIndex: 'driverProfile.providersInfoEntity.providerName',
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Select
            labelInValue
            style={{ width: 120 }}
            value={[]}
            placeholder="Chọn">
            <Option value="1" onClick={() => handleOpenUpdate(value.userId)}>
              Sửa
            </Option>
            <Option value="2">
              <Popconfirm
                placement="left"
                onConfirm={() => handleConfirmDelete(value.userId)}
                icon={
                  <Icon type="question-circle-o" style={{ color: 'red' }} />
                }
                title="Bạn có chắc chắn muốn xóa tài xế"
                okText="Có"
                cancelText="Không">
                <div>Xóa</div>
              </Popconfirm>
            </Option>
          </Select>
        </>
      ),
    },
  ]
  const columnsProvider = [
    {
      title: 'Họ và tên',
      key: 'fullName',
      render: value => {
        return (
          <div
            onClick={() => handleOpenDetail(value.userId)}
            className="link-list">
            {value.driverProfile.fullName}
          </div>
        )
      },
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
    },
    {
      title: 'Tình trạng',
      dataIndex: 'status',
      render: value => {
        const result = config.MANAGEMENT_STATUS_ARR.find(item => {
          return item.key === value
        })
        if (!result) return ''
        return (
          <span className={`btn-status btn-status-${result.label}`}>
            {result.label}
          </span>
        )
      },
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Select
            labelInValue
            style={{ width: 120 }}
            value={[]}
            placeholder="Chọn">
            <Option value="1" onClick={() => handleOpenUpdate(value.userId)}>
              Sửa
            </Option>
            <Option value="2">
              <Popconfirm
                placement="left"
                onConfirm={() => handleConfirmDelete(value.userId)}
                icon={
                  <Icon type="question-circle-o" style={{ color: 'red' }} />
                }
                title="Bạn có chắc chắn muốn xóa tài xế"
                okText="Có"
                cancelText="Không">
                <div>Xóa</div>
              </Popconfirm>
            </Option>
          </Select>
        </>
      ),
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    if (
      managementPage ||
      idProvider ||
      itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER
    ) {
      getItems(filter)
    }
  }, [filter])
  useEffect(() => {
    if (itemUser.get('type') === config.MANAGEMENT_TYPE_USER.PROVIDER) {
      setFilter({
        ...filter,
        provider_id: itemUser.getIn(['providersInfo', 'id']),
      })
    }
  }, [itemUser])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  return (
    <>
      <TableList
        columns={idProvider ? columnsProvider : columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

const mapStateToProps = state => ({
  items: DriverManagementSelectors.getItems(state),
  totalCount: DriverManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
  filter: DriverManagementSelectors.getFilter(state),
  itemUser: UserSelectors.getUserData(state),
})

DriverManagementContainer.propTypes = {
  filter: PropTypes.object,
  getItems: PropTypes.func,
  items: PropTypes.object,
  history: PropTypes.any,
  setFilter: PropTypes.func,
  clearItems: PropTypes.func,
  totalCount: PropTypes.number,
  showLoading: PropTypes.bool,
  deleteItem: PropTypes.func,
  search: PropTypes.string,
  idProvider: PropTypes.number,
  managementPage: PropTypes.bool,
  itemUser: PropTypes.object,
}

const mapDispatchToProps = dispatch => ({
  getItems: filter => dispatch(DriverManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(DriverManagementActions.clearItems()),
  deleteItem: id => dispatch(DriverManagementActions.deleteItemRequest(id)),
  setFilter: filter => dispatch(DriverManagementActions.setFilter(filter)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(DriverManagementContainer)
