import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import { Modal, Popconfirm, Table } from 'antd'
import { Button } from 'reactstrap'
import { compose } from 'redux'
import { connect } from 'react-redux'
import 'Stores/Intercity/AreaManagement/Reducers'
import 'Stores/Intercity/AreaManagement/Sagas'
import { AreaManagementSelectors } from 'Stores/Intercity/AreaManagement/Selectors'
import { AreaManagementActions } from 'Stores/Intercity/AreaManagement/Actions'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import FormArea from 'components/organisms/FormArea'
import PropTypes from 'prop-types'

const AreaManagementContainer = ({
  items,
  getItems,
  deleteItem,
  visible,
  setVisible,
  showPopUp,
  filter,
  setFilter,
  search,
  clearItems,
  showLoadingList,
  history
}) => {
  const [item, setItem] = useState(null)
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const handleOpenUpdate = id => {
    history.push('/admin/tour/areaManagement/' + id)
  }
  const columns = [
    {
      title: 'STT',
      dataIndex: 'indexRow',
    },
    {
      title: 'Địa điểm',
      dataIndex: 'name',
    },
    // {
    //   title: 'Lat',
    //   dataIndex: 'coordinates.lat',
    // },
    // {
    //   title: 'Lng',
    //   dataIndex: 'coordinates.lng',
    // },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      width: 140,
      render: (key, value) => (
        <>
          <Button
            color="light"
            size="sm"
            className="btn-sm-ct"
            style={{ marginRight: '5px' }}
            onClick={() => handleOpenUpdate(value.uuid)}>
            <i className="fa fa-pencil" />
            &nbsp;Sửa
          </Button>
          <Popconfirm
            placement="top"
            onConfirm={() => handleConfirmDelete(value.id)}
            title={
              'Bạn có chắc chắn muốn xóa địa điểm ' +
              `${value.name ? value.name : ''}`
            }
            okText="Có"
            cancelText="Không">
            <Button color="danger" size="sm" className="btn-sm-ct">
              <i className="fa fa-trash-o" />
              &nbsp;Xóa
            </Button>
          </Popconfirm>
        </>
      ),
    },
  ]
  const handleCancel = () => {
    setVisible(false)
    setItem(null)
  }
  const showModal = value => {
    setVisible(true)
    setItem(value)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  useEffect(() => {
    setFilter({
      ...filter.toJS(),
      keyword: search,
      page: 1,
    })
  }, [search])
  useEffect(() => {
    if (showPopUp) {
      handleCancel()
    }
  }, [showPopUp])
  useEffect(() => {
    getItems(filter)
  }, [filter])
  useEffect(() => {
    setFilter({
      per_page: 10,
      page: 1,
      keyword: '',
    })
    return () => {
      clearItems()
    }
  }, [])
  const [listData, setListData] = useState([])
  useEffect(() => {
    if (items) {
      setListData([])
      let arr = []
      for (let i = 0; i < items.toJS().length; i++) {
        let item = {
          ...items.toJS()[i],
          indexRow: i + 1,
        }
        arr.push(item)
      }
      setListData(arr)
    }
  }, [items])
  return (
    <>
      <Table
        columns={columns}
        dataSource={listData}
        totalCount={items.size}
        pagination={pagination}
        loading={showLoadingList}
        rowKey="indexRow"
      />
      <Modal
        title={item ? 'Chỉnh sửa địa điểm' : 'Thêm địa điểm'}
        visible={visible}
        footer={null}
        className="modal-area"
        onCancel={handleCancel}>
        <FormArea item={item} visible={visible} />
      </Modal>
    </>
  )
}

const mapStateToProps = state => ({
  items: AreaManagementSelectors.getItems(state),
  showPopUp: LoadingSelectors.getShowPopUp(state),
  showLoadingList: LoadingSelectors.getLoadingList(state),
})

AreaManagementContainer.propTypes = {
  items: PropTypes.object,
  getItems: PropTypes.func,
  deleteItem: PropTypes.func,
  visible: PropTypes.bool,
  setVisible: PropTypes.func,
  showPopUp: PropTypes.bool,
  showLoadingList: PropTypes.bool,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
  search: PropTypes.string,
  clearItems: PropTypes.func,
  history: PropTypes.any,
}
const mapDispatchToProps = dispatch => ({
  getItems: values => dispatch(AreaManagementActions.getItemsRequest(values)),
  deleteItem: id => dispatch(AreaManagementActions.deleteItemRequest(id)),
  clearItems: () => dispatch(AreaManagementActions.clearItems()),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(AreaManagementContainer)
