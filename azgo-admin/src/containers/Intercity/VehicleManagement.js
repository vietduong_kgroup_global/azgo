import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { withRouter } from 'react-router-dom'
import { Modal, Popconfirm, Select } from 'antd'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import { VehicleManagementActions } from 'Stores/Intercity/VehicleManagement/Actions'
import { connect } from 'react-redux'
import { VehicleManagementSelectors } from 'Stores/Intercity/VehicleManagement/Selectors'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { Button } from 'reactstrap'
import FormVehicle from 'components/organisms/Intercity/FormVehicle'

const VehicleManagementContainer = ({
  items,
  filter,
  history,
  setFilter,
  getItems,
  totalCount,
  showLoading,
  showPopUp,
  deleteItem,
  clearItems,
  search,
  setVisible,
  idProvider,
  managementPage,
}) => {
  const handleOpenUpdate = id => {
    history.push('/bus/vehicle/' + id)
  }
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const [id, setId] = useState(null)
  const handleCancel = () => {
    setVisible(false)
    setId(null)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    if (managementPage) {
      getItems(filter)
    }
    if (idProvider) {
      getItems(filter)
    }
  }, [getItems, filter, clearItems])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  useEffect(() => {
    if (showPopUp) {
      handleCancel()
    }
  }, [showPopUp])
  const { Option } = Select
  const columns = [
    {
      title: 'Biển số xe',
      dataIndex: 'licensePlates',
    },
    {
      title: 'Loại xe',
      dataIndex: 'vehicleType.name',
    },
    {
      title: 'Sơ đồ ngồi',
      dataIndex: 'seatPattern.name',
    },
    {
      title: 'Nhà xe',
      dataIndex: 'provider.providerName',
    },
    {
      title: 'Chức năng',
      width: 140,
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Select
            labelInValue
            style={{ width: 120 }}
            value={[]}
            placeholder="Chọn">
            <Option value="1" onClick={() => handleOpenUpdate(value.id)}>
              Sửa
            </Option>
            <Option value="2">
              <Popconfirm
                placement="left"
                onConfirm={() => handleConfirmDelete(value.id)}
                title={
                  'Bạn có chắc chắn muốn xóa xe ' +
                  `${value.licensePlates ? value.licensePlates : ''}`
                }
                okText="Có"
                cancelText="Không">
                <div>Xóa</div>
              </Popconfirm>
            </Option>
          </Select>
        </>
      ),
    },
  ]
  const columnsProvider = [
    {
      title: 'Biển số xe',
      dataIndex: 'licensePlates',
    },
    {
      title: 'Loại xe',
      dataIndex: 'vehicleType.name',
    },
    {
      title: 'Sơ đồ ngồi',
      dataIndex: 'seatPattern.name',
    },
    {
      title: 'Chức năng',
      width: 140,
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Select
            labelInValue
            style={{ width: 120 }}
            value={[]}
            placeholder="Chọn">
            <Option value="1" onClick={() => handleOpenUpdate(value.id)}>
              Sửa
            </Option>
            <Option value="2">
              <Popconfirm
                placement="left"
                onConfirm={() => handleConfirmDelete(value.id)}
                title={
                  'Bạn có chắc chắn muốn xóa xe ' +
                  `${value.licensePlates ? value.licensePlates : ''}`
                }
                okText="Có"
                cancelText="Không">
                <div>Xóa</div>
              </Popconfirm>
            </Option>
          </Select>
        </>
      ),
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  return (
    <>
      <TableList
        columns={idProvider ? columnsProvider : columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

VehicleManagementContainer.propTypes = {
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  history: PropTypes.any,
  setFilter: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  deleteItem: PropTypes.func,
  clearItems: PropTypes.func.isRequired,
  search: PropTypes.string,
  visible: PropTypes.any,
  setVisible: PropTypes.func,
  showPopUp: PropTypes.any,
  idProvider: PropTypes.number,
  managementPage: PropTypes.bool,
}

const mapStateToProps = state => ({
  items: VehicleManagementSelectors.getItems(state),
  filter: VehicleManagementSelectors.getFilter(state),
  totalCount: VehicleManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
  showPopUp: LoadingSelectors.getShowPopUp(state),
})

const mapDispatchToProps = dispatch => ({
  getItems: filter =>
    dispatch(VehicleManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(VehicleManagementActions.clearItems()),
  deleteItem: id => dispatch(VehicleManagementActions.deleteItemRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(VehicleManagementContainer)
