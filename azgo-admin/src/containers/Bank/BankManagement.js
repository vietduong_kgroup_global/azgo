import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { Icon, Popconfirm, Select, Tag} from 'antd'
import { Button } from 'reactstrap'
import { Link, withRouter } from 'react-router-dom'
import * as configsEnum from 'Utils/enum'
import 'Stores/BankManagement/Reducers'
import 'Stores/BankManagement/Sagas'
import { BankManagementSelectors } from 'Stores/BankManagement/Selectors'
import { BankManagementActions } from 'Stores/BankManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import PropTypes from 'prop-types'
import { formatPhone } from 'Utils/helper'
import VerhicleList from 'containers/Driver/VerhicleList'
const TableDriverManagementContainer = ({
  items,
  getItems,
  history,
  filter,
  setFilter,
  clearItems,
  showLoading,
  totalCount,
  deleteItem,
  search,
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter)
  }, [getItems, clearItems, filter])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  const handleOpenUpdate = id => {
    history.push('/admin/content-management/bank/edit/' + id) 
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  const { Option } = Select
   const columns = [
    {
      title: 'Tên Tiếng Anh',
      key: 'enName',
      dataIndex: 'enName',
    },
    {
      title: 'Tên Tiếng Việt',
      key: 'vnName',
      dataIndex: 'vnName',
    },
    {
      title: 'Tên Viết tắt',
      key: 'shortName',
      dataIndex: 'shortName',
    },
    {
      title: 'Mã ngân hàng',
      key: 'bankCode',
      dataIndex: 'bankCode',
    },
    {
      title: 'Chức năng',
      width: 140,
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Button
            color="light"
            size="sm"
            className="btn-sm-ct"
            style={{ marginRight: '5px' }}
            onClick={() =>  handleOpenUpdate(value.uuid)}>
            <i className="fa fa-pencil" />
            &nbsp;Sửa
          </Button>
          <Popconfirm
            placement="top"
            onConfirm={() => handleConfirmDelete(value.uuid)}
            title={
              'Bạn có chắc chắn muốn xóa ngân hàng ' +
              `${value.shortName ? value.shortName : ''}`
            }
            okText="Có"
            cancelText="Không">
            <Button color="danger" size="sm" className="btn-sm-ct">
              <i className="fa fa-trash-o" />
              &nbsp;Xóa
            </Button>
          </Popconfirm>
        </>
      ),
    },
  ] 
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  } 
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
        // expandedRowRender={record => <VerhicleList verhicleList = {record.vehicles}/>}
      />
    </>
  )
}

const mapStateToProps = state => ({
  items: BankManagementSelectors.getItems(state),
  totalCount: BankManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

TableDriverManagementContainer.propTypes = {
  filter: PropTypes.object.isRequired,
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  history: PropTypes.any,
  setFilter: PropTypes.func.isRequired,
  clearItems: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  deleteItem: PropTypes.func,
  search: PropTypes.string,
}

const mapDispatchToProps = dispatch => ({
    getItems: (filter) => dispatch(BankManagementActions.getItemsRequest(filter)),
    clearItems: () => dispatch(BankManagementActions.clearItems()),
    deleteItem:  (values) => dispatch(BankManagementActions.deleteItemRequest(values)),
 })

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(TableDriverManagementContainer)
