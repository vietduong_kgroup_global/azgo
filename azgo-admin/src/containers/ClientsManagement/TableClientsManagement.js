import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { Popconfirm, Select, Icon } from 'antd'
import PropTypes from 'prop-types'
import { ClientManagementSelectors } from 'Stores/ClientManagement/Selectors'
import { ClientManagementActions } from 'Stores/ClientManagement/Actions'
import * as configsEnum from 'Utils/enum'
import { Link, withRouter } from 'react-router-dom'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { formatPhone } from 'Utils/helper'

const TableClientsManagementContainer = ({
  items,
  history,
  deleteItem,
  filter,
  setFilter,
  getItems,
  totalCount,
  showLoading,
  clearItems,
  search,
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter)
  }, [getItems, clearItems, filter])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  const handleOpenUpdate = id => {
    history.push('/admin/clients-management/edit/' + id)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  const { Option } = Select
  const columns = [
    {
      title: 'Họ và tên',
      key: 'fullName',
      render: value => {
        return (
          <Link to={`/admin/clients-management/detail/${value.userId}`}>
            {value.customerProfile && value.customerProfile.fullName}
          </Link>
        )
      },
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
      // render: value => {
      //   return formatPhone(value.countryCode, value.phone)
      // },
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'Tình trạng',
      dataIndex: 'status',
      render: value => {
        const result = configsEnum.MANAGEMENT_STATUS_ARR.find(item => {
          return item.key === value
        })
        if (!result) return ''
        return (
          <span className={`btn-status btn-status-${result.label}`}>
            {result.label}
          </span>
        )
      },
    },
    {
      title: 'Giới tính',
      dataIndex: 'customerProfile.gender',
      render: value => {
        const result = configsEnum.MANAGEMENT_GENDER_ARR.find(item => {
          return item.key === value
        })
        if (!result) return ''
        return (
          <span className={`btn-status btn-status-${result.label}`}>
            {result.label}
          </span>
        )
      },
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Select
            labelInValue
            style={{ width: 120 }}
            value={[]}
            className="select-custom"
            placeholder="Chọn">
            <Option value="1" onClick={() => handleOpenUpdate(value.userId)}>
              Sửa
            </Option>
            <Option value="2">
              <Popconfirm
                placement="left"
                onConfirm={() => handleConfirmDelete(value.userId)}
                icon={
                  <Icon type="question-circle-o" style={{ color: 'red' }} />
                }
                title={
                  'Bạn có chắc chắn muốn xóa khách hàng ' +
                  `${
                    value.customerProfile ? value.customerProfile.fullName : ''
                  }`
                }
                okText="Có"
                cancelText="Không">
                <div>Xóa</div>
              </Popconfirm>
            </Option>
          </Select>
        </>
      ),
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

const mapStateToProps = state => ({
  items: ClientManagementSelectors.getItems(state),
  totalCount: ClientManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

TableClientsManagementContainer.propTypes = {
  deleteItem: PropTypes.func,
  filter: PropTypes.object.isRequired,
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  history: PropTypes.any,
  setFilter: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  clearItems: PropTypes.func.isRequired,
  search: PropTypes.string,
}

const mapDispatchToProps = dispatch => ({
  getItems: filter => dispatch(ClientManagementActions.getItemsRequest(filter)),
  deleteItem: id => dispatch(ClientManagementActions.deleteItemRequest(id)),
  clearItems: () => dispatch(ClientManagementActions.clearItems()),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(TableClientsManagementContainer)
