import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { withRouter } from 'react-router-dom'
import { Popconfirm, Select } from 'antd'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import { VehicleManagementActions } from 'Stores/BikeCar/VehicleManagement/Actions'
import { connect } from 'react-redux'
import { VehicleManagementSelectors } from 'Stores/BikeCar/VehicleManagement/Selectors'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import * as configsEnum from '../../Utils/enum'

const VehicleManagementContainer = ({
  items,
  history,
  filter,
  setFilter,
  getItems,
  totalCount,
  showLoading,
  deleteItem,
  clearItems,
  search,
}) => {
  const handleOpenUpdate = id => {
    history.push('/admin/bike-car/vehicle/edit/' + id)
  }
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  useEffect(() => {
    getItems(filter)
  }, [getItems, filter, clearItems])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  const { Option } = Select
  const columns = [
    {
      title: 'Biển số xe',
      dataIndex: 'licensePlates',
    },
    {
      title: 'Tài xế',
      dataIndex: 'users',
      render: value => {
        return (
          <>
            {value.map(item => (
              <span key={item.userId}>{item.driverProfile.fullName}</span>
            ))}
          </>
        )
      },
    },
    {
      title: 'Loại xe',
      dataIndex: 'vehicleType.vehicleGroupId',
      render: record => (
        <>
          <span>
            {record === configsEnum.VEHICLE_TYPE.BIKE ? 'Xe máy' : 'Ôtô'}
          </span>
        </>
      ),
    },
    {
      title: 'Tên loại xe',
      dataIndex: 'vehicleType.name',
    },
    {
      title: 'Hãng xe',
      dataIndex: 'vehicleBrand.name',
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      render: value => {
        const result = configsEnum.MANAGEMENT_STATUS_ARR.find(item => {
          return item.key === value
        })
        if (!result) return ''
        return (
          <span className={`btn-status btn-status-${result.label}`}>
            {result.label}
          </span>
        )
      },
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Select
            labelInValue
            style={{ width: 120 }}
            value={[]}
            placeholder="Chọn">
            <Option value="1" onClick={() => handleOpenUpdate(value.id)}>
              Sửa
            </Option>
            <Option value="2">
              <Popconfirm
                placement="left"
                onConfirm={() => handleConfirmDelete(value.id)}
                title="Bạn có muốn xóa?"
                okText="Có"
                cancelText="Không">
                <div>Xóa</div>
              </Popconfirm>
            </Option>
          </Select>
        </>
      ),
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

VehicleManagementContainer.propTypes = {
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  history: PropTypes.any,
  filter: PropTypes.object.isRequired,
  setFilter: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  deleteItem: PropTypes.func,
  clearItems: PropTypes.func.isRequired,
  search: PropTypes.string,
}

const mapStateToProps = state => ({
  items: VehicleManagementSelectors.getItems(state),
  filter: VehicleManagementSelectors.getFilter(state),
  totalCount: VehicleManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

const mapDispatchToProps = dispatch => ({
  getItems: filter =>
    dispatch(VehicleManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(VehicleManagementActions.clearItems()),
  deleteItem: id => dispatch(VehicleManagementActions.deleteItemRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(VehicleManagementContainer)
