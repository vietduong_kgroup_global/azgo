import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { Link, withRouter } from 'react-router-dom'
import { Tooltip } from 'antd'
import { Button } from 'reactstrap'
import { compose } from 'redux'
import * as configsEnum from 'Utils/enum'
import 'Stores/BikeCar/TripManagement/Reducers'
import 'Stores/BikeCar/TripManagement/Sagas'
import { TripManagementSelectors } from 'Stores/BikeCar/TripManagement/Selectors'
import { TripManagementActions } from 'Stores/BikeCar/TripManagement/Actions'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { LoadingSelectors } from 'Stores/Loading/Selectors'

const TripManagementContainer = ({
  items,
  history,
  clearItems,
  getItems,
  setFilter,
  filter,
  totalCount,
  showLoading,
  search,
}) => {
  const columns = [
    {
      title: 'Mã chuyến',
      dataIndex: 'id',
      width: 100,
      render: (key, value) => (
        <>
          <Link to={'/bike-car/trip-management/' + value.id}>
            <span>{value.id}</span>
          </Link>
        </>
      ),
    },
    {
      title: 'Tài xế',
      dataIndex: 'driverProfile.fullName',
    },
    {
      title: 'Loại xe',
      dataIndex: 'driverProfile.users.vehicles.0.vehicleType.name',
    },
    {
      title: 'Biển số xe',
      dataIndex: 'driverProfile.users.vehicles.0.licensePlates',
    },
    {
      title: 'Khách hàng',
      dataIndex: 'customerProfile.fullName',
    },
    {
      title: 'Điểm đi',
      dataIndex: 'routeInfo.startPosition.address',
      width: 250,
      render: value => {
        return (
          <Tooltip placement="topLeft" title={value}>
            <span className="overflow-text">{value}</span>
          </Tooltip>
        )
      },
    },
    {
      title: 'Điểm đến',
      dataIndex: 'routeInfo.endPosition.address',
      width: 250,
      render: value => {
        return (
          <Tooltip placement="topLeft" title={value}>
            <span className="overflow-text">{value}</span>
          </Tooltip>
        )
      },
    },
    {
      title: 'Tình trạng',
      dataIndex: 'status',
      render: value => {
        const result = configsEnum.MANAGEMENT_STATUS_TRIP_ARR.find(item => {
          return item.key === value
        })
        if (!result) return ''
        return (
          <Button color={result.color} size="sm">
            {result.label}
          </Button>
        )
      },
    },
  ]
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  useEffect(() => {
    getItems(filter)
  }, [getItems, filter, clearItems])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        totalCount={totalCount}
        pagination={pagination}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

TripManagementContainer.propTypes = {
  clearItems: PropTypes.func,
  getItems: PropTypes.func,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
  items: PropTypes.object,
  totalCount: PropTypes.number,
  showLoading: PropTypes.bool,
  search: PropTypes.string,
}

const mapStateToProps = state => ({
  filter: TripManagementSelectors.getFilter(state),
  items: TripManagementSelectors.getItems(state),
  totalCount: TripManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

const mapDispatchToProps = dispatch => ({
  getItems: filter => dispatch(TripManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(TripManagementActions.clearItems()),
  setFilter: filter => dispatch(TripManagementActions.setFilter(filter)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(TripManagementContainer)
