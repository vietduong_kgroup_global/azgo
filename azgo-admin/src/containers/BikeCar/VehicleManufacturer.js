import React, { useEffect, useState } from 'react'
import CardVehicleManufacturer from 'components/organisms/BikeCar/CardVehicleManufacturer'
import PropTypes from 'prop-types'
import { BrandVehicleManagementSelectors } from 'Stores/BrandVehicleManagement/Selectors'
import { BrandVehicleManagementActions } from 'Stores/BrandVehicleManagement/Actions'
import { withRouter } from 'react-router-dom'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { connect } from 'react-redux'
import { compose } from 'redux'
import {Row} from "reactstrap";

const CardVehicleManufacturerContainer = ({
  items,
  history,
  filter,
  setFilter,
  getItems,
  totalCount,
  showLoading,
  clearItems,
  search,
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter)
  }, [getItems, clearItems, filter])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  return (
    <>
      <CardVehicleManufacturer dataSource={items} />
      {
        totalCount === 0 && (
          <div>Không có kết quả tìm kiếm</div>
        )
      }
    </>
  )
}

CardVehicleManufacturerContainer.propTypes = {
  filter: PropTypes.object.isRequired,
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  history: PropTypes.any,
  setFilter: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  clearItems: PropTypes.func.isRequired,
  search: PropTypes.string,
}
const mapStateToProps = state => ({
  items: BrandVehicleManagementSelectors.getItems(state),
  totalCount: BrandVehicleManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})
const mapDispatchToProps = dispatch => ({
  getItems: filter =>
    dispatch(BrandVehicleManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(BrandVehicleManagementActions.clearItems()),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(
  withConnect,
  withRouter
)(CardVehicleManufacturerContainer)
