import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { withRouter, Link } from 'react-router-dom'
import * as configsEnum from 'Utils/enum'
import { Select, Popconfirm, Icon } from 'antd'
import { formatPhone } from 'Utils/helper'
import { UserManagementSelectors } from 'Stores/UserManagement/Selectors'
import { UserManagementActions } from 'Stores/UserManagement/Actions'
import TableList from 'components/organisms/TableList'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import {MANAGEMENT_TYPE_USER_ARR} from 'Utils/enum'
const TableUserManagementContainer = ({
  deleteItem,
  getItems,
  items,
  history,
  filter,
  setFilter,
  totalCount,
  showLoading,
  clearItems,
  search,
  // getUserType,
  // userType
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter) 
    // getUserType()
  }, [getItems, clearItems, filter])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])

  const handleOpenUpdate = id => {
    history.push('/admin/internal-management/edit/' + id)
  }
  const handleConfirmDelete = id => { 
    deleteItem(id)
  } 
  const { Option } = Select
  const columns = [
    // {
    //   title: 'Email',
    //   key: 'email',
    //   dataIndex: 'email',
    //   render: value => {
    //     return (
    //       // <Link to={`/admin/internal-management/detail/${value.userId}`}>
    //         value.adminProfile && value.adminProfile.fullName
    //       // </Link>
    //     )
    //   },
    // },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      render: value => {
        const result = configsEnum.MANAGEMENT_STATUS_ARR.find(item => {
          return item.key === value
        })
        if (!result) return ''
        return (
          <span className={`btn-status btn-status-${result.label}`}>
            {result.label}
          </span>
        )
      },
    },
    {
      title: 'Quyền',
      dataIndex: 'roles',
      render: value => { 
        return(
          <>
          {value && value.map((e) => {
          const result = MANAGEMENT_TYPE_USER_ARR.find(item => {
            return item.key === e
          })
          if (!result) return ''
          return (
            <span className={`btn-status btn-status-${result.key}`}><div> </div>
               {result.label}
            </span>
          )
        }) }
        </>
      )
      },
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => {
        return (
          <>
            <Select
              labelInValue
              style={{ width: 100 }}
              value={[]}
              placeholder="Chọn">
              {/* <Option value="1" onClick={() => handleOpenUpdate(value.userId)}>
                Sửa
              </Option> */}
              <Option value="2">
                <Popconfirm
                  placement="left"
                  onConfirm={() => handleConfirmDelete(value.userId)}
                  icon={
                    <Icon type="question-circle-o" style={{ color: 'red' }} />
                  }
                  title={'Bạn có chắc chắn muốn xóa tài khoản'}
                  okText="Có"
                  cancelText="Không">
                  <div>Xóa</div>
                </Popconfirm>
              </Option>
            </Select>
          </>
        )
      },
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  } 
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        pagination={pagination}
        handleTableChange={handleTableChange}
        showLoading={showLoading}
        totalCount={totalCount}
      />
    </>
  )
}

const mapStateToProps = state => ({
  items: UserManagementSelectors.getItems(state),
  totalCount: UserManagementSelectors.getTotalCount(state),
  // userType: UserManagementSelectors.getUserType(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

TableUserManagementContainer.propTypes = {
  deleteItem: PropTypes.func,
  filter: PropTypes.object,
  // userType: PropTypes.object,
  getItems: PropTypes.func,
  // getUserType: PropTypes.func,
  items: PropTypes.object,
  history: PropTypes.any,
  totalCount: PropTypes.number,
  showLoading: PropTypes.bool,
  clearItems: PropTypes.func,
  setFilter: PropTypes.func,
  search: PropTypes.string,
}
const mapDispatchToProps = dispatch => ({
  clearItems: () => dispatch(UserManagementActions.clearItems()),
  deleteItem: id => dispatch(UserManagementActions.deleteItemRequest(id)),
  getItems: filter =>
    dispatch(UserManagementActions.getItemsRequest(filter)),
  // getUserType: () => dispatch(UserManagementActions.getUserType()),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(
  withConnect,
  withRouter
)(TableUserManagementContainer)
