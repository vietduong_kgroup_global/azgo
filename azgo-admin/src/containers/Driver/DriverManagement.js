import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { Icon, Popconfirm, Select, Tag } from 'antd'
import { Link, withRouter } from 'react-router-dom'
import * as configsEnum from 'Utils/enum'
import { DriverManagementSelectors } from 'Stores/DriverManagement/Selectors'
import { DriverManagementActions } from 'Stores/DriverManagement/Actions'
  
import { WalletManagementActions } from 'Stores/WalletManagement/Actions'
import 'Stores/WalletManagement/Reducers'
import 'Stores/WalletManagement/Sagas'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import PropTypes from 'prop-types'
import { formatPhone } from 'Utils/helper'
import VerhicleList from 'containers/Driver/VerhicleList'
import * as config from 'Utils/enum'
import { Button } from 'antd'

const TableDriverManagementContainer = ({
  items,
  getItems,
  history,
  filter,
  setFilter,
  clearItems,
  showLoading,
  totalCount,
  deleteItem,
  search,
  createWallet
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter)
  }, [getItems, clearItems, filter])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  const handleOpenUpdate = id => {
    history.push('/admin/driver/edit/' + id)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  const { Option } = Select
  const columns = [
    {
      title: 'Họ và tên',
      key: 'fullName',
      render: value => {
        return (
          <Link to={`/admin/driver/detail/${value.userId}`}>
            {value.driverProfile && value.driverProfile.fullName}
          </Link>
        )
      },
    },
    {
      title: 'Số điện thoại',
      key: 'phone',
      render: value => {
        return formatPhone(value.countryCode, value.phone)
      },
    },
      {
      title: 'Email', 
      render: value => {
      return <div>{value && value.driverProfile && value.driverProfile.email}</div>
      },
    },
    {
      title: 'Ví tiền',
      // dataIndex: 'email',
      render: value => {
        return (value.walletEntity && value.walletEntity.id) ?   <Button onClick={() => handleOpenDetail(value.userId)}>Xem ví</Button> :   <Button  type="danger" onClick = {() => createWallet({user_id: value.userId})}>Tạo ví</Button>
      },
    },
    {
      title: 'Tình trạng',
      dataIndex: 'status',
      render: value => {
        const result = configsEnum.MANAGEMENT_STATUS_ARR.find(item => {
          return item.key === value
        })
        if (!result) return ''
        return (
          <span className={`btn-status btn-status-${result.label}`}>
            {result.label}
          </span>
        )
      },
    },
    {
      title: 'Xe',
      key: 'vehicles',
      dataIndex: 'vehicles',
      render: value => {
        return (
          <>
            {value.map((item, idx) => (
                <Tag color="blue">{item.licensePlates + '(' + (item.vehicleType && item.vehicleType.totalSeat + ' chỗ') +')'}</Tag>
              // <span className= 'list_verhicle' key={idx}>{item.licensePlates}</span>
            ))}
          </>
        )
      },
    },
    {
      title: 'Tài xế',
      key: 'roles',
      dataIndex: 'roles',
      render: value => {
        return (
          <>
            {value.map((item, idx) => { 
              if(item == config.MANAGEMENT_TYPE_USER.DRIVER_BIKE_CAR)  return  <Tag color="#2db7f5">{'Bike car'}</Tag>
              if(item == config.MANAGEMENT_TYPE_USER.DRIVER_TOUR)  return  <Tag color="#87d068">{'Tour'}</Tag>
              // <span className= 'list_verhicle' key={idx}>{item.licensePlates}</span>
            })}
          </>
        )
      },
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Select
            labelInValue
            style={{ width: 120 }}
            value={[]}
            placeholder="Chọn">
            <Option value="1" onClick={() => handleOpenUpdate(value.userId)}>
              Sửa
            </Option>
            {/* <Option value="2" onClick={() => handleOpenDetail(value.userId)}> 
              Xem TK Ví
            </Option> */}
            <Option value="3">
              <Popconfirm
                placement="left"
                onConfirm={() => handleConfirmDelete(value.userId)}
                icon={
                  <Icon type="question-circle-o" style={{ color: 'red' }} />
                }
                title="Bạn có chắc chắn muốn xóa tài xế"
                okText="Có"
                cancelText="Không">
                <div>Xóa</div>
              </Popconfirm>
            </Option>
          </Select>
        </>
      ),
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  const handleOpenDetail = uuid => {
    history.push('/admin/wallet/detail/' + uuid)
  }
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
        expandedRowRender={record => <VerhicleList verhicleList = {record.vehicles}/>}
      />
    </>
  )
}

const mapStateToProps = state => ({
  items: DriverManagementSelectors.getItems(state),
  totalCount: DriverManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

TableDriverManagementContainer.propTypes = {
  filter: PropTypes.object.isRequired,
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  history: PropTypes.any,
  setFilter: PropTypes.func.isRequired,
  clearItems: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  deleteItem: PropTypes.func,
  search: PropTypes.string,
}

const mapDispatchToProps = dispatch => ({
  getItems: filter => dispatch(DriverManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(DriverManagementActions.clearItems()),
  deleteItem: id => dispatch(DriverManagementActions.deleteItemRequest(id)),
  createWallet: data => dispatch(WalletManagementActions.createWalletForDriverRequest(data)), 
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(TableDriverManagementContainer)
