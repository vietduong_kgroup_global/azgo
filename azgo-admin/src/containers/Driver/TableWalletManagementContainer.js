import React, { useEffect, useState } from 'react'
import { Button } from 'reactstrap'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import { Link, withRouter } from 'react-router-dom'
import * as configsEnum from 'Utils/enum'
import TableList from 'components/organisms/TableList'
import { WalletManagementSelectors } from 'Stores/WalletManagement/Selectors'
import { WalletManagementActions } from 'Stores/WalletManagement/Actions'
import FormatPrice from 'components/organisms/FormatPrice'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { formatPhone, formatDateTime } from 'Utils/helper'

const TableWalletManagementContainer = ({
  items,
  getItems,
  history,
  filter,
  setFilter,
  clearItems,
  showLoading,
  totalCount,
  search,
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter)
  }, [getItems, clearItems, filter])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  const handleOpenDetail = uuid => {
    history.push('/admin/wallet/detail/' + uuid);
  }
  const columns = [
    {
      title: 'SDT TÀI XẾ',
      key: 'driverId',
      render: value => {
        return (
          <Link to={`/admin/driver/detail/${value.usersEntity ? Buffer.from(value.usersEntity.userId.data).toString() : ""}`}>
           { formatPhone(value.usersEntity.countryCode, value.usersEntity.phone) }
          </Link>
        )
      },
    },
    {
      title: 'Họ và tên',
      render: value => {
        return  value.usersEntity && value.usersEntity.driverProfile && value.usersEntity.driverProfile.fullName
      }   
    },
    {
      title: 'Tài khoản doanh thu (VND)',
      dataIndex: 'cashWallet',
      render: value => {
        return  <FormatPrice 
            value = {value} 
          />}   
    },
    {
      title: 'Tài khoản thanh toán (VND)',
      dataIndex: 'feeWallet',
      render: value => {
        return  <FormatPrice 
            value = {value} 
          />}   
    },
    {
        title: 'Tạo lúc',
        dataIndex: 'createdAt',
        render: (key, value) => { 
            return (
              <span   size="sm">
                {formatDateTime(value.createdAt) }
              </span>
            )
          },
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => (
        <>
         <Button
            color="light"
            size="sm"
            className="btn-sm-ct"
            style={{ marginRight: '5px' }}
            onClick={() => handleOpenDetail(value.usersEntity ? Buffer.from(value.usersEntity.userId.data).toString() : "")}>
            <i className="fa fa-pencil" />
            &nbsp;  Xem chi tiết 
          </Button>
        </>
      ),
    },
  ]
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

const mapStateToProps = state => ({
  items: WalletManagementSelectors.getItems(state),
  totalCount: WalletManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

TableWalletManagementContainer.propTypes = {
  filter: PropTypes.object.isRequired,
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  history: PropTypes.any,
  setFilter: PropTypes.func.isRequired,
  clearItems: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  deleteItem: PropTypes.func,
  search: PropTypes.string,
}

const mapDispatchToProps = dispatch => ({
  getItems: filter => dispatch(WalletManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(WalletManagementActions.clearItems()),
  deleteItem: id => dispatch(WalletManagementActions.deleteItemRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(TableWalletManagementContainer)
