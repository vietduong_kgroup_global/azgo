import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { Icon, Popconfirm, Select } from 'antd'
import { Link, withRouter } from 'react-router-dom'
import * as configsEnum from 'Utils/enum'
import { WalletHistorySelectors } from 'Stores/WalletHistory/Selectors'
import { WalletHistoryActions } from 'Stores/WalletHistory/Actions'
import 'Stores/WalletHistory/Reducers'
import 'Stores/WalletHistory/Sagas'
import { WalletManagementSelectors } from 'Stores/WalletManagement/Selectors'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import PropTypes from 'prop-types'
import { formatPhone,formatDateTime } from 'Utils/helper'
import FormatPrice from 'components/organisms/FormatPrice' 

const TableWalletHistoryContainer = ({
  items,
  item,
  getItems,
  history,
  filter,
  setFilter,
  clearItems,
  showLoading,
  totalCount,
  deleteItem,
  search,
  id,
  itemWallet
}) => { 
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
      driver_uuid: id
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => { 
    getItems(filter)
  }, [getItems, clearItems, filter])
  useEffect(() => {  
    getItems(filter)
  }, [itemWallet])
   
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  const handleOpenUpdate = id => {
    history.push('/admin/wallet/detail/' + id)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  const { Option } = Select
  const columns = [ 
    {
      title: 'Số tiền',
      dataIndex: 'amount',
      render: value => {
        return (
          <div>
            <FormatPrice 
              value={value}
              // thousandSeparator={'.'}
              // decimalSeparator={','}
              displayType={'text'}
            /><span> VND</span>
          </div>)
      }   
    //   render: value => {
    //     return formatPhone(value.countryCode, value.phone)
    //   },
    },
    {
      title: 'Nội dung',
      dataIndex: 'content',
         render: value => { 
            return <span className="overflow-text">{value}</span>
      },
    }, 
    {
        title: 'Chỉnh sửa lúc',
        dataIndex: 'createdAt',
        render: (key, value) => { 
            return (
              <span   size="sm">
                {formatDateTime(value.createdAt) }
              </span>
            )
          },
    },
    {
      title: 'Người cập nhật',
      // dataIndex: 'content',
        render: value => { 
            return <span className="overflow-text">{value.userUpdateEntity ? value.userUpdateEntity.email : ""}</span>
      },
    },
    {
      title: 'Loại cập nhật',
      // dataIndex: 'content',
        render: value => { 
            return <span className="overflow-text">{value.type == 1 ? "Tài khoản thanh toán" : "Tài khoản doanh thu"}</span>
      },
    },
    {
      title: 'Giao dịch',
      // dataIndex: 'content',
      render: value => { 
        return (
          <div>
            {value.paymentCode && <p className="overflow-text"><b>Mã giao dịch: </b>{value.paymentCode}</p>}
            {value.orderCode && <p className="overflow-text"><b>Mã đơn hàng: </b>{value.orderCode}</p>}
          </div>
        )
      },
    },
    // {
    //   title: 'Chức năng',
    //   dataIndex: 'function',
    //   render: (key, value) => (
    //     <>
    //       <Select
    //         labelInValue
    //         style={{ width: 120 }}
    //         value={[]}
    //         placeholder="Chọn">
    //         {/* <Option value="1" onClick={() => handleOpenUpdate(value.userId)}>
    //           Sửa
    //         </Option> */}
    //         <Option value="2" onClick={() => handleOpenDetail(value.uuid)}> 
    //           Xem chi tiết 
    //         </Option>
    //         <Option value="3">
    //           <Popconfirm
    //             placement="left"
    //             onConfirm={() => handleConfirmDelete(value.userId)}
    //             icon={
    //               <Icon type="question-circle-o" style={{ color: 'red' }} />
    //             }
    //             title="Bạn có chắc chắn muốn xóa tài xế"
    //             okText="Có"
    //             cancelText="Không">
    //             <div>Xóa</div>
    //           </Popconfirm>
    //         </Option>
    //       </Select>
    //     </>
    //   ),
    // },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  } 
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

const mapStateToProps = state => ({
  itemWallet: WalletManagementSelectors.getItem(state),
  items: WalletHistorySelectors.getItems(state),
  item: WalletHistorySelectors.getItem(state),
  totalCount: WalletHistorySelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

TableWalletHistoryContainer.propTypes = {
  filter: PropTypes.object.isRequired,
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
  history: PropTypes.any,
  setFilter: PropTypes.func.isRequired,
  clearItems: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  deleteItem: PropTypes.func,
  search: PropTypes.string,
}

const mapDispatchToProps = dispatch => ({
  getItems: filter => dispatch(WalletHistoryActions.getItemsRequest(filter)),
  clearItems: () => dispatch(WalletHistoryActions.clearItems()),
  deleteItem: id => dispatch(WalletHistoryActions.deleteItemRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(TableWalletHistoryContainer)
