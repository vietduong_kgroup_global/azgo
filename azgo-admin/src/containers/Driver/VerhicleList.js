import { List, Row, Col, Button } from 'antd';
import React, { useEffect, useState } from 'react'
import { Link, withRouter } from 'react-router-dom'

const data = [
  'Racing car sprays burning fuel into crowd.',
  'Japanese princess to wed commoner.',
  'Australian walks 100km after outback crash.',
  'Man charged over missing wedding girl.',
  'Los Angeles battles huge wildfires.',
];
const VerhicleList = ({verhicleList = []}) => {
    return(<div>
         <h5 style={{ margin: '16px 0' }}>Danh sách xe của tài xế</h5>
         <Row gutter={[24, 16]}>
              <Col span={6}>Biển số</Col>
              <Col span={6}>Tên</Col>
              <Col span={6}>Số ghế</Col>
              <Col span={6}> </Col>
          </Row>
            {verhicleList.map((item) => (
                    <Row gutter={[24, 16]}>
                    <Col span={6}>{item.licensePlates}</Col>
                    <Col span={6}>{item.vehicleType && item.vehicleType.name}</Col>
                    <Col span={6}>{item.vehicleType && item.vehicleType.totalSeat}</Col>
                    <Col span={6}>  
                        <Link to={'/admin/vehicle/vehicle/edit/' + item.id}>
                            <Button type="primary" size={'small'}>
                            Xem chi tiết
                            </Button>
                        </Link> 
                    </Col>
            </Row>
            ))}
      </div>)
} 
export default  VerhicleList