import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { Link, withRouter } from 'react-router-dom'
import { Tooltip, Select, Popconfirm, Icon } from 'antd'
import { Button } from 'reactstrap'
import { compose } from 'redux'
import { Tag } from 'antd';
import * as configsEnum from 'Utils/enum'
import {formatDateTime} from 'Utils/helper'
import 'Stores/TourCar/OrderTourLog/Reducers'
import 'Stores/TourCar/OrderTourLog/Sagas'
import { OrderTourLogSelectors } from 'Stores/TourCar/OrderTourLog/Selectors'
import { OrderTourLogActions } from 'Stores/TourCar/OrderTourLog/Actions'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { LoadingSelectors } from 'Stores/Loading/Selectors'

const { Option } = Select

const OrderTourLogContainer = ({
  items,
  history,
  clearItems,
  getItems,
  deleteItem,
  setFilter,
  filter,
  totalCount,
  showLoading,
  search,
}) => {
  const columns = [
    {
      title: 'Mã order',
      dataIndex: 'orderCode',
      // width: 100,
      render: (key, value) => (
        <>
          <Link to={'/admin/tour/orderTourLog/orderTourLogDetail/' + value.uuid}>
            <span>{value.tourOrderProfile.orderCode }</span>
          </Link>
          {value.company && <div>
            <Tag color="red">{value.company}</Tag>
            {/* <Tag  color="#cd201f">
              {value.company}
            </Tag> */}
          </div>}

        </>
      ),
    },
    {
      title: 'Người thay đổi',
      // dataIndex: 'userProfile.email',
      render: (key, value) => { 
        let userProfile = value.userProfile
        return (
          <>
            <div>{userProfile ? (userProfile.email || userProfile.phone) : ""}</div>
            <div>{userProfile ? ((userProfile.customerProfile && userProfile.customerProfile.fullName) || (userProfile.adminProfile && userProfile.adminProfile.fullName)) : ""}</div>
          </>
        )
      }, 
    }, 
    // {
    //   title: 'Loại TK',
    //   render: (key, value) => (
    //     <> 
    //         <span>{value.userProfile.type}</span> 
    //     </>
    //   ),
    //   // dataIndex: 'type',
    // },
    {
      title: 'Tour',
      dataIndex: 'tourOrderProfile.tourProfile.name',
    },
    {
      title: 'Ngày thay đổi',
      dataIndex: 'createdAt',
      render: (key, value) => { 
        return (
          <span   size="sm">
            {formatDateTime(value.createdAt)}
          </span>
        )
      },
    }, 
    {
      title: 'Trạng thái mới nhất',
      dataIndex: 'status',
      render: (key, value) => {
        const result = configsEnum.MANAGEMENT_STATUS_TOUR_ARR.find(item => {
          return item.value === value.tourOrderProfile.status
        })
        if (!result) return ''
        return (
          <Button color={result.color} size="sm">
            {result.label}
          </Button>
        )
      },
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => (
        <Button
          color="light"
          size="sm"
          className="btn-sm-ct"
          style={{ marginRight: '5px' }}
          onClick={() => handleOpenDetail(value.uuid)}>
          <i className="fa fa-pencil" />
          &nbsp;Xem chi tiết 
        </Button> 
      ),
    },
  ]
  const handleConfirmDelete = uuid => {
    deleteItem(uuid)
  }
  const handleOpenDetail = uuid => {
    history.push('/admin/tour/orderTourLog/orderTourLogDetail/' + uuid)
  } 
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  useEffect(() => {
    getItems(filter)
  }, [getItems, filter, clearItems])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        totalCount={totalCount}
        pagination={pagination}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

OrderTourLogContainer.propTypes = {
  clearItems: PropTypes.func,
  getItems: PropTypes.func,
  deleteItem: PropTypes.func,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
  items: PropTypes.object,
  totalCount: PropTypes.number,
  showLoading: PropTypes.bool,
  search: PropTypes.string,
}

const mapStateToProps = state => ({
  filter: OrderTourLogSelectors.getFilter(state),
  items: OrderTourLogSelectors.getItems(state),
  totalCount: OrderTourLogSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

const mapDispatchToProps = dispatch => ({
  getItems: filter => dispatch(OrderTourLogActions.getItemsRequest(filter)),
  deleteItem: filter => dispatch(OrderTourLogActions.deleteItemRequest(filter)),
  clearItems: () => dispatch(OrderTourLogActions.clearItems()),
  setFilter: filter => dispatch(OrderTourLogActions.setFilter(filter)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(OrderTourLogContainer)
