import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { Icon, Popconfirm, Select, Collapse } from 'antd'
import { Link, withRouter } from 'react-router-dom'
import * as configsEnum from '../../Utils/enum'
import { TourManagementSelectors } from 'Stores/TourCar/TourManagement/Selectors'
import { TourManagementActions } from 'Stores/TourCar/TourManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import PropTypes from 'prop-types'
import { formatPhone } from 'Utils/helper'
import { Button } from 'reactstrap'
import { UserSelectors } from 'Stores/User/Selectors'
import FormatPrice from 'components/organisms/FormatPrice'
const TableTourManagementContainer = ({
  items,
  getItems,
  history,
  filter,
  setFilter,
  clearItems,
  showLoading,
  totalCount,
  deleteItem,
  search,
  userInfo
}) => {
  let company = userInfo && userInfo.get('adminProfile') && userInfo.get('adminProfile').toJS().company  || ''
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter)
  }, [getItems, filter])
  useEffect(() => {
    setFilter({
      per_page: 10,
      page: 1,
      keyword: '',
      isFromAirport: null
    })
  }, [])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount, filter])
  const handleOpenUpdate = id => { 
    history.push('/admin/tour/tourManagement/' + id)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  const { Option } = Select
  const columns = [
    {
      title: 'Tên',
      key: 'name',
      dataIndex: 'name',
      // render: value => {
      //   return (
      //     <Link to={`/admin/tour/tourManagement/${value.userId}`}>
      //       {value.name}
      //     </Link>
      //   )
      // },
    },
    {
      title: 'Thời gian (phút)',
      key: 'time',
      render: value => { 
        if(value.type === "fix_price")
          return (
            <div><FormatPrice value={value.time}/></div>
          )  
      },
    },
    {
      title: 'Số chỗ',
      key: 'numberOfSeat',
      dataIndex: 'numberOfSeat',
      // dataIndex: 'email',
    },
    {
      title: 'Giá tour',
      // key: 'tourPrice',
      // dataIndex: 'tourPrice',
      render: value => { 
        if(value.type === "fix_price")
          return (
            <div><FormatPrice value={value.tourPrice}/>  VND</div>
          )
        else if(value.type === "distance_price")
          return (
            <div><FormatPrice value={value.distanceRate}/>  VND/km</div>
          )
        else if(value.type === "time_price")
          return (
            <div><FormatPrice value={value.timeRate}/>  VND/ngày</div>
          )
         
      },
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      width: 140,
      render: (key, value) => (
        company ? <></> : 
        <> 
          <Button
            color="light"
            size="sm"
            className="btn-sm-ct"
            style={{ marginRight: '5px' }}
            onClick={() =>  handleOpenUpdate(value.uuid)}>
            <i className="fa fa-pencil" />
            &nbsp;Sửa
          </Button>
          <Popconfirm
            placement="top"
            onConfirm={() => handleConfirmDelete(value.uuid)}
            title={
              'Bạn có chắc chắn muốn xóa tour ' +
              `${value.name ? value.name : ''}`
            }
            okText="Có"
            cancelText="Không">
            <Button color="danger" size="sm" className="btn-sm-ct">
              <i className="fa fa-trash-o" />
              &nbsp;Xóa
            </Button>
          </Popconfirm>
        </> 
      ),
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

const mapStateToProps = state => ({
  userInfo: UserSelectors.getUserData(state),
  items: TourManagementSelectors.getItems(state),
  totalCount: TourManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

TableTourManagementContainer.propTypes = {
  filter: PropTypes.object.isRequired,
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  history: PropTypes.any,
  setFilter: PropTypes.func.isRequired,
  clearItems: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  deleteItem: PropTypes.func,
  search: PropTypes.string,
}

const mapDispatchToProps = dispatch => ({
  getItems: filter => dispatch(TourManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(TourManagementActions.clearItems()),
  deleteItem: id => dispatch(TourManagementActions.deleteItemRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(TableTourManagementContainer)
