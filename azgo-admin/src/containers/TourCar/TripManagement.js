import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { Link, withRouter } from 'react-router-dom'
import { Tag, Select, Popconfirm, Icon } from 'antd'
import { Button } from 'reactstrap'
import { compose } from 'redux'
import * as configsEnum from 'Utils/enum'
import 'Stores/TourCar/TripManagement/Reducers'
import 'Stores/TourCar/TripManagement/Sagas'
import { TripManagementSelectors } from 'Stores/TourCar/TripManagement/Selectors'
import { TripManagementActions } from 'Stores/TourCar/TripManagement/Actions'
import { UserSelectors } from 'Stores/User/Selectors'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { formatDateTime } from 'Utils/helper'
import FormatPrice from 'components/organisms/FormatPrice'
const { Option } = Select

const TripManagementContainer = ({
  items,
  history,
  clearItems,
  getItems,
  deleteItem,
  setFilter,
  filter,
  totalCount,
  showLoading,
  search,
  listStatus,
  listCompany,
  userInfo
}) => {
  let company = userInfo && userInfo.get('adminProfile') && userInfo.get('adminProfile').toJS().company  || ''
  const columns = [
    {
      title: 'Mã order',
      key: 'id',
      dataIndex: 'orderCode',
      defaultSortOrder: 'descend',
      sorter: true,
      render: (key, value) => (
        <>
          <Link to={'/admin/tour/orderManagement/edit/' + value.uuid}>
            <span>{value.orderCode}</span> 
          </Link> 
          {value.company && <div>
            <Tag color="red">{value.company}</Tag>
            {/* <Tag  color="#cd201f">
              {value.company}
            </Tag> */}
          </div>}
        </>
      ),
    },
    {
      title: 'Khách hàng',
      dataIndex: 'customerName',
      render: (key, value) => {
        if(!!value.customerFirstName && !!value.customerLastName){
          return (
            <span>{`${value.customerFirstName} ${value.customerLastName}`}</span>
          )
        }
        else if(!!value.customerTour && !!value.customerTour.customerProfile){
          return (
            <span>{`${value.customerTour.customerProfile.fullName}`}</span>
          )
        }
        return ''
    },
    },
    {
      title: 'Tài xế',
      dataIndex: 'driverTour.driverProfile.fullName',
    },
    {
      title: 'Tour',
      dataIndex: 'tourProfile.name',
    },
    {
      title: 'Ngày tạo',
      key: "created_at",
      dataIndex: 'createdAt',
      sorter: true,
      render: (key, value) => (
        <>
          <span>{formatDateTime(value.createdAt)}</span>
        </>
      ),
    },
    {
      title: 'Khởi hành',
      key: "start_time",
      sorter: true,
      dataIndex: 'startTime',
      render: (key, value) => (
        <>
          <span>{formatDateTime(value.startTime)}</span>
        </>
      ),
    },
    {
      title: 'Giá',
      key: 'order_price',
      dataIndex: 'orderPrice',
      render: value => { 
        if(value)
        return (
          <div><FormatPrice value={value}/>  VND</div>
        )
      },
    },
    // {
    //   title: 'Điểm đi',
    //   dataIndex: 'routeInfo.startPosition.address',
    //   width: 250,
    //   render: value => {
    //     return (
    //       <Tooltip placement="topLeft" title={value}>
    //         <span className="overflow-text">{value}</span>
    //       </Tooltip>
    //     )
    //   },
    // },
    // {
    //   title: 'Điểm đến',
    //   dataIndex: 'routeInfo.endPosition.address',
    //   width: 250,
    //   render: value => {
    //     return (
    //       <Tooltip placement="topLeft" title={value}>
    //         <span className="overflow-text">{value}</span>
    //       </Tooltip>
    //     )
    //   },
    // },
    {
      title: 'Tình trạng',
      dataIndex: 'status',
      render: value => {
        const result = configsEnum.MANAGEMENT_STATUS_TOUR_ARR.find(item => {
          return item.value === value
        })
        if (!result) return ''
        return (
          <Button color={result.color} size="sm">
            {result.label}
          </Button>
        )
      },
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Select
            labelInValue
            style={{ width: 120 }}
            value={[]}
            placeholder="Chọn">
            <Option value="1" onClick={() => handleOpenUpdate(value.uuid)}>
              Sửa
            </Option>
            <Option value="2" onClick={() => handleOpenDetail(value.uuid)}>
              Xem chi tiết
            </Option>
            {!company && <Option value="3">
              <Popconfirm
                placement="left"
                onConfirm={() => handleConfirmDelete(value.uuid)}
                icon={
                  <Icon type="question-circle-o" style={{ color: 'red' }} />
                }
                title="Bạn có chắc chắn muốn xóa order này"
                okText="Có"
                cancelText="Không">
                <div>Xóa</div>
              </Popconfirm>
            </Option>}
          </Select>
        </>
      ),
    },
  ]
  const handleConfirmDelete = uuid => {
    deleteItem(uuid)
  }
  const handleOpenDetail = uuid => {
    history.push('/admin/tour/orderManagement/detail/' + uuid)
  }
  const handleOpenUpdate = uuid => {
    history.push('/admin/tour/orderManagement/edit/' + uuid)
  }
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const [sort, setSort] = useState("descend")
  const [sortBy, setSortBy] = useState("created_at")
  const handleTableChange = (pagination, filters, sorter) => {
    setSort(sort == "descend" ? "ascend" : "descend")
    setSortBy(sorter.columnKey)
    setFilter({
      page: pagination.current,
      sortBy: sortBy,
      sort: sort
    })
  }
  useEffect(() => { 
    setFilter({
      keyword: search,
      status: listStatus,
      listCompany: listCompany,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search, listStatus, listCompany])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  useEffect(() => {
    getItems(filter)
  }, [getItems, filter, clearItems])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, []) 
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        totalCount={totalCount}
        pagination={pagination}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

TripManagementContainer.propTypes = {
  clearItems: PropTypes.func,
  getItems: PropTypes.func,
  deleteItem: PropTypes.func,
  filter: PropTypes.object,
  setFilter: PropTypes.func,
  items: PropTypes.object,
  totalCount: PropTypes.number,
  showLoading: PropTypes.bool,
  search: PropTypes.string,
}

const mapStateToProps = state => ({
  userInfo: UserSelectors.getUserData(state),
  filter: TripManagementSelectors.getFilter(state),
  items: TripManagementSelectors.getItems(state),
  totalCount: TripManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

const mapDispatchToProps = dispatch => ({
  getItems: filter => dispatch(TripManagementActions.getItemsRequest(filter)),
  deleteItem: filter => dispatch(TripManagementActions.deleteItemRequest(filter)),
  clearItems: () => dispatch(TripManagementActions.clearItems()),
  setFilter: filter => dispatch(TripManagementActions.setFilter(filter)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(TripManagementContainer)
