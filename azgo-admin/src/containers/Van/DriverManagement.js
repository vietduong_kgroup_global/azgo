import React, { useEffect, useState } from 'react'
import TableList from 'components/organisms/TableList'
import { Icon, Popconfirm, Select, Tag } from 'antd'
import { Link, withRouter } from 'react-router-dom'
import * as configsEnum from '../../Utils/enum'
import { DriverManagementSelectors } from 'Stores/Van/DriverManagement/Selectors'
import { DriverManagementActions } from 'Stores/Van/DriverManagement/Actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import PropTypes from 'prop-types'
import { formatPhone } from 'Utils/helper'

const TableDriverManagementContainer = ({
  items,
  getItems,
  history,
  filter,
  setFilter,
  clearItems,
  showLoading,
  totalCount,
  deleteItem,
  search,
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter)
  }, [getItems, filter])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  const handleOpenUpdate = id => {
    history.push('/driver/van/edit/' + id)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  const { Option } = Select
  const columns = [
    {
      title: 'Họ và tên',
      key: 'fullName',
      render: value => {
        return (
          <Link to={`/driver/van/detail/${value.userId}`}>
            {value.driverProfile.fullName}
          </Link>
        )
      },
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'Tình trạng',
      dataIndex: 'status',
      render: value => {
        const result = configsEnum.MANAGEMENT_STATUS_ARR.find(item => {
          return item.key === value
        })
        if (!result) return ''
        return (
          <span className={`btn-status btn-status-${result.label}`}>
            {result.label}
          </span>
        )
      },
    },
    {
      title: 'Xe',
      key: 'vehicles',
      dataIndex: 'vehicles',
      render: value => {
        return (
          <>
            {value.map((item, idx) => (
                <Tag color="blue">{item.licensePlates + '(' + (item.vehicleType && item.vehicleType.totalSeat + ' chỗ') +')'}</Tag>
              // <span className= 'list_verhicle' key={idx}>{item.licensePlates}</span>
            ))}
          </>
        )
        // return (
        //   <>
        //     {value.map((item, idx) => (
        //       <span key={idx}>{item.licensePlates}, </span>
        //     ))}
        //   </>
        // )
      },
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => (
        <>
          <Select
            labelInValue
            style={{ width: 120 }}
            value={[]}
            placeholder="Chọn">
            <Option value="1" onClick={() => handleOpenUpdate(value.userId)}>
              Sửa
            </Option>
            <Option value="2">
              <Popconfirm
                placement="left"
                onConfirm={() => handleConfirmDelete(value.userId)}
                icon={
                  <Icon type="question-circle-o" style={{ color: 'red' }} />
                }
                title="Bạn có chắc chắn muốn xóa tài xế"
                okText="Có"
                cancelText="Không">
                <div>Xóa</div>
              </Popconfirm>
            </Option>
          </Select>
        </>
      ),
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

const mapStateToProps = state => ({
  items: DriverManagementSelectors.getItems(state),
  totalCount: DriverManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

TableDriverManagementContainer.propTypes = {
  filter: PropTypes.object.isRequired,
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  history: PropTypes.any,
  setFilter: PropTypes.func.isRequired,
  clearItems: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  deleteItem: PropTypes.func,
  search: PropTypes.string,
}

const mapDispatchToProps = dispatch => ({
  getItems: filter => dispatch(DriverManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(DriverManagementActions.clearItems()),
  deleteItem: id => dispatch(DriverManagementActions.deleteItemRequest(id)),
})

const withConnect = connect(mapStateToProps, mapDispatchToProps)

export default compose(withConnect, withRouter)(TableDriverManagementContainer)
