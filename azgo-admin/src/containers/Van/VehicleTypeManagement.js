import React, { useEffect, useState } from 'react'
import CardVehicleType from 'components/organisms/Van/CardVehicleTypesManagement'
import PropTypes from 'prop-types'
import { VehicleTypeManagementSelectors } from 'Stores/Van/VehicleTypeManagement/Selectors'
import { VehicleTypeManagementActions } from 'Stores/Van/VehicleTypeManagement/Actions'
import { withRouter } from 'react-router-dom'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { connect } from 'react-redux'
import { compose } from 'redux'

const CardVehicleTypeContainer = ({
  items,
  history,
  filter,
  setFilter,
  getItems,
  totalCount,
  showLoading,
  clearItems,
  search,
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter)
  }, [getItems, clearItems, filter])
  useEffect(() => {
    return () => {
      clearItems()
    }
  }, [])
  return (
    <>
      <CardVehicleType dataSource={items} />
      {
        totalCount === 0 && (
          <div>Không có kết quả tìm kiếm</div>
        )
      }
    </>
  )
}

CardVehicleTypeContainer.propTypes = {
  filter: PropTypes.object.isRequired,
  getItems: PropTypes.func.isRequired,
  items: PropTypes.object.isRequired,
  history: PropTypes.any,
  setFilter: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  showLoading: PropTypes.bool.isRequired,
  clearItems: PropTypes.func.isRequired,
  search: PropTypes.string,
}
const mapStateToProps = state => ({
  items: VehicleTypeManagementSelectors.getItems(state),
  totalCount: VehicleTypeManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})
const mapDispatchToProps = dispatch => ({
  getItems: filter =>
    dispatch(VehicleTypeManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(VehicleTypeManagementActions.clearItems()),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(withConnect, withRouter)(CardVehicleTypeContainer)
