import React, { useState, useEffect } from 'react';
import { Table, Radio, Divider, Button, Tag } from 'antd';
import { connect } from 'react-redux'
import { compose } from 'redux'
import { DriverManagementSelectors } from 'Stores/DriverManagement/Selectors'
import { DriverManagementActions } from 'Stores/DriverManagement/Actions'
import { Link, withRouter } from 'react-router-dom'
import * as configsEnum from 'Utils/enum'
import { formatPhone } from 'Utils/helper'
import PropTypes from 'prop-types'
import TableList from 'components/organisms/TableList'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import FormSearch from 'components/molecules/FormSearch'

   
const columns = [
    {
        title: 'Họ và tên',
        key: 'fullName',
        render: value => {
          return (
            <Link to={`/admin/driver/detail/${value.userId}`}>
              {value.driverProfile && value.driverProfile.fullName}
            </Link>
          )
        },
      },
      {
        title: 'Số điện thoại',
        key: 'phone',
        render: value => {
          return formatPhone(value.countryCode, value.phone)
        },
      },
      {
        title: 'Email',
        dataIndex: 'email',
      },
      {
        title: 'Tình trạng',
        dataIndex: 'status',
        render: value => {
          const result = configsEnum.MANAGEMENT_STATUS_ARR.find(item => {
            return item.key === value
          })
          if (!result) return ''
          return (
            <span className={`btn-status btn-status-${result.label}`}>
              {result.label}
            </span>
          )
        },
      },
      {
        title: 'Xe',
        key: 'vehicles',
        dataIndex: 'vehicles',
        render: value => {
          return (
            <>
              {value.map((item, idx) => (
                  <Tag color="blue">{item.licensePlates + '(' + (item.vehicleType && item.vehicleType.totalSeat + ' chỗ') +')'}</Tag>
                // <span className= 'list_verhicle' key={idx}>{item.licensePlates}</span>
              ))}
            </>
          )
          // return (
          //   <>
          //     {value && value.map((item, idx) => (
          //       <span className= 'list_verhicle' key={idx}>{item.licensePlates}</span>
          //     ))}
          //   </>
          // )
        },
      },
    //   {
    //     title: 'Chọn',
    //     // key: 'vehicles',
    //     // dataIndex: 'vehicles',
    //     render: value => {
    //       return (
    //         <Button onClick={(value) => handleSelectDriver(value)} />
    //       )
    //     },
    //   },
];
const DriverList = ({
  getItems,
  items,
  filter,
  setFilter,
  selectDriver,
  totalCount,
  showLoading,
  clearItems,
  tourInfo
}) => {
  const [selectionType, setSelectionType] = useState('checkbox');
  const [search, setSearch] = useState()
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
 
const handleSelectDriver = (driver) => {
    selectDriver(driver)
}
  const handleSearch = value => {
    setSearch(value)
  }
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => { 
    getItems(filter)
  }, [getItems, clearItems, filter])
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  }
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])
  return (
    <div>
      {/* <Divider /> */}
      <div className="header-main-content"> 
        <FormSearch handleSearch={handleSearch} />
      </div>
      <TableList
        onRow={(record, rowIndex) => {
            return {
              onClick: event => {handleSelectDriver(record)} , // click row
            //   onDoubleClick: event => {}, // double click row
            //   onContextMenu: event => {}, // right button click row
            //   onMouseEnter: event => {}, // mouse enter row
            //   onMouseLeave: event => {}, // mouse leave row
            };
          }}
        columns={columns}
        dataSource={items}
        pagination={pagination}
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </div>
  );
};
DriverList.propTypes = {
    filter: PropTypes.object.isRequired,
    getItems: PropTypes.func.isRequired,
    items: PropTypes.object.isRequired,
    selectDriver: PropTypes.func.isRequired,
    // history: PropTypes.any,
    setFilter: PropTypes.func.isRequired,
    clearItems: PropTypes.func.isRequired,
    totalCount: PropTypes.number.isRequired,
    showLoading: PropTypes.bool.isRequired,
    // deleteItem: PropTypes.func,
    search: PropTypes.string,
  }
const mapStateToProps = state => ({
  totalCount: DriverManagementSelectors.getTotalCount(state),
  filter: DriverManagementSelectors.getFilter(state),
  items: DriverManagementSelectors.getItems(state),
 
  showLoading: LoadingSelectors.getLoadingList(state),
  })
const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(DriverManagementActions.setFilter(filter)),
  getItems: filter => dispatch(DriverManagementActions.getItemsRequest(filter)),
  clearItems: () => dispatch(DriverManagementActions.clearItems()),
  })
  
  const withConnect = connect(mapStateToProps, mapDispatchToProps)
  
  export default compose(withConnect, withRouter)(DriverList)