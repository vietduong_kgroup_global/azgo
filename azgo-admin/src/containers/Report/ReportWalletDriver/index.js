import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { DatePicker,Row, Col,Button, Input, Modal } from 'antd'
import { compose } from 'redux'
import { withRouter } from 'react-router-dom'
import moment from 'moment'
import 'Stores/Report/Reducers'
import 'Stores/Report/Sagas'
import TableList from 'components/organisms/TableList'
import { ReportActions } from 'Stores/Report/Actions'
import { ReportSelectors } from 'Stores/Report/Selectors'
import { formatDate } from 'Utils/helper'
import 'Stores/DriverManagement/Reducers'
import 'Stores/DriverManagement/Sagas' 
import { formatPrice } from 'Utils/helper'
import { LoadingSelectors } from 'Stores/Loading/Selectors' 
import DriverList from 'containers/Report/driverList'
const {  RangePicker } = DatePicker;
const ReportDriverWallet = ({ 
    // exportExcel,
    items, 
    driverWalletReport,
    totalCount,
    setFilterDriverWallet,
    filter,
    showLoading,
    exportDriverWalletReport,
    currentPage
}) => { 
  const [multiData, setMultiData] = useState([])
  const date = new Date()

  useEffect(() => { 
    driverWalletReport(filter)
  }, [filter]) 
  const handleExport = async () => {
    const header = [
      "STT",
      "Mã Tài xế",
      "SĐT",
      "Ho ten",
      "CMND", 
      "email",
      "Số tài khoản",
      "Ngân hàng",
      "Chi nhánh",
      "Tên TK", 
      "Tài khoản thanh toán", 
      "Tài khoản doanh thu", 
    ];
    // await this.getlistInfo({ export: true });
    const { listInfo = [] } = [

    ]

    let title = "BÁO CÁO TIỀN TÀI XẾ";
    const exportData = [[title], header];
    let data = (items ? items.toJS() : []).map((e, index) => {
      return [index+1 , e.id, e.phone, e.driverProfile && e.driverProfile.fullName, e.driverProfile && e.driverProfile.cmnd, e.driverProfile && e.driverProfile.email, e.userAccount && e.userAccount.account && e.userAccount.account.account_bank_number, e.userAccount && e.userAccount.account && e.userAccount.account.account_bank_name, e.userAccount && e.userAccount.account && e.userAccount.account.account_bank_branch, e.userAccount && e.userAccount.account && e.userAccount.account.account_bank_user_name, e.walletEntity && e.walletEntity.fee_wallet, e.walletEntity && e.walletEntity.cash_wallet];
    });
    exportExcel({
      exportData: exportData.concat(data),
      name: `REPORT`,
      sheet: `Report`,
      length: header.length - 1,
      header : header
    });
  };
  const columns = [
    {
      title: 'STT',
      key: 'id',
      // dataIndex: 'driverProfile_id',
      render : (text, record, index) => { 
        return((currentPage - 1) * pagination.pageSize + index + 1)
      },
      // render: value => {
      //   return (
      //     <Link to={`/admin/driver/detail/${value.userId}`}>
      //       {value.driverProfile && value.driverProfile && value.driverProfile.fullName}
      //     </Link>
      //   )
      // },
    },
    {
      title: 'Số điện thoại',
      // key: 'phone',
      dataIndex: 'azUsersPhone', 
    },
    {
      title: 'Tên tài xế',
      dataIndex: 'driverProfileFullName', 
    },
    {
      title: 'CMND',
      dataIndex: 'driverProfileCmnd', 
    },
    {
      title: 'Email',
      dataIndex: 'azUsersEmail',
      // render: value => {
      //   return  value && value.email
      // },
    },
    {
      title: 'Số tài khoản',
      dataIndex: 'accountAccountBankNumber', 
    },
    {
      title: 'Ngân hàng',
      // dataIndex: 'account_account_bank_name', 
      render: value => { 
        return(<div>{(value.accountAccountBankName ? value.accountAccountBankName : "") + (value.accountAccountBankBranch ? ` (CN ${value.accountAccountBankBranch})` : "")}</div>)
      }
    }, 
    {
      title: 'Tên Chủ Tài Khoản',
      dataIndex: 'accountAccountBankUserName', 
    },
    {
      title: 'Fee (Hiện tại)',
      dataIndex: 'walletEntityFeeWalletBalance', 
      render: value => <div>{formatPrice(value)}</div>,
    },
    {
      title: 'Cash (Hiện tại)',
      dataIndex: 'walletEntityCastWalletBalance', 
      render: value => <div>{formatPrice(value)}</div>,
    },
    {
      title: 'Đơn vị tiền', 
      render: value => <div>VND</div>,
    },
  ]
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const [driverSelect, setDriverSelect] = useState({})
  const [visibleDriverList, setVisibleDriverList] = useState(false);
  
  const selectDriver = (driver) => {
    setVisibleDriverList(false)
    setDriverSelect(driver)
    setFilterDriverWallet({
      driverId: driver.id,
    }) 
  }

  const handleOpenDriverList = () => {
    setVisibleDriverList(true)
  }

  useEffect(() => { 
    setPagination({
      ...pagination,
      current: 1,
    })
    let from =formatDate(moment(new Date(date.getFullYear(), date.getMonth(), 1)).format('DD/MM/YYYY')) + ' 00:00:00Z';
    let  to = formatDate(moment(new Date(date.getFullYear(), date.getMonth() + 1, 0)).format('DD/MM/YYYY'))+ ' 23:59:59Z';
    setFilterDriverWallet({
      from: from, 
      to: to,
      driverId: null
    })
  }, [])

  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount, filter])

  useEffect(() => { 
    driverWalletReport(filter)
  }, [filter])
  
  const onChange = async(date = [], dateString) => { 
    setFilterDriverWallet({from: date[0] ? formatDate(moment(date[0]).format('DD/MM/YYYY')) + ' 00:00:00Z' : null, to: date[1] ? formatDate(moment(date[1]).format('DD/MM/YYYY'))+ ' 23:59:59Z' : null, page: 1})
    // driverWalletReport(filter)
  }
  
  const handleTableChange = pagination => {
    setFilterDriverWallet({
      page: pagination.current,
    }) 
  }
  
  const exportExcel = () => {
    exportDriverWalletReport(filter) 
  }
  console.log(driverSelect);
  return (
    <> 
    <Modal
      title="Chọn tài xế"
      centered
      visible={visibleDriverList}
      onCancel={() => setVisibleDriverList(false)}
      width={1000}
      zIndex={1020}
      footer={[
        <Button key="back" onClick={() => setVisibleDriverList(false)}>
          Huỷ
        </Button>,
      ]}
    >
      <DriverList selectDriver={selectDriver} />
    </Modal>
    <div style = {{marginBottom: "16px"}}>
      <Row gutter={16} >
        <Col className="gutter-row" xs={{span: 24}} sm={{span: 12}} md={{span: 8}} lg={{span: 8}} xxl={{span: 8}}>
            <Button type="primary"  icon="download" size={'default'} onClick = {() => exportExcel()}>
              Xuất excel
            </Button> 
        </Col>
        <Col className="gutter-row" xs={{span: 24}} sm={{span: 12}} md={{span: 8}} lg={{span: 8}} xxl={{span: 8}}>
          <Input
            style={{maxWidth: '250px', float: "right"}}
            allowClear={true}
            placeholder="Chọn tài xế"
            onClick={handleOpenDriverList}
            value={(driverSelect && driverSelect.driverProfile && driverSelect.driverProfile.fullName)}
          />
        </Col>
        <Col className="gutter-row" xs={{span: 24}} sm={{span: 12}} md={{span: 8}} lg={{span: 8}} xxl={{span: 8}}>
          <RangePicker onChange={onChange}   defaultValue={[moment(new Date(date.getFullYear(), date.getMonth(), 1), 'YYYY-MM-DD'), moment(new Date(date.getFullYear(), date.getMonth() + 1, 0))]}  format={'DD/MM/YYYY'} />
        </Col>
      </Row> 
    </div>
    <TableList
        columns={columns}
        dataSource={items} 
        pagination={pagination} 
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
      />
    </>
  )
}

const mapStateToProps = state => ({
  filter: ReportSelectors.getFilterDriverWallet(state),
  items: ReportSelectors.getDriverWallet(state),
  totalCount: ReportSelectors.getTotalCountDriverWallet(state),
  currentPage: ReportSelectors.getCurrentPageDriverWallet(state),
  showLoading: LoadingSelectors.getLoadingList(state),
   
})

ReportDriverWallet.propTypes = {
  filter: PropTypes.object,
  totalCount: PropTypes.number,
  showLoading: PropTypes.bool,
  setFilterDriverWallet: PropTypes.func,
}
const mapDispatchToProps = dispatch => ({ 
  driverWalletReport: data => dispatch(ReportActions.driverWalletReport(data)),
  exportDriverWalletReport: data => dispatch(ReportActions.exportDriverWalletReport(data)),
  setFilterDriverWallet: filter => dispatch(ReportActions.setFilterDriverWallet(filter)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(
  withConnect,
  withRouter
)(ReportDriverWallet)








