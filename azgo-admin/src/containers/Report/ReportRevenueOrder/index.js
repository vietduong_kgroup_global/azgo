import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Input, Modal, DatePicker,Row, Col, Button as ButtonAntd } from 'antd'
import { compose } from 'redux'
import { withRouter, Link } from 'react-router-dom'
import { Card, CardBody, CardHeader, Button } from 'reactstrap'
import moment from 'moment'
import 'Stores/Report/Reducers'
import 'Stores/Report/Sagas'
import TableList from 'components/organisms/TableList'
import { ReportActions } from 'Stores/Report/Actions'
import { ReportSelectors } from 'Stores/Report/Selectors'
import * as configsEnum from 'Utils/enum' 
import { formatDate } from 'Utils/helper'
import 'Stores/DriverManagement/Reducers'
import 'Stores/DriverManagement/Sagas' 
import { formatPrice } from 'Utils/helper'
import { formatDateTime } from 'Utils/helper' 
import { LoadingSelectors } from 'Stores/Loading/Selectors' 
import DriverList from 'containers/Report/driverList'
const {  RangePicker } = DatePicker;
const ReportRevenueOrder = ({ 
    // exportExcel,
    items, 
    revenueOrderReport,
    totalCount,
    setFilterRevenueOrder,
    filter,
    showLoading,
    exportRevenueOrderReport,
    currentPage
}) => {
  const [driverSelect, setDriverSelect] = useState({})
  const [visibleDriverList, setVisibleDriverList] = useState(false);
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  const columns = [
    {
      title: 'STT',
      key: 'id', 
      render : (text, record, index) => {
        return((currentPage - 1) * pagination.pageSize + index + 1)
      }
    },
    {
      title: 'Mã chuyến đi',
      // key: 'phone',
      dataIndex: 'orderCode', 
    },
    {
      title: 'Họ tên Khách hàng',
      dataIndex: 'customerName',
      render: (key, value) => {
        if(!!value.customerFirstName && !!value.customerLastName){
          return (
            <span>{`${value.customerFirstName} ${value.customerLastName}`}</span>
          )
        }
        else if(!!value.customerTour && !!value.customerTour.customerProfile){
          return (
            <span>{`${value.customerTour.customerProfile.fullName}`}</span>
          )
        }
        return ''
      },
    },
    {
      title: 'SDT Khách hàng',
      dataIndex: 'customerName',
      render: (key, value) => {
        if(!!value.customerMobile){
          return (
            <span>{`${value.customerMobile}`}</span>
          )
        }
        else if(!!value.customerTour && !!value.customerTour.customerProfile){
          return (
            <span>{`${value.customerTour.customerProfile.phone}`}</span>
          )
        }
        return ''
      },
    },
    {
      title: 'Họ và tên Tài xế',
      dataIndex: 'driverTour.driverProfile.fullName',
    }, 
    {
      title: 'SDT Tài xế',
      dataIndex: 'driverTour.phone',
    },
    {
      title: 'Tên Tour',
      dataIndex: 'tourProfile.name',
    },
    {
      title: 'Thời gian khách đặt chuyến',
      key: "createdAt", 
      sorter: true,
      render: (key, value) => {
        return <span>{formatDateTime(value && value.createdAt)}</span>
      }  
    },
    {
      title: 'Thời gian bắt đầu thực hiện chuyến xe',
      key: "startTime",
      sorter: true, 
      render: (key, value) => (
        <>
          <span>{formatDateTime(value && value.startTime)}</span>
        </>
      ),
    },
    {
      title: 'Thời gian kết thúc chuyến xe',
      key: "finishTime",
      sorter: true, 
      render: (key, value) => (
        <>
          <span>{formatDateTime(value && value.finishTime)}</span>
        </>
      ),
    }, 
    {
      title: 'Tổng giá trị chuyến xe ước tính',
      key: 'orderPrice',
      render: value => { 
        if(value)
        return (
          <div>{value && value.orderPrice >= 0 && formatPrice(value.orderPrice)} VND</div>
        )
      },
    }, 
    {
      title: 'Đơn vị tiền', 
      render: value => <div>VND</div>,
    },
    {
      title: 'Tình trạng',
      dataIndex: 'status',
      render: value => {
        const result = configsEnum.MANAGEMENT_STATUS_TOUR_ARR.find(item => {
          return item.value === value
        })
        if (!result) return ''
        return (
          <Button color={result.color} size="sm">
            {result.label}
          </Button>
        )
      },
    },
  ]
  
  const selectDriver = (driver) => {
    setVisibleDriverList(false)
    setDriverSelect(driver)
    setFilterRevenueOrder({
      driverId: driver.id,
    }) 
  }

  const handleOpenDriverList = () => {
    setVisibleDriverList(true)
  }
 
  useEffect(() => { 
    setPagination({
      ...pagination,
      current: 1,
    })
    let from =formatDate(moment(new Date(date.getFullYear(), date.getMonth(), 1)).format('DD/MM/YYYY')) + ' 00:00:00Z';
    let  to = formatDate(moment(new Date(date.getFullYear(), date.getMonth() + 1, 0)).format('DD/MM/YYYY'))+ ' 23:59:59Z';
    setFilterRevenueOrder({
      from: from,
      to: to,
      driverId: null
    })
  }, [])

  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount, filter])

  useEffect(() => {
    revenueOrderReport(filter)
  }, [filter])
  
  const onChange = async(date = [], dateString) => { 
    setFilterRevenueOrder({
      from: date[0] ? formatDate(moment(date[0]).format('DD/MM/YYYY')) + ' 00:00:00Z' : null,
      to: date[1] ? formatDate(moment(date[1]).format('DD/MM/YYYY'))+ ' 23:59:59Z' : null,
      page: 1
    })
    // revenueOrderReport(filter)
  }
  
  const handleTableChange = pagination => {
    setFilterRevenueOrder({
      page: pagination.current,
    }) 
  }
  
  const exportExcel = () => {
    exportRevenueOrderReport(filter) 
  }
  
  const date = new Date();
  return (
    <> 
    <Modal
      title="Chọn tài xế"
      centered
      visible={visibleDriverList}
      onCancel={() => setVisibleDriverList(false)}
      width={1000}
      zIndex={1020}
      footer={[
        <ButtonAntd key="back" onClick={() => setVisibleDriverList(false)}>
          Huỷ
        </ButtonAntd>,
      ]}
    >
      <DriverList selectDriver={selectDriver} />
    </Modal>
    <div style = {{marginBottom: "16px"}}>
      <Row>
        <Col span={8}> 
          <ButtonAntd type="primary"  icon="download" size={'default'} onClick = {() => exportExcel()}>
            Xuất excel
          </ButtonAntd> 
        </Col>
        <Col span={8}> 
          <Input
            style={{maxWidth: '250px', float: "right"}}
            allowClear={true}
            placeholder="Chọn tài xế"
            onClick={handleOpenDriverList}
            value={(driverSelect && driverSelect.driverProfile && driverSelect.driverProfile.fullName)}
          />
        </Col>
        <Col span={8}> 
          <RangePicker
            onChange={onChange}
            defaultValue={[moment(new Date(date.getFullYear(), date.getMonth(), 1), 'YYYY-MM-DD'), moment(new Date(date.getFullYear(), date.getMonth() + 1, 0))]}
            format={'DD/MM/YYYY'}
          />
        </Col>
      </Row> 
    </div>
    <TableList
        columns={columns}
        dataSource={items} 
        pagination={pagination} 
        totalCount={totalCount}
        showLoading={showLoading}
        handleTableChange={handleTableChange}
        // expandedRowRender={record => <VerhicleList verhicleList = {record.vehicles}/>}
      />
    </>
  )
}

const mapStateToProps = state => ({
  filter: ReportSelectors.getFilterRevenueOrder(state),
  items: ReportSelectors.getRevenueOrder(state),
  totalCount: ReportSelectors.getTotalCountRevenueOrder(state),
  showLoading: LoadingSelectors.getLoadingList(state),
  currentPage: ReportSelectors.getCurrentPageRevenueOrder(state),
})

ReportRevenueOrder.propTypes = {
  filter: PropTypes.object,
  totalCount: PropTypes.number,
  showLoading: PropTypes.bool,
  setFilterRevenueOrder: PropTypes.func,
}
const mapDispatchToProps = dispatch => ({ 
  revenueOrderReport: data => dispatch(ReportActions.revenueOrderReport(data)),
  exportRevenueOrderReport: data => dispatch(ReportActions.exportRevenueOrderReport(data)),
  setFilterRevenueOrder: filter => dispatch(ReportActions.setFilterRevenueOrder(filter)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(
  withConnect,
  withRouter
)(ReportRevenueOrder)





