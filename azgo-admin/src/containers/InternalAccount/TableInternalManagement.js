import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { withRouter, Link } from 'react-router-dom'
import * as configsEnum from 'Utils/enum'
import { Select, Popconfirm, Icon } from 'antd'
import { formatPhone } from 'Utils/helper'
import { InternalManagementSelectors } from 'Stores/InternalManagement/Selectors'
import { InternalManagementActions } from 'Stores/InternalManagement/Actions'
import TableList from 'components/organisms/TableList'
import { LoadingSelectors } from 'Stores/Loading/Selectors'


import 'Stores/Intercity/AreaManagement/Reducers'
import 'Stores/Intercity/AreaManagement/Sagas'
import { AreaManagementActions } from 'Stores/Intercity/AreaManagement/Actions'
import { AreaManagementSelectors } from 'Stores/Intercity/AreaManagement/Selectors'

const TableInternalManagementContainer = ({
  deleteItem,
  getItems,
  items,
  history,
  filter,
  setFilter,
  totalCount,
  showLoading,
  clearItems,
  search,
  getListItemsArea,
  itemsArea
}) => {
  const [pagination, setPagination] = useState({
    pageSize: 0,
    total: 0,
  })
  useEffect(() => {
    setFilter({
      keyword: search,
      page: 1,
    })
    setPagination({
      ...pagination,
      current: 1,
    })
  }, [search])
  useEffect(() => {
    getItems(filter)
    getListItemsArea(); 
  }, [getItems, clearItems, filter])
  useEffect(() => {
    return () => {
      clearItems(); 
    }
  }, [])
  useEffect(() => {
    setPagination({
      pageSize: filter.get('per_page'),
      total: totalCount,
    })
  }, [totalCount])

  const handleOpenUpdate = id => {
    history.push('/admin/internal-management/edit/' + id)
  }
  const handleConfirmDelete = id => {
    deleteItem(id)
  }
  const { Option } = Select
  const columns = [
    {
      title: 'Họ tên',
      key: 'fullName',
      render: value => {
        return (
          <div>
            <Link to={`/admin/internal-management/detail/${value.userId}`}>
              {value.adminProfile && value.adminProfile.fullName}
            </Link>
              {/* <span>{value.adminProfile && value.adminProfile.phone}</span> */}
          </div>
        )
      },
    },
    {
      title: 'Số điện thoại',
      render: value => {
        return (
          <div>{value.adminProfile && value.adminProfile.phone}</div>
        )
      },
    },
    {
      title: 'Email (Username)',
      dataIndex: 'email',
    },
    {
      title: 'Công ty', 
      render: value => {
        return value.adminProfile && value.adminProfile.company
      }
      },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      render: value => {
        const result = configsEnum.MANAGEMENT_STATUS_ARR.find(item => {
          return item.key === value
        })
        if (!result) return ''
        return (
          <span className={`btn-status btn-status-${result.label}`}>
            {result.label}
          </span>
        )
      },
    },
    {
      title: 'Quyền',
      // dataIndex: 'userRoles.0.roles.id',
      render: value => {
        let type  = value.type 
        let area_uuid  = value.adminProfile && value.adminProfile.areaUuid   
        let resultRole = configsEnum.ACCOUNT_INITIAL_ROLE_ARR.find(item => {
          return item.key === type
        }) 
        if (!resultRole) resultRole = configsEnum.ACCOUNT_INITIAL_ROLE_ARR[0]
        const resultArea = (itemsArea && itemsArea.toJS()).find(item => {
          return item.uuid === area_uuid
        })  
        return (
          <span className={`btn-status btn-status-${resultRole.label}`}>
            {resultRole && resultRole.label} <span>{resultArea ? resultArea.name : ""}</span>
          </span>
        )
      },
    },
    {
      title: 'Chức năng',
      dataIndex: 'function',
      render: (key, value) => {
        return (
          <>
            <Select
              labelInValue
              style={{ width: 100 }}
              value={[]}
              placeholder="Chọn">
              <Option value="1" onClick={() => handleOpenUpdate(value.userId)}>
                Sửa
              </Option>
              <Option value="2">
                <Popconfirm
                  placement="left"
                  onConfirm={() => handleConfirmDelete(value.userId)}
                  icon={
                    <Icon type="question-circle-o" style={{ color: 'red' }} />
                  }
                  title={'Bạn có chắc chắn muốn xóa tài khoản'}
                  okText="Có"
                  cancelText="Không">
                  <div>Xóa</div>
                </Popconfirm>
              </Option>
            </Select>
          </>
        )
      },
    },
  ]
  const handleTableChange = pagination => {
    setFilter({
      page: pagination.current,
    })
    getItems(filter)
  } 
  return (
    <>
      <TableList
        columns={columns}
        dataSource={items}
        pagination={pagination}
        handleTableChange={handleTableChange}
        showLoading={showLoading}
        totalCount={totalCount}
      />
    </>
  )
}

const mapStateToProps = state => ({
  itemsArea: AreaManagementSelectors.getItems(state),
  items: InternalManagementSelectors.getItems(state),
  totalCount: InternalManagementSelectors.getTotalCount(state),
  showLoading: LoadingSelectors.getLoadingList(state),
})

TableInternalManagementContainer.propTypes = {
  deleteItem: PropTypes.func,
  filter: PropTypes.object,
  getItems: PropTypes.func,
  items: PropTypes.object,
  history: PropTypes.any,
  totalCount: PropTypes.number,
  showLoading: PropTypes.bool,
  clearItems: PropTypes.func,
  setFilter: PropTypes.func,
  search: PropTypes.string,
}
const mapDispatchToProps = dispatch => ({
  clearItems: () => dispatch(InternalManagementActions.clearItems()),
  deleteItem: id => dispatch(InternalManagementActions.deleteItemRequest(id)),
  getItems: filter =>
    dispatch(InternalManagementActions.getItemsRequest(filter)),
  getListItemsArea: values =>
    dispatch(AreaManagementActions.getItemsRequest(values)),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
export default compose(
  withConnect,
  withRouter
)(TableInternalManagementContainer)

