import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { InternalManagementActions, InternalManagementTypes } from './Actions'
import { InternalManagementSelectors } from './Selectors'
import { InternalManagementService } from 'Services/InternalManagementService'
import { BlockUserService } from 'Services/BlockUser'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { fromJS } from 'immutable'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'

function* getItemsWorker({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      InternalManagementService.getListItems,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: InternalManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: InternalManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailWorker({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(InternalManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: InternalManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: InternalManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* createItemWorker({ values }) {
  try {
    const response = yield call(InternalManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: InternalManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/admin/internal-management'))
  } catch (error) {
    yield put({
      type: InternalManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* editItemWorker({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(InternalManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: InternalManagementTypes.EDIT_ITEM_SUCCESS,
    })
    if (data.changeBlockUser === true && data.status === 0) {
      yield call(BlockUserService.unblockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 1) {
      yield call(BlockUserService.blockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 0) {
      yield call(BlockUserService.unblockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 1) {
      yield call(BlockUserService.blockUser, data.id)
    }
    yield put(push('/admin/internal-management'))
  } catch (error) {
    yield put({
      type: InternalManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemWorker({ id }) {
  try {
    const filter = yield select(InternalManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(InternalManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: InternalManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      InternalManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: InternalManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(InternalManagementTypes.GET_ITEMS_REQUEST, getItemsWorker),
    takeLatest(
      InternalManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailWorker
    ),
    takeLatest(InternalManagementTypes.CREATE_ITEM_REQUEST, createItemWorker),
    takeLatest(InternalManagementTypes.EDIT_ITEM_REQUEST, editItemWorker),
    takeLatest(InternalManagementTypes.DELETE_ITEM_REQUEST, deleteItemWorker),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
