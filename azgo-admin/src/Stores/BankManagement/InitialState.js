import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'BankManagement'

export const BANK_STATE = fromJS({
  items: [],
  allItem: [],
  item: {},
  filter: {
    per_page: 100,
    page: 1,
    keyword: '',
  },
  totalCount: 0,
})
