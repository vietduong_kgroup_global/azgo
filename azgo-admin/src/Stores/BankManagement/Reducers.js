import { fromJS } from 'immutable'

import { BANK_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { BankManagementTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
      },
    })
  )
}
const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}
const setAllItems = (state, { allItem , count}) => {
  return state.merge(
    fromJS({
      allItem,
      // totalCount: count
    })
  )
}
 
const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
      totalCount: count,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(BANK_STATE, {
  [BankManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [BankManagementTypes.GET_ALL_ITEM_SUCCESS]: setAllItems,
  [BankManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [BankManagementTypes.CLEAR_ITEMS]: clearItems,
  [BankManagementTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
