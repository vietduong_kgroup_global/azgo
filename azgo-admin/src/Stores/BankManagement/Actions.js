import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List Bank actions
    getItemsRequest: ['filter'],
    getItemsSuccess: ['items'],
    getItemsFailure: null,
    clearItems: null,
     // Get List Bank actions By Offset Limit
     getAllItemRequest: ['filter'],
     getAllItemSuccess: ['items'],
     getAllItemFailure: null,
     clearAllItems: null,
    // Edit Bank actions
    editItemRequest: ['values'],
    editItemSuccess: ['item'],
    editItemFailure: null,
    // Create Bank actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
    // Delete Bank actions
    deleteItemRequest: ['id'],
    deleteItemSuccess: null,
    deleteItemFailure: null,
    // Get Bank Detail
    getItemDetailRequest: ['id'],
    getItemDetailSuccess: ['item'],
    getItemDetailFailure: null,
    // Change Filter
    setFilter: ['filter'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const BankManagementTypes = Types
export const BankManagementActions = Creators
