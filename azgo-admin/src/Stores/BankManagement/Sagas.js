import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { BankManagementTypes, BankManagementActions } from './Actions'
import { BankManagementService } from 'Services/BankManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { BankManagementSelectors } from 'Stores/BankManagement/Selectors'
import { fromJS } from 'immutable'
import { ConsoleSqlOutlined } from '@ant-design/icons'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      BankManagementService.getListItems,
      filter ? filter.toJS() : null
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: BankManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,

    })
  } catch (error) {
    yield put({
      type: BankManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}
function* getAllItemRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      BankManagementService.getAllItemRequest,
      filter ? filter.toJS() : null
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: BankManagementTypes.GET_ALL_ITEM_SUCCESS,
      allItem: response.data,
    })
  } catch (error) {
    yield put({
      type: BankManagementTypes.GET_ALL_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}
function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(BankManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: BankManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: BankManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* editItemRequest({ values }) {
  try {
    yield put(LoadingActions.showPopUp())
    const filter = yield select(BankManagementSelectors.getFilter)
    const response = yield call(BankManagementService.editItem, values)
    checkResponseError(response)
    yield put({
      type: BankManagementTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hidePopUp())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Chỉnh sửa thông tin ngân hàng thành công',
      },
    })
    yield put(
      BankManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: BankManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hidePopUp())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    yield put(LoadingActions.showPopUp())
    const filter = yield select(BankManagementSelectors.getFilter)
    const response = yield call(BankManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: BankManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hidePopUp())
    yield put(
      BankManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Tạo ngân hàng mới thành công',
      },
    })
    yield put(push('/admin/content-management/bank'))
  } catch (error) {
    yield put({
      type: BankManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(BankManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(BankManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: BankManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      BankManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Xóa ngân hàng thành công',
      },
    })
  } catch (error) {
    yield put({
      type: BankManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(BankManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(BankManagementTypes.GET_ALL_ITEM_REQUEST, getAllItemRequest),
    takeLatest(BankManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(BankManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(BankManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
    takeLatest(
      BankManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
