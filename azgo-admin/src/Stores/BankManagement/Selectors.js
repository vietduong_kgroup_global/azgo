import { MODULE_NAME } from './InitialState'

const getFilter = state => state[MODULE_NAME].get('filter')

const getItems = state => state[MODULE_NAME].get('items')

const getAllItem = state => state[MODULE_NAME].get('allItem')

const getItem = state => state[MODULE_NAME].get('item')

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

export const BankManagementSelectors = {
  getFilter,
  getItems,
  getItem,
  getAllItem,
  getTotalCount
}
