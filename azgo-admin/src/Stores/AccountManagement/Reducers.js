import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { AccountManagementTypes } from './Actions'
import reducerRegistry from 'Stores/Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
      },
      totalCount: 0,
    })
  )
}

const setItem = (state, { item }) => {
  if(!item) 
  return state.merge(
    fromJS({
      item: {},
    })
  )
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setItems = (state, { account }) => {
  return state.merge(
    fromJS({
      account,
    })
  )
}

const setItemsWithout = (state, { items }) => {
  return state.merge(
    fromJS({
      itemsWithout: items,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(INITIAL_STATE, {
  [AccountManagementTypes.GET_ACCOUNT_SUCCESS]: setItems,
  [AccountManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [AccountManagementTypes.CLEAR_ITEMS]: clearItems,
  [AccountManagementTypes.SET_FILTER]: setFilter,
  [AccountManagementTypes.GET_ITEMS_WITHOUT_SUCCESS]: setItemsWithout,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
