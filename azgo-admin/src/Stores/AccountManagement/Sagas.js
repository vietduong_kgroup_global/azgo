import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { AccountManagementTypes, AccountManagementActions } from './Actions'
import { AccountManagementService } from 'Services/AccountManagementService'
import { BlockUserService } from 'Services/BlockUser'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { AccountManagementSelectors } from 'Stores/AccountManagement/Selectors'
import { fromJS } from 'immutable'

function* getAccount({ filter }) {  
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      AccountManagementService.getAccountByAdmin,
      filter
    ) 
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: AccountManagementTypes.GET_ACCOUNT_SUCCESS,
      account: response.data || {},
    })
  } catch (error) {
    console.log('error', error)
    yield put({
      type: AccountManagementTypes.GET_ACCOUNT_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(AccountManagementService.getItemById, id)
    // checkResponseError(response)
    yield put(LoadingActions.hideLoadingList()) 
    yield put({
      type: AccountManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: AccountManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* editAccountRequest({ values }) {
  try {
    let data = {
      user_uuid: values.user_uuid,
      accountBankBranch: values.accountBankBranch,
      accountBankName: values.accountBankName,
      accountBankNumber: values.accountBankNumber,
      accountBankUserName: values.accountBankUserName,
      isActive: values.isActive,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(AccountManagementService.editAccount, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: AccountManagementTypes.EDIT_ACCOUNT_SUCCESS,
    })
    if (data.changeBlockUser === true && data.status === 0) {
      yield call(BlockUserService.unblockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 1) {
      yield call(BlockUserService.blockUser, data.id)
    }
    yield put(push('/admin/driver'))
  } catch (error) {
    console.log("error: ", error)
    yield put({
      type: AccountManagementTypes.EDIT_ACCOUNT_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(AccountManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: AccountManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/admin/additional-fee'))
  } catch (error) {
    yield put({
      type: AccountManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(AccountManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(AccountManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: AccountManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      AccountManagementActions.getAccount(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
    yield put(push('/admin/additional-fee'))

  } catch (error) {
    yield put({
      type: AccountManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemsWithoutRequest({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      AccountManagementService.getListWithoutItems,
      values
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: AccountManagementTypes.GET_ITEMS_WITHOUT_SUCCESS,
      items: response.data.results,
    })
  } catch (error) {
    yield put({
      type: AccountManagementTypes.GET_ITEMS_WITHOUT_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(AccountManagementTypes.GET_ACCOUNT, getAccount),
    takeLatest(AccountManagementTypes.EDIT_ACCOUNT, editAccountRequest),
    takeLatest(AccountManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(
      AccountManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
    takeLatest(AccountManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
    takeLatest(
      AccountManagementTypes.GET_ITEMS_WITHOUT_REQUEST,
      getItemsWithoutRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
