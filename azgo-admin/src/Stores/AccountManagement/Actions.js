import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List Driver actions
    getAccount: ['filter'],
    getAccountSuccess: ['account'],
    getAccountFailure: null,
    clearItems: null,
    // Get List Driver actions(Without)
    getItemsWithoutRequest: null,
    getItemsWithoutSuccess: ['items'],
    getItemsWithoutFailure: null,
    // Edit Driver actions
    editAccount: ['values'],
    editAccountSuccess: ['item'],
    editAccountFailure: null,
    // Create Driver actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
    // Delete Driver actions
    deleteItemRequest: ['id'],
    deleteItemSuccess: null,
    deleteItemFailure: null,
    // Get Driver Detail
    getItemDetailRequest: ['id'],
    getItemDetailSuccess: ['item'],
    getItemDetailFailure: null,
    // Change Filter
    setFilter: ['filter'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const AccountManagementTypes = Types
export const AccountManagementActions = Creators
