import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { fromJS } from 'immutable'
import { createReducer } from 'reduxsauce'
import { ResetPasswordTypes, ResetPasswordActions } from './Actions'
import reducerRegistry from 'Stores/Reducers/ReducerRegistry'
import { STATUS_CHECK_SUCCESS } from 'Utils/enum'

const requestPasswordSuccess = state => {
  return state.merge(
    fromJS({
      resetPwdErrorMsg: null,
      status: STATUS_CHECK_SUCCESS.SUCCESS,
    })
  )
}
const requestPasswordFailed = (state, { message }) => {
  return state.merge(
    fromJS({
      resetPwdErrorMsg: message,
      status: STATUS_CHECK_SUCCESS.FAILED,
    })
  )
}

const clearErrorMsg = state => {
  return state.merge(
    fromJS({
      resetPwdErrorMsg: null,
      status: null,
    })
  )
}

const newPasswordSuccess = state => {
  return state.merge(
    fromJS({
      statusNewPassword: STATUS_CHECK_SUCCESS.SUCCESS,
      createNewPasswordSuccess: true,
    })
  )
}
const newPasswordFailed = state => {
  return state.merge(
    fromJS({
      statusNewPassword: STATUS_CHECK_SUCCESS.FAILED,
      createNewPasswordSuccess: false,
    })
  )
}

const clearNewPassword = state => {
  return state.merge(
    fromJS({
      statusNewPassword: null,
    })
  )
}

const clearStatusSuccess = state => {
  return state.merge(
    fromJS({
      createNewPasswordSuccess: false,
    })
  )
}
const setCheckTokenSuccess = state => {
  return state.merge(
    fromJS({
      createNewPasswordSuccess: false,
    })
  )
}
const reducer = createReducer(INITIAL_STATE, {
  [ResetPasswordTypes.REQUEST_RESET_PASSWORD_SUCCESS]: requestPasswordSuccess,
  [ResetPasswordTypes.REQUEST_RESET_PASSWORD_FAILED]: requestPasswordFailed,
  [ResetPasswordTypes.CLEAR_RESET_PASSWORD]: clearErrorMsg,
  [ResetPasswordTypes.CREATE_NEW_PASSWORD_SUCCESS]: newPasswordSuccess,
  [ResetPasswordTypes.CREATE_NEW_PASSWORD_FAILED]: newPasswordFailed,
  [ResetPasswordTypes.CLEAR_NEW_PASSWORD]: clearNewPassword,
  [ResetPasswordTypes.CLEAR_STATUS_SUCCESS]: clearStatusSuccess,
})

reducerRegistry.register(MODULE_NAME, reducer)
export default reducer
