import { put, call, takeLatest, all } from 'redux-saga/effects'
import {
  ResetPasswordActions,
  ResetPasswordTypes,
} from 'Stores/ResetPassword/Actions'
import { AuthService } from 'Services/AuthService'
import { checkResponseError } from 'Utils/checkResponseError'
import sagaRegistry from '../Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { push } from 'connected-react-router'
import { message } from 'antd'

function* requestResetPassword({ email }) {
  try {
    const response = yield call(AuthService.requestResetPassword, {
      email: email,
    })
    checkResponseError(response)
    yield put(ResetPasswordActions.requestResetPasswordSuccess())
  } catch (error) {
    yield put({
      type: ResetPasswordTypes.REQUEST_RESET_PASSWORD_FAILED,
      message: error.message,
    })
  }
}
function* createNewPassword({ password, passwordConfirm, token }) {
  try {
    const response = yield call(AuthService.createNewPassword, {
      password,
      password_confirmation: passwordConfirm,
      token,
    })
    checkResponseError(response)
    yield put(ResetPasswordActions.createNewPasswordSuccess())
    yield put(push('/login'))
  } catch (error) {
    yield put(ResetPasswordActions.createNewPasswordFailed())
    message.error('Error. Token has expired.', 3)
  }
}
function* checkTokenResetPassword({ token }) {
  try {
    const response = yield call(AuthService.checkTokenResetPassword, token)
    checkResponseError(response)
  } catch (error) {
    yield put(push('/login'))
    // message.error('Error. Token has expired.', 3)
  }
}
function* watcher() {
  yield all([
    takeLatest(ResetPasswordTypes.REQUEST_RESET_PASSWORD, requestResetPassword),
    takeLatest(ResetPasswordTypes.CREATE_NEW_PASSWORD, createNewPassword),
    takeLatest(
      ResetPasswordTypes.CHECK_TOKEN_RESET_PASSWORD,
      checkTokenResetPassword
    ),
  ])
}

sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
