import { MODULE_NAME } from './InitialState'

const getResetPwdErrorMsg = state => {
  return state[MODULE_NAME].get('resetPwdErrorMsg')
    ? state[MODULE_NAME].get('resetPwdErrorMsg')
    : null
}

const getStatus = state => {
  return state[MODULE_NAME].get('status')
    ? state[MODULE_NAME].get('status')
    : null
}

const getStatusNewPassword = state => {
  return state[MODULE_NAME].get('statusNewPassword')
    ? state[MODULE_NAME].get('statusNewPassword')
    : null
}
const getCreateNewPasswordSuccess = state => {
  return state[MODULE_NAME].get('createNewPasswordSuccess')
    ? state[MODULE_NAME].get('createNewPasswordSuccess')
    : null
}

export const ResetPasswordSelectors = {
  getResetPwdErrorMsg,
  getStatus,
  getStatusNewPassword,
  getCreateNewPasswordSuccess,
}
