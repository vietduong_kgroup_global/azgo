import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    checkTokenResetPassword: ['token'],
    checkTokenResetPasswordSuccess: null,
    // request reset password
    requestResetPassword: ['email'],
    requestResetPasswordSuccess: null,
    requestResetPasswordFailed: ['message'],
    clearResetPassword: null,

    createNewPassword: ['password', 'passwordConfirm', 'token'],
    createNewPasswordSuccess: null,
    createNewPasswordFailed: null,
    clearNewPassword: null,
    clearStatusSuccess: null,
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const ResetPasswordTypes = Types
export const ResetPasswordActions = Creators
