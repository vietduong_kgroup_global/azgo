import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'resetPwd'

export const INITIAL_STATE = fromJS({
  resetPwdErrorMsg: null,
  status: null,
  statusNewPassword: null,
  createNewPasswordSuccess: false,
})
