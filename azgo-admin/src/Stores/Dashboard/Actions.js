import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get Amount Customer Bus actions
    getItemAmountCustomerBus: ['filter'],
    getItemAmountCustomerBusSuccess: ['count'],
    getItemAmountCustomerBusFailure: null,

    // Get Amount Schedule Bus actions
    getItemAmountScheduleBus: ['filter'],
    getItemAmountScheduleBusSuccess: ['count'],
    getItemAmountScheduleBusFailure: null,

    // Get Amount Schedule Bus Done actions
    getItemAmountScheduleDoneBus: ['filter'],
    getItemAmountScheduleDoneBusSuccess: ['count'],
    getItemAmountScheduleDoneBusFailure: null,

    // Get Rating Provider
    getItemRatingProvider: null,
    getItemRatingProviderSuccess: ['count'],
    getItemRatingProviderFailure: null,

    // Get Revenue bus
    getItemRevenueBus: ['filter'],
    getItemRevenueBusSuccess: ['count'],
    getItemRevenueBusFailure: null,

    // Get Amount Order Bike actions
    getItemAmountOrderBike: ['filter'],
    getItemAmountOrderBikeSuccess: ['count'],
    getItemAmountOrderBikeFailure: null,

    // Get Amount Order Month Bike actions
    getItemAmountOrderMonthBike: ['filter'],
    getItemAmountOrderMonthBikeSuccess: ['count'],
    getItemAmountOrderMonthBikeFailure: null,

    // Get Amount Order Month Bike Done actions
    getItemAmountOrderMonthDoneBike: ['filter'],
    getItemAmountOrderMonthDoneBikeSuccess: ['count'],
    getItemAmountOrderMonthDoneBikeFailure: null,

    // Get Revenue bike
    getItemRevenueBike: ['filter'],
    getItemRevenueBikeSuccess: ['count'],
    getItemRevenueBikeFailure: null,

    // Get Amount Order Van/bacgac actions
    getItemAmountOrderVan: ['filter'],
    getItemAmountOrderVanSuccess: ['count'],
    getItemAmountOrderVanFailure: null,

    // Get Amount Order Month Van/bagac actions
    getItemAmountOrderMonthVan: ['filter'],
    getItemAmountOrderMonthVanSuccess: ['count'],
    getItemAmountOrderMonthVanFailure: null,

    // Get Amount Order Month Van/bagac Done actions
    getItemAmountOrderMonthDoneVan: ['filter'],
    getItemAmountOrderMonthDoneVanSuccess: ['count'],
    getItemAmountOrderMonthDoneVanFailure: null,

    // Get Revenue Van
    getItemRevenueVan: ['filter'],
    getItemRevenueVanSuccess: ['count'],
    getItemRevenueVanFailure: null,

    // Get Line Chart
    getItemsLineChart: ['filter'],
    getItemsLineChartSuccess: null,
    getItemsLineChartFailure: null,

    // Get Line Chart Provider
    getItemsLineChartProvider: ['filter'],
    getItemsLineChartProviderSuccess: null,
    getItemsLineChartProviderFailure: null,

    // Get vehicle schedule nearest
    getItemsVehicleScheduleNearest: ['filter'],
    getItemsVehicleScheduleNearestSuccess: null,
    getItemsVehicleScheduleNearestFailure: null,

    // Get vehicle schedule nearest not
    getItemsVehicleScheduleNearestNot: ['filter'],
    getItemsVehicleScheduleNearestNotSuccess: null,
    getItemsVehicleScheduleNearestNotFailure: null,

    getItemsRevenueRequest: ['filter'],
    getItemsRevenueSuccess: ['items', 'count'],
    getItemsRevenueFailure: null,

    // Change Filter
    setFilter: ['filter'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const DashboardManagementTypes = Types
export const DashboardManagementActions = Creators
