import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { DashboardManagementTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const setItemAmountCustomerBus = (state, { count }) => {
  return state.merge(
    fromJS({
      itemAmountCustomerBus: count,
    })
  )
}

const setItemAmountScheduleBus = (state, { count }) => {
  return state.merge(
    fromJS({
      itemAmountScheduleBus: count,
    })
  )
}

const setItemAmountScheduleDoneBus = (state, { count }) => {
  return state.merge(
    fromJS({
      itemAmountScheduleDoneBus: count,
    })
  )
}

const setItemRevenueBus = (state, { count }) => {
  return state.merge(
    fromJS({
      itemRevenueBus: count,
    })
  )
}

const setItemRatingProvider = (state, { count }) => {
  return state.merge(
    fromJS({
      itemRatingProvider: count,
    })
  )
}

const setItemAmountOrderBike = (state, { count }) => {
  return state.merge(
    fromJS({
      itemAmountOrderBike: count,
    })
  )
}

const setItemAmountOrderMonthBike = (state, { count }) => {
  return state.merge(
    fromJS({
      itemAmountOrderMonthBike: count,
    })
  )
}

const setItemAmountOrderMonthDoneBike = (state, { count }) => {
  return state.merge(
    fromJS({
      itemAmountOrderMonthDoneBike: count,
    })
  )
}

const setItemRevenueBike = (state, { count }) => {
  return state.merge(
    fromJS({
      itemRevenueBike: count,
    })
  )
}

const setItemAmountOrderVan = (state, { count }) => {
  return state.merge(
    fromJS({
      itemAmountOrderVan: count,
    })
  )
}

const setItemAmountOrderMonthVan = (state, { count }) => {
  return state.merge(
    fromJS({
      itemAmountOrderMonthVan: count,
    })
  )
}

const setItemAmountOrderMonthDoneVan = (state, { count }) => {
  return state.merge(
    fromJS({
      itemAmountOrderMonthDoneVan: count,
    })
  )
}

const setItemRevenueVan = (state, { count }) => {
  return state.merge(
    fromJS({
      itemRevenueVan: count,
    })
  )
}

const setItemsLineChart = (state, { items }) => {
  return state.merge(
    fromJS({
      itemsLineChart: items,
    })
  )
}

const setItemsLineChartProvider = (state, { items }) => {
  return state.merge(
    fromJS({
      itemsLineChartProvider: items,
    })
  )
}

// provider
const setItemsVehicleScheduleNearest = (state, { items }) => {
  return state.merge(
    fromJS({
      itemsVehicleScheduleNearest: items,
    })
  )
}

const setItemsVehicleScheduleNearestNot = (state, { items }) => {
  return state.merge(
    fromJS({
      itemsVehicleScheduleNearestNot: items,
    })
  )
}

const setItemsRevenue = (state, { items, count }) => {
  return state.merge(
    fromJS({
      itemsRevenue: items,
      totalCount: count,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(INITIAL_STATE, {
  [DashboardManagementTypes.GET_ITEM_AMOUNT_CUSTOMER_BUS_SUCCESS]: setItemAmountCustomerBus,
  [DashboardManagementTypes.GET_ITEM_AMOUNT_SCHEDULE_BUS_SUCCESS]: setItemAmountScheduleBus,
  [DashboardManagementTypes.GET_ITEM_AMOUNT_SCHEDULE_DONE_BUS_SUCCESS]: setItemAmountScheduleDoneBus,
  [DashboardManagementTypes.GET_ITEM_REVENUE_BUS_SUCCESS]: setItemRevenueBus,
  [DashboardManagementTypes.GET_ITEM_RATING_PROVIDER_SUCCESS]: setItemRatingProvider,
  [DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_BIKE_SUCCESS]: setItemAmountOrderBike,
  [DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_BIKE_SUCCESS]: setItemAmountOrderMonthBike,
  [DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_DONE_BIKE_SUCCESS]: setItemAmountOrderMonthDoneBike,
  [DashboardManagementTypes.GET_ITEM_REVENUE_BIKE_SUCCESS]: setItemRevenueBike,
  [DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_VAN_SUCCESS]: setItemAmountOrderVan,
  [DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_VAN_SUCCESS]: setItemAmountOrderMonthVan,
  [DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_DONE_VAN_SUCCESS]: setItemAmountOrderMonthDoneVan,
  [DashboardManagementTypes.GET_ITEM_REVENUE_VAN_SUCCESS]: setItemRevenueVan,
  [DashboardManagementTypes.GET_ITEMS_LINE_CHART_SUCCESS]: setItemsLineChart,
  [DashboardManagementTypes.GET_ITEMS_LINE_CHART_PROVIDER_SUCCESS]: setItemsLineChartProvider,
  [DashboardManagementTypes.GET_ITEMS_VEHICLE_SCHEDULE_NEAREST_SUCCESS]: setItemsVehicleScheduleNearest,
  [DashboardManagementTypes.GET_ITEMS_VEHICLE_SCHEDULE_NEAREST_NOT_SUCCESS]: setItemsVehicleScheduleNearestNot,
  [DashboardManagementTypes.GET_ITEMS_REVENUE_SUCCESS]: setItemsRevenue,
  [DashboardManagementTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
