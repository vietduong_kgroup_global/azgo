import { put, call, takeLatest, all } from 'redux-saga/effects'
import { DashboardManagementTypes } from './Actions'
import { DashboardManagementService } from 'Services/DashboardManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'

function* getItemAmountCustomerBus({ filter }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      DashboardManagementService.getItemAmountCustomerBus,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: DashboardManagementTypes.GET_ITEM_AMOUNT_CUSTOMER_BUS_SUCCESS,
      count: response.data.amountCustomer,
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEM_AMOUNT_CUSTOMER_BUS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemAmountScheduleBus({ filter }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      DashboardManagementService.getItemAmountScheduleBus,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: DashboardManagementTypes.GET_ITEM_AMOUNT_SCHEDULE_BUS_SUCCESS,
      count: response.data,
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEM_AMOUNT_SCHEDULE_BUS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemAmountScheduleDoneBus({ filter }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      DashboardManagementService.getItemAmountScheduleDoneBus,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: DashboardManagementTypes.GET_ITEM_AMOUNT_SCHEDULE_DONE_BUS_SUCCESS,
      count: response.data,
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEM_AMOUNT_SCHEDULE_DONE_BUS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemRevenueBus({ filter }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      DashboardManagementService.getItemRevenueBus,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: DashboardManagementTypes.GET_ITEM_REVENUE_BUS_SUCCESS,
      count: response.data,
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEM_REVENUE_BUS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemRatingProvider() {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      DashboardManagementService.getItemRatingProvider
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: DashboardManagementTypes.GET_ITEM_RATING_PROVIDER_SUCCESS,
      count: response.data.avg,
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEM_RATING_PROVIDER_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemAmountOrderBike({ filter }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      DashboardManagementService.getItemAmountOrderBike,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_BIKE_SUCCESS,
      count: response.data.amountCustomer,
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_BIKE_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemAmountOrderMonthBike({ filter }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      DashboardManagementService.getItemAmountOrderMonthBike,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_BIKE_SUCCESS,
      count: response.data,
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_BIKE_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemAmountOrderMonthDoneBike({ filter }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      DashboardManagementService.getItemAmountOrderMonthDoneBike,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type:
        DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_DONE_BIKE_SUCCESS,
      count: response.data,
    })
  } catch (error) {
    yield put({
      type:
        DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_DONE_BIKE_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemRevenueBike({ filter }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      DashboardManagementService.getItemRevenueBike,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: DashboardManagementTypes.GET_ITEM_REVENUE_BIKE_SUCCESS,
      count: response.data,
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEM_REVENUE_BIKE_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

// van
function* getItemAmountOrderVan({ filter }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      DashboardManagementService.getItemAmountOrderVan,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_VAN_SUCCESS,
      count: response.data.amountCustomer,
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_VAN_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemAmountOrderMonthVan({ filter }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      DashboardManagementService.getItemAmountOrderMonthVan,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_VAN_SUCCESS,
      count: response.data,
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_VAN_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemAmountOrderMonthDoneVan({ filter }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      DashboardManagementService.getItemAmountOrderMonthDoneVan,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type:
        DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_DONE_VAN_SUCCESS,
      count: response.data,
    })
  } catch (error) {
    yield put({
      type:
        DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_DONE_VAN_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemRevenueVan({ filter }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      DashboardManagementService.getItemRevenueVan,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: DashboardManagementTypes.GET_ITEM_REVENUE_VAN_SUCCESS,
      count: response.data,
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEM_REVENUE_VAN_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemsLineChartRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      DashboardManagementService.getItemsLineChart,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: DashboardManagementTypes.GET_ITEMS_LINE_CHART_SUCCESS,
      items: response.data,
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEMS_LINE_CHART_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemsLineChartProviderRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      DashboardManagementService.getItemsLineChartProvider,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: DashboardManagementTypes.GET_ITEMS_LINE_CHART_PROVIDER_SUCCESS,
      items: response.data,
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEMS_LINE_CHART_PROVIDER_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

// provider
function* getItemsVehicleScheduleNearest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      DashboardManagementService.getItemsVehicleScheduleNearest,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: DashboardManagementTypes.GET_ITEMS_VEHICLE_SCHEDULE_NEAREST_SUCCESS,
      items: response.data,
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEMS_VEHICLE_SCHEDULE_NEAREST_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemsVehicleScheduleNearestNot({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      DashboardManagementService.getItemsVehicleScheduleNearest,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type:
        DashboardManagementTypes.GET_ITEMS_VEHICLE_SCHEDULE_NEAREST_NOT_SUCCESS,
      items: response.data,
    })
  } catch (error) {
    yield put({
      type:
        DashboardManagementTypes.GET_ITEMS_VEHICLE_SCHEDULE_NEAREST_NOT_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemsRevenueRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      DashboardManagementService.getListItemsRevenue,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: DashboardManagementTypes.GET_ITEMS_REVENUE_SUCCESS,
      items: response.data.results,
      count: parseInt(response.data.count, 10),
    })
  } catch (error) {
    yield put({
      type: DashboardManagementTypes.GET_ITEMS_REVENUE_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* watcher() {
  yield all([
    takeLatest(
      DashboardManagementTypes.GET_ITEM_AMOUNT_CUSTOMER_BUS,
      getItemAmountCustomerBus
    ),
    takeLatest(
      DashboardManagementTypes.GET_ITEM_AMOUNT_SCHEDULE_BUS,
      getItemAmountScheduleBus
    ),
    takeLatest(
      DashboardManagementTypes.GET_ITEM_AMOUNT_SCHEDULE_DONE_BUS,
      getItemAmountScheduleDoneBus
    ),
    takeLatest(
      DashboardManagementTypes.GET_ITEM_REVENUE_BUS,
      getItemRevenueBus
    ),
    takeLatest(
      DashboardManagementTypes.GET_ITEM_RATING_PROVIDER,
      getItemRatingProvider
    ),
    // bike
    takeLatest(
      DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_BIKE,
      getItemAmountOrderBike
    ),
    takeLatest(
      DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_BIKE,
      getItemAmountOrderMonthBike
    ),
    takeLatest(
      DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_DONE_BIKE,
      getItemAmountOrderMonthDoneBike
    ),
    takeLatest(
      DashboardManagementTypes.GET_ITEM_REVENUE_BIKE,
      getItemRevenueBike
    ),
    // bagac
    takeLatest(
      DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_VAN,
      getItemAmountOrderVan
    ),
    takeLatest(
      DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_VAN,
      getItemAmountOrderMonthVan
    ),
    takeLatest(
      DashboardManagementTypes.GET_ITEM_AMOUNT_ORDER_MONTH_DONE_VAN,
      getItemAmountOrderMonthDoneVan
    ),
    takeLatest(
      DashboardManagementTypes.GET_ITEM_REVENUE_VAN,
      getItemRevenueVan
    ),
    takeLatest(
      DashboardManagementTypes.GET_ITEMS_LINE_CHART,
      getItemsLineChartRequest
    ),
    takeLatest(
      DashboardManagementTypes.GET_ITEMS_LINE_CHART_PROVIDER,
      getItemsLineChartProviderRequest
    ),
    // provider
    takeLatest(
      DashboardManagementTypes.GET_ITEMS_VEHICLE_SCHEDULE_NEAREST,
      getItemsVehicleScheduleNearest
    ),
    takeLatest(
      DashboardManagementTypes.GET_ITEMS_VEHICLE_SCHEDULE_NEAREST_NOT,
      getItemsVehicleScheduleNearestNot
    ),
    takeLatest(
      DashboardManagementTypes.GET_ITEMS_REVENUE_REQUEST,
      getItemsRevenueRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
