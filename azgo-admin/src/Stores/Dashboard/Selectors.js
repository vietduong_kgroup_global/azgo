import { MODULE_NAME } from './InitialState'

const getItemAmountCustomerBus = state =>
  // bus
  state[MODULE_NAME].get('itemAmountCustomerBus')
const getItemAmountScheduleBus = state =>
  state[MODULE_NAME].get('itemAmountScheduleBus')
const getItemAmountScheduleDoneBus = state =>
  state[MODULE_NAME].get('itemAmountScheduleDoneBus')
const getItemRevenueBus = state => state[MODULE_NAME].get('itemRevenueBus')
const getItemRatingProvider = state =>
  state[MODULE_NAME].get('itemRatingProvider')
// bike
const getItemAmountOrderBike = state =>
  state[MODULE_NAME].get('itemAmountOrderBike')
const getItemAmountOrderMonthBike = state =>
  state[MODULE_NAME].get('itemAmountOrderMonthBike')
const getItemAmountOrderMonthDoneBike = state =>
  state[MODULE_NAME].get('itemAmountOrderMonthDoneBike')
const getItemRevenueBike = state => state[MODULE_NAME].get('itemRevenueBike')
// van
const getItemAmountOrderVan = state =>
  state[MODULE_NAME].get('itemAmountOrderVan')
const getItemAmountOrderMonthVan = state =>
  state[MODULE_NAME].get('itemAmountOrderMonthVan')
const getItemAmountOrderMonthDoneVan = state =>
  state[MODULE_NAME].get('itemAmountOrderMonthDoneVan')
const getItemRevenueVan = state => state[MODULE_NAME].get('itemRevenueVan')
// line chart
const getItemsLineChart = state => state[MODULE_NAME].get('itemsLineChart')
const getFilter = state => state[MODULE_NAME].get('filter')

// line chart provider
const getItemsLineChartProvider = state =>
  state[MODULE_NAME].get('itemsLineChartProvider')

// provider
const getItemsVehicleScheduleNearest = state =>
  state[MODULE_NAME].get('itemsVehicleScheduleNearest')
const getItemsVehicleScheduleNearestNot = state =>
  state[MODULE_NAME].get('itemsVehicleScheduleNearestNot')

const getItemsRevenue = state => state[MODULE_NAME].get('itemsRevenue')

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

export const DashboardManagementSelectors = {
  // bus
  getItemAmountCustomerBus,
  getItemAmountScheduleBus,
  getItemAmountScheduleDoneBus,
  getItemRevenueBus,
  getItemRatingProvider,
  // bike
  getItemAmountOrderBike,
  getItemAmountOrderMonthBike,
  getItemAmountOrderMonthDoneBike,
  getItemRevenueBike,
  // van
  getItemAmountOrderVan,
  getItemAmountOrderMonthVan,
  getItemAmountOrderMonthDoneVan,
  getItemRevenueVan,
  //  line chart
  getItemsLineChart,
  getFilter,
  // line chart provider
  getItemsLineChartProvider,
  //  provider
  getItemsVehicleScheduleNearest,
  getItemsVehicleScheduleNearestNot,
  getItemsRevenue,
  getTotalCount,
}
