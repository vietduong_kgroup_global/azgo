import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'DashboardManagement'

export const INITIAL_STATE = fromJS({
  itemAmountCustomerBus: null,
  itemAmountScheduleBus: null,
  itemAmountScheduleDoneBus: null,
  itemRevenueBus: null,
  itemRatingProvider: null,
  itemAmountOrderBike: null,
  itemAmountOrderMonthBike: null,
  itemAmountOrderMonthDoneBike: null,
  itemRevenueBike: null,
  itemAmountOrderVan: null,
  itemAmountOrderMonthVan: null,
  itemAmountOrderMonthDoneVan: null,
  itemRevenueVan: null,
  itemsLineChart: [],
  itemsLineChartProvider: [],
  itemsVehicleScheduleNearest: [],
  itemsVehicleScheduleNearestNot: [],
  filter: {
    month: null,
    year: null,
  },
  itemsRevenue: [],
  totalCount: 0,
})
