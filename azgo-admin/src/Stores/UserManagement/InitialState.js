import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'UserManagement'

export const INITIAL_STATE = fromJS({
  items: [],
  item: {},
  userType: [],
  filter: {
    per_page: 10,
    page: 1,
    keyword: '',
  },
  totalCount: 0,
})
