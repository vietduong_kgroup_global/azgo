import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { UserManagementTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
      },
      totalCount: 0,
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setUserType = (state, {userType}) => { 
  return state.merge(
    fromJS({
      userType,
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
      totalCount: count,
    })
  )
}

const setFilter = (state, { filter }) => {
  return state.mergeIn(['filter'], fromJS(filter))
}

const reducer = createReducer(INITIAL_STATE, {
  [UserManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [UserManagementTypes.GET_USER_TYPE_SUCCESS]: setUserType,
  [UserManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [UserManagementTypes.CLEAR_ITEMS]: clearItems,
  [UserManagementTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
