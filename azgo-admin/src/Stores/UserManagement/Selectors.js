import { MODULE_NAME } from './InitialState'

const getFilter = state => state[MODULE_NAME].get('filter')

const getItems = state => state[MODULE_NAME].get('items')

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

const getItem = state => state[MODULE_NAME].get('item')

const getUserType = state => state[MODULE_NAME].get('userType')

export const UserManagementSelectors = {
  getFilter,
  getItems,
  getTotalCount,
  getItem,
  getUserType
}
