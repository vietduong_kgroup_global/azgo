import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { UserManagementActions, UserManagementTypes } from './Actions'
import { UserManagementSelectors } from './Selectors'
import { UserManagementService } from 'Services/UserManagementService'
import { BlockUserService } from 'Services/BlockUser'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { fromJS } from 'immutable'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'

function* getItemsWorker({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      UserManagementService.getListItems,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: UserManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: UserManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailWorker({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(UserManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: UserManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: UserManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getUserType({ filter }) { 
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      UserManagementService.getUserType
    )
    checkResponseError(response) 
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: UserManagementTypes.GET_USER_TYPE_SUCCESS,
      userType: response.data,
    })
  } catch (error) { 
    // yield put({
    //   type: UserManagementTypes.GET_ITEMS_FAILURE,
    // })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* createItemWorker({ values }) {
  try {
    const response = yield call(UserManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: UserManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/admin/internal-management'))
  } catch (error) {
    yield put({
      type: UserManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* editItemWorker({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(UserManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: UserManagementTypes.EDIT_ITEM_SUCCESS,
    })
    if (data.changeBlockUser === true && data.status === 0) {
      yield call(BlockUserService.unblockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 1) {
      yield call(BlockUserService.blockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 0) {
      yield call(BlockUserService.unblockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 1) {
      yield call(BlockUserService.blockUser, data.id)
    }
    yield put(push('/admin/internal-management'))
  } catch (error) {
    yield put({
      type: UserManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemWorker({ id }) { 
  try { 
    const filter = yield select(UserManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(UserManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: UserManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      UserManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: UserManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(UserManagementTypes.GET_ITEMS_REQUEST, getItemsWorker),
    takeLatest(UserManagementTypes.GET_USER_TYPE, getUserType),
    takeLatest(
      UserManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailWorker
    ),
    takeLatest(UserManagementTypes.CREATE_ITEM_REQUEST, createItemWorker),
    takeLatest(UserManagementTypes.EDIT_ITEM_REQUEST, editItemWorker),
    takeLatest(UserManagementTypes.DELETE_ITEM_REQUEST, deleteItemWorker),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
