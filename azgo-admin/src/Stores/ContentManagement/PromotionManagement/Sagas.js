import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { PromotionManagementTypes, PromotionManagementActions } from './Actions'
import { PromotionManagementService } from 'Services/ContentManagement/PromotionManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { PromotionManagementSelectors } from 'Stores/ContentManagement/PromotionManagement/Selectors'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      PromotionManagementService.getListItems,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: PromotionManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: PromotionManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(PromotionManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: PromotionManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: PromotionManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(PromotionManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: PromotionManagementTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(push('/admin/content-management/promotion'))
  } catch (error) {
    yield put({
      type: PromotionManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(PromotionManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: PromotionManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/admin/content-management/promotion'))
  } catch (error) {
    yield put({
      type: PromotionManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(PromotionManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(PromotionManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: PromotionManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      PromotionManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: PromotionManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(PromotionManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(PromotionManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(PromotionManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(PromotionManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
    takeLatest(
      PromotionManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
