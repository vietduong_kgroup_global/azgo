import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List Promotion actions
    getItemsRequest: ['filter'],
    getItemsSuccess: ['items', 'count'],
    getItemsFailure: null,
    clearItems: null,
    // Edit Promotion actions
    editItemRequest: ['values'],
    editItemSuccess: ['item'],
    editItemFailure: null,
    // Create Promotion actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
    // Delete Promotion actions
    deleteItemRequest: ['id'],
    deleteItemSuccess: null,
    deleteItemFailure: null,
    // Get Promotion Detail
    getItemDetailRequest: ['id'],
    getItemDetailSuccess: ['item'],
    getItemDetailFailure: null,
    // Change Filter
    setFilter: ['filter'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const PromotionManagementTypes = Types
export const PromotionManagementActions = Creators
