import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { BannerManagementTypes } from './Actions'
import reducerRegistry from 'Stores/Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setItems = (state, { items }) => {
  return state.merge(
    fromJS({
      items,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(INITIAL_STATE, {
  [BannerManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [BannerManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [BannerManagementTypes.CLEAR_ITEMS]: clearItems,
  [BannerManagementTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
