import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { BannerManagementTypes, BannerManagementActions } from './Actions'
import { BannerManagementService } from 'Services/ContentManagement/BannerManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'

function* getItemsRequest() {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(BannerManagementService.getListItems)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: BannerManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data,
    })
  } catch (error) {
    yield put({
      type: BannerManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(BannerManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: BannerManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: BannerManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(BannerManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: BannerManagementTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(push('/admin/content-management/banner'))
  } catch (error) {
    yield put({
      type: BannerManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(BannerManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: BannerManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/admin/content-management/banner'))
  } catch (error) {
    yield put({
      type: BannerManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* watcher() {
  yield all([
    takeLatest(BannerManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(BannerManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(BannerManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(
      BannerManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
