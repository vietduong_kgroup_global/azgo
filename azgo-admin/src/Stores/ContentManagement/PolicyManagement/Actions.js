import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Edit
    editItemRequest: ['values'],
    editItemSuccess: ['item'],
    editItemFailure: null,
    clearItems: null,
    // Get Detail
    getItemDetailRequest: null,
    getItemDetailSuccess: ['item'],
    getItemDetailFailure: null,
    // Create Client actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const PolicyManagementTypes = Types
export const PolicyManagementActions = Creators
