import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { PolicyManagementTypes } from './Actions'
import reducerRegistry from 'Stores/Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      item: {},
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const reducer = createReducer(INITIAL_STATE, {
  [PolicyManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [PolicyManagementTypes.CLEAR_ITEMS]: clearItems,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
