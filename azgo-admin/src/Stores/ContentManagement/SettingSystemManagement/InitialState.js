import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'SettingSystemManagement'

export const INITIAL_STATE = fromJS({
  item: {},
})
