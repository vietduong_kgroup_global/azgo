import { MODULE_NAME } from './InitialState'
const getItem = state => state[MODULE_NAME].get('item')

export const SettingSystemManagementSelectors = {
  getItem,
}
