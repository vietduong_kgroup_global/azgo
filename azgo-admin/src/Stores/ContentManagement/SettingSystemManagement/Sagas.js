import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { SettingSystemManagementTypes, SettingSystemManagementActions } from './Actions'
import { SettingSystemManagementService } from 'Services/ContentManagement/SettingSystemManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { push } from 'connected-react-router'

function* getItemDetailRequest() {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(SettingSystemManagementService.getItem)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: SettingSystemManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: SettingSystemManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(SettingSystemManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: SettingSystemManagementTypes.EDIT_ITEM_SUCCESS,
    })
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Chỉnh sửa thành công',
      },
    })
  } catch (error) {
    yield put({
      type: SettingSystemManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}
function* createItemRequest({ values }) {
  try {
    const response = yield call(SettingSystemManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: SettingSystemManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Chỉnh sửa thành công',
      },
    })
  } catch (error) {
    yield put({
      type: SettingSystemManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}
function* watcher() {
  yield all([
    takeLatest(SettingSystemManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(
      SettingSystemManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
    takeLatest(SettingSystemManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
