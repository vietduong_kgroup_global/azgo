import { MODULE_NAME } from './InitialState'

const getFilterDriverWallet = state => state[MODULE_NAME].get('filterDriverWallet')

const getFilterRevenueOrder = state => state[MODULE_NAME].get('filterRevenueOrder')

const getItems = state => state[MODULE_NAME].get('items')

const getTotalCountDriverWallet = state => state[MODULE_NAME].get('totalCountDriverWallet')

const getTotalCountRevenueOrder = state => state[MODULE_NAME].get('totalCountRevenueOrder')

const getItem = state => state[MODULE_NAME].get('item')

const getRevenueOrder = state => state[MODULE_NAME].get('revenueOrder') 

const getDriverWallet = state => state[MODULE_NAME].get('driverWallet') 

const getCurrentPageDriverWallet = state => state[MODULE_NAME].get('currentPageDriverWallet')

const getCurrentPageRevenueOrder = state => state[MODULE_NAME].get('currentPageRevenueOrder') 

export const ReportSelectors = {
  getFilterDriverWallet,
  getFilterRevenueOrder,
  getItems,
  getRevenueOrder,
  getDriverWallet,
  getTotalCountRevenueOrder,
  getTotalCountDriverWallet,
  getItem,
  getCurrentPageDriverWallet,
  getCurrentPageRevenueOrder
}
