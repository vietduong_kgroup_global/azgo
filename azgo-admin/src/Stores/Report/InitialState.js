import { fromJS } from 'immutable'
import { formatDate } from 'Utils/helper'
import moment from 'moment'
let date = new Date()
// date.setFullYear(14, 0, 1);
/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'Report'

export const INITIAL_STATE = fromJS({
  // items: [],
  // item: {},
  filterDriverWallet: { 
   from: formatDate(moment(new Date(date.getFullYear(), date.getMonth(), 1)).format('DD/MM/YYYY')) + ' 00:00:00Z',
   to: formatDate(moment(new Date(date.getFullYear(), date.getMonth() + 1, 0)).format('DD/MM/YYYY'))+ ' 23:59:59Z',
   per_page: 10,
   page: 1,  
  },
  filterRevenueOrder: { 
    from: formatDate(moment(new Date(date.getFullYear(), date.getMonth(), 1)).format('DD/MM/YYYY')) + ' 00:00:00Z',
    to: formatDate(moment(new Date(date.getFullYear(), date.getMonth() + 1, 0)).format('DD/MM/YYYY'))+ ' 23:59:59Z',
    per_page: 10,
    page: 1, 
    timeZone: new Date().getTimezoneOffset() // offset is positive if the local timezone is behind UTC and negative if it is ahead.

  },
  revenueOrder: [],
  revenueDriver: [],
  totalCountDriverWallet: 0,
  totalCountRevenueOrder: 0,
  currentPageDriverWallet: 1,
})
