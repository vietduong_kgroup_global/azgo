import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // exportExcel: ['values'],
    // // Get List Internal actions
    // getItemsRequest: ['filter'],
    driverWalletReport: ['filterDriverWallet'], 
    getDriverWalletReportSuccess: ['revenueDriver'],
    getDriverWalletReportFailure: null,
    // getItemsSuccess: ['items'],


    revenueOrderReport: ['filterRevenueOrder'],
    getRevenueOrderReportSuccess: ['revenueOrder'],
    getRevenueOrderReportFailure: null,

     
    exportRevenueOrderReport: ['filterRevenueOrder'],
    exportRevenueOrderReportSuccess: [],
    exportRevenueOrderReportFailure: null,

    exportDriverWalletReport: ['filterDriverWallet'],
    exportDriverWalletReportSuccess: [],
    exportDriverWalletReportFailure: null,
    // getItemsFailure: null,
    // clearItems: null,
    // // Edit Internal actions
    // editItemRequest: ['values'],
    // editItemSuccess: ['item'],
    // editItemFailure: null,
    // // Create Internal actions
    // createItemRequest: ['values'],
    // createItemSuccess: null,
    // createItemFailure: null,
    // // Delete Internal actions
    // deleteItemRequest: ['id'],
    // deleteItemSuccess: null,
    // deleteItemFailure: null,
    // // Get Internal Detail
    // getItemDetailRequest: ['id'],
    // getItemDetailSuccess: ['item'],
    // getItemDetailFailure: null,
    // // Change Filter
    setFilterDriverWallet: ['filterDriverWallet'],
    setFilterRevenueOrder: ['filterRevenueOrder'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const ReportTypes = Types
export const ReportActions = Creators
