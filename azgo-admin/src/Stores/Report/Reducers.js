import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { ReportTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
      },
      totalCount: 0,
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setDriverWallet = (state, { driverWallet, count, currentPage }) => {  
  return state.merge(
    fromJS({
      driverWallet, 
      totalCountDriverWallet: count,
      currentPageDriverWallet: currentPage
    })
  )
}
const setRevenueOrder = (state, { revenueOrder, count, currentPage }) => { 
  return state.merge(
    fromJS({
      revenueOrder, 
      totalCountRevenueOrder: count,
      currentPageRevenueOrder: currentPage
    })
  )
}
 
const setFilteDriverWallet = (state, { filterDriverWallet }) => {
  return state.mergeIn(['filterDriverWallet'], fromJS(filterDriverWallet))
}
const setFilterRevenueOrder = (state, { filterRevenueOrder }) => {
  return state.mergeIn(['filterRevenueOrder'], fromJS(filterRevenueOrder))
}
const reducer = createReducer(INITIAL_STATE, { 
  [ReportTypes.GET_REVENUE_ORDER_REPORT_SUCCESS]: setRevenueOrder,
  [ReportTypes.GET_REVENUE_ORDER_REPORT_FAILURE]: setRevenueOrder, 
  [ReportTypes.GET_DRIVER_WALLET_REPORT_SUCCESS]: setDriverWallet,
  [ReportTypes.GET_DRIVER_WALLET_REPORT_FAILURE]: setDriverWallet, 
  // [ReportTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  // [ReportTypes.CLEAR_ITEMS]: clearItems,
  [ReportTypes.SET_FILTER_DRIVER_WALLET]: setFilteDriverWallet,
  [ReportTypes.SET_FILTER_REVENUE_ORDER]: setFilterRevenueOrder,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
