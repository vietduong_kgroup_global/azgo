import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { ReportActions, ReportTypes } from './Actions'
import { ReportSelectors } from './Selectors'
import { ReportService } from 'Services/ReportService'
import { BlockUserService } from 'Services/BlockUser'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { fromJS } from 'immutable'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import FileDownload  from 'js-file-download';
import { changeFormatDate } from 'Utils/helper'

// function* exportExcel({ values }) {  
//   try { 
//     yield put(LoadingActions.showLoadingList())
//     const response = yield call(
//       ReportService.exportExcel,
//       values 
//     )
//     checkResponseError(response)
//     // yield put(LoadingActions.hideLoadingList())
//     // yield put({
//     //   type: ReportTypes.GET_ITEMS_SUCCESS,
//     //   items: response.data.results,
//     //   count: response.data.count,
//     // })
//   } catch (error) {
//     console.log(error)
//     // yield put({
//     //   type: ReportTypes.GET_ITEMS_FAILURE,
//     // })
//     // yield put(LoadingActions.hideLoadingList())
//   }
// } 
function* driverWalletReport({ filterDriverWallet }) {  
  try { 
    yield put(LoadingActions.showLoadingList()) 
    const response = yield call(
      ReportService.driverWalletReport,
      filterDriverWallet && filterDriverWallet.toJS()
 
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList()) 
    yield put({
      type: ReportTypes.GET_DRIVER_WALLET_REPORT_SUCCESS, 
      driverWallet: response.data.results,
      count: response.data.count,
      currentPage: response.data.currentPage
    })
  } catch (error) {
    console.log(error)
    yield put({
      type: ReportTypes.GET_DRIVER_WALLET_REPORT_FAILURE,
      driverWallet: [],
      count: 0,
      currentPage: 1
    })
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
    yield put(LoadingActions.hideLoadingList())
  }
} 
function* exportDriverWalletReport({ filterDriverWallet }) {  
  let now = new Date().toLocaleDateString('en-GB')  
  try { 
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      ReportService.exportDriverWalletReport,
      filterDriverWallet && filterDriverWallet.toJS() 
    ) 
    // console.log(filterDriverWallet)
    checkResponseError(response) 
    let from = (filterDriverWallet && filterDriverWallet.toJS()) ? changeFormatDate(filterDriverWallet.toJS().from.split(" ")[0]) : ""
    let to = (filterDriverWallet && filterDriverWallet.toJS()) ? changeFormatDate(filterDriverWallet.toJS().to.split(" ")[0]) : ""
 
    const titlePage = (from && to) ? `Báo_cáo_tổng_hợp_số_dư_ví_tài_xế_từ_${from}_đến_${to}` :  `Báo_cáo_tổng_hợp_số_dư_ví_tài_xế` 
    FileDownload(response.data,  `${titlePage}.xlsx`); // npm " js-file-download" responsible for downloading the file for the user 
    yield put(LoadingActions.hideLoadingList())  
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Tải file excel thành công',
      },
    })
  //   yield put({
  //     type: ReportTypes.GET_REVENUE_ORDER_REPORT_SUCCESS,
  //     revenueOrder: response, 
  //   })
  } catch (error) {
    console.log(error)
    yield put({
      type: ReportTypes.GET_DRIVER_WALLET_REPORT_FAILURE,
    })
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
    yield put(LoadingActions.hideLoadingList())
  }
} 
function* revenueOrderReport({ filterRevenueOrder }) {  
  try { 
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      ReportService.revenueOrderReport,
      filterRevenueOrder && filterRevenueOrder.toJS() 
    )
  checkResponseError(response) 
    yield put(LoadingActions.hideLoadingList())  
    yield put({
      type: ReportTypes.GET_REVENUE_ORDER_REPORT_SUCCESS, 
      revenueOrder: response.data.results,
      count: response.data.count,
      currentPage: response.data.currentPage
    })
  } catch (error) {
    console.log(error)
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
    yield put({
      type: ReportTypes.GET_REVENUE_ORDER_REPORT_FAILURE,
      revenueOrder: [],
      count: 0,
      currentPage: 1
    })
    yield put(LoadingActions.hideLoadingList()) 
  }
} 
function* exportRevenueOrderReport({ filterRevenueOrder }) {  
  try { 
    let now = new Date().toLocaleDateString('en-GB') 
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      ReportService.exportRevenueOrderReport,
      filterRevenueOrder && filterRevenueOrder.toJS() 
    ) 
    checkResponseError(response)  
    let from = (filterRevenueOrder && filterRevenueOrder.toJS()) ? changeFormatDate(filterRevenueOrder.toJS().from.split(" ")[0]) : ""
    let to = (filterRevenueOrder && filterRevenueOrder.toJS()) ? changeFormatDate(filterRevenueOrder.toJS().to.split(" ")[0]) : ""
 
    const titlePage = (from && to) ? `Báo_cáo_tổng_hợp_chi_tiết_tiền_thu_chuyến_xe_và_từ_${from}_đến_${to}` :  `Báo_cáo_tổng_hợp_chi_tiết_tiền_thu_chuyến_xe` 
    FileDownload(response.data,  `${titlePage}.xlsx`); // npm " js-file-download" responsible for downloading the file for the user 
    // FileDownload(response.data, `Báo_cáo_kế_toán_Azgo_nhập_liệu_admin_ngày_${now}.xlsx`); // npm " js-file-download" responsible for downloading the file for the user 
    yield put(LoadingActions.hideLoadingList())  
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Tải file excel thành công',
      },
    })
  //   yield put({
  //     type: ReportTypes.GET_REVENUE_ORDER_REPORT_SUCCESS,
  //     revenueOrder: response, 
  //   })
  } catch (error) {
    console.log(error)
    yield put({
      type: ReportTypes.GET_REVENUE_ORDER_REPORT_FAILURE,
    })
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
    yield put(LoadingActions.hideLoadingList())
  }
} 
 
function* watcher() {
  yield all([
    // takeLatest(ReportTypes.EXPORT_EXCEL, exportExcel),
    takeLatest(ReportTypes.DRIVER_WALLET_REPORT, driverWalletReport), 
    takeLatest(ReportTypes.REVENUE_ORDER_REPORT, revenueOrderReport), 
    takeLatest(ReportTypes.EXPORT_REVENUE_ORDER_REPORT, exportRevenueOrderReport), 
    takeLatest(ReportTypes.EXPORT_DRIVER_WALLET_REPORT, exportDriverWalletReport), 
   ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
