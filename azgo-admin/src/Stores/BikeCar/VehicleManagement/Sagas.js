import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { VehicleManagementTypes, VehicleManagementActions } from './Actions'
import { VehicleManagementService } from 'Services/BikeCar/VehicleManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { VehicleManagementSelectors } from 'Stores/BikeCar/VehicleManagement/Selectors'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      VehicleManagementService.getListItems,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: VehicleManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: VehicleManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(VehicleManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: VehicleManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: VehicleManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(VehicleManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: VehicleManagementTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(push('/bike-car/vehicle'))
  } catch (error) {
    yield put({
      type: VehicleManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(VehicleManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: VehicleManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/bike-car/vehicle'))
  } catch (error) {
    yield put({
      type: VehicleManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(VehicleManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(VehicleManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: VehicleManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      VehicleManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: VehicleManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(VehicleManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(VehicleManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(VehicleManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(VehicleManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
    takeLatest(
      VehicleManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
