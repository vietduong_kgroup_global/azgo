import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { DriverManagementTypes } from './Actions'
import reducerRegistry from 'Stores/Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
      },
      totalCount: 0,
      itemsWithout: [],
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
      totalCount: count,
    })
  )
}

const setItemsWithout = (state, { items }) => {
  return state.merge(
    fromJS({
      itemsWithout: items,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(INITIAL_STATE, {
  [DriverManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [DriverManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [DriverManagementTypes.CLEAR_ITEMS]: clearItems,
  [DriverManagementTypes.SET_FILTER]: setFilter,
  [DriverManagementTypes.GET_ITEMS_WITHOUT_SUCCESS]: setItemsWithout,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
