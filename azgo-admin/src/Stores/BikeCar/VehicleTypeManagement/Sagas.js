import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import {
  VehicleTypeManagementTypes,
  VehicleTypeManagementActions,
} from './Actions'
import { VehicleTypeManagementService } from 'Services/BikeCar/VehicleTypeManagementService'
import { VehicleTypeManagementSelectors } from './Selectors'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      VehicleTypeManagementService.getListItems,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: VehicleTypeManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: VehicleTypeManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(VehicleTypeManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: VehicleTypeManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: VehicleTypeManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(VehicleTypeManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: VehicleTypeManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/admin/bike-car/vehicle-type'))
  } catch (error) {
    yield put({
      type: VehicleTypeManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(VehicleTypeManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: VehicleTypeManagementTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(push('/admin/bike-car/vehicle-type'))
  } catch (error) {
    yield put({
      type: VehicleTypeManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(VehicleTypeManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(VehicleTypeManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: VehicleTypeManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      VehicleTypeManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: VehicleTypeManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemsRequestByVehicleGroup({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      VehicleTypeManagementService.getListItemsByVehicleGroup,
      id
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: VehicleTypeManagementTypes.GET_ITEMS_SUCCESS_BY_VEHICLE_GROUP,
      itemsByVehicleGroup: response.data,
    })
  } catch (error) {
    yield put({
      type: VehicleTypeManagementTypes.GET_ITEMS_FAILURE_BY_VEHICLE_GROUP,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* watcher() {
  yield all([
    takeLatest(VehicleTypeManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(
      VehicleTypeManagementTypes.GET_ITEMS_REQUEST_BY_VEHICLE_GROUP,
      getItemsRequestByVehicleGroup
    ),
    takeLatest(
      VehicleTypeManagementTypes.CREATE_ITEM_REQUEST,
      createItemRequest
    ),
    takeLatest(VehicleTypeManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(
      VehicleTypeManagementTypes.DELETE_ITEM_REQUEST,
      deleteItemRequest
    ),
    takeLatest(
      VehicleTypeManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
