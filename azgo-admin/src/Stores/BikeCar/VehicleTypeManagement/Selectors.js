import { MODULE_NAME } from './InitialState'

const getFilter = state => state[MODULE_NAME].get('filter')

const getItems = state => state[MODULE_NAME].get('items')

const itemsByVehicleGroup = state =>
  state[MODULE_NAME].get('itemsByVehicleGroup')

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

const getItem = state => state[MODULE_NAME].get('item')

export const VehicleTypeManagementSelectors = {
  getFilter,
  getItems,
  getTotalCount,
  getItem,
  itemsByVehicleGroup,
}
