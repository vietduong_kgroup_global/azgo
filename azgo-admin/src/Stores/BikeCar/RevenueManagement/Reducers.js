import { fromJS } from 'immutable'
import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { RevenueManagementBikeTypes } from './Actions'
import reducerRegistry from 'Stores/Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      filter: {
        per_page: 10,
        page: 1,
        provider_id: '',
        to_date: '',
        from_date: '',
      },
      totalCount: 0,
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
      totalCount: count,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(INITIAL_STATE, {
  [RevenueManagementBikeTypes.GET_ITEMS_SUCCESS]: setItems,
  [RevenueManagementBikeTypes.CLEAR_ITEMS]: clearItems,
  [RevenueManagementBikeTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
