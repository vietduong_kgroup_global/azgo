import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { RevenueManagementBikeTypes, RevenueManagementBikeActions } from './Actions'
import { RevenueManagementService } from 'Services/BikeCar/RevenueManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      RevenueManagementService.getListItems,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: RevenueManagementBikeTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: RevenueManagementBikeTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* watcher() {
  yield all([
    takeLatest(RevenueManagementBikeTypes.GET_ITEMS_REQUEST, getItemsRequest),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
