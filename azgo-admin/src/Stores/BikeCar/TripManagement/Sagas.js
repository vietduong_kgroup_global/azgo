import { put, call, takeLatest, all } from 'redux-saga/effects'
import { TripManagementTypes } from './Actions'
import { TripCarManagementService } from 'Services/BikeCar/TripCarManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      TripCarManagementService.getListItems,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: TripManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: TripManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(TripCarManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: TripManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: TripManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* watcher() {
  yield all([
    takeLatest(TripManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(
      TripManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
