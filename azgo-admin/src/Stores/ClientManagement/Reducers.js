import { fromJS } from 'immutable'

import { CLIENT_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { ClientManagementTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
      },
      totalCount: 0,
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items: items,
      totalCount: count,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(CLIENT_STATE, {
  [ClientManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [ClientManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [ClientManagementTypes.CLEAR_ITEMS]: clearItems,
  [ClientManagementTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
