import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List Client actions
    getItemsRequest: ['filter'],
    getItemsSuccess: ['items', 'count'],
    getItemsFailure: null,
    clearItems: null,
    // Edit Client actions
    editItemRequest: ['values'],
    editItemSuccess: ['item'],
    editItemFailure: null,
    // Create Client actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
    // Delete Client actions
    deleteItemRequest: ['id'],
    deleteItemSuccess: null,
    deleteItemFailure: null,
    // Get Client Detail
    getItemDetailRequest: ['id'],
    getItemDetailSuccess: ['item'],
    getItemDetailFailure: null,
    // Change Filter
    setFilter: ['filter'],

    // block user
    block: ['id'],
    unblock: ['id'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const ClientManagementTypes = Types
export const ClientManagementActions = Creators
