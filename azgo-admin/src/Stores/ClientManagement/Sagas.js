import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { ClientManagementTypes, ClientManagementActions } from './Actions'
import { ClientManagementService } from 'Services/ClientManagementService'
import { BlockUserService } from 'Services/BlockUser'
import { ClientManagementSelectors } from './Selectors'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      ClientManagementService.getListItems,
      filter && filter.toJS() 
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: ClientManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    console.log("error getItemsRequest CLIENT: ", error)
    yield put({
      type: ClientManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(ClientManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: ClientManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: ClientManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(ClientManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: ClientManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/admin/clients-management'))
  } catch (error) {
    yield put({
      type: ClientManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(ClientManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: ClientManagementTypes.EDIT_ITEM_SUCCESS,
    })
    if (data.changeBlockUser === true && data.status === 0) {
      yield call(BlockUserService.unblockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 1) {
      yield call(BlockUserService.blockUser, data.id)
    }
    yield put(push('/admin/clients-management'))
  } catch (error) {
    yield put({
      type: ClientManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(ClientManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(ClientManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: ClientManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      ClientManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: ClientManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(ClientManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(ClientManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(ClientManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(ClientManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
    takeLatest(
      ClientManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
