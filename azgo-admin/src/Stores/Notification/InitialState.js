import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'notification'

export const INITIAL_STATE = fromJS({
  notifyInfo: {
    type: null,
    message: null,
  },
  isShowNotify: false,
})
