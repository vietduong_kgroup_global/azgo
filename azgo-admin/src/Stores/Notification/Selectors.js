import { MODULE_NAME } from './InitialState'

const getNotifyInfo = state => {
  return state[MODULE_NAME].get('notifyInfo').toJS()
}

const getIsShowNotify = state => state[MODULE_NAME].get('isShowNotify')

export const NotificationSelectors = {
  getNotifyInfo,
  getIsShowNotify,
}
