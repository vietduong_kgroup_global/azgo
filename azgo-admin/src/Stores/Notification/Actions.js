import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get Notify actions
    showNotifyRequest: ['notifyInfo'],
    clearNotifyRequest: null,
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const NotificationTypes = Types
export const NotificationActions = Creators
