import { fromJS } from 'immutable'
import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { NotificationTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const setNotifyInfo = (state, { notifyInfo }) => {
  return state.merge(
    fromJS({
      notifyInfo,
      isShowNotify: true,
    })
  )
}
const clearNotifyRequest = state => {
  return state.merge(
    fromJS({
      isShowNotify: false,
    })
  )
}

const reducer = createReducer(INITIAL_STATE, {
  [NotificationTypes.SHOW_NOTIFY_REQUEST]: setNotifyInfo,
  [NotificationTypes.CLEAR_NOTIFY_REQUEST]: clearNotifyRequest,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
