import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { SeatMapManagementTypes } from './Actions'
import reducerRegistry from 'Stores/Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
        provider_id: '',
      },
      totalCount: 0,
    })
  )
}

const clearFilter = state => {
  return state.merge(
    fromJS({
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
        provider_id: '',
      },
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
      totalCount: count,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(INITIAL_STATE, {
  [SeatMapManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [SeatMapManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [SeatMapManagementTypes.CLEAR_ITEMS]: clearItems,
  [SeatMapManagementTypes.CLEAR_FILTER]: clearFilter,
  [SeatMapManagementTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
