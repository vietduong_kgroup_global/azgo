import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { SeatMapManagementTypes, SeatMapManagementActions } from './Actions'
import { SeatMapManagementService } from 'Services/Intercity/SeatMapManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { SeatMapManagementSelectors } from 'Stores/Intercity/SeatMapManagement/Selectors'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      SeatMapManagementService.getListItems,
      filter ? filter.toJS() : null
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: SeatMapManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: SeatMapManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(SeatMapManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: SeatMapManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: SeatMapManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(SeatMapManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: SeatMapManagementTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(push('/bus/seat-map'))
  } catch (error) {
    yield put({
      type: SeatMapManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(SeatMapManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: SeatMapManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/bus/seat-map'))
  } catch (error) {
    yield put({
      type: SeatMapManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(SeatMapManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(SeatMapManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: SeatMapManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      SeatMapManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: SeatMapManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(SeatMapManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(SeatMapManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(SeatMapManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(SeatMapManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
    takeLatest(
      SeatMapManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
