import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { RouteManagementTypes, RouteManagementActions } from './Actions'
import { RouteManagementService } from 'Services/Intercity/RouteManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { RouteManagementSelectors } from 'Stores/Intercity/RouteManagement/Selectors'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      RouteManagementService.getListItems,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: RouteManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: RouteManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemsRequestProvider({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(RouteManagementService.getListItemsProvider, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: RouteManagementTypes.GET_ITEMS_SUCCESS_PROVIDER,
      itemsProvider: response.data[0],
    })
  } catch (error) {
    yield put({
      type: RouteManagementTypes.GET_ITEMS_FAILURE_PROVIDER,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(RouteManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: RouteManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: RouteManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(RouteManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: RouteManagementTypes.EDIT_ITEM_SUCCESS,
    })
    if (values.budget) {
      yield put(push('/bus/budget'))
    } else {
      yield put(push('/bus/route'))
    }
  } catch (error) {
    yield put({
      type: RouteManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(RouteManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: RouteManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/bus/route'))
  } catch (error) {
    yield put({
      type: RouteManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(RouteManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(RouteManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: RouteManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      RouteManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: RouteManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(RouteManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(
      RouteManagementTypes.GET_ITEMS_REQUEST_PROVIDER,
      getItemsRequestProvider
    ),
    takeLatest(RouteManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(RouteManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(
      RouteManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
    takeLatest(RouteManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
