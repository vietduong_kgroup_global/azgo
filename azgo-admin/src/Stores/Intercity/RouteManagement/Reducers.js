import { fromJS } from 'immutable'
import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { RouteManagementTypes } from './Actions'
import reducerRegistry from 'Stores/Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
      },
      totalCount: 0,
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
      totalCount: count,
    })
  )
}

const setItemsProvider = (state, { itemsProvider }) => {
  return state.merge(
    fromJS({
      itemsProvider,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(INITIAL_STATE, {
  [RouteManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [RouteManagementTypes.GET_ITEMS_SUCCESS_PROVIDER]: setItemsProvider,
  [RouteManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [RouteManagementTypes.CLEAR_ITEMS]: clearItems,
  [RouteManagementTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
