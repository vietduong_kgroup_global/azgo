import { MODULE_NAME } from './InitialState'

const getFilter = state => state[MODULE_NAME].get('filter')

const getItems = state => state[MODULE_NAME].get('items')

const getItemsProvider = state => {
  return state[MODULE_NAME].get('itemsProvider')
}

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

const getItem = state => state[MODULE_NAME].get('item')

export const RouteManagementSelectors = {
  getFilter,
  getItems,
  getItemsProvider,
  getTotalCount,
  getItem,
}
