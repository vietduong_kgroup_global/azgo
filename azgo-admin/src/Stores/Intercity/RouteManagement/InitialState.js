import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'RouteManagement'

export const INITIAL_STATE = fromJS({
  items: [],
  itemsProvider: [],
  item: {},
  filter: {
    per_page: 10,
    page: 1,
    keyword: '',
    provider_id: '',
  },
  totalCount: 0,
})
