import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { TicketManagementTypes, TicketManagementActions } from './Actions'
import { TicketManagementService } from 'Services/Intercity/TicketManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { ScheduleManagementActions } from 'Stores/Intercity/ScheduleManagement/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      TicketManagementService.getListItems,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: TicketManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: TicketManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(TicketManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: TicketManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: TicketManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(TicketManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: TicketManagementTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(push('/bus/schedule'))
  } catch (error) {
    yield put({
      type: TicketManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    yield put(LoadingActions.showPopUp())
    const response = yield call(TicketManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: TicketManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hidePopUp())
    // window.location.reload()
    yield put(ScheduleManagementActions.getItemDetailRequest(values.scheduleId))
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Đặt vé thành công',
      },
    })
  } catch (error) {
    yield put({
      type: TicketManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hidePopUp())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest(values) {
  try {
    yield put(LoadingActions.showPopUp())
    const respones = yield call(
      TicketManagementService.deleteItem,
      values.id.idTicket
    )
    checkResponseError(respones)
    yield put({
      type: TicketManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(
      ScheduleManagementActions.getItemDetailRequest(values.id.scheduleId)
    )
    yield put(LoadingActions.hidePopUp())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Hủy vé thành công',
      },
    })
  } catch (error) {
    yield put({
      type: TicketManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hidePopUp())
  }
}

function* watcher() {
  yield all([
    takeLatest(TicketManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(TicketManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(TicketManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(TicketManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
    takeLatest(
      TicketManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
