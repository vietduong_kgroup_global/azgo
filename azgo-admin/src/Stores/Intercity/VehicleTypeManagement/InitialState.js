import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'VehicleTypeBusManagement'

export const VEHICLE_TYPE_STATE = fromJS({
  items: [],
  item: {},
  filter: {
    per_page: 10,
    page: 1,
    keyword: '',
  },
  totalCount: 0,
  // Get Vehicle Type Of Provider
  itemsVehicleType: [],
})
