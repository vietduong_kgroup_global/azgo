import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List VehicleType actions
    getItemsRequest: ['filter'],
    getItemsSuccess: ['items', 'count'],
    getItemsFailure: null,
    clearItems: null,
    // Edit VehicleType actions
    editItemRequest: ['values'],
    editItemSuccess: ['item'],
    editItemFailure: null,
    // Create VehicleType actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
    // Delete VehicleType actions
    deleteItemRequest: ['id'],
    deleteItemSuccess: null,
    deleteItemFailure: null,
    // Get VehicleType Detail
    getItemDetailRequest: ['id'],
    getItemDetailSuccess: ['item'],
    getItemDetailFailure: null,
    // Get Vehicle Type Of Provider
    getItemsVehicleTypeRequest: ['id'],
    getItemsVehicleTypeSuccess: ['item'],
    getItemsVehicleTypeFailure: null,
    clearItemsVehicleType: null,
    // Change Filter
    setFilter: ['filter'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const VehicleTypeManagementTypes = Types
export const VehicleTypeManagementActions = Creators
