import { MODULE_NAME } from './InitialState'

const getFilter = state => state[MODULE_NAME].get('filter')

const getItems = state => state[MODULE_NAME].get('items')

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

const getItem = state => state[MODULE_NAME].get('item')

// Get Vehicle Type Of Provider
const getItemsVehicleType = state => state[MODULE_NAME].get('itemsVehicleType')

export const VehicleTypeManagementSelectors = {
  getFilter,
  getItems,
  getTotalCount,
  getItem,
  getItemsVehicleType,
}
