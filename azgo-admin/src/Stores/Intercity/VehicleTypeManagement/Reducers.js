import { fromJS } from 'immutable'

import { VEHICLE_TYPE_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { VehicleTypeManagementTypes } from './Actions'
import reducerRegistry from '../../Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
      },
      totalCount: 0,
    })
  )
}
//  Vehicle Type Of Provider
const clearItemsVehicleType = state => {
  return state.merge(
    fromJS({
      itemsVehicleType: [],
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
      totalCount: count,
    })
  )
}

// Get Vehicle Type Of Provider
const setItemsVehicleType = (state, { itemsVehicleType }) => {
  return state.merge(
    fromJS({
      itemsVehicleType,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(VEHICLE_TYPE_STATE, {
  [VehicleTypeManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [VehicleTypeManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [VehicleTypeManagementTypes.CLEAR_ITEMS]: clearItems,
  [VehicleTypeManagementTypes.CLEAR_ITEMS_VEHICLE_TYPE]: clearItemsVehicleType,
  [VehicleTypeManagementTypes.SET_FILTER]: setFilter,
  [VehicleTypeManagementTypes.GET_ITEMS_VEHICLE_TYPE_SUCCESS]: setItemsVehicleType,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
