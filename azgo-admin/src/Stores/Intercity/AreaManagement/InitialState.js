import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'AreaManagement'

export const AREA_STATE = fromJS({
  items: [],
  item: {},
  filter: {
    per_page: 100,
    page: 1,
    keyword: '',
  },
  totalCount: 0,
})
