import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List Area actions
    getItemsRequest: ['filter'],
    getItemsSuccess: ['items'],
    getItemsFailure: null,
    clearItems: null,
    // Edit Area actions
    editItemRequest: ['values'],
    editItemSuccess: ['item'],
    editItemFailure: null,
    // Create Area actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
    // Delete Area actions
    deleteItemRequest: ['id'],
    deleteItemSuccess: null,
    deleteItemFailure: null,
    // Get Area Detail
    getItemDetailRequest: ['id'],
    getItemDetailSuccess: ['item'],
    getItemDetailFailure: null,
    // Change Filter
    setFilter: ['filter'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const AreaManagementTypes = Types
export const AreaManagementActions = Creators
