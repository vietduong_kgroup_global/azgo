import { fromJS } from 'immutable'

import { AREA_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { AreaManagementTypes } from './Actions'
import reducerRegistry from '../../Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
      },
    })
  )
}
const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
      totalCount: count,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(AREA_STATE, {
  [AreaManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [AreaManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [AreaManagementTypes.CLEAR_ITEMS]: clearItems,
  [AreaManagementTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
