import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { AreaManagementTypes, AreaManagementActions } from './Actions'
import { AreaManagementService } from 'Services/Intercity/AreaManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { AreaManagementSelectors } from 'Stores/Intercity/AreaManagement/Selectors'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      AreaManagementService.getListItems,
      filter ? filter.toJS() : null
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: AreaManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data,
    })
  } catch (error) {
    yield put({
      type: AreaManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(AreaManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: AreaManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: AreaManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* editItemRequest({ values }) {
  try {
    yield put(LoadingActions.showPopUp())
    const filter = yield select(AreaManagementSelectors.getFilter)
    const response = yield call(AreaManagementService.editItem, values)
    checkResponseError(response)
    yield put({
      type: AreaManagementTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hidePopUp())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Chỉnh sửa địa điểm thành công',
      },
    })
    yield put(
      AreaManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: AreaManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hidePopUp())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    yield put(LoadingActions.showPopUp())
    const filter = yield select(AreaManagementSelectors.getFilter)
    const response = yield call(AreaManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: AreaManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hidePopUp())
    yield put(
      AreaManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Tạo địa điểm mới thành công',
      },
    })
    yield put(push('/admin/tour/areaManagement'))
  } catch (error) {
    yield put({
      type: AreaManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(AreaManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(AreaManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: AreaManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      AreaManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Xóa địa điểm thành công',
      },
    })
  } catch (error) {
    yield put({
      type: AreaManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(AreaManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(AreaManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(AreaManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(AreaManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
    takeLatest(
      AreaManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
