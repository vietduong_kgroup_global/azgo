import { MODULE_NAME } from './InitialState'

const getFilter = state => state[MODULE_NAME].get('filter')

const getItems = state => state[MODULE_NAME].get('items')

const getItem = state => state[MODULE_NAME].get('item')

export const AreaManagementSelectors = {
  getFilter,
  getItems,
  getItem,
}
