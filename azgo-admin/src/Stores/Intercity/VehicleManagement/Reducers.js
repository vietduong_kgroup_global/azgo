import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { VehicleManagementTypes } from './Actions'
import reducerRegistry from 'Stores/Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
      },
      totalCount: 0,
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
      totalCount: count,
    })
  )
}

const setItemLicensePlates = (state, { itemLicensePlates }) => {
  return state.merge(
    fromJS({
      itemLicensePlates,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(INITIAL_STATE, {
  [VehicleManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [VehicleManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [VehicleManagementTypes.CLEAR_ITEMS]: clearItems,
  [VehicleManagementTypes.SET_FILTER]: setFilter,
  [VehicleManagementTypes.GET_ITEM_LICENSE_PLATES_SUCCESS]: setItemLicensePlates,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
