import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List Schedule actions
    getItemsRequest: ['filter'],
    getItemsSuccess: ['items', 'count'],
    getItemsFailure: null,
    clearItems: null,
    // Edit Schedule actions
    editItemRequest: ['values'],
    editItemSuccess: ['item'],
    editItemFailure: null,
    // Create Schedule actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
    // Delete Schedule actions
    deleteItemRequest: ['id'],
    deleteItemSuccess: null,
    deleteItemFailure: null,
    // Get Schedule Detail
    getItemDetailRequest: ['id'],
    getItemDetailSuccess: ['item'],
    getItemDetailFailure: null,
    // Change Filter
    setFilter: ['filter'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const ScheduleManagementTypes = Types
export const ScheduleManagementActions = Creators
