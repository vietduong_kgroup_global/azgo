import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { ScheduleManagementTypes, ScheduleManagementActions } from './Actions'
import { ScheduleManagementService } from 'Services/Intercity/ScheduleManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { ScheduleManagementSelectors } from 'Stores/Intercity/ScheduleManagement/Selectors'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      ScheduleManagementService.getListItems,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: ScheduleManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: ScheduleManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(ScheduleManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: ScheduleManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: ScheduleManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(ScheduleManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: ScheduleManagementTypes.EDIT_ITEM_SUCCESS,
    })
    if (values.editSchedule === 1) {
      yield put(push('/bus/schedule'))
    }
  } catch (error) {
    yield put({
      type: ScheduleManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(ScheduleManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: ScheduleManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/bus/schedule'))
  } catch (error) {
    yield put({
      type: ScheduleManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(ScheduleManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(ScheduleManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: ScheduleManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      ScheduleManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: ScheduleManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(ScheduleManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(ScheduleManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(ScheduleManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(
      ScheduleManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
    takeLatest(ScheduleManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
