import { INITIAL_STATE } from './InitialState'
import { fromJS } from 'immutable'
import { createReducer } from 'reduxsauce'
import { AuthTypes } from './Actions'

// set state user info
const setUserInfo = (state, { dataUserInfo }) => {
  return state.merge(
    fromJS({
      userInfo: dataUserInfo,
    })
  )
}

// Set userdata and permission when checkroles success
const setRoleItems = (state, { userData }) =>
  state.merge(
    fromJS({
      userRole: userData,
    })
  )

const reducer = createReducer(INITIAL_STATE, {
  // Check roles
  [AuthTypes.GET_ROLES_SUCCESS]: setRoleItems,
})

export default reducer
