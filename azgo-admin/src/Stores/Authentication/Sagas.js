import { put, call, takeLatest, all } from 'redux-saga/effects'
import { AuthTypes } from 'Stores/Authentication/Actions'

// Get roles worker
function* getRolesWorker() {}

export default function* watcher() {
  yield all([takeLatest(AuthTypes.GET_ROLES_REQUEST, getRolesWorker)])
}
