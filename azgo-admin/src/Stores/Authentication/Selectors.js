import { MODULE_NAME } from './InitialState'

const getStateRequestPassword = state => {
  return state[MODULE_NAME].get('checkEmailSuccess')
    ? state[MODULE_NAME].get('checkEmailSuccess').toJS()
    : ''
}
export const AuthSelectors = {
  getStateRequestPassword,
}
