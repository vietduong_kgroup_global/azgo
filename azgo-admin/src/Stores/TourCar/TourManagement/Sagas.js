import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { TourManagementTypes, TourManagementActions } from './Actions'
import { TourManagementService } from 'Services/TourCar/TourManagementService'
import { BlockUserService } from 'Services/BlockUser'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { TourManagementSelectors } from 'Stores/TourCar/TourManagement/Selectors'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) { 
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      TourManagementService.getListItems,
      filter && filter.toJS() 
    ) 
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: TourManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: TourManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(TourManagementService.getItemById, id)
    // checkResponseError(response)
    yield put(LoadingActions.hideLoadingList()) 
    yield put({
      type: TourManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: TourManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(TourManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: TourManagementTypes.EDIT_ITEM_SUCCESS,
    })
    if (data.changeBlockUser === true && data.status === 0) {
      yield call(BlockUserService.unblockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 1) {
      yield call(BlockUserService.blockUser, data.id)
    }
    yield put(push('/admin/tour/tourManagement'))
  } catch (error) {
    yield put({
      type: TourManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(TourManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: TourManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/admin/tour/tourManagement'))
  } catch (error) {
    yield put({
      type: TourManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(TourManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(TourManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: TourManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      TourManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
    yield put(push('/admin/tour/tourManagement'))

  } catch (error) {
    yield put({
      type: TourManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemsWithoutRequest({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      TourManagementService.getListWithoutItems,
      values
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: TourManagementTypes.GET_ITEMS_WITHOUT_SUCCESS,
      items: response.data.results,
    })
  } catch (error) {
    yield put({
      type: TourManagementTypes.GET_ITEMS_WITHOUT_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(TourManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(TourManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(TourManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(
      TourManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
    takeLatest(TourManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
    takeLatest(
      TourManagementTypes.GET_ITEMS_WITHOUT_REQUEST,
      getItemsWithoutRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
