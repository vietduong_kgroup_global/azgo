import { MODULE_NAME } from './InitialState'

const getFilter = state => state[MODULE_NAME].get('filter')

const getItems = state => state[MODULE_NAME].get('items')

const getItemsWithout = state => state[MODULE_NAME].get('itemsWithout')

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

const getItem = state => state[MODULE_NAME].get('item')

export const TourManagementSelectors = {
  getFilter,
  getItems,
  getTotalCount,
  getItem,
  getItemsWithout,
}
