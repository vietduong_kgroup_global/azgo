import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List TripCar actions
    getItemsRequest: ['filter'],
    getItemsSuccess: ['items', 'count'],
    getItemsFailure: null,
    clearItems: null,
    // Get TripCar Detail
    getItemDetailRequest: ['id'],
    getItemDetailSuccess: ['item'],
    getItemDetailFailure: null,

    // Edit Driver actions
    editItemRequest: ['values'],
    editItemSuccess: ['item'],
    editItemFailure: null,

    // checkFeeWalletToReceiveOrder  actions
    checkFeeWalletOrder: ['values'],
    checkFeeWalletOrderSuccess: ['item'],
    checkFeeWalletOrderFailure: null, 

    // Create Driver actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
    // Delete Driver actions
    deleteItemRequest: ['id'],
    deleteItemSuccess: null,
    deleteItemFailure: null, 
    // Change Filter
    setFilter: ['filter'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const TripManagementTypes = Types
export const TripManagementActions = Creators
