import { put, call, takeLatest, all , select} from 'redux-saga/effects'
import { TripManagementTypes, TripManagementActions } from './Actions'
import { TripManagementService } from 'Services/TourCar/TripManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { BlockUserService } from 'Services/BlockUser'
import { TYPE_MESSAGE } from 'Utils/enum'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { push } from 'connected-react-router'
import { fromJS } from 'immutable'
import { TripManagementSelectors } from 'Stores/TourCar/TripManagement/Selectors'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      TripManagementService.getListItems,
      filter && filter.toJS() 
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: TripManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: TripManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(TripManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: TripManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: TripManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}
function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(TripManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction()) 
    yield put({
      type: TripManagementTypes.EDIT_ITEM_SUCCESS, 
    })
    if (data.changeBlockUser === true && data.status === 0) {
      yield call(BlockUserService.unblockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 1) {
      yield call(BlockUserService.blockUser, data.id)
    }
    yield put(push('/admin/tour/orderManagement'))
  } catch (error) {
    yield put({
      type: TripManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}
function* checkFeeWalletOrder({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(TripManagementService.checkFeeWalletOrder, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: TripManagementTypes.CHECK_FEE_WALLET_ORDER_SUCCESS,
      statusFeeWallet: response.data

    })
  } catch (error) {
    // console.log("error: ", error);
    yield put({
      type: TripManagementTypes.CHECK_FEE_WALLET_ORDER_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}
function* createItemRequest({ values }) {
  try { 
    const response = yield call(TripManagementService.createItem, values)
    checkResponseError(response)
  yield put({
      type: TripManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/admin/tour/orderManagement'))
  } catch (error) {
  // console.log(error)
  yield put({
      type: TripManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}
function* deleteItemRequest({ id }) { 
  try {
  const filter = yield select(TripManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
  const respones = yield call(TripManagementService.deleteItem, id)
    checkResponseError(respones)
  yield put({
      type: TripManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      TripManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
    yield put(push('/admin/tour/orderManagement'))

  } catch (error) {
    console.log(error)
    yield put({
      type: TripManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}
function* watcher() {
  yield all([ 
    takeLatest(TripManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(TripManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(TripManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(TripManagementTypes.GET_ITEM_DETAIL_REQUEST, getItemDetailRequest),
    takeLatest(TripManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
    takeLatest(TripManagementTypes.CHECK_FEE_WALLET_ORDER, checkFeeWalletOrder), 
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
