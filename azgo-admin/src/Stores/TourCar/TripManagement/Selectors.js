import { MODULE_NAME } from './InitialState'

const getFilter = state => state[MODULE_NAME].get('filter')

const getItems = state => state[MODULE_NAME].get('items')

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

const getItem = state => state[MODULE_NAME].get('item')

const getStatusFeeWallet = state => state[MODULE_NAME].get('statusFeeWallet')

export const TripManagementSelectors = {
  getFilter,
  getItems,
  getTotalCount,
  getItem,
  getStatusFeeWallet
}
