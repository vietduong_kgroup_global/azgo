import { put, call, takeLatest, all , select} from 'redux-saga/effects'
import { OrderTourLogTypes, OrderTourLogActions } from './Actions'
import { OrderTourLogService } from 'Services/TourCar/OrderTourLogService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { BlockUserService } from 'Services/BlockUser'
import { TYPE_MESSAGE } from 'Utils/enum'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { push } from 'connected-react-router'
import { fromJS } from 'immutable'
import { OrderTourLogSelectors } from 'Stores/TourCar/OrderTourLog/Selectors'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      OrderTourLogService.getListItems,
      filter && filter.toJS() 
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: OrderTourLogTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: OrderTourLogTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) { 
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(OrderTourLogService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: OrderTourLogTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: OrderTourLogTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}
function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(OrderTourLogService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: OrderTourLogTypes.EDIT_ITEM_SUCCESS,
    })
    if (data.changeBlockUser === true && data.status === 0) {
      yield call(BlockUserService.unblockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 1) {
      yield call(BlockUserService.blockUser, data.id)
    }
    yield put(push('/admin/tour/orderManagement'))
  } catch (error) {
    yield put({
      type: OrderTourLogTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}
function* createItemRequest({ values }) { 
  try { 
    const response = yield call(OrderTourLogService.createItem, values)
    checkResponseError(response)
  yield put({
      type: OrderTourLogTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/admin/tour/orderManagement'))
  } catch (error) {
  console.log(error)
  yield put({
      type: OrderTourLogTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}
function* deleteItemRequest({ id }) { 
  try {
  const filter = yield select(OrderTourLogSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
  const respones = yield call(OrderTourLogService.deleteItem, id)
    checkResponseError(respones)
  yield put({
      type: OrderTourLogTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      OrderTourLogActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
    yield put(push('/admin/tour/orderManagement'))

  } catch (error) {
    console.log(error)
    yield put({
      type: OrderTourLogTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}
function* watcher() {
  yield all([ 
    takeLatest(OrderTourLogTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(OrderTourLogTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(OrderTourLogTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(OrderTourLogTypes.GET_ITEM_DETAIL_REQUEST, getItemDetailRequest),
    takeLatest(OrderTourLogTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
