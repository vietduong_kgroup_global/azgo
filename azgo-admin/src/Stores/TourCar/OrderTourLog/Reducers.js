import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { OrderTourLogTypes } from './Actions'
import reducerRegistry from 'Stores/Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
      },
      totalCount: 0,
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
      totalCount: count,
    })
  )
}

const setFilter = (state, { filter }) =>{
  
  return   state.mergeIn(['filter'], fromJS(filter))
} 

const reducer = createReducer(INITIAL_STATE, {
  [OrderTourLogTypes.GET_ITEMS_SUCCESS]: setItems,
  [OrderTourLogTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [OrderTourLogTypes.CLEAR_ITEMS]: clearItems,
  [OrderTourLogTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
