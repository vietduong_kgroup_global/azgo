import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'BrandVehicleManagement'

export const BRAND_VEHICLE_STATE = fromJS({
  items: [],
  item: {},
  filter: {
    per_page: 10,
    page: 1,
    keyword: '',
  },
  totalCount: 0,
})
