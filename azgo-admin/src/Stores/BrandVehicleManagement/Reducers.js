import { fromJS } from 'immutable'

import { BRAND_VEHICLE_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { BrandVehicleManagementTypes } from './Actions'
import reducerRegistry from 'Stores/Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
      },
      totalCount: 0,
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
      totalCount: count,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(BRAND_VEHICLE_STATE, {
  [BrandVehicleManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [BrandVehicleManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [BrandVehicleManagementTypes.CLEAR_ITEMS]: clearItems,
  [BrandVehicleManagementTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
