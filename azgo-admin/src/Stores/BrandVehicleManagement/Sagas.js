import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import {
  BrandVehicleManagementTypes,
  BrandVehicleManagementActions,
} from './Actions'
import { BrandVehicleManagementService } from 'Services/BrandVehicleManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { BrandVehicleManagementSelectors } from 'Stores/BrandVehicleManagement/Selectors'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      BrandVehicleManagementService.getListItems,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: BrandVehicleManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: BrandVehicleManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(BrandVehicleManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: BrandVehicleManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: BrandVehicleManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(BrandVehicleManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: BrandVehicleManagementTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(push('/admin/vehicle/vehicle-manufacturer'))
  } catch (error) {
    yield put({
      type: BrandVehicleManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(
      BrandVehicleManagementService.createItem,
      values
    )
    checkResponseError(response)
    yield put({
      type: BrandVehicleManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/admin/vehicle/vehicle-manufacturer'))
  } catch (error) {
    yield put({
      type: BrandVehicleManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(BrandVehicleManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(BrandVehicleManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: BrandVehicleManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(push('/admin/vehicle/vehicle-manufacturer'))
    yield put(
      BrandVehicleManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: BrandVehicleManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(BrandVehicleManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(BrandVehicleManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(
      BrandVehicleManagementTypes.CREATE_ITEM_REQUEST,
      createItemRequest
    ),
    takeLatest(
      BrandVehicleManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
    takeLatest(
      BrandVehicleManagementTypes.DELETE_ITEM_REQUEST,
      deleteItemRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
