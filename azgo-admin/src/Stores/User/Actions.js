import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // login
    userLogin: ['dataLogin'],
    loginFailed: null,
    loginSuccess: null,
    showLoginError: null,
    removeMsgLoginFail: null,
    // log out
    doLogout: null,
    // fetch user info
    fetchUserInfo: ['id'],
    fetchApiVersionInfo: null, 
    fetchApiVersionInfoSuccess: null, 
    fetchUserInfoSuccess: ['dataUserInfo'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const UserTypes = Types
export const UserActions = Creators
