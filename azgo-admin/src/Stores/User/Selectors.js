import { MODULE_NAME } from './InitialState'

const getUserData = state => {
  return state[MODULE_NAME].get('userInfo')
    ? state[MODULE_NAME].get('userInfo')
    : {}
}
const getShowLoginError = state => {
  return state[MODULE_NAME].get('showLoginError')
}
const getAPIVersion = state => {
  return state[MODULE_NAME].get('apiVersion')
    ? state[MODULE_NAME].get('apiVersion')
    : ""
}
export const UserSelectors = {
  getUserData,
  getShowLoginError,
  getAPIVersion,
}
