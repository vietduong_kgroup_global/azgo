import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'user'

export const INITIAL_STATE = fromJS({
  userInfo: {},
  apiVersion: "",
  showLoginError: 'hide',
})
