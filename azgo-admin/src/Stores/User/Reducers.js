import { INITIAL_STATE } from './InitialState'
import { fromJS } from 'immutable'
import { createReducer } from 'reduxsauce'
import { UserTypes } from './Actions'
import {
  removeToken,
  setPermissions,
  setToken,
  getToken,
  removePermissions,
} from 'Utils/token'
import { apiClient, apiClientCore } from 'Services/index'
import { setApiKey } from 'react-geocode'

const loginSuccess = (state, { data }) => { 
  setToken(data._token, true)
  apiClient.setHeader('Authorization', 'bearer ' + getToken())
  apiClientCore.setHeader('Authorization', 'bearer ' + getToken())

  return state
}

// set state user info
const setUserInfo = (state, dataUserInfo) => {
  setPermissions(dataUserInfo.data.type)
  return state.merge(
    fromJS({
      userInfo: dataUserInfo.data,
    })
  )
}
const setApiVersion = (state, apiVersion) => { 
  return state.merge(
    fromJS({
      apiVersion: apiVersion.data,
    })
  )
}
 
const showLoginError = state => {
  return state.merge(
    fromJS({
      showLoginError: 'show',
    })
  )
}
const removeMsgLoginFail = state => {
  return state.merge(
    fromJS({
      showLoginError: 'hide',
    })
  )
}
// Log out
const doLogout = (state = INITIAL_STATE) => {
  removeToken()
  removePermissions()
  return INITIAL_STATE
}
const reducer = createReducer(INITIAL_STATE, {
  // List items action handler
  [UserTypes.LOGIN_SUCCESS]: loginSuccess,
  [UserTypes.LOGIN_FAILED]: showLoginError,
  [UserTypes.REMOVE_MSG_LOGIN_FAIL]: removeMsgLoginFail,

  [UserTypes.FETCH_USER_INFO_SUCCESS]: setUserInfo,
  [UserTypes.FETCH_API_VERSION_INFO_SUCCESS]: setApiVersion,
  // do log out
  [UserTypes.DO_LOGOUT]: doLogout,
})

export default reducer
