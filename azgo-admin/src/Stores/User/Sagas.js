import { put, call, takeLatest, all } from 'redux-saga/effects'
import { UserActions, UserTypes } from './Actions'
import { UserService } from 'Services/UserService'
import { LoadingActions } from 'Stores/Loading/Actions'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE, MANAGEMENT_TYPE_USER } from 'Utils/enum'
import { removePermissions, removeToken } from 'Utils/token'
import { changeLocation } from 'Utils/changeLocation'

// Get demo list worker
function* handleLogin({ dataLogin }) {
  try {
    dataLogin.current_role = 'admin'
    const response = yield call(UserService.login, dataLogin)
    checkResponseError(response)
    yield put({
      type: UserTypes.LOGIN_SUCCESS,
      data: response.data,
    }) 
    yield put(UserActions.fetchUserInfo(response.data.user_id)) 
    if (response.data.roles.includes(MANAGEMENT_TYPE_USER.ADMIN)) {
      if(response.data.company) yield put(push('/admin/tour/orderManagement'))
      else yield put(push('/admin'))
    } else {
      if (response.data.roles.includes(MANAGEMENT_TYPE_USER.PROVIDER)) {
        yield put(push('/admin/dashboard-provider'))
      } else {
        removePermissions()
        removeToken()
        changeLocation('/404')
      }
    }
  } catch (error) {
    yield put({
      type: UserTypes.LOGIN_FAILED,
    })
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
    yield put(LoadingActions.hideLoadingList())
  }
}
// fetch user info
function* fetchUserInfo({ id }) {
  try {
    const response = yield call(UserService.fetchUserInfo, id)
    checkResponseError(response)
    yield put({
      type: UserTypes.FETCH_USER_INFO_SUCCESS,
      data: response.data,
    })
  } catch (error) {
    yield put(UserActions.doLogout())
    yield put(push('/login'))
  }
}
// fetch api version
function* fetchAPIVersion({ id }) {
  try {
    const response = yield call(UserService.fetchAPIVersion, id)
    checkResponseError(response)
    yield put({
      type: UserTypes.FETCH_API_VERSION_INFO_SUCCESS,
      data: response.data,
    })
  } catch (error) { 
  }
}
function* watcher() {
  yield all([
    takeLatest(UserTypes.USER_LOGIN, handleLogin),
    takeLatest(UserTypes.FETCH_USER_INFO, fetchUserInfo),
    takeLatest(UserTypes.FETCH_API_VERSION_INFO, fetchAPIVersion), 
  ])
}

export default watcher
