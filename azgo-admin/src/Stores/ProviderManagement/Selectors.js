import { MODULE_NAME } from './InitialState'

const getFilter = state => state[MODULE_NAME].get('filter')

const getItems = state => state[MODULE_NAME].get('items')

const getItemsList = state => state[MODULE_NAME].get('itemsList')

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

const getItem = state => state[MODULE_NAME].get('item')

export const ProviderManagementSelectors = {
  getFilter,
  getItems,
  getItemsList,
  getTotalCount,
  getItem,
}
