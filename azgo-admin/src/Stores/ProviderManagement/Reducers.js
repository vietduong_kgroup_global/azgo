import { fromJS } from 'immutable'

import { PROVIDER_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { ProviderManagementTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      itemsList: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
      },
      totalCount: 0,
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
      totalCount: count,
    })
  )
}

const setItemsList = (state, { itemsList }) => {
  return state.merge(
    fromJS({
      itemsList,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(PROVIDER_STATE, {
  [ProviderManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [ProviderManagementTypes.GET_ITEMS_LIST_SUCCESS]: setItemsList,
  [ProviderManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [ProviderManagementTypes.CLEAR_ITEMS]: clearItems,
  [ProviderManagementTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
