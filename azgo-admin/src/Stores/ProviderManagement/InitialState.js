import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'ProviderManagement'

export const PROVIDER_STATE = fromJS({
  items: [],
  itemsList: [],
  item: {},
  filter: {
    per_page: 10,
    page: 1,
    keyword: '',
  },
  totalCount: 0,
})
