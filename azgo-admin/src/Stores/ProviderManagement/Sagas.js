import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { ProviderManagementTypes, ProviderManagementActions } from './Actions'
import { ProviderManagementService } from 'Services/ProviderManagementService'
import { BlockUserService } from 'Services/BlockUser'
import { LoadingActions } from 'Stores/Loading/Actions'
import { ProviderManagementSelectors } from './Selectors'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      ProviderManagementService.getListItems,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: ProviderManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: ProviderManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}
// Get List Provider ( use for select)
function* getItemsListRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      ProviderManagementService.getListItemsOfDriver,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: ProviderManagementTypes.GET_ITEMS_LIST_SUCCESS,
      itemsList: response.data,
    })
  } catch (error) {
    yield put({
      type: ProviderManagementTypes.GET_ITEMS_LIST_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(ProviderManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: ProviderManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: ProviderManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(ProviderManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: ProviderManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/provider-management'))
  } catch (error) {
    yield put({
      type: ProviderManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(ProviderManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: ProviderManagementTypes.EDIT_ITEM_SUCCESS,
    })
    if (data.changeBlockUser === true && data.status === 0) {
      yield call(BlockUserService.unblockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 1) {
      yield call(BlockUserService.blockUser, data.id)
    }
    yield put(push('/provider-management'))
  } catch (error) {
    yield put({
      type: ProviderManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(ProviderManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(ProviderManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: ProviderManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      ProviderManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: ProviderManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(ProviderManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(
      ProviderManagementTypes.GET_ITEMS_LIST_REQUEST,
      getItemsListRequest
    ),
    takeLatest(ProviderManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(ProviderManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(ProviderManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
    takeLatest(
      ProviderManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
