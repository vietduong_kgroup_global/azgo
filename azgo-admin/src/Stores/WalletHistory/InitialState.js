import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'WalletHistory'

export const INIT_STATE = fromJS({
  items: [],
  itemsOfProvider: [],
  item: {},
  filter: {
    per_page: 10,
    page: 1,
    keyword: '',
    driver_uuid: '',
  },
  totalCount: 0,
})
