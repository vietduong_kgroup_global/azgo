import { fromJS } from 'immutable'

import { INIT_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { WalletHistoryTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
        driver_uuid: '',
      },
      totalCount: 0,
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
      totalCount: count,
    })
  )
}

const setItemsOfProvider = (state, itemsOfProvider) => {
  return state.merge(
    fromJS({
      itemsOfProvider: itemsOfProvider.itemsOfProvider,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(INIT_STATE, {
  [WalletHistoryTypes.GET_ITEMS_SUCCESS]: setItems,
  [WalletHistoryTypes.GET_ITEMS_SUCCESS_OF_PROVIDER]: setItemsOfProvider,
  [WalletHistoryTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [WalletHistoryTypes.CLEAR_ITEMS]: clearItems,
  [WalletHistoryTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
