import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { WalletHistoryTypes, WalletHistoryActions } from './Actions'
import { WalletHistoryService } from 'Services/WalletHistoryService'
import { BlockUserService } from 'Services/BlockUser'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { WalletHistorySelectors } from './Selectors'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) {  
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      WalletHistoryService.getListItems,
      filter && filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())  
    yield put({
      type: WalletHistoryTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) { 
  console.log(error)
    yield put({
      type: WalletHistoryTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemsRequestOfProvider({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      WalletHistoryService.getListItemsOfProvider,
      id
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: WalletHistoryTypes.GET_ITEMS_SUCCESS_OF_PROVIDER,
      itemsOfProvider: response.data[0],
    })
  } catch (error) {
    yield put({
      type: WalletHistoryTypes.GET_ITEMS_FAILURE_OF_PROVIDER,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) { 
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(WalletHistoryService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: WalletHistoryTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: WalletHistoryTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(WalletHistoryService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: WalletHistoryTypes.EDIT_ITEM_SUCCESS,
    })
    if (data.changeBlockUser === true && data.status === 0) {
      yield call(BlockUserService.unblockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 1) {
      yield call(BlockUserService.blockUser, data.id)
    }
    yield put(push('/admin/wallet'))
  } catch (error) {
    yield put({
      type: WalletHistoryTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(WalletHistoryService.createItem, values)
    checkResponseError(response)
    yield put({
      type: WalletHistoryTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/admin/wallet'))
  } catch (error) {
    yield put({
      type: WalletHistoryTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(WalletHistorySelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(WalletHistoryService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: WalletHistoryTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      WalletHistoryActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: WalletHistoryTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(WalletHistoryTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(
      WalletHistoryTypes.GET_ITEMS_REQUEST_OF_PROVIDER,
      getItemsRequestOfProvider
    ),
    takeLatest(WalletHistoryTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(WalletHistoryTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(
      WalletHistoryTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
    takeLatest(WalletHistoryTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
