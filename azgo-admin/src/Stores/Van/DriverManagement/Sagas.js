import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { DriverManagementTypes, DriverManagementActions } from './Actions'
import { DriverManagementService } from 'Services/Van/DriverManagementService'
import { BlockUserService } from 'Services/BlockUser'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { DriverManagementSelectors } from 'Stores/Van/DriverManagement/Selectors'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      DriverManagementService.getListItems,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: DriverManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: DriverManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(DriverManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: DriverManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: DriverManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(DriverManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: DriverManagementTypes.EDIT_ITEM_SUCCESS,
    })
    if (data.changeBlockUser === true && data.status === 0) {
      yield call(BlockUserService.unblockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 1) {
      yield call(BlockUserService.blockUser, data.id)
    }
    yield put(push('/driver/van'))
  } catch (error) {
    yield put({
      type: DriverManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(DriverManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: DriverManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/driver/van'))
  } catch (error) {
    yield put({
      type: DriverManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(DriverManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(DriverManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: DriverManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      DriverManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: DriverManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemsWithoutRequest({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      DriverManagementService.getListWithoutItems,
      values
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: DriverManagementTypes.GET_ITEMS_WITHOUT_SUCCESS,
      items: response.data.results,
    })
  } catch (error) {
    yield put({
      type: DriverManagementTypes.GET_ITEMS_WITHOUT_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(DriverManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(DriverManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(DriverManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(
      DriverManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
    takeLatest(DriverManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
    takeLatest(
      DriverManagementTypes.GET_ITEMS_WITHOUT_REQUEST,
      getItemsWithoutRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
