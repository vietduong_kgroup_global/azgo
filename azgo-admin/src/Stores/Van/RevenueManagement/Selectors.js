import { MODULE_NAME } from './InitialState'

const getFilter = state => state[MODULE_NAME].get('filter')

const getItems = state => state[MODULE_NAME].get('items')

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

export const RevenueManagementVanSelectors = {
  getFilter,
  getItems,
  getTotalCount,
}
