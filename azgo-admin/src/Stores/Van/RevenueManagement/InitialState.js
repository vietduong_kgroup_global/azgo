import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'RevenueManagementVan'

export const INITIAL_STATE = fromJS({
  items: [],
  filter: {
    per_page: 10,
    page: 1,
    provider_id: '',
    to_date: '',
    from_date: '',
  },
  totalCount: 0,
})
