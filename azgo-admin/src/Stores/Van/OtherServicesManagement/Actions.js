import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List OtherServices actions
    getItemsRequest: null,
    getItemsSuccess: ['items'],
    getItemsFailure: null,
    clearItems: null,
    // Edit OtherServices actions
    editItemRequest: ['id'],
    editItemSuccess: ['item'],
    editItemFailure: null,
    // Create OtherServices actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
    // Delete OtherServices actions
    deleteItemRequest: ['id'],
    deleteItemSuccess: null,
    deleteItemFailure: null,
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const OtherServicesManagementTypes = Types
export const OtherServicesManagementActions = Creators
