import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { OtherServicesManagementTypes } from './Actions'
import reducerRegistry from 'Stores/Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
    })
  )
}

const setItems = (state, { items }) => {
  return state.merge(
    fromJS({
      items,
    })
  )
}

const reducer = createReducer(INITIAL_STATE, {
  [OtherServicesManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [OtherServicesManagementTypes.CLEAR_ITEMS]: clearItems,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
