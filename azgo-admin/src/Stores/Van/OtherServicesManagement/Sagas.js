import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import {
  OtherServicesManagementTypes,
  OtherServicesManagementActions,
} from './Actions'
import { OtherServiceManagement } from 'Services/Van/OtherServiceManagement'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { checkResponseError } from 'Utils/checkResponseError'

function* getItemsRequest() {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(OtherServiceManagement.getListItems)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: OtherServicesManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data,
    })
  } catch (error) {
    yield put({
      type: OtherServicesManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(OtherServiceManagement.createItem, values)
    checkResponseError(response)
    yield put({
      type: OtherServicesManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(OtherServicesManagementActions.getItemsRequest())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Thêm dịch vụ thành công',
      },
    })
  } catch (error) {
    yield put({
      type: OtherServicesManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* editItemRequest(values) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(OtherServiceManagement.editItem, values.id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: OtherServicesManagementTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(OtherServicesManagementActions.getItemsRequest())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Chỉnh sửa thành công',
      },
    })
  } catch (error) {
    yield put({
      type: OtherServicesManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(OtherServiceManagement.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: OtherServicesManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.WARNING,
        message: 'Xóa dịch vụ thành công',
      },
    })
    yield put(OtherServicesManagementActions.getItemsRequest())
  } catch (error) {
    yield put({
      type: OtherServicesManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(OtherServicesManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(
      OtherServicesManagementTypes.CREATE_ITEM_REQUEST,
      createItemRequest
    ),
    takeLatest(OtherServicesManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(
      OtherServicesManagementTypes.DELETE_ITEM_REQUEST,
      deleteItemRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
