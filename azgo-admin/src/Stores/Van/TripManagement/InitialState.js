import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'TripVaManagement'

export const INITIAL_STATE = fromJS({
  items: [],
  item: {},
  filter: {
    per_page: 10,
    page: 1,
    keyword: '',
  },
  totalCount: 0,
})
