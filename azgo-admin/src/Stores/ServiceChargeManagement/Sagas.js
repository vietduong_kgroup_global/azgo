import { put, call, takeLatest, all, select } from 'redux-saga/effects'
import {
  ServiceChargeManagementTypes,
  ServiceChargeManagementActions,
} from './Actions'
import { ServiceChargeManagementService } from 'Services/ServiceChargeManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { fromJS } from 'immutable'

function* getItemsRequest() {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(ServiceChargeManagementService.getListItems)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: ServiceChargeManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data,
    })
  } catch (error) {
    yield put({
      type: ServiceChargeManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* editItemRequest({ values }) {
  try {
    yield put(LoadingActions.showPopUp())
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(ServiceChargeManagementService.editItem, values)
    checkResponseError(response)
    yield put({
      type: ServiceChargeManagementTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hidePopUp())
    yield put(LoadingActions.hideLoadingAction())
    yield put(ServiceChargeManagementActions.getItemsRequest())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Chỉnh sửa phí dịch vụ thành công',
      },
    })
  } catch (error) {
    yield put({
      type: ServiceChargeManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* watcher() {
  yield all([
    takeLatest(ServiceChargeManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(ServiceChargeManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
