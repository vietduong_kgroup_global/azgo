import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List ServiceCharge actions
    getItemsRequest: null,
    getItemsSuccess: ['items', 'count'],
    getItemsFailure: null,
    clearItems: null,

    // Edit ServiceCharge actions
    editItemRequest: ['values'],
    editItemSuccess: ['item'],
    editItemFailure: null,
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const ServiceChargeManagementTypes = Types
export const ServiceChargeManagementActions = Creators
