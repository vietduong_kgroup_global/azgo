import { fromJS } from 'immutable'
import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { ServiceChargeManagementTypes } from './Actions'
import reducerRegistry from 'Stores/Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
    })
  )
}

const reducer = createReducer(INITIAL_STATE, {
  [ServiceChargeManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [ServiceChargeManagementTypes.CLEAR_ITEMS]: clearItems,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
