import { MODULE_NAME } from './InitialState'

const getItems = state => state[MODULE_NAME].get('items')

export const ServiceChargeManagementSelectors = {
  getItems,
}
