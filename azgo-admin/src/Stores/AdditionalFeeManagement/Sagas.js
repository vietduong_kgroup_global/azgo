import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { AdditionalFeeManagementTypes, AdditionalFeeManagementActions } from './Actions'
import { AdditionalFeeManagementService } from 'Services/AdditionalFeeManagementService'
import { BlockUserService } from 'Services/BlockUser'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { AdditionalFeeManagementSelectors } from 'Stores/AdditionalFeeManagement/Selectors'
import { fromJS } from 'immutable'

function* getItemsRequest({ filter }) { 
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      AdditionalFeeManagementService.getListItems,
      filter && filter.toJS() 
    ) 
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: AdditionalFeeManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: AdditionalFeeManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(AdditionalFeeManagementService.getItemById, id)
    // checkResponseError(response)
    yield put(LoadingActions.hideLoadingList()) 
    yield put({
      type: AdditionalFeeManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: AdditionalFeeManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(AdditionalFeeManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: AdditionalFeeManagementTypes.EDIT_ITEM_SUCCESS,
    })
    if (data.changeBlockUser === true && data.status === 0) {
      yield call(BlockUserService.unblockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 1) {
      yield call(BlockUserService.blockUser, data.id)
    }
    yield put(push('/admin/additional-fee'))
  } catch (error) {
    yield put({
      type: AdditionalFeeManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(AdditionalFeeManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: AdditionalFeeManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/admin/additional-fee'))
  } catch (error) {
    yield put({
      type: AdditionalFeeManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(AdditionalFeeManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(AdditionalFeeManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: AdditionalFeeManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      AdditionalFeeManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
    yield put(push('/admin/additional-fee'))

  } catch (error) {
    yield put({
      type: AdditionalFeeManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* getItemsWithoutRequest({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(
      AdditionalFeeManagementService.getListWithoutItems,
      values
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: AdditionalFeeManagementTypes.GET_ITEMS_WITHOUT_SUCCESS,
      items: response.data.results,
    })
  } catch (error) {
    yield put({
      type: AdditionalFeeManagementTypes.GET_ITEMS_WITHOUT_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(AdditionalFeeManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(AdditionalFeeManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(AdditionalFeeManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(
      AdditionalFeeManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
    takeLatest(AdditionalFeeManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
    takeLatest(
      AdditionalFeeManagementTypes.GET_ITEMS_WITHOUT_REQUEST,
      getItemsWithoutRequest
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
