import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'WalletManagement'

export const DRIVER_STATE = fromJS({
  items: [],
  itemsOfProvider: [],
  item: {},
  filter: {
    per_page: 10,
    page: 1,
    keyword: '',
  },
  totalCount: 0,
})
