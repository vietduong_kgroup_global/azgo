import { fromJS } from 'immutable'

import { DRIVER_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { WalletManagementTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const clearItems = state => {
  return state.merge(
    fromJS({
      items: [],
      item: {},
      filter: {
        per_page: 10,
        page: 1,
        keyword: '',
      },
      totalCount: 0,
    })
  )
}

const setItem = (state, { item }) => {
  return state.merge(
    fromJS({
      item,
    })
  )
}

const setItems = (state, { items, count }) => {
  return state.merge(
    fromJS({
      items,
      totalCount: count,
    })
  )
}

const setItemsOfProvider = (state, itemsOfProvider) => {
  return state.merge(
    fromJS({
      itemsOfProvider: itemsOfProvider.itemsOfProvider,
    })
  )
}

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(DRIVER_STATE, {
  [WalletManagementTypes.GET_ITEMS_SUCCESS]: setItems,
  [WalletManagementTypes.GET_ITEMS_SUCCESS_OF_PROVIDER]: setItemsOfProvider,
  [WalletManagementTypes.GET_ITEM_DETAIL_SUCCESS]: setItem,
  [WalletManagementTypes.CLEAR_ITEMS]: clearItems,
  [WalletManagementTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
