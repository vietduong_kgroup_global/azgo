import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { WalletManagementTypes, WalletManagementActions } from './Actions'
import { WalletManagementService } from 'Services/WalletManagementService'
import { BlockUserService } from 'Services/BlockUser'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { WalletManagementSelectors } from './Selectors'
import { fromJS } from 'immutable'
import { WalletHistoryService } from 'Services/WalletHistoryService'
import { DriverManagementTypes } from '../DriverManagement/Actions'
 
function* getItemsRequest({ filter }) { 
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      WalletManagementService.getListItems,
      filter && filter.toJS()
    )
    checkResponseError(response) 
    yield put(LoadingActions.hideLoadingList())  
    yield put({
      type: WalletManagementTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) { 
  console.log(error)
    yield put({
      type: WalletManagementTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemsRequestOfProvider({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      WalletManagementService.getListItemsOfProvider,
      id
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: WalletManagementTypes.GET_ITEMS_SUCCESS_OF_PROVIDER,
      itemsOfProvider: response.data[0],
    })
  } catch (error) {
    yield put({
      type: WalletManagementTypes.GET_ITEMS_FAILURE_OF_PROVIDER,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) { 
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(WalletManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: WalletManagementTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data || {},
    })
  } catch (error) {
    yield put({
      type: WalletManagementTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* editItemRequest({values, callback} ) {   
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(WalletManagementService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    if (callback) {
      callback(response);
    } 
    // yield put(push(`/admin/wallet/detail/${values.id}`))

    yield put({
      type: WalletManagementTypes.EDIT_ITEM_SUCCESS,
    })
    if (data.changeBlockUser === true && data.status === 0) {
      yield call(BlockUserService.unblockUser, data.id)
    }
    if (data.changeBlockUser === true && data.status === 1) {
      yield call(BlockUserService.blockUser, data.id)
    } 
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Chỉnh sửa tài khoả ví thành công',
      },
    })
    // yield put(push(`/admin/wallet/detail/${values.id}`))
  } catch (error) {
    console.log(error)
    yield put({
      type: WalletManagementTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(WalletManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: WalletManagementTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/admin/wallet'))
  } catch (error) {
    yield put({
      type: WalletManagementTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}
function* createWalletForDriverRequest({ values }) { 
  try {
    const response = yield call(WalletManagementService.createItem, values)
    checkResponseError(response)
    yield put({ 
      type: WalletManagementTypes.CREATE_WALLET_FOR_DRIVER_SUCCESS,
    }) 
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.SUCCESS,
        message: 'Tạo tài khoản ví cho tài xế thành công',
      },
    })
    yield put({
      type: DriverManagementTypes.GET_ITEMS_REQUEST,
    })
  } catch (error) {
    yield put({
      type: WalletManagementTypes.CREATE_WALLET_FOR_DRIVER_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

 
function* deleteItemRequest({ id }) {
  try {
    const filter = yield select(WalletManagementSelectors.getFilter)
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(WalletManagementService.deleteItem, id)
    checkResponseError(respones)
    yield put({
      type: WalletManagementTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(
      WalletManagementActions.getItemsRequest(
        fromJS({
          ...filter.toJS(),
          page: 1,
        })
      )
    )
  } catch (error) {
    yield put({
      type: WalletManagementTypes.DELETE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(WalletManagementTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(
      WalletManagementTypes.GET_ITEMS_REQUEST_OF_PROVIDER,
      getItemsRequestOfProvider
    ),
    takeLatest(WalletManagementTypes.EDIT_ITEM_REQUEST, editItemRequest),
    takeLatest(WalletManagementTypes.CREATE_ITEM_REQUEST, createItemRequest),
    takeLatest(
      WalletManagementTypes.GET_ITEM_DETAIL_REQUEST,
      getItemDetailRequest
    ),
    takeLatest(WalletManagementTypes.DELETE_ITEM_REQUEST, deleteItemRequest),
    takeLatest(WalletManagementTypes.CREATE_WALLET_FOR_DRIVER_REQUEST, createWalletForDriverRequest), 
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
