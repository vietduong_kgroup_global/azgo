import { MODULE_NAME } from './InitialState'

const getFilter = state => state[MODULE_NAME].get('filter')

const getItems = state => state[MODULE_NAME].get('items')

const getItemsOfProvider = state => state[MODULE_NAME].get('itemsOfProvider')

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

const getItem = state => state[MODULE_NAME].get('item')

export const WalletManagementSelectors = {
  getFilter,
  getItems,
  getItemsOfProvider,
  getTotalCount,
  getItem,
}
