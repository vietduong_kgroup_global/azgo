import { put, call, takeLatest, all } from 'redux-saga/effects'
import { TicketTypes } from './Actions'
import { ProviderManagementService } from 'Services/ProviderManagementService'
import { LoadingActions } from 'Stores/Loading/Actions'
import sagaRegistry from 'Stores/Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { push } from 'connected-react-router'
import { NotificationTypes } from 'Stores/Notification/Actions'
import { TYPE_MESSAGE } from 'Utils/enum'
import { TicketService } from 'Services/TicketService'

function* getItemsRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      ProviderManagementService.getListItems,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: TicketTypes.GET_ITEMS_SUCCESS,
      items: response.data.results,
      count: response.data.count,
    })
  } catch (error) {
    yield put({
      type: TicketTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}
// Get List Provider ( use for select)
function* getItemsListRequest({ filter }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(
      ProviderManagementService.getListItemsOfDriver,
      filter
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: TicketTypes.GET_ITEMS_LIST_SUCCESS,
      itemsList: response.data,
    })
  } catch (error) {
    yield put({
      type: TicketTypes.GET_ITEMS_LIST_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getItemDetailRequest({ id }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(ProviderManagementService.getItemById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: TicketTypes.GET_ITEM_DETAIL_SUCCESS,
      item: response.data,
    })
  } catch (error) {
    yield put({
      type: TicketTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

function* createItemRequest({ values }) {
  try {
    const response = yield call(ProviderManagementService.createItem, values)
    checkResponseError(response)
    yield put({
      type: TicketTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(push('/provider-management'))
  } catch (error) {
    yield put({
      type: TicketTypes.CREATE_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* editItemRequest({ values }) {
  try {
    let data = {
      ...values,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(TicketService.editItem, data)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: TicketTypes.EDIT_ITEM_SUCCESS,
      item: response.data,
    })
    window.location.reload()
  } catch (error) {
    yield put({
      type: TicketTypes.EDIT_ITEM_FAILURE,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put({
      type: NotificationTypes.SHOW_NOTIFY_REQUEST,
      notifyInfo: {
        type: TYPE_MESSAGE.ERROR,
        message: error.message,
      },
    })
  }
}

function* watcher() {
  yield all([
    takeLatest(TicketTypes.GET_ITEMS_REQUEST, getItemsRequest),
    takeLatest(TicketTypes.EDIT_ITEM_REQUEST, editItemRequest),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
