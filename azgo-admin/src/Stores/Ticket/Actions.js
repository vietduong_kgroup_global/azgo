import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List Ticket actions
    getItemsRequest: ['filter'],
    getItemsSuccess: ['items', 'count'],
    getItemsFailure: null,
    clearItems: null,
    // Get Ticket Detail
    getItemDetailRequest: ['id'],
    getItemDetailSuccess: ['item'],
    getItemDetailFailure: null,
    // Update Ticket
    editItemRequest: ['values'],
    editItemSuccess: ['item'],
    editItemFailure: null,
    // Change Filter
    setFilter: ['filter'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const TicketTypes = Types
export const TicketActions = Creators
