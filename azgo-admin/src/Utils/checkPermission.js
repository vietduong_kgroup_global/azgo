export const havePermission = (permissions = []) => {
  return permissions ? permissions.length !== 0 : false
}

export const canAccess = (permissions = [], name = '') => {
  return Boolean(permissions.includes(name))
}

export const checkPermissionsByPath = (permissions = [], path = '') => {
  switch (path) {
    case '/login':
      if (!permissions.includes('users.manage')) return true
      break
    case '/dashboard':
      if (!permissions.includes('users.manage')) return false
      break
    default:
      break
  }
  return false
}
