import moment from 'moment'
import { Config } from 'Config'
import { parsePhoneNumberFromString, findPhoneNumbersInText  } from "libphonenumber-js"; 
export const rangeYear = (yearFrom = 1900, yearTo = moment().year() + 10) => {
  let years = []
  for (let i = yearTo; i >= yearFrom; i--) years.push(i)
  return years
}
export const handleScroll = e => {
  return e.target.scrollTop !== 0
}

// export const getUrlENV = typeUrl => {
//   // check ENV
//   const env = 'DEV'
//   switch (env) {
//     case 'PRODUCTION':
//       return null
//     case 'TESTING':
//       return null
//     // default ENV = DEV
//     default:
//       switch (typeUrl) {
//         case 'FILE_SERVICE':
//           return Config.DEV_URL.FILE_SERVICE
//         default:
//           return null
//       }
//   }
// }

export const alphabetToInt = char => {
  try {
    return char.toLowerCase().charCodeAt(0) - 'a'.charCodeAt(0) + 1
  } catch (error) {
    return null
  }
}

export function formatPrice(value = 0) {
  const val = (value / 1).toFixed(0).replace('.', ',')
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
}

export const formatPhone = (value, value2) => {
  if (value && value2) {
    if (value === 84) {
      let index = value2.toString().slice(0, 1)
      let index2 = value2.toString().slice(0, 2)
      if (index === '0' || index2 === '18' || index2 === '19') {
        return value2
      } else {
        return '0' + value2
      }
    } else {
      return value + value2
    }
  } else {
    return null
  }
}
 
export function _findPhoneNumbersInText(phone) {
  try{
  let find = findPhoneNumbersInText(phone, 'VN')
  return (find && find.length) ? find[0].number.nationalNumber : null
  } catch(error) {
    return null
  }
}
export function _parsePhoneNumber(countryCode, phone) { 
  let phoneCountryCode = "+"+countryCode + " " + phone; 
  let phoneNumber = parsePhoneNumberFromString(phoneCountryCode);
  return {
    isValid: phoneNumber ? phoneNumber.isValid() : false,
    format_internal: phoneNumber
      ? phoneNumber.formatInternational().replace(/ /g, "")
      : "",
    format_not_calling: phoneNumber
      ? phoneNumber
          .formatInternational()
          .replace(countryCode, "")
          .replace(/ /g, "")
      : "",
    format_national: phoneNumber
      ? phoneNumber.formatNational().replace(/ /g, "")
      : ""
  };
}
export function getNumberOnString(text) {
  let result = text.match(/\d+/g);
  return result ? result.join("") : "";
} 
export const formatDate = (date) => {
  const dateSplit = date.split('/');
  // month is 0-based, that's why we need dataParts[1] - 1
  const dateObject = new Date(+dateSplit[2],  +dateSplit[1] - 1, +dateSplit[0]);
  const formatDate = moment(dateObject, 'YYYY-MM-DD');
  return formatDate.format('YYYY-MM-DD').toString();
}

export const formatDateTime = (date) => { 
  return  moment(new Date(date)).format('DD/MM/YYYY HH:mm:ss')
} 
export const changeFormatDate =  (stringDate) => { // input 2021-03-01  => output: 01/03/2021
  try {
    let split = stringDate.split('-')
    return `${split[2]}/${split[1]}/${split[0]}`
  }
  catch(error) {
    console.log(error)
    return ""
  }  
}

export const changeFormatObjectDate =  (stringDate) => { // input 2021-03-01  => output: 03/01/2021
  try {
    let split = stringDate.split('-')
    return new Date(`${split[1]}/${split[2]}/${split[0]}`)
  }
  catch(error) {
    console.log(error)
    return ""
  }  
}
