export const MANAGEMENT_STATUS = {
  ACTIVATE: 1,
  INACTIVE: 0,
}

export const MANAGEMENT_STATUS_ARR = [
  {
    key: MANAGEMENT_STATUS.ACTIVATE,
    label: 'Hoạt động',
  },
  {
    key: MANAGEMENT_STATUS.INACTIVE,
    label: 'Không hoạt động',
  },
]

export const MANAGEMENT_STATUS_TRIP = {
  PREPARE: 1,
  ARRIVE: 2,
  BOARDING: 3,
  COMPLETED: 4,
  CANCEL: 5,
}

export const MANAGEMENT_STATUS_TOUR = {
  VERIFYING: 1,
  ACCEPTED_DRIVER: 2,
  ACCEPTED_CUSTOMER: 3,
  CANCELED_DRIVER: 4,
  CANCELED_CUSTOMER: 5,
  DONE: 6,
  PICKED_UP_CUSTOMER: 7
}

export const MANAGEMENT_STATUS_TRIP_ARR = [
  {
    key: MANAGEMENT_STATUS_TRIP.PREPARE,
    label: 'Chưa đi',
    color: 'secondary',
  },
  {
    key: MANAGEMENT_STATUS_TRIP.ARRIVE,
    label: 'Đã đến nơi',
    color: 'primary',
  },
  {
    key: MANAGEMENT_STATUS_TRIP.BOARDING,
    label: 'Khách lên xe',
    color: 'info',
  },
  {
    key: MANAGEMENT_STATUS_TRIP.COMPLETED,
    label: 'Hoàn thành',
    color: 'success',
  },
  {
    key: MANAGEMENT_STATUS_TRIP.CANCEL,
    label: 'Hủy chuyến',
    color: 'warning',
  },
]
export const MANAGEMENT_STATUS_TOUR_ARR = [
  {
    key: "VERIFYING",
    value: MANAGEMENT_STATUS_TOUR.VERIFYING,
    label: 'Chờ xác nhận',
    color: 'secondary',
  },
  {
    key: "ACCEPTED_DRIVER",
    value: MANAGEMENT_STATUS_TOUR.ACCEPTED_DRIVER,
    label: 'Tài xế đã xác nhận',
    color: 'primary',
  },
  {
    key: "ACCEPTED_CUSTOMER",
    value: MANAGEMENT_STATUS_TOUR.ACCEPTED_CUSTOMER,
    label: 'Khách hàng đã xác nhận',
    color: 'info',
  },
  {
    key: "PICKED_UP_CUSTOMER",
    value: MANAGEMENT_STATUS_TOUR.PICKED_UP_CUSTOMER,
    label: 'Tài xế đã đến điểm đón',
    color: 'info',
  },
  {
    key: "CANCELED_DRIVER",
    value: MANAGEMENT_STATUS_TOUR.CANCELED_DRIVER,
    label: 'Tài xế huỷ',
    color: 'danger',
  },
  {
    key: "CANCELED_CUSTOMER",
    value: MANAGEMENT_STATUS_TOUR.CANCELED_CUSTOMER,
    label: 'Khách hàng huỷ',
    color: 'warning',
  },
  {
    key: "DONE",
    value: MANAGEMENT_STATUS_TOUR.DONE,
    label: 'Hoàn tất',
    color: 'success',
  },
]
export const MANAGEMENT_STATUS_VAN_TRIP = {
  APPROVE: 1,
  RECEIVED: 2,
  SUCCESSFULLY_DELIVERY: 3,
  COMPLETED: 4,
  CANCEL: 5,
}

export const MANAGEMENT_STATUS_VAN_TRIP_ARR = [
  {
    key: MANAGEMENT_STATUS_VAN_TRIP.APPROVE,
    label: 'Chấp nhận',
    color: 'secondary',
  },
  {
    key: MANAGEMENT_STATUS_VAN_TRIP.RECEIVED,
    label: 'Đã nhận hàng',
    color: 'primary',
  },
  {
    key: MANAGEMENT_STATUS_VAN_TRIP.SUCCESSFULLY_DELIVERY,
    label: 'Giao hàng thành công',
    color: 'info',
  },
  {
    key: MANAGEMENT_STATUS_VAN_TRIP.COMPLETED,
    label: 'Hoàn tất',
    color: 'success',
  },
  {
    key: MANAGEMENT_STATUS_VAN_TRIP.CANCEL,
    label: 'Đã hủy',
    color: 'warning',
  },
]

export const MANAGEMENT_GENDER = {
  ORTHER: 0,
  MALE: 1,
  FEMALE: 2,
}

export const MANAGEMENT_TYPE_PROMOTION = {
  PERCENT: 0,
  VALUE: 1,
}

export const MANAGEMENT_TYPE_ADDITIONAL_FEE = {
  PERCENT: 'percent',
  FLAT: 'flat',
}

export const MANAGEMENT_GENDER_ARR = [
  {
    key: MANAGEMENT_GENDER.ORTHER,
    label: 'Khác',
  },
  {
    key: MANAGEMENT_GENDER.MALE,
    label: 'Nam',
  },
  {
    key: MANAGEMENT_GENDER.FEMALE,
    label: 'Nữ',
  },
]

export const MANAGEMENT_TYPE_PROMOTION_ARR = [
  {
    key: MANAGEMENT_TYPE_PROMOTION.PERCENT,
    label: 'Phần trăm',
  },
  {
    key: MANAGEMENT_TYPE_PROMOTION.VALUE,
    label: 'Số tiền',
  },
]
export const MANAGEMENT_TYPE_ADDITIONAL_FEE_ARR = [
  {
    key: MANAGEMENT_TYPE_ADDITIONAL_FEE.FLAT,
    label: 'Số tiền',
  },
  {
    key: MANAGEMENT_TYPE_ADDITIONAL_FEE.PERCENT,
    label: 'Phần trăm',
  },
]

export const MANAGEMENT_TYPE_USER = {
  ADMIN: 1,
  DRIVER: 2,
  CUSTOMER: 3,
  PROVIDER: 4,
  DRIVER_BIKE_CAR: 5,
  DRIVER_VAN_BAGAC: 6,
  DRIVER_TOUR: 7,
  REGION_MANAGER: 8,
}

export const MANAGEMENT_TYPE_USER_ARR = [
  {
    key: MANAGEMENT_TYPE_USER.ADMIN,
    label: 'Admin',
  },
  {
    key: MANAGEMENT_TYPE_USER.DRIVER,
    label: 'Tài xế',
  },
  {
    key: MANAGEMENT_TYPE_USER.CUSTOMER,
    label: 'Khách hàng',
  },
  {
    key: MANAGEMENT_TYPE_USER.PROVIDER,
    label: 'Nhà xe',
  },
  {
    key: MANAGEMENT_TYPE_USER.DRIVER_BIKE_CAR,
    label: 'Tài xế bike/car',
  },
  {
    key: MANAGEMENT_TYPE_USER.DRIVER_VAN_BAGAC,
    label: 'Tài xế van/bagac',
  }, 
  {
    key: MANAGEMENT_TYPE_USER.DRIVER_TOUR,
    label: 'Tài xế tour',
  },
]

export const PLATFORM_TYPE = {
  WEB: 1,
  CUSTOMER_APP: 2,
  DRIVER_APP: 3,
}

export const ACCOUNT_INITIAL_ROLE = {
  ADMIN: 1,
  REGION_MANAGER: 8,
  ACCOUNTANT: 9,
  OPERATOR: 10,
}
export const ACCOUNT_INITIAL_ROLE_ARR = [
  {
    key: ACCOUNT_INITIAL_ROLE.ADMIN,
    label: 'Admin',
  },
  {
    key: ACCOUNT_INITIAL_ROLE.REGION_MANAGER,
    label: 'Quản lý khu vực',
  },
  {
    key: ACCOUNT_INITIAL_ROLE.ACCOUNTANT,
    label: 'Kế toán',
  },
  {
    key: ACCOUNT_INITIAL_ROLE.OPERATOR,
    label: 'Điều hành',
  },
]

export const TYPE_MESSAGE = {
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
  WARNING: 'WARNING',
}

export const MANAGEMENT_STATUS_SCHEDULE_BUS = {
  NOTSTARTED: 1,
  STARTED: 2,
  DONE: 3,
  CANCEL: 4,
}

export const MANAGEMENT_STATUS_SCHEDULE = [
  {
    key: 1,
    label: 'Chưa khởi hành',
    color: 'secondary',
  },
  {
    key: 2,
    label: 'Đã khởi hành',
    color: 'success',
  },
  {
    key: 3,
    label: 'Kết thúc',
    color: 'info',
  },
  {
    key: 4,
    label: 'Đã hủy',
    color: 'warning',
  },
]

export const VEHICLE_TYPE = {
  BUS: 1,
  BIKE: 2,
  CAR: 3,
  VAN: 4,
  BAGAC: 5,
  TOUR: 6,
}

export const VEHICLE_TYPE_ARR = [
  {
    key: 1,
    label: 'Xe khách',
  },
  {
    key: 2,
    label: 'Xe máy',
  },
  {
    key: 3,
    label: 'Ôtô',
  },
  {
    key: 4,
    label: 'Van',
  },
  {
    key: 5,
    label: 'Bagac',
  },
]

export const PAYMENT_STATUS = {
  UNPAID: 1,
  PAID: 2,
}

export const PAYMENT_STATUS_ARR = [
  {
    key: PAYMENT_STATUS.UNPAID,
    label: 'Chưa thanh toán',
    color: 'secondary',
  },
  {
    key: PAYMENT_STATUS.PAID,
    label: 'Đã thanh toán',
    color: 'success',
  },
]

export const PAYMENT_METHOD_ARR = [
  {
    key: 1,
    label: 'Tiền mặt',
  },
]


//Display banner

export const BANNER_STATUS_ARR = [
  {
    key: 1,
    label: 'Hiển thị',
  },
  {
    key: 0,
    label: 'Không hiển thị',
  },
]

export const areaArray = [
  {title: "trung tâm", value: 1},
  {title: "bắc đảo", value: 2},
  {title: "nam đảo", value: 3},
]