import jwtDecode from 'jwt-decode'
import { camelizeKeys } from 'humps'
const AUTH_TOKEN = 'auth'
const LANG = 'language'
const PERMISSIONS = 'permissions'
/**
|--------------------------------------------------
| Authentication Token
|--------------------------------------------------
*/
const setToken = (token, isRememberMe) => {
  const days = 5
  const expireDay = new Date(Date.now() + days * 24 * 60 * 60 * 1000)
  let strToken = AUTH_TOKEN + '=' + token + ';'
  if (isRememberMe) {
    strToken += ' expires=' + expireDay + ';'
  }
  document.cookie = strToken + ' path=/'
}

const getToken = () => {
  let name = AUTH_TOKEN + '='
  let ca = document.cookie.split(';')
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i]
    while (c.charAt(0) === ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length)
    }
  }
  return null
}

const getTokenData = () => {
  let name = AUTH_TOKEN + '='
  let ca = document.cookie.split(';')
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i]
    while (c.charAt(0) === ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) === 0) {
      return jwtDecode(c.substring(name.length, c.length))
    }
  }
  return null
}

const removeToken = () =>
  (document.cookie = `${AUTH_TOKEN}=''; expires=Thu, 18 Dec 2000 12:00:00 UTC; path=/`)

const setLang = lang => localStorage.setItem(LANG, lang)
const getLang = () => localStorage.getItem(LANG) || null
const removeLang = () => localStorage.removeItem(LANG)

const setPermissions = permissions =>
  localStorage.setItem(PERMISSIONS, JSON.stringify(permissions))
const getPermissions = () =>
  localStorage.getItem(PERMISSIONS) ? localStorage.getItem(PERMISSIONS) : []
const removePermissions = () => localStorage.removeItem(PERMISSIONS)

export {
  setToken,
  getToken,
  getTokenData,
  removeToken,
  setLang,
  getLang,
  removeLang,
  setPermissions,
  getPermissions,
  removePermissions,
}
