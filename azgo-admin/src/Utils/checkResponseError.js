export const checkResponseError = res => {
  // check status from api throw error 
  if(!res)  throw new Error('Không kết nối được server')
  if (!res.status) {
    throw new Error(res.message)
  }
}
