import React, { Component } from 'react'
import { Route, Switch, withRouter, Redirect } from 'react-router-dom'
import 'antd/dist/antd.css'
import './App.scss'

import { compose } from 'redux'
import { connect } from 'react-redux'
import 'Stores/User/Reducers'
import { UserActions } from 'Stores/User/Actions'
import PropTypes from 'prop-types'
import { changeLocation } from 'Utils/changeLocation'
import Notification from './components/organisms/Notification'
import { removePermissions, removeToken } from './Utils/token' 
const loading = () => (
  <div className="animated fadeIn pt-3 text-center">Loading...</div>
)

// Containers
const DefaultLayout = React.lazy(() =>
  import('./components/templates/DefaultLayout')
)

// Pages
const Login = React.lazy(() => import('./components/pages/AuthPage/Login'))
const Register = React.lazy(() =>
  import('./components/pages/AuthPage/Register')
)
const ResetPassword = React.lazy(() =>
  import('./components/pages/AuthPage/ResetPassword')
)
const Page404 = React.lazy(() => import('./components/pages/AuthPage/Page404'))
const Page500 = React.lazy(() => import('./components/pages/AuthPage/Page500'))

const App = ({ logout }) => {
  const handleLogout = props => {
    // logout()
    removeToken()
    removePermissions()
    setTimeout(() => {
      changeLocation('/login')
    }, 0)
  } 
  return (
    <React.Suspense fallback={loading()}>
      <Notification />
      <Switch>
        <Route
          exact
          path="/login"
          name="Login Page"
          render={props => <Login {...props} />}
        />
        <Route
          exact
          path="/register"
          name="Register Page"
          render={props => <Register {...props} />}
        />
        <Route
          exact
          path="/reset-password"
          name="Reset Password"
          render={props => <ResetPassword {...props} />}
        />
        <Route
          exact
          path="/500"
          name="Page 500"
          render={props => <Page500 {...props} />}
        />
        <Route
          exact
          path="/logout"
          name="Logout"
          render={props => handleLogout(props)}
        />
        <Route
          path="/"
          name="Home"
          render={props => <DefaultLayout logout={logout} {...props} />}
        />
        <Route path="*">
          <Redirect to="/login" />
        </Route>
      </Switch>
    </React.Suspense>
  )
}

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(UserActions.doLogout()),
})
const withConnect = connect(mapStateToProps, mapDispatchToProps)
App.propTypes = {
  logout: PropTypes.func,
  history: PropTypes.object,
}

export default compose(withConnect, withRouter)(App)
