#!/bin/bash

ENV_NAME=production
ENV_VERSION=prod-0.6.4
#DRY_RUN=echo
DRY_RUN=

function display_message {
  echo ""
  echo "<<==================================================================================>>"
  echo "$1"
  echo "<<==================================================================================>>"
}


read -p "Press [Enter] key to start the ${ENV_VERSION} release... or Ctrl+C to stop release"
${DRY_RUN} sudo rm -r node_modules
${DRY_RUN} sudo rm -r dist
${DRY_RUN} docker rmi -f $(docker image ls) 
${DRY_RUN} docker rmi -f azgo/azgo-admin:${ENV_VERSION}
${DRY_RUN} docker rmi  -f 505145921145.dkr.ecr.ap-southeast-1.amazonaws.com/azgo-admin:${ENV_VERSION}
${DRY_RUN} npm install
${DRY_RUN} npm run build:prod
${DRY_RUN} aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 505145921145.dkr.ecr.ap-southeast-1.amazonaws.com 
display_message "Building the deploy image... " && \
${DRY_RUN} docker build -t azgo/azgo-admin:${ENV_VERSION} . && \
display_message "Adding new ${ENV_VERSION}  tag to image..." &&
${DRY_RUN} docker tag azgo/azgo-admin:${ENV_VERSION}  505145921145.dkr.ecr.ap-southeast-1.amazonaws.com/azgo-admin:${ENV_VERSION}  && \
display_message "Pushing the new ${ENV_VERSION}  images..." &&
${DRY_RUN} docker push 505145921145.dkr.ecr.ap-southeast-1.amazonaws.com/azgo-admin:${ENV_VERSION} && \
display_message "Successfully build and pushed ${ENV_VERSION}  images"


