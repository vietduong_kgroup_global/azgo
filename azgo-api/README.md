## Description test 1

APIs for AZGO mobile platform

## Installation

```bash
$ npm install
```

## Migrate

```bash
$ npm run migrate
```

## Start

```bash
$ npm run start
```

## create migration 
typeorm migration:create -n PostRefactoring

## Access ECR
aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 505145921145.dkr.ecr.ap-southeast-1.amazonaws.com 

```create tag in repository
docker tag azgo/azgo-api:0.0.1  505145921145.dkr.ecr.ap-southeast-1.amazonaws.com/azgo-api:0.0.1

```push image  in repository
docker push 505145921145.dkr.ecr.ap-southeast-1.amazonaws.com/azgo-api:0.0.1


Send notification demo
curl --location --request POST 'http://54.255.252.122:8100/api/v1/firebase/push-notification' \
--header 'Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6bnVsbCwicGhvbmUiOiI5MDgxODQ5ODEiLCJ1c2VyX2lkIjoiNjRhMGIxZGYtMGY2Mi00OGI1LWI5MTktOTRjZDI5MjEyYzIwIiwiaWQiOjMsInJvbGUiOjEsInBsYXRmb3JtIjoxLCJ0eXBlIjo1LCJjb21wYW55IjoiIiwiYXpnb3JvbGVzIjpbNSw3LDNdLCJjdXJyZW50X3JvbGUiOiJkcml2ZXIiLCJpYXQiOjE2MTQ2Nzc5ODQsImV4cCI6MTYxNDg1MDc4NH0.vuBE25H1fKKefnFCuV3llR76EXdsgxo6EKguLo4NDPw' \
--header 'Content-Type: application/json' \
--data-raw '{
    "device_tokens": ["d8PHmxeGQGKBRkT4jXDAxU:APA91bE34Y3bGsE5TPt5nWBK7CeEoKACrDjMa2XPYuMSFq_uNyhWYOKybTVy6WX5LpUgE_gp0LJVP1FoCaKDWossGgHw4ESsaemhekUc7PSPUEia2cgyOgWNOXzNd4kmM2Vw2_XjumB_"],
    "message": {
        "title": "TEST",
        "body": "Push Notification"
    },
    "data": {
        "key": "KEY_DRIVER_APPROVE_REQUEST_BOOK_SHIPPING_BY_CUSTOMER",
        "click_action": "KEY_CLICK_ACTION"
    }
}'