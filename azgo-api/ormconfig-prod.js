

module.exports ={
  "type": "mysql",
  "port": 3306,
  "database": "azgo_prod",
  "entities": ["dist/src/module/**/**.entity{.ts,.js}"],
   "migrations": [ 
      "dist/src/migrations/*{.ts,.js}"
    ],
  "cli": {
    "migrationsDir": "src/migrations"
  },
  "synchronize": false,
  "logging":true
}