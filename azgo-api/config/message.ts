export default {
    bikeCarCustomerToDriver: {
        requestBikeCar: {
            title: 'Tôi cần đặt xe',
            body: 'Khách hàng cần đặt xe',
        },
    },
    bikeCarDriverToCustomer: {
        acceptRequestBikeCar: {
            title: 'Có 1 tài xế nhận yêu cầu của bạn',
            body: 'Có 1 tài xế nhận yêu cầu của bạn',
        },
    },
    vanBagacCustomerToDriver: {
        requestVanBagac: {
            title: 'Tôi cần đặt xe',
            body: 'Khách hàng cần đặt xe',
        },
    },
    vanBagacDriverToCustomer: {
        acceptRequestVanBagac: {
            title: 'Có 1 tài xế nhận yêu cầu của bạn',
            body: 'Có 1 tài xế nhận yêu cầu của bạn',
        },
    },

};
