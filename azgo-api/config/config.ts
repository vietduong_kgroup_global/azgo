require('dotenv').config();
export default {
    env: {
        type: process.env.ENV_IDENTITY_TYPE || 'mysql',
        host: process.env.ENV_IDENTITY_HOST || 'localhost',
        url: process.env.ENV_IDENTITY_HOST_PAGE + ":"+ process.env.ENV_IDENTITY_PORT_WEB,
        port: Number(process.env.ENV_IDENTITY_PORT).valueOf() || 3306,
        username: process.env.ENV_IDENTITY_USERNAME || 'root',
        password: process.env.ENV_IDENTITY_PASSWORD || '',
        database: process.env.ENV_IDENTITY_DATABASE || 'azgo',
        portWeb: Number(process.env.ENV_IDENTITY_PORT_WEB) || 8100,
        fireBaseServerKey: process.env.FIRE_SERVER_KEY || 'AAAAJu6Tkek:APA91bG9NolPo3TRyGH4loWMkxFzGqC_9d-8pcmUb5cqnuNfsYe4-9H0T1fVq6kE-dB3e5HOtRmia8LyoZat7Z5x42BY0jBgyzf93R_tPK1k5ecLeKXjJ-bNGnnlpdk_t67q9y4mAgS2',
    },
    awsSetting: {
        accessKey: process.env.AWS_ACCESS_KEY || 'AKIAJ7YAOJSKCQE33XCQ',
        regionKey: process.env.AWS_REGION_KEY || 'ap-southeast-1',
        secretKey: process.env.AWS_SECRET_KEY || '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',
        s3Setting: {
            public_bucket: `${process.env.AWS_BUCKET_NAME}/${process.env.AWS_PUBLIC_ASSETS}` || 'dev-azgo-asia/public/assets',
            private_bucket: `${process.env.AWS_BUCKET_NAME}/${process.env.AWS_PUBLIC_ASSETS}`  || 'dev-azgo-asia/private',
            key: process.env.S3_KEY_FOLDER || 'public',
            ACL: process.env.S3_ACL || 'public-read',
        },
    },
    uploadConfig: {
        RESIZE_IMAGE_WIDTH: Number(process.env.RESIZE_IMAGE_WIDTH).valueOf() || 400,
        RESIZE_IMAGE_HEIGHT: Number(process.env.RESIZE_IMAGE_HEIGHT).valueOf() || 400,
    },
    jwtSecret: process.env.JWT_SECRET || 'azgoSecretKey', // jwtSecret
    jwtExpiresIn: process.env.JWT_EXPIRES_IN || '48h', // 2 day
    urlRestPassword: process.env.URL_RESET_PASSWORD || 'http://localhost:3000/#/new-password/', // URL reset password
    apiPrefix: process.env.API_PREFIX || 'api/v1',
    apiCheckTokenFacebook: process.env.API_CHECK_TOKEN_FACEBOOK || 'https://graph.facebook.com/me?fields=name,first_name,last_name&access_token=',
    apiCheckTokenGoogle: process.env.API_CHECK_TOKEN_GOOLE || 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=',
    countryCode: process.env.COUNTRY_CODE || 84,
    bikeCarStatusBus: {
        NEW: 'NEW',
        ACCEPT: 'ACCEPT',
        MOVING: 'MOVING',
        DONE: 'DONE',
    },
    vanBagacStatus: {
        NEW: 'NEW',
        ACCEPT: 'ACCEPT',
        MOVING: 'MOVING',
        DONE: 'DONE',
    },
    paymentStatus: {
        NOT_PAYMENT: 0,
        PAYMENT: 1,
    },
    paymentMethod: {
        MONEY: 1,
    },
    enableSMS: process.env.ENV_IDENTITY_ENABLESMS || 'true',
    version: '0.0.4',
};

