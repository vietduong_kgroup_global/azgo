module.exports = {
    apps: [{
        name: "app-gateway",
        script: "ts-node ./src/main",
        watch: true,
        error_file: './logs/error.log',
        out_file: './logs/out.log',
        ignore_watch: ['logs', 'node_modules'],
        log_date_format: "YYYY-MM-DD HH:mm"
    }]
}