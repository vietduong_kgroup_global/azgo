import {ApiModelProperty} from '@nestjs/swagger';
import {IsNotEmpty, IsNumberString} from 'class-validator';

export class CoordinatesDto {
    @ApiModelProperty({example: '10.814243', type: String, required: true})
    @IsNumberString()
    @IsNotEmpty()
    lat: string;

    @ApiModelProperty({example: '106.6693821', type: String, required: true})
    @IsNumberString()
    @IsNotEmpty()
    lng: string;

    @ApiModelProperty({example: 'p5 q8 tphcm', type: String, required: false})
    address?: string;

    @ApiModelProperty({example: 'Tòa nhà VinCom', type: String, required: false})
    short_address?: string;
}
