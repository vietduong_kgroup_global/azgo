import {IsNotEmpty} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';

export class ImageDto {

    @ApiModelProperty({example: 'assets/images', type: String , required: false  })
    file_path: string;

    @ApiModelProperty({example: '1583745101578.jpg', type: String , required: false  })
    file_name: string;

    @ApiModelProperty({example: 'http://localhost:8200/assets/images/1583745101578.jpg', type: String , required: false  })
    address?: string;

}
