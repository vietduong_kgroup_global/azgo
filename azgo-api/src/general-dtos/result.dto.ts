import {ApiModelProperty} from '@nestjs/swagger';
import {IsNotEmpty, IsNumberString} from 'class-validator';

export class ResultDto<T> {

    status: boolean;

    error: string;

    message: string;

    data: T;
}
