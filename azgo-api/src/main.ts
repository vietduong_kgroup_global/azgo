import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import config from './../config/config';
import {ValidationPipe} from '@nestjs/common';
import {useContainer} from 'class-validator';
import {join} from 'path';
import {NestExpressApplication} from '@nestjs/platform-express';
const bodyParser = require('body-parser');
import * as express from 'express';
import { AllExceptionFilter } from './global.exception';

async function bootstrap() {
    console.log(config.env.url)
    console.log("API is listen on:"+config.env.portWeb);
    console.log("API DB sever:"+config.env.host);
    console.log("API DB:"+config.env.database);
    console.log("API AWS S3:"+config.awsSetting.s3Setting.public_bucket);
    console.log("API Redis:"+process.env.ENV_REDIS_HOST);
    let d = new Date()
    console.log("API TimeZone:"+process.env.TZ);
    console.log("API CurrentTime:"+d.toLocaleTimeString());

    const app = await NestFactory.create<NestExpressApplication>(AppModule ,{
        logger: ['error', 'warn','log','debug','verbose'],
      });
    app.useGlobalFilters(new AllExceptionFilter());     
    app.setGlobalPrefix(config.apiPrefix);
    app.enableCors();

    const options = new DocumentBuilder()
        .setTitle('API Document')
        .setDescription('API description')
        .setVersion('1.0')
        .setBasePath(config.apiPrefix)
        .addTag('cats')
        .addBearerAuth()
        .build();

    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('document', app, document);

    /* Accept files static*/
    const environmentHosting: string = process.env.NODE_ENV || 'development';
    if (environmentHosting === 'local') {
        app.useStaticAssets(join(__dirname, '..', "public"));
        // app.useStaticAssets(join(__dirname, '..', process.env.USE_STATIC_ASSETS));
    } else {
        app.useStaticAssets(join(__dirname, '..', '..',  "public"));
        // app.useStaticAssets(join(__dirname, '..', '..', process.env.USE_STATIC_ASSETS));
    }

    // Use Validation Pipe
    /* Comment b/c use custom validatorPipe*/
    // app.useGlobalPipes(new ValidationPipe());

    /* Use Container for custom validator*/
    useContainer(app.select(AppModule), { fallbackOnErrors: true });

    /* Use bodyParser */
    app.use(bodyParser.urlencoded({
        extended: true,
    }));
    app.use(express.static('public'));
    app.use(bodyParser.json({ limit: '5mb' }));
    app.use(bodyParser.urlencoded({ limit: '15mb', extended: true }));
    await app.listen(config.env.portWeb);
}
bootstrap();
