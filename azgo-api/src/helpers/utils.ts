

/**
 * roundKmFromDistance
 * @param distance is meter
 */

import * as moment from 'moment';
export function roundKmFromDistance(distance:number) {
    distance=distance/1000;
    distance=Math.round(distance*10)/10;
    return distance;
}

export function roundPrice(price:number,currency:String) {
    if(currency=="vnd"){
        price=Math.round(price/100)*100;
    }
    return price;
}

export function roundUpPriceToThousand(price:number,currency:String) {
    if(currency=="vnd"){
        price=Math.ceil(price/1000)*1000;
    }
    return price;
}

export function roundPriceToUnit(price:number,currency:String) {
    if(currency=="vnd"){
        price = Math.round(price);
    }
    return price;
}

export function formatMoney(number: number){
    return new Intl.NumberFormat('en-IN').format(number)
}

export const mergeArrays = (arr1 = [], arr2 = [], key) => {
    let res = [];
    res = arr1.map(obj => {
       const index = arr2.findIndex(el => el[key] == obj[key]);
       const address = index !== -1 ? arr2[index] : {};
       if (index === -1) arr1.push(arr2[index]) 
       return {
          ...obj,
          ...address,
       };
    });
    return res;
 };

export const formatDateTime = (date) => { 
    return moment(new Date(date)).format('DD/MM/YYYY HH:mm:ss'); 
} 
export const changeFormatDate =  (stringDate) => { // input 2021-03-11T02:33:37.000Z  => output: 11/03/2021
    try {
        let dateObj = new Date(stringDate) ;
        return `${dateObj.getDate()}/${dateObj.getMonth() + 1}/${dateObj.getFullYear()}`;
    }
    catch (error) {
        console.log(error);
        return '';
    }  
};
export const formatDateByTimeZone = (date, timeZone) => { 
    try {
       return  formatDateTime(new Date(new Date(date).getTime() - timeZone * 60 * 1000));
    }
    catch (error) { 
        return '';
    }   
};
export const calcFeesOrder = (price, setting, azgo_fee) => {
    let vat = setting.vat;
    let tax = setting.tax;
    let vat_fee = roundPriceToUnit((price / (1 + vat)) * vat, "vnd");
    let azgo_fee_order = roundPriceToUnit((price - vat_fee) * azgo_fee, "vnd");
    let tax_fee = roundPriceToUnit((price - vat_fee - azgo_fee_order) * tax, "vnd");
    return { vat_fee, tax_fee, azgo_fee_order }
}