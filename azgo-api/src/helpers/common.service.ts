import { RedisService } from '@azgo-module/core/dist/background-utils/Redis/redis.service';
import config from '@azgo-module/core/dist/config';
import { RedisUtil } from '@azgo-module/core/dist/utils/redis.util';
import * as _ from 'lodash';
import jwt_decode from 'jwt-decode';

export class CommonService {
    private redisService;
    private redis;
    // use for vehicle bus
    private keyRedisVehiclePositionBus = 'vehicle_positions_bus';
    private fieldRedisVehiclePositionBus = 'field_vehicle_positions_bus';
    // use for vehicle bike
    private keyRedisVehiclePositionBike = 'vehicle_positions_bike';
    private fieldRedisVehiclePositionBike = 'field_vehicle_positions_bike';
    // use for vehicle car
    private keyRedisVehiclePositionCar = 'vehicle_positions_car';
    private fieldRedisVehiclePositionCar = 'field_vehicle_positions_car';
    // use for vehicle van
    private keyRedisVehiclePositionVan = 'vehicle_positions_van';
    private fieldRedisVehiclePositionVan = 'field_vehicle_positions_van';
    // use for vehicle bagac
    private keyRedisVehiclePositionBagac = 'vehicle_positions_bagac';
    private fieldRedisVehiclePositionBagac = 'field_vehicle_positions_bagac';
    private  connect:any;
    constructor() {
        this.connect= {
            port: process.env.ENV_REDIS_PORT || 6379,
            host: process.env.ENV_REDIS_HOST || '127.0.0.1', 
            password: process.env.ENV_REDIS_PASSWORD || null,
        };
        console.log(" Redis constructor:",this.connect);
        this.redisService = RedisService.getInstance(this.connect);
        this.redis = RedisUtil.getInstance();
    }
    calculateDistance(lat1, lon1, lat2, lon2, unit) {

        let r = 0;
        if ((lat1 === lat2) && (lon1 === lon2)) {
            r = 0;
        }
        else {
            const radlat1 = Math.PI * lat1 / 180;
            const radlat2 = Math.PI * lat2 / 180;
            const theta = lon1 - lon2;
            const radtheta = Math.PI * theta / 180;
            let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            if (unit === 'K') {
                dist = dist * 1.609344;
            }
            // if (unit === 'N') {
            //     dist = dist * 0.8684
            // }
            r = dist;
        }
        return r;
    }
    async calculateLongTime(vehicleId, lat, long, type_group = 'BUS') {
        let longTime = 0;
        const redisKey = await this.keyRedisVehiclePosition(type_group);
        const redisField = await this.fieldRedisVehiclePosition(type_group);
        let getCache = await this.redis.getInfoRedis(redisKey, redisField);
        if (getCache.isValidate) {
            let vehiclePositions = JSON.parse(getCache.value);
            const checkVehicle = _.find(vehiclePositions, { vehicle_id: vehicleId });
            if (checkVehicle) {
                const distance =
                    this.calculateDistance(parseFloat(lat), parseFloat(long), parseFloat(checkVehicle.lat), parseFloat(checkVehicle.long), 'K');
                if (distance > 0) {
                    // longTime =  ( distance / 40 ) * 60 ; // minute
                    longTime = (distance / 40); // hour
                }
            }
        }
        return longTime;
    }

    async calculateLongTimeVanBagac(data) {
        let longTime = 0;
        const distance =
            this.calculateDistance(
                parseFloat(data.customer_position.lat), parseFloat(data.customer_position.long),
                parseFloat(data.driver_position.lat), parseFloat(data.driver_position.long), 'K');
        if (distance > 0) {
            longTime = (distance / 40) * 60; // minute

        }
        return longTime;
    }
    fieldRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.fieldRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.fieldRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.fieldRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.fieldRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.fieldRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    keyRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.keyRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.keyRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.keyRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.keyRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.keyRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    vnp_ResponseCode(vnp_ResponseCode: string) {
        let response_data: { status: boolean, message: string } = { status: false, message: 'Giao dịch không thành công' };
        switch (vnp_ResponseCode) {
            case '00':
                response_data = { status: true, message: 'Giao dịch thành công' };
                break;
            case '01':
                response_data = { status: false, message: 'Giao dịch đã tồn tại' };
                break;
            case '02':
                response_data = { status: false, message: 'Merchant không hợp lệ (kiểm tra lại vnp_TmnCode)' };
                break;
            case '03':
                response_data = { status: false, message: 'Dữ liệu gửi sang không đúng định dạng' };
                break;
            case '04':
                response_data = { status: false, message: 'Khởi tạo GD không thành công do Website đang bị tạm khóa' };
                break;
            case '08':
                response_data = {
                    status: false,
                    message: 'Giao dịch không thành công do: Hệ thống Ngân hàng đang bảo trì. ' +
                        'Xin quý khách tạm thời không thực hiện giao dịch bằng thẻ/tài khoản của Ngân hàng này.',
                };
                break;
            case '05':
                response_data = {
                    status: false, message: 'Giao dịch không thành công do: Quý khách nhập sai mật khẩu thanh toán\n' +
                        'quá số lần quy định. Xin quý khách vui lòng thực hiện lại giao dịch'
                };
                break;
            case '06':
                response_data = {
                    status: false, message: 'Giao dịch không thành công do Quý khách nhập sai mật khẩu xác thực giao\n' +
                        'dịch (OTP). Xin quý khách vui lòng thực hiện lại giao dịch.'
                };
                break;
            case '07':
                response_data = {
                    status: false, message: 'Trừ tiền thành công. Giao dịch bị nghi ngờ (liên quan tới lừa đảo, giao dịch bất thường). Đối với giao dịch này cần merchant xác nhận thông qua\n' +
                        'merchant admin: Từ chối/Đồng ý giao dịch'
                };
                break;
            case '12':
                response_data = { status: false, message: 'Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa.' };
                break;
            case '09':
                response_data = {
                    status: false, message: 'Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng\n' +
                        'ký dịch vụ InternetBanking tại ngân hàng.'
                };
                break;
            case '10':
                response_data = {
                    status: false, message: 'Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài\n' +
                        'khoản không đúng quá 3 lần'
                };
                break;
            case '11':
                response_data = {
                    status: false, message: 'Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách\n' +
                        'vui lòng thực hiện lại giao dịch.'
                };
                break;
            case '24':
                response_data = { status: false, message: 'Giao dịch không thành công do: Khách hàng hủy giao dịch' };
                break;
            case '51':
                response_data = {
                    status: false, message: 'Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư\n' +
                        'để thực hiện giao dịch.'
                };
                break;
            case '65':
                response_data = {
                    status: false, message: 'Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn\n' +
                        'mức giao dịch trong ngày.'
                };
                break;
            case '75':
                response_data = { status: false, message: 'Ngân hàng thanh toán đang bảo trì' };
                break;
            case '99':
                response_data = { status: false, message: 'Các lỗi khác (lỗi còn lại, không có trong danh sách mã lỗi đã liệt kê)' };
                break;
        }
        return response_data;
    }
}
