import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from '@azgo-module/core/dist/common/guards/roles.guards';
import { UsersModule } from './module/users/users.module';
import { UserProfileModule } from './module/user-profile/user-profile.module';
import { AuthModule } from './module/auth/auth.module';
import { AuthenticationMiddleware } from '@azgo-module/core/dist/common/middleware/authentication.middleware';
import config from '../config/config';
import { FunctionSettingsModule } from './module/function-settings/function-settings.module';
import { UserRolesModule } from './module/user-roles/user-roles.module';
import { ResetPasswordModule } from './module/reset-password/reset-password.module';
import { UploadMiddleware } from '@azgo-module/core/dist/common/middleware/upload.middleware';
import { UploadController } from './module/upload/upload.controller';
import { TestController } from './module/test/test.controller';
import { RoutesModule } from './module/routes/routes.module';
import { VehicleModule } from './module/vehicle/vehicle.module';
import { ProvidersInfoModule } from './module/providers-info/providers-info.module';
import { VehicleTypesModule } from './module/vehicle-types/vehicle-types.module';
import { VehicleGroupsModule } from './module/vehicle-groups/vehicle-groups.module';
import { PromotionsModule } from './module/promotions/promotions.module';
import { ValidatorModule } from './module/validator-module/validator.module';
import { VehicleMovingModule } from './module/vehicle-moving/vehicleMoving.module';
import { VehicleScheduleModule } from './module/vehicle-schedule/vehicleSchedule.module';
import { SeatPatternModule } from './module/seat-pattern/seat-pattern.module';
import { UploadFileModule } from './module/upload-file/upload-file.module';
import { UploadAWSModule } from './module/upload-aws/upload-aws.module';
import { AdminProfileModule } from './module/admin-profile/admin-profile.module';
import { DriverProfileModule } from './module/driver-profile/driver-profile.module';
import { CustomerProfileModule } from './module/customer-profile/customer-profile.module';
import { TicketModule } from './module/ticket/ticket.module';
import { AreaModule } from './module/area/area.module';
import { RatingDriverModule } from './module/rating-driver/rating-driver.module';
import { FirebaseModule } from './module/firebase/firebase.module';
import { BookTicketModule } from './module/book-ticket/book-ticket.module';
import { BikeCarModule } from './module/bike-car-customer/bikeCar.module';
import { BikeCarDriverModule } from './module/bike-car-driver/bikeCarDriver.module';
import { BikeCarOrderModule } from './module/bike-car-order/bikeCarOrder.module';
import { BusOrderModule } from './module/bus-order/bus-order.module';
import { VehicleBrandModule } from './module/vehicle-brand/vehicle-brand.module';
import { VehicleDriverModule } from './module/vehicle-driver/vehicle-driver.module';
import { BookShippingModule } from './module/book-shipping/book-shipping.module';
import { VanBagacOrdersModule } from './module/van-bagac-orders/van-bagac-orders.module';
import { ServiceOthersModule } from './module/service-others/service-others.module';
import { DriverLogsModule } from './module/driver-logs/driver-logs.module';
import { RatingScheduleModule } from './module/rating-schedule/rating-schedule.module';
import { HotlinePolicyModule } from './module/hotline-policy/hotline-policy.module';
import { DashboardModule } from './module/dashboard/dashboard.module';
import { RatingCustomerModule } from './module/rating-customer/rating-customer.module';
import { CustomChargeModule } from './module/custom-charge/custom-charge.module';
import { VnPayModule } from './module/vn-pay/vn-pay.module';
import { GoogleMapModule } from './module/google-map/google-map.module';
import { BannerManagementModule } from './module/banner-management/banner-management.module';
import { ScheduleModule } from 'nest-schedule';
import { CronService } from './cron/cron.service';
import { CronTourOrderService } from './cron/cron-tour-order.service';
import { WalletModule } from './module/wallet/wallet.module';
import { WalletHistoryModule } from './module/wallet-history/wallet-history.module';
import { WithdrawMoneyModule } from './module/withdraw-money/withdraw-money.module';
import { TourModule } from './module/tour/tour.module';
import { TourOrderModule } from './module/tour-order/tour-order.module';
import { TourOrderLogModule } from './module/tour-order-log/tour-order-log.module';
import { NotificationModule } from './module/notification/notification.module';
import { SettingSystemModule } from './module/setting-system/setting-system.module';
import { DriverWorkingDateModule } from './module/driver-working-date/driver-working-date.module';
import { SlackAlertModule } from './module/slack-alert/slack-alert.module';
import { AdditionalFeeModule } from './module/additional-fee/additional-fee.module';
import { UserAccountModule } from './module/user-account/user-account.module';
import { AccountsModule } from './module/accounts/accounts.module';
import { BankModule } from './module/bank/bank.module'; 
import { UserAccountLogModule } from './module/user-account-log/user-account-log.module';
import { APP_FILTER } from '@nestjs/core';
import { AllExceptionFilter } from './global.exception';
import { RequestWithdrawModule } from './module/request-withdraw/request-withdraw.module';
import { RequestWithdrawHistoryModule } from './module/request-withdraw-history/request-withdraw-history.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql' as 'mysql',
            host: config.env.host,
            port: config.env.port,
            username: config.env.username,
            password: config.env.password,
            database: config.env.database,
            entities: ['dist/src/module/**/*.entity{.ts,.js}'],
            subscribers: [],
            synchronize: false,
            logging: false,
            timezone: '+00:00'

        }),
        ServeStaticModule.forRoot({
            rootPath: join(__dirname, '..', 'public'),
        }),
        // import module
        AuthModule,
        UsersModule,
        UserProfileModule,
        UserRolesModule,
        FunctionSettingsModule,
        ResetPasswordModule,
        VehicleModule,
        ProvidersInfoModule,
        VehicleTypesModule,
        RoutesModule,
        VehicleGroupsModule,
        PromotionsModule,
        ValidatorModule,
        VehicleMovingModule,
        VehicleScheduleModule,
        SeatPatternModule,
        UploadFileModule,
        UploadAWSModule,
        AdminProfileModule,
        DriverProfileModule,
        CustomerProfileModule,
        TicketModule,
        AreaModule,
        RatingDriverModule,
        BookTicketModule,
        FirebaseModule,
        BikeCarModule,
        BikeCarDriverModule,
        BikeCarOrderModule,
        BusOrderModule,
        VehicleBrandModule,
        VehicleDriverModule,
        BookShippingModule,
        VanBagacOrdersModule,
        ServiceOthersModule,
        DriverLogsModule,
        RatingScheduleModule,
        HotlinePolicyModule,
        DashboardModule,
        RatingCustomerModule,
        CustomChargeModule,
        VnPayModule,
        GoogleMapModule,
        BannerManagementModule,
        ScheduleModule.register(),
        WalletModule,
        WalletHistoryModule,
        WithdrawMoneyModule,
        TourModule,
        TourOrderModule,
        TourOrderLogModule,
        NotificationModule,
        SettingSystemModule,
        DriverWorkingDateModule,
        SlackAlertModule,
        AdditionalFeeModule,
        UserAccountModule,
        AccountsModule,
        BankModule,
        UserAccountLogModule,
        RequestWithdrawModule,
        RequestWithdrawHistoryModule,
    ],
    controllers: [AppController, UploadController, TestController],
    providers: [{
        provide: APP_GUARD,
        useClass: RolesGuard,
    },{
        provide: APP_FILTER,
        useClass: AllExceptionFilter,
    }, CronService, CronTourOrderService],
})

export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(AuthenticationMiddleware)
            .forRoutes(
                { path: '/api/auth/user', method: RequestMethod.ALL },
            );
        consumer.apply(UploadMiddleware)
            .forRoutes(
                { path: '/api/upload', method: RequestMethod.POST },
            );
    }
}
