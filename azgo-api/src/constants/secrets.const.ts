export const SECRET_BOOK_BUS = 'bookbus';
export const SECRET_BOOK_TICKET = 'bookticket';
export const SECRET_BOOK_TICKET_BY_ADMIN = 'bookticketbyadmin';
export const SECRET_BOOK_SHIPPING = 'bookshipping';
export const SECRET_BOOK_BIKE_CAR = 'bookbikecar';
export const SECRET_BOOK_VAN_BAGAC = 'bookvanbagac';
export const STATUS_TOUR_ARR = {
    VERIFYING: 1,
    ACCEPTED_DRIVER: 2,
    ACCEPTED_CUSTOMER: 3,
    CANCELED_DRIVER: 4,
    CANCELED_CUSTOMER: 5,
    DONE: 6,
    ARRIVED_LOCATION_PICKED_UP_CUSTOMER: 7,
}
export const STATUS_TOUR_LABEL_ARRAY = [
    {value: STATUS_TOUR_ARR.VERIFYING, label: "Chờ xác nhận"},
    {value: STATUS_TOUR_ARR.ACCEPTED_DRIVER, label: "Tài xế  xác nhận"},
    {value: STATUS_TOUR_ARR.ACCEPTED_CUSTOMER, label: "Khách hàng xác nhận"},
    {value: STATUS_TOUR_ARR.CANCELED_DRIVER, label: "Tài xế huỷ"},
    {value: STATUS_TOUR_ARR.CANCELED_CUSTOMER, label: "Khách hàng huỷ"},
    {value: STATUS_TOUR_ARR.DONE, label: "Hoàn thành"},
    {value: STATUS_TOUR_ARR.ARRIVED_LOCATION_PICKED_UP_CUSTOMER, label: "Tài xế đã đến điểm đón"}
] 
export const STATUS_REQUEST_WITHDRAW_ARR = {
    VERIFYING: 1,
    ACCEPTED_ADMIN: 2,
    CANCELED_DRIVER: 3,
    DECLINE_ADMIN: 4,
    TRANSFERRED_ADMIN: 5,
}
export const TAG_HISTORY_WALLET_ARR = {
    VAT: 'vat-fee',
    TAX: 'tax-fee',
    AZGO_FEE: 'azgo-fee',
    RETURN_AZGO_FEE: 'return-azgo-fee',
    ORDER_CASH: 'order-cash',
    WITHDRAW_CASH_WALLET_TO_BANK: 'withdraw-cash-wallet',
    RETURN_MONEY_WITHDRAW_TO_CASH_WALLET: 'return-money-to-cash-wallet',
}
export const PAYMEN_METHOD_WALLET_HISTORY_ARR = {
    VNPAY: 1,
    CASH_WALLET: 2,
    FEE_WALLET: 3,
    OTHER: 4
}
export const TYPE_WALLET_HISTORY_ARR = {
    FEE_WALLET: 1,
    CASH_WALLET: 2,
}