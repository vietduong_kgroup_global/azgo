/*
* Use for cron job
* */
export const KEY_CANCEL_FIND_BUS = 'CANCEL_FIND_BUS';
export const KEY_CANCEL_APPROVED_BUS = 'CANCEL_APPROVED_BUS';
export const KEY_DISABLE_BTN_CANCEL_APPROVED_VANBAGAC = 'KEY_DISABLE_BTN_CANCEL_APPROVED_VANBAGAC';
export const KEY_BOOK_TICKET = 'KEY_BOOK_TICKET';

/*
* Use for dataNotification
* */
export const KEY_CLICK_ACTION = 'FLUTTER_NOTIFICATION_CLICK';
export const KEY_DRIVER_UPDATE_POSITION = 'driver_update_position';
export const KEY_CANCEL_APPROVE_REQUEST_BOOK_BUS_BY_CUSTOMER = 'cancel_approve_request_book_bus_by_customer';
export const KEY_CUSTOMER_SEND_REQUEST_BOOK_SHIPPING_TO_DRIVER = 'customer_send_request_book_shipping_to_driver';
export const KEY_CANCEL_FIND_BOOK_SHIPPING_BY_CUSTOMER = 'cancel_find_book_shipping_by_customer';
export const REQUEST_BOOK_BUS_FROM_CUSTOMER = 'request_book_bus_from_customer';
export const KEY_NOTIFY_PAID_BOOK_BUS_TO_CUSTOMER = 'notify_paid_book_bus_to_customer';
export const KEY_DRIVER_APPROVE_REQUEST_BOOK_SHIPPING_BY_CUSTOMER = 'driver_approve_request_book_shipping_by_customer';
export const KEY_TIMEOUT_PAYMENT = 'timeout_payment';
export const KEY_CUSTOMER_SEND_REQUEST_BOOK_BIKE_CAR_TO_DRIVER = 'customer_send_request_book_bike_car_to_driver';
export const KEY_CUSTOMER_CANCEL_APPROVE_BOOK_BIKE_CAR_TO_DRIVER = 'customer_cancel_approve_book_bike_car_to_driver';
export const KEY_CANCEL_FIND_BOOK_BIKE_CAR_BY_CUSTOMER = 'cancel_find_book_bike_car_by_customer';
export const KEY_ALREADY_HAVE_A_DRIVER_ACCEPT = 'already_have_a_driver_accept';
export const KEY_DRIVER_ACCEPT_REQUEST_BOOK_BIKE_CAR_FROM_CUSTOMER = 'driver_accept_request_book_bike_car_from_customer';
export const KEY_DRIVER_UPDATE_STATUS_THE_DRIVER_HAS_ARRIVED = 'driver_update_status_the_driver_has_arrived';
export const KEY_DRIVER_UPDATE_STATUS_CUSTOMER_ON_BIKE = 'driver_update_status_customer_on_bike';
export const KEY_CANCEL_APPROVE_REQUEST_BOOK_SHIPPING_BY_CUSTOMER = 'cancel_approve_request_book_shipping_by_customer';
export const KEY_DRIVER_ACCEPT_REQUEST_BOOK_BUS_FROM_CUSTOMER = 'driver_accept_request_book_bus_from_customer';
export const KEY_DRIVER_REJECT_REQUEST_BOOK_BUS_FROM_CUSTOMER = 'driver_reject_request_book_bus_from_customer';
export const KEY_CANCEL_FIND_BOOK_BUS_BY_CUSTOMER = 'cancel_find_book_bus_by_customer';
export const KEY_CUSTOMER_SCAN_TICKET = 'customer_scan_ticket';
export const KEY_CUSTOMER_SEND_REQUEST_BOOK_VAN_BAGAC_TO_DRIVER = 'customer_send_request_book_van_bagac_to_driver';
export const KEY_DRIVER_ACCEPT_REQUEST_BOOK_VAN_BAGAC_FROM_CUSTOMER = 'driver_accept_request_book_van_bagac_from_customer';
export const KEY_CUSTOMER_CANCEL_APPROVE_BOOK_VAN_BAGAC_TO_DRIVER = 'customer_cancel_approve_book_van_bagac_to_driver';
export const KEY_CANCEL_FIND_BOOK_VAN_BA_GAC_BY_CUSTOMER = 'cancel_find_book_van_ba_gac_by_customer';
export const KEY_DRIVER_SUBMIT_DONE_SCHEDULE = 'driver_submit_done_schedule';
export const KEY_TRIGGER_LIST_REQUEST_VAN = 'trigger_list_request_van';
export const KEY_TRIGGER_LIST_REQUEST_BAGAC = 'trigger_list_request_bagac';
export const KEY_TRIGGER_SEAT_MAP = 'trigger_seat_map';
export const DRIVER_VAN_BAGAC_UPDATE_STATUS_MOVING = 'driver_van_bagac_update_status_moving';
export const DRIVER_VAN_BAGAC_UPDATE_STATUS_RECEIVED = 'driver_van_bagac_update_status_received';
export const DRIVER_VAN_BAGAC_UPDATE_STATUS_DELIVERIED = 'driver_van_bagac_update_status_deliveried';
export const DRIVER_VAN_BAGAC_UPDATE_STATUS_COMPLETED = 'driver_van_bagac_update_status_completed';
export const KEY_CUSTOMER_TIME_OUT_CANCEL_APPROVE_VAN_BAGAC = 'customer_time_out_cancel_approve_van_bagac';
export const KEY_CREATE_TOUR_ORDER = 'create_new_tour_order';
export const KEY_RECEIVE_TOUR_ORDER = 'receive_new_tour_order';
export const KEY_UPDATE_TOUR_ORDER = 'update_tour_order';
/*
* Use for redis
* */
// key, field  request
export const KEY_CUSTOMER_REQUEST_BOOK_BUS = 'key_customer_request_book_bus';
export const FIELD_CUSTOMER_REQUEST_BOOK_BUS = 'field_customer_request_book_bus';

export const KEY_CUSTOMER_REQUEST_BOOK_SHIPPING = 'key_customer_request_book_shipping';
export const FIELD_CUSTOMER_REQUEST_BOOK_SHIPPING = 'field_customer_request_book_shipping';

export const KEY_CUSTOMER_REQUEST_BOOK_TICKET = 'key_customer_request_book_ticket';
export const FIELD_CUSTOMER_REQUEST_BOOK_TICKET = 'field_customer_request_book_ticket';

export const KEY_CUSTOMER_REQUEST_BOOK_CAR_BIKE = 'key_customer_request_book_car_bike';
export const FIELD_CUSTOMER_REQUEST_BOOK_CAR_BIKE = 'field_customer_request_book_car_bike';

export const KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC = 'key_customer_request_book_van_bagac';
export const FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC = 'field_customer_request_book_van_bagac';

// key, field update position vehicle
export const VEHICLE_POSITIONS_BUS = 'vehicle_positions_bus';
export const FIELD_VEHICLE_POSITIONS_BUS = 'field_vehicle_positions_bus';

export const VEHICLE_POSITIONS_BIKE = 'vehicle_positions_bike';
export const FIELD_VEHICLE_POSITIONS_BIKE = 'field_vehicle_positions_bike';

export const VEHICLE_POSITIONS_CAR = 'vehicle_positions_car';
export const FIELD_VEHICLE_POSITIONS_CAR = 'field_vehicle_positions_car';

export const VEHICLE_POSITIONS_VAN = 'vehicle_positions_van';
export const FIELD_VEHICLE_POSITIONS_VAN = 'field_vehicle_positions_van';

export const VEHICLE_POSITIONS_BAGAC = 'vehicle_positions_bagac';
export const FIELD_VEHICLE_POSITIONS_BAGAC = 'field_vehicle_positions_bagac';

export const VEHICLE_RUNNING = 'vehicle_running';
export const FIELD_VEHICLE = 'field_vehicle';
