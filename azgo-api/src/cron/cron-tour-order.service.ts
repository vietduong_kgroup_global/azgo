import {forwardRef, Inject, Injectable} from '@nestjs/common';
import {Cron, InjectSchedule, Interval, NestSchedule, Schedule, Timeout} from 'nest-schedule';
import {VehicleMovingService} from '../module/vehicle-moving/vehicleMoving.service';
import {VehicleScheduleService} from '../module/vehicle-schedule/vehicleSchedule.service';
import {TourOrderService} from '../module/tour-order/tour-order.service';

@Injectable()
export class CronTourOrderService extends NestSchedule {
    constructor(
        private readonly vehicleScheduleService: VehicleScheduleService,
        private readonly tourOrderService: TourOrderService,
    ) {
        super();
    }
    // @Cron('0 0 2 * *', {
    //     startTime: new Date(),
    //     endTime: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
    // })
    // async cronJob() {
    //     console.log('executing cron job');
    // }
    /*
        * * * * * *
        | | | | | |
        | | | | | day of week
        | | | | month
        | | | day of month
        | | hour
        | minute
        second (optional)
     */ 

    @Cron('*/30 * * * *')
    async intervalJob() {
        console.log('executing interval job');
        await this.tourOrderService.checkTourOrder();
        if (!process.env.WEB_HOOK_SLACK || !process.env.CHANNEL_ALERT_TOUR_ORDER) 
            return true; // cancel the job if not found  WEB_HOOK_SLACK or CHANNEL_ALERT_TOUR_ORDER
         
    }
}
