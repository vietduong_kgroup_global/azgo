export enum VehicleGroup {
    BUS = 'BUS',
    BIKE = 'BIKE',
    CAR = 'CAR',
    VAN = 'VAN',
    BAGAC = 'BAGAC',
}

export enum VehicleGroupID {
    BUS = 1,
    BIKE ,
    CAR ,
    VAN ,
    BAGAC ,
}
