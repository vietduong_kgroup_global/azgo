export enum DeviceType {
    IOS = 'ios',
    ANDROID = 'android',
    OTHER = 'other',
}


export enum CurrentRole {
    ADMIN = 'admin',
    DRIVER = 'driver',
    CUSTOMER = 'customer',
}

export enum SocialType {
    FACEBOOK = 1,
    GOOGLE,
}

export enum AreaType {
    PROVINCE = 1 ,
    DISTRICT ,
}

export enum RequestBikeCar {
    BIKE = 'BIKE',
    CAR = 'CAR',
}

export enum TicketType {
    BOOK_TICKET = 1,
    BOOK_VEHICLE,
}
