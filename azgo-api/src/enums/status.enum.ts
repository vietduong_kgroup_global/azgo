export enum StatusCommon {
    ACTIVE = 1 ,
    INACTIVE,
}

export enum StatusVehicleSchedule {
    UNSTART = 1 ,
    STARTING,
    END,
    CANCEL,
}

 
export enum StatusVehicleScheduleRunning {
    STARTING = 1 ,
    END,
    CANCEL,
}

export enum StatusCode {
    CODE_CONFLICT = 409,
    OK = 200,
    CREATED ,
    BAD_REQUEST = 400,
    UNAUTHORIZED = 401,
    FORBIDDEN = 403,
    NOT_FOUND = 404,
    INTERNAL_SERVER_ERROR = 500,
    BAD_GATEWAY = 502,
}

export enum StatusRequestBookBus{
    REQUEST_NEW = 1,
    APPROVED,
    MOVING,
}

export enum StatusRequestWithdraw{
    Pending = 1,
    APPROVED,
    CANCEL,
}

export enum StatusRequestBooKVanBaGac{
    REQUEST_NEW = 1,
    APPROVED,
    MOVING,
    RECEIVED,
    DELIVERIED,
    COMPLETED,
}

export enum StatusDisableBtnApproveBooKVanBaGac{
    DEFAULT ,
    BE_CANCELED,
    CANNOT_BE_CANCELED,
}

export enum StatusPayment{
    UNPAID = 1,
    PAID,
}

export enum StatusPaymentMethod{
    CASH = 1,
    BANK,
}

export enum StatusCodeMessage {
    CODE_CONFLICT = 'Yều cầu đã tồn tại',
    OK = 'OK',
    CREATED = 'Created',
    BAD_REQUEST = 'Bad Request',
    UNAUTHORIZED = 'Unauthorized',
    FORBIDDEN = 'Forbidden',
    NOT_FOUND = 'Not Found',
    INTERNAL_SERVER_ERROR = 'Internal Server Error',
    BAD_GATEWAY = 'Bad Gateway',
}
