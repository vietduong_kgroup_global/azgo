import {Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {CustomerProfileEntity} from '../customer-profile/customer-profile.entity';
import {DriverProfileEntity} from '../driver-profile/driver-profile.entity';
import {VehicleTypesEntity} from '../vehicle-types/vehicle-types.entity';
import {VehicleEntity} from '../vehicle/vehicle.entity';
import {CustomChargeEntity} from '../custom-charge/custom-charge.entity';
import {ApiModelProperty} from '@nestjs/swagger';
import {RatingDriverEntity} from '../rating-driver/rating-driver.entity';

@Entity('az_bike_car_orders')
export class BikeCarOrderEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
    })
    customer_id: number;

    @ManyToOne(type => CustomerProfileEntity, user => user.user_id)
    @JoinColumn({name: 'customer_id', referencedColumnName: 'user_id'})
    customerProfile: CustomerProfileEntity;

    @Column({
        type: 'int',
    })
    driver_id: number;

    @ManyToOne(type => DriverProfileEntity, user => user.user_id)
    @JoinColumn({name: 'driver_id', referencedColumnName: 'user_id'})
    driverProfile: DriverProfileEntity;

    @Column({
        type: 'int',
    })
    service_id: number;

    @Column({
        type: 'int',
    })
    vehicle_id: number;

    @ManyToOne(type => VehicleEntity, vehicle => vehicle.bikeCarOrder)
    @JoinColumn({name: 'vehicle_id', referencedColumnName: 'id'})
    vehicleBikeCar: VehicleEntity;

    @Column({
        type: 'int',
    })
    vehicle_group_id: number;

    @ManyToOne(type => CustomChargeEntity, customCharge => customCharge.bikeCarOrders)
    @JoinColumn({name: 'vehicle_group_id', referencedColumnName: 'vehicle_group_id'})
    customCharge: CustomChargeEntity;

    @ManyToOne(type => VehicleTypesEntity, vehicle_type => vehicle_type.bikeCarService)
    @JoinColumn({name: 'service_id', referencedColumnName: 'id'})
    vehicleType: VehicleTypesEntity;

    @Column({
        type: 'json',
    })
    route_info: object;

    @ApiModelProperty({ default: 1 })
    payment_method: number;

    @Column({
        type: 'smallint',
    })
    @ApiModelProperty({ default: 1 })
    payment_status: number;

    @Column({
        type: 'smallint',
    })
    @ApiModelProperty({ default: 1 }) // 1: Not yet rated, 2: Have evaluated
    flag_rating_customer: number;

    @Column({
        type: 'smallint',
    })
    @ApiModelProperty({ default: 1 }) // 1: Not yet rated, 2: Have evaluated
    flag_rating_driver: number;

    @Column({
        type: 'varchar',
        length: '255',
    })
    book_car_bike_reference: string;

    @Column({
        type: 'smallint',
        comment: '1: Chưa đi , 2: Đang đi, 3: Hoàn thành, 4: Đã hủy',
    })
    status: number;

    @Column({
        type: 'float',
    })
    price: number;

    @Column({
        type: 'float',
    })
    rate_of_charge: number;

    @Column({
        type: 'float',
    })
    fee_tax: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @OneToOne(type => RatingDriverEntity, rating_bike_car => rating_bike_car.bikeCarOrder)
    ratingDriver: RatingDriverEntity;
}
