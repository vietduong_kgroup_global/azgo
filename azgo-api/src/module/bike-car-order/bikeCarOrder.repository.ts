import {EntityRepository, Repository} from 'typeorm';
import {BikeCarOrderEntity} from './bikeCarOrder.entity';

@EntityRepository(BikeCarOrderEntity)
export class BikeCarOrderRepository extends Repository<BikeCarOrderEntity> {

}
