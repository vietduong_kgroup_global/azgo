import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {BikeCarOrderRepository} from './bikeCarOrder.repository';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            BikeCarOrderRepository,
        ]),
    ],
    providers: [BikeCarOrderRepository],
    controllers: [],
})

export class BikeCarOrderModule {

}
