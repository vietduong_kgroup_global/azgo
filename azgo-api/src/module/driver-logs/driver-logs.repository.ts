import {EntityRepository, Repository} from 'typeorm';
import {DriverLogsEntity} from './driver-logs.entity';

@EntityRepository(DriverLogsEntity)
export class DriverLogsRepository extends Repository<DriverLogsEntity> {

}