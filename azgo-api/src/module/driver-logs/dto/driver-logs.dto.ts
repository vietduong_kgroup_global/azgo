import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class DriverLogsDto {

    @IsNotEmpty({ message: 'status' })
    @ApiModelProperty({ example: '1: Accept, 2:Cancel' , required: true, default: 1 })
    status: number;
}
