import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {UsersEntity} from '../users/users.entity';

@Entity('az_driver_logs')
export class DriverLogsEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
    })
    driver_id: number;

    @ManyToOne(type => UsersEntity, user => user.driverLogs)
    @JoinColumn({name: 'driver_id', referencedColumnName: 'id'})
    driver: UsersEntity;

    @Column({
        type: 'smallint',
        comment: '1: Accept, 2: Cancel',
    })
    status: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;
}
