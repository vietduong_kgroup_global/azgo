import {Controller} from '@nestjs/common';
import {
    ApiUseTags,
} from '@nestjs/swagger';

@ApiUseTags('Driver logs')
@Controller('driver-logs')
export class DriverLogsController {

    constructor() {
    }

}
