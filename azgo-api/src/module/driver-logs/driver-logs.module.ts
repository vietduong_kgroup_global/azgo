import {Module} from '@nestjs/common';
import { DriverLogsController } from './driver-logs.controller';
import { DriverLogsService } from './driver-logs.service';
import {DriverLogsRepository} from './driver-logs.repository';
import {TypeOrmModule} from '@nestjs/typeorm';
import {FirebaseRepository} from '../firebase/firebase.repository';
import {UsersModule} from '../users/users.module';

@Module({imports: [
    TypeOrmModule.forFeature([
        DriverLogsRepository,
        FirebaseRepository,
    ]),
    UsersModule,
  ],
  controllers: [ DriverLogsController],
  providers: [DriverLogsService],
  exports: [DriverLogsService],
})
export class DriverLogsModule {}
