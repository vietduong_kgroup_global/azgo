import { Injectable } from '@nestjs/common';
import {DriverLogsRepository} from './driver-logs.repository';
import {InjectRepository} from '@nestjs/typeorm';

@Injectable()
export class DriverLogsService {
    constructor(
        @InjectRepository(DriverLogsRepository)
        private readonly driverLogsRepository: DriverLogsRepository,
    ) {
    }

    async createDriverLogs(data) {
        return await this.driverLogsRepository.save(data);
    }

    async report(driver_id: number) {
        const count = await this.driverLogsRepository.count({driver_id});
        const countCancel = await this.driverLogsRepository.count({driver_id, status: 2});
        const countApprove = await this.driverLogsRepository.count({driver_id, status: 1});
        return {approve: Math.round((100 * countApprove) / count), cancle: Math.round((100 / countCancel) / count)};
    }
}
