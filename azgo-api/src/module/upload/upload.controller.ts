import { Controller, Body, Post, HttpStatus, Res, Req, UseInterceptors, UploadedFile } from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation, ApiConsumes, ApiImplicitFile } from '@nestjs/swagger';
import { Response } from 'express';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import {S3AwsService} from '@azgo-module/core/dist/common/aws/s3-aws.service';
import config from '@azgo-module/core/dist/config';
import {FileInterceptor} from '@nestjs/platform-express';

@ApiUseTags('S3 Upload file')
@Controller('upload')
export class UploadController {
    constructor(
    ) { }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @Post()
    @ApiOperation({ title: 'Upload image' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @UseInterceptors(FileInterceptor('files[]'))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({ name: 'files', required: true, description: 'Upload image' })
    async upload(@UploadedFile() files, @Req() request, @Body() body, @Res() res: Response) {
        if (!request.files) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('No files is uploaded!', null));
        }
        let data = request.files.map(file => {
            return {
                address: file.location,
                file_path: config.awsSetting.s3Setting.key,
                file_name: file.key.replace(config.awsSetting.s3Setting.key + '/', ''),
            };
        });
        return res.status(HttpStatus.CREATED).send(dataSuccess('Uploaded', data));
    }
}
