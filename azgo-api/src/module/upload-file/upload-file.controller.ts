import {Controller, UseGuards} from '@nestjs/common';
import {UploadFilesController} from './upload-files.controller';
import {ApiBearerAuth, ApiUseTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';

@ApiUseTags('Upload file')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('upload-file')
export class UploadFileController extends UploadFilesController{

}