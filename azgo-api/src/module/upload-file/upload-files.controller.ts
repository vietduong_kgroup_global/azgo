import {Controller, HttpException, Param, Post, UploadedFiles, UseInterceptors, Req, HttpStatus} from '@nestjs/common';
import {extname, join} from 'path';
import * as fs from 'fs';
import {FilesInterceptor} from '@nestjs/platform-express';
import * as sharp from 'sharp';
import {diskStorage} from 'multer';
import config from '../../../config/config'
import { Request } from 'express';
// import {Roles} from '../decorators/roles.decorator';
// import {ActionKey, FunctionKey} from '../guards/roles.guards';
import {ApiImplicitFile} from '@nestjs/swagger';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { ActionKey, FunctionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
// import config from '../../config/config';

@Controller()
export class UploadFilesController {
    @Post('images')
    @Roles({function_key: FunctionKey.All, action_key: ActionKey.All})
    @ApiImplicitFile({name: 'images[]', description: 'Support files type jpg|jpeg|png|gif'})
    @UseInterceptors(FilesInterceptor('images[]', 20,
        {
            limits: {
                fileSize: 3000000,
                fieldSize: 300000,
                fieldNameSize: 500,
            },
            fileFilter: (req, images, cb) => {
                if (images.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
                    // Allow storage of file
                    return cb(null, true);
                } else {
                    // Reject file
                    return cb(new HttpException(`Unsupported file type ${extname(images.originalname)}`, HttpStatus.BAD_REQUEST), false);

                }
            },
            storage: diskStorage({
                destination: (req, files, cb) => {
                    const parentPath = join('./public/', 'images');
                    if (!fs.existsSync(parentPath)) {
                        fs.mkdirSync(parentPath, {recursive: true});
                    }
                    // const path = join(parentPath); /* Maybe join with @Param so create folder*/
                    // if (!fs.existsSync(parentPath)) {
                    //     fs.mkdirSync(parentPath);
                    // }
                    return cb(null, parentPath);
                },
                filename: (req: Request, images, cb) => {
                    const randomName = Date.now();
                    return cb(null, `${randomName}${extname(images.originalname).toLowerCase()}`);
                },
            }),
        },
    ))
    async uploadImages(@Req() req: Request, @UploadedFiles() images) {
        console.log("uploadImages", images)
        sharp.cache(false);
        if (typeof images === 'undefined') {
            return { status: false, message: 'File is require'};
        }
        const data = await Promise.all(images.map(async item => {
            const image_buffer = await sharp(item.path)
                .resize(Number(process.env.RESIZE_IMAGE_WIDTH), Number(process.env.RESIZE_IMAGE_HEIGHT), {fit: 'inside', withoutEnlargement: true})
                .toBuffer();
            fs.writeFileSync(item.path, image_buffer);
            return {
                address: "http://" +config.env.url  + '/' + item.path.replace('/files', 'assets').replace(new RegExp(/\\/g), '/').replace('public/', ''),
                file_name: item.filename,
                file_path: 'assets/images',
            };

        }));

        return {status: true, message: 'Uploaded', data};
    }
}
