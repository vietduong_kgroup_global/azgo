import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {ApiModelProperty} from '@nestjs/swagger';
import {VehicleScheduleEntity} from '../vehicle-schedule/vehicleSchedule.entity';
import {UsersEntity} from '../users/users.entity';

@Entity('az_book_shipping')
export class BookShippingEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    ticket_code: string;

    @Column({
        type: 'json',
        nullable: false,
    })
    pickup_point: object;

    @Column({
        type: 'int',
        nullable: false,
    })
    @ApiModelProperty({ required: true, example: 1})
    schedule_id: number;

    @ManyToOne(type => VehicleScheduleEntity, vehicleSchedule => vehicleSchedule.bookShippings)
    @JoinColumn({name: 'schedule_id', referencedColumnName: 'id'})
    vehicleSchedule: VehicleScheduleEntity;

    @Column({
        type: 'int',
        nullable: false,
    })
    @ApiModelProperty({ required: true, example: 1})
    user_id: number;
    @ManyToOne(type => UsersEntity, user => user.user_id)
    @JoinColumn({name: 'user_id', referencedColumnName: 'id'})
    user: UsersEntity;

    @Column({
        type: 'smallint',
        default: 1,
    })
    @ApiModelProperty({ required: true, example: 1, default: 1})
    status: number;

    @Column({
        type: 'smallint',
    })
    @ApiModelProperty({ default: 1 })
    payment_method: number;

    @Column({
        type: 'smallint',
    })
    @ApiModelProperty({ default: 1 })
    payment_status: number;

    @Column({
        type: 'float',
    })
    @ApiModelProperty({ example: 5000 })
    price: number;

    @Column({
        type: 'varchar',
        length: 255,
    })
    @ApiModelProperty({ example: 'Cần Thơ' })
    address_ship: string;

    @Column({
        type: 'varchar',
        length: 255,
    })
    @ApiModelProperty({ example: 'Cần Thơ' })
    destination: string;

    @Column({
        type: 'varchar',
        length: 255,
    })
    @ApiModelProperty({ example: '2cec793b-ac49-4f62-90a8-b990923509d7_dzjj0hhx975' })
    book_shipping_reference: string;

    @Column({
        type: 'varchar',
        length: 255,
    })
    @ApiModelProperty({ example: 'Bùi Anh Tuấn' })
    receiver: string;

    @Column({
        type: 'varchar',
        length: 255,
    })
    @ApiModelProperty({ example: '01229125354' })
    phone: string;

    @Column({
        type: 'json',
    })
    commodity_information: object;

    @Column({
        type: 'json',
    })
    commodity_images: [object];

    @Column({
        type: 'text',
        nullable: true,
    })
    @ApiModelProperty({ example: 'abcxyz'})
    qr_code: string;

    @Column({
        type: 'smallint',
        nullable: true,
    })
    @ApiModelProperty({ example: 1})
    qr_code_status: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;
}
