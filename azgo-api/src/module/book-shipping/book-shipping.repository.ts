import {EntityRepository, Repository} from 'typeorm';
import {BookShippingEntity} from './book-shipping.entity';
@EntityRepository(BookShippingEntity)
export class BookShippingRepository extends Repository<BookShippingEntity>{

}