import {
    Body,
    Controller,
    Delete,
    Get,
    HttpStatus,
    Param,
    Post,
    Put,
    Req,
    Res,
    UseGuards,
    UsePipes,
} from '@nestjs/common';
import {ApiBearerAuth, ApiImplicitParam, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { BookShippingService } from './book-shipping.service';
import { AuthGuard } from '@nestjs/passport';
import { CreateBookShippingDto } from './dto/create-book-shipping.dto';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { UsersService } from '../users/users.service';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { Response } from 'express';
import { FindStationNearCustomerBookShippingDto } from './dto/findStationNearCustomerBookShipping.dto';
import { RequestBookShippingDto } from './dto/request-book-shipping.dto';
import { PaidBookShippingDto } from './dto/PaidBookShipping.dto';
import * as jwt from 'jsonwebtoken';
import { RatingScheduleService } from '../rating-schedule/rating-schedule.service';
import { UpdateStatusBookShippingDto } from './dto/update-status-book-shipping.dto';
import { VehicleScheduleService } from '../vehicle-schedule/vehicleSchedule.service';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import {BookBusUrlPaymentDto} from '../vn-pay/dto/book-bus-url-payment.dto';
import {BookShippingUrlPaymentDto} from './dto/book-shipping-url-payment.dto';
import * as moment from 'moment';
import {StatusPayment} from '../../enums/status.enum';

@ApiBearerAuth()
@ApiUseTags('Book shipping')
@Controller('book-shipping')
export class BookShippingController {
    constructor(
        private bookShippingService: BookShippingService,
        private userService: UsersService,
        private ratingScheduleService: RatingScheduleService,
        private vehicleScheduleService: VehicleScheduleService,

    ) {
    }

    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BUS]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create book shipping, when driver approve request' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @UsePipes(CustomValidationPipe)
    async orderBookShipping(@Body() data: CreateBookShippingDto, @Res() res: Response){
        try{
            const result = await this.bookShippingService.driverApproveOrder(data.book_shipping_reference);
            if (!result) {
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Request order not found', null));
            }
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:book_shipping_reference')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Edit book shipping' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'book_shipping_reference', description: 'book shipping reference', required: true, type: 'string' })
    async updateBookshipping(
        @Param('book_shipping_reference') book_shipping_reference,
        @Body() data: UpdateStatusBookShippingDto, @Res() res: Response) {
        try{
            const result = await this.bookShippingService.updateBookShipping(data.status, book_shipping_reference);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Book shipping reference not found', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/:book_shipping_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get detail book shipping' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'book_shipping_id', description: 'Book Shipping Id', required: true, type: 'number' })
    async getDetailBookShipping(@Param('book_shipping_id') book_shipping_id, @Res() res: Response){
        try {
            const result = await this.bookShippingService.getDetail(book_shipping_id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('cancel-approve-book-shipping-by-customer/:book_shipping_reference')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Cancel approve book shipping by customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'book_shipping_reference', description: 'Book shipping reference', required: true, type: String })
    async cancelApproveBookShipping(
        @Param('book_shipping_reference') book_shipping_reference: string,
        @Req() request,
        @Res() res: Response){
        try {
            const result = await this.bookShippingService.cronCancleApproveBookShipping(book_shipping_reference);
            if (!result) {
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
            }
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/get-url-payment-book-bus-shipping')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @ApiOperation({ title: 'Get url payment book bus shipping' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getUrlPayment(@Req() request, @Body() body: BookShippingUrlPaymentDto , @Res() res: Response) {
        try {
            /* Get IP address */
            const vnp_IpAddr = request.headers['x-forwarded-for'] ||
                request.connection.remoteAddress ||
                request.socket.remoteAddress ||
                request.connection.socket.remoteAddress;
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const customerInfo: any =  jwt.decode(token);
            const data = {...body, ...{customer_uuid: customerInfo.user_id.toString(), vnp_IpAddr}};
            const result = await this.bookShippingService.createUrlPayment(data);
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Paid book bus existed! ', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/list/get-book-shipping-by-customer')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get list book shipping follow user, history book shipping of customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getBookShippingByCustomer(@Req() request, @Res() res: Response){
        try {
            const token = request.headers.authorization.split(' ')[1];
            const customerInfo =  jwt.decode(token);
            const findUserByUuid = await this.userService.findOneByUserInfo(customerInfo);
            if (findUserByUuid){
                const bookShippingEntities = await this.bookShippingService.getBookShippingByCustomer(findUserByUuid.id);

                await Promise.all(bookShippingEntities.map(async (item, index) => {
                    try {
                        const avgDriver = await this.ratingScheduleService.avgRatingDriverByDriverId(item.vehicleSchedule.user_id);
                        const avgProviderByAllSchedule = await this.vehicleScheduleService.getAllScheduleByProvider(item.vehicleSchedule.provider_id);
                        bookShippingEntities[index] = {...item,
                            ...{rating_driver: avgDriver ? Number(avgDriver.avg).toFixed(1) : '0.0' },
                            ...{rating_provider: avgProviderByAllSchedule ? Number(avgProviderByAllSchedule.avg).toFixed(1) : '0.0' }};
                    }catch (e) {
                        throw {message: e.message, status: e.status};
                    }

                }));
                if (bookShippingEntities) {
                    return res.status(HttpStatus.OK).send(dataSuccess('oK', bookShippingEntities));
                }
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('User not found', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('get-book-shipping-by-schedule/:schedule_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get list book shipping follow schedule' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'schedule_id', required: true, type: 'number'})
    async getBookShippingBySchedule(@Param('schedule_id') schedule_id: number, @Res() res: Response){
        try {
            const result = await this.bookShippingService.getBookShippingBySchedule(schedule_id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all book shipping' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getAllBookShipping(@Res() res: Response){
        try {
            const result = await this.bookShippingService.getAllBookShipping();
            
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Post('/find-vehicle-bus-station-near-customer')
    @ApiOperation({ title: 'Find vehicle bus station near customer or filter provider & price (book shipping)' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async findStationsNearCustomer(@Body() body: FindStationNearCustomerBookShippingDto, @Res() res: Response) {
        try {
            const result = await this.bookShippingService.findBusHasSchedule(body);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
            }
            return res.status(HttpStatus.OK).send(dataSuccess('Not station removed', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/choose-bus-book-shipping')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Choose bus book shipping, push notify to driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async requestBusBookShipping(@Req() request, @Body() body: RequestBookShippingDto, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        // decode token
        const customerInfo =  jwt.decode(token);
        const findUserByUuid = await this.userService.findOneByUserInfo(customerInfo);
        if (!findUserByUuid) return res.status(HttpStatus.OK).send(dataError('Not found customer', null));
        const created_at = moment();
        const customerRequest = {
            customer_uuid: findUserByUuid.user_id.toString(),
            user_id: findUserByUuid.id,
            customer_full_name: findUserByUuid.customerProfile.full_name,
            customer_phone: findUserByUuid.phone,
            customer_image: findUserByUuid.customerProfile.image_profile,
            created_at,
            expires_request_miliseconds: Number(process.env.REQUEST_BOOK_BUS_TIMER || 120000),
            payment_status: StatusPayment.UNPAID,
        };
        try {
            const result = await this.bookShippingService.requestBusAndPushNotify({...body, ...customerRequest});
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Order not success', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/paid-book-shipping')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Push notify and update payment status book bus for customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async customerPaidBookShipping(
        @Req() request,
        @Body() body: PaidBookShippingDto , @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const customerInfo: any =  jwt.decode(token);
        try {
            const result = await this.bookShippingService.customerPaid({
                book_shipping_reference: body.book_shipping_reference, customer_uuid: customerInfo.user_id.toString(),
            });
            if (!result)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found order', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Paid success', result));
        }
        catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/cancel-find-driver-book-shipping-by-customer/:book_shipping_reference')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Cancel find driver book shipping by customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'book_shipping_reference', description: 'book shipping reference', required: true, type: String})
    async cancelFindDriver(@Req() request, @Res() res: Response, @Param('book_shipping_reference') book_shipping_reference: string) {
        try {
            const result = await this.bookShippingService.cancelFindDrivers(book_shipping_reference);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('OK', null));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found request', null));
        }
        catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
