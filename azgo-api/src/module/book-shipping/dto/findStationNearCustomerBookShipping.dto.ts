import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';


export class FindStationNearCustomerBookShippingDto {

    @IsNotEmpty()
    @ApiModelProperty({type: CoordinatesDto , required: true  })
    start_point: CoordinatesDto;

    @IsNotEmpty()
    @ApiModelProperty({ example: 2, type: Number, required: true  })
    end_point: number;

    /* For filter*/
    @ApiModelProperty({ example: 1 , type: Number,  required: false  })
    provider_id: number;

    @ApiModelProperty({ example: 10000 , type: Number, required: false  })
    price: number;
}
