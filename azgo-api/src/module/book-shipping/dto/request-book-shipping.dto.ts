import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';
import {ImageDto} from '../../../general-dtos/image.dto';
export class CommodityInformation {
    @ApiModelProperty({example: 'Sản phẩm abcxyz', description: 'Thông tin hàng hóa', type: String, required: true})
    @IsNotEmpty()
    commodity_info: string;

    @ApiModelProperty({example: '15 cm', description: 'Chiều dài', type: String, required: true})
    @IsNotEmpty()
    length: string;

    @ApiModelProperty({example: '20 cm', description: 'Chiều cao', type: String, required: true})
    @IsNotEmpty()
    height: string;

    @ApiModelProperty({example: '10 cm', description: 'Chiều rộng', type: String, required: true})
    @IsNotEmpty()
    width: string;

    @ApiModelProperty({example: '200 g', description: 'Khối lượng', type: String, required: true})
    @IsNotEmpty()
    mass: string;
}
export class RequestBookShippingDto {

    @IsNotEmpty()
    @ApiModelProperty({ example: 'f36d515f-e996-4093-a3e4-c548d92c09a8' , description: 'Driver uuid', type: String, required: true  })
    driver_uuid: string;

    @IsNotEmpty()
    @ApiModelProperty({ type: CoordinatesDto, description: 'Điểm đi lấy từ gg map' , required: true  })
    pickup_point: CoordinatesDto;

    @IsNotEmpty()
    @ApiModelProperty({ example: 2, type: Number, description: 'Điểm đến lấy từ area', required: true  })
    end_point: number;

    @IsNotEmpty()
    @ApiModelProperty({type: CommodityInformation, required: true})
    commodity_information: CommodityInformation;

    @ApiModelProperty({ type: [ImageDto], required: false})
    commodity_images: [ImageDto];

    @IsNotEmpty()
    @ApiModelProperty({example: 1, description: 'Mã chuyến', required: true})
    schedule_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1000, type: Number, required: true})
    price: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'Cần Thơ', type: String, required: true})
    address_ship: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'Bùi Anh Tuấn', type: String})
    receiver: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: '0909990009', type: String})
    phone: string;

}
