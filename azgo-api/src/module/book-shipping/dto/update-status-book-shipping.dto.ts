import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString} from 'class-validator';
export class UpdateStatusBookShippingDto {

    @IsNotEmpty()
    @ApiModelProperty({example: '1: Chấp nhận, 2: Đã nhận hàng ',  required: true})
    status: number;
}
