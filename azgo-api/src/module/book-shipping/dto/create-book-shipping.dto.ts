import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString} from 'class-validator';
export class CreateBookShippingDto {

    @IsNotEmpty()
    @ApiModelProperty({example: '1188cbac-94f9-4c04-8d96-17f5bac71530_w1n8wtaxdndbookshipping',  type: String, required: true})
    @IsString()
    book_shipping_reference: string;

}
