import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class PaidBookShippingDto {

    @IsNotEmpty()
    @ApiModelProperty({ example: 'book shipping reference' , required: true  })
    book_shipping_reference: string;

}
