import {forwardRef, Inject, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {BookShippingRepository} from '../book-shipping/book-shipping.repository';
import {VehicleScheduleService} from '../vehicle-schedule/vehicleSchedule.service';
import {RedisUtil} from '@azgo-module/core/dist/utils/redis.util';
import {RedisService} from '@azgo-module/core/dist/background-utils/Redis/redis.service';
import config from '@azgo-module/core/dist/config';
import {CommonService} from '../../helpers/common.service';
import {FirebaseService} from '../firebase/firebase.service';
import * as _ from 'lodash';
import {AreaService} from '../area/area.service';
import { StringHelper } from '@azgo-module/core/dist/utils/string.helper';
import {VehicleMovingService} from '../vehicle-moving/vehicleMoving.service';
import {BookShippingEntity} from './book-shipping.entity';
import {GoogleMapService} from '../google-map/google-map.service';
import {VnPayService} from '../vn-pay/vn-pay.service';
import {StatusCode, StatusPayment, StatusPaymentMethod, StatusRequestBookBus} from '../../enums/status.enum';
import {VehicleGroup} from '../../enums/group.enum';
import {
    KEY_CANCEL_APPROVE_REQUEST_BOOK_BUS_BY_CUSTOMER, KEY_CANCEL_APPROVE_REQUEST_BOOK_SHIPPING_BY_CUSTOMER,
    KEY_CANCEL_APPROVED_BUS,
    KEY_CANCEL_FIND_BOOK_SHIPPING_BY_CUSTOMER,
    KEY_CANCEL_FIND_BUS,
    KEY_CLICK_ACTION,
    KEY_CUSTOMER_SEND_REQUEST_BOOK_SHIPPING_TO_DRIVER,
    KEY_DRIVER_APPROVE_REQUEST_BOOK_SHIPPING_BY_CUSTOMER,
    KEY_NOTIFY_PAID_BOOK_BUS_TO_CUSTOMER,
} from '../../constants/keys.const';
import {InjectSchedule, Schedule} from 'nest-schedule';
import {SECRET_BOOK_SHIPPING} from '../../constants/secrets.const';
import * as moment from 'moment';

@Injectable()
export class BookShippingService {
    private redisService;
    private redis;
    // Book shipping
    private keyRedisCustomerRequestBookShipping = 'key_customer_request_book_shipping';
    private fieldRedisCustomerRequestBookShipping = 'field_customer_request_book_shipping';
    // use for vehicle bus
    private keyRedisVehiclePositionBus = 'vehicle_positions_bus';
    private fieldRedisVehiclePositionBus = 'field_vehicle_positions_bus';
    // use for vehicle bike
    private keyRedisVehiclePositionBike = 'vehicle_positions_bike';
    private fieldRedisVehiclePositionBike = 'field_vehicle_positions_bike';
    // use for vehicle car
    private keyRedisVehiclePositionCar = 'vehicle_positions_car';
    private fieldRedisVehiclePositionCar = 'field_vehicle_positions_car';
    // use for vehicle van
    private keyRedisVehiclePositionVan = 'vehicle_positions_van';
    private fieldRedisVehiclePositionVan = 'field_vehicle_positions_van';
    // use for vehicle bagac
    private keyRedisVehiclePositionBagac = 'vehicle_positions_bagac';
    private fieldRedisVehiclePositionBagac = 'field_vehicle_positions_bagac';
    private distanceBusApprove = 2000;
    private  connect:any;
    constructor(
        @InjectRepository(BookShippingRepository)
        private readonly bookShippingRepository: BookShippingRepository,
        @Inject(forwardRef(() => VehicleScheduleService))
        private readonly scheduleService: VehicleScheduleService,
        private commonService: CommonService,
        private firebaseService: FirebaseService,
        private areaService: AreaService,
        private vehicleMovingService: VehicleMovingService,
        private readonly googleMapService: GoogleMapService,
        private readonly vnPayService: VnPayService,
        @InjectSchedule() private readonly schedule: Schedule,

    ) {
        this.connect= {
            port: process.env.ENV_REDIS_PORT || 6379,
            host: process.env.ENV_REDIS_HOST || '127.0.0.1', 
            password: process.env.ENV_REDIS_PASSWORD || null,
        };
        console.log(" Redis constructor:",this.connect);
        this.redisService = RedisService.getInstance(this.connect);
        this.redis = RedisUtil.getInstance();
    }
    async findBusHasSchedule(data) {
        const today = moment().format('x');

        // get cache vehicle position
        let listScheduleChoose = [];
        const unit = 'K';

        // Get vehicle position by driver
        const redisKey = await this.keyRedisVehiclePosition('BUS');
        const redisField = await this.fieldRedisVehiclePosition('BUS');
        let getCache = await this.redis.getInfoRedis(redisKey, redisField);

        if (getCache.isValidate) {
            const listVehicleBus = JSON.parse(getCache.value);
            /* Filter end point customer with end point schedule*/
            const findScheduleWithEndPoint = _.filter(listVehicleBus, (vehicle) => {
                const end_hour_schedule = moment(vehicle.end_hour, 'HH:mm DD-MM-YYYY').format('x');
                return Number(vehicle.end_point) === (data.end_point)
                    && end_hour_schedule > today;
            });
            console.log(findScheduleWithEndPoint)
            if (findScheduleWithEndPoint.length > 0) {
                // get list station approve about distance
                await Promise.all(findScheduleWithEndPoint.map(async (val) => {
                    if (val.router.additional_station.length > 0) {
                        // filter station with flag flase
                        const filterAdditionalStation = await _.filter( val.router.additional_station , {flag: false});
                        let distance;
                        let long_time = 0,
                            tempDistanceCustomerToStation = null,
                            distance_station_to_end_point = 0,
                            distance_customer_to_station = 0;
                        let station_address = null;
                        // Find station approve about distance
                        if (filterAdditionalStation.length > 0) {

                            /* Get coordinates of end point*/
                            const areaEntity = await this.areaService.findById(Number(val.end_point));
                            if (!areaEntity) throw {message: 'coordinates end point not found!', status: StatusCode.NOT_FOUND};
                            const coordinatesEndPoint: any = areaEntity.coordinates;
                            /* Loop stations and calc distance*/
                            await Promise.all(filterAdditionalStation.map(
                                async (station, index) => {
                                    // Distance customer to station
                                    const getDistances: any = await Promise.all([
                                        this.googleMapService.rawUrlGetDistance({
                                            origins: {lat: data.start_point.lat, lng: data.start_point.lng},
                                            destinations: {lat: station.lat, lng: station.lng},
                                        }),
                                        this.googleMapService.rawUrlGetDistance({
                                            origins: {lat: val.lat, lng: val.lng},
                                            destinations: {lat: station.lat, lng: station.lng},
                                        }),

                                        this.googleMapService.rawUrlGetDistance({
                                            origins: {lat: station.lat, lng: station.lng},
                                            destinations: {lat: coordinatesEndPoint.lat, lng: coordinatesEndPoint.lng},
                                        }),
                                        new Promise((resolved) => {
                                            resolved(station);
                                        }),
                                    ]);

                                    if (getDistances) {
                                        // Distance from customer to station, unit: meters
                                        const distance_pickup_point = getDistances[0].data.rows[0].elements[0].distance.value;

                                        // Distance from vehicle to station
                                        const distance_vehicle_to_station = getDistances[1].data.rows[0].elements[0].distance.text;
                                        const distance_vehicle_to_station_value = getDistances[1].data.rows[0].elements[0].distance.value;

                                        // Distance from station to end point
                                        const distance_station_end_point = getDistances[2].data.rows[0].elements[0].distance.text;

                                        // Compare distance approve
                                        const conditionFilterStation =
                                            Number(distance_pickup_point) <= (Number(process.env.BUS_DISTANCE_APPROVED) || 50000)
                                            && (Number(distance_vehicle_to_station_value) / 3) >= Number(distance_pickup_point);
                                        if (conditionFilterStation) {
                                            //  Get station nearest by customer
                                            const stationChoosed = tempDistanceCustomerToStation === null ||
                                                tempDistanceCustomerToStation >= distance_pickup_point;

                                            if (stationChoosed) {
                                                tempDistanceCustomerToStation = distance_pickup_point;
                                                distance = distance_vehicle_to_station;
                                                distance_station_to_end_point = distance_station_end_point;
                                                distance_customer_to_station =  getDistances[0].data.rows[0].elements[0].distance.text;
                                                station_address = getDistances[3];
                                                long_time = getDistances[1].data.rows[0].elements[0].duration.text;
                                            }
                                        }
                                    }
                                }));
                            val.distance = distance;
                            val.distance_station_to_end_point = distance_station_to_end_point;
                            val.distance_customer_to_station = distance_customer_to_station;
                            val.unit = unit;
                            val.price = (Number(String(distance_station_to_end_point).match(/\d+/)[0]) * val.price_per_km).toFixed(1);
                            val.pickup_point = station_address;
                            val.long_time = long_time;
                        }
                        if (distance !== undefined) {
                            listScheduleChoose.push(val);
                        }

                    }

                }));

                if (data.provider_id !== undefined) {
                    listScheduleChoose = _.filter(listScheduleChoose, {provider_id: data.provider_id});

                }
                if (data.price !== undefined) {
                    listScheduleChoose = _.filter(listScheduleChoose, schedule => {
                        return Number(schedule.price) <= Number(data.price);
                    });
                }
                return listScheduleChoose;
            }
        }
        return null;
    }

    async driverApproveOrder(book_shipping_reference: string) {
        let rsPositionVehicle, requestBook;
        const getCache = await this.redis.getInfoRedis(
            this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping);
        if (getCache.isValidate) {
            const listRequest = JSON.parse(getCache.value);
            requestBook = _.find(listRequest, {book_shipping_reference});
            const index = _.findIndex(listRequest, {book_shipping_reference});
            if (index < 0) return  false;

            requestBook.status = StatusRequestBookBus.APPROVED;
            listRequest[index] = requestBook;
            await this.redis.setInfoRedis(
                this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping, JSON.stringify(listRequest));

            // Add cron job, remove request when expise timer
            await this.schedule.scheduleIntervalJob(
                KEY_CANCEL_APPROVED_BUS + book_shipping_reference,
                Number(process.env.REQUEST_BOOK_BUS_APPROVED_TIMER) || 120000,
                () => this.cronCancleApproveBookShipping(book_shipping_reference),
            );
            // Cancel cron
            this.schedule.cancelJob(KEY_CANCEL_FIND_BUS + book_shipping_reference);

            // get position driver, vehicle
            const redisKey = await this.keyRedisVehiclePosition(VehicleGroup.BUS);
            const redisField = await this.fieldRedisVehiclePosition(VehicleGroup.BUS);
            let getCachePositionVehicles = await this.redis.getInfoRedis(redisKey, redisField);
            if (getCachePositionVehicles.isValidate) {
                const listPositionVehicles = JSON.parse(getCachePositionVehicles.value);
                rsPositionVehicle = listPositionVehicles.find(item => item.user_uuid === requestBook.driver_uuid);

            }
            const userFCM = await this.firebaseService.findOne(requestBook.customer_uuid);
            const notification = {
                title: 'Tài xế chấp nhận yêu cầu của bạn',
                body: 'Tài xế chấp nhận yêu cầu của bạn',
            };
            const dataNotificationData = {
                key: KEY_DRIVER_APPROVE_REQUEST_BOOK_SHIPPING_BY_CUSTOMER,
                book_shipping_reference,
                click_action: KEY_CLICK_ACTION,
                driver_position: {lat: rsPositionVehicle.lat.toString(), lng: rsPositionVehicle.lng.toString()},
                expires_approve_miliseconds: Number(process.env.REQUEST_BOOK_BUS_APPROVED_TIMER || 300000),
            };

            // Emit event to customer
            this.redisService.triggerEventBookBus({room: 'user_' + requestBook.customer_uuid, message: dataNotificationData});

            if (userFCM) {
                const token = userFCM.token ? userFCM.token : userFCM.apns_id;
                await this.firebaseService.pushNotificationWithTokens([token], notification, dataNotificationData);
            }
            return requestBook;
        }
        return false;
    }

    async updateBookShipping(status: number, book_shipping_reference: string) {
        const bookShipping = await this.bookShippingRepository.findOne({book_shipping_reference});
        const dataSave = { ...bookShipping, ...{status}};
        return await this.bookShippingRepository.save(dataSave);
    }

    async getDetail(id: number){
        const result = await this.bookShippingRepository.findOne(id, {
            relations: ['user', 'vehicleSchedule'],
        });
        if (result)
            return result;
        return false;
    }

    async cronCancleApproveBookShipping(book_shipping_reference: string) {
        let findBookBusReference;
        // Clear request book bus on redis
        let getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping);
        if (!getCache.isValidate) return false;
        const listRequestRedis = JSON.parse(getCache.value);
        findBookBusReference = _.find(listRequestRedis, {book_shipping_reference});
        if (!findBookBusReference) return false;
        _.remove(listRequestRedis, {book_shipping_reference});
        await this.redis.setInfoRedis(
            this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping, JSON.stringify(listRequestRedis));

        // Get device token
        const userFCM = await this.firebaseService.findOne(findBookBusReference.driver_uuid);
        if (userFCM){
            // Get info position by driver
            let rsPositionVehicle;
            const redisKey = await this.keyRedisVehiclePosition(VehicleGroup.BUS);
            const redisField = await this.fieldRedisVehiclePosition(VehicleGroup.BUS);
            let getCachePositionVehicles = await this.redis.getInfoRedis(redisKey, redisField);
            if (!getCachePositionVehicles.isValidate) return false;
            const listPositionVehicles = JSON.parse(getCachePositionVehicles.value);
            rsPositionVehicle = listPositionVehicles.find(item => item.user_uuid === userFCM.user_uuid);
            if (!rsPositionVehicle) return false;

            // Push notification
            let listDriverDeviceToken = [];
            const token = userFCM.token ? userFCM.token : userFCM.apns_id;
            listDriverDeviceToken.push(token);
            const notification = {
                title: 'Khách hàng đã hết thời gian thanh toán',
                body: 'Khách hàng chưa thanh toán',
            };
            const dataNotification = {
                key: KEY_CANCEL_APPROVE_REQUEST_BOOK_BUS_BY_CUSTOMER,
                book_shipping_reference,
                type_group: VehicleGroup.BUS,
                click_action: KEY_CLICK_ACTION,
            };
            // Emit event request book bus for customer
            this.redisService.triggerEventBookBus({room: 'user_' + userFCM.user_uuid, message: dataNotification});
            // Push notify
            await this.firebaseService.pushNotificationWithTokens(listDriverDeviceToken, notification,  dataNotification);
            return true;
        }
        return false;
    }

    async deleteBookShipping(book_shipping_reference: string, customrInfo: any) {
        const result = await this.bookShippingRepository.createQueryBuilder('book_shipping')
            .leftJoinAndSelect('book_shipping.vehicleSchedule', 'vehicleSchedule')
            .where('book_shipping.book_shipping_reference = :book_shipping_reference', {book_shipping_reference})
            .andWhere('book_shipping.user_id = :customer_id', {customer_id: customrInfo.id})
            .getOne();
        if (result) {
            const userFCM = await this.firebaseService.findOneByUserId(result.vehicleSchedule.user_id);
            result.deleted_at = new Date();
            result.status = 4; // Hủy chuyến
            const updateOrder =  await this.bookShippingRepository.save(result);
            if (updateOrder) {
                let findReference;
                // Remove request book shipping on redis
                const getCacheRequest = await this.redis.getInfoRedis(
                    this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping);
                if (getCacheRequest.isValidate) {
                    const listRequestCaching = JSON.parse(getCacheRequest.value);
                    findReference = _.find(listRequestCaching, item => {
                        return item.book_shipping_reference === book_shipping_reference;
                    });
                    _.remove(listRequestCaching, (item: any) => {
                        return item.book_shipping_reference === book_shipping_reference;
                    });
                    await this.redis.setInfoRedis(
                        this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping, JSON.stringify(listRequestCaching));
                }
                if (!findReference) throw {message: 'Not found request', status: 404};
                const notification = {
                    title: 'Khách hàng hủy chuyến',
                    body: 'Khách hàng đã hủy chuyến gửi hàng',
                };
                const dataNotificationData = {
                    key: KEY_CANCEL_APPROVE_REQUEST_BOOK_SHIPPING_BY_CUSTOMER,
                    book_shipping_reference,
                    click_action: KEY_CLICK_ACTION,
                    customerInfo: {
                        full_name: customrInfo.customerProfile.full_name,
                        phone: customrInfo.phone,
                    },
                    type_group: VehicleGroup.BUS,
                };
                // emit event to driver
                this.redisService.triggerEventBookBus({room: 'user_' + findReference.driver_uuid, message: dataNotificationData});

                if (userFCM) {
                    // Get list device token
                    let listDriverDeviceToken = [];
                    const token = userFCM.token ? userFCM.token : userFCM.apns_id;
                    listDriverDeviceToken.push(token);
                    await this.firebaseService.pushNotificationWithTokens(listDriverDeviceToken, notification,  dataNotificationData);
                }
                return true;
            }
        }
        return false;
    }

    async createUrlPayment(data: { customer_uuid: string, book_shipping_reference: string, vnp_IpAddr: string, vnp_BankCode: string }){
        let getRequestBook;
        // Get request by customer
        let getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping);
        if (getCache.isValidate) {
            const listRequestRedis = JSON.parse(getCache.value);
            getRequestBook = _.find(listRequestRedis, {customer_uuid: data.customer_uuid, book_shipping_reference: data.book_shipping_reference});
            if (!getRequestBook) {
                throw {message: 'Not found request book shipping', status: 404};  // Not found request on redis
            }
            const amount =  getRequestBook.price * 100; // Khử số thập phân, vn pay bắt buộc nhân 100
            const dataPost = {
                vnp_Amount: Number(amount),
                vnp_BankCode: 'NCB',
                vnp_OrderInfo: 'Thanh toan dat xe',
                vnp_OrderType: '240000',
                vnp_TxnRef: data.book_shipping_reference,
            };
            return await this.vnPayService.createPaymentUrl(dataPost, data.vnp_IpAddr);
        }
        return false;
    }

    async customerPaid(data: { customer_uuid: string, book_shipping_reference: string}){
        // Check payment status by book_bus_reference
        const checkTicketPaid = await this.bookShippingRepository.createQueryBuilder('ticket')
            .where('book_shipping_reference = :book_shipping_reference', {book_shipping_reference: data.book_shipping_reference})
            .andWhere('deleted_at is null')
            .getOne();
        if (checkTicketPaid) throw {message: 'Ticket is paid', status: StatusCode.CODE_CONFLICT};

        let listPositionVehicles, rsPositionVehicle, distanceDriverToStation, requestBook;
        let long_time = 0;

        // Update status request book bus by driver approve customer
        let getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping);
        if (!getCache.isValidate) throw {message: 'Not found request book bus', status: StatusCode.NOT_FOUND};
        const listRequestRedis = JSON.parse(getCache.value);
        requestBook = listRequestRedis.find(
            req => req.customer_uuid === data.customer_uuid && req.book_shipping_reference === data.book_shipping_reference);
        const indexRequest = listRequestRedis.findIndex(req => req.book_shipping_reference === data.book_shipping_reference);
        if (!requestBook) throw {message: 'Not found request book bus', status: StatusCode.NOT_FOUND};
        requestBook.payment_status = StatusPayment.PAID;
        listRequestRedis[indexRequest] = requestBook;
        await this.redis.setInfoRedis(
            this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping, JSON.stringify(listRequestRedis));

        // Get list vehicle schedule on redis
        const redisKey = await this.keyRedisVehiclePosition(VehicleGroup.BUS);
        const redisField = await this.fieldRedisVehiclePosition(VehicleGroup.BUS);
        let getCachePositionVehicles = await this.redis.getInfoRedis(redisKey, redisField);
        if (getCachePositionVehicles.isValidate) {
            listPositionVehicles = JSON.parse(getCachePositionVehicles.value);
            // Find vehicle schedule by driver uuid
            rsPositionVehicle = listPositionVehicles.find(item => item.user_uuid === requestBook.driver_uuid);
        }
        if (!rsPositionVehicle) throw {message: 'Not found vehicle!', status: StatusCode.NOT_FOUND};

        const userFCM = await this.firebaseService.findOne(data.customer_uuid);
        // Cancel cron
        this.schedule.cancelJob(KEY_CANCEL_APPROVED_BUS + data.book_shipping_reference);

        // Get distance, long time from vehicle position to station
        // distanceDriverToStation = await this.googleMapService.rawUrlGetDistance({
        //     origins: {lat: rsPositionVehicle.lat, lng: rsPositionVehicle.lng},
        //     destinations: {lat: requestBook.pickup_point.lat, lng: requestBook.pickup_point.lng},
        // });
        // if (!distanceDriverToStation) throw {message: 'Format station or pickup point incorrect', status: StatusCode.BAD_GATEWAY};
        // long_time = distanceDriverToStation.data.rows[0].elements[0].duration.text.match(/\d+/)[0];

        const area = await this.areaService.findById(requestBook.end_point);
        const dataInsert = {
            user_id: requestBook.user_id,
            schedule_id: requestBook.schedule_id,
            price: requestBook.price,
            address_ship: requestBook.address_ship,
            receiver: requestBook.receiver,
            phone: requestBook.phone,
            commodity_information: requestBook.commodity_information,
            book_shipping_reference: requestBook.book_shipping_reference,
            commodity_images: requestBook.commodity_images,
            destination: area.name,
            pickup_point: requestBook.pickup_point,
            ticket_code: new Date().getTime().toString(36),
            payment_method: StatusPaymentMethod.BANK,
            payment_status: StatusPayment.PAID,
        };
        const orderBookShipping = await this.bookShippingRepository.save(dataInsert);
        const ticketToUpdate = await this.bookShippingRepository.findOne(orderBookShipping.id);

        ticketToUpdate.qr_code = StringHelper.encodeBase64(JSON.stringify(
            {
                schedule_id: ticketToUpdate.schedule_id,
                user_uuid: requestBook.customer_uuid,
                ticket_id: ticketToUpdate.id,
                is_shipping: true,
            },
        ));
        const ticket = await this.bookShippingRepository.save(ticketToUpdate);
        if (ticket) {
            // send notification to customer paid
            const notification = {
                title: 'Chúc mừng bạn đã thanh toán thành công',
                body: 'Chúc mừng bạn đã thanh toán thành công',
            };
            const dataNotification = {
                key: KEY_NOTIFY_PAID_BOOK_BUS_TO_CUSTOMER,
                customer_user_uuid: data.customer_uuid,
                ticket_info: ticket,
                book_bus_reference: data.book_shipping_reference,
                click_action: KEY_CLICK_ACTION,
            };
            // emit event to customer
            this.redisService.triggerEventBookBus({room: 'user_' + data.customer_uuid, message: dataNotification});

            if (userFCM) {
                const token = userFCM.token ? userFCM.token : userFCM.apns_id;
                await this.firebaseService.pushNotificationWithTokens([token], notification,  dataNotification);
            }
            return requestBook;
        }
        return false;
    }

    async getBookShippingByCustomer(user_id: number){
        return await this.bookShippingRepository.createQueryBuilder('order')
            .leftJoinAndSelect('order.vehicleSchedule', 'vehicleSchedule')
            .leftJoinAndSelect('vehicleSchedule.route', 'route')
            .leftJoinAndSelect('vehicleSchedule.provider', 'provider')
            .leftJoinAndSelect('vehicleSchedule.vehicle', 'vehicle')
            .leftJoinAndSelect('vehicle.vehicleType', 'vehicleType')
            .leftJoinAndSelect('vehicleSchedule.user', 'user')
            .leftJoinAndSelect('user.users', 'users')
            .where('order.user_id = :user_id', {user_id})
            .andWhere('order.deleted_at is null')
            .orderBy('order.id', 'DESC')
            .getMany();
    }

    async getBookShippingBySchedule(schedule_id: number){
        const bookShipping = await this.bookShippingRepository.find({
            where: { schedule_id },
            order: { id: 'DESC'},
            relations: [
                'user',
                'user.customerProfile',
            ],
        });
        // let scheduleId = 0;
        // bookShipping.map( item => {
        //    scheduleId = item.schedule_id;
        // });
        const schedule = await this.scheduleService.getDetail(schedule_id);
        const data = {schedule, bookShipping};
        return data;
    }

    async getAllBookShipping(){
        return  await this.bookShippingRepository.find({
            order: { id: 'DESC'},
            relations: [
                'user',
                'user.customerProfile',
                'vehicleSchedule',
                'vehicleSchedule.provider',
            ],
            where: {
                deleted_at: null,
            },
        });
    }

    async requestBusAndPushNotify(data) {
        let rsPositionVehicle, checkSchedule;
        const book_shipping_reference = data.customer_uuid + '_' + Math.random().toString(36).slice(2) + SECRET_BOOK_SHIPPING;
        const userFCM = await this.firebaseService.findOne(data.driver_uuid);
        if (userFCM){
            const getCache = await this.redis.getInfoRedis(
                this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping);

            // Check exist request book bus by customer
            if (getCache.isValidate) {
                let listRequestCheck = JSON.parse(getCache.value);
                const checkExistRequest = _.findIndex(listRequestCheck, {customer_uuid: data.customer_uuid});
                if (checkExistRequest > -1) throw {message: 'Request existed', status: StatusCode.CODE_CONFLICT};
            }

            // Get caching vehicle position
            const redisKey = await this.keyRedisVehiclePosition(VehicleGroup.BUS);
            const redisField = await this.fieldRedisVehiclePosition(VehicleGroup.BUS);
            let getCachePositionVehicles = await this.redis.getInfoRedis(redisKey, redisField);
            if (!getCachePositionVehicles.isValidate) throw {message: 'Not found driver', status: StatusCode.NOT_FOUND};

            // Get vehicle schedule by driver
            const listPositionVehicles = JSON.parse(getCachePositionVehicles.value);
            rsPositionVehicle = listPositionVehicles.find(item => item.user_uuid === data.driver_uuid && item.schedule_id === data.schedule_id);
            if (!rsPositionVehicle) throw {message: 'Not found driver or schedule', status: StatusCode.NOT_FOUND};

            checkSchedule = await this.vehicleMovingService.findOne(rsPositionVehicle.schedule_id);
            if (!checkSchedule) throw {message: 'Vehicle schedule not found', status: StatusCode.NOT_FOUND}; // Schedule not found

            // Add data for request book
            data.license_plates = rsPositionVehicle.license_plates;
            data.provider_name = rsPositionVehicle.provider_name;
            data.logo = rsPositionVehicle.logo;
            data.start_date = checkSchedule.vehicleSchedule.start_date;
            data.end_point_name = checkSchedule.vehicleSchedule.route.endPoint.name;
            data.end_point_coordinates = checkSchedule.vehicleSchedule.route.endPoint.coordinates;
            data.status = StatusRequestBookBus.REQUEST_NEW;
            data.book_shipping_reference = book_shipping_reference;

            // Add request on redis
            if (getCache.isValidate) {
                const listRequest = JSON.parse(getCache.value);
                const index = _.findIndex(listRequest, {book_shipping_reference});
                if (index > -1) {
                    throw {message: 'Request existed', status: StatusCode.CODE_CONFLICT};
                } else {
                    listRequest.push(data);
                    await this.redis.setInfoRedis(
                        this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping,
                        JSON.stringify(listRequest));
                }
            } else {
                await this.redis.setInfoRedis(
                    this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping,
                    JSON.stringify([data]) );
            }

            // Add cron job, remove request when expise timer
            await this.schedule.scheduleIntervalJob(
                KEY_CANCEL_FIND_BUS + book_shipping_reference,
                Number(process.env.REQUEST_BOOK_BUS_TIMER) || 120000,
                () => this.cancelFindDrivers(book_shipping_reference));

            const notification = {
                title: 'Yêu cầu gửi hàng',
                body: 'Khách hàng cần gửi hàng',
            };
            const dataNotificationData = {
                key: KEY_CUSTOMER_SEND_REQUEST_BOOK_SHIPPING_TO_DRIVER,
                route_info: data,
                book_shipping_reference,
                click_action: KEY_CLICK_ACTION,

            };
            // emit event to driver
            this.redisService.triggerEventBookBus({room: 'user_' + data.driver_uuid, message: dataNotificationData});

            // Push notify
            const token = userFCM.token ? userFCM.token : userFCM.apns_id;
            await this.firebaseService.pushNotificationWithTokens([token], notification,  dataNotificationData);
            return data;
        }
        return false;
    }
    //
    // async customerPaid(data: any, customer_uuid: string){
    //     const userFCM = await this.firebaseService.findOne(customer_uuid);
    //     const bookShipping = await this.findByBookShippinpReference(data.book_shipping_reference);
    //     let requestBook;
    //     // check request book shipping
    //     let getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping);
    //     if (getCache.isValidate) {
    //         const listRequestRedis = JSON.parse(getCache.value);
    //         requestBook = listRequestRedis.find(
    //             req => req.customer_uuid === customer_uuid && req.book_shipping_reference === data.book_shipping_reference);
    //         if (!requestBook) {
    //             // Not found request on redis
    //             throw {message: 'Not found request book bus', status: 404};
    //         }
    //     }
    //
    //     if (userFCM && bookShipping) {
    //         const dataUpdateBS = {...bookShipping, ...{payment_status: data.payment_status, payment_method: data.payment_method}};
    //         const updateBS = await this.bookShippingRepository.save(dataUpdateBS);
    //         if (updateBS){
    //             let listDriverDeviceToken = [];
    //             const token = userFCM.token ? userFCM.token : userFCM.apns_id;
    //             listDriverDeviceToken.push(token);
    //             // send notification to customer paid
    //             const notification = {
    //                 title: 'Chúc mừng bạn đã thanh toán thành công',
    //                 body: 'Chúc mừng bạn đã thanh toán thành công',
    //             };
    //             const dataNotification = {
    //                 customer_user_uuid: data.customer_uuid,
    //                 bookShippingInfo: await this.bookShippingRepository.findOne(data.book_shipping_id),
    //                 click_action: 'FLUTTER_NOTIFICATION_CLICK',
    //                 key: 'customer_paid_order_shipping',
    //             };
    //             await this.firebaseService.pushNotificationWithTokens(listDriverDeviceToken, notification,  dataNotification);
    //             return requestBook;
    //         }
    //     }
    //     return false;
    // }

    async countBookShippingAndSumPrice(schedule_id: number){
        return await this.bookShippingRepository.createQueryBuilder('book')
            .where('book.schedule_id = :schedule_id', {schedule_id} )
            .select('COUNT(book.id) as count, SUM(book.price) as total_price')
            .getRawOne();
    }

    async findByBookShippinpReference(book_shipping_reference: string) {
        return await this.bookShippingRepository.createQueryBuilder('book')
            .where('book.book_shipping_reference = :book_shipping_reference', {book_shipping_reference} )
            .getOne();
    }
    async fieldRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.fieldRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.fieldRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.fieldRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.fieldRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.fieldRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    async keyRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.keyRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.keyRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.keyRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.keyRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.keyRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    async deleteAllBookShippingBySchedule(schedule_id: number) {
        return await this.bookShippingRepository.createQueryBuilder('order')
            .update(BookShippingEntity)
            .set({ deleted_at: new Date()})
            .where('schedule_id = :schedule_id', {schedule_id})
            .execute();
    }

    async cancelFindDrivers(book_shipping_reference: string) {
        let findBookBusReference, userFCM;
        const getCacheOrder = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping);
        if (getCacheOrder.isValidate) {
            const getListOrder = JSON.parse(getCacheOrder.value);
            findBookBusReference = _.find(getListOrder, {book_shipping_reference});
            if (!findBookBusReference) return true;
            // remove order by customer uuid
            _.remove(getListOrder, {book_shipping_reference});
            await this.redis.setInfoRedis(
                this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping, JSON.stringify(getListOrder));
            const notification = {
                title: 'Khách hàng hủy tìm xe',
                body: 'Khách hàng đã hủy tìm xe',
            };
            const dataNotification = {
                key: KEY_CANCEL_FIND_BOOK_SHIPPING_BY_CUSTOMER,
                customerInfo: {
                    customer_uuid: findBookBusReference.customer_uuid.toString(),
                    phone: findBookBusReference.phone,
                },
                type_group: VehicleGroup.BUS,
                click_action: KEY_CLICK_ACTION,
            };
            // emit event to driver
            this.redisService.triggerEventBookBus({room: 'user_' + findBookBusReference.driver_uuid, message: dataNotification});

            //  Push notify fo driver
            userFCM = await this.firebaseService.findOne(findBookBusReference.driver_uuid);
            if (userFCM) {
                const token = userFCM.token ? userFCM.token : userFCM.apns_id;
                await this.firebaseService.pushNotificationWithTokens([token], notification,  dataNotification);
            }
        }
        return  true;
    }
}
