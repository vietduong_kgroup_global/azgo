import {forwardRef, Module} from '@nestjs/common';
import { BookShippingController } from './book-shipping.controller';
import { BookShippingService } from './book-shipping.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {BookShippingRepository} from './book-shipping.repository';
import {UsersModule} from '../users/users.module';
import {VehicleScheduleModule} from '../vehicle-schedule/vehicleSchedule.module';
import {CommonService} from '../../helpers/common.service';
import {FirebaseModule} from '../firebase/firebase.module';
import {RatingScheduleModule} from '../rating-schedule/rating-schedule.module';
import {AreaModule} from '../area/area.module';
import {VehicleMovingModule} from '../vehicle-moving/vehicleMoving.module';
import {GoogleMapService} from '../google-map/google-map.service';
import {VnPayModule} from '../vn-pay/vn-pay.module';

@Module({
  imports: [
      TypeOrmModule.forFeature([
        BookShippingRepository,
      ]),
      UsersModule, FirebaseModule,
      forwardRef(() => RatingScheduleModule),
      forwardRef(() => VehicleScheduleModule),
      AreaModule,
      forwardRef(() => VehicleMovingModule),
      VnPayModule,
  ],
  controllers: [BookShippingController],
  providers: [BookShippingService, CommonService, GoogleMapService],
  exports: [BookShippingService],
})
export class BookShippingModule {}
