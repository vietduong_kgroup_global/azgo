import {forwardRef, Inject, Injectable} from '@nestjs/common';
import { UserProfileEntity } from './user-profile.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { UserProfileRepository } from './user-profile.repository';
import {VehicleService} from '../vehicle/vehicle.service';

@Injectable()
export class UserProfileService {
    constructor(
        @InjectRepository(UserProfileRepository)
        private readonly userProfileRepository: UserProfileRepository,
        @Inject(forwardRef(() => VehicleService))
        private readonly vehicleService: VehicleService,
    ) { }

    async createUserProfile(data: any, transactionalEntityManager: any) {
        try { 
            let data_user_profile = {
                first_name: data.first_name || '',
                last_name: data.last_name || '',
                full_name: data.full_name || '',
                gender: [0, 1, 2].includes(data.gender) ? data.gender : 0,
                birthday: data.birthday || '',
                address: data.address || '',
                cmnd: data.cmnd || '',
                image_profile: data.image_profile || {},
                user_id: data.user_id || '',
                // users: data.users,
            }; 
            let data_merge = await this.userProfileRepository.merge(new UserProfileEntity(), data_user_profile); 
            await transactionalEntityManager.save(UserProfileEntity, data_merge); 
            return true;
        } catch (error) {
            console.log('Error createUserProfile ', error);
            return false;
        }
    } 
    /**
     * This is function get all user
     * @param options
     */
    async getAll(options?: any) {
        return await this.userProfileRepository.find({
            relations: ['users'],
        });
    }

    /**
     * This is function find by id
     * @param id
     */
    async findById(id: number) {
        return await this.userProfileRepository.findOne(id);
    }

    /**
     * This is function get user profile by user_id
     * @param user_id
     */
    // async findByUserId(user_id: number): Promise<UserProfileEntity> {
    //     return await this.userProfileRepository.findByUserId(user_id);
    // }

}
