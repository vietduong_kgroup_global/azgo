import {forwardRef, Module} from '@nestjs/common';
import {UserProfileController} from './user-profile.controller';
import {TypeOrmModule} from '@nestjs/typeorm';
import {UserProfileRepository} from './user-profile.repository';
import {UserProfileService} from './user-profile.service';
import {VehicleModule} from '../vehicle/vehicle.module';
import {ProvidersInfoModule} from '../providers-info/providers-info.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([UserProfileRepository]),
        forwardRef(() => VehicleModule), // B/c Circular dependency
        ProvidersInfoModule,
    ],
    exports: [
        UserProfileService,
    ],
    controllers: [UserProfileController],
    providers: [UserProfileService],
})
export class UserProfileModule {
}
