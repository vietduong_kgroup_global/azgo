import { EntityRepository, Repository } from 'typeorm';
import { UserProfileEntity } from './user-profile.entity';

@EntityRepository(UserProfileEntity)
export class UserProfileRepository extends Repository<UserProfileEntity> {
    /**
     * This is function find by user id
     * @param user_id
     */
    // async findByUserId(user_id: number): Promise<UserProfileEntity> {
    //     return await this.createQueryBuilder('az_driver_profile')
    //         .where('az_driver_profile.user_id = :user_id', { user_id })
    //         .leftJoinAndSelect('az_driver_profile.users', 'users')
    //         .getOne();
    // }

}
