import {Controller, Get, HttpStatus, Param, Req, Res, UseGuards} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import {UserProfileService} from './user-profile.service';
import {AuthGuard} from '@nestjs/passport';
import { Response } from 'express';
import {Roles} from '@azgo-module/core/dist/common/decorators/roles.decorator';
import {ActionKey, FunctionKey} from '@azgo-module/core/dist/common/guards/roles.guards';
import { dataSuccess, dataError} from '@azgo-module/core/dist/utils/jsonFormat';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import {ProvidersInfoService} from '../providers-info/providers-info.service';

@ApiUseTags('User')
@Controller('user')
export class UserProfileController {
    constructor(
        private readonly userProfileService: UserProfileService,
        private readonly providersInfoService: ProvidersInfoService,
    ) { } 
  
}
