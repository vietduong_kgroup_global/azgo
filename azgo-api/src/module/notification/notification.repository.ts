import { Brackets, EntityRepository, Repository } from 'typeorm';
import { NotificationEntity } from './notification.entity';

@EntityRepository(NotificationEntity)
export class NotificationRepository extends Repository<NotificationEntity> {
    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     * @param options
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        try {
            let query = this.createQueryBuilder('az_notifications');
            query.leftJoinAndSelect('az_notifications.userProfile', 'userProfile');
            query.leftJoinAndSelect('userProfile.customerProfile', 'customerProfile');
            query.leftJoinAndSelect('userProfile.driverProfile', 'driverProfile');
            query.leftJoinAndSelect('az_notifications.userActionProfile', 'userActionProfile');
            query.leftJoinAndSelect('userActionProfile.customerProfile', 'customerAction');
            query.leftJoinAndSelect('userActionProfile.driverProfile', 'driverAction');
            // not delete_at
            query.andWhere('az_notifications.deleted_at is null');
            query.andWhere(`az_notifications.user_id = '${options.user_id}'`);

            // Filter by date create
            if (!!options.from_date && !!options.to_date) {
                query.andWhere(`az_notifications.created_at >= :from_date`, { from_date: options.from_date });
                query.andWhere(`az_notifications.created_at <= :to_date`, { to_date: options.to_date });
            }
            // limit + offset
            query.orderBy('az_notifications.id', 'DESC')
                .limit(limit)
                .offset(offset);

            const results = await query.getManyAndCount();

            return {
                success: true,
                data: results[0],
                total: results[1],
            };
        } catch (error) {
            console.log('error - getAllByOffsetLimit Notification: ', error);
            return {
                success: false,
                data: [],
                total: 0,
            };
        }
    }

    /**
     * This is function get tour by id
     * @param id
     */
    async findByUuid(uuid: string) {
        return await this.findOne({
            where: { uuid, deleted_at: null },
            relations: [
                'userProfile',
                'userActionProfile'
            ],
        });
    }

    /**
     * This is function get notification by uuid and user_id
     * @param id
     */
    async findByUuidAndUserId(uuid: string, user_id: number) {
        return await this.findOne({
            where: { uuid, user_id, deleted_at: null },
            relations: [
                'userProfile',
            ],
        });
    }

    /**
     * This is function set all notification to read
     * @param id
     */
    async updateReadForToken(user_id: number, data: any) {
        try {
            let result = await this.update({ user_id }, data);
            let list = await this.find({ user_id });
            return {
                success: true,
                data: list,
            };
        } catch (error) {
            console.log('error - updateReadForToken: ', error);
            return {
                success: false,
                error,
            };
        }
    }

    /**
     * This is function delete notification
     * @param notification
     */
    // async deleteNotification(notification: NotificationEntity): Promise<boolean> {
    //     notification.deleted_at = new Date();
    //     await this.update(notification.id, notification);
    //     return true;
    // }

}
