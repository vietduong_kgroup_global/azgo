
import { Entity, PrimaryGeneratedColumn, Generated, JoinColumn, OneToOne, Column, BaseEntity } from 'typeorm';
import { UsersEntity } from '../users/users.entity';
import { TourOrderEntity } from '../tour-order/tour-order.entity';
import { Exclude } from 'class-transformer';
import { IsInt, IsNotEmpty } from 'class-validator';

@Entity('az_notifications')
export class NotificationEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    @Generated('uuid')
    @IsNotEmpty()
    uuid: string;

    @Column({
        type: 'int',
        nullable: false,
    })
    @IsNotEmpty()
    user_id: number;

    @OneToOne(type => UsersEntity, user => user.notification)
    @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
    userProfile: UsersEntity;

    @Column({
        type: 'int',
        nullable: false,
    })
    user_action_id: number;

    @OneToOne(type => UsersEntity, user => user.notification)
    @JoinColumn({ name: 'user_action_id', referencedColumnName: 'id' })
    userActionProfile: UsersEntity;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    user_action_role: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    title: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    message: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    object_id: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    type_object: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    type: string;

    @Column({
        type: 'smallint',
        nullable: true,
        default: 0,
    })
    is_read: number;

    @Column({
        type: 'json',
        comment: 'Thông tin bị thay đổi',
    })
    data: object;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;
}