import { Body, Controller, Get, HttpStatus, Param, Post, Put, Delete, Req, Res, UseGuards } from '@nestjs/common';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { AuthGuard } from '@nestjs/passport';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiResponse,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiUseTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import * as jwt from 'jsonwebtoken';
import { NotificationService } from './notification.service';

@ApiUseTags('Notification')
@Controller('notification')
export class NotificationController {
    constructor(
        private readonly notificationService: NotificationService,
    ) {
    }
    @Get('/get-list-by-token')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get for user by token' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'from_date', description: 'Filter Date From', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'to_date', description: 'Filter Date To', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    async getListForUser(@Req() request, @Res() res: Response) {
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.notificationService.getAllByOffsetLimit(per_page, offset, { ...request.query, user_id: data_user.id });
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).
                send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
        // res.status(HttpStatus.OK).send(query);
    }

    @Get('/count-unread')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Count notification unread' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async countUnRead(@Req() request, @Res() res: Response) {
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        try {
            const result = await this.notificationService.count({ user_id: data_user.id, is_read: 0, deleted_at: null });
            if (!result.error)
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            return res.status(HttpStatus.NOT_FOUND).send(dataError(result.error || result.error.message, null));
        } catch (error) {
            console.log('countUnRead controller: ', error);
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/:notification_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Detail Notification' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'notification_uuid', description: 'Notification Uuid', required: true, type: 'string' })
    async detail(@Param('notification_uuid') notification_uuid, @Res() res: Response) {
        try {
            const notification = await this.notificationService.findByUuid(notification_uuid);
            if (!notification)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy thông báo', null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', notification));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/check-read/:notification_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Check notification is read - by token' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'notification_uuid', description: 'Order Uuid', required: true, type: 'string' })
    async updateRead(@Param('notification_uuid') notification_uuid, @Req() request, @Res() res: Response) {
        let notification;
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        try {
            notification = await this.notificationService.findByUuidAndUserId(notification_uuid, data_user.id);
            if (!notification)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy thông báo này của user', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
        try {
            let data = {
                is_read: 1,
            };
            const result = await this.notificationService.updateById(notification.id, data);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Chỉnh sửa đơn hàng bị lỗi', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/check-read-all')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Check all notification is read - by token' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'notification_uuid', description: 'Order Uuid', required: true, type: 'string' })
    async updateReadAll(@Req() request, @Res() res: Response) {
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        try {
            let data = {
                is_read: 1,
            };
            const result = await this.notificationService.updateReadForToken(data_user.id, data);
            if (!result.error) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.error.message || 'Cập nhật thông báo bị lỗi', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:notification_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete notification' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'notification_uuid', description: 'Notification Uuid', required: true, type: 'string' })
    async deleteUserAccount(@Param('notification_uuid') notification_uuid, @Req() request, @Res() res: Response) {
        let notification;
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        try {
            notification = await this.notificationService.findByUuidAndUserId(notification_uuid, data_user.id);
            if (!notification)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy notification', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
        try {
            let result = await this.notificationService.deleteNotification(notification.id);
            if (!result.error) {
                return res.status(HttpStatus.OK).send(dataSuccess('Xóa notification thành công', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.error.message || 'Xóa thông báo bị lỗi', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
