import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityId } from 'typeorm/repository/EntityId';
import { NotificationEntity } from './notification.entity';
import { NotificationRepository } from './notification.repository';

@Injectable()
export class NotificationService {
    constructor(
        @InjectRepository(NotificationRepository)
        private readonly repository: NotificationRepository,
    ) {
    }

    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        return await this.repository.getAllByOffsetLimit(limit, offset, options);
    }

    async count(options: any) {
        try {
            let count = await this.repository.count(options);
            return {
                success: true,
                data: count,
            };
        } catch (error) {
            console.log('error - count unread notification: ', error);
            return {
                success: true,
                error,
            };
        }
    }

    /**
     * This function get notification by uuid
     * @param tour_id
     */
    async findByUuid(uuid: string) {
        return await this.repository.findByUuid(uuid);
    }

    /**
     * This function get notification by uuid and user_id
     * @param tour_id
     */
    async findByUuidAndUserId(uuid: string, user_id: number) {
        return await this.repository.findByUuidAndUserId(uuid, user_id);
    }

    async save(data: any) {
        return await this.repository.save(data);
    }

    async updateById(id: EntityId, data: any) {
        await this.repository.update(id, data);
        return await this.repository.findOne(id);
    }

    async updateReadForToken(user_id: number, data: any) {
        return await this.repository.updateReadForToken(user_id, data);
    }

    /**
     * This is function delete notification
     * @param notification
     */
    async deleteNotification(notification_id: number) {
        try {
            let data = {
                deleted_at: new Date(),
            }
            await this.repository.update(notification_id, data);
            return {
                success: true,
            };
        } catch (error) {
            console.log('error - delete notification: ', error);
            return {
                success: true,
                error,
            };
        }
    }
}
