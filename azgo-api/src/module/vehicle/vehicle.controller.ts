import {
    Controller,
    Get,
    HttpStatus,
    Param,
    UseGuards,
    Post,
    Put,
    Req,
    Res,
    Body,
    Delete,
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiImplicitQuery,
    ApiUseTags,
    ApiImplicitParam,
    ApiOperation,
    ApiResponse,
} from '@nestjs/swagger';
import {VehicleService} from './vehicle.service';
import {AuthGuard} from '@nestjs/passport';
import {Roles} from '@azgo-module/core/dist/common/decorators/roles.decorator';
import {ActionKey, FunctionKey} from '@azgo-module/core/dist/common/guards/roles.guards';
import {CustomValidationPipe} from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import {CreateVehicleDto} from './dto/create-vehicle.dto';
import {CreateVehicleBus} from './dto/create-vehicle-bus';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { Response } from 'express';
import {VehicleGroup, UserType} from '@azgo-module/core/dist/utils/enum.ultis';
import {ProvidersInfoService} from '../providers-info/providers-info.service';

@ApiUseTags('Vehicle')
@Controller('vehicle')

export class VehicleController{
    constructor(
        private readonly service: VehicleService,
        private readonly providersInfoService: ProvidersInfoService,
    ) { }

    /**
     * Get All vehicle by provider and vehicle type
     * Return number
     * @param res
     * @param request
     */
    @Get('/bus')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all vehicle by bus' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'license_plates', description: 'License plates', required: false, type: 'string'})
    @ApiImplicitQuery({name: 'provider_id', description: 'Provider ID', required: false, type: 'number'})
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    async getAllBus(@Res() res: Response, @Req() request) {
        // get provider by user
        if (request.user.type === UserType.PROVIDER) {
            try {
                const provider = await this.providersInfoService.findByUserId(request.user.id);
                if (!provider) return res.status(HttpStatus.BAD_REQUEST).send(dataError('Provider not found', null));
                request.query.provider_id = provider.id;
            } catch (e) {
                console.error('[getAllBus] get provider: ', e);
                return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Bad requests', null));
            }
        }

        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.service.findByVehicleGroup(VehicleGroup.BUS, per_page, offset, request.query);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    /**
     * Get All vehicle by vehicle type
     * Return number
     * @param res
     * @param request
     */
    @Get('/vehicle')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all vehicle' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'keyword', description: 'Filter license plates, vehicle type name, provider name', required: false, type: 'string'})
    async getAllVehicle(@Res() res: Response, @Req() request) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            let vehicleGroup = Object.keys(VehicleGroup)
            results = await this.service.findByVehicleGroup([...vehicleGroup.map(item => item)], per_page, offset, request.query);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    /**
     * Get All vehicle by provider and vehicle type
     * Return number
     * @param res
     * @param request
     */
    @Get('/bike-cars')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all vehicle by bike/car' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'keyword', description: 'Filter license plates, vehicle type name, provider name', required: false, type: 'string'})
    async getAllBikeCars(@Res() res: Response, @Req() request) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.service.findByVehicleGroup([VehicleGroup.BIKE, VehicleGroup.CAR], per_page, offset, request.query);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    /**
     * Get All vehicle by provider and vehicle type
     * Return number
     * @param res
     * @param request
     */
    @Get('/van-ba-gac')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all vehicle by VAN/BAGAC' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'license_plates', description: 'License plates', required: false, type: 'string'})
    async getAllVanBaGac(@Res() res: Response, @Req() request) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.service.findByVehicleGroup([VehicleGroup.VAN, VehicleGroup.BAGAC], per_page, offset, request.query);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    @Post('/bus')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create vehicle bus' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createBus(@Req() request, @Body(CustomValidationPipe) body: CreateVehicleBus, @Res() res: Response) {
        try {
            let vehicle = await this.service.createBus(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', vehicle));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    /**
     * Create vehicle
     * Return number
     * @param body
     * @param res
     * @param request
     */

    @Post('/vehicle')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create Vehicle' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createVehicle(@Req() request, @Body(CustomValidationPipe) body: CreateVehicleDto, @Res() res: Response) {
        try {
            let vehicle = await this.service.createVehicle(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', vehicle));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create bike car' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createBikeCar(@Req() request, @Body(CustomValidationPipe) body: CreateVehicleDto, @Res() res: Response) {
        try {

            let vehicle = await this.service.createVehicle(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', vehicle));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/van-bagac')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create Van Bagac' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createVanBagac(@Req() request, @Body(CustomValidationPipe) body: CreateVehicleDto, @Res() res: Response) {
        try {
            let vehicle = await this.service.createVehicle(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', vehicle));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/bus/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update Bus' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'string' })
    async updateBus(@Param('id') id, @Req() request, @Body(CustomValidationPipe) body: CreateVehicleBus, @Res() res: Response) {
        try {
            let vehicle = await this.service.updateBus(id, body);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', vehicle));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    /**
     * Update vehicle
     * @param id
     * @param body
     * @param res
     * @param request
     */

    @Put('/vehicle/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update vehicle' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'string' })
    async updateVehicle(@Param('id') id, @Req() request, @Body(CustomValidationPipe) body: CreateVehicleDto, @Res() res: Response) {
        try {
            let vehicleGroup = Object.keys(VehicleGroup)
            let vehicle = await this.service.updateVehicle(id, body, [...vehicleGroup.map(item => item)]);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', vehicle));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/bike-car/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update bike car' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'string' })
    async updateBikeCar(@Param('id') id, @Req() request, @Body(CustomValidationPipe) body: CreateVehicleDto, @Res() res: Response) {
        try {
            let vehicle = await this.service.updateVehicle(id, body, [VehicleGroup.BIKE, VehicleGroup.CAR]);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', vehicle));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/van-bagac/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update Van Bagac' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'string' })
    async updateVanBagac(@Param('id') id, @Req() request, @Body(CustomValidationPipe) body: CreateVehicleDto, @Res() res: Response) {
        try {
            let vehicle = await this.service.updateVehicle(id, body, [VehicleGroup.VAN, VehicleGroup.BAGAC]);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', vehicle));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Detail Vehicle' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiResponse({ status: 500, description: 'Server Error' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'number' })
    async detail(@Param('id') id, @Req() req, @Res() res: Response) {
        try {
            const result = await this.service.findById(id, req.user);
            if (!result) return res.status(HttpStatus.NOT_FOUND).send(dataError('Vehicle not found', null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    /**
     * Get All vehicle by provider and vehicle type
     * Return number
     * @param provider_id
     * @param vehicle_type_id
     * @param res
     * @param req
     */
    @Get('/:provider_id/:vehicle_type_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all vehicle by provider and vehicle type' })
    // @Roles({function_key: FunctionKey.All, action_key: ActionKey.All})
    @ApiImplicitQuery({name: 'license_plates', description: 'License plates', required: false, type: 'string'})
    async getAllByProviderAndVehicleType(
        @Param('provider_id') provider_id: number,
        @Param('vehicle_type_id') vehicle_type_id: number,
        @Res() res: Response,
        @Req() req,
    ) {
        // get provider by user
        if (req.user.type === UserType.PROVIDER) {
            try {
                const provider = await this.providersInfoService.findByUserId(req.user.id);
                if (!provider) return res.status(HttpStatus.BAD_REQUEST).send(dataError('Provider not found', null));
                provider_id = provider.id;
            } catch (e) {
                console.error('[findAllDriverByProvider] get provider: ', e);
                return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Bad requests', null));
            }
        }
        try {
            const vehicles = await this.service.findByProviderAndVehicleType(provider_id, vehicle_type_id, req.query);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', vehicles));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete vehicle' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'number' })
    async deleteVehicle(@Param('id') id, @Req() request, @Res() res: Response) {
        try {
            let result = await this.service.delete(id);
            return res.status(HttpStatus.OK).send(dataSuccess('Deleted vehicle success', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
