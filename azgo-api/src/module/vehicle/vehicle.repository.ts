import {EntityRepository, Repository} from 'typeorm';
import {VehicleEntity} from './vehicle.entity';

@EntityRepository(VehicleEntity)
export class VehicleRepository extends Repository<VehicleEntity> {

    async getAll(): Promise<VehicleEntity []> {
        const vehicles = await this.find();
        return vehicles;
    }

    async createAndSave(vehicle: VehicleEntity): Promise<number> {
        await this.save(vehicle);
        return vehicle.id;
    }

    async findOneVehical(id: number): Promise<VehicleEntity> {
        const vehical = await this.findOne(id);
        return vehical;
    }

    async updateVehical(id: number, vehical: VehicleEntity): Promise<number> {

        await this.manager.update(VehicleEntity, id, vehical);
        return id;
    }

    async deleteVehical(vehical: number | VehicleEntity) {
        if (typeof vehical !== 'number'
            && !VehicleRepository.isVehical(vehical)) {
            throw new Error('vehical object not a VehicleEntity');
        }
        await this.manager.delete(VehicleEntity,
            typeof vehical === 'number'
                ? vehical : vehical.id);
    }

    static isVehical(vehical: any): vehical is VehicleEntity {
        return typeof vehical === 'object'
            && typeof vehical.license_plates === 'string'
            && typeof vehical.start_point === 'string'
            && typeof vehical.end_point === 'string';
    }

    /**
     * This is function get all vehicle by Provider and Vehicle Type
     * @param provider_id
     * @param vehicle_type_id
     */
    async getAllByProviderAndVehicleType(provider_id: number, vehicle_type_id: number) {
        return await this.createQueryBuilder('az_vehicle')
            .leftJoin('az_vehicle.providerInfo', 'providerInfo')
            .leftJoin('az_vehicle.vehicleType', 'vehicleType')
            .getMany();
    }
}
