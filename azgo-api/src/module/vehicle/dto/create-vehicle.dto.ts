import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class CreateVehicleDto {
    @IsNotEmpty({ message: 'Vehicle type is require' })
    @ApiModelProperty({ example: 1, required: true  })
    vehicle_type_id: number;

    @IsNotEmpty({ message: 'Vehicle brand is require' })
    @ApiModelProperty({ example: 1, required: true  })
    vehicle_brand_id: number;

    @IsNotEmpty({ message: 'License plates is require' })
    @ApiModelProperty({ example: '51C-7777', required: true  })
    license_plates: string;

    @IsNotEmpty({ message: 'Driver id is require' })
    @ApiModelProperty({ example: 1, required: true  })
    driver_id: number;

    @ApiModelProperty({ example: {file_path: 'assets/images', file_name: '1583745101578.jpg'}, required: false  })
    front_registration_certificates: object;

    @ApiModelProperty({ example: {file_path: 'assets/images', file_name: '1583745101578.jpg'}, required: false  })
    back_registration_certificates: object;

    @ApiModelProperty({ example: {file_path: 'assets/images', file_name: '1583745101578.jpg'}, required: false  })
    front_car_insurance: object;

    @ApiModelProperty({ example: {file_path: 'assets/images', file_name: '1583745101578.jpg'}, required: false  })
    back_car_insurance: object;

    @ApiModelProperty({ example: [{file_path: 'assets/images', file_name: '1583745101578.jpg'}], required: false  })
    images: object[];

}
