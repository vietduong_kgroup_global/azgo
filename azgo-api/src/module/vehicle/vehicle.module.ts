import {forwardRef, Module} from '@nestjs/common';
import { VehicleService } from './vehicle.service';
import { VehicleController } from './vehicle.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import {VehicleTypesModule} from '../vehicle-types/vehicle-types.module';
import {DriverProfileModule} from '../driver-profile/driver-profile.module';
import {VehicleBrandModule} from '../vehicle-brand/vehicle-brand.module';
import {VehicleDriverModule} from '../vehicle-driver/vehicle-driver.module';
import {SeatPatternModule} from '../seat-pattern/seat-pattern.module';
import {ProvidersInfoModule} from '../providers-info/providers-info.module';
import {VehicleRepository} from './vehicle.repository';

@Module({
  imports: [
      TypeOrmModule.forFeature([VehicleRepository]),
      forwardRef(() => DriverProfileModule), // B/c Circular dependency
      VehicleBrandModule,
      VehicleDriverModule,
      SeatPatternModule,
      ProvidersInfoModule,
      VehicleTypesModule,
  ],
  exports: [
      VehicleService,
  ],
  providers: [VehicleService],
  controllers: [VehicleController],
})
export class VehicleModule {}
