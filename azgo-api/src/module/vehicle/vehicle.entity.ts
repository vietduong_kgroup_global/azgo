import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToMany,
    JoinTable,
    OneToMany,
    OneToOne, JoinColumn, ManyToOne,
} from 'typeorm';
import {Exclude} from 'class-transformer';
import {UsersEntity} from '../users/users.entity';
import {ApiModelProperty} from '@nestjs/swagger';
import {ProvidersInfoEntity} from '../providers-info/providers-info.entity';
import {VehicleTypesEntity} from '../vehicle-types/vehicle-types.entity';
import {IsInt, IsNotEmpty, IsNumber, MaxLength} from 'class-validator';
import {VehicleBrandEntity} from '../vehicle-brand/vehicle-brand.entity';
import {VehicleScheduleEntity} from '../vehicle-schedule/vehicleSchedule.entity';
import {SeatPatternEntity} from '../seat-pattern/seat-pattern.entity';
import {VehicleDriverEntity} from '../vehicle-driver/vehicle-driver.entity';
import {BikeCarOrderEntity} from '../bike-car-order/bikeCarOrder.entity';
import {VanBagacOrdersEntity} from '../van-bagac-orders/van-bagac-orders.entity';

@Entity('az_vehicle')
export class VehicleEntity {
    @Exclude()
    @PrimaryGeneratedColumn({ type: 'bigint', unsigned: true })
    id: number;

    @Column('int')
    @IsInt({message: 'Provider must be a integer'})
    @ApiModelProperty({ example: 1, required: true  })
    provider_id: number;

    @ManyToOne(type => ProvidersInfoEntity, provider => provider.vehicles)
    @JoinColumn({name: 'provider_id'})
    @ApiModelProperty({ example: 'provider'})
    provider: ProvidersInfoEntity;

    @Column('int')
    @IsInt({message: 'Vehicle type must be a integer'})
    @ApiModelProperty({ example: 1, required: true  })
    vehicle_type_id: number;

    @OneToMany(type => VehicleScheduleEntity, vehicleScheduleEntity => vehicleScheduleEntity.vehicle)
    @ApiModelProperty({ example: 'vehicleSchedules'})
    vehicleSchedules: VehicleScheduleEntity[];

    @OneToOne(type => VehicleTypesEntity, vehicleType => vehicleType.id)
    @JoinColumn({name: 'vehicle_type_id', referencedColumnName: 'id'})
    @ApiModelProperty({ example: 'vehicleType'})
    vehicleType: VehicleTypesEntity;

    @ManyToOne(type => VehicleBrandEntity, vehicle_brand => vehicle_brand.vehicles)
    @JoinColumn({
        name: 'vehicle_brand_id',
        referencedColumnName: 'id',
    })
    vehicleBrand: VehicleBrandEntity;

    @ManyToOne(type => SeatPatternEntity, seat_pattern => seat_pattern.vehicles)
    @JoinColumn({
        name: 'seat_pattern_id',
        referencedColumnName: 'id',
    })
    seatPattern: SeatPatternEntity;

    @Column('varchar')
    @IsNotEmpty({message: 'License plates is require'})
    @MaxLength(255, {message: 'License plates max length is 255'})
    @ApiModelProperty({ example: '62C1-9999', required: true  })
    license_plates: string;

    @Column({ type: 'smallint', default: 1})
    @IsNumber()
    @ApiModelProperty({ example: 1, required: true, default: 1 })
    status: number;

    @Column({
        type: 'json',
        nullable: true,
    })
    images: object[];

    @Column({
        type: 'json',
        nullable: true,
    })
    front_car_insurance: object;

    @Column({
        type: 'json',
        nullable: true,
    })
    back_car_insurance: object;

    @Column({
        type: 'json',
        nullable: true,
    })
    front_registration_certificates: object;

    @Column({
        type: 'json',
        nullable: true,
    })
    back_registration_certificates: object;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @OneToMany(type => VehicleDriverEntity, vehicle_driver => vehicle_driver.vehicle)
    vehicleDriver: VehicleDriverEntity[];

    @ManyToMany(type => UsersEntity, user => user.vehicles)
    @JoinTable({
        name: 'az_vehicle_driver',
        joinColumn: {
            name: 'vehicle_id',
            referencedColumnName: 'id',
        },
        inverseJoinColumn: {
            name: 'driver_id',
            referencedColumnName: 'id',
        },
    })
    users: UsersEntity[];

    @OneToMany(type => BikeCarOrderEntity, bike_car_order => bike_car_order.vehicleBikeCar)
    bikeCarOrder: BikeCarOrderEntity[];

    @OneToMany(type => VanBagacOrdersEntity, van_bagac_order => van_bagac_order.vehicleVanBagac)
    vanBagacOrder: VanBagacOrdersEntity[];

}
