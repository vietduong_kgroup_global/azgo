import {forwardRef, Inject, Injectable} from '@nestjs/common';
import {VehicleRepository} from './vehicle.repository';
import {InjectRepository} from '@nestjs/typeorm';
import {VehicleEntity} from './vehicle.entity';
import {getManager} from 'typeorm';
import {DriverProfileService} from '../driver-profile/driver-profile.service';
import {CreateVehicleDto} from './dto/create-vehicle.dto';
import {VehicleTypesService} from '../vehicle-types/vehicle-types.service';
import {VehicleBrandService} from '../vehicle-brand/vehicle-brand.service';
import {VehicleDriverEntity} from '../vehicle-driver/vehicle-driver.entity';
import {VehicleDriverService} from '../vehicle-driver/vehicle-driver.service';
import {SeatPatternService} from '../seat-pattern/seat-pattern.service';
import {CreateVehicleBus} from './dto/create-vehicle-bus';
import {ProvidersInfoService} from '../providers-info/providers-info.service';
import {VehicleGroup, UserType} from '@azgo-module/core/dist/utils/enum.ultis';
import {UsersEntity} from '../users/users.entity';

@Injectable()
export class VehicleService {

    constructor(
        @InjectRepository(VehicleRepository)
        private readonly vehicleRepository: VehicleRepository,
        @Inject(forwardRef(() => DriverProfileService))
        private readonly driverProfileService: DriverProfileService,
        private readonly vehicleTypesService: VehicleTypesService,
        private readonly vehicleBrandService: VehicleBrandService,
        private readonly vehicleDriverService: VehicleDriverService,
        private readonly seatPatternService: SeatPatternService,
        private readonly providersInfoService: ProvidersInfoService,
        private readonly vehicelTypeService: VehicleTypesService,

    ) {}
    async getAllDriverByProvider(provider_id: number) {
        return await this.vehicleRepository.createQueryBuilder('az_vehicle')
            .where('provider_id = :provider_id', {provider_id})
            .select('id')
            .getRawMany();
    }
    /**
     * This is function find vehicle by id
     * @param id
     * @param user
     */
    async findById(id: number, user?: UsersEntity): Promise<VehicleEntity> {
        let query = this.vehicleRepository.createQueryBuilder('az_vehicle')
            .leftJoinAndSelect('az_vehicle.vehicleBrand', 'vehicleBrand')
            .leftJoinAndSelect('az_vehicle.vehicleType', 'vehicleType')
            .leftJoinAndSelect('vehicleType.vehicleGroup', 'vehicleGroup')
            .leftJoinAndSelect('az_vehicle.provider', 'provider')
            .leftJoinAndSelect('az_vehicle.seatPattern', 'seatPattern')
            .leftJoinAndSelect('az_vehicle.users', 'drivers')
            .leftJoinAndSelect('drivers.driverProfile', 'driverProfile')
            .where('az_vehicle.id = :id', {id})
            .andWhere('az_vehicle.deleted_at is null');

        if (user && user.type === UserType.PROVIDER) {
            query.leftJoin('provider.users', 'user')
                .andWhere('user.id = :user_id', {user_id: user.id});
        }

        return await query.getOne();
    }

    /**
     * This is function find vehicle by provider and vehicle type
     * @param provider_id
     * @param vehicle_type_id
     * @param options
     */
    async findByProviderAndVehicleType(provider_id: number, vehicle_type_id: number, options?: any) {
        let license_plates = options.license_plates ? options.license_plates.trim() : null;
        let query = await this.vehicleRepository.createQueryBuilder('az_vehicle')
            .where('provider_id = :provider_id', {provider_id})
            .andWhere('vehicle_type_id = :vehicle_type_id', {vehicle_type_id})
            .andWhere('az_vehicle.deleted_at is null');
        if (license_plates) {
            query.andWhere(`license_plates like "%${license_plates}%" `);
        }
        return query.getMany();
    }

    async findVehicleByProviderId(provider_id: number) {
        return await this.vehicleRepository.createQueryBuilder('az_vehicle')
            .where('provider_id = :provider_id', {provider_id})
            .andWhere('az_vehicle.deleted_at is null')
            .orderBy('id', 'DESC')
            .getOne();
    }

    /**
     * This is function find vehicle by vehicle group
     * @param vehicle_group_id
     * @param limit
     * @param offset
     * @param options
     */
    async findByVehicleGroup(vehicle_group_ids: any, limit: number, offset: number,  options?: any) {
        const license_plates = options.license_plates ? options.license_plates.trim() : null;
        const provider_id = !isNaN(Number(options.provider_id).valueOf()) ? Number(options.provider_id).valueOf() : null;
        let query = await this.vehicleRepository.createQueryBuilder('az_vehicle')
            .leftJoinAndSelect('az_vehicle.vehicleBrand', 'vehicleBrand')
            .leftJoinAndSelect('az_vehicle.vehicleType', 'vehicleType')
            .leftJoinAndSelect('az_vehicle.provider', 'provider')
            .leftJoinAndSelect('az_vehicle.users', 'users')
            .leftJoinAndSelect('users.driverProfile', 'driverProfile')
            .leftJoinAndSelect('az_vehicle.seatPattern', 'seatPattern')
            .where('vehicleType.vehicle_group_id in(:vehicle_group_ids)', {vehicle_group_ids});

        // search by keyword
        if (options.keyword && options.keyword.trim()) {
            query.andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select('az_vehicle.id')
                    .from(VehicleEntity, 'az_vehicle')
                    .where('license_plates like :license_plates', { license_plates: '%' + options.keyword.trim() + '%' })
                    .orWhere('vehicleType.name like :name', { name: '%' + options.keyword.trim() + '%' })
                    .orWhere('provider.provider_name like :provider_name', { provider_name: '%' + options.keyword.trim() + '%' })
                    .getQuery();
                return 'az_vehicle.id IN ' + subQuery;
            });
        }

        query.andWhere(`az_vehicle.deleted_at is null`);

        if (license_plates) {
            query.andWhere(`az_vehicle.license_plates like "%${license_plates}%" `);
        }
        if (provider_id) {
            query.andWhere('provider.id = :provider_id', {provider_id});
        }
        // limit + offset
        query.orderBy('az_vehicle.id', 'DESC')
            .limit(limit)
            .offset(offset);

        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }

    /**
     * This is function create bus
     * @param body
     * @param vehicle_group_id
     */
    async createBus(body: CreateVehicleBus) {
        const checkExistLicensePlate = await this.vehicleRepository.findOne( {where: {license_plates: body.license_plates}});
        if (checkExistLicensePlate) {
            throw { message: 'license_plates existed', status: 400 };
        }
        let result = await Promise.all([
            this.vehicleTypesService.findByIdAndVehicleGroup(body.vehicle_type_id, [VehicleGroup.BUS]),
            this.seatPatternService.findById(body.seat_pattern_id),
            this.providersInfoService.findById(body.provider_id),
        ]);

        let vehicle_type = result[0];
        let seat_pattern = result[1];
        let provider = result[2];

        if (!vehicle_type) throw { message: 'Vehicle type not found', status: 404 };
        if (!seat_pattern) throw { message: 'Seat pattern not found', status: 404 };
        if (!provider) throw { message: 'Provider not found', status: 404 };

        let vehicle = new VehicleEntity();
        vehicle.license_plates = body.license_plates;
        vehicle.front_registration_certificates = body.front_registration_certificates;
        vehicle.back_registration_certificates = body.back_registration_certificates;
        vehicle.front_car_insurance = body.front_car_insurance;
        vehicle.back_car_insurance = body.back_car_insurance;
        vehicle.images = body.images;
        vehicle.vehicleType = vehicle_type;
        vehicle.seatPattern = seat_pattern;
        vehicle.provider = provider;
        return await this.vehicleRepository.save(vehicle);
    }

    /**
     * This is function update bus
     * @param id
     * @param body
     * @param vehicle_group_id
     */
    async updateBus(id: number, body: CreateVehicleBus) {
        // Check exist license_plates
        const checkExistLicensePlate = await this.vehicleRepository.createQueryBuilder( 'vehicle')
            .where('license_plates = :license_plates', {license_plates: body.license_plates})
            .andWhere('id != :id', {id})
            .getOne();
        if (checkExistLicensePlate) throw { message: 'license_plates existed', status: 400 };
        // get vehicle. vehicle brand
        let result = await Promise.all([
            this.findById(id),
            this.vehicleTypesService.findByIdAndVehicleGroup(body.vehicle_type_id, [VehicleGroup.BUS]),
            this.seatPatternService.findById(body.seat_pattern_id),
            this.providersInfoService.findById(body.provider_id),
        ]);

        let vehicle = result[0];
        let vehicle_type = result[1];
        let seat_pattern = result[2];
        let provider = result[3];

        if (!vehicle) throw { message: 'Vehicle not found', status: 404 };
        if (!vehicle_type) throw { message: 'Vehicle type not found', status: 404 };
        if (!seat_pattern) throw { message: 'Seat pattern not found', status: 404 };
        if (!provider) throw { message: 'Provider not found', status: 404 };

        vehicle.license_plates = body.license_plates;
        vehicle.front_registration_certificates = body.front_registration_certificates;
        vehicle.back_registration_certificates = body.back_registration_certificates;
        vehicle.front_car_insurance = body.front_car_insurance;
        vehicle.back_car_insurance = body.back_car_insurance;
        vehicle.images = body.images;
        vehicle.vehicleType = vehicle_type;
        vehicle.seatPattern = seat_pattern;
        vehicle.provider = provider;
        return await this.vehicleRepository.save(vehicle);
    }

    /**
     * This is function create bike car or van bagac
     * @param body
     * @param vehicle_group_id
     */
    async createVehicle(body: CreateVehicleDto) {
        // Check exist license_plates
        const checkExistLicensePlate = await this.vehicleRepository.findOne( {where: {license_plates: body.license_plates}});
        if (checkExistLicensePlate) {
            throw { message: 'license_plates existed', status: 400 };
        }
        // Find driver & vehicle brand
        let vehicle, vehicle_driver;
        let result = await Promise.all([
            this.driverProfileService.findByUserId(body.driver_id),
            this.vehicleBrandService.findById(body.vehicle_brand_id),
        ]);
        const vehicleGroup = await this.vehicleTypesService.findById(body.vehicle_type_id);
        let vehicleType;
        if (vehicleGroup) {
            vehicleType = await this.vehicleTypesService.findByIdAndVehicleGroup(body.vehicle_type_id, [vehicleGroup.vehicle_group_id]);
        }
        let driver = result[0];
        let vehicle_brand = result[1];
        if (!driver) throw { message: 'Driver not found', status: 404 };
        if (!vehicle_brand) throw { message: 'Vehicle brand not found', status: 404 };
        if (!vehicleType) throw { message: 'Vehicle type not found', status: 404 };
        return await getManager().transaction(async transactionalEntityManager => {
            vehicle = new VehicleEntity();
            vehicle.vehicleType = vehicleType;
            vehicle.vehicleBrand = vehicle_brand;
            vehicle.license_plates = body.license_plates;
            vehicle.front_registration_certificates = body.front_registration_certificates;
            vehicle.back_registration_certificates = body.back_registration_certificates;
            vehicle.front_car_insurance = body.front_car_insurance;
            vehicle.back_car_insurance = body.back_car_insurance;
            vehicle.images = body.images;
            await transactionalEntityManager.save(vehicle);
            // check vehicle driver exists
            // vehicle_driver = await this.vehicleDriverService.findByDriver(driver.user_id);
            // if (vehicle_driver) throw { message: 'Driver is car already exists', status: 409 };
            // save vehicle driver
            vehicle_driver = new VehicleDriverEntity();
            vehicle_driver.vehicle = vehicle;
            vehicle_driver.driverProfile = driver;
            await transactionalEntityManager.save(vehicle_driver);
        });
    }

    /**
     * This is function update bike car or van bagac
     * @param vehicle_id
     * @param body
     * @param vehicle_group_id
     */
    async updateVehicle(vehicle_id: number, body: CreateVehicleDto, vehicle_group_ids: any) {
        // check exist license_plates
        const checkExistLicensePlate = await this.vehicleRepository.createQueryBuilder( 'vehicle')
            .where('license_plates = :license_plates', {license_plates: body.license_plates})
            .andWhere('id != :vehicle_id', {vehicle_id})
            .getOne();
        if (checkExistLicensePlate) throw { message: 'license_plates existed', status: 400 };
        // Get vehicle, driver, vehicle brand
        const vehicleType = await this.vehicelTypeService.findById(body.vehicle_type_id);
        let result = await Promise.all([
            this.findById(vehicle_id),
            this.vehicleDriverService.findByVehicle(vehicle_id),
            this.driverProfileService.findByUserId(body.driver_id),
            this.vehicleBrandService.findById(body.vehicle_brand_id),
        ]);
        let vehicle = result[0];
        let vehicle_driver = result[1];
        let driver = result[2];
        let vehicle_brand = result[3];

        // check data old exits
        if (!vehicle) throw { message: 'Vehicle not found', status: 404 };
        // console.log('vehicle_driver',vehicle_driver) 
        // if (vehicle_driver && body.driver_id !== vehicle_driver.driver_id) {
        //     throw { message: 'Driver is car already exists', status: 409 };
        // }
        // check data new
        if (!driver) throw { message: 'Driver not found', status: 404 };
        if (!vehicleType) throw { message: 'Vehicle type not found', status: 404 };
        if (!vehicle_brand) throw { message: 'Vehicle brand not found', status: 404 };

        await getManager().transaction(async transactionalEntityManager => {
            vehicle.license_plates = body.license_plates;
            vehicle.front_registration_certificates = body.front_registration_certificates;
            vehicle.back_registration_certificates = body.back_registration_certificates;
            vehicle.front_car_insurance = body.front_car_insurance;
            vehicle.back_car_insurance = body.back_car_insurance;
            vehicle.images = body.images;
            vehicle.vehicleType = vehicleType;
            vehicle.vehicleBrand = vehicle_brand;
            await transactionalEntityManager.save(vehicle);
            // save vehicle driver
            if (!vehicle_driver) {
                vehicle_driver = new VehicleDriverEntity();
            }
            vehicle_driver.vehicle = vehicle;
            vehicle_driver.driverProfile = driver;
            await transactionalEntityManager.save(vehicle_driver);
        });

        return vehicle;
    }

    /**
     * This is function delete vehicle
     * @param id
     */
    async delete(id: number): Promise<boolean> {
        let vehicle = await this.findById(id);
        if (!vehicle) throw { message: 'Vehicle not found', status: 404 };
        await this.vehicleDriverService.deleteByVehicle(vehicle.id);
        vehicle.deleted_at = new Date();
        await this.vehicleRepository.save(vehicle);
        return true;
    }
    async findOne(id: number) {
        return await this.vehicleRepository.findOne(id, {relations: ['provider', 'vehicleType']});
    }

}
