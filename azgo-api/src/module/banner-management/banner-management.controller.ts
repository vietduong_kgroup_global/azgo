import {Body, Controller, Get, HttpStatus, Param, Post, Put, Req, Res, UseGuards} from '@nestjs/common';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import {ApiBearerAuth, ApiImplicitParam, ApiOperation, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import {BannerManagementService} from './banner-management.service';
import { Response } from 'express';
import {AddBannerDto} from './dto/add-banner.dto';
import {AuthGuard} from '@nestjs/passport';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@ApiUseTags('Banner management')
@Controller('banner-management')
export class BannerManagementController {
    constructor(private readonly bannerManagementService: BannerManagementService) {
    }
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @Get()
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all banner' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getAll(@Req() request, @Res() res: Response) {
        try {
            let result = await this.bannerManagementService.findAll();

            return res.status(HttpStatus.OK).send(dataSuccess('Success', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @Get(':id')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get one banner' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'id', description: 'Id banner', required: true, type: Number})
    async getOne(@Req() request, @Res() res: Response, @Param('id') id: number) {
        try {
            let result = await this.bannerManagementService.findOne(id);

            return res.status(HttpStatus.OK).send(dataSuccess('Success', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @Post()
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create one banner' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createOne(@Req() request, @Body() body: AddBannerDto, @Res() res: Response) {
        try {
            let result = await this.bannerManagementService.create(body);
            return res.status(HttpStatus.OK).send(dataSuccess('Success', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @Put(':id')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update banner' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiImplicitParam({name: 'id', description: 'Id banner', required: true, type: Number})
    async update(@Req() request, @Body() body: AddBannerDto, @Res() res: Response, @Param('id') id: number) {
        try {
            let result = await this.bannerManagementService.update(id, body);
            return res.status(HttpStatus.OK).send(dataSuccess('Success', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
