import { Injectable } from '@nestjs/common';
import {BannerManagementRepository} from './banner-management.repository';
import {BannerManagementEntity} from './banner-management.entity';
import {AddBannerDto} from './dto/add-banner.dto';

@Injectable()
export class BannerManagementService {
    constructor(private readonly bannerManagementRepository: BannerManagementRepository) {
    }
    async findAll(): Promise<BannerManagementEntity[]> {
        return await this.bannerManagementRepository.find();
    }
    async findOne(id: number): Promise<BannerManagementEntity> {
        return await this.bannerManagementRepository.findOneOrFail(id);
    }
    async create(body: AddBannerDto): Promise<BannerManagementEntity> {
        return await this.bannerManagementRepository.save(body);
    }
    async update(id: number, body: AddBannerDto): Promise<BannerManagementEntity> {
        const result = await this.bannerManagementRepository.findOneOrFail(id);
        const dataUpdate = {...result, ...body};
        return await this.bannerManagementRepository.save(dataUpdate);
    }
}
