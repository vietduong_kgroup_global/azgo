import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';
import {IsNotEmpty, MaxLength} from 'class-validator';
import {Exclude} from 'class-transformer';
import {StatusCommon} from '../../enums/status.enum';

@Entity('az_banner_management')
export class BannerManagementEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar')
    @IsNotEmpty()
    @MaxLength(255)
    banner: string;

    @Column({
        type: 'json',
    })
    image: object;

    @Column('smallint')
    status: StatusCommon;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

}
