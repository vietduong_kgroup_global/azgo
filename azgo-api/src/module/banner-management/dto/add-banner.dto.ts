import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';
import {ImageDto} from '../../../general-dtos/image.dto';
import {StatusCommon} from '../../../enums/status.enum';

export class AddBannerDto {

    @IsNotEmpty()
    @ApiModelProperty({ example: 'Banner name' , required: true  })
    banner: string;

    @IsNotEmpty()
    @ApiModelProperty({ type: ImageDto, required: false })
    image: ImageDto;

    @IsNotEmpty()
    @ApiModelProperty({ enum: StatusCommon, default: 1,  required: true})
    status: number;

}
