import { Module } from '@nestjs/common';
import { BannerManagementController } from './banner-management.controller';
import { BannerManagementService } from './banner-management.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {BannerManagementRepository} from './banner-management.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BannerManagementRepository,
    ]),
  ],
  controllers: [BannerManagementController],
  providers: [BannerManagementService],
  exports: [BannerManagementService],
})
export class BannerManagementModule {}
