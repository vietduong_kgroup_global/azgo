import {BannerManagementEntity} from './banner-management.entity';
import {EntityRepository, Repository} from 'typeorm';

@EntityRepository(BannerManagementEntity)
export class BannerManagementRepository extends Repository<BannerManagementEntity>{

}
