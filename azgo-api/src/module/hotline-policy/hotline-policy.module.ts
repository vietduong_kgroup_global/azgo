import { Module } from '@nestjs/common';
import { HotlinePolicyService } from './hotline-policy.service';
import { HotlinePolicyController } from './hotline-policy.controller';
import {TypeOrmModule} from '@nestjs/typeorm';
import {HotlinePolicyRepository} from './hotline-policy.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([HotlinePolicyRepository]),
  ],
  providers: [HotlinePolicyService],
  controllers: [HotlinePolicyController],
})
export class HotlinePolicyModule {}
