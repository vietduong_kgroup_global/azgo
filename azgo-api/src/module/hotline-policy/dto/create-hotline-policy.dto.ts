import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsString, ValidateNested} from 'class-validator';
import {Type} from 'class-transformer';

export class HotLine {
    @ApiModelProperty({
        required: true,
        type: String,
    })
    report_problem: string;

    @ApiModelProperty({
        required: true,
        type: String,
    })
    @IsString()
    help_center: string;

}
export class CreateHotlinePolicyDto {

    @IsNotEmpty()
    @ApiModelProperty({ example: {report_problem: '113', help_center: '119', type: HotLine} })
    @ValidateNested({each: true})
    @Type(() => HotLine)
    hotline: HotLine;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'viết văn' })
    policy: string;
}
