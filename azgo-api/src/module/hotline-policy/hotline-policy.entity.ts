import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';

@Entity('az_hotline_policy')
export class HotlinePolicyEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'json',
        nullable: true,
    })
    hotline: object;

    @Column({
        type: 'longtext',
        nullable: true,
    })
    policy: string;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;
}
