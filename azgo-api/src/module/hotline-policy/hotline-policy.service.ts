import { Injectable } from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {HotlinePolicyRepository} from './hotline-policy.repository';
import {CreateHotlinePolicyDto} from './dto/create-hotline-policy.dto';

@Injectable()
export class HotlinePolicyService {
    constructor(
        @InjectRepository(HotlinePolicyRepository)
        private readonly hotlinePolicyRepository: HotlinePolicyRepository,
    ) {
    }

    async create(data) {
        return await this.hotlinePolicyRepository.save(data);
    }

    async getOne() {
        return await this.hotlinePolicyRepository.findOne();
    }

    async update(id: number, data: CreateHotlinePolicyDto) {
        return await this.hotlinePolicyRepository.update(id, data);
    }
}
