import {EntityRepository, Repository} from 'typeorm';
import {HotlinePolicyEntity} from './hotline-policy.entity';

@EntityRepository(HotlinePolicyEntity)
export class HotlinePolicyRepository extends Repository<HotlinePolicyEntity> {

}
