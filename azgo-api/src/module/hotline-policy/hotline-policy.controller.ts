import { Get, Controller, UseGuards, Post, Body, Res, HttpStatus, Put, Param } from '@nestjs/common';
import { HotlinePolicyService } from './hotline-policy.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiImplicitParam, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { Response } from 'express';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { CreateHotlinePolicyDto } from './dto/create-hotline-policy.dto';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@ApiUseTags('Hotline - Policy')
@Controller('hotline-policy')
export class HotlinePolicyController {
    constructor(private readonly hotlinePolicyService: HotlinePolicyService) {
    }

    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Add new hotline policy' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    async create(@Body(CustomValidationPipe) body: CreateHotlinePolicyDto, @Res() res: Response) {
        try {
            const result = await this.hotlinePolicyService.create(body);
            return res.status(HttpStatus.OK).send(dataSuccess('Create success', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    // @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get one hotline policy' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    async getOne(@Res() res: Response) {
        try {
            const result = await this.hotlinePolicyService.getOne();
            if (!result) {
                return res.status(HttpStatus.OK).send(dataSuccess('Success', {}));
            }
            return res.status(HttpStatus.OK).send(dataSuccess('Success', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Put('/:id')
    @ApiOperation({ title: 'Update hotline policy' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiImplicitParam({name: 'id', description: 'id hotline policy', required: true, type: 'number' })
    async update(@Param('id') id, @Body(CustomValidationPipe) body: CreateHotlinePolicyDto, @Res() res: Response) {
        try {
            await this.hotlinePolicyService.update(id, body);
            return res.status(HttpStatus.OK).send(dataSuccess('Update success', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
