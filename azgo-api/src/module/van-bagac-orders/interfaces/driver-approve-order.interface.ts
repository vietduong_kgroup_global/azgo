export interface DriverApproveOrderInterface {
    book_van_bagac_reference: string;
    start_time: string;
    end_time: string;
    driver_uuid: string;
    driver_id: number;
}
