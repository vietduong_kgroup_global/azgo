export interface CountOrderVanBagacInteface {
    status?: number;
    is_in_month?: string;
    from_date?: string;
    to_date?: string;
    driver_id?: number;
}
