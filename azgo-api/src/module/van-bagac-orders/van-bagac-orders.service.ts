import {forwardRef, Inject, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {VanBagacOrdersRepository} from './van-bagac-orders.repository';
import {RedisUtil} from '@azgo-module/core/dist/utils/redis.util';
import {RedisService} from '@azgo-module/core/dist/background-utils/Redis/redis.service';
import config from '@azgo-module/core/dist/config';
import {CommonService} from '../../helpers/common.service';
import {FirebaseService} from '../firebase/firebase.service';
import * as _ from 'lodash';
import {VehicleMovingService} from '../vehicle-moving/vehicleMoving.service';
import {UsersService} from '../users/users.service';
import {FirebaseRepository} from '../firebase/firebase.repository';
import {DriverProfileService} from '../driver-profile/driver-profile.service';
import {VanBagacOrdersEntity} from './van-bagac-orders.entity';
import {Brackets, getConnection} from 'typeorm';
import {RatingDriverService} from '../rating-driver/rating-driver.service';
import * as moment from 'moment';
import {CountOrderVanBagacInteface} from './interfaces/count-order-van-bagac.inteface';
import {CountCustomerInterface} from '../bike-car-customer/interfaces/count-customer.interface';
import {
    KEY_CUSTOMER_TIME_OUT_CANCEL_APPROVE_VAN_BAGAC,
    DRIVER_VAN_BAGAC_UPDATE_STATUS_COMPLETED,
    DRIVER_VAN_BAGAC_UPDATE_STATUS_DELIVERIED,
    DRIVER_VAN_BAGAC_UPDATE_STATUS_MOVING, DRIVER_VAN_BAGAC_UPDATE_STATUS_RECEIVED,
    FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC,
    FIELD_VEHICLE_POSITIONS_BAGAC,
    FIELD_VEHICLE_POSITIONS_VAN, KEY_CANCEL_APPROVED_BUS,
    KEY_CANCEL_FIND_BOOK_VAN_BA_GAC_BY_CUSTOMER,
    KEY_CLICK_ACTION,
    KEY_CUSTOMER_CANCEL_APPROVE_BOOK_VAN_BAGAC_TO_DRIVER,
    KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC,
    KEY_CUSTOMER_SEND_REQUEST_BOOK_VAN_BAGAC_TO_DRIVER, KEY_DISABLE_BTN_CANCEL_APPROVED_VANBAGAC,
    KEY_DRIVER_ACCEPT_REQUEST_BOOK_VAN_BAGAC_FROM_CUSTOMER,
    KEY_TRIGGER_LIST_REQUEST_BAGAC,
    KEY_TRIGGER_LIST_REQUEST_VAN,
    VEHICLE_POSITIONS_BAGAC,
    VEHICLE_POSITIONS_VAN,
} from '../../constants/keys.const';
import {StatusCode, StatusDisableBtnApproveBooKVanBaGac, StatusRequestBooKVanBaGac} from '../../enums/status.enum';
import {VehicleGroup, VehicleGroupID} from '../../enums/group.enum';
import {GoogleMapService} from '../google-map/google-map.service';
import {InjectSchedule, Schedule} from 'nest-schedule';
import {CustomChargeService} from '../custom-charge/custom-charge.service';
import {WalletService} from '../wallet/wallet.service';
import {DriverApproveOrderInterface} from './interfaces/driver-approve-order.interface';

@Injectable()
export class VanBagacOrdersService {
    private redisService: RedisService;
    private redis: RedisUtil;
    // use for vehicle bus
    private keyRedisVehiclePositionBus = 'vehicle_positions_bus';
    private fieldRedisVehiclePositionBus = 'field_vehicle_positions_bus';
    // use for vehicle bike
    private keyRedisVehiclePositionBike = 'vehicle_positions_bike';
    private fieldRedisVehiclePositionBike = 'field_vehicle_positions_bike';
    // use for vehicle car
    private keyRedisVehiclePositionCar = 'vehicle_positions_car';
    private fieldRedisVehiclePositionCar = 'field_vehicle_positions_car';
    // use for vehicle van
    private keyRedisVehiclePositionVan = 'vehicle_positions_van';
    private fieldRedisVehiclePositionVan = 'field_vehicle_positions_van';
    // use for vehicle bagac
    private keyRedisVehiclePositionBagac = 'vehicle_positions_bagac';
    private fieldRedisVehiclePositionBagac = 'field_vehicle_positions_bagac';

    private keyRedisVanBagacOrder = 'key_customer_request_book_van_bagac';
    private fieldRedisVanBagacOrder = 'field_customer_request_book_van_bagac';
    private  connect:any;
    constructor(
        @InjectRepository(VanBagacOrdersRepository)
        private readonly vanBagacOrdersRepository: VanBagacOrdersRepository,
        private commonService: CommonService,
        @InjectRepository(FirebaseRepository)
        private firebaseRepository: FirebaseRepository,
        private firebaseService: FirebaseService,
        @Inject(forwardRef(() => VehicleMovingService))
        private readonly vehicleMovingService: VehicleMovingService,
        private readonly userService: UsersService,
        private driverProfileService: DriverProfileService,
        private readonly ratingService: RatingDriverService,
        private readonly googleMapService: GoogleMapService,
        private readonly walletService: WalletService,
        private readonly customChargeService: CustomChargeService,
        @InjectSchedule() private readonly schedule: Schedule,
    ) {
        this.connect= {
            port: process.env.ENV_REDIS_PORT || 6379,
            host: process.env.ENV_REDIS_HOST || '127.0.0.1', 
            password: process.env.ENV_REDIS_PASSWORD || null,
        };
        console.log(" Redis constructor:",this.connect);
        this.redisService = RedisService.getInstance(this.connect);
        this.redis = RedisUtil.getInstance();
    }

    async getAll() {
        return await this.vanBagacOrdersRepository.find({
            order: {id: 'ASC'},
        });
    }

    async getAllOrderByCustomer(customer_id: number) {
        const results = await this.vanBagacOrdersRepository.createQueryBuilder('order')
            .leftJoinAndSelect('order.customerProfile', 'customerProfile')
            .leftJoinAndSelect('order.ratingDriver', 'ratingDriver')
            .leftJoinAndSelect('order.service', 'service')
            .leftJoinAndSelect('service.vehicles', 'vehicles')
            .andWhere('order.customer_id = :customer_id', {customer_id})
            .getManyAndCount();
        // Loop result to add new record
        let dataNew = [];
        await Promise.all(results[0].map(async (item) => {
            if (item.driver_id) {
                const driverInfo = await this.userService.getOneDriver(item.driver_id);
                dataNew.push({...item, ...{driverInfo}});
            } else {
                dataNew.push(item);
            }
        }));
        // sort asc by status
        return dataNew.sort((a, b) => Number(b.status) - Number(a.status)).reverse();
    }

    async getAllOrderByDriver(driver_id: number, status: number = StatusRequestBooKVanBaGac.REQUEST_NEW) {
        return await this.vanBagacOrdersRepository.createQueryBuilder('order')
            .where('order.driver_id = :driver_id',  {driver_id})
            .andWhere('order.status != :status',  {status})
            .leftJoinAndSelect('order.customerProfile', 'customerProfile')
            .leftJoinAndSelect('customerProfile.users', 'users')
            .leftJoinAndSelect('order.driverProfile', 'driverProfile')
            .leftJoinAndSelect('order.service', 'service')
            .getMany();
    }

    async getListOrderNearDriver(driver_uuid: string) {
        // Get info driver
        let findDriver: { type_group: any; vehicle_type_id: any; lat: any; lng: any; }, getCacheDriverByVehicleGroup: { isValidate: any; value: string; }, flagExistDriver = false;
        getCacheDriverByVehicleGroup = await this.redis.getInfoRedis(VEHICLE_POSITIONS_VAN, FIELD_VEHICLE_POSITIONS_VAN);
        if (getCacheDriverByVehicleGroup.isValidate) {
            const listDriver = JSON.parse(getCacheDriverByVehicleGroup.value);
            if (listDriver) {
                findDriver = _.find(listDriver, {user_uuid: driver_uuid});
                if (findDriver) flagExistDriver = true;
            }
        }
        if (!flagExistDriver) {
            getCacheDriverByVehicleGroup = await this.redis.getInfoRedis(VEHICLE_POSITIONS_BAGAC, FIELD_VEHICLE_POSITIONS_BAGAC);
            if (getCacheDriverByVehicleGroup.isValidate) {
                const listDriver = JSON.parse(getCacheDriverByVehicleGroup.value);
                if (listDriver) {
                    findDriver = _.find(listDriver, {user_uuid: driver_uuid});
                }
            }
        }
        if (!findDriver) throw {message: 'Not found driver position', status: StatusCode.NOT_FOUND};

        // Get list request by type_group
        const getCacheOrder = await this.redis.getInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC);
        if (getCacheOrder.isValidate) {

            const listOrder = JSON.parse(getCacheOrder.value);
            if (listOrder) {
                const getRequestByVehicleGroup: any = _.filter(listOrder, (req: { route_info: { start_date: string; start_hour: string; type_group: any; service_id: any; }; driver_accept: boolean; }) => {
                    const start_date = moment(req.route_info.start_date + ' ' + req.route_info.start_hour + ':00').format('YYYY-MM-DD HH:mm:ss');
                    return req.route_info.type_group === findDriver.type_group
                        && req.driver_accept === false
                        && req.route_info.service_id === findDriver.vehicle_type_id
                        && start_date >= moment().format('YYYY-MM-DD HH:mm:ss');
                        // && findDriver.fee_wallet >= Math.ceil((req.total_price * Number(process.env.PERCENT_APPROVED || 10) / 100));
                });
                await Promise.all(getRequestByVehicleGroup.map(async (value: { route_info: { start_point: { lat: any; lng: any; }; end_point: { lat: any; lng: any; }; }; distance_receive: any; receive_duration: any; distance_return: any; return_duration: any; }) => {
                    const distances: any = await Promise.all([
                        this.googleMapService.rawUrlGetDistance({
                            origins: {lat: findDriver.lat, lng: findDriver.lng},
                            destinations: {lat: value.route_info.start_point.lat, lng: value.route_info.start_point.lng},
                        }),
                        this.googleMapService.rawUrlGetDistance({
                            origins: {lat: findDriver.lat, lng: findDriver.lng},
                            destinations: {lat: value.route_info.end_point.lat, lng: value.route_info.end_point.lng},
                        }),
                    ]);
                    if (!distances) return false;
                    value.distance_receive = distances[0].data.rows[0].elements[0].distance.text;
                    value.receive_duration = distances[0].data.rows[0].elements[0].duration.text;
                    value.distance_return = distances[1].data.rows[0].elements[0].distance.text;
                    value.return_duration = distances[1].data.rows[0].elements[0].duration.text;

                }));
                return getRequestByVehicleGroup.sort((a: { route_info: { start_date: string | number | Date; }; }, b: { route_info: { start_date: string | number | Date; }; }) => {
                    const c: any = new Date(a.route_info.start_date);
                    const d: any = new Date(b.route_info.start_date);
                    return d - c;
                });
            }
        }
        return null;
    }

    /**
     * Find 5 "BAGAC" devices near order start point. Processes:
     *  - Get all BAGAC records that are ready to receive order and calculate distance from start point to each devices.
     *  - Check if each BAGAC record has device_token, if yes, add to push noti list.
     *  -
     * @param data FindBagacNearCustomerDto
     * @param customerInfo
     * @return boolean
     */
    async findBagacNearCustomer(data: any, customer_uuid: string): Promise<boolean> {
        const book_van_bagac_reference = customer_uuid.toString() + '_' + Math.random().toString(36).slice(2);
        try {
            // Get distance, duration from to
            const distance: any = await this.googleMapService.rawUrlGetDistance({
                origins: {lat: data.start_point.lat, lng: data.start_point.lng},
                destinations: {lat: data.end_point.lat, lng: data.end_point.lng},
            });

            if (distance) {
                data.receive_and_delivery_distance = distance.data.rows[0].elements[0].distance.text;
                data.receive_and_delivery_duration = distance.data.rows[0].elements[0].duration.text;
            }
        }catch (error) {
            throw {message: 'Not found position', status: StatusCode.BAD_REQUEST};
        }
        const customerInfo = await this.userService.getDetailCustomer(customer_uuid.toString());
        // Set array request order
        const dataObj = {
            driver_accept: false,
            disable_btn_cancel_approve: StatusDisableBtnApproveBooKVanBaGac.DEFAULT,
            status: StatusRequestBooKVanBaGac.REQUEST_NEW,
            book_van_bagac_reference,
            route_info: data,
            customerInfo,
        };

        const vehicle_group_id = data.type_group === VehicleGroup.VAN ? VehicleGroupID.VAN :  VehicleGroupID.BAGAC;
        // Get first rate of charge by  vehicle group
        const rateOfChageByVehicleGroup = await this.customChargeService.getOneByVehicleGroup(vehicle_group_id);
        if (!rateOfChageByVehicleGroup) throw {message: 'Not found custom charge', status: 404};
        // Save data into db
        const order = new VanBagacOrdersEntity();
        const dataInsert = {...order, ...{
                customer_id: customerInfo.id,
                service_id: data.service_id,
                route_info: {start_point: data.start_point, end_point: data.end_point},
                receiver_info: data.receiver_info,
                luggage_info: data.luggage_info,
                start_date: data.start_date,
                start_hour: data.start_hour,
                images: data.images,
                price: data.price,
                total_price: data.total_price,
                status: StatusRequestBooKVanBaGac.REQUEST_NEW,
                service_others: data.service_others,
                book_van_bagac_reference,
                receive_and_delivery_distance: data.receive_and_delivery_distance,
                vehicle_group_id,
                rate_of_charge: rateOfChageByVehicleGroup.rate_of_charge,
                fee_tax: Math.ceil((data.total_price * rateOfChageByVehicleGroup.rate_of_charge) / 100),
            }};
        await this.vanBagacOrdersRepository.save(dataInsert);

        const getCacheOrder = await this.redis.getInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC);
        if (getCacheOrder.isValidate) {
            const cacheValue = JSON.parse(getCacheOrder.value);
            if (cacheValue) {
                cacheValue.push(dataObj);
            }
            await this.redis.setInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, JSON.stringify(cacheValue));
        } else {
            await this.redis.setInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, JSON.stringify([dataObj]));
        }
        let getCacheListDriver: { isValidate: any; value: string; }, dataNotification: { key: string; type_group: VehicleGroup; click_action: string; };
        if (data.type_group === VehicleGroup.VAN) {
            dataNotification = {
                key: KEY_TRIGGER_LIST_REQUEST_VAN,
                type_group: VehicleGroup.VAN,
                click_action: KEY_CLICK_ACTION,
            };
            getCacheListDriver = await this.redis.getInfoRedis(VEHICLE_POSITIONS_VAN, FIELD_VEHICLE_POSITIONS_VAN);
        }
        if (data.type_group === VehicleGroup.BAGAC) {
            dataNotification = {
                key: KEY_TRIGGER_LIST_REQUEST_BAGAC,
                type_group: VehicleGroup.BAGAC,
                click_action: KEY_CLICK_ACTION,
            };
            getCacheListDriver = await this.redis.getInfoRedis(VEHICLE_POSITIONS_BAGAC, FIELD_VEHICLE_POSITIONS_BAGAC);
        }
        if (getCacheListDriver && getCacheListDriver.isValidate) {
            const getCacheListDriverParse = JSON.parse(getCacheListDriver.value);
            if (getCacheListDriverParse) {
                getCacheListDriverParse.map((val: { user_uuid: string; }) => {
                    // Emit event to driver
                    this.redisService.triggerEventVanBagac({room: 'user_' + val.user_uuid, message: dataNotification});
                });
            }
        }
        return true;
    }

    async checkRequestOrderVanBagac(customer_uuid: string) {

        const getCacheOrder = await this.redis.getInfoRedis(
            this.keyRedisVanBagacOrder, this.fieldRedisVanBagacOrder);
        if (getCacheOrder.isValidate) {
            const listRequestOrderVanBagac = JSON.parse(getCacheOrder.value);
            const findCustomerUuid = _.find(listRequestOrderVanBagac, (request: { route_info: { customer_uuid: string; }; }) => {
                return request.route_info.customer_uuid === customer_uuid;
            });
            if (!findCustomerUuid) return false;
            // get caching vehcle position by driver uuid
            const getVehicle = await this.vehicleMovingService.getVehiclePostionOnRedisByDriverUuid(
                findCustomerUuid.driver_uuid_accept, findCustomerUuid.route_info.type_group);
            if (!getVehicle) throw {message: 'Not found vehicle', status: 404};
            findCustomerUuid.driver_info = getVehicle;
            return findCustomerUuid;
        }
        return false;
    }

    async checkApproveOrderVanBagac(driver_uuid: string) {
        const getCacheOrder = await this.redis.getInfoRedis(
            this.keyRedisVanBagacOrder, this.fieldRedisVanBagacOrder);
        if (getCacheOrder.isValidate) {
            const listRequestOrderBikeCar = JSON.parse(getCacheOrder.value);
            const findCustomerUuid = _.find(listRequestOrderBikeCar, (request: { driver_uuid_accept: string; }) => {
                return request.driver_uuid_accept === driver_uuid;
            });
            if (!findCustomerUuid) return false;
            return findCustomerUuid;
        }
        return false;
    }

    async cronJobDisableBTNCancelApprove(book_van_bagac_reference: string) {
        const getCacheOrder = await this.redis.getInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC);
        if (getCacheOrder.isValidate) {
            // Get list order and update
            let listOrder = JSON.parse(getCacheOrder.value);
            let orderByReference = _.find(listOrder, {book_van_bagac_reference});
            if (!orderByReference) return true;
            orderByReference.disable_btn_cancel_approve = StatusDisableBtnApproveBooKVanBaGac.CANNOT_BE_CANCELED;
            // Update request
            await this.redis.setInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, JSON.stringify(listOrder));
            const dataNotificationToCustomer = {
                key: KEY_CUSTOMER_TIME_OUT_CANCEL_APPROVE_VAN_BAGAC,
                book_van_bagac_reference,
                click_action: KEY_CLICK_ACTION,
            };
            // Emit event to customer
            this.redisService.triggerEventVanBagac({room: 'user_' +  orderByReference.route_info.customer_uuid, message: dataNotificationToCustomer});
        }
        return true;
    }
    async driverApproveOrder(data: DriverApproveOrderInterface) {
        // Todo: Get detail order by book_van_bagac_reference
        const findOrderByReference = await this.vanBagacOrdersRepository.findOneOrFail({
            book_van_bagac_reference: data.book_van_bagac_reference});
        // Todo: Get list order aceppted but not yet done
        const getAllRequestAcceptedButNotDone = await this.vanBagacOrdersRepository.createQueryBuilder('order')
            .where('driver_id = :driver_id', {driver_id: data.driver_id})
            .andWhere('created_accept is not null')
            .getMany();
        // Todo: Loop list order to compare about time accept with order new
        if (getAllRequestAcceptedButNotDone.length > 0) {
            // Todo: List all case
            let case1 = false;
            let case2 = false;
            let case3 = false;
            let case4 = false;
            let case5 = false;
            // Todo: Concat start date order new with start time driver input
            const start_time_input_cc = moment(findOrderByReference.start_date).format('YYYY-MM-DD').toString() + ' ' + data.start_time + ':00';
            const start_time_input = new Date(start_time_input_cc).getTime();

            const end_time_input = new Date(data.end_time).getTime();
            if (end_time_input < start_time_input)
                throw {message: 'Bạn không được approve trong thời gian này', status: StatusCode.CODE_CONFLICT};
            console.log(end_time_input, start_time_input, getAllRequestAcceptedButNotDone );
            getAllRequestAcceptedButNotDone.forEach(order => {
                const start_time_order_cc = moment(order.start_date).format('YYYY-MM-DD').toString() + ' ' + order.start_time + ':00';
                const start_time_order = new Date(start_time_order_cc).getTime() - 60 * 60 * 1000; // minus 1 hour
                const end_time_order = new Date(order.end_time).getTime() + 60 * 60 * 1000; // plus 1 hour;
                if (start_time_input < start_time_order || start_time_input > end_time_order) {
                    case1 = true;
                } else {
                    case1 = false;
                }
                if (end_time_input >= start_time_order || end_time_input >= end_time_order ||
                    (end_time_input <= start_time_order && end_time_input <= end_time_order)) {
                    case2 = true;
                } else {
                    case2 = false;
                }
                if (start_time_input < start_time_order && end_time_input <= start_time_order) {
                    case3 = true;
                } else {
                    case3 = false;
                }
                if (start_time_input < end_time_order && end_time_input >= end_time_order) {
                    case4 = true;
                } else {
                    case4 = false;
                }
                if (start_time_input > end_time_order) {
                    case5 = true;
                }
            });
            console.log(case1, case2, case3, case4, case5);
            if ((!(case1 && case2) || (!case3 && !case4)) && !case5 )
                throw {message: 'Bạn không được approve trong thời gian này', status: StatusCode.CODE_CONFLICT};
        }

        // Check request on redis
        const created_accept = moment().format('YYYY-MM-DD HH:mm:ss');
        // get cache order van bagac
        const getCacheOrder = await this.redis.getInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC);
        if (getCacheOrder.isValidate) {

            // Get list order and update
            const listOrder = JSON.parse(getCacheOrder.value);
            const orderByReference =  _.find(listOrder, {book_van_bagac_reference: data.book_van_bagac_reference});
            if (!orderByReference) throw {message: 'Reference not found', status: 404};

            // Add new field new
            orderByReference.driver_accept = true;
            orderByReference.disable_btn_cancel_approve = StatusDisableBtnApproveBooKVanBaGac.BE_CANCELED;
            orderByReference.status = StatusRequestBooKVanBaGac.APPROVED;
            orderByReference.created_accept = created_accept;
            orderByReference.driver_uuid_accept = data.driver_uuid;

            // Get info driver and update count receive request
            let getCacheDriver: any, vehicle_id: any;
            if (orderByReference.route_info.type_group === VehicleGroup.BAGAC) {
                getCacheDriver = await this.redis.getInfoRedis(VEHICLE_POSITIONS_BAGAC, FIELD_VEHICLE_POSITIONS_BAGAC);
            }
            if (orderByReference.route_info.type_group === VehicleGroup.VAN) {
                getCacheDriver = await this.redis.getInfoRedis(VEHICLE_POSITIONS_VAN, FIELD_VEHICLE_POSITIONS_VAN);
            }
            if (!getCacheDriver.isValidate) throw {message: 'Not found driver', status: StatusCode.NOT_FOUND};

            let listDriver = JSON.parse(getCacheDriver.value);
            const findDriver =  _.find(listDriver, (driver: { user_uuid: string; count_receive_request: any; }) => {
                return driver.user_uuid === data.driver_uuid
                    && Number(driver.count_receive_request) < (Number(process.env.COUNT_RECEIVED_REQUEST_VANBAGAC) || 10);
            });
            if (!findDriver) {
                throw {message: 'Driver not found or has received 10 request', status: StatusCode.NOT_FOUND};
            }

            // Add cron job count down disable btn cancel approve
            // Get miliseccond
            const start_time_go_to = moment(orderByReference.route_info.start_date + ' ' + data.start_time + ':00');
            const today = moment();
            const plusTimeToDisableBtnCancel = start_time_go_to.diff(today) + (Number(process.env.TIME_DISABLE_AFTER_APPROVED) || 120000);
            if (plusTimeToDisableBtnCancel <= 0) throw {message: 'Start time incorrect', status: StatusCode.BAD_REQUEST};
            await this.schedule.scheduleIntervalJob(
                KEY_DISABLE_BTN_CANCEL_APPROVED_VANBAGAC + data.book_van_bagac_reference,
                Number(plusTimeToDisableBtnCancel),
                () => this.cronJobDisableBTNCancelApprove(data.book_van_bagac_reference),
            );

            // Update request
            await this.redis.setInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, JSON.stringify(listOrder));

            // Update count_receive_request
            vehicle_id = findDriver.vehicle_id;
            findDriver.count_receive_request += 1;
            if (orderByReference.route_info.type_group === VehicleGroup.BAGAC) {
                await this.redis.setInfoRedis(VEHICLE_POSITIONS_BAGAC, FIELD_VEHICLE_POSITIONS_BAGAC, JSON.stringify(listDriver));
            }
            if (orderByReference.route_info.type_group === VehicleGroup.VAN) {
                await this.redis.setInfoRedis(VEHICLE_POSITIONS_VAN, FIELD_VEHICLE_POSITIONS_VAN, JSON.stringify(listDriver));
            }

            // Update  data into db
            const order = await this.vanBagacOrdersRepository.createQueryBuilder('order')
                .where('book_van_bagac_reference = :key', {key: data.book_van_bagac_reference})
                .getOne();
            const dataInsert = {...order, ...{
                status: StatusRequestBooKVanBaGac.APPROVED, // Driver đã chấp nhận
                driver_id: data.driver_id,
                start_time: data.start_time,
                end_time: data.end_time,
                vehicle_id, created_accept,
            }};
            await this.vanBagacOrdersRepository.save(dataInsert);

            const driver_info = await this.userService.getOneDriver(data.driver_id);
            const dataNotificationToCustomer = {
                key: KEY_DRIVER_ACCEPT_REQUEST_BOOK_VAN_BAGAC_FROM_CUSTOMER,
                driver_user_uuid: data.driver_uuid,
                book_van_bagac_reference: data.book_van_bagac_reference,
                id: order.id,
                // time_to_go: orderByReference.receive_and_delivery_duration,
                info_driver: driver_info,
                click_action: KEY_CLICK_ACTION,

            };
            // Emit event to customer
            this.redisService.triggerEventVanBagac({room: 'user_' +  orderByReference.route_info.customer_uuid, message: dataNotificationToCustomer});
            // Emit event to driver
            if (listDriver) {
                let dataNotification: any;
                if (orderByReference.route_info.type_group === VehicleGroup.BAGAC) {
                    dataNotification = {
                        key: KEY_TRIGGER_LIST_REQUEST_BAGAC,
                        type_group: VehicleGroup.BAGAC,
                        click_action: KEY_CLICK_ACTION,
                    };
                }
                if (orderByReference.route_info.type_group === VehicleGroup.VAN) {
                    dataNotification = {
                        key: KEY_TRIGGER_LIST_REQUEST_VAN,
                        type_group: VehicleGroup.VAN,
                        click_action: KEY_CLICK_ACTION,
                    };
                }
                if (dataNotification) {
                    listDriver.map((val: any) => {
                        // Emit event minus this driver approved
                        if (val.user_uuid !== data.driver_uuid) {
                            this.redisService.triggerEventVanBagac({room: 'user_' + val.user_uuid, message: dataNotification});
                        }
                    });
                }
            }
        }
        return true;
    }

    async updateVanBagacOrder(driver_uuid: string, book_van_bagac_reference: string, status: number) {
        let notification: any, log_time: any;
        switch (status) {
            case 3:
                notification = {
                    key: DRIVER_VAN_BAGAC_UPDATE_STATUS_MOVING,
                    driver_uuid,
                    book_van_bagac_reference,
                    click_action: KEY_CLICK_ACTION,
                };
                log_time = {created_moving: new Date()};
                break;
            case 4:
                notification = {
                    key: DRIVER_VAN_BAGAC_UPDATE_STATUS_RECEIVED,
                    driver_uuid,
                    book_van_bagac_reference,
                    click_action: KEY_CLICK_ACTION,
                };
                log_time = {created_received: new Date()};
                break;
            case 5:
                notification = {
                    key: DRIVER_VAN_BAGAC_UPDATE_STATUS_DELIVERIED,
                    driver_uuid,
                    book_van_bagac_reference,
                    click_action: KEY_CLICK_ACTION,
                };
                log_time = {created_deliveried: new Date()};
                break;
            case 6:
                notification = {
                    key: DRIVER_VAN_BAGAC_UPDATE_STATUS_COMPLETED,
                    driver_uuid,
                    book_van_bagac_reference,
                    click_action: KEY_CLICK_ACTION,
                };
                log_time = {created_completed: new Date()};
                break;
        }

        const order = await this.vanBagacOrdersRepository.findOne({book_van_bagac_reference});
        if (!order) throw {message: 'Not found request', status: 404};
        const dataSave = {...order, ... {status}, ...log_time};

        const getCacheOrder = await this.redis.getInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC);
        if (getCacheOrder) {
            let listOrder = JSON.parse(getCacheOrder.value);
            let findReference = _.find(listOrder, {book_van_bagac_reference});
            if (!findReference) throw {message: 'Not found request', status: 404};
            if (Number(status) === StatusRequestBooKVanBaGac.COMPLETED) { // 6 = Hoàn tất đơn hàng
                _.remove(listOrder, {book_van_bagac_reference});
            } else {
                // Update status request on redis
                let getReference = _.find(listOrder, {book_van_bagac_reference});
                const getReferenceIndex = _.findIndex(listOrder, {book_van_bagac_reference});
                if (getReference) {
                    getReference.status = status;
                    getReference = {...getReference, ...log_time};
                    listOrder[getReferenceIndex] = getReference;
                }
            }

            // Emit event to customer
            this.redisService.triggerEventVanBagac({room: 'user_' +  findReference.route_info.customer_uuid, message: notification});
            // Save data & push notify
            await this.vanBagacOrdersRepository.save(dataSave);

            // Minus fee wallet by driver submit done schedule
            // if (status === StatusRequestBooKVanBaGac.COMPLETED) {
            //     await this.walletService
            //         .minusFeeWalletByDriverId(order.driver_id, order.driver_id, order.fee_tax, order.rate_of_charge);
            // }

            await this.redis.setInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, JSON.stringify(listOrder));
            return true;
        }
        return false;
    }

    async pustNotify(data: object, customer_id: number, notify: object, key: string) {
        const usersEntity = await this.userService.findById(customer_id);
        const userFCM = await this.firebaseService.findOne(usersEntity.user_id);
        if (userFCM) {
            if (data) {
                let listDriverDeviceToken = [];
                const token = userFCM.token ? userFCM.token : userFCM.apns_id;
                listDriverDeviceToken.push(token);
                const notificationData = {...data, ...{
                    click_action: KEY_CLICK_ACTION,
                    key,
                }};
                await this.firebaseService.pushNotificationWithTokens(listDriverDeviceToken, notify, notificationData);
                return data;
            }
            return false;
        }
        return false;
    }

    async getDatailVanBagacOrder(id: number) {
        const rs = await this.vanBagacOrdersRepository.createQueryBuilder('order')
            .where('order.id = :id',  {id})
            .leftJoinAndSelect('order.customerProfile', 'customerProfile')
            .leftJoinAndSelect('customerProfile.users', 'customerUsers')
            .leftJoinAndSelect('order.ratingDriver', 'ratingDriver')
            .leftJoinAndSelect('order.driverProfile', 'driverProfile')
            .leftJoinAndSelect('driverProfile.users', 'users')
            .leftJoinAndSelect('users.vehicles', 'vehicles')
            .leftJoinAndSelect('vehicles.vehicleBrand', 'vehicleBrand')
            .leftJoinAndSelect('vehicles.vehicleType', 'vehicleType')
            .leftJoinAndSelect('order.service', 'service')
            .getOne();
        if (!rs) return false;
        let avgRatingDriver = null;
        if (rs.driverProfile) {
            avgRatingDriver = await this.ratingService.avgRatingDriver(rs.driverProfile.user_id);
        }
        return {...rs, ...{avgRatingDriver: avgRatingDriver !== null ? Number(avgRatingDriver).toFixed(1) : avgRatingDriver}};
    }

    async getDatailVanBagacOrderOnRedis(van_bagac_order_id: string) {
        let getCache = await this.redis.getInfoRedis(this.keyRedisVanBagacOrder, '');
        if (getCache.isValidate) {
            const listVanBaGacOrdersRedis = JSON.parse(getCache.value);
            return _.remove(listVanBaGacOrdersRedis, (item: any) => {
                return parseInt(item.van_bagac_order_id, 10) === parseInt(van_bagac_order_id, 10);
            });
        }
        return null;
    }

    async getAllOrderVanBagac(
        limit: number = 0,
        offset: number = 10,
        options?: any,
    ) {

        // let license_plates = options.license_plates ? options.license_plates.trim() : null;
        let query = await this.vanBagacOrdersRepository.createQueryBuilder('order')
            .leftJoinAndSelect('order.customerProfile', 'customerProfile')
            .leftJoinAndSelect('customerProfile.users', 'customerUsers')
            .leftJoinAndSelect('order.ratingDriver', 'ratingDriver')
            .leftJoinAndSelect('order.driverProfile', 'driverProfile')
            .leftJoinAndSelect('driverProfile.users', 'users')
            .leftJoinAndSelect('users.vehicles', 'vehicles')
            .leftJoinAndSelect('vehicles.vehicleBrand', 'vehicleBrand')
            .leftJoinAndSelect('vehicles.vehicleType', 'vehicleType')
            .orderBy('order.id', 'DESC');
        if (!options.keyword === false) {
            query.andWhere(new Brackets(qb => {
                qb.where('order.book_van_bagac_reference like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('customerProfile.full_name like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('driverProfile.full_name like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('vehicles.license_plates like :keyword', { keyword: '%' + options.keyword.trim() + '%' });
            }));
        }
        // limit + offset
        query.take(limit).skip(offset);

        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }
    async fieldRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.fieldRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.fieldRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.fieldRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.fieldRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.fieldRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    async keyRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.keyRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.keyRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.keyRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.keyRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.keyRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    async sleep(number: number) {
        return new Promise(resolve => {
            setTimeout(resolve, number);
        });
    }

    async cancelApproveDriver(book_van_bagac_reference: string, customerInfo: any){
        let findReference: any;

        // Remove request on redis
        const getCacheOrder = await this.redis.getInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC);
        if (!getCacheOrder.isValidate) throw {message: 'Not found request', status: StatusCode.NOT_FOUND};
        const cacheValue = JSON.parse(getCacheOrder.value);
        if (cacheValue) {
            findReference = _.find(cacheValue, (item: any) => {
                return item.route_info.customer_uuid === customerInfo.user_id.toString()
                    && item.book_van_bagac_reference === book_van_bagac_reference
                    && item.status === StatusRequestBooKVanBaGac.APPROVED
                    && item.disable_btn_cancel_approve === StatusDisableBtnApproveBooKVanBaGac.BE_CANCELED;
            });
            // Remove book_van_bagac_reference
            _.remove(cacheValue, (item: any) => {
                return item.route_info.customer_uuid === customerInfo.user_id.toString()
                    && item.book_van_bagac_reference === book_van_bagac_reference
                    && item.status === StatusRequestBooKVanBaGac.APPROVED
                    && item.disable_btn_cancel_approve === StatusDisableBtnApproveBooKVanBaGac.BE_CANCELED;
            });
        }
        if (!findReference) throw {message: 'Not found request or status request current not allow', status: StatusCode.BAD_REQUEST};

        // Update request on db and remove request on redis
        const checkExistReference = await this.vanBagacOrdersRepository.findOne({book_van_bagac_reference});
        if (!checkExistReference) {
            return false;
        }
        await this.redis.setInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, JSON.stringify(cacheValue));
        await getConnection()
            .createQueryBuilder()
            .update(VanBagacOrdersEntity)
            .set({deleted_at: new Date()})
            .where('book_van_bagac_reference = :book_van_bagac_reference', { book_van_bagac_reference })
            .execute();

        const userFCM = await this.firebaseService.findOne(findReference.driver_uuid_accept);
        const dataNotification = {
            key: KEY_CUSTOMER_CANCEL_APPROVE_BOOK_VAN_BAGAC_TO_DRIVER,
            customer_info: {customer_uuid: customerInfo.user_id.toString(), full_name: customerInfo.customerProfile.full_name},
            driver_uuid: findReference.driver_uuid_accept,
            book_van_bagac_reference,
            click_action: KEY_CLICK_ACTION,
        };
        if (userFCM){
            let listDriverDeviceToken = [];
            const token = userFCM.token ? userFCM.token : userFCM.apns_id;
            listDriverDeviceToken.push(token);
            // send notification to customer
            const notification = {
                title: 'Từ chối tài xế !',
                body: 'Khách hàng đã từ chối xe của bạn',
            };
            await this.firebaseService.pushNotificationWithTokens(listDriverDeviceToken, notification,  dataNotification);
        }
        // Emit event to driver
        this.redisService.triggerEventVanBagac({room: 'user_' + findReference.driver_uuid_accept, message: dataNotification});
        return true;
    }

    async cancelFindDrivers(customer_uuid: string, book_van_bagac_reference: string) {
        let findBookBusReference: { route_info: { type_group: VehicleGroup; }; };
        const getCacheOrder = await this.redis.getInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_VAN_BAGAC, FIELD_CUSTOMER_REQUEST_BOOK_VAN_BAGAC);
        if (getCacheOrder.isValidate) {
            const getListOrder = JSON.parse(getCacheOrder.value);
            findBookBusReference = _.find(getListOrder, (item: { route_info: { customer_uuid: string; }; book_van_bagac_reference: string; driver_accept: boolean; }) => {
                return item.route_info.customer_uuid === customer_uuid
                    && item.book_van_bagac_reference === book_van_bagac_reference
                    && item.driver_accept === false;
            });
            if (!findBookBusReference) return false;
            await getConnection()
                .createQueryBuilder()
                .update(VanBagacOrdersEntity)
                .set({deleted_at: new Date()})
                .where('book_van_bagac_reference = :book_van_bagac_reference', { book_van_bagac_reference })
                .execute();

            // remove order by customer uuid
            _.remove(getListOrder, (item: any) => {
                return item.route_info.customer_uuid === customer_uuid
                    && item.book_van_bagac_reference === book_van_bagac_reference
                    && item.driver_accept === false;
            });
            let getCacheListDriver: any, dataNotification: any;
            if (findBookBusReference.route_info.type_group === VehicleGroup.VAN) {
                dataNotification = {
                    key: KEY_TRIGGER_LIST_REQUEST_VAN,
                    type_group: VehicleGroup.VAN,
                    click_action: KEY_CLICK_ACTION,
                };
                getCacheListDriver = await this.redis.getInfoRedis(VEHICLE_POSITIONS_VAN, FIELD_VEHICLE_POSITIONS_VAN);
            }
            if (findBookBusReference.route_info.type_group === VehicleGroup.BAGAC) {
                dataNotification = {
                    key: KEY_TRIGGER_LIST_REQUEST_BAGAC,
                    type_group: VehicleGroup.BAGAC,
                    click_action: KEY_CLICK_ACTION,
                };
                getCacheListDriver = await this.redis.getInfoRedis(VEHICLE_POSITIONS_BAGAC, FIELD_VEHICLE_POSITIONS_BAGAC);
            }
            if (getCacheListDriver && getCacheListDriver.isValidate) {
                const getCacheListDriverParse = JSON.parse(getCacheListDriver.value);
                if (getCacheListDriverParse) {
                    getCacheListDriverParse.map((val: { user_uuid: string; }) => {
                        // Emit event to driver
                        this.redisService.triggerEventVanBagac({room: 'user_' + val.user_uuid, message: dataNotification});
                    });
                }
            }
            return true;
        }
        return  false;
    }

    /*
    * Count order in month with @param is_in_month = true
    * Count order done with @param status = 4
    * */
    async countOrderVanBagac(options?: CountOrderVanBagacInteface): Promise<number> {
        const currentDate = moment(new Date(), 'YYYY-MM').format('YYYY-MM').toString();
        let query = this.vanBagacOrdersRepository.createQueryBuilder('order');
        if (options && options.is_in_month === 'true') {
            query.where('order.start_date like :start_date', {start_date: '%' + currentDate + '%'});
        }
        if (options && options.status) {
            query.andWhere('order.status = :status', {status: options.status });
        }

        if (options && options.from_date) {
            const from_date_tostring = await this.formatDate(options.from_date);
            query.andWhere('order.start_date >= :from_date_tostring', {from_date_tostring});
        }
        if (options && options.to_date) {
            const to_date_tostring = await this.formatDate(options.to_date);
            query.andWhere('order.start_date <= :to_date_tostring', {to_date_tostring});
        }
        if (options && options.driver_id){
            query.andWhere('order.driver_id = :driver_id', {driver_id: options.driver_id});
        }
        return query.getCount();
    }

    async countAmountCustsomer(options?: CountCustomerInterface): Promise<string>{
        let query = await this.vanBagacOrdersRepository.createQueryBuilder('order')
            .select('COUNT(DISTINCT order.customer_id) as amount_customer');
        if (options && options.is_in_month === 'true') {
            const currentDate = moment(new Date(), 'YYYY-MM').format('YYYY-MM').toString();
            query.andWhere('order.created_at like :created_at', {created_at: '%' + currentDate + '%'});
        }
        if (options && options.from_date) {
            const from_date_tostring = await this.formatDate(options.from_date);
            query.andWhere('order.created_at >= :from_date_tostring', {from_date_tostring});
        }
        if (options && options.to_date) {
            const to_date_tostring = await this.formatDate(options.to_date);
            query.andWhere('order.created_at <= :to_date_tostring', {to_date_tostring});
        }
        if (options && options.driver_id) {
            query.andWhere('order.driver_id = :driver_id', {driver_id: options.driver_id});
        }
        return query.getRawOne();
    }

    async revenueByBagac(options: {is_in_month?: string, from_date?: string, to_date?: string, driver_id?: number }) {
        let query = await this.vanBagacOrdersRepository.createQueryBuilder('order')
            .where('status = :status', {status: StatusRequestBooKVanBaGac.COMPLETED});
        if (options) {
            if (options.is_in_month === 'true') {
                const currentDate = moment(new Date(), 'YYYY-MM').format('YYYY-MM').toString();
                query.andWhere('order.start_date like :start_date', {start_date: '%' + currentDate + '%'});
            }
            if (options.from_date) {
                const from_date_tostring = await this.formatDate(options.from_date);
                query.andWhere('order.start_date >= :from_date_tostring', {from_date_tostring});
            }
            if (options.to_date) {
                const to_date_tostring = await this.formatDate(options.to_date);
                query.andWhere('order.start_date <= :to_date_tostring', {to_date_tostring});
            }
            if (options.driver_id) {
                query.andWhere('order.driver_id = :driver_id', {driver_id: options.driver_id});
            }
        }

        return  query.select('SUM(order.fee_tax) as total_price').getRawOne();
    }

    async revenuePerVehicleVanBagac(limit: number = 10, offset: number = 0,
                                    options: {is_in_month?: string, from_date?: string, to_date?: string , driver_id?: number }) {
        let query = await this.vanBagacOrdersRepository.createQueryBuilder('order')
            .leftJoin('order.vehicleVanBagac', 'vehicleVanBagac')
            .leftJoin('order.service', 'service');

        if (options) {
            if (options.is_in_month === 'true') {
                const currentDate = moment(new Date(), 'YYYY-MM').format('YYYY-MM').toString();
                query.where('order.start_date like :start_date', {start_date: '%' + currentDate + '%'});
            }
            if (options.from_date) {
                const from_date_tostring = await this.formatDate(options.from_date);
                query.andWhere('order.start_date >= :from_date_tostring', {from_date_tostring});
            }
            if (options.to_date) {
                const to_date_tostring = await this.formatDate(options.to_date);
                query.andWhere('order.start_date <= :to_date_tostring', {to_date_tostring});
            }
            if (options.driver_id) {
                query.andWhere('order.driver_id <= :driver_id', {driver_id: options.driver_id});
            }
        }
        query.select('DISTINCT(order.vehicle_id), order.vehicle_group_id as vehicle_group_id,' +
            'SUM(order.price) as total_price,' +
            'SUM(order.fee_tax) as total_price_azgo,' +
            ' vehicleVanBagac.license_plates as license_plates,' +
            ' service.name as vehicle_type_name,' +
            ' order.created_at as created_at')
            .groupBy('order.vehicle_id');
        const count = await query.getRawMany();
        const results = await query.limit(Number(limit)).offset(Number(offset)).getRawMany();
        return {
            success: true,
            data: results,
            total: count.length,
        };
    }
    /*
    * Input format: DD/MM/YYYY
    *
    * */
    formatDate(date: string) {
        const dateSplit = date.split('/');
        // month is 0-based, that's why we need dataParts[1] - 1
        const dateObject = new Date(+dateSplit[2],  +dateSplit[1] - 1, +dateSplit[0]);
        const formatDate = moment(dateObject, 'YYYY-MM-DD');
        return formatDate.format('YYYY-MM-DD').toString();
    }
}
