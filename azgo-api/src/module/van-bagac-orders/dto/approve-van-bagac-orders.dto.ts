import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsString} from 'class-validator';

export class  ApproveVanBagacOrdersDto {

    @ApiModelProperty({ example: '1188cbac-94f9-4c04-8d96-17f5bac71530_st6s1c7hiym', description: 'Mã request', type: String, required: true  })
    @IsString()
    @IsNotEmpty()
    book_van_bagac_reference: string;

    @ApiModelProperty({ example: '08:30', description: 'Giờ bắt đầu đi', type: String, required: true  })
    @IsString()
    @IsNotEmpty()
    start_time: string;

    @ApiModelProperty({ example: '2020-09-21 14:00:00', description: 'Thời gian kết thúc', type: String, required: true  })
    @IsString()
    @IsNotEmpty()
    end_time: string;
}
