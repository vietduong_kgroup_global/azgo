import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsString} from 'class-validator';

export class UpdatePaymentStatusDto {

    // customer_uuid
    @ApiModelProperty({example: 'customer_uuid',  required: true})
    @IsString()
    customer_uuid: string;

    @ApiModelProperty({ example: 1, required: true })
    @IsNotEmpty()
    payment_status: number;
}
