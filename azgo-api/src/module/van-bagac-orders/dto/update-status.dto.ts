import { ApiModelProperty } from '@nestjs/swagger';
import {IsNumber} from 'class-validator';

export class UpdateStatusDto {

    @ApiModelProperty({
        enum: [3, 4, 5, 6],
        description: 'MOVING = 3, RECEIVED = 4, DELIVERIED = 5, COMPLETED = 6',
        default: 2, type: String,
    })
    @IsNumber()
    status: number;
}
