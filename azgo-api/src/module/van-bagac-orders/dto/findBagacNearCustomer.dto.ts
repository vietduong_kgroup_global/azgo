import { ApiModelProperty } from '@nestjs/swagger';
import {IsEnum, IsNotEmpty} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';
import {ImageDto} from '../../../general-dtos/image.dto';
export class ReceiverInfo {
    @ApiModelProperty({ example: '0357739234', type: String , required: true  })
    phone: string;

    @ApiModelProperty({ example: 'Johnny Nguyen', type: String , required: true  })
    name: string;

    @ApiModelProperty({ example: 'HCM', type: String , required: true  })
    address: string;
}

export class ServiceOthers {
    @ApiModelProperty({ example: 1, type: Number , required: false  })
    id: number;

    @ApiModelProperty({ example: 'Bốc vác l', type: String , required: false  })
    name: string;

    @ApiModelProperty({ example: 12000, type: Number , required: false  })
    price: number;
}
export class FindBagacNearCustomerDto {
    @IsNotEmpty({ message: 'Start station, 46 Bach Dang' })
    @ApiModelProperty({ type: CoordinatesDto , required: true  })
    start_point: CoordinatesDto;

    @IsNotEmpty({ message: 'End point' })
    @ApiModelProperty({ type: CoordinatesDto, required: true  })
    end_point: CoordinatesDto;

    @IsNotEmpty()
    @ApiModelProperty({ enum: ['VAN', 'BAGAC'], default: 'BAGAC',  example: 'BAGAC', type: String, required: true})
    @IsEnum( ['VAN', 'BAGAC'])
    type_group: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'Vehicle type', type: Number, required: true})
    service_id: number;

    @ApiModelProperty({ example: [ServiceOthers]})
    service_others: [ServiceOthers];

    @IsNotEmpty()
    @ApiModelProperty({type: ReceiverInfo, required: true  })
    receiver_info: ReceiverInfo;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'Thùng hàng ', type: String, required: false})
    luggage_info: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: '20/2/2020', type: String, required: false})
    start_date: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: '14:00', type: String})
    start_hour: string;

    @ApiModelProperty({ type: [ImageDto], required: false})
    images: ImageDto[];

    @ApiModelProperty({ example: 10000 , type: Number, required: true  })
    price: number;

    @ApiModelProperty({ example: 50000 , description: 'Total price = price + price services other', type: Number, required: true  })
    total_price: number;
}
