import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';

export class FindOrdersNearDriverDto {
    @IsNotEmpty()
    @ApiModelProperty({ example: 'VAN', description: 'VAN or BAGAC', type: String , required: true  })
    vehicle_group_name: string;
}
