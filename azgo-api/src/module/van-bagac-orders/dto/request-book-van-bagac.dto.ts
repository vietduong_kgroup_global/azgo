import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsNumber, IsString} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';

export class RequestBookVanBagacDto {

    @ApiModelProperty({ example: 1,  required: true })
    van_bagac_order_id: number;

    // driver_uuid
    @IsNotEmpty()
    @ApiModelProperty({example: 'driver_uuid',  required: true})
    @IsString()
    driver_uuid: string;

    // service_id
    @IsNotEmpty()
    @ApiModelProperty({ required: true, example: 1})
    @IsNumber()
    service_id: number;

    // service_other_id
    @ApiModelProperty({ example: [{id: 1, name: 'boc vac len', price: 10000}]})
    service_others: object;

    // route_info
    @IsNotEmpty({ message: 'Start station, 46 Bach Dang' })
    @ApiModelProperty({ type: CoordinatesDto , required: true  })
    route_info: CoordinatesDto;

    // receiver_info
    @IsNotEmpty()
    @ApiModelProperty({ type: CoordinatesDto, required: true  })
    receiver_info: CoordinatesDto;

    // luggage_info
    @IsNotEmpty({ message: 'Thông tin hành lý' })
    @ApiModelProperty({ example: 'Thùng hàng ', required: false})
    luggage_info: object;

    // start_date
    @IsNotEmpty({ message: 'start_date' })
    @ApiModelProperty({ example: '2020-02-20', required: false})
    start_date: Date;

    // start_hour
    @IsNotEmpty()
    @ApiModelProperty({ example: '14:00'})
    start_hour: string;

    // images
    @ApiModelProperty({ example: {}, required: false})
    images: object;

    @ApiModelProperty({ example: 10000 , required: false  })
    price: number;
}
