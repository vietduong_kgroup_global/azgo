import {
    Body,
    Controller,
    Delete,
    Get,
    HttpStatus,
    Param,
    Post,
    Put,
    Req,
    Res,
    UseGuards,
    UsePipes,
} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { Response } from 'express';
import { VanBagacOrdersService } from './van-bagac-orders.service';
import { FindBagacNearCustomerDto } from './dto/findBagacNearCustomer.dto';
import { UsersService } from '../users/users.service';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { ApproveVanBagacOrdersDto } from './dto/approve-van-bagac-orders.dto';
import { UpdatePaymentStatusDto } from './dto/update-payment-status.dto';
import { UpdateStatusDto } from './dto/update-status.dto';
import * as jwt from 'jsonwebtoken';
import { FindOrdersNearDriverDto } from './dto/findOrdersNearDriver.dto';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import * as moment from 'moment';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import { VehicleGroup } from '@azgo-module/core/dist/utils/enum.ultis';
import { CustomChargeService } from '../custom-charge/custom-charge.service';

@ApiUseTags('Van / Bagac orders')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('van-bagac-orders')
export class VanBagacOrdersController {
    constructor(
        private vanBagacOrdersService: VanBagacOrdersService,
        private userService: UsersService,
        private readonly customChageService: CustomChargeService,
    ) {
    }
    /**
     * Get All vehicle by provider and vehicle type
     * Return number
     * @param res
     * @param request
     */
    @Get('/get-list-ticket-by-van-ba-gac')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all ticket by van ba gac' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'keyword', description: 'search full name driver, customer, book car bike reference', required: false, type: 'string'})
    async getAllVanBaGac(@Res() res: Response, @Req() request) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        try {
            results = await this.vanBagacOrdersService.getAllOrderVanBagac(per_page, offset, request.query);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    @Post('driver-approve')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_VAN_BAGAC]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Driver approve request, push notify to customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @UsePipes(CustomValidationPipe)
    async driverApproRequetsBookBagac(@Req() request, @Body() body: ApproveVanBagacOrdersDto, @Res() res: Response){
        try {
            // get infor driver
            const token = request.headers.authorization.split(' ')[1];
            const driverInfo: any =  jwt.decode(token);
            const data = { ...body, ...{driver_uuid: driverInfo.user_id.toString(), driver_id: driverInfo.id}};

            const result = await this.vanBagacOrdersService.driverApproveOrder(data);
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Book van or bagac exited! ', null));
            return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/check-status-request-order-van-bagac-by-customer')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Check staus request order van bagac by customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async checkRequestOrderVanBgac(@Req() request, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const customerInfo: any =  jwt.decode(token);
            const result = await this.vanBagacOrdersService.checkRequestOrderVanBagac(customerInfo.user_id.toString());
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/check-status-approve-order-van-ba-gac-by-driver')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_VAN_BAGAC]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Check status approve order van/bagac by driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async checkApproveOrderBikeCar(@Req() request, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const driver_info: any =  jwt.decode(token);
            const result = await this.vanBagacOrdersService.checkApproveOrderVanBagac(driver_info.user_id.toString());
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all van bagac order' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getAllVanBaGacOrder(@Res() res: Response){
        try {
            const result = await this.vanBagacOrdersService.getAll();
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/find-bagac-near-customer')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Find van / bagac near customer, create van/bagac order' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async findBagacNearCustomer(@Req() request, @Body() body: FindBagacNearCustomerDto, @Res() res: Response){
        try {

            const from_date = body.start_date.split('/');
            // month is 0-based, that's why we need dataParts[1] - 1
            const dateObject = new Date(+from_date[2],  +from_date[1] - 1, +from_date[0]);
            const formatDate = moment(dateObject, 'YYYY-MM-DD');
            const start_date_fortmat = formatDate.format('YYYY-MM-DD').toString();

            const token = request.headers.authorization.split(' ')[1];
            const customerInfo =  jwt.decode(token);
            const findCustomerByUuid = await this.userService.findOneByUserInfo(customerInfo);
            if (findCustomerByUuid.type !== 3) {
                return res.status(HttpStatus.BAD_REQUEST).send(dataSuccess('Please login account customer', null));
            }
            const created_at = moment( ).format('YYYY-MM-DD HH:mm:ss');
            if (findCustomerByUuid) {
                delete body.start_date;
                const dataVanBagacOrder = {...body, ...{
                        start_date: start_date_fortmat,
                        created_at,
                        customer_id: findCustomerByUuid.id,
                        customer_uuid: findCustomerByUuid.user_id.toString(),
                        customer_phone: findCustomerByUuid.phone,
                        customer_name: findCustomerByUuid.customerProfile.full_name,
                        customer_image_profile: findCustomerByUuid.customerProfile.image_profile,
                    }};
                // find driver
                const result = await this.vanBagacOrdersService.findBagacNearCustomer(dataVanBagacOrder, findCustomerByUuid.user_id.toString());
                if (result) {
                    return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
                }
                return res.status(HttpStatus.BAD_REQUEST).send(dataSuccess('Not found vehicle', null));
            }
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('get-order-van-ba-gac-by-customer')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Roles({function_key: FunctionKey.All, action_key: ActionKey.All})
    @ApiOperation({ title: 'Get list van ba gac by customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async listVanBagacOrderFollowCustomer(@Req() request,  @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const customerInfo: any =  jwt.decode(token);
            const result = await this.vanBagacOrdersService.getAllOrderByCustomer(customerInfo.id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

    }

    @Get('get-order-van-ba-gac-by-driver')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_VAN_BAGAC]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Roles({function_key: FunctionKey.All, action_key: ActionKey.All})
    @ApiOperation({ title: 'Get list activity history van ba gac by driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async listVanBagacOrderFollowDriver(@Req() request,  @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        // decode token
        const driverInfo =  jwt.decode(token);
        const findUserByUuid = await this.userService.findOneByUserInfo(driverInfo); // uuid
        if (findUserByUuid) {
            try {
                const result = await this.vanBagacOrdersService.getAllOrderByDriver(findUserByUuid.id);
                if (result) {
                    return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
                }
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
            } catch (error) {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
            }
        }
        return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));

    }

    @Get('get-order-van-ba-gac-near-driver')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_VAN_BAGAC]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Roles({function_key: FunctionKey.All, action_key: ActionKey.All})
    @ApiOperation({ title: 'Get List order of customer on redis for driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiResponse({ status: 400, description: 'Error' })
    async listVanBagacOrderNearDriver(@Req() request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const driverInfo: any =  jwt.decode(token);
        try {
            const results = await this.vanBagacOrdersService.getListOrderNearDriver(driverInfo.user_id.toString());
            if (results) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', {count: results.length, results}));
            }
            return res.status(HttpStatus.OK).send(dataSuccess('Not found', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('get-order-van-ba-gac-detail-of-customer/:van_bagac_order_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get order detail of customer on redis for driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'van_bagac_order_id', description: 'van bagac order Id', required: true, type: 'number' })
    async getOrderDetailOfCustomer(@Param('van_bagac_order_id') van_bagac_order_id, @Res() res: Response){
        try {
            const result = await this.vanBagacOrdersService.getDatailVanBagacOrderOnRedis(van_bagac_order_id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));
        }
    }

    @Put('update-status/:book_van_bagac_reference')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_VAN_BAGAC]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Driver update status van bagac order' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'book_van_bagac_reference', description: 'book_van_bagac_reference', required: true, type: 'string' })
    async updateStatusVanBagacOrder(
        @Param('book_van_bagac_reference') book_van_bagac_reference,
        @Body() body: UpdateStatusDto, @Res() res: Response,
        @Req() request,
    ){
        try {
            const token = request.headers.authorization.split(' ')[1];
            const driverInfo: any =  jwt.decode(token);
            const order = await this.vanBagacOrdersService.updateVanBagacOrder(driverInfo.user_id.toString(), book_van_bagac_reference, body.status);
            if (order){
                return res.status(HttpStatus.OK).send(dataSuccess('oK', order));
            }
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));
        }
    }

    @Get('/:van_bagac_order_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.CUSTOMER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get detail van bagac order' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'van_bagac_order_id', description: 'van bagac order Id', required: true, type: 'number' })
    async getDetailVanBagacOrder(@Param('van_bagac_order_id') van_bagac_order_id, @Res() res: Response){
        try {
            const result = await this.vanBagacOrdersService.getDatailVanBagacOrder(van_bagac_order_id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/cancel-approve-driver-by-customer/:book_van_bagac_reference')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Cancel approve driver van/bagac by customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'book_van_bagac_reference', description: 'book_van_bagac_reference', required: true, type: String})
    async cancelApproveDriver(@Req() request, @Param('book_van_bagac_reference') book_van_bagac_reference: string, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const decodeToken =  jwt.decode(token);
            const customerInfo = await this.userService.findOneByUserInfo(decodeToken);
            const result = await this.vanBagacOrdersService.cancelApproveDriver(book_van_bagac_reference, customerInfo);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found reference', null));
        }
        catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/cancel-find-driver-van-bagac-by-customer/:book_van_bagac_reference')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Cancel find driver van/bagac by customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'book_van_bagac_reference', description: 'book_van_bagac_reference', required: true, type: String})
    async cancelFindDriver(@Req() request, @Res() res: Response, @Param('book_van_bagac_reference') book_van_bagac_reference: string) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const customerInfo: any = jwt.decode(token);
            const result = await this.vanBagacOrdersService.cancelFindDrivers(customerInfo.user_id.toString(), book_van_bagac_reference);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('OK', null));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found reference or request approved', null));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/count-order-van-bagac/by-admin')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Count order van/bagac by admin' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'is_in_month', description: 'Get order in month', required: false, type: Boolean})
    @ApiImplicitQuery({name: 'status', description: 'Get order by status', required: false, type: Number})
    @ApiImplicitQuery({name: 'from_date', description: '31/3/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'to_date', description: '31/4/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'driver_id', description: 'Driver id', required: false, type: Number})
    async countOrderVanBagac(@Req() request, @Res() res: Response) {
        try {
            const result = await this.vanBagacOrdersService.countOrderVanBagac(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/count-amount-customer-van-bagac/by-admin')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Count amount customer van/bagac by admin' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'from_date', description: '31/3/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'to_date', description: '31/4/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'is_in_month', description: 'Get order in month', required: false, type: Boolean})
    @ApiImplicitQuery({name: 'driver_id', description: 'Driver id', required: false, type: Number})
    async countCustomer(@Req() request, @Res() res: Response) {
        try {
            const result = await this.vanBagacOrdersService.countAmountCustsomer(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('revenue-van-bagac/by-admin')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Revenue van/bagac by admin' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'from_date', description: '31/3/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'to_date', description: '31/4/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'is_in_month', description: 'Get order in month', required: false, type: Boolean})
    @ApiImplicitQuery({name: 'driver_id', description: 'Driver id', required: false, type: Number})
    async revenueInMonthByBagac(@Req() request, @Res() res: Response) {
        try {
            const result = await this.vanBagacOrdersService.revenueByBagac(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result.total_price));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('revenue-per-vehicle/by-van-bagac')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Revenue van/bagac by vehicle' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'from_date', description: '31/3/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'to_date', description: '31/4/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'is_in_month', description: 'Get order in month', required: false, type: Boolean})
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: Number})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: Number})
    @ApiImplicitQuery({name: 'driver_id', description: 'Driver id', required: false, type: Number})
    async revenuePerVehicle(@Req() request, @Res() res: Response) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        try {
            results = await this.vanBagacOrdersService.revenuePerVehicleVanBagac(per_page, offset, request.query);

        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

}
