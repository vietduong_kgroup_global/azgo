import {Column, Entity, JoinColumn, OneToOne, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {IsNotEmpty} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';
import {VehicleTypesEntity} from '../vehicle-types/vehicle-types.entity';
import {CustomerProfileEntity} from '../customer-profile/customer-profile.entity';
import {DriverProfileEntity} from '../driver-profile/driver-profile.entity';
import {VehicleEntity} from '../vehicle/vehicle.entity';
import {CustomChargeEntity} from '../custom-charge/custom-charge.entity';
import {RatingDriverEntity} from '../rating-driver/rating-driver.entity';

@Entity('az_van_bagac_orders')
export class VanBagacOrdersEntity {
    @Exclude()
    @PrimaryGeneratedColumn({type: 'int', unsigned: true})
    id: number;

    @Column({
        type: 'int',
        nullable: false,
    })

    @Column({
        type: 'int',
    })
    customer_id: number;

    @ManyToOne(type => CustomerProfileEntity, user => user.user_id)
    @JoinColumn({name: 'customer_id', referencedColumnName: 'user_id'})
    customerProfile: CustomerProfileEntity;

    @Column({
        type: 'int',
        nullable: false,
    })

    @Column({
        type: 'int',
    })
    driver_id: number;

    @ManyToOne(type => DriverProfileEntity, user => user.user_id)
    @JoinColumn({name: 'driver_id', referencedColumnName: 'user_id'})
    driverProfile: DriverProfileEntity;

    @Column({
        type: 'int',
    })
    vehicle_id: number;

    @ManyToOne(type => VehicleEntity, vehicle => vehicle.vanBagacOrder)
    @JoinColumn({name: 'vehicle_id', referencedColumnName: 'id'})
    vehicleVanBagac: VehicleEntity;

    @Column({
        type: 'int',
    })
    vehicle_group_id: number;

    @ManyToOne(type => CustomChargeEntity, customCharge => customCharge.vanBagacOrders)
    @JoinColumn({name: 'vehicle_group_id', referencedColumnName: 'vehicle_group_id'})
    customCharge: CustomChargeEntity;

    @Column({
        type: 'int',
    })
    service_id: number;

    @ManyToOne(type => VehicleTypesEntity, service => service.id)
    @JoinColumn({name: 'service_id', referencedColumnName: 'id'})
    service: VehicleTypesEntity;

    @Column({
        type: 'json',
    })
    service_others: object;
    // @ManyToOne(type => ServiceOthersEntity, serviceOthers => serviceOthers.id)
    // @JoinColumn({name: 'service_other_id', referencedColumnName: 'id'})
    // service_other: ServiceOthersEntity;

    @Column({
        type: 'json',
    })
    @IsNotEmpty({message: 'Content is require'})
    route_info: object;

    @Column({
        type: 'json',
    })
    @IsNotEmpty({message: 'Content is require'})
    receiver_info: object;

    @Column({
        type: 'varchar',
    })
    @IsNotEmpty({message: 'Content is require'})
    luggage_info: string;

    @Column({
        type: 'datetime',
    })
    @IsNotEmpty({message: 'Content is require'})
    start_date: Date;

    @Column({
        type: 'varchar',
    })
    @IsNotEmpty({message: 'Content is require'})
    start_hour: string;

    @Column({
        type: 'json',
    })
    images: [object];

    @Column('smallint')
    @IsNotEmpty({message: 'Content is require'})
    @ApiModelProperty({example: '', required: true})
    status: number;

    @Column('smallint')
    disable_btn_cancel_approve: number;

    @Column({
        type: 'smallint',
    })
    @ApiModelProperty({ default: 1 }) // 1: Not yet rated, 2: Have evaluated
    flag_rating_customer: number;

    @Column({
        type: 'smallint',
    })
    @ApiModelProperty({ default: 1 }) // 1: Not yet rated, 2: Have evaluated
    flag_rating_driver: number;

    @Column('float')
    price: number;

    @Column('float')
    total_price: number;

    @Column({
        type: 'float',
    })
    rate_of_charge: number;

    @Column({
        type: 'float',
    })
    fee_tax: number;

    @Column({
        type: 'varchar',
    })
    @IsNotEmpty()
    receive_and_delivery_distance: string;

    @Column({
        type: 'smallint',
        nullable: true,
    })
    @ApiModelProperty({ example: 1})
    payment_method: number;

    @Column({
        type: 'smallint',
        nullable: true,
    })
    @ApiModelProperty({ example: 1})
    payment_status: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    @ApiModelProperty({ example: 'Get from data notify by customer'})
    book_van_bagac_reference: string;

    @OneToOne(type => RatingDriverEntity, rating_vanbagac => rating_vanbagac.vanBagacOrder)
    ratingDriver: RatingDriverEntity;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    start_time: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    end_time: string;

    @Column({
        type: 'timestamp',
        nullable: true,
    })
    created_accept: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
    })
    created_moving: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
    })
    created_received: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
    })
    created_deliveried: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
    })
    created_completed: Date;

}
