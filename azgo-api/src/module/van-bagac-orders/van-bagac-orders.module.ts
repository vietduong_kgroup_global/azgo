import {forwardRef, Module} from '@nestjs/common';
import { VanBagacOrdersService } from './van-bagac-orders.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {VanBagacOrdersRepository} from './van-bagac-orders.repository';
import {VanBagacOrdersController} from './van-bagac-orders.controller';
import {CommonService} from '../../helpers/common.service';
import {UsersModule} from '../users/users.module';
import {FirebaseModule} from '../firebase/firebase.module';
import {VehicleMovingModule} from '../vehicle-moving/vehicleMoving.module';
import {FirebaseRepository} from '../firebase/firebase.repository';
import {DriverProfileModule} from '../driver-profile/driver-profile.module';
import {RatingDriverModule} from '../rating-driver/rating-driver.module';
import {CustomChargeModule} from '../custom-charge/custom-charge.module';
import {GoogleMapModule} from '../google-map/google-map.module';
import {GoogleMapService} from '../google-map/google-map.service';
import {WalletModule} from '../wallet/wallet.module';
@Module({
  imports: [
      TypeOrmModule.forFeature([VanBagacOrdersRepository, FirebaseRepository]),
      UsersModule, FirebaseModule,
      forwardRef(() => VehicleMovingModule), // B/c Circular dependency,
      DriverProfileModule,
      RatingDriverModule,
      CustomChargeModule,
      WalletModule,
  ],
  controllers: [VanBagacOrdersController],
  providers: [VanBagacOrdersService, CommonService, GoogleMapService],
  exports: [VanBagacOrdersService],
})
export class VanBagacOrdersModule {}
