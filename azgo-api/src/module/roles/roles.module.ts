import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolesController } from './roles.controller';
import { RolesService } from './roles.service';
import { RolesRepository } from './roles.repository';

@Module({
    imports: [TypeOrmModule.forFeature([RolesRepository])],
    providers: [RolesService],
    exports: [RolesService],
    controllers: [RolesController],
})
export class RolesModule { }
