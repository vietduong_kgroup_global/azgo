import {Entity, Column, PrimaryGeneratedColumn, OneToMany} from 'typeorm';
import {UserRolesEntity} from '../user-roles/user-roles.entity';

@Entity('az_roles')
export class RolesEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '50',
    })
    name: string;

    @Column({
        type: 'varchar',
        length: '255',
    })
    description: string;

    @Column({
        type: 'json',
    })
    permission: object;

    @OneToMany(type => UserRolesEntity, user_role => user_role.roles)
    user_roles: UserRolesEntity[];
}
