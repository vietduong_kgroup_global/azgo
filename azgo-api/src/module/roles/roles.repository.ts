import { EntityRepository, Repository, getManager } from 'typeorm';
import { RolesEntity } from './roles.entity';

@EntityRepository(RolesEntity)
export class RolesRepository extends Repository<RolesEntity> {
    /**
     * This function get role by id
     * @param id
     */
    async findById(id: number): Promise<RolesEntity> {
        return this.findOne(id);
    }

    /**
     * This function get role of user
     * @param user_id
     */
    async getRoleByUserId(user_id: number): Promise<RolesEntity> {
        return this.createQueryBuilder('role')
            .leftJoin('role.user_roles', 'user_roles')
            .where('user_roles.user_id = :user_id', {user_id})
            .getOne();
    }
}