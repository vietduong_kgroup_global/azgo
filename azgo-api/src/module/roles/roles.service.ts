import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RolesEntity } from './roles.entity';
import { RolesRepository } from './roles.repository';

@Injectable()
export class RolesService {
    constructor(
        @InjectRepository(RolesRepository)
        private readonly rolesRepository: RolesRepository,
    ) {
    }

    /**
     * This function get role by id
     * @param id
     */
    async findOne(id: number): Promise<RolesEntity> {
        return this.rolesRepository.findById(id);
    }

    /**
     * This function get role of user
     * @param user_id
     */
    async getRoleByUserId(user_id: number): Promise<RolesEntity> {
        return this.rolesRepository.getRoleByUserId(user_id);
    }

    /**
     * This is function get all roles
     * @param options
     */
    async getAll(options?: any) {
        let name = options.name ? options.name.trim() : null;
        let query = await this.rolesRepository.createQueryBuilder('az_roles');
        if (name) {
            query.where(`name like "%${name}%"`);
        }
        return query.getMany();
    }
}
