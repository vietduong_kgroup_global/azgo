import { EntityRepository, Repository } from 'typeorm';
import { CustomerProfileEntity } from './customer-profile.entity';

@EntityRepository(CustomerProfileEntity)
export class CustomerProfileRepository extends Repository<CustomerProfileEntity> {
    /**
     * This is function find by user id
     * @param user_id
     */
    async findByUserId(user_id: string): Promise<CustomerProfileEntity> {
        return await this.createQueryBuilder('az_customer_profile').where('user_id = :user_id', { user_id }).getOne();
    }
}