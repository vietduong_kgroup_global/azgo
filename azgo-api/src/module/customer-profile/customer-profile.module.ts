import { Module } from '@nestjs/common';
import { CustomerProfileController } from './customer-profile.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CustomerProfileRepository } from './customer-profile.repository';
import { CustomerProfileService } from './customer-profile.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([CustomerProfileRepository]),
  ],
  exports: [
    CustomerProfileService,
  ],
  controllers: [CustomerProfileController],
  providers: [CustomerProfileService],
})
export class CustomerProfileModule {}
