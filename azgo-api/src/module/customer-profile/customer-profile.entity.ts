import {Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn} from 'typeorm';
import { Exclude } from 'class-transformer';
import { UsersEntity } from '../users/users.entity';

@Entity('az_customer_profile')
export class CustomerProfileEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '100',
        nullable: true,
    })
    first_name: string;

    @Column({
        type: 'varchar',
        length: '100',
        nullable: true,
    })
    last_name: string;

    @Column({
        type: 'varchar',
        length: '255',
        nullable: true,
    })
    full_name: string;

    @Column({
        type: 'smallint',
        default: 1,
    })
    gender: number;

    @Column({
        nullable: true,
    })
    birthday: Date;

    @Column({
        type: 'varchar',
        length: '50',
        nullable: true,
    })
    cmnd: string;

    @Column({
        type: 'text',
        nullable: true,
    })
    address: string;

    @Column({
        type: 'text',
        nullable: true,
    })
    email: string;

    @Column({
        type: 'json',
        nullable: true,
    })
    image_profile: object;

    @Column({
        type: 'json',
        nullable: true,
    })
    front_of_cmnd: object;

    @Column({
        type: 'json',
        nullable: true,
    })
    back_of_cmnd: object;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @Column({
        type: 'int',
    })
    user_id: number;

    @OneToOne(type => UsersEntity, user => user.customerProfile)
    @JoinColumn({
        name: 'user_id',
    })
    users: UsersEntity;

}
