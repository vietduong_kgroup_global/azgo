import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CustomerProfileEntity } from './customer-profile.entity';
import { CustomerProfileRepository } from './customer-profile.repository';

@Injectable()
export class CustomerProfileService {
    constructor(
        @InjectRepository(CustomerProfileRepository)
        private readonly customerProfileRepository: CustomerProfileRepository,
    ) { }

    /**
     * This is function get driver profile by user_id
     * @param user_id
     */
    async findByUserId(user_id): Promise<CustomerProfileEntity> {
        return await this.customerProfileRepository.findByUserId(user_id);
    }

    /**
     * This is function get driver profile by user_id
     * @param UserInfo when login
     */
    async findByUserInfo(user_infor): Promise<CustomerProfileEntity> {
        return await this.customerProfileRepository.findOne({user_id: user_infor.user_id});
    }
}
