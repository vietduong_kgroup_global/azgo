import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {RoutesRepository} from './routes.repository';
import {RoutesEntity} from './routes.entity';
import {ProvidersInfoService} from '../providers-info/providers-info.service';
import {CreateRouteDto} from './dto/createRoute.dto';
import {UsersEntity} from '../users/users.entity';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@Injectable()
export class RoutesService {
    constructor(
        @InjectRepository(RoutesRepository)
        private readonly routesRepository: RoutesRepository,
        private readonly providersInfoService: ProvidersInfoService,
    ) {}

    async findAll(limit: number, offset: number, options?: any){
       return await this.routesRepository.getAllRoutes(limit, offset, options);
    }

    async findOne(routeId: number, user?: UsersEntity){
        let query = this.routesRepository.createQueryBuilder('route')
            .leftJoinAndSelect('route.startPoint', 'startPoint')
            .leftJoinAndSelect('route.endPoint', 'endPoint')
            .leftJoinAndSelect('route.provider', 'provider')
            .where('route.id = :routeId', {routeId});

        if (user && user.type === UserType.PROVIDER) {
            query.leftJoin('provider.users', 'user')
                .andWhere('user.id = :user_id', {user_id: user.id});
        }

        return await query.getOne();
    }
    async deleteRoute(routeId: number){
        const obj = await this.routesRepository.findOne({id: routeId });
        if (!obj)
            return false;
        obj.deleted_at = new Date();
        return await this.routesRepository.save(obj);
    }

    async getAllRouteByProvider(provider_id: number) {
        return await this.routesRepository.createQueryBuilder('route')
            .where('route.provider_id = :provider_id', {provider_id})
            .andWhere('route.deleted_at is null')
            .getManyAndCount();
    }
    async create(body: CreateRouteDto) {
        const route = new RoutesEntity();
        let provider = await this.providersInfoService.findById(body.provider_id);
        if (!provider) throw { message: 'Provider not found', status: 404 };
        route.provider = provider;
        const dataSave = {...route, ...body};
        return await this.routesRepository.save(dataSave);
    }
    async update(routeId: number, body?: any) {
        const route = await this.routesRepository.findOne({id: routeId});
        if (!route) throw { message: 'Route not found', status: 404 };
        let provider = await this.providersInfoService.findById(body.provider_id);
        if (!provider) throw { message: 'Provider not found', status: 404 };
        route.provider = provider;
        const dataSave = {...route, ...body};
        return await this.routesRepository.save(dataSave);
    }
}
