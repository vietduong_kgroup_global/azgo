import { EntityRepository, Repository} from 'typeorm';
import { RoutesEntity} from './routes.entity';

@EntityRepository(RoutesEntity)
export class RoutesRepository extends Repository<RoutesEntity>{
    async getAllRoutes(limit: number, offset: number, options?: any){
        const provider_id = !isNaN(Number(options.provider_id).valueOf()) ? Number(options.provider_id).valueOf() : null;

        // create new query
        let query = this.createQueryBuilder('az_routes')
        .leftJoinAndSelect('az_routes.provider', 'provider')
        .leftJoinAndSelect('az_routes.startPoint', 'startPoint')
        .leftJoinAndSelect('az_routes.endPoint', 'endPoint');

        // search by keyword
        if (options.keyword && options.keyword.trim()) {
            query.where(qb => {
                const subQuery = qb.subQuery()
                    .select('az_routes.id')
                    .from(RoutesEntity, 'az_routes')
                    .where('az_routes.name like :name', { name: '%' + options.keyword.trim() + '%' })
                    .orWhere('az_routes.distance like :distance', { distance: '%' + options.keyword.trim() + '%' })
                    .orWhere('az_routes.start_point like :start_point', { start_point: '%' + options.keyword.trim() + '%' })
                    .orWhere('az_routes.end_point like :end_point', { end_point: '%' + options.keyword.trim() + '%' })
                    .getQuery();
                return 'az_routes.id IN ' + subQuery;
            });
        }
        if (provider_id) {
            query.andWhere('az_routes.provider_id = :provider_id', {provider_id});
        }
        query.andWhere('az_routes.deleted_at is null')
            .orderBy('az_routes.id', 'DESC')
            .take(limit)
            .skip(offset);

        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }
    async filterRouter(options?: any){
        // create new query
        let query =  await this.createQueryBuilder('az_routes');

        query.where('vehicle_id', options.vehicle_id );
        // list
        query.orderBy('id', 'DESC');

        const results = await query.getManyAndCount();

        return {
            total: results[1],
            results: results[0],
        };
    }

}
