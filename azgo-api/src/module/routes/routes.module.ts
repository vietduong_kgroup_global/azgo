import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {RoutesRepository} from './routes.repository';
import {RoutesService} from './routes.service';
import {RoutesController} from './routes.controller';
import {ProvidersInfoModule} from '../providers-info/providers-info.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([RoutesRepository]),
        ProvidersInfoModule,
    ],
    exports: [
        RoutesService,
    ],
    providers: [RoutesService],
    controllers: [RoutesController],
})
export class RoutesModule { }
