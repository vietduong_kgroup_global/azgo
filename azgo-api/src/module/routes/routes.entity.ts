import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {ProvidersInfoEntity} from '../providers-info/providers-info.entity';
import {AreaEntity} from '../area/area.entity';

@Entity('az_routes')
export class RoutesEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '255',
    })
    name: string;

    @Column({
        type: 'float',
    })
    distance: number;

    @Column({
        type: 'int',
    })
    start_point: number;

    @Column({
        type: 'int',
    })
    end_point: number;

    @ManyToOne(type => AreaEntity, area => area.id)
    @JoinColumn({name: 'start_point', referencedColumnName: 'id'})
    startPoint: AreaEntity;

    @ManyToOne(type => AreaEntity, area => area.id)
    @JoinColumn({name: 'end_point', referencedColumnName: 'id'})
    endPoint: AreaEntity;

    @Column({
        type: 'int',
    })
    provider_id: number;

    @ManyToOne(type => ProvidersInfoEntity, provider => provider.routes)
    @JoinColumn({name: 'provider_id'})
    provider: ProvidersInfoEntity;

    @Column({
        type: 'json',
    })
    router: object;

    @Column()
    duration: string;

    @Column()
    base_price: number;

    @Column()
    price_per_km: number;

    @Column()
    route_code: string;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;
}
