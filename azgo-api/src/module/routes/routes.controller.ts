import {
    Controller,
    Body,
    Post,
    Get,
    HttpStatus,
    Res,
    Req,
    Param,
    Put,
    Delete, UseGuards,
} from '@nestjs/common';
import {
    ApiUseTags,
    ApiResponse,
    ApiOperation,
    ApiImplicitParam,
    ApiBearerAuth, ApiImplicitQuery,
} from '@nestjs/swagger';
import { Response } from 'express';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { CreateRouteDto } from './dto/createRoute.dto';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { RoutesService } from './routes.service';
import { UpdateRouteDto } from './dto/updateRoute.dto';
import { AuthGuard } from '@nestjs/passport';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import {ProvidersInfoService} from '../providers-info/providers-info.service';

@ApiUseTags('Routes')
@Controller('routes')
export class RoutesController {
    constructor(
        private routesService: RoutesService,
        private readonly providersInfoService: ProvidersInfoService,
    ) {
    }

    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Add new route' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    async routes(@Req() request, @Body(CustomValidationPipe) body: CreateRouteDto, @Res() res: Response) {
        try {
            const result = await this.routesService.create(body);
            return res.status(HttpStatus.OK).send(dataSuccess('Create success', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:route_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update route' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiImplicitParam({ name: 'route_id', description: 'Route Id', required: true, type: 'number' })
    async update(@Req() request, @Body(CustomValidationPipe) body: UpdateRouteDto, @Res() res: Response) {

        try {
            const result = await this.routesService.update(request.params.route_id, body);
            return res.status(HttpStatus.OK).send(dataSuccess('Update success', result));
        }catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get routes' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'provider_id', description: 'Provider ID', required: false, type: 'number'})
    async getRoutes(@Req() request, @Res() res: Response) {
        // get provider
        if (request.user.type === UserType.PROVIDER) {
            try {
                const provider = await this.providersInfoService.findByUserId(request.user.id);
                if (!provider) return res.status(HttpStatus.BAD_REQUEST).send(dataError('Provider not found', null));
                request.query.provider_id = provider.id;
            } catch (e) {
                console.error('[findAllDriverByProvider] get provider: ', e);
                return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Bad requests', null));
            }
        }

        let results, routes = [];
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.routesService.findAll(per_page, offset, request.query);
        } catch (e) {
            console.error('[findAllUsers] error', e);
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        for (let route of results.data) {
            let main_station = 0;
            let additional_station = 0;
            if (route.router != null && route.router.stations) {
                main_station = route.router.stations.length;
            }
            if (route.router != null && route.router.additional_station) {
                additional_station = route.router.additional_station.length;
            }
            route.main_station = main_station;
            route.additional_station = additional_station;
            routes.push(route);
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', routes, results.total, options));
    }

    @Get('/:route_id')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Detail Route' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'route_id', description: 'Route Id', required: true, type: 'number' })
    async detail(@Param('route_id') route_id, @Req() request, @Res() res: Response) {
        let result, main_station, additional_station;
        try {
            result = await this.routesService.findOne(route_id, request.user);
            if (!result)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Route not found', null));

            main_station = 0;
            additional_station = 0;

            if (result.router != null && result.router.stations) {
                main_station = result.router.stations.length;
            }
            if (result.router != null && result.router.additional_station) {
                additional_station = result.router.additional_station.length;
            }
            result.main_station = main_station;
            result.additional_station = additional_station;

            return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:route_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete Route' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'route_id', description: 'Route Id', required: true, type: 'number' })
    async deleteRoute(@Param('route_id') route_id, @Res() res: Response) {
        try {
            const result = await this.routesService.deleteRoute(route_id);
            if (result)
                return res.status(HttpStatus.OK).send(dataSuccess('Delete success', result));
            return res.status(HttpStatus.OK).send(dataError('Delete error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/get-route-by-provider/:provider_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all route by provider' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'provider_id', description: 'Provider Id', required: true, type: 'number' })
    async getAllRouteByProvider(@Param('provider_id') provider_id, @Req() request, @Res() res: Response) {
        // get provider
        if (request.user.type === UserType.PROVIDER) {
            try {
                const provider = await this.providersInfoService.findByUserId(request.user.id);
                if (!provider) return res.status(HttpStatus.BAD_REQUEST).send(dataError('Provider not found', null));
                provider_id = provider.id;
            } catch (e) {
                console.error('[findAllDriverByProvider] get provider: ', e);
                return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Bad requests', null));
            }
        }

        try {
            const result = await this.routesService.getAllRouteByProvider(provider_id);
            if (result)
                return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
            return res.status(HttpStatus.OK).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
