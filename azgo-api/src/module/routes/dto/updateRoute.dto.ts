import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsNotBlank } from '@azgo-module/core/dist/utils/blank.util';

export class UpdateRouteDto {
    @IsNotEmpty({ message: 'Route code is require' })
    @IsNotBlank('name', { message: 'Is not white space' })
    @ApiModelProperty({ example: 'HMTB-10001' })
    route_code: string;

    @IsNotEmpty({ message: 'Name is require' })
    @IsNotBlank('name', { message: 'Is not white space' })
    @ApiModelProperty({ example: 'abc' })
    name: string;

    @ApiModelProperty({ example: '0.5' })
    distance: number;

    @ApiModelProperty({ example: 1, required: true})
    start_point: number;

    @ApiModelProperty({ example: 1, required: true})
    end_point: number;

    @ApiModelProperty({ example: 1 })
    provider_id: number;

    @ApiModelProperty({
        example:
            {
                destinations: [
                    { lat: '10.3692058', lng: '106.310169', address: 'Trạm đến 1'},
                    {lat: '10.2518877', lng: '105.903988', address: 'tram đến 2'},
                ],
                pickup_points: [
                    { lat: '10.3692058', lng: '106.310169', address: 'Trạm đón 1'},
                    {lat: '10.2518877', lng: '105.903988', address: 'tram đón 2'},
                ],
            },
    })
    router: object;

    @ApiModelProperty({ example: '2h' })
    duration: string;

    @ApiModelProperty({ example: '1000' })
    base_price: number;

    @ApiModelProperty({ example: '1000' })
    price_per_km: number;

}
