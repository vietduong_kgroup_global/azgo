import {forwardRef, Module} from '@nestjs/common';
import { RatingCustomerController } from './rating-customer.controller';
import { RatingCustomerService } from './rating-customer.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {UsersModule} from '../users/users.module';
import {CustomerProfileModule} from '../customer-profile/customer-profile.module';
import {AdminProfileModule} from '../admin-profile/admin-profile.module';
import {DriverProfileModule} from '../driver-profile/driver-profile.module';
import {RatingCustomerRepository} from './rating-customer.repository';

@Module({
  imports: [
      TypeOrmModule.forFeature([RatingCustomerRepository]),
      CustomerProfileModule,
      AdminProfileModule,
      DriverProfileModule,
      forwardRef(() => UsersModule),
  ],
  controllers: [RatingCustomerController],
  providers: [RatingCustomerService],
  exports: [RatingCustomerService],

})
export class RatingCustomerModule {}
