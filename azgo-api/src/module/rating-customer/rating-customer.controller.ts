import { Body, Controller, Get, HttpStatus, Param, Post, Req, Res, UseGuards } from '@nestjs/common';
import { RatingCustomerService } from './rating-customer.service';
import { ApiBearerAuth, ApiImplicitParam, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import * as jwt from 'jsonwebtoken';
import { Response } from 'express';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { CreateRatingCustomerDto } from './dto/create-rating-customer.dto';
import { UsersService } from '../users/users.service';
import { AuthGuard } from '@nestjs/passport';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@Controller('driver-rating-customer')
@ApiUseTags('Driver rating customer')
export class RatingCustomerController {
    constructor(
        private readonly ratingCustomerService: RatingCustomerService,
        private readonly userService: UsersService,
    ) {
    }

    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All,
        user_type: [UserType.DRIVER_BIKE_CAR, UserType.DRIVER_VAN_BAGAC, UserType.DRIVER_BUS ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Driver rating customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async ratingDriver(@Req() request, @Body() body: CreateRatingCustomerDto, @Res() res: Response){
        try {
            const token = request.headers.authorization.split(' ')[1];
            const driverInfo =  jwt.decode(token);
            const findUserByUuid = await this.userService.findOneByUserInfo(driverInfo);
            if (findUserByUuid.type === 1
                || findUserByUuid.type === 3
                || findUserByUuid.type === 4
            ) {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Please login account driver', null));
            }

            const data = {...body, ...{driver_id: findUserByUuid.id}};
            const result = await this.ratingCustomerService.create(data);
            if (result){
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('get-rating-by-customer/:customer_id')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Roles({function_key: FunctionKey.All, action_key: ActionKey.All})
    @ApiOperation({ title: 'Avg rating by customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'customer_id', required: true, type: 'number'})
    async getRatingDriverByDriver(@Param('customer_id') customer_id: number, @Res() res: Response){
        try {
            const result = await this.ratingCustomerService.avgRatingCustomer(customer_id);
            if (result){
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
