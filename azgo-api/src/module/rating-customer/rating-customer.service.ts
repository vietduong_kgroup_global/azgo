import { Injectable } from '@nestjs/common';
import {RatingCustomerRepository} from './rating-customer.repository';
import {InjectRepository} from '@nestjs/typeorm';
import {CreateRatingCustomerDto} from './dto/create-rating-customer.dto';

@Injectable()
export class RatingCustomerService {
    constructor(
        @InjectRepository(RatingCustomerRepository)
        private readonly ratingCustomerRepository: RatingCustomerRepository,
    ) {
    }

    async create(body: CreateRatingCustomerDto) {
        return await this.ratingCustomerRepository.save(body);
    }

    async avgRatingCustomer(customer_id: number) {
        const sumRatingCustomer = await this.ratingCustomerRepository
            .createQueryBuilder('ratingCustomer')
            .select(['ratingCustomer.customer_id as customer_id', 'AVG(ratingCustomer.point) as avg_rating'])
            .where('ratingCustomer.customer_id = :customer_id', { customer_id })
            .getRawOne();
        const avgRatingCustomer = sumRatingCustomer.avg_rating;
        return Number(avgRatingCustomer).toFixed(1);
    }
}
