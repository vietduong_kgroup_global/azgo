import {EntityRepository, Repository} from 'typeorm';
import {RatingCustomerEntity} from './rating-customer.entity';
@EntityRepository(RatingCustomerEntity)
export class RatingCustomerRepository extends Repository<RatingCustomerEntity>{

}
