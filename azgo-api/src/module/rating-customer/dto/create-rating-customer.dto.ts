import { ApiModelProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsString, Max, MaxLength, Min, Validate } from 'class-validator';
import { DriverProfileEntityExists } from '../../validator-module/entity-driver-profile-exists.constraint';
export class CreateRatingCustomerDto {

    @Validate(DriverProfileEntityExists, {message: 'Driver profile doesn`t exists'})
    @IsNotEmpty()
    @ApiModelProperty({example: 74, required: true})
    @IsInt()
    customer_id: number;

    @IsNotEmpty()
    @ApiModelProperty({example: 'Xe chay rat em'})
    @IsString()
    @MaxLength(500, {message: 'Content max length is 500'})
    content: string;

    @IsNotEmpty()
    @ApiModelProperty({example: 1, required: true})
    @Max(5)
    @Min(0)
    point: number;
}
