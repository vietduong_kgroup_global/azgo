import {EntityRepository, Repository} from 'typeorm';
import { AdminProfileEntity } from './admin-profile.entity';

@EntityRepository(AdminProfileEntity)
export class AdminProfileRepository extends Repository<AdminProfileEntity> {
    /**
     * This is function find by user id
     * @param user_id
     */
    async findByUserId(user_id: string): Promise<AdminProfileEntity> {
        return await this.createQueryBuilder('az_admin_profile').where('user_id = :user_id', { user_id }).getOne();
    }
}