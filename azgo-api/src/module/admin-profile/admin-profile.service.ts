import { Injectable } from '@nestjs/common';
import { AdminProfileEntity } from './admin-profile.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { AdminProfileRepository } from './admin-profile.repository';

@Injectable()
export class AdminProfileService {
    constructor(
        @InjectRepository(AdminProfileRepository)
        private readonly adminProfileRepository: AdminProfileRepository,
    ) { }

    /**
     * This is function get admin profile by user_id
     * @param user_id
     */
    async findByUserId(user_id): Promise<AdminProfileEntity> {
        return await this.adminProfileRepository.findByUserId(user_id);
    }

    async findByUserInfo(user_infor): Promise<AdminProfileEntity> {
        return await this.adminProfileRepository.findOne({user_id: user_infor.user_id});
    }
}
