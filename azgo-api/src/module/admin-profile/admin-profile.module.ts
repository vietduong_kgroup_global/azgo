import { Module } from '@nestjs/common';
import { AdminProfileController } from './admin-profile.controller';
import { AdminProfileService } from './admin-profile.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdminProfileRepository } from './admin-profile.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([AdminProfileRepository]),
  ],
  exports: [
    AdminProfileService,
  ],
  controllers: [AdminProfileController],
  providers: [AdminProfileService],
})
export class AdminProfileModule { }
