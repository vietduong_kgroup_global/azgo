import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {FunctionSettingsEntity} from './function-settings.entity';

@Injectable()
export class FunctionSettingsService {
    constructor(
        @InjectRepository(FunctionSettingsEntity)
        private readonly functionSettingsRepository: Repository<FunctionSettingsEntity>,
    ) {
    }

    /**
     * This function get function setting by id
     * @param id
     */
    async getOneById(id: number): Promise<FunctionSettingsEntity> {
        return this.functionSettingsRepository.findOne(id);
    }

    /**
     * This function create function setting
     * @param body
     */
    async create(body: any): Promise<FunctionSettingsEntity> {
        let setting = await this.functionSettingsRepository.merge(new FunctionSettingsEntity(), body);
        // setting.action = {crete: 0, read: 0, update: 0, delete: 0};
        return this.functionSettingsRepository.save(setting);
    }

    /**
     * This function update function setting
     * @param id
     * @param body
     */
    async update(id: number, body: any): Promise<FunctionSettingsEntity> {
        let setting = await this.getOneById(id);
        if (!setting) throw { message: 'Function not found', status: 404 };

        let new_setting = await this.functionSettingsRepository.merge(new FunctionSettingsEntity(), body);
        return this.functionSettingsRepository.save(new_setting);
    }

    /**
     * This function delete function setting
     * @param id
     * @param body
     */
    async delete(id: number, body: any): Promise<boolean> {
        let setting = await this.getOneById(id);
        if (!setting) throw { message: 'Function not found', status: 404 };

        return !!this.functionSettingsRepository.delete(setting);
    }

    /**
     * This function get all function setting
     * @param options
     */
    async getAll(options?: any) {
        let query_limit = options.limit || 10;
        let is_load_more = false;

        let query = this.functionSettingsRepository.createQueryBuilder('setting');

        // count list data
        let count_query = query.getCount();

        // pagination
        if (options.last_id)
            query.andWhere('id < :last_id', { last_id: options.last_id });

        // list data
        let settings_query = query.orderBy('id', 'DESC')
            .take(query_limit + 1)
            .getMany();

        let results = await Promise.all([
            count_query,
            settings_query,
        ]);

        if (results[1].length > query_limit) {
            is_load_more = true;
            results[1].pop();
        }

        return {
            count: results[0],
            data: results[1],
            load_more: is_load_more,
        };
    }
}
