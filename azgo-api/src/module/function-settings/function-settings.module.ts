import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FunctionSettingsEntity } from './function-settings.entity';
import { FunctionSettingsService } from './function-settings.service';
import { FunctionSettingsController } from './function-settings.controller';

@Module({
    imports: [
        TypeOrmModule.forFeature([FunctionSettingsEntity]),
    ],
    exports: [
        FunctionSettingsService,
    ],
    providers: [FunctionSettingsService],
    controllers: [FunctionSettingsController],
})
export class FunctionSettingsModule { }
