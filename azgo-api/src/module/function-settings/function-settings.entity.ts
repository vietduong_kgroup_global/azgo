import {Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm';

@Entity('az_function_settings')
export class FunctionSettingsEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '50',
    })
    name: string;

    @Index('function_key_unique', { unique: true })
    @Column({
        type: 'varchar',
        length: '50',
    })
    key: string;

    @Column({
        type: 'json',
        nullable: false,
    })
    action: object = {create: 0, read: 0, update: 0, delete: 0};
}
