import { Body, Controller, Get, HttpStatus, Param, Post, Put, Delete, Req, Res, UseGuards } from '@nestjs/common';
import {
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
    ApiBearerAuth,
} from '@nestjs/swagger';
import { Response } from 'express';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { dataSuccess, dataError, getPaginationData } from '@azgo-module/core/dist/utils/jsonFormat';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { FunctionSettingsService } from './function-settings.service';
import { CreateSettingDto } from './dto/createSetting.dto';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@ApiUseTags('Function Settings')
@Controller('function-settings')
export class FunctionSettingsController {
    constructor(private readonly settingsService: FunctionSettingsService) {
    }

    @Get()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get function settings' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiImplicitQuery({ name: 'last_id', description: 'Last id', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'query_limit', description: 'Size item continued', required: false, type: 'number' })
    async findAll(@Req() request, @Res() res: Response) {
        try {
            let results = await this.settingsService.getAll(request.query);
            return res.status(HttpStatus.OK).send(getPaginationData('oK', results.data, results.count, results.load_more));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Function setting detail' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'number' })
    async detail(@Param('id') id, @Res() res: Response) {
        try {
            const setting = await this.settingsService.getOneById(id);
            if (!setting)
                return res.status(HttpStatus.NOT_FOUND).send(dataSuccess('Function not found', null));
            return res.status(HttpStatus.OK).send(dataSuccess('OK', setting));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Crate new function' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    async createSetting(@Req() request, @Body(CustomValidationPipe) body: CreateSettingDto, @Res() res: Response) {
        try {
            const setting = await this.settingsService.create(body);
            if (!setting)
                return res.status(HttpStatus.CREATED).send(dataError('Created fail', null));

            return res.status(HttpStatus.CREATED).send(dataSuccess('OK', setting));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update function setting' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'number' })
    async update(@Param('id') id, @Body(CustomValidationPipe) body: CreateSettingDto, @Res() res: Response) {
        try {
            const setting = await this.settingsService.update(id, body);
            if (!setting)
                return res.status(HttpStatus.CREATED).send(dataError('Updated fail', null));

            return res.status(HttpStatus.CREATED).send(dataSuccess('OK', setting));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete function setting' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'number' })
    async delete(@Param('id') id, @Body(CustomValidationPipe) body: CreateSettingDto, @Res() res: Response) {
        try {
            await this.settingsService.update(id, body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('OK', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
