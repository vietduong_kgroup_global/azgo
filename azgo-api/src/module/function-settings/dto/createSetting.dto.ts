import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsNotBlank } from '@azgo-module/core/dist/utils/blank.util';

export class CreateSettingDto {

    @IsNotEmpty({ message: 'Name is required' })
    @IsNotBlank('name', { message: 'Is not white space' })
    @ApiModelProperty({ required: true, example: 'Users' })
    name: string;

    @IsNotEmpty({ message: 'Key is required' })
    @IsNotBlank('key', { message: 'Is not white space' })
    @ApiModelProperty({ required: true, example: 'users' })
    key: string;
}
