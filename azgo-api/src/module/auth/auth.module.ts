import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtStrategy } from './jwt.strategy';
import { UsersModule } from '../users/users.module';
import { AuthController } from './auth.controller';
import { FirebaseModule } from '../firebase/firebase.module';
import { CustomChargeModule } from '../custom-charge/custom-charge.module';
import { SettingSystemModule } from '../setting-system/setting-system.module';

@Module({
    imports: [UsersModule, FirebaseModule, CustomChargeModule, SettingSystemModule],
    providers: [AuthService, JwtStrategy],
    exports: [AuthService, JwtStrategy],
    controllers: [AuthController],
})
export class AuthModule { }
