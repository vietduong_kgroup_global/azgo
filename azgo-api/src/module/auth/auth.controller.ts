import { Controller, Get, UseGuards, Body, Post, Req, HttpStatus, Res } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { ApiUseTags, ApiBearerAuth, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { Response } from 'express';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { LoginDto } from './dto/login.dto';
import { LoginSocialDto } from './dto/loginSocial.dto';
import { UsersService } from '../users/users.service';
import { AuthService } from './auth.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { UserUtil } from '@azgo-module/core/dist/utils/user.util';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { UserStatus, UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import { PhoneNumberUtil } from '@azgo-module/core/dist/utils/phone-number.util';
import * as _ from 'lodash';
import { FirebaseService } from '../firebase/firebase.service';
import { CustomChargeService } from '../custom-charge/custom-charge.service';
import { SettingSystemService } from '../setting-system/setting-system.service';
import { CurrentRole } from '../../enums/type.enum';

@ApiUseTags('Auth')
@Controller('auth')
export class AuthController {
    constructor(
        private readonly userService: UsersService,
        private readonly authService: AuthService,
        private readonly firebaseService: FirebaseService,
        private readonly customChargeService: CustomChargeService,
        private readonly settingSystemService: SettingSystemService,
    ) { }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @Post('login')
    @ApiOperation({ title: 'Login' })
    @ApiResponse({ status: 200, description: 'Login success' })
    @ApiResponse({ status: 400, description: 'Bad request' })
    @ApiResponse({ status: 401, description: 'UNAUTHORIZED' })
    async login(@Body(CustomValidationPipe) body: LoginDto, @Res() res: Response) {
        let rs, token, errorUsername, area_uuid;
        let username = body.username;
        if (!body.current_role) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Role was not found', null));
        } else {

        }
        const checkExistMail = _.indexOf(username, '@');
        // check country code
        if (checkExistMail === -1) {
            // check phone number
            let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(body.username, body.country_code);
            if (!data_phone_number) {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Invalid phone number', null));
            }
            username = (data_phone_number.national_number).toString();
            errorUsername = 'phone';

        } else {
            errorUsername = 'email';
        }
        // check user login
        try {
            rs = await this.userService.findOneByEmailOrPhone(username);
            if (!rs.user)
                return res.status(HttpStatus.UNAUTHORIZED).send(dataError(`Password or ${errorUsername} incorrect`, null));
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('User not found' || 'Error', null));
        }
        if (!rs.user.checkIfUnencryptedPasswordIsValid(body.password))
            return res.status(HttpStatus.UNAUTHORIZED).send(dataError(`Password or ${errorUsername} incorrect`, null));

        if (rs.user.status !== UserStatus.ACTIVE) {
            return res.status(HttpStatus.UNAUTHORIZED).send(dataError('User is not active', null));
        }

        if (rs.user.block) {
            return res.status(HttpStatus.UNAUTHORIZED).send(dataError('User blocked', null));
        }
        await this.firebaseService.saveFireBaseData(body.device_type, body.device_token, rs.user.id, rs.user.user_id);
        let customCharge = await this.customChargeService.getByVehicleGroupName(['BIKE', 'CAR', 'TOUR']);
        let setting = await this.settingSystemService.getOne();
        let setting_system: any = {};
        customCharge.forEach(item => {
            setting_system[`azgo_fee_${item.vehicleGroupEntity.name.toLowerCase()}`] = Number(item.rate_of_charge / 100);
        });
        setting_system.azgo_fee_vat = setting.vat;
        setting_system.azgo_fee_tax = setting.tax;
        setting_system.hours_allow_cancel = setting.hours_allow_cancel;
        // if (!Array.isArray(rs.user.roles)) rs.user.roles = JSON.parse(rs.user.roles);
        // get area_uuid
        if (rs.user.type === UserType.REGION_MANAGER) {
            area_uuid = rs.user.adminProfile ? rs.user.adminProfile.area_uuid : ''
        }
        // create jwt token
        let data: JwtPayload = {
            email: rs.user.email,
            phone: rs.user.phone,
            id: rs.user.id,
            user_id: rs.user.user_id_str,
            role: (!!rs.user.user_roles && rs.user.user_roles.length > 0) ? rs.user.user_roles[0].roles.id : null, // phase 1: 1 role
            platform: rs.user.platform,
            type: rs.user.type,
            // kiem tra username la email(dang nhap tu admin) thi them company vao token
            company: (rs.user.adminProfile && checkExistMail !== -1) ? rs.user.adminProfile.company : '',
            area_uuid: area_uuid || '',
            azgoroles: rs.user.roles || [],
            current_role: body.current_role || '',
        };

        try {
            token = await this.authService.createToken(data);
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }
        rs.user = UserUtil.serialize(rs.user);
        if (!rs.user.roles) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('User has no role' || 'Error', null));
        } else {
            if (body.current_role == CurrentRole.ADMIN) {
                let index = rs.user.roles.indexOf(1); //Admin
                if (index == -1) {
                    return res.status(HttpStatus.BAD_REQUEST).send(dataError('Your role is incorrect' || 'Error', null));
                }
            } else if (body.current_role == CurrentRole.CUSTOMER) {
                let index = rs.user.roles.indexOf(3); //Customer
                if (index == -1) {
                    return res.status(HttpStatus.BAD_REQUEST).send(dataError('Your role is incorrect' || 'Error', null));
                }
            }
        }
        rs.user._token = token;
        rs.user.company = (rs.user.adminProfile && checkExistMail !== -1) ? rs.user.adminProfile.company : '';
        rs.user.permission = (!!rs.user.user_roles && rs.user.user_roles.length > 0) ? rs.user.user_roles[0].permission : null,
            rs.user.setting_system = setting_system;  // 
        delete rs.user.user_roles;
        return res.status(HttpStatus.OK).send(dataSuccess('Login success', { ...rs.user, ...rs.vehicle }));
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @Post('login-social')
    @ApiOperation({ title: 'Login Social' })
    @ApiResponse({ status: 200, description: 'Login success' })
    @ApiResponse({ status: 400, description: 'Bad request' })
    @ApiResponse({ status: 401, description: 'UNAUTHORIZED' })
    async loginSocial(@Body(CustomValidationPipe) body: LoginSocialDto, @Res() res: Response) {
        let user, token, result, area_uuid;
        // check access token exist
        try {
            result = await this.userService.findUserByAccessToken(body.access_token, body.social_type);
            if (!result)
                return res.status(HttpStatus.UNAUTHORIZED).send(dataError('Access Token invalid', null));
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }
        // find user by social id
        try {
            user = await this.userService.findUserBySocialId(result.data.id, body.social_type);
            if (!user)
                return res.status(HttpStatus.UNAUTHORIZED).send(dataError('Unregistered User', null));
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }

        if (user.status !== UserStatus.ACTIVE) {
            return res.status(HttpStatus.UNAUTHORIZED).send(dataError('User is not active', null));
        }

        if (user.block) {
            return res.status(HttpStatus.UNAUTHORIZED).send(dataError('User blocked', null));
        }

        let customCharge = await this.customChargeService.getByVehicleGroupName(['BIKE', 'CAR', 'TOUR']);
        let setting_system: any = {};
        customCharge.forEach(item => {
            setting_system[`azgo_fee_${item.vehicleGroupEntity.name.toLowerCase()}`] = Number(item.rate_of_charge / 100);
        });
        // get area_uuid
        if (body.current_role === 'admin') {
            area_uuid = user.adminProfile ? user.adminProfile.area_uuid : ''
        }
        else if (body.current_role === 'driver') {
            area_uuid = user.driverProfile ? user.driverProfile.area_uuid : ''
        }
        // create jwt token
        let data: JwtPayload = {
            email: user.email,
            phone: user.phone,
            id: user.id,
            user_id: user.user_id_str,
            role: user.user_roles[0].roles.id, // phase 1: 1 role
            platform: user.platform,
            type: user.type,
            company: '',
            area_uuid: area_uuid || '',
            azgoroles: user.roles || [],
            current_role: body.current_role || '',
        };

        try {
            token = await this.authService.createToken(data);
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }

        user = UserUtil.serialize(user);
        user._token = token;
        user.permission = user.user_roles[0].permission;  // phase 1: 1 role
        user.setting_system = setting_system;  // 
        delete user.user_roles;

        return res.status(HttpStatus.OK).send(dataSuccess('Login success', user));
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Get('user')
    @ApiOperation({ title: 'User Profile' })
    @ApiResponse({ status: 200, description: 'Users data.' })
    @ApiResponse({ status: 401, description: 'Unauthorized.' })
    async profile(@Req() request, @Res() res: Response) {
        try {
            const user = await UserUtil.serialize(request.user);
            return res.status(HttpStatus.OK).send(dataSuccess('Login success', user));
        } catch (e) {
            return res.status(e.statusCode || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }
    }
}
