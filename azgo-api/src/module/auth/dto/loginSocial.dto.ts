import { ApiModelProperty } from '@nestjs/swagger';
import {IsEnum, IsNotEmpty, IsNumber, IsOptional} from 'class-validator';
import { IsNotBlank } from '@azgo-module/core/dist/utils/blank.util';
import {SocialType} from '../../../enums/type.enum';

export class LoginSocialDto {
    @IsNotBlank('access_token', { message: 'Is not white space' })
    @IsNotEmpty({ message: 'Access token is required' })
    @ApiModelProperty({ required: true, example: 'EAAHJJCgtbsUBABEQf4I1URfM7VOgHiUpjbXoHaPWbUFP0ZBwrav04aZBOi9SQmZBDNSbrTZBMtbf5FhHU646Hzg3rWOnkK8UFJxdWwrxuhzI06irxdFC4A4FugMqpzL3Q40VxLgOnDsuOXm7CDFoAdeyDotBLlMy5Li1peMNccObB4GePLPk6Wh6oZAimBy7fh33lbHi4dPWPV3q2u3tTB7sx0BEoh40hIpgleHmxTUn3ldYz9F0Y' })
    access_token: string;

    @ApiModelProperty({ enum: [1, 2], required: true, default: 1 })
    @IsEnum(SocialType)
    social_type: SocialType;

    @ApiModelProperty({ example: 'driver, customer', type: String })
    current_role: string; 
}
