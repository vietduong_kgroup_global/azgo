import { ApiModelProperty } from '@nestjs/swagger';
import {IsEnum, IsNotEmpty, IsString} from 'class-validator';
import { IsNotBlank } from '@azgo-module/core/dist/utils/blank.util';
import {DeviceType,CurrentRole} from '../../../enums/type.enum';

export class LoginDto {

    @IsNotEmpty()
    @IsNotBlank('username', { message: 'Is not white space' })
    @ApiModelProperty({ required: true, example: '975716503', type: String})
    username: string;

    @IsNotEmpty()
    @IsNotBlank('password', { message: 'Is not white space' })
    @ApiModelProperty({ format: 'password', example: '123456', type: String })
    password: string;

    @ApiModelProperty({ example: '84', required: false, default: 84 , type: Number})
    country_code: number;

    @IsNotEmpty()
    @ApiModelProperty({ enum: ['ios', 'android', 'other'], default: 'android', type: String })
    @IsEnum(DeviceType)
    device_type: DeviceType;

    @ApiModelProperty({ example: 'device_id_token...', type: String })
    device_token: string;

    @IsNotEmpty()
    @ApiModelProperty({ required: true,enum: ['driver', 'customer', 'admin'],example: 'driver, customer,admin', type: String })
    @IsEnum(CurrentRole)
    current_role: string; 
}
