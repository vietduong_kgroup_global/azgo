import * as jwt from 'jsonwebtoken';
import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import config from '../../../config/config';

@Injectable()
export class AuthService {

    constructor(
        private readonly usersService: UsersService,
    ) { }

    /**
     * Create token
     * @param data
     * @returns token
     * otherwise return false
     */
    async createToken(dataLogin) {
        const user: JwtPayload = {
            email: dataLogin.email,
            phone: dataLogin.phone,
            user_id: dataLogin.user_id,
            id: dataLogin.id,
            role: dataLogin.role,
            platform: dataLogin.platform,
            type: dataLogin.type,
            company: dataLogin.company || '',
            area_uuid: dataLogin.area_uuid || '',
            azgoroles: dataLogin.azgoroles || [],
            current_role: dataLogin.current_role || '',
        };
        let expiresIn = config.jwtExpiresIn; /* 2 days */
        if (dataLogin.platform === 2 || dataLogin.platform === 3) { // platform mobile
            expiresIn = '365d';
        }
        return await jwt.sign(user, config.jwtSecret, { expiresIn });
    }

    /**
     * validate user by email
     * @param payload
     * @returns {Promise<any>}
     */
    async validateUser(payload: JwtPayload): Promise<any> {
        try {
            return await this.usersService.findOneByEmail(payload.email);
        } catch (e) {
            throw {
                status: 400,
                message: 'Validate user error',
            };
        }
    }

    /**
     * validate user by email
     * @param payload
     * @returns {Promise<any>}
     */
    async validateUserByPhone(payload: JwtPayload): Promise<any> {
        try {
            return await this.usersService.findOneByPhone(payload.phone);
        } catch (e) {
            throw {
                status: 400,
                message: 'Validate user error',
            };
        }
    }
}
