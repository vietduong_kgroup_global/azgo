import { ExtractJwt, Strategy } from 'passport-jwt';
import { AuthService } from './auth.service';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import config from '../../../config/config';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: config.jwtSecret,
        });
    }

    async validate(payload: JwtPayload, done: (message, data) => void) {

        const user = await this.authService.validateUserByPhone(payload);
        if (!user) {
            return done(new UnauthorizedException(), false);
        }
        return done(null, user);
    }
}
