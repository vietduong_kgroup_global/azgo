export interface JwtPayload {
    email: string;
    phone: string;
    user_id: string;
    id: number;
    role: number;
    platform?: number;
    type: number;
    company: string;
    area_uuid: string;
    azgoroles: object;
    current_role: string;
}
