import { Module } from '@nestjs/common';
import { UserAccountLogController } from './user-account-log.controller';
import { UserAccountLogService } from './user-account-log.service';

@Module({
  controllers: [UserAccountLogController],
  providers: [UserAccountLogService]
})
export class UserAccountLogModule {}
