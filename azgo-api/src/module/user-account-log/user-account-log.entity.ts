
import { Entity, PrimaryGeneratedColumn, Generated, JoinColumn, ManyToOne, OneToOne, Column, BaseEntity } from 'typeorm';
import { CustomerProfileEntity } from '../customer-profile/customer-profile.entity';
import { DriverProfileEntity } from '../driver-profile/driver-profile.entity';
import { UsersEntity } from '../users/users.entity';
import { AccountsEntity } from '../accounts/accounts.entity';
import { Exclude } from 'class-transformer';
import { IsInt, IsNotEmpty } from 'class-validator';

@Entity('az_user_account_log')
export class UserAccountLogEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    @Generated('uuid')
    @IsNotEmpty()
    uuid: string;

    @Column({
        type: 'int',
        nullable: false,
    })
    @IsNotEmpty()
    account_id: number;

    @ManyToOne(type => AccountsEntity, account => account.userAccountLog)
    @JoinColumn({ name: 'account_id', referencedColumnName: 'id' })
    accountProfile: AccountsEntity;

    @Column({
        type: 'int',
        nullable: false,
    })
    @IsNotEmpty()
    user_id: number;

    @ManyToOne(type => UsersEntity, user => user.userAccountLog)
    @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
    userProfile: UsersEntity;

    @Column({
        type: 'int',
        nullable: false,
    })
    @IsNotEmpty()
    user_action_id: number;

    @ManyToOne(type => UsersEntity, user => user.userAccountLog)
    @JoinColumn({ name: 'user_action_id', referencedColumnName: 'id' })
    userActionProfile: UsersEntity;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    type: string;

    @Column({
        type: 'json',
        comment: 'Thông tin bị thay đổi',
    })
    data: object;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;
}