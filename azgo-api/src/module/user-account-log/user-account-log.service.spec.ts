import { Test, TestingModule } from '@nestjs/testing';
import { UserAccountLogService } from './user-account-log.service';

describe('UserAccountLogService', () => {
  let service: UserAccountLogService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserAccountLogService],
    }).compile();

    service = module.get<UserAccountLogService>(UserAccountLogService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
