import { Test, TestingModule } from '@nestjs/testing';
import { UserAccountLogController } from './user-account-log.controller';

describe('UserAccountLog Controller', () => {
  let controller: UserAccountLogController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserAccountLogController],
    }).compile();

    controller = module.get<UserAccountLogController>(UserAccountLogController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
