import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {UserRolesEntity} from './user-roles.entity';
import {RolesType} from '@azgo-module/core/dist/common/guards/roles.guards';
import {UsersService} from '../users/users.service';

@Injectable()
export class UserRolesService {
    constructor(
        @InjectRepository(UserRolesEntity)
        private readonly userRolesRepository: Repository<UserRolesEntity>,
        private readonly usersService: UsersService,
    ) {
    }

    /**
     * This function get all permission by user and role
     * @param user_id
     */
    async getPermissionByUserIdAndRoleId(user_id: string) {
        /*=== Phase 1: 1 role ===*/
        return this.userRolesRepository
            .createQueryBuilder('user_roles')
            .leftJoin('user_roles.users', 'users')
            .where('users.user_id = :user_id', {user_id})
            .getOne();
    }

    /**
     * This function update permission by user_id
     * @param user_id
     * @param body
     * @param role
     * @param actor
     */
    async updatePermissionByUserId(user_id: string, body: any, role: number, actor: number) {
        console.log("updatePermissionByUserId: ");
        // check role -> if admin -> pass || if PO or PM -> check is parent user || !admin | !PO | !Pm -> return false
        if ([RolesType.Master_Admin, RolesType.Plant_Owner, RolesType.Project_Manager].indexOf(role) < 0)
            throw {status: 403, message: 'Forbidden'};

        if ([RolesType.Plant_Owner, RolesType.Project_Manager].indexOf(role) > 0) {
            let isParent = await this.usersService.isExistParentByUserId(user_id, actor);
            if (!isParent) throw {status: 403, message: 'Forbidden'};
        }

        // Validate body permission
        let errors;
        for (let permission of body.permission) {
            let functional = permission[Object.keys(permission)[0]];
            if ([0, 1].indexOf(functional.create) < 0)
                errors = (Object.keys(permission)[0] + ': create action is require');
            if ([0, 1].indexOf(functional.update) < 0)
                errors = (Object.keys(permission)[0] + ': update action is require');
            if ([0, 1].indexOf(functional.delete) < 0)
                errors = (Object.keys(permission)[0] + ': delete action is require');
            if ([0, 1].indexOf(functional.read) < 0)
                errors = (Object.keys(permission)[0] + ': read action is require');

            if (errors) break;
        }
        if (errors) throw {status: 400, message: errors};

        let user_roles = await this.getPermissionByUserIdAndRoleId(user_id);
        if (!user_roles) throw {status: 404, message: 'user does not assign role'};
        user_roles = await this.userRolesRepository.merge(user_roles, body);

        return this.userRolesRepository.save(user_roles);
    }

    /**
     * This function update permission by id
     * @param id
     * @param body
     */
    async updatePermissionById(id: number, body: any) {
        let user_roles = await this.userRolesRepository.findOne(id);
        if (!user_roles) throw {status: 404, message: 'user role not found'};
        user_roles = await this.userRolesRepository.merge(user_roles, body);

        return this.userRolesRepository.save(user_roles);
    }
}
