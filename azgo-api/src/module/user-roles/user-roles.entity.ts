import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm';
import {UsersEntity} from '../users/users.entity';
import {RolesEntity} from '../roles/roles.entity';

@Entity('az_user_roles')
export class UserRolesEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'json',
    })
    permission: object;

    @ManyToOne(type => UsersEntity, user => user.user_roles, { cascade: true, onDelete: 'CASCADE', onUpdate: 'RESTRICT' })
    @JoinColumn({ name: 'user_id' })
    users: UsersEntity;

    @ManyToOne(type => RolesEntity, role => role.user_roles, { cascade: true, onDelete: 'CASCADE', onUpdate: 'RESTRICT' })
    @JoinColumn({ name: 'role_id' })
    roles: RolesEntity;
}
