import { Body, Controller, Get, HttpStatus, Param, Put, Req, Res, UseGuards } from '@nestjs/common';
import {
    ApiImplicitParam,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
    ApiBearerAuth,
} from '@nestjs/swagger';
import { Response } from 'express';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import { UserRolesService } from './user-roles.service';
import { UpdateUserRoleDto } from './dto/updateUserRole.dto';
import * as jwt from 'jsonwebtoken';

@ApiUseTags('User Roles')
@Controller('user-roles')
export class UserRolesController {
    constructor(
        private readonly userRolesService: UserRolesService,
    ) { }

    @Get('user/:user_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get permission by user_id' })
    @ApiResponse({ status: 200, description: 'Updated' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiImplicitParam({ name: 'id', description: 'User UUID', required: true, type: 'string' })
    async getPermissionByUserId(@Param('user_id') user_id, @Res() res: Response) {
        try {
            let user_roles = await this.userRolesService.getPermissionByUserIdAndRoleId(user_id);
            if (!user_roles)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Role not found', null));
            return res.status(HttpStatus.OK).send(dataError('OK', user_roles));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:user_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update role by user' })
    @ApiResponse({ status: 200, description: 'Updated' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiImplicitParam({ name: 'id', description: 'User UUID', required: true, type: 'string' })
    async update(@Param('user_id') user_id, @Req() request, @Body() body: UpdateUserRoleDto, @Res() res: Response) {
        // get token in header
        const token = request.headers.authorization.split(' ')[1];
        let token_decoded: any = jwt.decode(token);

        if (!body.permission)
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('permission is require', null));

        try {
            let user_roles = await this.userRolesService.updatePermissionByUserId(user_id, body, token_decoded.role, request.user.id);
            if (!user_roles)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Update role failed', null));
            return res.status(HttpStatus.OK).send(dataError('Update success', user_roles));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
