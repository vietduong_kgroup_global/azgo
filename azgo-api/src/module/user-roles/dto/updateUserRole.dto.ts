import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateUserRoleDto {

    @ApiModelProperty({ example: [
            {users: {read: 1, create: 1, delete: 1, update: 1}},
            {plants: {read: 1, create: 1, delete: 1, update: 1}},
        ],
    })
    permission: object[];
}
