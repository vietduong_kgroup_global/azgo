import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRolesEntity } from './user-roles.entity';
import { UserRolesController } from './user-roles.controller';
import { UserRolesService } from './user-roles.service';
import { UsersModule } from '../users/users.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([UserRolesEntity]),
        UsersModule,
    ],
    exports: [UserRolesService],
    providers: [UserRolesService],
    controllers: [UserRolesController],
})
export class UserRolesModule { }
