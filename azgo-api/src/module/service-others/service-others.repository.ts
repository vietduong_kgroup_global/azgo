import {EntityRepository, Repository} from 'typeorm';
import {ServiceOthersEntity} from './service-others.entity';
@EntityRepository(ServiceOthersEntity)
export class ServiceOthersRepository extends Repository<ServiceOthersEntity>{

}
