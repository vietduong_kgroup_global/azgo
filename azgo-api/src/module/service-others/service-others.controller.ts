import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiImplicitParam, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { ServiceOthersService } from './service-others.service';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { Response } from 'express';
import { CreateServiceOthersDto } from './dto/create-service-others.dto';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@ApiUseTags('Service others')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('service-others')
export class ServiceOthersController {
    constructor(
        private serviceOthersService: ServiceOthersService,
    ) {
    }

    @Get()
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all service others' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getAllBookShipping(@Res() res: Response){
        try {
            const result = await this.serviceOthersService.getAll();
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create service others' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async create(@Res() res: Response, @Body() body: CreateServiceOthersDto){
        try {
            const result = await this.serviceOthersService.create(body);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update service others' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'id', description: 'service id', type: 'number', required: true})
    async update(@Res() res: Response, @Body() body: CreateServiceOthersDto, @Param('id') id: number){
        try {
            const result = await this.serviceOthersService.update(id, body);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete service others' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'id', description: 'service id', type: 'number', required: true})
    async delete(@Res() res: Response, @Param('id') id: number){
        try {
            const result = await this.serviceOthersService.delete(id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
