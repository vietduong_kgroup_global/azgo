import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {IsNotEmpty, IsString} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';

@Entity('az_service_others')
export class ServiceOthersEntity {
    @Exclude()
    @PrimaryGeneratedColumn({type: 'bigint', unsigned: true})
    id: number;

    @Column('text')
    @IsString({message: 'Content must be a string'})
    @IsNotEmpty({message: 'Content is require'})
    @ApiModelProperty({example: 'Name service others', required: true})
    name: string;

    @Column('decimal')
    @IsNotEmpty({message: ''})
    @ApiModelProperty({example: '4.5', required: true})
    price: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;
}
