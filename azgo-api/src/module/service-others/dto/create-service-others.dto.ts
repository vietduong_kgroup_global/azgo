import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString} from 'class-validator';

export class CreateServiceOthersDto {

    @IsNotEmpty()
    @ApiModelProperty({example: 'Bốc vác lên'})
    @IsString()
    name: string;

    @IsNotEmpty()
    @ApiModelProperty({example: 12000, required: true})
    price: number;
}
