import { Module } from '@nestjs/common';
import { ServiceOthersService } from './service-others.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ServiceOthersController} from './service-others.controller';
import {ServiceOthersRepository} from './service-others.repository';
@Module({
  imports: [
    TypeOrmModule.forFeature([ServiceOthersRepository]),
  ],
  controllers: [ServiceOthersController],
  providers: [ServiceOthersService],
})
export class ServiceOthersModule {}
