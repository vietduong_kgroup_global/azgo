import { Injectable } from '@nestjs/common';
import { ServiceOthersRepository} from './service-others.repository';
import {InjectRepository} from '@nestjs/typeorm';
import {CreateServiceOthersDto} from './dto/create-service-others.dto';

@Injectable()
export class ServiceOthersService {
    constructor(
        @InjectRepository(ServiceOthersRepository)
        private readonly serviceOthersRepository: ServiceOthersRepository,
    ){
    }
    async getAll(){
        return  await this.serviceOthersRepository.createQueryBuilder('service')
            .where('service.deleted_at is null')
            .orderBy('service.id', 'ASC')
            .getMany();
    }
    async create(body: CreateServiceOthersDto){
        return  await this.serviceOthersRepository.save(body);
    }

    async update(id: number, body: CreateServiceOthersDto){
        const findOne = await this.serviceOthersRepository.findOne({id});
        if (!findOne) {
            return false;
        }
        return  await this.serviceOthersRepository.save({...findOne, ...body});
    }

    async delete(id: number){
        const findOne = await this.serviceOthersRepository.findOne({id});
        if (!findOne) {
            return false;
        }
        findOne.deleted_at = new Date();
        return  await this.serviceOthersRepository.save(findOne);
    }

}
