import {HttpStatus, Injectable} from '@nestjs/common';
import {FirebaseService} from '../firebase/firebase.service';
import {RedisService} from '@azgo-module/core/dist/background-utils/Redis/redis.service';
import config from '@azgo-module/core/dist/config';
import {RedisUtil} from '@azgo-module/core/dist/utils/redis.util';
import {TicketService} from '../ticket/ticket.service';
import * as _ from 'lodash';
import {VehicleMovingService} from '../vehicle-moving/vehicleMoving.service';
import {RejectBookBusDto} from './dto/reject-book-bus.dto.ts';
import {GoogleMapService} from '../google-map/google-map.service';
import {VnPayService} from '../vn-pay/vn-pay.service';
import {InjectSchedule, Schedule} from 'nest-schedule';
import {
    KEY_CANCEL_APPROVE_REQUEST_BOOK_BUS_BY_CUSTOMER,
    KEY_CANCEL_APPROVED_BUS, KEY_CANCEL_FIND_BOOK_BUS_BY_CUSTOMER,
    KEY_CANCEL_FIND_BUS,
    KEY_CLICK_ACTION,
    KEY_DRIVER_ACCEPT_REQUEST_BOOK_BUS_FROM_CUSTOMER, KEY_DRIVER_REJECT_REQUEST_BOOK_BUS_FROM_CUSTOMER,
    KEY_NOTIFY_PAID_BOOK_BUS_TO_CUSTOMER, KEY_TRIGGER_SEAT_MAP,
    REQUEST_BOOK_BUS_FROM_CUSTOMER,
} from '../../constants/keys.const';
import {StatusCode, StatusPayment, StatusPaymentMethod, StatusRequestBookBus} from '../../enums/status.enum';
import {VehicleGroup} from '../../enums/group.enum';
import {TicketType} from '../../enums/type.enum';
import {SECRET_BOOK_TICKET, SECRET_BOOK_TICKET_BY_ADMIN} from '../../constants/secrets.const';
import {TicketEntity} from '../ticket/ticket.entity';
import {VehicleScheduleService} from '../vehicle-schedule/vehicleSchedule.service';

@Injectable()
export class BusOrderService {
    private redisService;
    private redis;
    // Book shipping
    private keyRedisCustomerRequestBookShipping = 'key_customer_request_book_shipping';
    private fieldRedisCustomerRequestBookShipping = 'field_customer_request_book_shipping';
    // Book bus
    private keyRedisCustomerRequestBookBus = 'key_customer_request_book_bus';
    private fieldRedisCustomerRequestBookBus = 'field_customer_request_book_bus';
    // use for vehicle bus
    private keyRedisVehiclePositionBus = 'vehicle_positions_bus';
    private fieldRedisVehiclePositionBus = 'field_vehicle_positions_bus';
    // use for vehicle bike
    private keyRedisVehiclePositionBike = 'vehicle_positions_bike';
    private fieldRedisVehiclePositionBike = 'field_vehicle_positions_bike';
    // use for vehicle car
    private keyRedisVehiclePositionCar = 'vehicle_positions_car';
    private fieldRedisVehiclePositionCar = 'field_vehicle_positions_car';
    // use for vehicle van
    private keyRedisVehiclePositionVan = 'vehicle_positions_van';
    private fieldRedisVehiclePositionVan = 'field_vehicle_positions_van';
    // use for vehicle bagac
    private keyRedisVehiclePositionBagac = 'vehicle_positions_bagac';
    private fieldRedisVehiclePositionBagac = 'field_vehicle_positions_bagac';
    private  connect:any;
    constructor(
        private firebaseService: FirebaseService,
        private ticketService: TicketService,
        private vehicleMovingService: VehicleMovingService,
        private readonly googleMapService: GoogleMapService,
        private readonly vnPayService: VnPayService,
        private readonly vehicleScheduleService: VehicleScheduleService,
        @InjectSchedule() private readonly schedule: Schedule,

    ) {
        this.connect= {
            port: process.env.ENV_REDIS_PORT || 6379,
            host: process.env.ENV_REDIS_HOST || '127.0.0.1', 
            password: process.env.ENV_REDIS_PASSWORD || null,
        };
        console.log(" Redis constructor:",this.connect);
        this.redisService = RedisService.getInstance(this.connect);
        this.redis = RedisUtil.getInstance();
    }

    async requestBookBus(body: any) {
        let rsPositionVehicle, checkSchedule;

        // const book_bus_reference = data.book_bus_reference;
        let getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus);

        // Check exist request book bus by customer
        if (getCache.isValidate) {
            let listRequestCheck = JSON.parse(getCache.value);
            const checkExist = _.findIndex(listRequestCheck, {customer_uuid: body.customer_uuid});
            if (checkExist > -1) throw {message: 'Request existed', status: StatusCode.CODE_CONFLICT}; // status conflict

        }

        // Get device token
        const userFCM = await this.firebaseService.findOne(body.driver_uuid);
        if (userFCM){
            // Update seat map
            const listSeat =  body.order_seat;
            if (listSeat.length > 0) {
                // Update seat map on redis
                const redisKey = await this.keyRedisVehiclePosition(VehicleGroup.BUS);
                const redisField = await this.fieldRedisVehiclePosition(VehicleGroup.BUS);
                let getCachePositionVehicles = await this.redis.getInfoRedis(redisKey, redisField);
                if (getCachePositionVehicles.isValidate) {
                    const listPositionVehicles = JSON.parse(getCachePositionVehicles.value);
                    rsPositionVehicle = listPositionVehicles.find(
                        item => item.user_uuid === body.driver_uuid && item.schedule_id === body.schedule_id,
                    );
                    if (!rsPositionVehicle) throw {message: 'Not found driver or schedule', status: StatusCode.NOT_FOUND};

                    checkSchedule = await this.vehicleMovingService.findOne(rsPositionVehicle.schedule_id);
                    if (!checkSchedule) throw {message: 'Vehicle schedule not found', status: StatusCode.NOT_FOUND}; // Schedule not found

                    let flagFloor = rsPositionVehicle.floor;
                    const seat_map_clone = _.cloneDeep(rsPositionVehicle.seat_map);
                    // Loop seat order
                    for (let seatName of listSeat) {
                        let flagExistSeatName = false; // To check existed seat name
                        for (let i = 1 ; i <=  rsPositionVehicle.floor; i++) {
                            if (rsPositionVehicle.seat_map.hasOwnProperty('f' + i)) {
                                await Promise.all(rsPositionVehicle.seat_map['f' + i].map((item) => {
                                    item.map((v) => {
                                        if (v.name === seatName && v.status !== true && v.is_waiting !== true) {
                                            v.is_waiting = true;
                                            flagExistSeatName = true;
                                            rsPositionVehicle.available_seat -= 1;
                                        }
                                    });
                                }));
                            }
                            flagFloor--;
                        }
                        const index = listPositionVehicles.findIndex(vehicle => vehicle.user_uuid === body.driver_uuid);

                        if (!flagExistSeatName) {
                            // Recovery seat_map
                            rsPositionVehicle.seat_map = seat_map_clone;
                            listPositionVehicles[index] = rsPositionVehicle;
                            await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listPositionVehicles));

                            // Seat name not found in seat map
                            throw {
                                message: 'The seat name can not be found in the seat map or seat name already reserved!',
                                status: StatusCode.BAD_REQUEST};
                        }
                        // update seat map
                        if (flagFloor <= 0) {
                            listPositionVehicles[index] = rsPositionVehicle;
                            await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listPositionVehicles));
                        }
                    }
                }
                body.license_plates = rsPositionVehicle.license_plates;
                body.provider_name = rsPositionVehicle.provider_name;
                body.logo = rsPositionVehicle.logo;
                body.start_date = checkSchedule.vehicleSchedule.start_date;
                body.end_point_name = checkSchedule.vehicleSchedule.route.endPoint.name;
                body.end_point_coordinates = checkSchedule.vehicleSchedule.route.endPoint.coordinates;
                body.status = StatusRequestBookBus.REQUEST_NEW;
            }
            // Set info custom request
            if (getCache.isValidate) {
                // []
                let listRequest = JSON.parse(getCache.value);
                const checkRequestExit = _.findIndex(listRequest, {book_bus_reference: body.book_bus_reference});
                if (checkRequestExit < 0) {
                    listRequest.push(body);
                }
                await this.redis.setInfoRedis(
                    this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus, JSON.stringify(listRequest));
            } else {
                // (empty list or set)
                let listRequest = [];
                listRequest.push(body);
                await this.redis.setInfoRedis(
                    this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus, JSON.stringify(listRequest));
            }
            // Add cron job, remove request when expise timer
            await this.schedule.scheduleIntervalJob(
                KEY_CANCEL_FIND_BUS + body.book_bus_reference,
                Number(process.env.REQUEST_BOOK_BUS_TIMER) || 120000,
                () => this.cancelFindVehicleBus(body.book_bus_reference));

            // get list token to push notify
            let listDriverDeviceToken = [];
            const token = userFCM.token ? userFCM.token : userFCM.apns_id;
            listDriverDeviceToken.push(token);
            // send notification to driver
            const notification = {
                title: 'Có khách đặt xe',
                body: 'Có khách đặt xe',
            };
            const dataNotification = {
                key: REQUEST_BOOK_BUS_FROM_CUSTOMER,
                customer_user_uuid: body.customer_uuid,
                data: {...body, ...{type: 2, type_group: VehicleGroup.BUS}},
                book_bus_reference: body.book_bus_reference,
                click_action: KEY_CLICK_ACTION,
            };
            // Emit event request book bus for driver
            this.redisService.triggerEventBookBus({room: 'user_' + userFCM.user_uuid, message: dataNotification});
            // Push notify for driver
            await this.firebaseService.pushNotificationWithTokens(listDriverDeviceToken, notification,  dataNotification);
            return true;
        }
        return false;
    }

    async approveRequestBookBus(body: {book_bus_reference: string, driver_uuid: string}){
        let requestBook, indexRequest, listRequestRedis, listPositionVehicles, rsPositionVehicle;
        let long_time = 0;
        // Update status request book bus by driver approve customer
        let getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus);
        if (getCache.isValidate) {
            listRequestRedis = JSON.parse(getCache.value);
            indexRequest = listRequestRedis.findIndex(req => req.book_bus_reference === body.book_bus_reference);
            requestBook = listRequestRedis.find(req => req.book_bus_reference === body.book_bus_reference);
            if (indexRequest !== -1) {
                requestBook.status = 2; // driver approve request
            }  else {
                // Not found request on redis
                return false;
            }
        }

        // Get list vehicle schedule on redis
        const redisKey = await this.keyRedisVehiclePosition(VehicleGroup.BUS);
        const redisField = await this.fieldRedisVehiclePosition(VehicleGroup.BUS);
        let getCachePositionVehicles = await this.redis.getInfoRedis(redisKey, redisField);
        if (getCachePositionVehicles.isValidate) {
            listPositionVehicles = JSON.parse(getCachePositionVehicles.value);
            // Find vehicle schedule by driver uuid
            rsPositionVehicle = listPositionVehicles.find(item => item.user_uuid === requestBook.driver_uuid);
        }
        if (!rsPositionVehicle) {
            throw {message: 'Not found vehicle!', status: 404};
        }

        // Update request booking
        listRequestRedis[indexRequest] = requestBook;
        // Add cron job, remove request when expise timer
        await this.schedule.scheduleIntervalJob(
            KEY_CANCEL_APPROVED_BUS + body.book_bus_reference,
            Number(process.env.REQUEST_BOOK_BUS_APPROVED_TIMER) || 120000,
            () => this.cronCancleApproveBookBus(body.book_bus_reference),
        );
        // Cancel cron
        this.schedule.cancelJob(KEY_CANCEL_FIND_BUS + body.book_bus_reference);
        await this.redis.setInfoRedis(
            this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus, JSON.stringify(listRequestRedis));
        // Get device token
        // send notification to driver
        const notification = {
            title: 'Tài xế đã nhận yêu cầu của bạn',
            body: 'Tài xế đã nhận yêu cầu của bạn',
        };

        const dataNotification = {
            key: KEY_DRIVER_ACCEPT_REQUEST_BOOK_BUS_FROM_CUSTOMER,
            customer_uuid: requestBook.customer_uuid,
            driver_uuid: body.driver_uuid,
            long_time,
            book_bus_reference: requestBook.book_bus_reference,
            type_group: VehicleGroup.BUS,
            click_action: KEY_CLICK_ACTION,
            driver_position: {lat: rsPositionVehicle.lat, lng: rsPositionVehicle.lng},
            expires_approve_miliseconds: Number(process.env.REQUEST_BOOK_BUS_APPROVED_TIMER || 300000),
        };
        // Emit event request book bus for customer
        this.redisService.triggerEventBookBus({room: 'user_' + requestBook.customer_uuid, message: dataNotification});
        // Push notify for customer
        const userFCM = await this.firebaseService.findOne(requestBook.customer_uuid);
        if (userFCM) {
            const token = userFCM.token ? userFCM.token : userFCM.apns_id;
            await this.firebaseService.pushNotificationWithTokens([token], notification,  dataNotification);
        }
        return dataNotification;
    }

    async rejectRequestBookBus(body: RejectBookBusDto, driver_uuid: string) {

        // Update status request book bus by driver approve customer
        if (Number(body.type) === 1) {
            let listPositionVehicles, rsPositionVehicle, findRequest;
            let flagExistSeatName = false;
            const getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus);
            if (getCache.isValidate) {
                const listRequestRedis = JSON.parse(getCache.value);
                findRequest = _.find(listRequestRedis, (item: any) => {
                    return item.driver_uuid === driver_uuid && item.customer_uuid === body.customer_uuid;
                });
                if (!findRequest) throw {message: 'Request not found', status: 404};
                _.remove(listRequestRedis, (item: any) => {
                    return item.driver_uuid === driver_uuid && item.customer_uuid === body.customer_uuid;
                });
                await this.redis.setInfoRedis(
                    this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus, JSON.stringify(listRequestRedis));

            }
            // Get list vehicle schedule on redis
            const redisKey = await this.keyRedisVehiclePosition('BUS');
            const redisField = await this.fieldRedisVehiclePosition('BUS');
            let getCachePositionVehicles = await this.redis.getInfoRedis(redisKey, redisField);
            if (getCachePositionVehicles.isValidate) {
                listPositionVehicles = JSON.parse(getCachePositionVehicles.value);
                // Find vehicle schedule by driver uuid
                rsPositionVehicle = listPositionVehicles.find(item => item.user_uuid === findRequest.driver_uuid);
            }
            // Loop order seat, and create ticket
            const arrSeatName = findRequest.order_seat;
            if (arrSeatName instanceof Array) {
                if (arrSeatName.length > 0) {
                    await Promise.all(arrSeatName.map(async (value) => {
                        for (let i = 1; i <= rsPositionVehicle.floor; i++) {
                            if (rsPositionVehicle.seat_map.hasOwnProperty('f' + i)) {
                                await Promise.all(rsPositionVehicle.seat_map['f' + i].map((item) => {
                                    item.map((v) => {
                                        if (v.name === value && v.is_waiting === true) {
                                            v.is_waiting = false;
                                            rsPositionVehicle.available_seat += 1;
                                            flagExistSeatName = true;
                                        }
                                    });
                                }));
                            }
                        }
                    }));
                    // update seat map on redis and vehicle running
                    const index = listPositionVehicles.findIndex(vehicle => vehicle.user_uuid === findRequest.driver_uuid);
                    listPositionVehicles[index] = rsPositionVehicle;
                    await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listPositionVehicles));
                    // update seat map vehicle schedule
                    await this.vehicleMovingService.updateSeatMap(
                        rsPositionVehicle.schedule_id, rsPositionVehicle.seat_map);
                }
            }
        }
        if (Number(body.type) === 2) {
            const getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping);
            if (getCache.isValidate) {
                const listRequestRedis = JSON.parse(getCache.value);
                const findRequest = _.find(listRequestRedis, item => {
                    return item.driver_uuid === driver_uuid && item.customer_uuid === body.customer_uuid;
                });
                if (!findRequest) throw {message: 'Request not found', status: 404};
                _.remove(listRequestRedis, (item: any) => {
                    return item.driver_uuid === driver_uuid && item.customer_uuid === body.customer_uuid;
                });
                await this.redis.setInfoRedis(
                    this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping, JSON.stringify(listRequestRedis));

            }
        }
        // send notification to customer
        const notification = {
            title: 'Xin lỗi, tài xế đã từ chối yêu cầu của bạn',
            body: 'Xin lỗi, tài xế đã từ chối yêu cầu của bạn',
        };
        const notificationData = {
            key: KEY_DRIVER_REJECT_REQUEST_BOOK_BUS_FROM_CUSTOMER,
            type_group: VehicleGroup.BUS,
            click_action: KEY_CLICK_ACTION,
        };
        // Emit event book bus
        this.redisService.triggerEventBookBus({room: 'user_' + body.customer_uuid, message: notificationData});
        // Push notify
        const userFCM = await this.firebaseService.findOne(body.customer_uuid);
        if (userFCM){
            let listDriverDeviceToken = [];
            const token = userFCM.token ? userFCM.token : userFCM.apns_id;
            listDriverDeviceToken.push(token);
            await this.firebaseService.pushNotificationWithTokens(listDriverDeviceToken, notification,  notificationData);
            return true;
        }
        return false;
    }
    async checkExistBookBus(type: number, customer_uuid: string) {
        const redisKey = await this.keyRedisVehiclePosition('BUS');
        const redisField = await this.fieldRedisVehiclePosition('BUS');
        let getCachePositionVehicles = await this.redis.getInfoRedis(redisKey, redisField);
        if (getCachePositionVehicles.isValidate) {
            const listPositionVehicles = JSON.parse(getCachePositionVehicles.value);
            if (Number(type) === 1) {
                // Book bus
                let getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus);
                if (getCache.isValidate) {
                    const listRequestRedis = JSON.parse(getCache.value);
                    const requestBook = listRequestRedis.find(
                        req => req.customer_uuid === customer_uuid);
                    if (!requestBook) {
                        // Not found request on redis
                        throw {message: 'Not found request book bus', status: 404};
                    }
                    requestBook.type = 1;
                    const rsPositionVehicle = listPositionVehicles.find(item => item.user_uuid === requestBook.driver_uuid);
                    requestBook.vehicle_coordinates = {lat: rsPositionVehicle.lat, lng: rsPositionVehicle.lng};
                    return requestBook;
                }
            }
            if (Number(type) === 2) {
                let getCacheBookShipping = await this.redis.getInfoRedis(
                    this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping);
                if (getCacheBookShipping.isValidate) {
                    const listRequestBookShippingRedis = JSON.parse(getCacheBookShipping.value);
                    const requestBookShipping = listRequestBookShippingRedis.find(
                        req => req.customer_uuid === customer_uuid);
                    if (!requestBookShipping) {
                        // Not found request on redis
                        throw {message: 'Not found request book bus', status: 404};
                    }
                    requestBookShipping.type = 2;
                    const rsPositionVehicle = listPositionVehicles.find(item => item.user_uuid === requestBookShipping.driver_uuid);
                    requestBookShipping.vehicle_coordinates = {lat: rsPositionVehicle.lat, lng: rsPositionVehicle.lng};
                    return requestBookShipping;
                }
            }
        }
        return false;

    }
    async createUrlPayment(data: { customer_uuid: string, book_bus_reference: string, vnp_IpAddr: string, vnp_BankCode: string }){
        let getRequestBook;
        // Get request by customer
        let getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus);
        if (getCache.isValidate) {
            const listRequestRedis = JSON.parse(getCache.value);
            getRequestBook = _.find(listRequestRedis, {customer_uuid: data.customer_uuid, book_bus_reference: data.book_bus_reference});
            if (!getRequestBook) throw {message: 'Not found request book bus', status: StatusCode.NOT_FOUND};  // Not found request on redis
            const orderSeats = getRequestBook.order_seat;
            if (orderSeats.length <= 0) throw {message: 'Seat order not found!', status: StatusCode.NOT_FOUND};
            const amount =  Number(getRequestBook.price) * Number(orderSeats.length) * 100; // Khử số thập phân, vn pay bắt buộc nhân 100
            const dataPost = {
                vnp_Amount: Number(amount),
                vnp_BankCode: 'NCB',
                vnp_OrderInfo: 'Thanh toan dat xe',
                vnp_OrderType: '240000',
                vnp_TxnRef: data.book_bus_reference,
            };
            return await this.vnPayService.createPaymentUrl(dataPost, data.vnp_IpAddr);
        }
        return false;
    }
    async customerPaid(data: { customer_uuid: string, book_bus_reference: string}){
        // Check payment status by book_bus_reference
        const checkTicketPaid = await this.ticketService.checkExistPaidByBookBusReference(data.book_bus_reference);
        if (checkTicketPaid) throw {message: 'Ticket is paid', status: StatusCode.CODE_CONFLICT};

        let listPositionVehicles, rsPositionVehicle, flagExistSeatName, distanceDriverToStation, ticket;
        let long_time = 0;
        let arrayTicket = [];
        const userFCM = await this.firebaseService.findOne(data.customer_uuid);

        // Update payment status request book bus by customer
        let getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus);
        if (!getCache.isValidate) throw {message: 'Not found request book bus', status: StatusCode.NOT_FOUND};
        let listRequestRedis = JSON.parse(getCache.value);
        let requestBook = listRequestRedis.find(
            req => req.customer_uuid === data.customer_uuid && req.book_bus_reference === data.book_bus_reference);
        const indexRequest = listRequestRedis.findIndex(req => req.book_bus_reference === data.book_bus_reference);
        if (!requestBook) throw {message: 'Not found request book bus', status: StatusCode.NOT_FOUND};

        requestBook.payment_status = StatusPayment.PAID;
        listRequestRedis[indexRequest] = requestBook;
        await this.redis.setInfoRedis(
            this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus, JSON.stringify(listRequestRedis));

        // Get list vehicle schedule on redis
        const redisKey = await this.keyRedisVehiclePosition(VehicleGroup.BUS);
        const redisField = await this.fieldRedisVehiclePosition(VehicleGroup.BUS);
        let getCachePositionVehicles = await this.redis.getInfoRedis(redisKey, redisField);
        if (getCachePositionVehicles.isValidate) {
            listPositionVehicles = JSON.parse(getCachePositionVehicles.value);
            // Find vehicle schedule by driver uuid
            rsPositionVehicle = listPositionVehicles.find(item => item.user_uuid === requestBook.driver_uuid);
        }
        if (!rsPositionVehicle) throw {message: 'Not found vehicle!', status: StatusCode.NOT_FOUND};

        // Cancel cron
        this.schedule.cancelJob(KEY_CANCEL_APPROVED_BUS + data.book_bus_reference);
        // Loop order seat, and create ticket
        const arrSeatName = requestBook.order_seat;
        if (arrSeatName instanceof Array) {
            if (arrSeatName.length > 0) {
                await Promise.all(arrSeatName.map(async (value) => {
                    // distanceDriverToStation = await this.googleMapService.rawUrlGetDistance({
                    //     origins: {lat: rsPositionVehicle.lat, lng: rsPositionVehicle.lng},
                    //     destinations: {lat: requestBook.pickup_point.lat, lng: requestBook.pickup_point.lng},
                    // });
                    // if (!distanceDriverToStation) throw {message: 'Format station or pickup point incorrect', status: StatusCode.BAD_GATEWAY};
                    // long_time = distanceDriverToStation.data.rows[0].elements[0].duration.text.match(/\d+/)[0];
                    const dataTicket = {
                        user_id: userFCM.user_id,
                        schedule_id: requestBook.schedule_id,
                        price: requestBook.price,
                        seat_name: value,
                        pickup_point: requestBook.pickup_point.address,
                        destination: requestBook.destination,
                        type: TicketType.BOOK_VEHICLE,
                        book_bus_reference: requestBook.book_bus_reference,
                        time_pick_up: requestBook.long_time,
                        payment_method: StatusPaymentMethod.BANK,
                        payment_status: StatusPayment.PAID,
                        luggage_info: requestBook.luggage_info,
                    };

                    for (let i = 1 ; i <= rsPositionVehicle.floor; i++) {
                        if (rsPositionVehicle.seat_map.hasOwnProperty('f' + i)) {
                            await Promise.all(rsPositionVehicle.seat_map['f' + i].map((item) => {
                                item.map((v) => {
                                    if (v.name === value && v.status !== true && v.is_waiting === true) {
                                        dataTicket.seat_name = v.seat_name;
                                        v.is_waiting = false;
                                        v.status = true;
                                        // rsPositionVehicle.available_seat -= 1;
                                        flagExistSeatName = true;
                                    }
                                });
                            }));
                        }
                    }
                    if (flagExistSeatName) {
                        ticket = await this.ticketService.createTicket(dataTicket, requestBook.customer_uuid);
                        arrayTicket.push(ticket);
                    }else {
                        throw {message: `Not found seat name ${value} or existed order!`, status: StatusCode.NOT_FOUND};
                    }

                }));

                // update seat map on redis and vehicle running
                const index = listPositionVehicles.findIndex(vehicle => vehicle.user_uuid === requestBook.driver_uuid);
                listPositionVehicles[index] = rsPositionVehicle;
                await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listPositionVehicles));
                // update seat map vehicle schedule
                await this.vehicleMovingService.updateSeatMap(
                    rsPositionVehicle.schedule_id, rsPositionVehicle.seat_map, rsPositionVehicle.available_seat);
            }
        }
        if (arrayTicket) {
            // send notification to customer paid
            const notification = {
                title: 'Chúc mừng bạn đã thanh toán thành công',
                body: 'Chúc mừng bạn đã thanh toán thành công',
            };
            const dataNotification = {
                key: KEY_NOTIFY_PAID_BOOK_BUS_TO_CUSTOMER,
                customer_user_uuid: data.customer_uuid,
                ticket_info: arrayTicket,
                book_bus_reference: data.book_bus_reference,
                click_action: KEY_CLICK_ACTION,
            };

            const dataNotificationSocket = {
                key: KEY_TRIGGER_SEAT_MAP,
                book_bus_reference: data.book_bus_reference,
                message: 'Customer paid successfully',
                click_action: KEY_CLICK_ACTION,
            };
            // emit event to customer
            this.redisService.triggerEventBookBus({room: 'user_' + data.customer_uuid, message: dataNotification});
            this.redisService.triggerEventBookBus({room: 'user_' + requestBook.driver_uuid, message: dataNotificationSocket});
            if (userFCM) {
                const token = userFCM.token ? userFCM.token : userFCM.apns_id;
                await this.firebaseService.pushNotificationWithTokens([token], notification,  dataNotification);
            }
            return requestBook;
        }
        return false;
    }

    async cancelFindVehicleBus(book_bus_reference: string) {
        let userFCM, findBookBusReference, listPositionVehicles, rsPositionVehicle, flagExistSeatName;
        // const book_bus_reference = data.book_bus_reference;
        let getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus);
        if (getCache.isValidate) {
            // Update seat map
            const listRequestBookBus = JSON.parse(getCache.value);
            findBookBusReference = _.find(listRequestBookBus, {book_bus_reference, status: 1});
            if (findBookBusReference) {
                userFCM = await this.firebaseService.findOne(findBookBusReference.driver_uuid);

                // Get list vehicle schedule on redis
                const redisKey = await this.keyRedisVehiclePosition('BUS');
                const redisField = await this.fieldRedisVehiclePosition('BUS');
                let getCachePositionVehicles = await this.redis.getInfoRedis(redisKey, redisField);
                if (getCachePositionVehicles.isValidate) {
                    listPositionVehicles = JSON.parse(getCachePositionVehicles.value);
                    // Find vehicle schedule by driver uuid
                    rsPositionVehicle = listPositionVehicles.find(item => item.user_uuid === findBookBusReference.driver_uuid);
                }

                // Loop order seat, and create ticket
                const arrSeatName = findBookBusReference.order_seat;
                if (arrSeatName instanceof Array) {
                    if (arrSeatName.length > 0) {
                        await Promise.all(arrSeatName.map(async (value) => {
                            for (let i = 1; i <= rsPositionVehicle.floor; i++) {
                                if (rsPositionVehicle.seat_map.hasOwnProperty('f' + i)) {
                                    await Promise.all(rsPositionVehicle.seat_map['f' + i].map((item) => {
                                        item.map((v) => {
                                            if (v.name === value && v.is_waiting === true) {
                                                v.is_waiting = false;
                                                rsPositionVehicle.available_seat += 1;
                                                flagExistSeatName = true;
                                            }
                                        });
                                    }));
                                }
                            }
                        }));
                        // update seat map on redis and vehicle running
                        const index = listPositionVehicles.findIndex(vehicle => vehicle.user_uuid === findBookBusReference.driver_uuid);
                        listPositionVehicles[index] = rsPositionVehicle;
                        await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listPositionVehicles));
                        // update seat map vehicle schedule
                        await this.vehicleMovingService.updateSeatMap(
                            rsPositionVehicle.schedule_id, rsPositionVehicle.seat_map);
                    }
                }
                // remove item exist book_bus_reference
                _.remove(listRequestBookBus, ( item: any ) => {
                    return item.book_bus_reference === book_bus_reference;
                });
                await this.redis.setInfoRedis(
                    this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus, JSON.stringify(listRequestBookBus));

                //  Push notify fo driver
                if (userFCM) {
                    let listDriverDeviceToken = [];
                    const token = userFCM.token ? userFCM.token : userFCM.apns_id;
                    listDriverDeviceToken.push(token);
                    // send notification to driver
                    const notification = {
                        title: 'Khách hàng hủy tìm xe',
                        body: 'Khách hàng đã hủy tìm xe',
                    };
                    const dataNotification = {
                        key: KEY_CANCEL_FIND_BOOK_BUS_BY_CUSTOMER,
                        customerInfo: {
                            customer_uuid: findBookBusReference.customer_uuid.toString(),
                            phone: findBookBusReference.phone,
                        },
                        type_group: VehicleGroup.BUS,
                        click_action: KEY_CLICK_ACTION,
                    };
                    // Emit event request book bus for driver
                    this.redisService.triggerEventBookBus({room: 'user_' + userFCM.user_uuid, message: dataNotification});
                    await this.firebaseService.pushNotificationWithTokens(listDriverDeviceToken, notification,  dataNotification);
                }
            }
        }
        return true;
    }
    async cronCancleApproveBookBus(book_bus_reference: string) {
        let findBookBusReference;
        // Clear request book bus on redis
        let getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus);
        if (!getCache.isValidate) return false;
        const listRequestRedis = JSON.parse(getCache.value);
        findBookBusReference = _.find(listRequestRedis, {book_bus_reference});
        if (!findBookBusReference) return false;
        _.remove(listRequestRedis, {book_bus_reference});
        await this.redis.setInfoRedis(
            this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus, JSON.stringify(listRequestRedis));

        // Get device token
        const userFCM = await this.firebaseService.findOne(findBookBusReference.driver_uuid);
        if (userFCM){
            // Get info position by driver
            let rsPositionVehicle;
            const redisKey = await this.keyRedisVehiclePosition(VehicleGroup.BUS);
            const redisField = await this.fieldRedisVehiclePosition(VehicleGroup.BUS);
            let getCachePositionVehicles = await this.redis.getInfoRedis(redisKey, redisField);
            if (!getCachePositionVehicles.isValidate) return false;
            const listPositionVehicles = JSON.parse(getCachePositionVehicles.value);
            rsPositionVehicle = listPositionVehicles.find(item => item.user_uuid === userFCM.user_uuid);
            if (!rsPositionVehicle) return false;

            // Update seat map on redis
            const arrSeatName = findBookBusReference.order_seat;
            if (arrSeatName instanceof Array) {
                if (arrSeatName.length > 0) {
                    await Promise.all(arrSeatName.map(async (value) => {
                        for (let i = 1; i <= rsPositionVehicle.floor; i++) {
                            if (rsPositionVehicle.seat_map.hasOwnProperty('f' + i)) {
                                await Promise.all(rsPositionVehicle.seat_map['f' + i].map((item) => {
                                    item.map((v) => {
                                        if (v.name === value) {
                                            v.is_waiting = false;
                                            v.status = false;
                                            rsPositionVehicle.available_seat += 1;
                                        }
                                    });
                                }));
                            }
                        }
                    }));
                    // update seat map on redis
                    const index = listPositionVehicles.findIndex(vehicle => vehicle.user_uuid === findBookBusReference.driver_uuid);
                    listPositionVehicles[index] = rsPositionVehicle;
                    await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listPositionVehicles));
                    // update seat map vehicle schedule running
                    await this.vehicleMovingService.updateSeatMap(rsPositionVehicle.schedule_id, rsPositionVehicle.seat_map);
                }
            }
            // Push notification
            let listDriverDeviceToken = [];
            const token = userFCM.token ? userFCM.token : userFCM.apns_id;
            listDriverDeviceToken.push(token);
            const notification = {
                title: 'Khách hàng đã hết thời gian thanh toán',
                body: 'Khách hàng chưa thanh toán',
            };
            const dataNotification = {
                key: KEY_CANCEL_APPROVE_REQUEST_BOOK_BUS_BY_CUSTOMER,
                book_bus_reference,
                type_group: VehicleGroup.BUS,
                click_action: 'FLUTTER_NOTIFICATION_CLICK',
            };
            // Emit event request book bus for customer
            this.redisService.triggerEventBookBus({room: 'user_' + userFCM.user_uuid, message: dataNotification});
            // Push notify
            await this.firebaseService.pushNotificationWithTokens(listDriverDeviceToken, notification,  dataNotification);
            return false;
        }
        return false;
    }
    async canceBookTicketByReference(book_bus_reference: string, listTicket: TicketEntity[]): Promise<boolean> {
        const arrSeatName = [];
        let floor, vehicleScheduleId = 0;
        listTicket.forEach(ticket => {
            arrSeatName.push(ticket.seat_name);
            floor = ticket.floor;
            vehicleScheduleId = ticket.schedule_id;
        });
        const vehicleSchedule = await this.vehicleScheduleService.getDetailById(vehicleScheduleId);
        if (!vehicleSchedule) return false;
        // Update seat map on redis
        if (arrSeatName instanceof Array) {
            if (arrSeatName.length > 0) {
                await Promise.all(arrSeatName.map(async (value) => {
                    for (let i = 1; i <= floor; i++) {
                        if (vehicleSchedule.seat_map.hasOwnProperty('f' + i)) {
                            await Promise.all(vehicleSchedule.seat_map['f' + i].map((item) => {
                                item.map((v) => {
                                    if (v.seat_name === value) {
                                        v.is_waiting = false;
                                        v.status = false;
                                        vehicleSchedule.available_seat += 1;
                                    }
                                });
                            }));
                        }
                    }
                }));
                // update seat map vehicle schedule running
                await this.vehicleScheduleService.updateEntity(vehicleSchedule);
            }
        }
        return true;
    }
    async cancelApproveRequestBookBus(book_bus_reference: string, customerInfo: any){
        let findBookBusReference;
        // Soft delete ticket
        const ticketEntity: any = await this.ticketService.deleteTicketByBookBusReference({book_bus_reference, customer_id: customerInfo.id});

        const checkReference = book_bus_reference.indexOf(SECRET_BOOK_TICKET || SECRET_BOOK_TICKET_BY_ADMIN);
        if (checkReference > -1) {
            if (ticketEntity.length > 0 )
                return await this.canceBookTicketByReference(book_bus_reference, ticketEntity);
            throw {message: 'Not found ticket or this ticket can not be canceled!', status: StatusCode.NOT_FOUND};
        }
        if (!ticketEntity) throw {message: 'Ticket not found or that ticket is scanned', status: StatusCode.NOT_FOUND};

        // Clear request book bus on redis
        let getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus);
        if (!getCache.isValidate) throw {message: 'Not found reference', status: StatusCode.NOT_FOUND};
        const listRequestRedis = JSON.parse(getCache.value);
        findBookBusReference = _.find(listRequestRedis, {book_bus_reference});
        if (!findBookBusReference) throw {message: 'Not found reference', status: StatusCode.NOT_FOUND};
        _.remove(listRequestRedis, {book_bus_reference});
        await this.redis.setInfoRedis(
            this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus, JSON.stringify(listRequestRedis));

        // Get device token
        const userFCM = await this.firebaseService.findOne(findBookBusReference.driver_uuid);
        if (userFCM){
            // Get info position by driver
            let rsPositionVehicle;
            const redisKey = await this.keyRedisVehiclePosition(VehicleGroup.BUS);
            const redisField = await this.fieldRedisVehiclePosition(VehicleGroup.BUS);
            let getCachePositionVehicles = await this.redis.getInfoRedis(redisKey, redisField);
            if (!getCachePositionVehicles.isValidate) throw {message: 'Not found vehicle', status: StatusCode.NOT_FOUND};
            const listPositionVehicles = JSON.parse(getCachePositionVehicles.value);
            rsPositionVehicle = listPositionVehicles.find(item => item.user_uuid === userFCM.user_uuid);
            if (!rsPositionVehicle) throw {message: 'Not found vehicle', status: StatusCode.NOT_FOUND};

            // Update seat map on redis
            const arrSeatName = findBookBusReference.order_seat;
            if (arrSeatName instanceof Array) {
                if (arrSeatName.length > 0) {
                    await Promise.all(arrSeatName.map(async (value) => {
                        for (let i = 1; i <= rsPositionVehicle.floor; i++) {
                            if (rsPositionVehicle.seat_map.hasOwnProperty('f' + i)) {
                                await Promise.all(rsPositionVehicle.seat_map['f' + i].map((item) => {
                                    item.map((v) => {
                                        if (v.name === value) {
                                            v.is_waiting = false;
                                            v.status = false;
                                            rsPositionVehicle.available_seat += 1;
                                        }
                                    });
                                }));
                            }
                        }
                    }));
                    // update seat map on redis
                    const index = listPositionVehicles.findIndex(vehicle => vehicle.user_uuid === findBookBusReference.driver_uuid);
                    listPositionVehicles[index] = rsPositionVehicle;
                    await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listPositionVehicles));
                    // update seat map vehicle schedule running
                    await this.vehicleMovingService.updateSeatMap(rsPositionVehicle.schedule_id, rsPositionVehicle.seat_map);
                }
            }
            // Push notification
            let listDriverDeviceToken = [];
            const token = userFCM.token ? userFCM.token : userFCM.apns_id;
            listDriverDeviceToken.push(token);
            const notification = {
                title: 'Khách hàng hủy chuyến',
                body: 'Khách hàng đã hủy đặt xe',
            };
            const dataNotification = {
                key: KEY_CANCEL_APPROVE_REQUEST_BOOK_BUS_BY_CUSTOMER,
                customerInfo: {
                    customer_uuid: customerInfo.user_id.toString(),
                    phone: customerInfo.phone,
                    full_name: customerInfo.full_name,
                },
                book_bus_reference,
                type_group: VehicleGroup.BUS,
                click_action: KEY_CLICK_ACTION,
            };
            // Emit event request book bus for customer
            this.redisService.triggerEventBookBus({room: 'user_' + userFCM.user_uuid, message: dataNotification});
            // Push notify
            await this.firebaseService.pushNotificationWithTokens(listDriverDeviceToken, notification,  dataNotification);
            return dataNotification;
        }
        return false;
    }

    async fieldRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.fieldRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.fieldRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.fieldRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.fieldRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.fieldRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    async keyRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.keyRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.keyRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.keyRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.keyRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.keyRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

}
