import {
    Body,
    Controller,
    Delete,
    Get,
    HttpStatus,
    Param,
    Post,
    Put,
    Req,
    Res,
    UseGuards,
    UsePipes,
} from '@nestjs/common';
import { ApiBearerAuth, ApiImplicitParam, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { BusOrderService} from './bus-order.service';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { Response } from 'express';
import { ApproveBookBusDto } from './dto/approve-book-bus.dto';
import { AuthGuard } from '@nestjs/passport';
import { RequestBookBusDto } from './dto/request-book-bus.dto';
import { PaidBookBusDto } from './dto/paid-book-bus.dto';
import * as jwt from 'jsonwebtoken';
import { UsersService } from '../users/users.service';
import { RejectBookBusDto } from './dto/reject-book-bus.dto.ts';
import { UpdateSeatMapDto } from '../book-ticket/dto/update-seat-map.dto';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { VehicleMovingService } from '../vehicle-moving/vehicleMoving.service';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import {BookBusUrlPaymentDto} from '../vn-pay/dto/book-bus-url-payment.dto';
import * as moment from 'moment';
import {SECRET_BOOK_BUS} from '../../constants/secrets.const';
import {StatusPayment} from '../../enums/status.enum';
@ApiUseTags('Order Bus - Notify Firebase')
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()

@Controller('order-bus')
export class BusOrderController {
    constructor(
        private readonly busOrderService: BusOrderService,
        private userService: UsersService,
        private vehicleMovingService: VehicleMovingService,
        ) {
    }

    @Post('/request-book-bus')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @ApiOperation({ title: 'Request Book Bus' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async requestBookBus(@Req() request, @Body() body: RequestBookBusDto , @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const customerInfo =  jwt.decode(token);
            const findUserByUuid = await this.userService.findOneByUserInfo(customerInfo);
            // generate key request for customer
            const book_bus_reference = request.user.user_id.toString() + '_' + Math.random().toString(36).slice(2) + SECRET_BOOK_BUS;
            const created_at = moment();
            const data = {...body, ...{
                customer_uuid: findUserByUuid.user_id.toString(),
                customer_phone: findUserByUuid.phone,
                customer_full_name: findUserByUuid.customerProfile.full_name,
                customer_image_profile: findUserByUuid.customerProfile.image_profile,
                    book_bus_reference, created_at,
                    expires_request_miliseconds: Number(process.env.REQUEST_BOOK_BUS_TIMER || 120000),
                    payment_status: StatusPayment.UNPAID,
                },
            };
            const result = await this.busOrderService.requestBookBus(data);
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Book bus existed or not found vehicle schedule! ', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', data));
        }
        catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/approve-request-book-bus')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BUS]})
    @ApiOperation({ title: 'Approve Request Book Bus' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async approveRequestBookBus(@Req() request, @Body() body: ApproveBookBusDto , @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const driverInfo: any =  jwt.decode(token);
            const data = {...body, ...{driver_uuid: driverInfo.user_id.toString()}};
            const result = await this.busOrderService.approveRequestBookBus(data);
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Book bus existed! ', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/reject-request-book-bus')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BUS]})
    @ApiOperation({ title: 'Reject Request Book Bus By Driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async rejectRequestBookBus(@Req() request, @Body() body: RejectBookBusDto , @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const driverInfo: any = jwt.decode(token);
            const result = await this.busOrderService.rejectRequestBookBus(body, driverInfo.user_id.toString());
            if (!result)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found customer! ', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', null));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/get-url-payment-book-bus')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @ApiOperation({ title: 'Get url payment book bus' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getUrlPayment(@Req() request, @Body() body: BookBusUrlPaymentDto , @Res() res: Response) {
        try {
            /* Get IP address */
            const vnp_IpAddr = request.headers['x-forwarded-for'] ||
                request.connection.remoteAddress ||
                request.socket.remoteAddress ||
                request.connection.socket.remoteAddress;
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const customerInfo: any =  jwt.decode(token);
            const data = {...body, ...{customer_uuid: customerInfo.user_id.toString(), vnp_IpAddr}};
            const result = await this.busOrderService.createUrlPayment(data);
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Paid book bus existed! ', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/paid-book-bus')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @ApiOperation({ title: 'Customer Paid Book Bus' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async customerPaid(@Req() request, @Body() body: PaidBookBusDto , @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const customerInfo: any =  jwt.decode(token);
            const data = {...body, ...{customer_uuid: customerInfo.user_id.toString()}};
            const result = await this.busOrderService.customerPaid(data);
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Paid book bus existed! ', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/check-status-request-order-bus-by-customer/:book_type')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @ApiOperation({ title: 'Check status request order bus by customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'book_type', description: '1: Book bus, 2: Book shipping', required: true, type: Number})
    async checkExistBookBus(@Req() request, @Param('book_type') book_type: number, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const customerInfo: any =  jwt.decode(token);
            const result = await this.busOrderService.checkExistBookBus(book_type, customerInfo.user_id.toString());
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Request not found! ', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:vehicle_schedule_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(CustomValidationPipe)
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update seat map in vehicle running' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'vehicle_schedule_id', description: 'Vehicle schedule Id', required: true, type: 'number' })
    async updateChoseSeat(
        @Param('vehicle_schedule_id') vehicle_schedule_id: number,
        @Body() body: UpdateSeatMapDto,
        @Res() res: Response,
    ) {
        try {
            // update seatmap vehicle running
            const result = await this.vehicleMovingService.updateSeatMap(vehicle_schedule_id, body.seat_map);
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/cancel-find-vehicle-bus-by-customer/:book_bus_reference')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Cancel find vehicle bus by customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'book_bus_reference', description: 'book bus reference', required: true, type: String})
    async cancelFindDriver(@Req() request, @Param('book_bus_reference') book_bus_reference: string, @Res() res: Response) {
        try {
            const result = await this.busOrderService.cancelFindVehicleBus(book_bus_reference);
            if (result)
                return res.status(HttpStatus.OK).send(dataSuccess('OK', null));
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found reference', null));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('cancel-approve-book-bus-by-customer/:book_bus_reference')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Cancel approve book bus by customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'book_bus_reference', description: 'book_bus_reference', required: true, type: String })
    async cancelApproveBookShipping(
        @Param('book_bus_reference') book_bus_reference: string,
        @Req() request,
        @Res() res: Response){
        try {
            const token = request.headers.authorization.split(' ')[1];
            const customerInfo: any = jwt.decode(token);
            const usersEntity = await this.userService.findOneByUserInfo(customerInfo);
            const result = await this.busOrderService.cancelApproveRequestBookBus(book_bus_reference, usersEntity);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
