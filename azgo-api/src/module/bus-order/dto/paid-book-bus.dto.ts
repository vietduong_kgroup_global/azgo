import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class PaidBookBusDto {

    @IsNotEmpty()
    @ApiModelProperty({ example: 'Book bus reference' , required: true  })
    book_bus_reference: string;
}
