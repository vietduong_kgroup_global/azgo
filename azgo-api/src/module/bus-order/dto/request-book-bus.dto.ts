import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';

export class RequestBookBusDto {

    @IsNotEmpty()
    @ApiModelProperty({ example: 'f36d515f-e996-4093-a3e4-c548d92c09a8', description: 'Driver uuid', type: String, required: true  })
    driver_uuid: string;

    @IsNotEmpty()
    @ApiModelProperty({ type: CoordinatesDto, required: true  })
    pickup_point: CoordinatesDto;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'Can Tho', description: 'Điểm đến area', type: String, required: true  })
    destination: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: ['111', '112'], description: 'Array seat name (Not seat_name field)', type: [String], required: true  })
    order_seat: string[];

    @IsNotEmpty()
    @ApiModelProperty({example: 1, type: Number, required: true})
    price: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'Balo + crush', description: 'Thông tin hành lý', type: String, required: false })
    luggage_info: string;

    @IsNotEmpty()
    @ApiModelProperty({example: 1, type: Number, required: true})
    schedule_id: number;

    @IsNotEmpty()
    @ApiModelProperty({example: '12 phút', type: String, required: true})
    long_time: string;
}
