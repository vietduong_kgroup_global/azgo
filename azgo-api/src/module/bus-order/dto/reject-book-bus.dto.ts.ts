import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class RejectBookBusDto {

    @IsNotEmpty({ message: 'Customer UUID' })
    @ApiModelProperty({ example: 'customer uuid' , required: true  })
    customer_uuid: string;

    @IsNotEmpty({ message: 'Customer UUID' })
    @ApiModelProperty({ example: '1: Book bus, 2: Book shipping' , required: true  })
    type: number;

}
