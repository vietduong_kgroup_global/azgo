import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class ApproveBookBusDto {

    @IsNotEmpty()
    @ApiModelProperty({ example: '2c6259d3-fd0d-460a-abd7-22d921e7ade5_5vswgohkw5d', required: true})
    book_bus_reference: string;
}
