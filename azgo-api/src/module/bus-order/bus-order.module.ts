import { Module } from '@nestjs/common';
import { BusOrderController } from './bus-order.controller';
import { BusOrderService } from './bus-order.service';
import {TicketModule} from '../ticket/ticket.module';
import {FirebaseModule} from '../firebase/firebase.module';
import {UsersModule} from '../users/users.module';
import {VehicleMovingModule} from '../vehicle-moving/vehicleMoving.module';
import {GoogleMapService} from '../google-map/google-map.service';
import {VnPayModule} from '../vn-pay/vn-pay.module';
import {VehicleScheduleModule} from '../vehicle-schedule/vehicleSchedule.module';

@Module({
  imports: [
      TicketModule,
      FirebaseModule,
      UsersModule,
      VehicleMovingModule,
      VnPayModule,
      VehicleScheduleModule,
  ],
  controllers: [BusOrderController],
  providers: [BusOrderService, GoogleMapService],
})
export class BusOrderModule {}
