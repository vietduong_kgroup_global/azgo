import { EntityRepository, Repository } from 'typeorm';
import { SeatPatternEntity } from './seat-pattern.entity';

@EntityRepository(SeatPatternEntity)
export class SeatPatternRepository extends Repository<SeatPatternEntity> {
    /**
     * This is function get all seat pattern
     * @param limit
     * @param offset
     * @param options
     */
    async getAll(limit: number, offset: number,  options?: any) {
        let provider_id = !isNaN(Number(options.provider_id).valueOf()) ? Number(options.provider_id).valueOf() : null;
        let query = await this.createQueryBuilder('az_seat_pattern');
        query.leftJoinAndSelect('az_seat_pattern.provider', 'provider');
        query.where('az_seat_pattern.deleted_at is null');
        // search by keyword
        if (options.keyword && options.keyword.trim()) {
            query.andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select('az_seat_pattern.id')
                    .from(SeatPatternEntity, 'az_seat_pattern')
                    .where('az_seat_pattern.name like :name', { name: '%' + options.keyword.trim() + '%' })
                    .orWhere('total_seat like :total_seat', { total_seat: '%' + options.keyword.trim() + '%' })
                    .orWhere('provider.provider_name like :provider_name', { provider_name: '%' + options.keyword.trim() + '%' })
                    .getQuery();
                return 'az_seat_pattern.id IN ' + subQuery;
            });
        }
        if (provider_id) {
            query.andWhere('az_seat_pattern.provider_id = :provider_id', {provider_id});
        }
        // limit + offset
        query.orderBy('az_seat_pattern.id', 'DESC')
            .limit(limit)
            .offset(offset);

        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }

    /**
     * This is function find by id
     * @param id
     */
    async findById(id: number): Promise<SeatPatternEntity> {
        return await this.createQueryBuilder('az_seat_pattern')
            .leftJoinAndSelect('az_seat_pattern.provider', 'provider')
            .where('az_seat_pattern.id = :id', {id})
            .andWhere('az_seat_pattern.deleted_at is null')
            .getOne();
    }
}
