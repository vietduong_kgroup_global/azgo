import {
    Body,
    Controller,
    Delete,
    Get,
    HttpStatus,
    Param,
    Post,
    Put,
    Req,
    Res,
    UseGuards,
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { SeatPatternService } from './seat-pattern.service';
import { CreateSeatPatternDto } from './dto/create-seat-pattern.dto';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { Response } from 'express';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import {ProvidersInfoService} from '../providers-info/providers-info.service';

@ApiUseTags('Seat pattern')
@Controller('seat-pattern')
export class SeatPatternController {
    constructor(
        private readonly seatPatternService: SeatPatternService,
        private readonly providersInfoService: ProvidersInfoService,
    ) { }

    @Get()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all seat pattern' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'provider_id', description: 'Provider ID', required: false, type: 'number'})
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    async getAll(@Res() res: Response, @Req() request) {
        if (request.user.type === UserType.PROVIDER) {
            try {
                const provider = await this.providersInfoService.findByUserId(request.user.id);
                if (!provider) return res.status(HttpStatus.BAD_REQUEST).send(dataError('Provider not found', null));
                request.query.provider_id = provider.id;
            } catch (e) {
                console.error('[findAllDriverByProvider] get provider: ', e);
                return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Bad requests', null));
            }
        }
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.seatPatternService.getAll(per_page, offset, request.query);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    @Get('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get detail seat pattern' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'number' })
    async detailSeatPattern(@Param('id') id, @Req() request, @Res() res: Response) {
        try {
            const result = await this.seatPatternService.findById(id, request.user);
            if (!result)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create seat pattern' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createSeatPattern(@Req() request, @Body(CustomValidationPipe) body: CreateSeatPatternDto, @Res() res: Response) {
        try {
            const result = await this.seatPatternService.create(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update seat pattern' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'number' })
    async updateSeatPattern(@Param('id') id, @Req() request, @Body(CustomValidationPipe) body: CreateSeatPatternDto, @Res() res: Response) {
        try {
            const result = await this.seatPatternService.update(id, body);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete seat pattern' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'number' })
    async deleteSeatPattern(@Param('id') id, @Req() request, @Res() res: Response) {
        try {
            const result = await this.seatPatternService.delete(id);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
