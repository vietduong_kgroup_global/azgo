import { Module } from '@nestjs/common';
import { SeatPatternController } from './seat-pattern.controller';
import { SeatPatternService } from './seat-pattern.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {SeatPatternRepository} from './seat-pattern.repository';
import {ProvidersInfoModule} from '../providers-info/providers-info.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([SeatPatternRepository]),
    ProvidersInfoModule,
  ],
  exports: [SeatPatternService],
  controllers: [SeatPatternController],
  providers: [SeatPatternService],
})
export class SeatPatternModule {}
