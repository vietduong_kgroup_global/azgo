import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsInt, IsString, MaxLength} from 'class-validator';

export class CreateSeatPatternDto {
    @IsNotEmpty({message: 'Provider is required'})
    @IsInt({message: 'Provider must be a integer'})
    @ApiModelProperty({example: 1, required: true})
    provider_id: number;

    @IsNotEmpty({message: 'Name is require'})
    @ApiModelProperty({example: '4 chỗ', required: true})
    @IsString({message: 'Name must be a string'})
    @MaxLength(255)
    name: string;

    @IsNotEmpty({message: 'Seat map is require'})
    @ApiModelProperty({example: {rows: 2, cols: 2, f1: [{id: 1, number: 1}, {id: 2, number: 2}]}, required: true})
    seat_map: object;

    @IsNotEmpty({message: 'Floor is require'})
    @ApiModelProperty({example: 2, required: true})
    floor: number;

    @IsNotEmpty({message: 'Total seat is require'})
    @ApiModelProperty({example: 4, required: true})
    total_seat: number;

    @IsNotEmpty({message: 'Max seat of vertical is require'})
    @ApiModelProperty({example: 2, required: true})
    max_seat_of_vertical: number;

    @IsNotEmpty({message: 'Max seat of horizontal is require'})
    @ApiModelProperty({example: 2, required: true})
    max_seat_of_horizontal: number;
}
