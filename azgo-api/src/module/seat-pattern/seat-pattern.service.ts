import { Injectable } from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {SeatPatternEntity} from './seat-pattern.entity';
import {SeatPatternRepository} from './seat-pattern.repository';
import {CreateSeatPatternDto} from './dto/create-seat-pattern.dto';
import {ProvidersInfoService} from '../providers-info/providers-info.service';
import {UsersEntity} from '../users/users.entity';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@Injectable()
export class SeatPatternService {
    constructor(
        @InjectRepository(SeatPatternRepository)
        private readonly seatPatternRepository: SeatPatternRepository,
        private readonly providersInfoService: ProvidersInfoService,
    ) { }

    /**
     * This is function find by id
     * @param id
     * @param user
     */
    async findById(id: number, user?: UsersEntity): Promise<SeatPatternEntity> {
        let query = this.seatPatternRepository.createQueryBuilder('seat')
            .leftJoinAndSelect('seat.provider', 'provider')
            .where('seat.id = :id', {id});

        if (user && user.type === UserType.PROVIDER) {
            query.leftJoin('provider.users', 'user')
                .andWhere('user.id = :user_id', {user_id: user.id});
        }

        return await query.getOne();
    }

    /**
     * This is function get all seat pattern
     * @param limit
     * @param offset
     * @param options
     */
    async getAll(limit: number, offset: number, options?: any) {
        return await this.seatPatternRepository.getAll(limit, offset, options);
    }

    /**
     * This is function create seat pattern
     * @param body
     */
    async create(body: CreateSeatPatternDto): Promise<SeatPatternEntity> {
        let provider = await this.providersInfoService.findById(body.provider_id);
        if (!provider) throw { message: 'Provider is not found!', status: 404 };
        let seat_pattern = new SeatPatternEntity();
        seat_pattern.provider = provider;
        seat_pattern = await this.seatPatternRepository.merge(seat_pattern, body);
        return await this.seatPatternRepository.save(seat_pattern);
    }

    /**
     * This is function update seat pattern
     * @param id
     * @param body
     */
    async update(id: number, body: CreateSeatPatternDto): Promise<SeatPatternEntity> {
        let seat_pattern = await this.findById(id);
        if (!seat_pattern) throw { message: 'Seat pattern not found!', status: 404 };

        let provider = await this.providersInfoService.findById(body.provider_id);
        if (!provider) throw { message: 'Provider is not found!', status: 404 };

        seat_pattern.provider = provider;
        seat_pattern = await this.seatPatternRepository.merge(seat_pattern, body);
        return await this.seatPatternRepository.save(seat_pattern);
    }

    /**
     * This is function delete seat pattern
     * @param id
     */
    async delete(id: number): Promise<boolean> {
        let seat_pattern = await this.findById(id);
        if (!seat_pattern) throw { message: 'Seat pattern not found!', status: 404 };

        seat_pattern.deleted_at = new Date();
        await this.seatPatternRepository.save(seat_pattern);
        return true;
    }
}
