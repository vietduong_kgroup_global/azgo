import {Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {VehicleEntity} from '../vehicle/vehicle.entity';
import {ProvidersInfoEntity} from '../providers-info/providers-info.entity';

@Entity('az_seat_pattern')
export class SeatPatternEntity {
    @PrimaryGeneratedColumn({ type: 'bigint', unsigned: true })
    id: number;

    @Column({
        type: 'varchar',
        length: '255',
    })
    name: string;

    @Column({
        type: 'json',
    })
    seat_map: object;

    @Column({
        type: 'int',
        nullable: true,
    })
    floor: number;

    @Column({
        type: 'int',
        nullable: true,
    })
    total_seat: number;

    @Column({
        type: 'int',
        nullable: true,
    })
    max_seat_of_vertical: number;

    @Column({
        type: 'int',
        nullable: true,
    })
    max_seat_of_horizontal: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
    })
    deleted_at: Date;

    @ManyToOne(type => ProvidersInfoEntity, provider => provider.id)
    @JoinColumn({name: 'provider_id'})
    provider: ProvidersInfoEntity;

    @OneToMany(type => VehicleEntity, vehicle => vehicle.seatPattern)
    vehicles: VehicleEntity[];
}
