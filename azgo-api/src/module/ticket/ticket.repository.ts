import {EntityRepository, Repository} from 'typeorm';
import {TicketEntity} from './ticket.entity';
@EntityRepository(TicketEntity)
export class TicketRepository extends Repository<TicketEntity>{

}
