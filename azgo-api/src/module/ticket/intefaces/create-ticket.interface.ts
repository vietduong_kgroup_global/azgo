export interface CreateTicketInterface {

    user_id: number;
    schedule_id: number;
    status?: number;
    payment_method?: number;
    payment_status?: number;
    price?: number;
    seat_name?: string;
    floor?: number;
    qr_code_status?: number;
    qr_code?: string;
    pickup_point?: string;
    destination?: string;
    luggage_info?: string;
    book_bus_reference: string;
    type?: number;
}
