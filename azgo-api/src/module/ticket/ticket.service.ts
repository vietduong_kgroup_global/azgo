import {forwardRef, HttpStatus, Inject, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {TicketRepository} from './ticket.repository';
import { StringHelper } from '@azgo-module/core/dist/utils/string.helper';
import {CreateTicketInterface} from './intefaces/create-ticket.interface';
import {TicketEntity} from './ticket.entity';
import {FirebaseService} from '../firebase/firebase.service';
import {UsersService} from '../users/users.service';
import {VehicleScheduleService} from '../vehicle-schedule/vehicleSchedule.service';
import {AdminCreateTicketDto} from './dto/admin-create-ticket.dto';
import {VehicleMovingService} from '../vehicle-moving/vehicleMoving.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import {CountCustomerInterface} from './intefaces/count-customer.interface';
import {BookShippingRepository} from '../book-shipping/book-shipping.repository';
import {AreaService} from '../area/area.service';
import {RedisUtil} from '@azgo-module/core/dist/utils/redis.util';
import config from '@azgo-module/core/dist/config';
import apiConfig from '../../../config/config';

import {RedisService} from '@azgo-module/core/dist/background-utils/Redis/redis.service';
import {VehicleScheduleRepository} from '../vehicle-schedule/vehicleSchedule.repository';
import { PhoneNumberUtil } from '@azgo-module/core/dist/utils/phone-number.util';
import {StatusCode, StatusPayment, StatusPaymentMethod, StatusVehicleSchedule} from '../../enums/status.enum';
import {VnPayService} from '../vn-pay/vn-pay.service';
import {SECRET_BOOK_BUS, SECRET_BOOK_TICKET, SECRET_BOOK_TICKET_BY_ADMIN} from '../../constants/secrets.const';
import {
    FIELD_CUSTOMER_REQUEST_BOOK_TICKET,
    KEY_BOOK_TICKET,
    KEY_CANCEL_APPROVED_BUS, KEY_CLICK_ACTION, KEY_CUSTOMER_REQUEST_BOOK_TICKET, KEY_CUSTOMER_SCAN_TICKET,
    KEY_TIMEOUT_PAYMENT,
    REQUEST_BOOK_BUS_FROM_CUSTOMER
} from '../../constants/keys.const';
import {InjectSchedule, Schedule} from 'nest-schedule';
import {VehicleGroup} from '../../enums/group.enum';

@Injectable()
export class TicketService {
    private redis;
    private redisService;
    // Book shipping
    private keyRedisCustomerRequestBookShipping = 'key_customer_request_book_shipping';
    private fieldRedisCustomerRequestBookShipping = 'field_customer_request_book_shipping';
    // Book bus
    private keyRedisCustomerRequestBookBus = 'key_customer_request_book_bus';
    private fieldRedisCustomerRequestBookBus = 'field_customer_request_book_bus';
    // use for vehicle bus
    private keyRedisVehiclePositionBus = 'vehicle_positions_bus';
    private fieldRedisVehiclePositionBus = 'field_vehicle_positions_bus';
    // use for vehicle bike
    private keyRedisVehiclePositionBike = 'vehicle_positions_bike';
    private fieldRedisVehiclePositionBike = 'field_vehicle_positions_bike';
    // use for vehicle car
    private keyRedisVehiclePositionCar = 'vehicle_positions_car';
    private fieldRedisVehiclePositionCar = 'field_vehicle_positions_car';
    // use for vehicle van
    private keyRedisVehiclePositionVan = 'vehicle_positions_van';
    private fieldRedisVehiclePositionVan = 'field_vehicle_positions_van';
    // use for vehicle bagac
    private keyRedisVehiclePositionBagac = 'vehicle_positions_bagac';
    private fieldRedisVehiclePositionBagac = 'field_vehicle_positions_bagac';
    private  connect:any;
    constructor(
        @InjectRepository(TicketRepository)
        private readonly ticketRepository: TicketRepository,
        private readonly firebaseService: FirebaseService,
        private readonly userService: UsersService,
        @Inject(forwardRef(() => VehicleScheduleService))
        private readonly vehicleScheduleService: VehicleScheduleService,
        private readonly vehicleMovingService: VehicleMovingService,
        @InjectRepository(BookShippingRepository)
        private readonly bookShippingRepository: BookShippingRepository,
        private areaService: AreaService,
        @InjectRepository(VehicleScheduleRepository)
        private readonly vehicleScheduleRepository: VehicleScheduleRepository,
        private readonly vnPayService: VnPayService,
        @InjectSchedule() private readonly schedule: Schedule,

    ) {
        this.connect= {
            port: process.env.ENV_REDIS_PORT || 6379,
            host: process.env.ENV_REDIS_HOST || '127.0.0.1', 
            password: process.env.ENV_REDIS_PASSWORD || null,
        };
        console.log(" Redis constructor:",this.connect);
        this.redisService = RedisService.getInstance(this.connect);
        this.redis = RedisUtil.getInstance();
    }

    async adminCreateTicket(body: AdminCreateTicketDto) {
        let flagExistSeatName = false;
        let checkSchedule = await this.vehicleScheduleService
            .checkVehicleSchedule(body.schedule_id, 1); // 1 = chua khoi hanh
        if (!checkSchedule) {
            throw {message: 'Vehicle schedule expise', status: 400};
        }
        // Check customer
        let country_code = body.country_code || apiConfig.countryCode;
        let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(body.phone, country_code);
        if (!data_phone_number) {
            throw {message: 'Invalid phone number', status: 400};
        }
        let checkCustomer = await this.userService.findUserByPhone(data_phone_number.national_number);
        if (!checkCustomer) {
            // If not exist then create new
            body.phone = data_phone_number.national_number;
            const infoCustomerInsert = {...body, ...{type: 3}}; // type = 3 is customer
            checkCustomer = await this.userService.adminCreateCustomer(infoCustomerInsert);
        }
        const book_bus_reference = body.phone + '_' + Math.random().toString(36).slice(2) + SECRET_BOOK_TICKET_BY_ADMIN;

        if (checkCustomer) {
            const arrSeatName = body.seat_name;
            if (arrSeatName instanceof Array) {
                if (body.seat_name.length > 0) {
                    await Promise.all(arrSeatName.map(async (value) => {

                        // Create ticket
                        const dataTicket: CreateTicketInterface = {
                            user_id: checkCustomer.id,
                            schedule_id: body.schedule_id,
                            status: 1,
                            payment_status: body.payment_status,
                            price: body.price,
                            seat_name: value,
                            floor: body.floor,
                            pickup_point: body.pickup_point,
                            destination: body.destination,
                            luggage_info: body.luggage_info,
                            book_bus_reference,
                        };
                        // set new data seat map
                        for (let i = 1 ; i <= body.floor; i++) {
                            if (checkSchedule.seat_map.hasOwnProperty('f' + i)) {
                                await Promise.all(checkSchedule.seat_map['f' + i].map((item) => {
                                    item.map((v) => {
                                        if (v.name === value && v.status !== true) {
                                            v.status = true;
                                            flagExistSeatName = true;
                                            checkSchedule.available_seat -= 1;
                                            dataTicket.seat_name = v.seat_name;
                                        }
                                    });
                                }));
                            }
                        }
                        if (!flagExistSeatName) throw {message: 'Not found seat name', status: 404};
                        await this.createTicket(dataTicket, checkCustomer.user_id.toString());

                    }));
                    // Update seat map
                    await this.vehicleScheduleService.update(checkSchedule.id, checkSchedule);
                    return true;
                }
            }else {
                throw {message: 'Seat_name not array!', status: 400};
            }
        }
        return false;
    }

    async orderTicketByCustomer(book_bus_reference: string) {
        let flagExistSeatName;

        // Get list request book ticket by customer
        let getCache = await this.redis.getInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_TICKET, FIELD_CUSTOMER_REQUEST_BOOK_TICKET);
        if (!getCache.isValidate) throw {message: 'Not found request', status: StatusCode.NOT_FOUND};
        const listRequestBookTicket = JSON.parse(getCache.value);
        const requestBook = _.find(listRequestBookTicket, {book_bus_reference});
        if (!requestBook)  throw {message: 'Not found request', status: StatusCode.NOT_FOUND};

        // Check exist vehicle schedule
        let checkSchedule = await this.vehicleScheduleService
            .checkVehicleSchedule(requestBook.schedule_id, StatusVehicleSchedule.UNSTART); // 1 = chua khoi hanh
        if (!checkSchedule) throw {message: 'Chuyến xe không họp lệ', status: StatusCode.BAD_REQUEST};

        // Loop order seat
        const arrSeatName = requestBook.order_seat;
        let arrTicketOrder = [];
        if (arrSeatName instanceof Array && arrSeatName.length > 0) {
            await Promise.all(arrSeatName.map(async (value) => {
                // Create ticket
                const dataTicket: CreateTicketInterface = {
                    user_id: requestBook.user_id,
                    schedule_id: requestBook.schedule_id,
                    payment_method: StatusPaymentMethod.BANK,
                    payment_status: StatusPayment.PAID,
                    price: requestBook.price,
                    seat_name: value,
                    floor: Number(requestBook.floor),
                    pickup_point: requestBook.pickup_point,
                    destination: requestBook.destination,
                    luggage_info: requestBook.luggage_info,
                    book_bus_reference: requestBook.book_bus_reference,
                };
                // set new data seat map
                for (let i = 1; i <= requestBook.floor; i++) {
                    if (checkSchedule.seat_map.hasOwnProperty('f' + i)) {
                        await Promise.all(checkSchedule.seat_map['f' + i].map((item) => {
                            item.map((v) => {
                                if (v.name === value && v.status === false && v.is_waiting === true) {
                                    v.status = true;
                                    v.is_waiting = false;
                                    flagExistSeatName = true;
                                    dataTicket.seat_name = v.seat_name;
                                }
                            });
                        }));
                    }
                }
                if (!flagExistSeatName) throw {message: 'Not found seat name', status: StatusCode.NOT_FOUND};
                const ticketEntity = await this.createTicket(dataTicket, requestBook.customer_uuid);
                arrTicketOrder.push(ticketEntity);

            }));
            // Update seat map
            await this.vehicleScheduleService.update(checkSchedule.id, checkSchedule);
            // Remove request book ticket by customer
            await _.remove(listRequestBookTicket, {book_bus_reference});
            await this.redis.setInfoRedis(
                KEY_CUSTOMER_REQUEST_BOOK_TICKET, FIELD_CUSTOMER_REQUEST_BOOK_TICKET, JSON.stringify(listRequestBookTicket));
            return arrTicketOrder;
        }
        else {
            throw {message: 'Seat name is not array!', status: StatusCode.BAD_REQUEST};
        }
    }
    async cronRevertSeatMap(order_seat: any[], schedule_id: number, floor: number, customer_uuid: string) {
      // Remove request book ticket by customer
      let getCache = await this.redis.getInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_TICKET, FIELD_CUSTOMER_REQUEST_BOOK_TICKET);
      if (getCache.isValidate) {
          const listRequestBookTicket = JSON.parse(getCache.value);
          await _.remove(listRequestBookTicket, {customer_uuid});
          await this.redis.setInfoRedis(
              KEY_CUSTOMER_REQUEST_BOOK_TICKET, FIELD_CUSTOMER_REQUEST_BOOK_TICKET, JSON.stringify(listRequestBookTicket));
      }

      let checkSchedule = await this.vehicleScheduleService
          .checkVehicleSchedule(schedule_id, StatusVehicleSchedule.UNSTART); // 1 = chua khoi hanh
      if (!checkSchedule) return true; // stop cron job
      if (order_seat instanceof Array) {
          if (order_seat.length > 0) {
              await Promise.all(order_seat.map(async (value) => {
                  for (let i = 1; i <= Number(floor); i++) {
                      if (checkSchedule.seat_map.hasOwnProperty('f' + i)) {
                          await Promise.all(checkSchedule.seat_map['f' + i].map((item) => {
                              item.map((v) => {
                                  if (v.name === value && v.status !== true) {
                                      v.is_waiting = false;
                                      checkSchedule.available_seat += 1;
                                  }
                              });
                          }));
                      }
                  }
              }));
              // Update seat map
              await this.vehicleScheduleService.update(checkSchedule.id, checkSchedule);
          }
      }
      // Emit event to customer
      const dataNotification = {
          key: KEY_TIMEOUT_PAYMENT,
          click_action: KEY_CLICK_ACTION,
          data: { message: 'Hết thời gian thanh toán'},
      };
      this.redisService.triggerEventBookBus({room: 'user_' + customer_uuid, message: dataNotification});
      return true;

    }
    async createUrlPayment(body: any) {
        body.created_at = moment().format('YYYY-MM-DD');
        body.expires_approve_miliseconds = Number(process.env.REQUEST_BOOK_BUS_APPROVED_TIMER || 300000);
        let flagExistSeatName, requestBook;
        let listRequestBookTicket = [];
        // Get list request book ticket by customer
        let getCache = await this.redis.getInfoRedis(KEY_CUSTOMER_REQUEST_BOOK_TICKET, FIELD_CUSTOMER_REQUEST_BOOK_TICKET);
        if (getCache.isValidate) {
            listRequestBookTicket = JSON.parse(getCache.value);
            requestBook = _.find(listRequestBookTicket, {customer_uuid: body.customer_uuid});
            if (requestBook) return requestBook;
        }

        // Check vehicle schedule
        let checkSchedule = await this.vehicleScheduleService
            .checkVehicleSchedule(body.schedule_id, StatusVehicleSchedule.UNSTART); // 1 = chua khoi hanh
        if (!checkSchedule) throw {message: 'Chuyến xe không họp lệ', status: StatusCode.BAD_REQUEST};
        const book_bus_reference = body.customer_uuid + '_' + Math.random().toString(36).slice(2) + SECRET_BOOK_TICKET;

        const arrSeatName = body.order_seat;
        if (arrSeatName instanceof Array && arrSeatName.length > 0) {
            await Promise.all(arrSeatName.map(async (value) => {
                for (let i = 1; i <= body.floor; i++) {
                    if (checkSchedule.seat_map.hasOwnProperty('f' + i)) {
                        await Promise.all(checkSchedule.seat_map['f' + i].map((item) => {
                            item.map((v) => {
                                if (v.name === value && v.status === false && v.is_waiting === false) {
                                    v.is_waiting = true;
                                    flagExistSeatName = true;
                                    checkSchedule.available_seat -= 1;
                                }
                            });
                        }));
                    }
                }
                if (!flagExistSeatName) throw {message: 'This seat is has a people set', status: StatusCode.CODE_CONFLICT};
            }));
            // Update seat map
            await this.vehicleScheduleService.update(checkSchedule.id, checkSchedule);
        }
        else {
            throw {message: 'Seat name is not array!', status: StatusCode.BAD_REQUEST};
        }
        const amount =  Number(checkSchedule.route.base_price) * arrSeatName.length * 100; // Khử số thập phân, vn pay bắt buộc nhân 100
        body.total_price = Number(checkSchedule.route.base_price) * arrSeatName.length;
        body.price = checkSchedule.route.base_price;
        body.book_bus_reference = book_bus_reference;
        // Add Cron
        await this.schedule.scheduleIntervalJob(
            KEY_BOOK_TICKET + book_bus_reference,
            Number(process.env.TIMEOUT_PAYMENT) || 120000,
            () => this.cronRevertSeatMap(arrSeatName, body.schedule_id, body.floor, body.customer_uuid),
        );

        const dataPost = {
            vnp_Amount: Number(amount),
            vnp_BankCode: 'NCB',
            vnp_OrderInfo: 'Thanh toan book ve',
            vnp_OrderType: '240000',
            vnp_TxnRef: book_bus_reference,
        };
        const url_payment = await this.vnPayService.createPaymentUrl(dataPost, body.vnp_IpAddr);
        body.url_payment = url_payment;
        // Add data redis
        listRequestBookTicket.push(body);
        await this.redis.setInfoRedis(
            KEY_CUSTOMER_REQUEST_BOOK_TICKET, FIELD_CUSTOMER_REQUEST_BOOK_TICKET,
            JSON.stringify(listRequestBookTicket),
        );
        return body;
    }
    async checkExistPaidByBookBusReference(book_bus_reference: string) {
        return  await this.ticketRepository.createQueryBuilder('ticket')
            .where('book_bus_reference = :book_bus_reference', {book_bus_reference})
            .andWhere('deleted_at is null')
            .getOne();
    }

    async createTicket(data: CreateTicketInterface, customer_uuid: string) {

        const orderTicket = await this.ticketRepository.save(data);
        let ticketToUpdate;
        if (orderTicket) {
            ticketToUpdate = await this.ticketRepository.findOne(orderTicket.id);
            ticketToUpdate.ticket_code = new Date().getTime().toString(36);
            ticketToUpdate.qr_code = StringHelper.encodeBase64(JSON.stringify(
                {schedule_id: orderTicket.schedule_id, user_uuid: customer_uuid, ticket_id: orderTicket.id, is_shipping: false },
            ));
            await this.ticketRepository.save(ticketToUpdate);
        }
        return await this.ticketRepository.findOne(orderTicket.id, { relations: ['vehicleSchedule', 'vehicleSchedule.vehicle']});
    }

    async listTicketFollowSchedule(schedule_id: number) {
        return  await this.ticketRepository.createQueryBuilder('ticket')
            .leftJoinAndSelect('ticket.user', 'user')
            .where('ticket.schedule_id = :schedule_id', {schedule_id})
            .andWhere('ticket.deleted_at is null')
            .getMany();
    }
    async getTicketFollowUserId(user_id: number, qr_code_status: number) {
        let concacTicket;
        if (!qr_code_status) {
            const ticket = await this.ticketRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.vehicleSchedule', 'vehicleSchedule')
                .leftJoinAndSelect('vehicleSchedule.vehicle', 'vehicle')
                .leftJoinAndSelect('vehicle.provider', 'provider')
                .leftJoinAndSelect('vehicleSchedule.route', 'route')
                .leftJoinAndSelect('route.endPoint', 'endPoint')
                .where('ticket.user_id = :user_id', {user_id})
                .andWhere('ticket.deleted_at is null')
                .orderBy('ticket.id', 'DESC')
                .getMany();
            const ticketShipping = await this.bookShippingRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.vehicleSchedule', 'vehicleSchedule')
                .leftJoinAndSelect('vehicleSchedule.vehicle', 'vehicle')
                .leftJoinAndSelect('vehicle.provider', 'provider')
                .leftJoinAndSelect('vehicleSchedule.route', 'route')
                .leftJoinAndSelect('route.endPoint', 'endPoint')
                .where('ticket.user_id = :user_id', {user_id})
                .andWhere('ticket.deleted_at is null')
                .orderBy('ticket.id', 'DESC')
                .getMany();
            concacTicket = [...ticket, ...ticketShipping];

        } else {
            const ticket =  await this.ticketRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.vehicleSchedule', 'vehicleSchedule')
                .leftJoinAndSelect('vehicleSchedule.vehicle', 'vehicle')
                .leftJoinAndSelect('vehicle.provider', 'provider')
                .leftJoinAndSelect('vehicleSchedule.route', 'route')
                .leftJoinAndSelect('route.endPoint', 'endPoint')
                .where('ticket.user_id = :user_id', {user_id})
                .andWhere('ticket.qr_code_status = :qr_code_status', {qr_code_status})
                .andWhere('ticket.deleted_at is null')
                .orderBy('ticket.id', 'DESC')
                .getMany();
            const ticketShipping = await this.bookShippingRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.vehicleSchedule', 'vehicleSchedule')
                .leftJoinAndSelect('vehicleSchedule.vehicle', 'vehicle')
                .leftJoinAndSelect('vehicle.provider', 'provider')
                .leftJoinAndSelect('vehicleSchedule.route', 'route')
                .leftJoinAndSelect('route.endPoint', 'endPoint')
                .where('ticket.user_id = :user_id', {user_id})
                .andWhere('ticket.qr_code_status = :qr_code_status', {qr_code_status})
                .andWhere('ticket.deleted_at is null')
                .orderBy('ticket.id', 'DESC')
                .getMany();
            concacTicket = [...ticket, ...ticketShipping];
        }
        if (concacTicket) {
            // Get vehicle postion on redis
            const redisKey = await this.keyRedisVehiclePosition(VehicleGroup.BUS);
            const redisField = await this.fieldRedisVehiclePosition(VehicleGroup.BUS);
            let getCachePositionVehicles = await this.redis.getInfoRedis(redisKey, redisField);
            if (getCachePositionVehicles.isValidate) {
                let arrTicket = [];
                const listPositionVehicles = JSON.parse(getCachePositionVehicles.value);
                await Promise.all(concacTicket.map((value) => {
                    try {
                        const endPoint_coordinates: any = value.vehicleSchedule.route.endPoint.coordinates;
                        const lat_endPoint = endPoint_coordinates.lat;
                        const lng_endPoint = endPoint_coordinates.lng;
                        const coordinates_destination = {
                            lat:  lat_endPoint.toString(),
                            lng:  lng_endPoint.toString(),
                        };
                        const rsPositionVehicle = listPositionVehicles.find(item => item.schedule_id === value.schedule_id);
                        let temp;
                        if (rsPositionVehicle) {
                            temp = {...value, ...{
                                    coordinates_vehicle: {lat: rsPositionVehicle.lat, lng: rsPositionVehicle.lng},
                                    coordinates_destination,
                                }};

                        } else {
                            temp = {...value, ...{
                                    coordinates_vehicle: null,
                                    coordinates_destination,
                                }};
                        }
                        arrTicket.push(temp);

                    }catch (e) {
                        throw {message: e.message, status: 400};
                    }

                }));
                return arrTicket;
            }
        }
        return false;
    }

    async findOne(id: number) {
        return await this.ticketRepository.createQueryBuilder('ticket')
            .where('ticket.id = :id', {id})
            .andWhere('ticket.deleted_at is null')
            .getOne();
    }
    async updateStatusPaymentByReference(book_bus_reference: string) {
        const result = await this.ticketRepository.createQueryBuilder('ticket')
            .update(TicketEntity)
            .set({payment_status: 2})//  2 da thanh toan
            .where('book_bus_reference = :book_bus_reference', {book_bus_reference})
            .andWhere('deleted_at is null')
            .execute();
        if (result) {
            const ticket = await this.ticketRepository.createQueryBuilder('ticket')
                .where('ticket.book_bus_reference = :book_bus_reference', {book_bus_reference})
                .select('ticket.ticket_code as ticket_code')
                .getMany();
            if (ticket) {
                let arr_ticket_code = [];
                ticket.map((v) => {
                    arr_ticket_code.push(v.ticket_code);
                });
                return arr_ticket_code;
            }
        }
        return false;
    }

    async update(id: number, data: TicketEntity) {
        return await this.ticketRepository.update(id, data);
    }

    async findOneBySchedule(schedule_id: number) {
        return  await this.ticketRepository.createQueryBuilder('ticket')
            .where('schedule_id = :schedule_id', {schedule_id})
            .andWhere('deleted_at is null')
            .getOne();
    }

    async findBySchedule(schedule_id: number) {
        return  await this.ticketRepository.find({
            where: {
                schedule_id,
                status: 1, // ticket not scan
                type: 2, // book vehicle
                deleted_at: null,
            },
            relations: ['user', 'user.customerProfile'],
            order: {user_id: 'ASC'},
        });
    }
    async findCustomerOnBus(id: number) {
        return  await this.ticketRepository.find({
            where: {
                schedule_id: id,
                qr_code_status: 2,
                deleted_at: null,
            },
            relations: ['user', 'user.customerProfile'],
            order: {user_id: 'ASC'},
        });
    }

    async scanQRCode(qr_code: string, driver_id: number) {
        let listRequestRedis, requestBook, indexRequest, ticket, result;
        const info_qr_code = JSON.parse(StringHelper.decodeBase64(qr_code));
        // const today = moment(new Date(), 'Asia/Ho_Chi_Minh').format('YYYY-MM-DD HH:mm:ss');

        // 1:Chua khoi hanh, 2: Da khoi hanh, 3 Ket thuc
        const schedule = await this.vehicleScheduleRepository.createQueryBuilder('schedule')
            .where('schedule.id = :schedule_id', {schedule_id: info_qr_code.schedule_id} )
            .andWhere('schedule.user_id = :driver_id', {driver_id})
            // .andWhere('schedule.start_date <= :start_date', {start_date: today})
            .getOne();
        if (!schedule) throw {message: 'Chuyến xe chưa chạy', status: 400};
        if (info_qr_code.is_shipping) {
            ticket = await this.bookShippingRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.vehicleSchedule', 'vehicleSchedule')
                .leftJoinAndSelect('vehicleSchedule.route', 'route')
                .leftJoinAndSelect('route.endPoint', 'endPoint')
                .where('ticket.id = :id', {id: info_qr_code.ticket_id})
                .andWhere('ticket.deleted_at is null')
                .andWhere('ticket.payment_status = :payment_status', {payment_status: StatusPayment.PAID})
                .andWhere('vehicleSchedule.user_id = :driver_id', {driver_id})
                .getOne();
            if (ticket) {
                let getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping);
                if (getCache.isValidate) {
                    listRequestRedis = JSON.parse(getCache.value);
                    indexRequest = listRequestRedis.findIndex(req => req.customer_uuid === info_qr_code.user_uuid);
                    requestBook = listRequestRedis.find(req => req.customer_uuid === info_qr_code.user_uuid);
                    if (requestBook) {
                        // Update request booking
                        requestBook.status = 3; // Dang di chuyen
                        listRequestRedis[indexRequest] = requestBook;
                        await this.redis.setInfoRedis(
                            this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus, JSON.stringify(listRequestRedis));
                    }
                }
            }
        } else {
            ticket = await this.ticketRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.vehicleSchedule', 'vehicleSchedule')
                .leftJoinAndSelect('vehicleSchedule.route', 'route')
                .leftJoinAndSelect('route.endPoint', 'endPoint')
                .where('ticket.id = :id', {id: info_qr_code.ticket_id})
                .andWhere('ticket.payment_status = :payment_status', {payment_status: StatusPayment.PAID})
                .andWhere('vehicleSchedule.user_id = :driver_id', {driver_id})
                .andWhere('ticket.deleted_at is null')
                .getOne();
            if (ticket) {
               let getCache = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus);
               if (getCache.isValidate) {
                   listRequestRedis = JSON.parse(getCache.value);
                   indexRequest = listRequestRedis.findIndex(req => req.customer_uuid === info_qr_code.user_uuid);
                   requestBook = listRequestRedis.find(req => req.customer_uuid === info_qr_code.user_uuid);
                   if (requestBook) {
                       // Update request booking
                       requestBook.status = 3; // Dang di chuyen
                       listRequestRedis[indexRequest] = requestBook;
                       await this.redis.setInfoRedis(
                           this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus, JSON.stringify(listRequestRedis));
                   }
               }
           }
        }
        if (ticket) {
            const userEntity = await this.userService.findById(ticket.user_id);
            if (ticket.qr_code_status === 1){
                const dataSave = { ...ticket, ...{qr_code_status: 2} };
                if (info_qr_code.is_shipping) {
                    result = await this.bookShippingRepository.save(dataSave);
                } else {
                    result = await this.ticketRepository.save(dataSave);
                }
                if (result){
                    const userFCM = await this.firebaseService.findOne(userEntity.user_id.toString());
                    // send notification to driver
                    const notification = {
                        title:  'Scan vé',
                        body: 'Chúc mừng bạn đã scan vé thành công',
                    };
                    if (!ticket.vehicleSchedule.route.endPoint.coordinates) return false;
                    const lat_endPoint = ticket.vehicleSchedule.route.endPoint.coordinates.lat;
                    const lng_endPoint = ticket.vehicleSchedule.route.endPoint.coordinates.lng;
                    const coordinates_destination = {
                        lat:  lat_endPoint.toString(),
                        lng:  lng_endPoint.toString(),
                    };
                    const dataNotification = {
                        key: KEY_CUSTOMER_SCAN_TICKET,
                        click_action: KEY_CLICK_ACTION,
                        coordinates_destination,
                    };
                    if (requestBook) {
                        // Emit event book bus
                        this.redisService.triggerEventBookBus({room: 'user_' + requestBook.customer_uuid, message: dataNotification});
                    }
                    if (userFCM) {
                        const token = userFCM.token ? userFCM.token : userFCM.apns_id;
                        await this.firebaseService.pushNotificationWithTokens([token], notification, dataNotification);
                    }
                    return result;
                }
            } else {
                return {message: 'QR Code đã được scan trước đó'};
            }
        }
        return  null;
    }

    async getTicketDetail(id: number) {
        return await this.ticketRepository.createQueryBuilder('ticket')
            .where('ticket.id = :id', {id})
            .andWhere('ticket.deleted_at is null')
            .leftJoinAndSelect('ticket.user', 'user')
            .leftJoinAndSelect('user.customerProfile', 'customerProfile')
            .getOne();

    }

    async countTicketAndSumPrice(schedule_id: number, type: number) {
        return await this.ticketRepository.createQueryBuilder('ticket')
            .where('ticket.type = :type', {type} )
            .andWhere('ticket.schedule_id = :schedule_id', {schedule_id} )
            .andWhere('ticket.deleted_at is null')
            .select('COUNT(ticket.id) as count, SUM(ticket.price) as total_price')
            .getRawOne();
    }

    async deleteOrderTicket(id: number) {
        const rs =  await this.ticketRepository.createQueryBuilder('ticket')
            .where('ticket.id = :id', {id} )
            .andWhere('ticket.status = :status', {status: 1} )
            .andWhere('ticket.deleted_at is null')
            .getOne();
        if (rs) {
            const checkSchedule = await this.vehicleScheduleService.getDetail(rs.schedule_id);
            let flagFloor = checkSchedule.vehicle.seatPattern.floor;
            let flagExistSeatName = false;
            if (checkSchedule) {
                switch (checkSchedule.status) {
                    // Case 1: If status vehicle schedule = 1 then update seat map stay at vehicle schedule
                    case 1:
                        for (let i = 1; i <= flagFloor; i++) {
                            if (checkSchedule.seat_map.hasOwnProperty('f' + i)) {
                                checkSchedule.seat_map['f' + i].map((item) => {
                                    item.map((v) => {
                                        if (v.seat_name === rs.seat_name && v.status === true) {
                                            v.status = false;
                                            flagExistSeatName = true;
                                        }
                                    });
                                });
                            }
                        }
                        // Update seat_map in vehicle schedule
                        checkSchedule.available_seat += 1;
                        await this.vehicleScheduleService.update(checkSchedule.id, checkSchedule);
                        break;
                    // Case 2:  If status vehicle schedule = 2 then update seat map stay at vehicle running
                    case 2:
                        const vehicleRunning = await this.vehicleMovingService.findOne(rs.schedule_id);
                        if (vehicleRunning && vehicleRunning.status === 1) {
                            for (let i = 1; i <= flagFloor; i++) {
                                if (vehicleRunning.seat_map.hasOwnProperty('f' + i)) {
                                    vehicleRunning.seat_map['f' + i].map((item) => {
                                        item.map((v) => {
                                            if (v.seat_name === rs.seat_name && v.status === true) {
                                                v.status = false;
                                                flagExistSeatName = true;
                                            }
                                        });
                                    });
                                }
                            }
                            // Update seat_map in vehicle schedule
                            await this.vehicleMovingService.updateBySchedule(vehicleRunning);
                        }
                        break;
                    default:
                        throw {message: 'Vé đã hết hạn sử dụng, hoặc chuyến đó đã chạy xong', status: 400};
                }
            }
            rs.deleted_at = new Date();
            await this.ticketRepository.save(rs);
            return true;
        }
        return false;
    }
    async deleteAllTicketBySchedule(schedule_id: number) {
        return await this.ticketRepository.createQueryBuilder('ticket')
            .update(TicketEntity)
            .set({deleted_at: new Date()})
            .where('schedule_id = :schedule_id', {schedule_id})
            .execute();
    }
    async countAmountCustomerByBus(options: CountCustomerInterface): Promise<any>  {
        // count amount book bus + book ticket
        let queryTicket = await this.ticketRepository.createQueryBuilder('ticket')
            .leftJoin('ticket.vehicleSchedule', 'vehicleSchedule')
            .select('DISTINCT ticket.user_id');
        if (options.provider_id) {
            queryTicket.where('vehicleSchedule.provider_id = :provider_id', {provider_id: options.provider_id});
        }
        if (options && options.is_in_month === 'true') {
            const currentDate = moment(new Date(), 'YYYY-MM').format('YYYY-MM').toString();
            queryTicket.andWhere('vehicleSchedule.start_date like :created_at', {created_at: '%' + currentDate + '%'});
        }
        if (options && options.from_date) {
            const from_date_tostring = await this.formatDate(options.from_date) + ' 00:00:00';
            queryTicket.andWhere('vehicleSchedule.start_date >= :from_date_tostring', {from_date_tostring});

        }
        if (options && options.to_date) {
            const to_date_tostring = await this.formatDate(options.to_date) + ' 23:59:59';
            queryTicket.andWhere('vehicleSchedule.start_date <= :to_date_tostring', {to_date_tostring});

        }
        if (options && options.driver_id) {
            queryTicket.andWhere('vehicleSchedule.user_id = :driver_id', {driver_id: options.driver_id});

        }
        const ticket = await queryTicket.andWhere('ticket.deleted_at is null').getRawMany();

        // count amount customer book shipping
        let shipping = await this.bookShippingRepository.createQueryBuilder('ticket')
            .leftJoin('ticket.vehicleSchedule', 'vehicleSchedule')
            .select('DISTINCT ticket.user_id');
        if (options.provider_id) {
            shipping.where('vehicleSchedule.provider_id = :provider_id', {provider_id: options.provider_id});
        }
        if (options && options.is_in_month === 'true') {
            const currentDate = moment(new Date(), 'YYYY-MM').format('YYYY-MM').toString();
            shipping.andWhere('vehicleSchedule.start_date like :created_at', {created_at: '%' + currentDate + '%'});
        }
        if (options && options.from_date) {
            const from_date_tostring = await this.formatDate(options.from_date) + ' 00:00:00';
            shipping.andWhere('vehicleSchedule.start_date >= :from_date_tostring', {from_date_tostring});

        }
        if (options && options.to_date) {
            const to_date_tostring = await this.formatDate(options.to_date) + ' 23:59:59';
            shipping.andWhere('vehicleSchedule.start_date <= :to_date_tostring', {to_date_tostring});

        }
        if (options && options.driver_id) {
            shipping.andWhere('vehicleSchedule.user_id = :driver_id', {driver_id: options.driver_id});

        }
        const getShipping = await shipping.andWhere('ticket.deleted_at is null').getRawMany();
        const unionByUserId = _.unionBy(ticket, getShipping, 'user_id');
        return {amount_customer: unionByUserId.length};
    }

    async revenuePerVehicleBus(limit: number, offset: number,
        options: {is_in_month?: string, from_date?: string, to_date?: string, provider_id: number }) {
        let query = await this.ticketRepository.createQueryBuilder('ticket')
            .leftJoinAndSelect('ticket.vehicleSchedule', 'vehicleSchedule')
            .leftJoinAndSelect('vehicleSchedule.vehicle', 'vehicle')
            .leftJoinAndSelect('vehicle.vehicleType', 'vehicleType')
            .where('ticket.deleted_at is null');
        if (options) {
            if (options.is_in_month === 'true') {
                const currentDate = moment(new Date(), 'YYYY-MM').format('YYYY-MM').toString();
                query.where('ticket.created_at like :created_at', {created_at: '%' + currentDate + '%'});
            }
            if (options.from_date) {
                const from_date_tostring = await this.formatDate(options.from_date);
                query.andWhere('ticket.created_at >= :from_date_tostring', {from_date_tostring});
            }
            if (options.to_date) {
                const to_date_tostring = await this.formatDate(options.to_date);
                query.andWhere('ticket.created_at <= :to_date_tostring', {to_date_tostring});
            }
            if (options.provider_id) {
                query.andWhere('vehicleSchedule.provider_id = :provider_id', {provider_id: options.provider_id});
            }
        }
        query.select('vehicle.license_plates as license_plates,' +
            ' vehicleType.name as vehicle_type_name,' +
            ' ticket.created_at as created_at')
            .addSelect('SUM(ticket.price)', 'total_price')
            .addSelect('SUM(ticket.price)', 'total_price_azgo')
            .addSelect('COUNT(DISTINCT(vehicle.license_plates)) as count')
            .groupBy('vehicle.id');

        const results = await query.limit(limit).offset(offset).getRawMany();
        const countALl = await query.addSelect('COUNT(DISTINCT(vehicle.license_plates)) as count').getRawOne();

        return {
            success: true,
            data: results,
            total:  countALl ? countALl.count : 0,
        };
    }

    async deleteTicketByCustomer(body: {ticket_code: string, customer_id: number}) {
        const rs =  await this.ticketRepository.createQueryBuilder('ticket')
            .leftJoinAndSelect('ticket.vehicleSchedule', 'vehicleSchedule')
            .where('ticket.ticket_code = :ticket_code', {ticket_code: body.ticket_code} )
            .andWhere('ticket.user_id = :customer_id', {customer_id: body.customer_id} )
            .andWhere('ticket.deleted_at is null')
            .getOne();
        if (rs) {
            rs.deleted_at = new Date();
            await this.ticketRepository.save(rs);
            return rs;
        }
        return false;
    }

    async deleteTicketByBookBusReference(body: {book_bus_reference: string, customer_id: number}) {
        const result = await this.ticketRepository.createQueryBuilder('ticket')
            .leftJoinAndSelect('ticket.vehicleSchedule', 'vehicleSchedule')
            .where('ticket.book_bus_reference = :book_bus_reference', {book_bus_reference: body.book_bus_reference} )
            .andWhere('ticket.user_id = :customer_id', {customer_id: body.customer_id} )
            .andWhere('ticket.qr_code_status = :qr_code_status', {qr_code_status: 1} )
            .andWhere('ticket.deleted_at is null')
            .andWhere('vehicleSchedule.status = :status', {status: StatusVehicleSchedule.UNSTART})
            .getMany();
        await this.ticketRepository.createQueryBuilder('ticket')
            .update(TicketEntity)
            .where('book_bus_reference = :book_bus_reference', {book_bus_reference: body.book_bus_reference} )
            .andWhere('user_id = :customer_id', {customer_id: body.customer_id} )
            .andWhere('qr_code_status = :qr_code_status', {qr_code_status: 1} )
            .andWhere('deleted_at is null')
            .set({deleted_at: new Date()})
            .execute();
        return result;
    }

    /*
    * Input format: DD/MM/YYYY
    *
    * */
    formatDate(date: string) {
        const dateSplit = date.split('/');
        // month is 0-based, that's why we need dataParts[1] - 1
        const dateObject = new Date(+dateSplit[2],  +dateSplit[1] - 1, +dateSplit[0]);
        const formatDate = moment(dateObject, 'YYYY-MM-DD');
        return formatDate.format('YYYY-MM-DD').toString();
    }
    async fieldRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.fieldRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.fieldRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.fieldRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.fieldRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.fieldRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    async keyRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.keyRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.keyRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.keyRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.keyRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.keyRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    }
