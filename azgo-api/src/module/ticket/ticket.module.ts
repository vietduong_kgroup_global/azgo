import {forwardRef, Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {TicketRepository} from './ticket.repository';
import {TicketService} from './ticket.service';
import { TicketController } from './ticket.controller';
import {UsersModule} from '../users/users.module';
import {FirebaseModule} from '../firebase/firebase.module';
import {VehicleScheduleModule} from '../vehicle-schedule/vehicleSchedule.module';
import {VehicleMovingModule} from '../vehicle-moving/vehicleMoving.module';
import {ProvidersInfoModule} from '../providers-info/providers-info.module';
import {CustomChargeModule} from '../custom-charge/custom-charge.module';
import {BookShippingRepository} from '../book-shipping/book-shipping.repository';
import {AreaModule} from '../area/area.module';
import {VehicleScheduleRepository} from '../vehicle-schedule/vehicleSchedule.repository';
import {VnPayModule} from '../vn-pay/vn-pay.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            TicketRepository, BookShippingRepository, VehicleScheduleRepository,
        ]),
        UsersModule,
        FirebaseModule,
        forwardRef(() => VehicleScheduleModule),
        forwardRef(() => VehicleMovingModule),
        ProvidersInfoModule,
        CustomChargeModule,
        AreaModule,
        VnPayModule,

    ],
    exports: [
        TicketService,
    ],
    providers: [TicketService],
    controllers: [TicketController],
})
export class TicketModule {

}
