import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {ApiModelProperty} from '@nestjs/swagger';
import {VehicleScheduleEntity} from '../vehicle-schedule/vehicleSchedule.entity';
import {UsersEntity} from '../users/users.entity';

@Entity('az_ticket')
export class TicketEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    ticket_code: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    book_bus_reference: string;

    @Column({
        type: 'int',
        nullable: false,
    })
    @ApiModelProperty({ required: true, example: 1})
    schedule_id: number;

    @ManyToOne(type => VehicleScheduleEntity, vehicleSchedule => vehicleSchedule.id)
    @JoinColumn({name: 'schedule_id', referencedColumnName: 'id'})
    vehicleSchedule: VehicleScheduleEntity;

    @Column({
        type: 'int',
        nullable: false,
    })
    @ApiModelProperty({ required: true, example: 1})
    user_id: number;

    @ManyToOne(type => UsersEntity, user => user.user_id)
    @JoinColumn({name: 'user_id', referencedColumnName: 'id'})
    user: UsersEntity;

    @Column({
        type: 'smallint',
        default: 1,
        comment: '1: chua su dung, 2:su dung',
    })
    @ApiModelProperty({ required: true, example: 1, default: 1})
    status: number;

    @Column({
        type: 'smallint',
        nullable: true,
    })
    @ApiModelProperty({ example: 1})
    payment_method: number;

    @Column({
        type: 'smallint',
        nullable: true,
    })
    @ApiModelProperty({ example: 1})
    payment_status: number;

    @Column({
        type: 'float',
        nullable: true,
    })
    price: number;

    @Column({
        type: 'varchar',
        length: 255,
        nullable: true,
    })
    @ApiModelProperty({ example: 'A1'})
    seat_name: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    @ApiModelProperty({ example: '11:30'})
    time_pick_up: string;

    @Column({
        type: 'int',
        nullable: true,
    })
    @ApiModelProperty({ example: 1})
    floor: number;

    @Column({
        type: 'text',
        nullable: true,
    })
    @ApiModelProperty({ example: 'abcxyz'})
    qr_code: string;

    @Column({
        type: 'smallint',
        nullable: true,
    })
    @ApiModelProperty({ example: 1})
    qr_code_status: number;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    @ApiModelProperty({ example: 'HCM'})
    pickup_point: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    @ApiModelProperty({ example: 'Can Tho'})
    destination: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    @ApiModelProperty({ example: 'Balo'})
    luggage_info: string;

    @Column({
        type: 'smallint',
        default: 1,
    })
    @ApiModelProperty({ example: '1: Book ticket, 2: Book vehicle'})
    type: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;
}
