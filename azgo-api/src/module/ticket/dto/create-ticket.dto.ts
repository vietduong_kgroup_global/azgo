import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsString} from 'class-validator';
export class CreateTicketDto {

    @IsNotEmpty()
    @ApiModelProperty({
        example: '1188cbac-94f9-4c04-8d96-17f5bac71530_jqocytxdi1bookticket',
        description: 'Book ticket reference', type: String, required: true,
    })
    @IsString()
    book_bus_reference: string;
}
