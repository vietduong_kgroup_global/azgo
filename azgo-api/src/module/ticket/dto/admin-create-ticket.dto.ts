import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsNumber, IsString} from 'class-validator';
export class AdminCreateTicketDto {

    @IsNotEmpty()
    @ApiModelProperty({ required: true, example: 1})
    schedule_id: number;

    @IsNotEmpty()
    @ApiModelProperty({example: 'Nguyễn Văn A',  required: true})
    @IsString()
    full_name: string;

    @IsNotEmpty()
    @ApiModelProperty({ required: true, example: '357739234'})
    phone: string;

    @IsNotEmpty()
    @ApiModelProperty({ required: true, example: 84})
    country_code: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1})
    @IsNumber()

    payment_status: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 120000})
    price: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: ['111', '112']})
    seat_name: [string];

    @IsNotEmpty()
    @ApiModelProperty({ example: 1})
    @IsNumber()
    floor: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'Hồ Chí Minh'})
    pickup_point: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'Can Tho'})
    destination: string;

    @IsNotEmpty()
    @ApiModelProperty({example: '1 cái balo',  required: true})
    @IsString()
    luggage_info: string;

}
