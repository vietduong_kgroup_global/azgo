import {IsNotEmpty, IsNumber, IsString} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';

export class CreateUrlPaymentDto {

    @IsNotEmpty()
    @ApiModelProperty({example: 'NCB', description: 'Mã ngân hàng', required: true })
    vnp_BankCode: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'Mã chuyến', type: Number, required: true })
    schedule_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: ['111'], description: 'Array name', type: [String], required: true})
    order_seat: [string];

    @IsNotEmpty()
    @ApiModelProperty({ example: 1, type: String, description: 'Xe đó có mấy tầng', required: true })
    @IsNumber()
    floor: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'HCM', description: 'Điểm đón', type: String, required: true})
    pickup_point: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'Can Tho', description: 'Điểm đến', type: String, required: true})
    destination: string;

    @IsNotEmpty()
    @ApiModelProperty({example: '1 cái balo',  required: true})
    @IsString()
    luggage_info: string;

}
