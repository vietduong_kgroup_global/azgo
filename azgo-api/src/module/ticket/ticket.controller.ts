import {
    Body,
    Controller, Delete,
    Get,
    HttpStatus,
    Param,
    Post,
    Put,
    Query,
    Req,
    Res,
    UseGuards,
    UsePipes,
} from '@nestjs/common';
import { TicketService} from './ticket.service';
import { CreateTicketDto} from './dto/create-ticket.dto';
import {
    ApiBearerAuth,
    ApiImplicitParam, ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {FunctionKey, ActionKey} from '@azgo-module/core/dist/common/guards/roles.guards';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { Response } from 'express';
import {UsersService} from '../users/users.service';
import {CustomValidationPipe} from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import {VehicleGroup} from '@azgo-module/core/dist/utils/enum.ultis';
import * as jwt from 'jsonwebtoken';
import {VehicleScheduleService} from '../vehicle-schedule/vehicleSchedule.service';
import {AdminCreateTicketDto} from './dto/admin-create-ticket.dto';
import {ProvidersInfoService} from '../providers-info/providers-info.service';
import {CustomChargeService} from '../custom-charge/custom-charge.service';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import * as _ from 'lodash';
import {BookBusUrlPaymentDto} from '../vn-pay/dto/book-bus-url-payment.dto';
import {BookTicketUrlPaymentDto} from '../vn-pay/dto/book-ticket-url-payment.dto';
import {CreateUrlPaymentDto} from './dto/create-url-payment.dto';

@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@ApiUseTags('Ticket')
@Controller('ticket')
export class TicketController {
    constructor(
        private ticketService: TicketService,
        private userService: UsersService,
        private vehicleScheduleService: VehicleScheduleService,
        private readonly providersInfoService: ProvidersInfoService,
        private readonly customChageService: CustomChargeService,
    ) {}

    @Post('/admin-add-ticket-for-customer')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @ApiOperation({ title: 'Admin add ticket for customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @UsePipes(CustomValidationPipe)
    async adminOrderTicket(@Body() data: AdminCreateTicketDto, @Res() res: Response) {
        try {
            const result = await this.ticketService.adminCreateTicket(data);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BUS, UserType.CUSTOMER]})
    @ApiOperation({ title: 'Add ticket' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @UsePipes(CustomValidationPipe)
    async orderTicket(@Req() request, @Body() data: CreateTicketDto, @Res() res: Response) {
        try {
            const result = await this.ticketService.orderTicketByCustomer(data.book_bus_reference);
            if (!result) return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/get-url-payment-book-ticket')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @ApiOperation({ title: 'Get url payment book ticket' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getUrlPayment(@Req() request, @Body() body: CreateUrlPaymentDto , @Res() res: Response) {
        try {
            /* Get IP address */
            const vnp_IpAddr = request.headers['x-forwarded-for'] ||
                request.connection.remoteAddress ||
                request.socket.remoteAddress ||
                request.connection.socket.remoteAddress;

            const token = request.headers.authorization.split(' ')[1];
            const customerInfo: any =  jwt.decode(token);

            const data = {...body, ...{customer_uuid: customerInfo.user_id.toString(), user_id: customerInfo.id, vnp_IpAddr}};
            const result = await this.ticketService.createUrlPayment(data);
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Paid book bus existed! ', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('get-ticket-by-schedule/:schedule_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @ApiOperation({ title: 'List ticket follow schedule' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async listTicketFollowSchedule(@Param('schedule_id') schedule_id: number, @Res() res: Response) {
        try {
            const result = await this.ticketService.listTicketFollowSchedule(schedule_id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('get-ticket-by-customer')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @ApiOperation({ title: 'Get list ticket by customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'status', required: false, description: 'Status'})
    async listTicketFollowUser(@Req() request,  @Res() res: Response, @Query('status') status: number) {
        const token = request.headers.authorization.split(' ')[1];
        // decode token
        const customerInfo =  jwt.decode(token);
        const findUserByUuid = await this.userService.findOneByUserInfo(customerInfo); // uuid

        if (findUserByUuid) {
            try {
                let result = await this.ticketService.getTicketFollowUserId(findUserByUuid.id, status );
                if (result) {
                    _.orderBy(result, ['qr_code_status'], ['asc']);
                    let dataNew = [];
                    await Promise.all(result.map( async (value) => {
                        try {
                            const driverInfo = await this.userService.getOneDriver(value.vehicleSchedule.user_id);
                            const avgProviderByAllSchedule = await this.vehicleScheduleService
                                .getAllScheduleByProvider(value.vehicleSchedule.provider_id);
                            const temp = {...value,
                                ...{rating_provider: avgProviderByAllSchedule ? Number(avgProviderByAllSchedule.avg).toFixed(1) : '0.0' },
                                ...{driverInfo}};
                            dataNew.push(temp);
                        } catch (e) {
                            throw { message: 'Vehicle schedule not found', status: HttpStatus.BAD_REQUEST};
                        }

                    }));
                    if (dataNew) {
                        return res.status(HttpStatus.OK).send(dataSuccess('oK', dataNew));
                    }
                }
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
            } catch (error) {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
            }
        }
        return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));

    }

    @Put('scan-qr-code-ticket/:qr_code')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BUS]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Scan QR Code ticket' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'qr_code', description: 'QR Code', required: true, type: 'string' })
    async scanQRCodeTicket(@Req() request, @Param('qr_code') qr_code, @Res() res: Response){
        try {
            const token = request.headers.authorization.split(' ')[1];
            const driverInfo: any =  jwt.decode(token);
            const result = await this.ticketService.scanQRCode(qr_code, driverInfo.id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('Done', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('get-ticket-detail/:id')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.DRIVER_BUS, UserType.CUSTOMER ]})
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get ticket detail' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'id', type: 'integer', description: 'Ticket ID'})
    async getTicketDetail(@Req() request,  @Res() res: Response, @Param('id') id) {
        try {
            const result = await this.ticketService.getTicketDetail(id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found ticket', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @ApiOperation({ title: 'Admin delete ticket' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'id', description: 'id ticket', required: true, type: 'number'})
    async adminDeleteTicket(@Param('id') id: number, @Res() res: Response) {
        try {
            const result = await this.ticketService.deleteOrderTicket(id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
        return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));

    }

    @Get('/count-amount-customer')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Count amount customer bus' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'from_date', description: '31/3/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'to_date', description: '31/4/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'is_in_month', description: 'Get order in month', required: false, type: Boolean})
    @ApiImplicitQuery({name: 'driver_id', description: 'Driver id', required: false, type: Number})
    @ApiImplicitQuery({name: 'provider_id', description: 'Provider id', required: false, type: Number})
    async countCustomer(@Req() request, @Res() res: Response) {
        try {
            let options = {user_type: request.user.type, ...request.query};
            if (request.user.type === 4) {
                const providerEntity = await this.providersInfoService.findByUserId(request.user.id);
                if (!providerEntity) {
                    return res.status(HttpStatus.BAD_REQUEST).send(dataError('Not found provider info', null));
                }
                options.provider_id = providerEntity.id;
            }
            const result = await this.ticketService.countAmountCustomerByBus(options);
            return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('revenue-per-vehicle-bus')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Revenue per vehicle bus' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'from_date', description: '31/3/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'to_date', description: '31/4/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'is_in_month', description: 'Get order in month', required: false, type: Boolean})
    @ApiImplicitQuery({name: 'provider_id', description: 'Provider id', required: false, type: Number})
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    async revenuePerVehicle(@Req() request, @Res() res: Response) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        try {
            results = await this.ticketService.revenuePerVehicleBus(per_page, offset, request.query);
            if (results.data) {
                await Promise.all(results.data.map(async (value) => {
                    const customCharge = await this.customChageService.getOneByVehicleGroup(VehicleGroup.BUS);
                    if (customCharge.rate_of_charge === 0) {
                        throw {message: 'rate_of_charge right thaner 0', status: HttpStatus.BAD_REQUEST};
                    }
                    value.total_price_azgo = Math.ceil((value.total_price * customCharge.rate_of_charge) / 100);
                }));
            }
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }
}
