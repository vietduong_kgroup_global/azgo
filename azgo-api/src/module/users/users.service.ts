import { forwardRef, HttpService, Inject, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { UsersEntity } from "./users.entity";
import { CreateUserDto } from "./dto/createUser.dto";
import { ValidateOTPDto } from "./dto/validateOTP.dto";
import { CreateUserMobileDto } from "./dto/createUserMobile.dto";
import { ForgotPasswordSendOTPDto } from "./dto/forgotPasswordSendOTP.dto";
import { ValidateOTPForgotPasswordDto } from "./dto/validateOTPForgotPassword.dto";
import { ChangePasswordUserDto } from "./dto/changePasswordUser.dto";
import { getManager } from "typeorm";
import { RolesService } from "../roles/roles.service";
import { AdminProfileService } from "../admin-profile/admin-profile.service";
import { DriverProfileService } from "../driver-profile/driver-profile.service";
import { UserProfileService } from "../user-profile/user-profile.service";
import { CustomerProfileService } from "../customer-profile/customer-profile.service";
import { ProvidersInfoService } from "../providers-info/providers-info.service";
import { RolesType } from "@azgo-module/core/dist/common/guards/roles.guards";
import { UserRolesEntity } from "../user-roles/user-roles.entity";
import { AdminProfileEntity } from "../admin-profile/admin-profile.entity";
import { DriverProfileEntity } from "../driver-profile/driver-profile.entity";
import { CustomerProfileEntity } from "../customer-profile/customer-profile.entity";
import { UserProfileEntity } from "../user-profile/user-profile.entity";
import { ProvidersInfoEntity } from "../providers-info/providers-info.entity";
import { validate } from "class-validator";
import {
  SocialType,
  UserStatus,
  UserType,
} from "@azgo-module/core/dist/utils/enum.ultis";
import { UsersRepository } from "./users.repository";
import { AdminProfileRepository } from "../admin-profile/admin-profile.repository";
import { DriverProfileRepository } from "../driver-profile/driver-profile.repository";
import { CustomerProfileRepository } from "../customer-profile/customer-profile.repository";
import { ProvidersInfoRepository } from "../providers-info/providers-info.repository";
import config from "../../../config/config";
import { StringHelper } from "@azgo-module/core/dist/utils/string.helper";
import { RedisUtil } from "@azgo-module/core/dist/utils/redis.util";
import { OtpService } from "@azgo-module/core/dist/common/otp/otp.service";
import { VehicleService } from "../vehicle/vehicle.service";
import { VehicleRepository } from "../vehicle/vehicle.repository";
import { VehicleDriverRepository } from "../vehicle-driver/vehicle-driver.repository";
import { RatingDriverService } from "../rating-driver/rating-driver.service";
import { PhoneNumberUtil } from "@azgo-module/core/dist/utils/phone-number.util";
import { VehicleDriverEntity } from "../vehicle-driver/vehicle-driver.entity";
import { VehicleDriverService } from "../vehicle-driver/vehicle-driver.service";
import * as moment from "moment";
import { RatingCustomerService } from "../rating-customer/rating-customer.service";
import { WalletEntity } from "../wallet/wallet.entity";
import { WalletHistoryEntity } from "../wallet-history/wallet-history.entity";
import { RedisService } from "@azgo-module/core/dist/background-utils/Redis/redis.service";
import { UploadAWSService } from "../upload-aws/upload-aws.service";
import { AccountsService } from "../accounts/accounts.service";
import { ResultDto } from "../../general-dtos/result.dto";
import { WalletService } from "../wallet/wallet.service";
import {
  ERROR_UNKNOWN,
  ERROR_USER_CAN_NOT_CREATE,
  ERROR_USER_EXITS_IN_SYSTEM,
  ERROR_USER_HAS_EXITS_EMAIL_IN_SYSTEM,
  ERROR_USER_ROLE_NOT_IN_SYSTEM,
  ERROR_USER_WAS_LOCK_IN_SYSTEM,
  ERROR_REPORT_DRIVER_WALLET,
} from "../../constants/error.const";
import { mergeArrays } from "../../helpers/utils";
@Injectable()
export class UsersService {
  private redisService;
  private redis;
  private connect: any;
  constructor(
    @InjectRepository(UsersRepository)
    private readonly usersRepository: UsersRepository,
    private readonly adminProfileRepository: AdminProfileRepository,
    private readonly driverProfileRepository: DriverProfileRepository,
    private readonly customerProfileRepository: CustomerProfileRepository,
    private readonly providersInfoRepository: ProvidersInfoRepository,
    private readonly rolesService: RolesService,
    private readonly adminProfileService: AdminProfileService,
    private readonly driverProfileService: DriverProfileService,
    private readonly customerProfileService: CustomerProfileService,
    private readonly userProfileService: UserProfileService,
    private readonly providersInfoService: ProvidersInfoService,
    private readonly uploadAWSService: UploadAWSService,
    private readonly vehicleService: VehicleService,
    private readonly otpService: OtpService,
    @InjectRepository(VehicleRepository)
    private readonly vehicleRepository: VehicleRepository,
    @InjectRepository(VehicleDriverRepository)
    private readonly vehicleDriver: VehicleDriverRepository,
    @Inject(forwardRef(() => RatingDriverService))
    private readonly ratingService: RatingDriverService,
    private readonly vehicleDriverService: VehicleDriverService,
    @Inject(forwardRef(() => RatingCustomerService))
    private readonly ratingCustomerService: RatingCustomerService,
    @Inject(forwardRef(() => AccountsService))
    private readonly accountService: AccountsService,
    @Inject(forwardRef(() => WalletService))
    private readonly walletService: WalletService
  ) {
    this.connect = {
      port: process.env.ENV_REDIS_PORT || 6379,
      host: process.env.ENV_REDIS_HOST || "127.0.0.1",
      password: process.env.ENV_REDIS_PASSWORD || null,
    };
    console.log(" UsersService Redis constructor:", this.connect);
    this.redisService = RedisService.getInstance(this.connect);
    this.redis = RedisUtil.getInstance();
  }
  async getAllUserBYlistId(user_list_id: number[]) {
    return await this.usersRepository
      .createQueryBuilder("user")
      .where("user.id in(:user_list_id)", { user_list_id })
      .getMany();
  }

  /**
   * This function get all users
   * @param options
   */
  // async findAll(options?: any) {
  //     return await this.usersRepository.getAll(options);
  // }

  /**
   * This function get user by user id
   * @param user_id
   */
  async findOne(user_id: string): Promise<UsersEntity> {
    return await this.usersRepository.findByUserId(user_id);
  }
  async findByUuid(uuid: string) {
    return await this.usersRepository.findByUuid(uuid);
  }
  async getDetailCustomer(user_uuid: string) {
    const rs = await this.usersRepository
      .createQueryBuilder("az_users")
      .leftJoinAndSelect("az_users.customerProfile", "customerProfile")
      // .andWhere('az_users.deleted_at is null')
      .andWhere("az_users.driver_deleted is null")
      .andWhere("az_users.user_id = :user_uuid", { user_uuid })
      .select([
        "az_users.user_id as customer_uuid",
        "az_users.id as id",
        "customerProfile.full_name as full_name",
        "az_users.phone as phone",
        "customerProfile.image_profile as image_profile",
      ])
      .getRawOne();
    const sumRatingCustomer = await this.ratingCustomerService.avgRatingCustomer(
      rs.id
    );
    return { ...rs, ...{ avg_rating: sumRatingCustomer } };
  }
  /**
   * This function get user by user id
   * @param user_id
   */
  async findOneByUserInfo(userInfo): Promise<UsersEntity> {
    return await this.usersRepository.findByUserId(userInfo.user_id);
  }

  /**
   * This is function get user by id
   * @param id
   */
  async findById(id: number): Promise<UsersEntity> {
    return await this.usersRepository.findById(id);
  }

  /**
   * This function get user by id and options
   * @param user_id
   * @param options
   */
  async findByIdOptions(user_id: number, options?: any): Promise<UsersEntity> {
    return await this.usersRepository.findByIdOptions(user_id, options);
  }

  /**
   * This function check exist user by email or phone number
   * @param email
   * @param phone
   */
  async isExistUser(email: string, phone: string): Promise<boolean> {
    return await this.usersRepository.isExistUser(email, phone);
  }

  /**
   * This is function get user by social id
   * @param id
   * @param type
   */
  async findUserBySocialId(id: string, type): Promise<UsersEntity> {
    if (type === SocialType.FB) {
      return await this.usersRepository.findByFacebookId(id);
    }
    return await this.usersRepository.findByGoogleId(id);
  }

  /**
   * This is function get user social by access token
   * @param access_token
   * @param social_type
   */
  async findUserByAccessToken(
    access_token: string,
    social_type: number
  ): Promise<any> {
    const httpService = new HttpService();
    let data;
    if (![SocialType.FB, SocialType.GG].includes(social_type))
      throw { message: "Trường social_type sai", status: 400 };
    try {
      if (social_type === SocialType.FB) {
        data = await httpService
          .get(config.apiCheckTokenFacebook + access_token)
          .toPromise();
      } else {
        data = await httpService
          .get(config.apiCheckTokenGoogle + access_token)
          .toPromise();
      }
    } catch (e) {
      throw {
        message: "Access token invalid",
        status: 400,
      };
    }
    return data;
  }

  /**
   * This function create user (DRIVER, PROVIDER)
   * @param body
   * @param user_id
   */
  async create(body: any, user_id?: string): Promise<ResultDto<UsersEntity>> {
    try {
      let user_action,
        role_of_user,
        isExistUser,
        vehicle,
        data_profile,
        profile_customer;
      isExistUser = await this.usersRepository.findOneByPhone(body.phone);
      let bodyRoles = body.roles;
      // merge data user
      if (!Array.isArray(body.roles)) body.roles = JSON.parse(body.roles);
      let user = await this.usersRepository.merge(new UsersEntity(), body);
      if (isExistUser) {
        if (!body.email) isExistUser.email = body.email;
        user = isExistUser;
      }

      if (isExistUser) {
        // tài khoản có trong db
        let roles = [];
        roles = isExistUser.roles;
        roles.concat(body.roles);
        let unique = roles.filter(function (item, index) {
          return roles.indexOf(item) === index;
        });
        user.roles = unique;
        if (body.roles.includes(UserType.ADMIN) || body.roles.includes(UserType.REGION_MANAGER)) {
          let userByEmail = await this.usersRepository.findUserByEmail(
            body.email
          );
          //If there is another account has same email
          if (userByEmail && userByEmail.user_id != isExistUser.user_id) {
            let result = new ResultDto<UsersEntity>();
            result.error = ERROR_USER_HAS_EXITS_EMAIL_IN_SYSTEM;
            result.status = false;
            result.message = "Tài khoản đã trùng email trong hệ thống";
            return result;
          }
          if (!!isExistUser.admin_deleted) {
            user = await this.usersRepository.merge(isExistUser, body);
            user.admin_deleted = null;
            user.customer_deleted = null;
          } else {
            //Notice tai khoan da toi tai
            if (!isExistUser.email) {
              user.email = body.email; //it's username for admin
            }
          }
        } else if (body.roles.includes(UserType.CUSTOMER)) {
          if (!!isExistUser.customer_deleted) {
            let result = new ResultDto<UsersEntity>();
            result.error = ERROR_USER_WAS_LOCK_IN_SYSTEM;
            result.status = false;
            result.message =
              "Tài khoản đã bị khóa, vui lòng liên hệ Azgo để mở lại!";
            return result;
          } else {
            if (isExistUser.roles.includes(UserType.CUSTOMER)) {
              let result = new ResultDto<UsersEntity>();
              result.error = ERROR_USER_EXITS_IN_SYSTEM;
              result.status = false;
              result.message = "Tài khoản đã tồn tại trong hệ thống";
              return result;
            }
          }
        } else if (
          body.roles.includes(UserType.DRIVER_BIKE_CAR) ||
          body.roles.includes(UserType.DRIVER_TOUR) ||
          body.roles.includes(UserType.DRIVER_BUS) ||
          body.roles.includes(UserType.DRIVER_VAN_BAGAC)
        ) {
          if (!!isExistUser.driver_deleted) {
            user = await this.usersRepository.merge(isExistUser, body);
            user.driver_deleted = null;
            user.customer_deleted = null;
          } else {
            if (
              isExistUser.roles.includes(UserType.DRIVER_BIKE_CAR) ||
              body.roles.includes(UserType.DRIVER_TOUR) ||
              body.roles.includes(UserType.DRIVER_BUS) ||
              body.roles.includes(UserType.DRIVER_VAN_BAGAC)
            ) {
              let result = new ResultDto<UsersEntity>();
              result.error = ERROR_USER_EXITS_IN_SYSTEM;
              result.status = false;
              result.message = "Tài khoản đã tồn tại trong hệ thống";
              return result;
            }
          }
        } else if (body.roles.includes(UserType.PROVIDER)) {
          if (!!isExistUser.provider_deleted) {
            user = await this.usersRepository.merge(isExistUser, body);
            user.provider_deleted = null;
            user.customer_deleted = null;
          } else {
            if (isExistUser.roles.includes(UserType.PROVIDER)) {
              let result = new ResultDto<UsersEntity>();
              result.error = ERROR_USER_EXITS_IN_SYSTEM;
              result.status = false;
              result.message = "Tài khoản đã tồn tại trong hệ thống";
              return result;
            }
          }
        } else {
          //Khong tim thay loai tai san nay
          let result = new ResultDto<UsersEntity>();
          result.error = ERROR_USER_ROLE_NOT_IN_SYSTEM;
          result.status = false;
          result.message = "Không tìm thấy loại role này trong hệ thống";
          return result;
        }
      } else {
        let roles = [];
        body.roles.forEach((value) => {
          if (UserType.hasOwnProperty(value)) roles.push(value);
        });
        if (body.roles.includes(UserType.ADMIN) || body.roles.includes(UserType.REGION_MANAGER)) {
          let userByEmail = await this.usersRepository.findUserByEmail(
            body.email
          );
          //If there is another account has same email
          if (userByEmail) {
            let result = new ResultDto<UsersEntity>();
            result.error = ERROR_USER_HAS_EXITS_EMAIL_IN_SYSTEM;
            result.status = false;
            result.message = "Tài khoản đã trùng email trong hệ thống";
            return result;
          }
          user.roles = roles.includes(UserType.CUSTOMER)
            ? roles
            : roles.concat([UserType.CUSTOMER]);
          user.driver_deleted = new Date();
          user.provider_deleted = new Date();
        } else if (body.roles.includes(UserType.CUSTOMER)) {
          user.admin_deleted = new Date();
          user.driver_deleted = new Date();
          user.provider_deleted = new Date();
        } else if (
          body.roles.includes(UserType.DRIVER_BIKE_CAR) ||
          body.roles.includes(UserType.DRIVER_TOUR) ||
          body.roles.includes(UserType.DRIVER_BUS) ||
          body.roles.includes(UserType.DRIVER_VAN_BAGAC)
        ) {
          user.roles = roles.includes(UserType.CUSTOMER)
            ? roles
            : roles.concat([UserType.CUSTOMER]);
          user.admin_deleted = new Date();
          user.provider_deleted = new Date();
        } else if (body.roles.includes(UserType.PROVIDER)) {
          user.roles = roles.includes(UserType.CUSTOMER)
            ? roles
            : roles.concat([UserType.CUSTOMER]);
          user.admin_deleted = new Date();
          user.driver_deleted = new Date();
        } else {
          //Khong tim thay loai tai khoan
          let result = new ResultDto<UsersEntity>();
          result.error = ERROR_USER_ROLE_NOT_IN_SYSTEM;
          result.status = false;
          result.message = "Không tìm thấy loại role này trong hệ thống";
          return result;
        }
      }

      if (user_id) {
        user_action = await this.findOne(user_id);
      }
      // get role of user action
      if (user_action) {
        role_of_user = await this.rolesService.getRoleByUserId(user_action.id);
      }

      if (!role_of_user || role_of_user.id !== RolesType.Master_Admin)
        user.status = UserStatus.INACTIVE; // define enum status
      // Use transaction for data synchronization
      let result;
      await getManager().transaction(async (transactionalEntityManager) => {
        // create user
        if (!isExistUser) {
          user.hashPassword();
        } else {
          delete user.password;
        }
        user = await transactionalEntityManager.save(user);
        let birthday_fortmat;
        if (body.birthday) {
          // covert date
          const from_date = body.birthday.split("/");
          // month is 0-based, that's why we need dataParts[1] - 1
          const dateObject = new Date(
            +from_date[2],
            +from_date[1] - 1,
            +from_date[0]
          );
          const formatDate = moment(dateObject, "YYYY-MM-DD");
          birthday_fortmat = formatDate.format("YYYY-MM-DD").toString();
        }
        profile_customer = await this.customerProfileService.findByUserId(
          user.id
        );
        if (!profile_customer) {
          data_profile = {
            users: user,
            first_name: body.first_name,
            last_name: body.last_name,
            full_name: body.last_name + " " + body.first_name,
            gender: body.gender,
            birthday: birthday_fortmat,
            cmnd: body.cmnd,
            address: body.address,
            image_profile: body.image_profile,
            front_of_cmnd: body.front_of_cmnd,
            back_of_cmnd: body.back_of_cmnd,
          };
          data_profile = await this.customerProfileRepository.merge(
            new CustomerProfileEntity(),
            data_profile
          );
          await transactionalEntityManager.save(
            CustomerProfileEntity,
            data_profile
          );
          data_profile.users.password = null;
          user.customerProfile = data_profile;
        }
        if (body.roles.includes(UserType.ADMIN) || body.roles.includes(UserType.REGION_MANAGER)) {
          // create data profile
          data_profile = {
            users: user,
            first_name: body.first_name,
            last_name: body.last_name,
            full_name: body.last_name + " " + body.first_name,
            image_profile: body.image_profile,
            front_of_cmnd: body.front_of_cmnd,
            back_of_cmnd: body.back_of_cmnd,
            gender: body.gender,
            company: body.company,
          };
          let adminProfile = await this.adminProfileService.findByUserId(
            user.id
          );
          if (!adminProfile) {
            data_profile = await this.adminProfileRepository.merge(
              new AdminProfileEntity(),
              data_profile
            );
            await transactionalEntityManager.save(
              AdminProfileEntity,
              data_profile
            );
            data_profile.users.password = null;
            user.adminProfile = data_profile;
          } else {
            result = new ResultDto<UsersEntity>();
            result.error = ERROR_USER_EXITS_IN_SYSTEM;
            result.status = false;
            result.message = "Tài khoản đã tồn tại trong hệ thống";
          }
        } else if (body.roles.includes(UserType.CUSTOMER)) {
          if (profile_customer) {
            result = new ResultDto<UsersEntity>();
            result.error = ERROR_USER_EXITS_IN_SYSTEM;
            result.status = false;
            result.message = "Tài khoản đã tồn tại trong hệ thống";
          }
        } else if (
          body.roles.includes(UserType.DRIVER_BIKE_CAR) ||
          body.roles.includes(UserType.DRIVER_TOUR) ||
          body.roles.includes(UserType.DRIVER_BUS) ||
          body.roles.includes(UserType.DRIVER_VAN_BAGAC)
        ) {
          // create data profile
          data_profile = {
            users: user,
            first_name: body.first_name,
            last_name: body.last_name,
            full_name: body.last_name + " " + body.first_name,
            gender: body.gender,
            birthday: birthday_fortmat,
            cmnd: body.cmnd,
            address: body.address,
            image_profile: body.image_profile,
            front_of_cmnd: body.front_of_cmnd,
            back_of_cmnd: body.back_of_cmnd,
            front_driving_license: body.front_driving_license,
            back_driving_license: body.back_driving_license,
            provider_id: body.provider_id,
            vehicle,
          };
          let driverProfile = await this.driverProfileService.findByUserId(
            user.id
          );
          if (!driverProfile) {
            data_profile = await this.driverProfileRepository.merge(
              new DriverProfileEntity(),
              data_profile
            );
            await transactionalEntityManager.save(
              DriverProfileEntity,
              data_profile
            );
            data_profile.users.password = null;
            user.driverProfile = data_profile;
          } else {
            result = new ResultDto<UsersEntity>();
            result.error = ERROR_USER_EXITS_IN_SYSTEM;
            result.status = false;
            result.message = "Tài khoản đã tồn tại trong hệ thống";
          }
        } else if (body.roles.includes(UserType.PROVIDER)) {
          // create data profile
          data_profile = {
            users: user,
            address: body.address,
            provider_name: body.provider_name,
            representative: body.representative,
            phone_representative: body.phone_representative,
            image_profile: body.image_profile,
          };
          let providerProfile = await this.providersInfoService.findByUserId(
            user.id
          );
          if (!providerProfile) {
            await transactionalEntityManager.save(
              ProvidersInfoEntity,
              data_profile
            );
          } else {
            result = new ResultDto<UsersEntity>();
            result.error = ERROR_USER_EXITS_IN_SYSTEM;
            result.status = false;
            result.message = "Tài khoản đã tồn tại trong hệ thống";
          }
        } else {
          //Khong tim thay loai tai khoan
          result = new ResultDto<UsersEntity>();
          result.error = ERROR_USER_ROLE_NOT_IN_SYSTEM;
          result.status = false;
          result.message = "Không tìm thấy loại role này trong hệ thống";
        }
      });
      //Check wallet and create if there is no wallet
      if (
        body.roles.includes(UserType.DRIVER_BIKE_CAR) ||
        body.roles.includes(UserType.DRIVER_TOUR) ||
        body.roles.includes(UserType.DRIVER_BUS) ||
        body.roles.includes(UserType.DRIVER_VAN_BAGAC)
      ) {
        let findWalletByDriver = await this.walletService.createNewWalletAccount(
          user.id
        );
      }
      user.password = null;
      if (!result) {
        result = new ResultDto<UsersEntity>();
        result.status = true;
        result.message = "Tài khoản đã tạo được thành công";
        result.data = user;
      }
      return result;
    } catch (error) {
      let result = new ResultDto<UsersEntity>();
      result.error = ERROR_UNKNOWN;
      result.status = false;
      result.message = "Error in create user service ";
      return result;
    }
  }

  /**
   * This function create user (CUSTOMER, DRIVER, PROVIDER)
   * @param body
   * @param user_id
   */
  async createUser(
    body: any,
    user_id?: string
  ): Promise<ResultDto<UsersEntity>> {
    try {
      let user_action,
        role_of_user,
        isExistUser,
        vehicle,
        data_profile,
        profile_customer,
        checkUpdateAccount = false;
      isExistUser = await this.usersRepository.findOneByPhone(body.phone);
      // merge data user
      if (!Array.isArray(body.roles)) body.roles = JSON.parse(body.roles);
      let user = await this.usersRepository.merge(new UsersEntity(), body);
      // if (isExistUser) {
      //     if(!body.email) isExistUser.email = body.email;
      //     // user = isExistUser;
      // }

      if (isExistUser) {
        // tài khoản có trong db
        let roles = [];
        roles = isExistUser.roles;
        roles = roles.concat(body.roles);
        let unique = roles.filter(function (item, index) {
          return roles.indexOf(item) === index;
        });
        user = await this.usersRepository.merge(isExistUser, body);
        user.roles = unique.includes(UserType.CUSTOMER)
          ? unique
          : unique.concat([UserType.CUSTOMER]);
        // user.roles = unique.includes(UserType.CUSTOMER) ?  unique  : unique.concat([UserType.CUSTOMER]);
        // if(body.roles.includes(UserType.ADMIN)){
        //     let userByEmail=  await this.usersRepository.findUserByEmail(body.email);
        //     //If there is another account has same email
        //     if(userByEmail && userByEmail.user_id != isExistUser.user_id){
        //         let result = new ResultDto<UsersEntity>();
        //         result.error = ERROR_USER_HAS_EXITS_EMAIL_IN_SYSTEM;
        //         result.status = false;
        //         result.message = "Tài khoản đã trùng email trong hệ thống";
        //         return result;
        //     }
        //     if (isExistUser.admin_deleted) {
        //         user.admin_deleted = null;
        //         user.customer_deleted = null;
        //     }
        //     else {
        //         //Notice tai khoan da toi tai
        //         if(!isExistUser.email){
        //             user.email=body.email; //it's username for admin
        //         }
        //     }
        // }else
        if (body.roles.includes(UserType.CUSTOMER)) {
          if (isExistUser.customer_deleted) {
            let result = new ResultDto<UsersEntity>();
            result.error = ERROR_USER_WAS_LOCK_IN_SYSTEM;
            result.status = false;
            result.message =
              "Tài khoản đã bị khóa, vui lòng liên hệ Azgo để mở lại!";
            return result;
          } else {
            if (isExistUser.roles.includes(UserType.CUSTOMER)) {
              let result = new ResultDto<UsersEntity>();
              result.error = ERROR_USER_EXITS_IN_SYSTEM;
              result.status = false;
              result.message = "Tài khoản đã tồn tại trong hệ thống";
              return result;
            }
          }
        } else if (
          body.roles.includes(UserType.DRIVER_BIKE_CAR) ||
          body.roles.includes(UserType.DRIVER_TOUR) ||
          body.roles.includes(UserType.DRIVER_BUS) ||
          body.roles.includes(UserType.DRIVER_VAN_BAGAC)
        ) {
          if (isExistUser.driver_deleted) {
            user.driver_deleted = null;
            user.customer_deleted = null;
          } else {
            let result = new ResultDto<UsersEntity>();
            result.error = ERROR_USER_EXITS_IN_SYSTEM;
            result.status = false;
            result.message = "Tài khoản đã tồn tại trong hệ thống";
            return result;
          }
        } else if (body.roles.includes(UserType.PROVIDER)) {
          if (isExistUser.provider_deleted) {
            user.provider_deleted = null;
            user.customer_deleted = null;
          } else {
            let result = new ResultDto<UsersEntity>();
            result.error = ERROR_USER_EXITS_IN_SYSTEM;
            result.status = false;
            result.message = "Tài khoản đã tồn tại trong hệ thống";
            return result;
          }
        } else {
          //Khong tim thay loai tai khoan nay
          let result = new ResultDto<UsersEntity>();
          result.error = ERROR_USER_ROLE_NOT_IN_SYSTEM;
          result.status = false;
          result.message = "Không tìm thấy loại role này trong hệ thống";
          return result;
        }
      } else {
        let roles = [];
        body.roles.forEach((value) => {
          if (UserType.hasOwnProperty(value)) roles.push(value);
        });
        if (body.roles.includes(UserType.CUSTOMER)) {
          user.admin_deleted = new Date();
          user.driver_deleted = new Date();
          user.provider_deleted = new Date();
          delete user.email;
        } else if (
          body.roles.includes(UserType.DRIVER_BIKE_CAR) ||
          body.roles.includes(UserType.DRIVER_TOUR) ||
          body.roles.includes(UserType.DRIVER_BUS) ||
          body.roles.includes(UserType.DRIVER_VAN_BAGAC)
        ) {
          user.roles = roles.includes(UserType.CUSTOMER)
            ? roles
            : roles.concat([UserType.CUSTOMER]);
          user.admin_deleted = new Date();
          user.provider_deleted = new Date();
        } else if (body.roles.includes(UserType.PROVIDER)) {
          user.roles = roles.includes(UserType.CUSTOMER)
            ? roles
            : roles.concat([UserType.CUSTOMER]);
          user.admin_deleted = new Date();
          user.driver_deleted = new Date();
          delete user.email;
        } else {
          //Khong tim thay loai tai khoan
          let result = new ResultDto<UsersEntity>();
          result.error = ERROR_USER_ROLE_NOT_IN_SYSTEM;
          result.status = false;
          result.message = "Không tìm thấy loại role này trong hệ thống";
          return result;
        }
      }

      if (user_id) {
        user_action = await this.findOne(user_id);
      }
      // get role of user action
      if (user_action) {
        role_of_user = await this.rolesService.getRoleByUserId(user_action.id);
      }

      if (!role_of_user || role_of_user.id !== RolesType.Master_Admin) {
        user.status = UserStatus.INACTIVE; // define enum status
      }
      // Use transaction for data synchronization
      let result;
      await getManager().transaction(async (transactionalEntityManager) => {
        // create user
        if (!isExistUser) {
          user.hashPassword();
        } else {
          delete user.password;
        }
        user = await transactionalEntityManager.save(user);
        let birthday_fortmat;
        if (body.birthday) {
          // covert date
          const from_date = body.birthday.split("/");
          // month is 0-based, that's why we need dataParts[1] - 1
          const dateObject = new Date(
            +from_date[2],
            +from_date[1] - 1,
            +from_date[0]
          );
          const formatDate = moment(dateObject, "YYYY-MM-DD");
          birthday_fortmat = formatDate.format("YYYY-MM-DD").toString();
        }
        profile_customer = await this.customerProfileService.findByUserId(
          user.id
        );
        if (!profile_customer) {
          data_profile = {
            users: user,
            first_name: body.first_name,
            last_name: body.last_name,
            full_name: body.last_name + " " + body.first_name,
            gender: body.gender,
            birthday: birthday_fortmat,
            cmnd: body.cmnd,
            address: body.address,
            email: body.email,
            image_profile: body.image_profile,
            front_of_cmnd: body.front_of_cmnd,
            back_of_cmnd: body.back_of_cmnd,
          };
          data_profile = await this.customerProfileRepository.merge(
            new CustomerProfileEntity(),
            data_profile
          );
          await transactionalEntityManager.save(
            CustomerProfileEntity,
            data_profile
          );
          // data_profile.users.password = null;
          user.customerProfile = data_profile;
        } else {
          data_profile = await this.customerProfileRepository.merge(
            new CustomerProfileEntity(),
            profile_customer,
            data_profile
          );
          await transactionalEntityManager.save(
            CustomerProfileEntity,
            data_profile
          );
          // data_profile.users.password = null;
          user.customerProfile = data_profile;
        }

        if (
          body.roles.includes(UserType.DRIVER_BIKE_CAR) ||
          body.roles.includes(UserType.DRIVER_TOUR) ||
          body.roles.includes(UserType.DRIVER_BUS) ||
          body.roles.includes(UserType.DRIVER_VAN_BAGAC)
        ) {
          // create data profile
          data_profile = {
            users: user,
            first_name: body.first_name,
            last_name: body.last_name,
            full_name: body.last_name + " " + body.first_name,
            gender: body.gender,
            birthday: birthday_fortmat,
            cmnd: body.cmnd,
            address: body.address,
            email: body.email,
            image_profile: body.image_profile,
            front_of_cmnd: body.front_of_cmnd,
            back_of_cmnd: body.back_of_cmnd,
            front_driving_license: body.front_driving_license,
            back_driving_license: body.back_driving_license,
            provider_id: body.provider_id,
            vehicle,
          };
          let driverProfile = await this.driverProfileService.findByUserId(
            user.id
          );
          if (!driverProfile) {
            data_profile = await this.driverProfileRepository.merge(
              new DriverProfileEntity(),
              data_profile
            );
            await transactionalEntityManager.save(
              DriverProfileEntity,
              data_profile
            );
            data_profile.users.password = null;
            user.driverProfile = data_profile;
          } else {
            data_profile = await this.driverProfileRepository.merge(
              new DriverProfileEntity(),
              driverProfile,
              data_profile
            );
            await transactionalEntityManager.save(
              DriverProfileEntity,
              data_profile
            );
            data_profile.users.password = null;
            user.driverProfile = data_profile;
          }
        } else if (body.roles.includes(UserType.PROVIDER)) {
          // create data profile
          data_profile = {
            users: user,
            address: body.address,
            provider_name: body.provider_name,
            representative: body.representative,
            phone_representative: body.phone_representative,
            image_profile: body.image_profile,
          };
          let providerProfile = await this.providersInfoService.findByUserId(
            user.id
          );
          if (!providerProfile) {
            await transactionalEntityManager.save(
              ProvidersInfoEntity,
              data_profile
            );
          } else {
            await transactionalEntityManager.save(
              ProvidersInfoEntity,
              providerProfile,
              data_profile
            );
          }
        } else {
          //Khong tim thay loai tai khoan
          result = new ResultDto<UsersEntity>();
          result.error = ERROR_USER_ROLE_NOT_IN_SYSTEM;
          result.status = false;
          result.message = "Không tìm thấy loại role này trong hệ thống";
        }
      });
      if (body.hasOwnProperty("account_bank_code")) checkUpdateAccount = true; // if body have account_bank_code (admin), backend will update account of 
      // update account
      if (checkUpdateAccount) {
        let updateAccount = await this.accountService.createAccount(
          body,
          user.user_id,
          user
        );
      }
      //Check wallet and create if there is no wallet
      if (
        body.roles.includes(UserType.DRIVER_BIKE_CAR) ||
        body.roles.includes(UserType.DRIVER_TOUR) ||
        body.roles.includes(UserType.DRIVER_BUS) ||
        body.roles.includes(UserType.DRIVER_VAN_BAGAC)
      ) {
        await this.walletService.createNewWalletAccount(user.id);
      }
      user.password = null;
      if (!result) {
        result = new ResultDto<UsersEntity>();
        result.status = true;
        result.message = "Tài khoản đã tạo được thành công";
        result.data = user;
      }
      return result;
    } catch (error) {
      console.log("error: ", error);
      let result = new ResultDto<UsersEntity>();
      result.error = ERROR_UNKNOWN;
      result.status = false;
      result.message = "Error in create user service ";
      return result;
    }
  }

  /**
   * This is function create user with mobile (CUSTOMER)
   * @param body
   */
  async createUserMobile(body: CreateUserMobileDto): Promise<UsersEntity> {
    let isExistUser, key, getCacheTokenRegister, profile_customer;

    // merge data user
    let user = await this.usersRepository.merge(new UsersEntity(), body);

    // redis
    key = "user_" + body.phone;
    // find token redis
    getCacheTokenRegister = await this.redis.getInfoRedis(key, true);
    if (!getCacheTokenRegister.isValidate)
      throw { message: "Token không tồn tại!", status: 404 };
    // check token
    if (getCacheTokenRegister.value !== body.token)
      throw { message: "Token không họp lệ!", status: 400 };
    // check phone
    isExistUser = await this.findOneByPhone(body.phone);
    // if (isExistUser) throw { message: 'Số điện thoại đã được người khác đăng ký!', status: 409 };

    if (body.fb_id) {
      isExistUser = await this.usersRepository.findByFacebookId(body.fb_id);
      // if (isExistUser) throw { message: 'Tài khoản đã tồn tại!', status: 409 };
    }

    if (body.gg_id) {
      isExistUser = await this.usersRepository.findByGoogleId(body.gg_id);
      // if (isExistUser) throw { message: 'Tài khoản đã tồn tại!', status: 409 };
    }

    if (isExistUser) {
      // tài khoản có trong db
      user.roles = isExistUser.roles.concat(body.roles);
      switch (user.type) {
        case UserType.DRIVER_BIKE_CAR:
        case UserType.DRIVER_BUS:
        case UserType.DRIVER_VAN_BAGAC:
        case UserType.DRIVER_TOUR:
          if (isExistUser.driver_deleted) {
            user = await this.usersRepository.merge(isExistUser, body);
            user.driver_deleted = null;
            user.customer_deleted = null;
          } else {
            throw {
              message: "Tài khoản đã tồn tại trong hệ thống!",
              status: 409,
            };
          }
          break;
        case UserType.CUSTOMER:
          if (isExistUser.customer_deleted) {
            throw {
              message: "Tài khoản đã bị khóa, vui lòng liên hệ Azgo để mở lại!",
              status: 409,
            };
          } else {
            throw {
              message: "Tài khoản đã tồn tại trong hệ thống!",
              status: 409,
            };
          }
          break;
        default:
          throw { message: "Không tìm thấy loại tài khoản!", status: 400 };
      }
    } else {
      switch (user.type) {
        case UserType.DRIVER_BIKE_CAR:
        case UserType.DRIVER_BUS:
        case UserType.DRIVER_VAN_BAGAC:
        case UserType.DRIVER_TOUR:
          user.admin_deleted = new Date();
          user.provider_deleted = new Date();
          break;
        case UserType.CUSTOMER:
          user.admin_deleted = new Date();
          user.driver_deleted = new Date();
          user.provider_deleted = new Date();
          break;
        default:
          throw { message: "Không tìm thấy loại tài khoản!", status: 400 };
      }
    }

    // get role by role_id
    let role = await this.rolesService.findOne(body.role);
    if (!role) throw { message: "Vai trò không tìm thấy!", status: 404 };

    // Use transaction for data synchronization
    await getManager().transaction(async (transactionalEntityManager) => {
      // create user
      user.hashPassword();
      user = await transactionalEntityManager.save(user);
      // update user_role
      let user_role_info = {
        permission: role.permission,
        users: user,
        roles: role,
      };
      await transactionalEntityManager.save(UserRolesEntity, user_role_info);
      // create data profile
      let data_profile = {
        users: user,
      };
      // check user type
      switch (user.type) {
        case UserType.DRIVER_BIKE_CAR:
        case UserType.DRIVER_BUS:
        case UserType.DRIVER_VAN_BAGAC:
        case UserType.DRIVER_TOUR:
          // save driver profile
          profile_customer = await this.customerProfileRepository.merge(
            new CustomerProfileEntity(),
            data_profile
          );
          if (isExistUser) {
            let profile = await this.driverProfileService.findByUserId(user.id);
            if (profile)
              data_profile = await this.driverProfileRepository.merge(
                profile,
                data_profile
              );
            // tìm profile customer
            profile_customer = await this.customerProfileService.findByUserId(
              user.id
            );
            if (!profile_customer) {
              profile_customer = await this.customerProfileRepository.merge(
                profile_customer,
                data_profile
              );
            }
          }
          await transactionalEntityManager.save(
            CustomerProfileEntity,
            profile_customer
          );
          await transactionalEntityManager.save(
            DriverProfileEntity,
            data_profile
          );
          break;
        case UserType.CUSTOMER:
          // save customer profile
          if (isExistUser) {
            let profile = await this.customerProfileService.findByUserId(
              user.id
            );
            if (profile)
              data_profile = await this.customerProfileRepository.merge(
                profile,
                data_profile
              );
          }
          await transactionalEntityManager.save(
            CustomerProfileEntity,
            data_profile
          );
          break;
        default:
          throw { message: "Không tìm thấy loại tài khoản!", status: 400 };
      }
    });

    await this.redis.delRedis(key);

    return user;
  }

  /**
   * This function create user (ADMIN)
   * @param body
   * @param user_id
   */
  async createAdmin(
    body: any,
    user_id?: string
  ): Promise<ResultDto<UsersEntity>> {
    try {
      let user_action, role_of_user, isExistUser, data_profile;
      isExistUser = await this.usersRepository.findUserByEmail(body.email);
      // merge data user
      if (!Array.isArray(body.roles)) body.roles = JSON.parse(body.roles);
      let user = await this.usersRepository.merge(new UsersEntity(), body);
      if (isExistUser) {
        // tài khoản có trong db
        user = await this.usersRepository.merge(
          new UsersEntity(),
          isExistUser,
          body
        );
        if (body.roles.includes(UserType.ADMIN)) {
          if (!isExistUser.admin_deleted) {
            // Chưa bị delete role admin
            let result = new ResultDto<UsersEntity>();
            result.error = ERROR_USER_HAS_EXITS_EMAIL_IN_SYSTEM;
            result.status = false;
            result.message = "Tài khoản đã trùng email trong hệ thống";
            return result;
          } else {
            user.admin_deleted = null;
          }
        } else {
          //Khong tim thay loai tai san nay
          let result = new ResultDto<UsersEntity>();
          result.error = ERROR_USER_ROLE_NOT_IN_SYSTEM;
          result.status = false;
          result.message = "Không tìm thấy loại role này trong hệ thống";
          return result;
        }
      } else {
        if (body.roles.includes(UserType.ADMIN)) {
          user.driver_deleted = new Date();
          user.customer_deleted = new Date();
          user.provider_deleted = new Date();
        } else {
          //Khong tim thay loai tai khoan
          let result = new ResultDto<UsersEntity>();
          result.error = ERROR_USER_ROLE_NOT_IN_SYSTEM;
          result.status = false;
          result.message = "Không tìm thấy loại role này trong hệ thống";
          return result;
        }
      }

      if (user_id) {
        user_action = await this.findOne(user_id);
      }
      // get role of user action
      if (user_action) {
        role_of_user = await this.rolesService.getRoleByUserId(user_action.id);
      }

      if (!role_of_user || role_of_user.id !== RolesType.Master_Admin)
        user.status = UserStatus.INACTIVE; // define enum status
      // Use transaction for data synchronization
      let result;
      await getManager().transaction(async (transactionalEntityManager) => {
        // create user
        if (!isExistUser) {
          user.hashPassword();
        } else {
          delete user.password;
        }
        delete user.phone;
        user = await transactionalEntityManager.save(user);
        let birthday_fortmat;
        if (body.birthday) {
          // covert date
          const from_date = body.birthday.split("/");
          // month is 0-based, that's why we need dataParts[1] - 1
          const dateObject = new Date(
            +from_date[2],
            +from_date[1] - 1,
            +from_date[0]
          );
          const formatDate = moment(dateObject, "YYYY-MM-DD");
          birthday_fortmat = formatDate.format("YYYY-MM-DD").toString();
        }
        if (body.roles.includes(UserType.ADMIN)) {
          // create data profile
          data_profile = {
            users: user,
            first_name: body.first_name,
            last_name: body.last_name,
            full_name: body.last_name + " " + body.first_name,
            image_profile: body.image_profile,
            front_of_cmnd: body.front_of_cmnd,
            back_of_cmnd: body.back_of_cmnd,
            gender: body.gender,
            company: body.company,
            phone: body.phone,
            area_uuid: body.area_uuid,
          };
          let adminProfile = await this.adminProfileService.findByUserId(
            user.id
          );
          if (!adminProfile) {
            data_profile = await this.adminProfileRepository.merge(
              new AdminProfileEntity(),
              data_profile
            );
            await transactionalEntityManager.save(
              AdminProfileEntity,
              data_profile
            );
            data_profile.users.password = null;
            user.adminProfile = data_profile;
          } else {
            data_profile = await this.adminProfileRepository.merge(
              new AdminProfileEntity(),
              adminProfile,
              data_profile
            );
            await transactionalEntityManager.save(
              AdminProfileEntity,
              data_profile
            );
            data_profile.users.password = null;
            user.adminProfile = data_profile;
          }
        } else {
          //Khong tim thay loai tai khoan
          result = new ResultDto<UsersEntity>();
          result.error = ERROR_USER_ROLE_NOT_IN_SYSTEM;
          result.status = false;
          result.message = "Không tìm thấy loại role này trong hệ thống";
        }
      });
      user.password = null;
      if (!result) {
        result = new ResultDto<UsersEntity>();
        result.status = true;
        result.message = "Tài khoản đã tạo được thành công";
        result.data = user;
      }
      return result;
    } catch (error) {
      let result = new ResultDto<UsersEntity>();
      result.error = ERROR_UNKNOWN;
      result.status = false;
      result.message = "Error in create user service ";
      return result;
    }
  }

  /**
   * This function update user
   * @param body
   * @param user
   */
  async update(body: any, user: UsersEntity): Promise<UsersEntity> {
    if (body.password !== body.confirm)
      throw { message: "Xác nhận mật khẩu không đúng!", status: 400 };
    if (body.email && body.email !== user.email) {
      const checkExistEmail = await this.usersRepository
        .createQueryBuilder("user")
        .where("email = :email", { email: body.email })
        .getOne();
      if (checkExistEmail)
        throw {
          message: "Email đã tồn tại, vui lòng nhập email khác!",
          status: 400,
        };
    }
    body.email = body.email || null;
    let profile, vehicleByDriver;
    if (body.roles && !body.roles.length)
      body.roles.push(UserType.DRIVER_BIKE_CAR); // default is bike/car driver
    user = this.usersRepository.merge(user, body);
    if (!body.password === false) {
      user.password = body.password;
      user.hashPassword();
    }
    const errors = await validate(user, { validationError: { target: false } });
    if (errors.length > 0) {
      throw { message: errors };
    }
    let checkUpdateAccount = false; // true if need update user account
    // check user type
    switch (user.type) {
      case UserType.ADMIN:
      case UserType.REGION_MANAGER:
      case UserType.OPERATOR:
      case UserType.ACCOUNTANT:
        profile = await this.adminProfileService.findByUserId(user.id);
        if (!profile)
          throw { message: "Hồ sơ admin không tìm thấy", status: 404 };
        profile.area_uuid = body.area_uuid;
        profile.phone = body.phone;
        delete user.phone;
        delete user.email; // khong duoc cap nhat email
        break;
      case UserType.DRIVER_BIKE_CAR:
      case UserType.DRIVER_BUS:
      case UserType.DRIVER_VAN_BAGAC:
      case UserType.DRIVER_TOUR:
        profile = await this.driverProfileService.findByUserId(user.id);
        if (!profile)
          throw { message: "Hồ sơ tài xế không tìm thấy", status: 404 };
        profile.area_uuid = body.area_uuid;
        profile.email = body.email;
        delete user.phone; // khong duoc cap nhat sdt
        delete user.email;
        vehicleByDriver = await this.vehicleDriverService.findByDriver(
          profile.user_id
        );
        if (body.hasOwnProperty("account_bank_code")) checkUpdateAccount = true; // if body have account_bank_code (admin), backend will update account of user
        break;
      case UserType.CUSTOMER:
        profile = await this.customerProfileService.findByUserId(user.id);
        if (!profile)
          throw { message: "Hồ sơ tài xế không tìm thấy", status: 404 };
        profile.email = body.email;
        delete user.phone; // khong duoc cap nhat sdt
        delete user.email;
        break;
      case UserType.PROVIDER:
        profile = await this.providersInfoService.findByUserId(user.id);
        if (!profile)
          throw { message: "Hồ sơ nhà xe không tìm thấy", status: 404 };
        profile.provider_name = body.provider_name;
        profile.representative = body.representative;
        profile.phone_representative = body.phone_representative;
        break;
      default:
        throw { message: "Không tìm thấy loại tài khoản!", status: 404 };
    }

    // Use transaction for data synchronization
    await getManager().transaction(async (transactionalEntityManager) => {
      let birthday_fortmat;
      if (body.birthday) {
        const from_date = body.birthday.split("/");
        // month is 0-based, that's why we need dataParts[1] - 1
        const dateObject = new Date(
          +from_date[2],
          +from_date[1] - 1,
          +from_date[0]
        );
        const formatDate = moment(dateObject, "YYYY-MM-DD");
        birthday_fortmat = formatDate.format("YYYY-MM-DD").toString();
      }
      if (
        body.image_profile &&
        profile.image_profile &&
        body.image_profile.file_name !== profile.image_profile.file_name
      ) {
        this.uploadAWSService.removeImg(profile.image_profile.file_name);
      }
      // create user
      user = await transactionalEntityManager.save(user);
      // update profile
      profile.users = user;
      profile.first_name = body.first_name;
      profile.last_name = body.last_name;
      (profile.full_name = body.last_name + " " + body.first_name),
        (profile.gender = body.gender);
      profile.birthday = birthday_fortmat;
      profile.cmnd = body.cmnd;
      profile.address = body.address;
      profile.image_profile = body.image_profile;
      profile.front_of_cmnd = body.front_of_cmnd;
      profile.back_of_cmnd = body.back_of_cmnd;
      profile.front_driving_license = body.front_driving_license;
      profile.back_driving_license = body.back_driving_license;
      profile.company = body.company;
      await transactionalEntityManager.save(profile);
      if (vehicleByDriver) {
        const vehicleDriver = new VehicleDriverEntity();
        vehicleDriver.vehicle = vehicleByDriver.vehicle;
        vehicleDriver.driverProfile = profile;
        await await transactionalEntityManager.save(vehicleDriver);
      }
    });
    let userData = await this.findOne(user.user_id);
    // update account
    if (checkUpdateAccount) {
      let updateAccount = await this.accountService.createAccount(
        body,
        userData.user_id,
        userData
      );
      // if ((updateAccount && !updateAccount.success) || !updateAccount) throw { message: 'Error in updateAccount'};
    }
    return userData;
  }

  /**
   * This function create user
   * @param body
   */
  async createAndUpdate(body: CreateUserDto): Promise<UsersEntity> {
    let user = new UsersEntity();
    user.email = body.email;
    user.phone = body.phone;
    user.country_code = body.country_code;
    user.password = body.password;
    user.hashPassword();
    return await this.usersRepository.save(user);
  }

  /**
   * This function get user by email
   * @param email
   */
  async findOneByEmail(email: string): Promise<UsersEntity> {
    return await this.usersRepository.findOne({
      where: { email },
      relations: ["user_roles", "user_roles.roles"],
    });
  }

  /**
   * This is function get user by phone
   * @param phone
   */
  async findOneByPhone(phone: string): Promise<UsersEntity> {
    return await this.usersRepository.findOne({
      where: { phone },
      relations: ["user_roles", "user_roles.roles", "fcmToken"],
    });
  }

  /**
   * This is function get user by email or phone
   * @param value
   */
  async findOneByEmailOrPhone(value: string): Promise<any> {
    try {
      const user = await this.usersRepository.findByEmailOrPhone(value);
      const driver = await this.vehicleDriver
        .createQueryBuilder("az_vehicle_driver")
        .where("az_vehicle_driver.driver_id = :driver_id", {
          driver_id: user.id,
        })
        .getRawOne();
      let vehicle = null;
      if (driver) {
        vehicle = await this.vehicleRepository
          .createQueryBuilder("az_vehicle")
          .leftJoinAndSelect("az_vehicle.vehicleBrand", "vehicleBrand")
          .leftJoinAndSelect("az_vehicle.vehicleType", "vehicleType")
          .leftJoinAndSelect("vehicleType.vehicleGroup", "vehicleGroup")
          .where("az_vehicle.id = :id", {
            id: driver.az_vehicle_driver_vehicle_id,
          })
          .getRawOne();
        if (vehicle) {
          vehicle.az_vehicle_images = JSON.parse(vehicle.az_vehicle_images);
          vehicle.az_vehicle_front_car_insurance = JSON.parse(
            vehicle.az_vehicle_front_car_insurance
          );
          vehicle.az_vehicle_back_car_insurance = JSON.parse(
            vehicle.az_vehicle_back_car_insurance
          );
          vehicle.az_vehicle_front_registration_certificates = JSON.parse(
            vehicle.az_vehicle_front_registration_certificates
          );
          vehicle.az_vehicle_back_registration_certificates = JSON.parse(
            vehicle.az_vehicle_back_registration_certificates
          );
        }
      }
      return { user, vehicle };
    } catch (e) {
      console.log(e);
      return {
        user: null,
        vehicle: null,
      };
    }
  }
  async driverWalletReport(limit: number, offset: number, options?: any) {
    let res_fee = await this.usersRepository.driverWalletReport(
      limit,
      offset,
      options,
      "fee_wallet"
    );
    let res_cast = await this.usersRepository.driverWalletReport(
      limit,
      offset,
      options,
      "cast_wallet"
    );
    if (res_fee && res_cast && res_cast.success && res_fee.success) {
      res_fee.data.forEach((element) => {
        element.walletEntity_cast_wallet_balance =
          element.walletEntity_cast_wallet_balance || 0;
        element.walletEntity_fee_wallet_balance =
          element.walletEntity_fee_wallet_balance || 0;
      });
      // merge 2 array to get 2 property : `walletEntity_fee_wallet_balance`, `walletEntity_cast_wallet_balance` into 1 array with key `az_users_id`
      let merge_first = mergeArrays(
        res_fee.data || [],
        res_cast.data || [],
        "az_users_id"
      );
      let result = new ResultDto<any>();
      result.error = null;
      result.status = true;
      result.message = null;
      result.data = {
        data: merge_first,
        total: res_fee.total,
      };
      return result;
    } else {
      let result = new ResultDto<any>();
      result.error = ERROR_REPORT_DRIVER_WALLET;
      result.status = false;
      result.message = "Lỗi BC Tiền nạp tài xế";
      result.data = null;
      return result;
    }
  }
  async exportDriverWalletReport(options?: any) {
    let res_fee = await this.usersRepository.exportDriverWalletReport(
      options,
      "fee_wallet"
    );
    let res_cast = await this.usersRepository.exportDriverWalletReport(
      options,
      "cast_wallet"
    );
    if (res_fee && res_cast && res_cast.success && res_fee.success) {
      res_fee.data.forEach((element) => {
        element.walletEntity_cast_wallet_balance =
          element.walletEntity_cast_wallet_balance || 0;
        element.walletEntity_fee_wallet_balance =
          element.walletEntity_fee_wallet_balance || 0;
      });
      let merge_first = mergeArrays(
        res_fee.data || [],
        res_cast.data || [],
        "az_users_id"
      );
      let result = new ResultDto<any>();
      result.error = null;
      result.status = true;
      result.message = null;
      result.data = merge_first;
      return result;
    } else {
      let result = new ResultDto<any>();
      result.error = ERROR_REPORT_DRIVER_WALLET;
      result.status = false;
      result.message = "Lỗi BC Tiền nạp tài xế";
      result.data = null;
      return result;
    }
  }

  /**
   * This is function get list user driver bus
   * @param limit
   * @param offset
   * @param options
   */
  async findAllDrivers(limit: number, offset: number, options?: any) {
    const arrDriverId = await this.vehicleDriverService.getAllDriverWithTotalSeat(
      options
    );
    return await this.usersRepository.getAllDrivers(
      arrDriverId,
      limit,
      offset,
      options
    );
  }

  /**
   * This is function get list user driver bus
   * @param limit
   * @param offset
   * @param options
   */
  async findAllDriverBus(limit: number, offset: number, options?: any) {
    return await this.usersRepository.getAllDriverBus(limit, offset, options);
  }

  /**
   * This is function get user by email or phone
   * @param value
   */
  async getOneDriver(id: number): Promise<any> {
    const user = await this.usersRepository.findOne({
      where: { id },
      relations: ["driverProfile", "adminProfile"],
    });
    const driver = await this.vehicleDriver
      .createQueryBuilder("az_vehicle_driver")
      .where("az_vehicle_driver.driver_id = :driver_id", { driver_id: user.id })
      .getRawOne();
    let vehicle = null;
    if (driver) {
      vehicle = await this.vehicleRepository
        .createQueryBuilder("az_vehicle")
        .leftJoinAndSelect("az_vehicle.vehicleBrand", "vehicleBrand")
        .where("az_vehicle.id = :id", {
          id: driver.az_vehicle_driver_vehicle_id,
        })
        .select([
          "az_vehicle.license_plates as license_plates",
          "vehicleBrand.name as brand_name",
        ])
        .getRawOne();
    }
    const ratingDriver = await this.ratingService.avgRatingDriver(user.id);
    return {
      ...vehicle,
      ...{ ratingDriver: Number(ratingDriver).toFixed(1) },
      ...{
        id: user.id,
        driver_uuid: user.user_id.toString(),
        full_name: user.driverProfile ? user.driverProfile.full_name : null,
        image_profile: user.driverProfile
          ? user.driverProfile.image_profile
          : null,
        phone: user.phone,
        country_code: user.country_code,
      },
    };
  }

  /**
   * This function get user by email
   * @param user
   */
  async updatePassword(user: UsersEntity): Promise<UsersEntity> {
    return await this.usersRepository.save(user);
  }

  /**
   * This function check exist user parent by user id
   * @param user_id
   * @param parent_id
   */
  async isExistParentByUserId(user_id: string, parent_id: number) {
    return await this.usersRepository.findOne({
      where: { user_id, parent_id },
    });
  }

  /**
   * This function block user account
   * @param user
   * @param block (True: block, False: unblock)
   */
  async blockUserAccount(user: UsersEntity, block: boolean): Promise<boolean> {
    let dataUpdate: any = {};
    if (block) {
      // block user
      dataUpdate.block = new Date();
      dataUpdate.status = UserStatus.INACTIVE;
    } else {
      // unblock user
      dataUpdate.block = null;
      dataUpdate.status = UserStatus.ACTIVE;
    }
    await this.usersRepository.update(user.id, dataUpdate);
    return true;
  }

  /**
   * This is function delete user roles
   * @param user
   */
  async deleteRolesUser(user: UsersEntity, user_role: Number) {
    // check user type
    switch (user_role) {
      case UserType.ADMIN:
      case UserType.REGION_MANAGER:
        user.admin_deleted = new Date();
        break;
      case UserType.DRIVER_BIKE_CAR:
      case UserType.DRIVER_BUS:
      case UserType.DRIVER_VAN_BAGAC:
      case UserType.DRIVER_TOUR:
        user.driver_deleted = new Date();
        break;
      case UserType.CUSTOMER:
        user.customer_deleted = new Date();
        break;
      case UserType.PROVIDER:
        user.provider_deleted = new Date();
        break;
      default:
        throw { message: "Không tìm thấy loại tài khoản!", status: 400 };
    }
    try {
      await this.usersRepository.save(user);
      return {
        success: true,
      };
    } catch (error) {
      return {
        success: true,
        message: error.message,
      };
    }
  }

  /**
   * This is function delete user
   * @param user
   */
  async deleteUser(user: UsersEntity): Promise<boolean> {
    user.email = `${user.email}-delete.${Math.round(new Date().getTime())
      .toString(16)
      .toLocaleUpperCase()}`;
    user.phone = `${user.phone}-delete.${Math.round(new Date().getTime())
      .toString(16)
      .toLocaleUpperCase()}`;
    if (user.type === UserType.PROVIDER) {
      await getManager().transaction(async (transactionalEntityManager) => {
        let provider_info = await this.providersInfoService.findByUserId(
          user.id
        );
        if (provider_info) {
          provider_info.deleted_at = new Date();
          await transactionalEntityManager.save(provider_info);
        }
        user.deleted_at = new Date();
        user.status = UserStatus.INACTIVE;
        await transactionalEntityManager.save(user);
      });
    } else {
      user.deleted_at = new Date();
      user.status = UserStatus.INACTIVE;
      await this.usersRepository.save(user);
    }
    return true;
  }

  /**
   * This is function send OTP sms
   * @param body
   * @param social
   */
  async sendOTP(
    body: { phone: string; SmsType?: number; Brandname?: string },
    social: boolean
  ): Promise<any> {
    let key,
      otp_token,
      otp = 1234;
    // check phone
    let isExistUser = await this.findOneByPhone(body.phone);
    if (isExistUser)
      throw {
        message: "Số điện thoại đã được người khác đăng ký!",
        status: 409,
      };
    // generate otp

    if (config.enableSMS.toUpperCase() === "TRUE") {
      otp = StringHelper.generateOtpCode();
      // Set otp sms
      const rsSendOtp = await this.otpService.sendOtp({
        Phone: body.phone,
        Content:
          otp +
          " " +
          (process.env.OTP_ESMS_TEST_REGISTER ||
            "la ma OTP dung de dang ky AZGO cua quy khach"),
        SmsType: 2,
        Brandname:  "AZGO",
      });
      console.log(rsSendOtp)
      if (Number(rsSendOtp.CodeResult).valueOf() !== 100)
        throw { status: 400, message: rsSendOtp.ErrorMessage };
    }

    // key redis
    key = "user_" + body.phone;
    // field token otp redis
    otp_token = StringHelper.generateOtpToken();
    // create redis otp
    await this.redis.setInfoRedis(key, otp_token, otp);
    /*
     * No function yet send mobile
     * */
    return otp_token;
  }

  /**
   * This is function check OTP and return token
   * @param body
   */
  async validateOTP(body: ValidateOTPDto): Promise<string> {
    let key, getToken;
    // set key redis
    key = "user_" + body.phone;
    // get token redis with OTP and Token otp
    getToken = await this.redis.getRedisOTP(key, body.token, body.otp, true);
    // check otp
    if (!getToken.isValidate) throw { message: getToken.message, status: 400 };
    // create token from redis register user
    await this.redis.setInfoRedis(key, true, getToken.value);
    return getToken.value;
  }

  /**
   * This is function forgot password with phone
   * @param body
   */
  async forgotPasswordSendOTP(body: ForgotPasswordSendOTPDto): Promise<string> {
    let key,
      otp_token,
      otp = 1234;
    // check phone
    let isExistUser = await this.findOneByPhone(body.phone);
    if (!isExistUser)
      throw { message: "Tài khoản không tìm thấy!", status: 404 };
    // generate otps
    if (config.enableSMS.toUpperCase() === "TRUE") {
      otp = StringHelper.generateOtpCode();
      // Set otp sms
      const rsSendOtp = await this.otpService.sendOtp({
        Phone: body.phone,
        Content:
          otp +
          " " +
          (process.env.OTP_ESMS_TEST_FORGET_PASS ||
            "la ma OTP dung de cap nhat lai mat khau AZGO cua quy khach"),
        SmsType: 2,
        Brandname: process.env.OTP_ESMS_BRAND_NAME || "AZGO",
      });
      if (Number(rsSendOtp.CodeResult).valueOf() !== 100)
        throw { status: 400, message: rsSendOtp.ErrorMessage };
    }
    // key redis
    key = "forgot_password_" + body.phone;
    // field token otp redis
    otp_token = StringHelper.generateOtpToken();
    // create redis otp
    await this.redis.setInfoRedis(key, otp_token, otp);
    /*
     * No function yet send mobile
     * */
    return otp_token;
  }

  /**
   * This is function validate forgot password
   * @param body
   */
  async validateForgotPassword(
    body: ValidateOTPForgotPasswordDto
  ): Promise<boolean> {
    let key, getToken, user;
    // set key redis
    key = "forgot_password_" + body.phone;
    // get token redis with OTP and Token otp
    getToken = await this.redis.getRedisOTP(key, body.token, body.otp, false);
    // check otp
    if (!getToken.isValidate) throw { message: getToken.message, status: 400 };
    // check password
    if (body.password !== body.password_confirm)
      throw { message: "Xác nhận mật khẩu không đúng", status: 400 };
    // get user by phone
    user = await this.findOneByPhone(body.phone);
    // check phone
    if (!user) throw { message: "Tài khoản không tìm thấy", status: 404 };
    // save password new
    user.password = body.password;
    user.hashPassword();
    await this.usersRepository.save(user);
    // remove redis with key
    await this.redis.delRedis(key);
    return true;
  }

  /**
   * This function get list users by limit & offset
   * @param limit
   * @param offset
   * @param options
   */
  async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
    return await this.usersRepository.getAllByOffsetLimit(
      limit,
      offset,
      options
    );
  }

  /**
   * This is function get list user customer
   * @param limit
   * @param offset
   * @param options
   */
  async findAllCustomer(limit: number, offset: number, options?: any) {
    return await this.usersRepository.getAllCustomer(limit, offset, options);
  }

  /**
   * This is function get list user driver bus
   * @param limit
   * @param offset
   * @param options
   */
  async findAllDriversAvaiable(options?: any) {
    const arrDriverId = await this.vehicleDriverService.getAllDriverWithTotalSeat(
      options
    );
    return await this.usersRepository.getAllDriversAvaiable(
      arrDriverId,
      options
    );
  }
  // async driverWalletReport(options?: any) {
  //   let res_fee = await this.usersRepository.driverWalletReport(
  //     options,
  //     "fee_wallet"
  //   );
  //   let res_cast = await this.usersRepository.driverWalletReport(
  //     options,
  //     "cast_wallet"
  //   );
  //   res_fee.forEach((element) => {
  //     element.walletEntity_cast_wallet_balance =
  //       element.walletEntity_cast_wallet_balance || 0;
  //     element.walletEntity_fee_wallet_balance =
  //       element.walletEntity_fee_wallet_balance || 0;
  //   });
  //   // merge 2 array to get 2 property : `walletEntity_fee_wallet_balance`, `walletEntity_cast_wallet_balance` into 1 array with key `az_users_id`
  //   let merge_first = mergeArrays(res_fee || [], res_cast || [], "az_users_id");
  //   return merge_first;
  // }

  /**
   * This is function get list user driver bike car
   * @param vehicle_group_id
   * @param limit
   * @param offset
   * @param options
   */
  async findAllDriverBikeCarOrVanBagac(
    vehicle_group_id: number,
    limit: number,
    offset: number,
    options?: any
  ) {
    return await this.usersRepository.getAllDriverBikeCarOrVanBagac(
      vehicle_group_id,
      limit,
      offset,
      options
    );
  }

  /**
   * This is function get list user driver bike car without vehicle
   * @param vehicle_group_id
   * @param limit
   * @param offset
   * @param options
   */
  async findAllDriverBikeCarOrVanBagacWithoutVehicle(
    vehicle_group_id: number,
    limit: number,
    offset: number,
    options?: any
  ) {
    const arrDriverId = await this.vehicleDriverService.getAllDriver();
    return await this.usersRepository.getAllDriverBikeCarOrVanBagacWithoutVehicle(
      vehicle_group_id,
      arrDriverId,
      limit,
      offset,
      options
    );
  }

  /**
   * This is function get list user admin
   * @param limit
   * @param offset
   * @param options
   */
  async findAllAdmin(limit: number, offset: number, options?: any) {
    return await this.usersRepository.getAllAdmin(limit, offset, options);
  }

  /**
   * This is function get list user provider
   * @param limit
   * @param offset
   * @param options
   */
  async findAllProvider(limit: number, offset: number, options?: any) {
    return await this.usersRepository.getAllProvider(limit, offset, options);
  }

  /**
   * This is function change password user
   * @param user
   * @param body
   */
  async changePassword(
    user: UsersEntity,
    body: ChangePasswordUserDto
  ): Promise<boolean> {
    if (body.password_new !== body.password_confirm)
      throw { message: "Xác nhận mật khẩu không đúng", status: 400 };
    // save password new
    user.password = body.password_new;
    user.hashPassword();
    await this.usersRepository.save(user);
    return true;
  }

  async findUserByPhone(phone: string) {
    return await this.usersRepository.findOne({
      where: { phone, deleted_at: null },
    });
  }

  async adminCreateCustomer(body: any): Promise<UsersEntity> {
    let user = new UsersEntity();
    const passwordTemp = "Azgo@123"; // Math.random().toString(36).slice(2);
    user.phone = body.phone;
    user.country_code = body.country_code;
    user.type = body.type;
    user.password = passwordTemp;
    user.hashPassword();
    // Use transaction for data synchronization
    await getManager().transaction(async (transactionalEntityManager) => {
      // create user
      user = await transactionalEntityManager.save(user);
      // send sms for customer
      const profile = new CustomerProfileEntity();
      // update profile
      profile.users = user;
      profile.full_name = body.full_name;
      profile.gender = 3;
      await transactionalEntityManager.save(profile);
    });
    await this.sendSMS({
      phone: "+" + user.country_code + user.phone,
      password: passwordTemp,
      Brandname: "NhacLichhen",
    });
    return await this.findOne(user.user_id);
  }

  async sendSMS(body: {
    phone: string;
    password: string;
    SmsType?: number;
    Brandname?: string;
  }): Promise<any> {
    let content =
      "Admin da tao tai khoan AZGO cho ban, vui long truy cap bang so dien thoai nay voi mat khau la: " +
      body.password;
    if (config.enableSMS.toUpperCase() === "TRUE") {
      // Set sms
      const rsSendOtp = await this.otpService.sendOtp({
        Phone: body.phone,
        Content: content,
        SmsType: 2,
        Brandname: body.Brandname,
      });
      if (Number(rsSendOtp.CodeResult).valueOf() !== 100)
        throw { status: 400, message: rsSendOtp.ErrorMessage };
    }
    return true;
  }
}
