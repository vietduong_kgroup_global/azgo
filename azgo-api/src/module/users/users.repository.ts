import { Brackets, EntityRepository, Repository } from "typeorm";
import * as moment from "moment";
import { UsersEntity } from "./users.entity";
import { UserType } from "@azgo-module/core/dist/utils/enum.ultis";

@EntityRepository(UsersEntity)
export class UsersRepository extends Repository<UsersEntity> {
  /**
   * This function get all users
   * @param options
   */
  async getAll(options?: any) {
    let query_limit = !isNaN(Number(options.limit).valueOf())
      ? Number(options.limit).valueOf()
      : 10;
    let is_load_more = false;

    // create new query
    let query = this.createQueryBuilder("az_users");
    if (options.keyword.startsWith("+84")) {
      options.keyword = options.keyword.substring(3, options.keyword.length);
    } else if (options.keyword.startsWith("0")) {
      options.keyword = options.keyword.substring(1, options.keyword.length);
    }
    // search by keyword
    if (options.keyword && options.keyword.trim()) {
      query.where((qb) => {
        const subQuery = qb
          .subQuery()
          .select("id")
          .from(UsersEntity, "sales")
          .where("first_name like :first_name", {
            first_name: "%" + options.keyword.trim() + "%",
          })
          .orWhere("middle_name like :middle_name", {
            middle_name: "%" + options.keyword.trim() + "%",
          })
          .orWhere("last_name like :last_name", {
            last_name: "%" + options.keyword.trim() + "%",
          })
          .orWhere("az_users.phone like :phone", {
            phone: "%" + options.keyword.trim() + "%",
          })
          .orWhere("az_users.email like :email", {
            email: "%" + options.keyword.trim() + "%",
          })
          .getQuery();
        return "id IN " + subQuery;
      });
    }

    // count list users
    let count_query = query.getCount();

    // pagination
    if (options.last_id)
      query.andWhere("id < :last_id", { last_id: options.last_id });

    // list users
    let users_query = query
      .orderBy("id", "DESC")
      .take(query_limit + 1)
      .getMany();

    let results = await Promise.all([count_query, users_query]);

    if (results[1].length > query_limit) {
      is_load_more = true;
      results[1].pop();
    }
  

    return {
      count: results[0],
      users: results[1],
      load_more: is_load_more,
    };
  }

  /**
   * This function get find by user id
   * @param user_id
   */
  async findByUserId(user_id: string): Promise<UsersEntity> {
    return await this.findOne({
      where: { user_id },
      relations: [
        "user_roles",
        "user_roles.roles",
        "customerProfile",
        "driverProfile",
        "driverProfile.areaInfo",
        "adminProfile",
        "adminProfile.areaInfo",
        "providersInfo",
        "vehicles",
      ],
    });
  }

  /**
   * This is function get user by id
   * @param id
   */
  async findById(id: number): Promise<UsersEntity> {
    return await this.findOne({
      where: { id },
      relations: [
        "user_roles",
        "customerProfile",
        "driverProfile",
        "adminProfile",
      ],
    });
  }

  /**
   * This function get find by id and search options
   * @param user_id
   * @param options
   */
  async findByIdOptions(user_id: number, options?: any): Promise<UsersEntity> {
    let role_id = options.role_id ? options.role_id.trim() : null;
    let query = await this.createQueryBuilder("user");
    query.leftJoinAndSelect("user.user_roles", "user_roles");
    query.where("user.id = :user_id", { user_id });
    if (role_id) {
      query.andWhere("user_roles.role_id = :role_id", { role_id });
    }
    return query.getOne();
  }

  /**
   * This function check exist user by email or phone number
   * @param email
   * @param phone
   */
  async isExistUser(email: string, phone: string): Promise<boolean> {
    let query = this.createQueryBuilder("user").where("user.phone = :phone", {
      phone,
    });
    if (email) {
      query.orWhere("user.email = :email", { email });
    }
    const user = await query.getOne();
    return !!user;
  }

  /**
   * This function check exist user by email or phone number
   * @param email
   * @param phone
   */
  async findOneByEmailOrPhone(
    email: string,
    phone: string
  ): Promise<UsersEntity> {
    let query = this.createQueryBuilder("user")
      .leftJoinAndSelect("user.user_roles", "user_roles")
      .leftJoinAndSelect("user_roles.roles", "roles")
      .leftJoinAndSelect("user.driverProfile", "driverProfile")
      .leftJoinAndSelect("user.customerProfile", "customerProfile")
      .leftJoinAndSelect("user.adminProfile", "adminProfile")
      .where("user.phone = :phone", { phone });
    if (email) {
      query.orWhere("email = :email", { email });
    }
    const user = await query.getOne();
    return user;
  }

  async findOneByPhone(phone: string): Promise<UsersEntity> {
    let query = this.createQueryBuilder("user")
      .leftJoinAndSelect("user.user_roles", "user_roles")
      .leftJoinAndSelect("user_roles.roles", "roles")
      .leftJoinAndSelect("user.driverProfile", "driverProfile")
      .leftJoinAndSelect("user.customerProfile", "customerProfile")
      .leftJoinAndSelect("user.adminProfile", "adminProfile")
      .where("user.phone = :phone", { phone });
    const user = await query.getOne();
    return user;
  }

  async findUserByEmail(email: string): Promise<UsersEntity> {
    let query = this.createQueryBuilder("user").where("user.email = :email", {
      email,
    });
    const user = await query.getOne();
    return user;
  }

  /**
   * This is function find user by email or phone
   * @param value
   */
  async findByEmailOrPhone(value: string): Promise<UsersEntity> {
    return await this.createQueryBuilder("user")
      .leftJoinAndSelect("user.user_roles", "user_roles")
      .leftJoinAndSelect("user_roles.roles", "roles")
      .leftJoinAndSelect("user.driverProfile", "driverProfile")
      .leftJoinAndSelect("user.customerProfile", "customerProfile")
      .leftJoinAndSelect("user.adminProfile", "adminProfile")
      .where("user.email = :value", { value })
      .orWhere("user.phone = :value", { value })
      .getOne();
  }

  /**
   * This is function find user by facebook id
   * @param id
   */
  async findByFacebookId(id: string): Promise<UsersEntity> {
    return await this.findOne({
      where: {
        fb_id: `${id}`,
      },
      relations: ["user_roles", "user_roles.roles"],
    });
  }

  /**
   * This is function find user by google id
   * @param id
   */
  async findByGoogleId(id: string): Promise<UsersEntity> {
    return await this.findOne({
      where: {
        gg_id: `${id}`,
      },
      relations: ["user_roles", "user_roles.roles"],
    });
  }

  /**
   * This is function find user by uuid
   * @param uuid
   */
  async findByUuid(uuid: string) {
    return await this.findOne({ where: { user_id: uuid } });
  }
  /**
   * This is function search or with subQuery
   * @param query
   * @param relation
   * @param keyword
   */
  async searchOrSubQuery(
    query: any,
    relation: string,
    keyword: any
  ): Promise<any> {
    if (keyword.startsWith("0")) keyword = keyword.substring(1, keyword.length);
    else if (keyword.startsWith("+84"))
      keyword = keyword.substring(3, keyword.length);

    if (!relation) {
      // search in user list
      return query.where((qb) => {
        const subQuery = qb
          .subQuery()
          .select(`az_users.id`)
          .from(UsersEntity, `az_users`)
          .orWhere(`az_users.phone like :phone`, { phone: "%" + keyword + "%" })
          .orWhere(`az_users.email like :email`, { email: "%" + keyword + "%" })
          .getQuery();
        return "az_users.id IN " + subQuery;
      });
    } else {
      return query.where((qb) => {
        const subQuery = qb
          .subQuery()
          .select(`az_users.id`)
          .from(UsersEntity, `az_users`)
          .leftJoin(`az_users.${relation}`, `${relation}`)
          .where(`${relation}.full_name like :full_name`, {
            full_name: "%" + keyword + "%",
          })
          .orWhere(`az_users.phone like :phone`, { phone: "%" + keyword + "%" })
          .orWhere(`az_users.email like :email`, { email: "%" + keyword + "%" })
          .getQuery();
        return "az_users.id IN " + subQuery;
      });
    }
  }

  /**
   * This function get list users by limit & offset
   * @param limit
   * @param offset
   * @param options
   */
  async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
    let type = !isNaN(Number(options.type).valueOf())
      ? Number(options.type).valueOf()
      : null;
    let query = this.createQueryBuilder("az_users");
    query.leftJoinAndSelect("az_users.adminProfile", "adminProfile");
    query.leftJoinAndSelect("az_users.customerProfile", "customerProfile");
    query.leftJoinAndSelect("az_users.driverProfile", "driverProfile");
    query.leftJoinAndSelect("az_users.providersInfo", "providersInfo");
    query.leftJoinAndSelect("providersInfo.vehicles", "vehicles");
    // search by keyword
    if (options.keyword && options.keyword.trim()) {
      switch (type) {
        case UserType.ADMIN:
          query = await this.searchOrSubQuery(
            query,
            "adminProfile",
            options.keyword.trim()
          );
          break;
        case UserType.CUSTOMER:
          query = await this.searchOrSubQuery(
            query,
            "customerProfile",
            options.keyword.trim()
          );
          break;
        case UserType.PROVIDER:
          query = await this.searchOrSubQuery(
            query,
            "providersInfo",
            options.keyword.trim()
          );
          break;
        case UserType.DRIVER_BIKE_CAR:
        case UserType.DRIVER_BUS:
        case UserType.DRIVER_VAN_BAGAC:
          query = await this.searchOrSubQuery(
            query,
            "driverProfile",
            options.keyword.trim()
          );
          break;
        default:
          query = await this.searchOrSubQuery(
            query,
            "",
            options.keyword.trim()
          );
      }
    }

    if (type) {
      query.andWhere("az_users.type = :type", { type });
    }
    if (!!options.role) {
      query.andWhere("JSON_CONTAINS(roles, ':role') <> 0", {
        role: options.role,
      });
    }

    // not delete_at
    query.andWhere("az_users.deleted_at is null");
    // limit + offset
    query.orderBy("az_users.id", "DESC").limit(limit).offset(offset);

    const results = await query.getManyAndCount();

    return {
      success: true,
      data: results[0],
      total: results[1],
    };
  }

  /**
   * This is function get all customer
   * @param limit
   * @param offset
   * @param options
   */
  async getAllCustomer(limit: number, offset: number, options?: any) {
    let query = this.createQueryBuilder("az_users");
    query.leftJoinAndSelect("az_users.customerProfile", "customerProfile");
    query.leftJoinAndSelect("az_users.userProfile", "userProfile");

    // search by keyword
    if (options.keyword && options.keyword.trim()) {
      options.keyword = options.keyword.trim();
      if (options.keyword.startsWith("+84")) {
        options.keyword = options.keyword.substring(3, options.keyword.length);
      } else if (options.keyword.startsWith("0")) {
        options.keyword = options.keyword.substring(1, options.keyword.length);
      }
      query = await this.searchOrSubQuery(
        query,
        "customerProfile",
        options.keyword.trim()
      );
    }

    // query.andWhere('az_users.type = :type', { type: UserType.CUSTOMER });
    query.andWhere("JSON_CONTAINS(roles, ':role') <> 0", {
      role: UserType.CUSTOMER,
    });
    query.andWhere("az_users.deleted_at is null");
    query.andWhere("az_users.customer_deleted is null");
    query.orderBy("az_users.id", "DESC").limit(limit).offset(offset);
    const results = await query.getManyAndCount();

    return {
      success: true,
      data: results[0],
      total: results[1],
    };
  }

  /**
   * This is function get all driver
   * @param limit
   * @param offset
   * @param options
   */
  async getAllDriversAvaiable(arr_driver_id: number[], options?: any) {
    try {
      let query = this.createQueryBuilder("az_users")
        // .leftJoinAndSelect('az_users.driverProfile', 'driverProfile')
        // .leftJoinAndSelect('az_users.userProfile', 'userProfile')
        // .leftJoinAndSelect('driverProfile.ProvidersInfoEntity', 'ProvidersInfoEntity')
        // .leftJoinAndSelect('az_users.vehicles', 'vehicles')
        // .leftJoinAndSelect('vehicles.vehicleType', 'vehicleType')
        .leftJoinAndSelect("az_users.workingDateDriver", "workingDateDriver")
        .where("az_users.deleted_at is null")
        .where("az_users.driver_deleted is null")
        .orderBy("az_users.id", "DESC");
      if (!options.role) {
        query.andWhere(
          "az_users.type IN (:DRIVER_BUS, :DRIVER_BIKE_CAR, :DRIVER_VAN_BAGAC, :DRIVER_TOUR)",
          {
            DRIVER_BUS: UserType.DRIVER_BUS,
            DRIVER_BIKE_CAR: UserType.DRIVER_BIKE_CAR,
            DRIVER_VAN_BAGAC: UserType.DRIVER_VAN_BAGAC,
            DRIVER_TOUR: UserType.DRIVER_TOUR,
          }
        );
      }
      if (!!options.role) {
        query.andWhere("JSON_CONTAINS(roles, ':role') <> 0", {
          role: options.role,
        });
      }
      if (!!options.total_seat) {
        if (arr_driver_id.length === 0) {
          return [];
        }
        query.andWhere("az_users.id IN (:arr_driver_id)", { arr_driver_id });
      }
      // limit + offset
      query.orderBy("az_users.id", "DESC");
      let results = await query.getMany();

      if (options.start_time) {
        let start_date = moment(options.start_time).format("DD-MM-YYYY");
        results = results.filter((driver) => {
          let check = true;
          if (!driver.workingDateDriver.length) return true;
          driver.workingDateDriver.forEach((item) => {
            if (
              moment(Number(item.working_date)).format("DD-MM-YYYY") ===
              start_date
            )
              check = false;
          });
          return check;
        });
      }
      return results;
    } catch (error) {
      console.log("users.getAllDrivers: ", error);
      return [];
    }
  } 

  /**
   * This is function get all driver
   * @param limit
   * @param offset
   * @param options
   */
  async getAllDrivers(
    arr_driver_id: number[],
    limit: number,
    offset: number,
    options?: any
  ) {
    try {
      let provider_id = !isNaN(Number(options.provider_id).valueOf())
        ? Number(options.provider_id).valueOf()
        : null;
      let query = this.createQueryBuilder("az_users")
        .leftJoinAndSelect("az_users.driverProfile", "driverProfile")
        .leftJoinAndSelect("az_users.userProfile", "userProfile")
        .leftJoinAndSelect(
          "driverProfile.ProvidersInfoEntity",
          "ProvidersInfoEntity"
        )
        .leftJoinAndSelect("az_users.vehicles", "vehicles")
        .leftJoinAndSelect("az_users.walletEntity", "walletEntity")
        .leftJoinAndSelect("vehicles.vehicleType", "vehicleType")
        .where("az_users.deleted_at is null")
        .where("az_users.driver_deleted is null")
        .orderBy("az_users.id", "DESC");
      if (!options.keyword === false) {
        options.keyword = options.keyword.trim();
        if (options.keyword.startsWith("0"))
          options.keyword = options.keyword.substring(
            1,
            options.keyword.length
          );
        else if (
          options.keyword.startsWith("+84") ||
          options.keyword.startsWith("84")
        )
          options.keyword = options.keyword.substring(
            3,
            options.keyword.length
          );
        query.andWhere(
          new Brackets((qb) => {
            qb.where("az_users.phone like :keyword", {
              keyword: "%" + options.keyword.trim() + "%",
            })
              .orWhere("az_users.email like :keyword", {
                keyword: "%" + options.keyword.trim() + "%",
              })
              .orWhere("driverProfile.full_name like :keyword", {
                keyword: "%" + options.keyword.trim() + "%",
              })
              .orWhere("ProvidersInfoEntity.provider_name like :keyword", {
                keyword: "%" + options.keyword.trim() + "%",
              });
          })
        );
      }
      // filter by area_uuid in token
      if (options.area_uuid) {
          query.andWhere(
            new Brackets((qb) => {
              qb.where(`driverProfile.area_uuid = :area_uuid`, { area_uuid: options.area_uuid })
                .orWhere(`driverProfile.area_uuid IS NULL`)
            })
          );
      }
      if (provider_id && options.user_info && options.user_info.type === 1) {
        query.andWhere("driverProfile.provider_id = :provider_id", {
          provider_id,
        });
      }
      if (options.user_info && options.user_info.type === 4) {
        const usersEntity = await this.findByUserId(
          options.user_info.user_id.toString()
        );
        query.andWhere("driverProfile.provider_id = :provider_id", {
          provider_id: usersEntity.providersInfo.id,
        });
      }
      if (!!options.role) {
        query.andWhere("JSON_CONTAINS(roles, ':role') <> 0", {
          role: options.role,
        });
      } else {
        query.andWhere(
          new Brackets((qb) => {
            qb.where("JSON_CONTAINS(roles, ':role') <> 0", {
              role: UserType.DRIVER_TOUR,
            })
              .orWhere("JSON_CONTAINS(roles, ':role1') <> 0", {
                role1: UserType.DRIVER_BUS,
              })
              .orWhere("JSON_CONTAINS(roles, ':role2') <> 0", {
                role2: UserType.DRIVER_VAN_BAGAC,
              })
              .orWhere("JSON_CONTAINS(roles, ':role3') <> 0", {
                role3: UserType.DRIVER_BIKE_CAR,
              });
          })
        );
      }
      if (!!options.total_seat) {
        if (arr_driver_id.length === 0) {
          return {
            success: true,
            data: [],
            total: 0,
          };
        }
        query.andWhere("az_users.id IN (:arr_driver_id)", { arr_driver_id });
      }
      limit + offset;
      query.orderBy("az_users.id", "DESC").take(limit).skip(offset);
      const results = await query.getManyAndCount();

      return {
        success: true,
        data: results[0],
        total: results[1],
      };
    } catch (error) {
      console.log("users.getAllDrivers: ", error);
      return {
        success: false,
        data: [],
        total: 0,
      };
    }
  }

  /**
   * This is function get all driver
   * @param limit
   * @param offset
   * @param options
   */
  async getAllDriverBus(limit: number, offset: number, options?: any) {
    let provider_id = !isNaN(Number(options.provider_id).valueOf())
      ? Number(options.provider_id).valueOf()
      : null;
    let query = this.createQueryBuilder("az_users")
      .leftJoinAndSelect("az_users.driverProfile", "driverProfile")
      .leftJoinAndSelect(
        "driverProfile.ProvidersInfoEntity",
        "ProvidersInfoEntity"
      )
      .leftJoinAndSelect("az_users.vehicles", "vehicles")
      .leftJoinAndSelect("vehicles.vehicleType", "vehicleType")
      .where("az_users.deleted_at is null")
      .where("az_users.driver_deleted is null")
      .orderBy("az_users.id", "DESC");
    if (!options.keyword === false) {
      query.andWhere(
        new Brackets((qb) => {
          qb.where("az_users.phone like :keyword", {
            keyword: "%" + options.keyword.trim() + "%",
          })
            .orWhere("az_users.email like :keyword", {
              keyword: "%" + options.keyword.trim() + "%",
            })
            .orWhere("driverProfile.full_name like :keyword", {
              keyword: "%" + options.keyword.trim() + "%",
            })
            .orWhere("ProvidersInfoEntity.provider_name like :keyword", {
              keyword: "%" + options.keyword.trim() + "%",
            });
        })
      );
    }
    if (provider_id && options.user_info.type === 1) {
      query.andWhere("driverProfile.provider_id = :provider_id", {
        provider_id,
      });
    }
    if (options.user_info.type === 4) {
      const usersEntity = await this.findByUserId(
        options.user_info.user_id.toString()
      );
      query.andWhere("driverProfile.provider_id = :provider_id", {
        provider_id: usersEntity.providersInfo.id,
      });
    }
    // query.andWhere('az_users.type =:type', { type: UserType.DRIVER_BUS });
    query.andWhere("JSON_CONTAINS(roles, ':role') <> 0", {
      role: UserType.DRIVER_BUS,
    });
    // limit + offset
    query.orderBy("az_users.id", "DESC").take(limit).skip(offset);

    const results = await query.getManyAndCount();

    return {
      success: true,
      data: results[0],
      total: results[1],
    };
  }

  /**
   * This is function get all driver bike car or van bagac
   * @param vehicle_group_id
   * @param limit
   * @param offset
   * @param options
   */
  async getAllDriverBikeCarOrVanBagac(
    vehicle_group_id: number,
    limit: number,
    offset: number,
    options?: any
  ) {
    let query = this.createQueryBuilder("az_users")
      .leftJoinAndSelect("az_users.driverProfile", "driverProfile")
      .leftJoinAndSelect("az_users.vehicles", "vehicles")
      .leftJoinAndSelect("vehicles.vehicleType", "vehicleType")
      .where("az_users.deleted_at is null")
      .where("az_users.driver_deleted is null")
      // .andWhere('az_users.type =:type', { type: vehicle_group_id });
      .andWhere("JSON_CONTAINS(roles, ':role') <> 0", {
        role: vehicle_group_id,
      });
    if (!options.keyword === false) {
      query.andWhere(
        new Brackets((qb) => {
          qb.where("az_users.phone like :keyword", {
            keyword: "%" + options.keyword.trim() + "%",
          })
            .orWhere("az_users.email like :keyword", {
              keyword: "%" + options.keyword.trim() + "%",
            })
            .orWhere("driverProfile.full_name like :keyword", {
              keyword: "%" + options.keyword.trim() + "%",
            })
            .orWhere("vehicles.license_plates like :keyword", {
              keyword: "%" + options.keyword.trim() + "%",
            });
        })
      );
    }
    query.orderBy("az_users.id", "DESC").skip(offset).take(limit);
    const results = await query.getManyAndCount();
    return {
      success: true,
      data: results[0],
      total: results[1],
    };
  }

  /**
   * This is function get all driver bike car or van bagac without vehicle
   * @param vehicle_group_id
   * @param limit
   * @param offset
   * @param options
   */
  async getAllDriverBikeCarOrVanBagacWithoutVehicle(
    vehicle_group_id: number,
    arr_driver_id: number[],
    limit: number,
    offset: number,
    options?: any
  ) {
    let query = this.createQueryBuilder("az_users")
      .leftJoinAndSelect("az_users.driverProfile", "driverProfile")
      .leftJoinAndSelect("az_users.vehicles", "vehicles")
      .leftJoinAndSelect("vehicles.vehicleType", "vehicleType")
      .where("az_users.deleted_at is null")
      .where("az_users.driver_deleted is null")
      // .andWhere('az_users.type =:type', { type: vehicle_group_id });
      .andWhere("JSON_CONTAINS(roles, ':role') <> 0", {
        role: vehicle_group_id,
      });
    if (!options.keyword === false) {
      query.andWhere(
        new Brackets((qb) => {
          qb.where("az_users.phone like :keyword", {
            keyword: "%" + options.keyword.trim() + "%",
          }).orWhere("az_users.email like :keyword", {
            keyword: "%" + options.keyword.trim() + "%",
          });
        })
      );
    }
    if (arr_driver_id.length > 0) {
      query.andWhere("az_users.id not in(:arr_driver_id)", { arr_driver_id });
    }
    query.skip(offset).take(limit);
    const results = await query.getManyAndCount();
    return {
      success: true,
      data: results[0],
      total: results[1],
    };
  }

  /**
   * This is function get all admin
   * @param limit
   * @param offset
   * @param options
   */
  async getAllAdmin(limit: number, offset: number, options?: any) {
    let query = this.createQueryBuilder("az_users")
      .leftJoinAndSelect("az_users.adminProfile", "adminProfile")
      .leftJoinAndSelect("az_users.user_roles", "userRoles")
      .leftJoinAndSelect("userRoles.roles", "roles");

    if (!options.keyword === false) {
      if (options.keyword.startsWith("+84")) {
        options.keyword = options.keyword.substring(3, options.keyword.length);
      } else if (options.keyword.startsWith("0")) {
        options.keyword = options.keyword.substring(1, options.keyword.length);
      }
      query.andWhere(
        new Brackets((qb) => {
          qb.where("az_users.phone like :keyword", {
            keyword: "%" + options.keyword.trim() + "%",
          })
            .orWhere("az_users.email like :keyword", {
              keyword: "%" + options.keyword.trim() + "%",
            })
            .orWhere("adminProfile.full_name like :keyword", {
              keyword: "%" + options.keyword.trim() + "%",
            });
        })
      );
    }
     // filter by area_uuid in token
    if (options.area_uuid) {
        query.andWhere(`adminProfile.area_uuid = :area_uuid`, { area_uuid: options.area_uuid });
        query.andWhere(`adminProfile.user_id != :user_id`, { user_id: options.user_id });
    }
    query.andWhere("JSON_CONTAINS(roles, ':role') <> 0", {
      role: UserType.ADMIN,
    });
    query.andWhere("az_users.deleted_at is null");
    query.andWhere("az_users.admin_deleted is null");
    query.orderBy("az_users.id", "DESC").take(limit).skip(offset);

    const results = await query.getManyAndCount();
    return {
      success: true,
      data: results[0],
      total: results[1],
    };
  }

  /**
   * This is function get all provider
   * @param limit
   * @param offset
   * @param options
   */
  async getAllProvider(limit: number, offset: number, options?: any) {
    let query = this.createQueryBuilder("az_users")
      .leftJoinAndSelect("az_users.providersInfo", "providersInfo")
      .leftJoinAndSelect("providersInfo.vehicles", "vehicles")
      // .where('az_users.type = :type', { type: UserType.PROVIDER })
      .where("JSON_CONTAINS(roles, ':role') <> 0", { role: UserType.PROVIDER })
      .andWhere("az_users.deleted_at is null")
      .andWhere("az_users.provider_deleted is null");
    // search by keyword
    if (!options.keyword === false) {
      query.andWhere(
        new Brackets((qb) => {
          qb.where("az_users.phone like :keyword", {
            keyword: "%" + options.keyword.trim() + "%",
          })
            .orWhere("az_users.email like :keyword", {
              keyword: "%" + options.keyword.trim() + "%",
            })
            .orWhere("providersInfo.provider_name like :keyword", {
              keyword: "%" + options.keyword.trim() + "%",
            });
        })
      );
    }
    query.orderBy("az_users.id", "DESC").skip(offset).take(limit);
    const results = await query.getManyAndCount();
    return {
      success: true,
      data: results[0],
      total: results[1],
    };
  }

  /**
   * This is function get all provider
   * @param limit
   * @param offset
   * @param options
   */
  async driverWalletReport( limit: number, offset: number, options?: any, typeWallet?: string) { 
    try {
        let query = this.createQueryBuilder('az_users')
        .leftJoin('az_users.driverProfile', 'driverProfile')
        .leftJoin('az_users.walletEntity', 'walletEntity')
        .leftJoin('az_users.userAccount', 'userAccount') 
        .leftJoin('az_users.walletHistoryEntity', 'walletHistoryEntity') 
        .leftJoin('userAccount.account', 'account')    
        .where('az_users.deleted_at is null')
        .andWhere('az_users.driver_deleted is null')
        .andWhere('walletHistoryEntity.type = :type', {type:  typeWallet === 'fee_wallet' ? 1 : 2});
        if (options.from && options.to) {
            query = query.andWhere('walletHistoryEntity.created_at >= :from ', { from: options.from})
            .andWhere('walletHistoryEntity.created_at <= :to ', { to: options.to})
        }
        // Filter by driver id
        if (!!options.driver_id) {
            query.andWhere(`az_users.id = :driver_id`, { driver_id: options.driver_id });
        }
        query = query.addSelect('driverProfile.full_name', 'driver_profile_full_name')
                    .addSelect('driverProfile.cmnd', 'driver_profile_cmnd') 
                    .addSelect('az_users.id', 'az_users_id')
                    .addSelect('SUM(walletHistoryEntity.amount)', typeWallet === 'fee_wallet' ? 'walletEntity_fee_wallet_balance' : 'walletEntity_cast_wallet_balance')
                    .addSelect('account.account_bank_number', 'account_account_bank_number')
                    .addSelect('account.account_bank_branch', 'account_account_bank_branch')
                    .addSelect('account.account_bank_name', 'account_account_bank_name')
                    .addSelect('account.account_bank_user_name', 'account_account_bank_user_name')
        query = query.groupBy('az_users_id')
                    .addGroupBy('driver_profile_full_name')
                    .addGroupBy('driver_profile_cmnd') 
                    .addGroupBy('account_account_bank_number')
                    .addGroupBy('account_account_bank_branch')
                    .addGroupBy('account_account_bank_name')
                    .addGroupBy('account_account_bank_user_name')
        
        .orderBy('az_users.id', 'DESC');   
        let results_raw_many = await query.getRawMany();
        let results_count = results_raw_many.length;
        let results = await query.offset(offset).limit(limit).getRawMany();
        return {
            success: true,
            data: results,
            total: results_count,
            error: null, 
        };  
    } catch (error) {
        console.log("users.driverWalletReport: ", error);
        return {
            success: false,
            data: [],
            total: 0,
            error: error, 
        };
    }
  }
  async exportDriverWalletReport(options?: any, typeWallet?: string) { 
      try {
          let query = this.createQueryBuilder('az_users')
          .leftJoin('az_users.driverProfile', 'driverProfile')
          .leftJoin('az_users.walletEntity', 'walletEntity')
          .leftJoin('az_users.userAccount', 'userAccount') 
          .leftJoin('az_users.walletHistoryEntity', 'walletHistoryEntity') 
          .leftJoin('userAccount.account', 'account')    
          .where('az_users.deleted_at is null')
          .andWhere('az_users.driver_deleted is null')
          .andWhere('walletHistoryEntity.type = :type', {type:  typeWallet === 'fee_wallet' ? 1 : 2});
          if (options.from && options.to) {
              query = query.andWhere('walletHistoryEntity.created_at >= :from ', { from: options.from})
              .andWhere('walletHistoryEntity.created_at <= :to ', { to: options.to})
          }
          // Filter by driver id
          if (!!options.driver_id) {
              query.andWhere(`az_users.id = :driver_id`, { driver_id: options.driver_id });
          }
          query = query.addSelect('driverProfile.full_name', 'driver_profile_full_name')
                      .addSelect('driverProfile.cmnd', 'driver_profile_cmnd') 
                      .addSelect('az_users.id', 'az_users_id')
                      .addSelect('SUM(walletHistoryEntity.amount)', typeWallet === 'fee_wallet' ? 'walletEntity_fee_wallet_balance' : 'walletEntity_cast_wallet_balance')
                      .addSelect('account.account_bank_number', 'account_account_bank_number')
                      .addSelect('account.account_bank_branch', 'account_account_bank_branch')
                      .addSelect('account.account_bank_name', 'account_account_bank_name')
                      .addSelect('account.account_bank_user_name', 'account_account_bank_user_name')
          query = query.groupBy('az_users_id')
                      .addGroupBy('driver_profile_full_name')
                      .addGroupBy('driver_profile_cmnd') 
                      .addGroupBy('account_account_bank_number')
                      .addGroupBy('account_account_bank_branch')
                      .addGroupBy('account_account_bank_name')
                      .addGroupBy('account_account_bank_user_name')
          
          .orderBy('az_users.id', 'DESC');    
          let results = await query.getRawMany();
          // console.log('results',results) 
          return {
              success: true,
              data: results, 
          };  
      } catch (error) {
          console.log("users.driverWalletReport: ", error);
          return {
              success: false, 
              error, 
          };
      }
  }
}
