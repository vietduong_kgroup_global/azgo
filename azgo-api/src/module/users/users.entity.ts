import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToMany,
    JoinTable,
    OneToOne,
    Index,
    Generated,
    OneToMany,
    JoinColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { UserRolesEntity } from '../user-roles/user-roles.entity';
import { AdminProfileEntity } from '../admin-profile/admin-profile.entity';
import { UserAccountEntity } from '../user-account/user-account.entity';
import { DriverProfileEntity } from '../driver-profile/driver-profile.entity';
import { CustomerProfileEntity } from '../customer-profile/customer-profile.entity';
import { UserProfileEntity } from '../user-profile/user-profile.entity';
import { ProvidersInfoEntity } from '../providers-info/providers-info.entity';
import * as bcrypt from 'bcryptjs';
import { BikeCarOrderEntity } from '../bike-car-order/bikeCarOrder.entity';
import { TourOrderEntity } from '../tour-order/tour-order.entity';
import { RequestWithdrawEntity } from '../request-withdraw/request-withdraw.entity';
import { RequestWithdrawHistoryEntity } from '../request-withdraw-history/request-withdraw-history.entity';
import { TourOrderLogEntity } from '../tour-order-log/tour-order-log.entity';
import { UserAccountLogEntity } from '../user-account-log/user-account-log.entity';
import { NotificationEntity } from '../notification/notification.entity';
import { DriverLogsEntity } from '../driver-logs/driver-logs.entity';
import { DriverWorkingDateEntity } from '../driver-working-date/driver-working-date.entity';
import { VehicleEntity } from '../vehicle/vehicle.entity';
import { WalletEntity } from '../wallet/wallet.entity';
import { WalletHistoryEntity } from '../wallet-history/wallet-history.entity';
import { WithdrawMoneyEntity } from '../withdraw-money/withdraw-money.entity';
import { FirebaseEntity } from '../firebase/firebase.entity';

@Entity('az_users')
export class UsersEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'binary',
        length: 36,
    })
    @Generated('uuid')
    user_id: string;

    @Index('user_email_unique', { unique: true })
    @Column({
        length: 255,
        nullable: true,
    })
    email: string;

    @Column()
    country_code: number;

    @Index('user_phone_unique', { unique: true })
    @Column()
    phone: string;

    @Exclude()
    @Column({
        length: 255,
        // select: false
    })
    password: string;

    @Column({
        type: 'smallint',
        default: 1,
    })
    status: number;

    @Column({
        type: 'smallint',
        default: 1,
    })
    platform: number;

    @Column({
        type: 'datetime',
        nullable: true,
    })
    block: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    provider_deleted: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    customer_deleted: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    driver_deleted: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    admin_deleted: Date;

    @Column({
        type: 'varchar',
        length: '100',
        nullable: true,
    })
    fb_id: string;

    @Column({
        type: 'varchar',
        length: '100',
        nullable: true,
    })
    gg_id: string;

    @Column({
        type: 'smallint',
        default: 1,
    })
    type: number;

    @Column({
        type: 'json',
        default: [1],
        nullable: false,
    })
    roles: object;

    @OneToMany(type => UserRolesEntity, user_role => user_role.users)
    user_roles: UserRolesEntity[];

    @OneToOne(type => AdminProfileEntity, admin_profile => admin_profile.users) // specify inverse side as a second parameter
    adminProfile: AdminProfileEntity;

    @OneToOne(type => UserAccountEntity, type => type.user) // specify inverse side as a second parameter
    userAccount: UserAccountEntity;

    @OneToOne(type => DriverProfileEntity, driver_profile => driver_profile.users) // specify inverse side as a second parameter
    driverProfile: DriverProfileEntity;

    @OneToOne(type => CustomerProfileEntity, customer_profile => customer_profile.users) // specify inverse side as a second parameter
    customerProfile: CustomerProfileEntity;

    @OneToOne(type => UserProfileEntity, user_profile => user_profile.users) // specify inverse side as a second parameter
    userProfile: UserProfileEntity;

    @OneToOne(type => ProvidersInfoEntity, providers_info => providers_info.users) // specify inverse side as a second parameter
    providersInfo: ProvidersInfoEntity;

    @OneToMany(type => BikeCarOrderEntity, bike_car_order => bike_car_order.customerProfile)
    bikeCarCustomers: BikeCarOrderEntity[];

    @OneToMany(type => BikeCarOrderEntity, bike_car_order => bike_car_order.driverProfile)
    bikeCarDrivers: BikeCarOrderEntity[];

    @OneToOne(type => TourOrderEntity, tour_order => tour_order.customerTour)
    tourCustomer: TourOrderEntity;

    @OneToOne(type => TourOrderEntity, tour_order => tour_order.driverTour)
    tourDriver: TourOrderEntity;

    @OneToMany(type => RequestWithdrawEntity, request => request.driverInfo)
    driverRequests: RequestWithdrawEntity[];

    @OneToMany(type => RequestWithdrawHistoryEntity, request => request.userUpdate)
    requestUpdate: RequestWithdrawHistoryEntity[];

    @OneToMany(type => DriverLogsEntity, driver_logs => driver_logs.driver)
    driverLogs: DriverLogsEntity[];

    @OneToMany(type => DriverWorkingDateEntity, working_log => working_log.driverInfo)
    workingDateDriver: DriverWorkingDateEntity[];

    @ManyToMany(type => VehicleEntity, vehicle => vehicle.users)
    @JoinTable({
        name: 'az_vehicle_driver',
        joinColumn: {
            name: 'driver_id',
            referencedColumnName: 'id',
        },
        inverseJoinColumn: {
            name: 'vehicle_id',
            referencedColumnName: 'id',
        },
    })
    vehicles: VehicleEntity[];

    @OneToOne(type => WalletEntity, wallet => wallet.usersEntity)
    walletEntity: WalletEntity;

    @OneToMany(type => WithdrawMoneyEntity, withdraw => withdraw.usersEntity)
    withDrawMoney: WithdrawMoneyEntity[];

    @OneToMany(type => WalletHistoryEntity, history => history.usersEntity)
    walletHistoryEntity: WalletHistoryEntity[];

    @OneToOne(type => FirebaseEntity, fcm => fcm.usersEntity)
    fcmToken: FirebaseEntity;

    @OneToOne(type => TourOrderLogEntity, tour_order_log => tour_order_log.userProfile)
    tourOrderLog: TourOrderLogEntity;

    @OneToMany(type => UserAccountLogEntity, user_account_log => user_account_log.userProfile)
    userAccountLog: UserAccountLogEntity[];

    @OneToOne(type => NotificationEntity, tour_order_log => tour_order_log.userProfile)
    notification: NotificationEntity;

    /*
    * This property is used to get full phone number of user
    * */
    get full_phone_number() {
        return '+' + this.country_code + this.phone;
    }

    /*
    * This property is used to get user_id with string type
    * */
    get user_id_str() {
        return this.user_id.toString();
    }

    /*
     * This function check valid user
     */
    get is_valid() {
        return !!this.status;
    }

    /*
     * This function has password
     */
    hashPassword() {
        this.password = bcrypt.hashSync(this.password, 8);
    }

    /*
     * This function compare password
     */
    checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
        return bcrypt.compareSync(unencryptedPassword, this.password);
    }
}
