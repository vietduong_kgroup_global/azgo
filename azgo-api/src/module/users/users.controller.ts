import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Req, Res, UseGuards, Redirect } from '@nestjs/common';
import {
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
    ApiBearerAuth,
} from '@nestjs/swagger';
import { Response } from 'express';
import { AuthGuard } from '@nestjs/passport';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import { UserUtil } from '@azgo-module/core/dist/utils/user.util';
import { PhoneNumberUtil } from '@azgo-module/core/dist/utils/phone-number.util';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { FunctionKey, ActionKey, RolesType } from '@azgo-module/core/dist/common/guards/roles.guards';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import * as jwt from 'jsonwebtoken';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/createUser.dto';
import { UpdateUserDto } from './dto/updateUser.dto';
import { CreateUserDriverDto } from './dto/createUserDriver.dto';
import { CreateUserMobileDto } from './dto/createUserMobile.dto';
import { SendOTPDto } from './dto/sendOTP.dto';
import { ValidateOTPDto } from './dto/validateOTP.dto';
import { ForgotPasswordSendOTPDto } from './dto/forgotPasswordSendOTP.dto';
import { ValidateOTPForgotPasswordDto } from './dto/validateOTPForgotPassword.dto';
import { ChangePasswordUserDto } from './dto/changePasswordUser.dto';
import { CreateUserProviderDto } from './dto/createUserProvider.dto';
import { CreateUserCustomerDto } from './dto/createUserCustomer.dto';
import { CreateUserAdminDto } from './dto/createUserAdmin.dto';
import config from '../../../config/config';
import * as _ from 'lodash';
import { RolesService } from '../roles/roles.service';
import {  formatDateTime, changeFormatDate } from '../../helpers/utils';
import * as filePackage from '../../../package.json';
import {
    ERROR_REPORT_DRIVER_WALLET,
} from '../../constants/error.const'; 
@ApiUseTags('Users')
@Controller('users')
export class UsersController {
    constructor(
        private readonly rolesService: RolesService,
        private readonly usersService: UsersService,
    ) { }
    @Get('/checkEnvironment')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiOperation({ title: 'checkEnvironment' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    // @ApiImplicitParam({ name: 'user_id', description: 'User UUID', required: true, type: 'string' })
    async checkEnvironment(@Req() request, @Res() res: Response) {
        console.log(process.env);
        try {
            const { ENV_REDIS_HOST, ENV_IDENTITY_DATABASE } = process.env;
            const { env, redis, database } = request.query;
            console.log('0', this.rolesService);
            let results = await this.rolesService.getAll({});
            // let results_redis = this.redisService.triggerEventDemo({room: 'user_123456', message: 'test socket'}) 
            // console.log('2', results_redis);
            // if (env === 'develop') {
            //     return ENV_REDIS_HOST === '' && ENV_IDENTITY_DATABASE === 'azgo-prod'
            // }
        } catch (error) {
            console.log(error);
        }

    }
    @Get('/apiversion')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @ApiOperation({ title: 'Get API Version' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'user_id', description: 'User UUID', required: true, type: 'string' })
    async getApiVersion(@Req() request, @Res() res: Response) {
        try {
            return res.status(HttpStatus.OK).send(dataSuccess('oK', filePackage.version));
        } catch (error) {
            console.log(error);
        }

    }

    @Get('')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get Users' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'type', description: 'User Type (1: Admin, 2: Driver, 3: Customer, 4: Provider)', required: false, type: 'number' })
    async findAll(@Req() request, @Res() res: Response) {
        let results, users = [];
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.usersService.getAllByOffsetLimit(per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).
                send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
        }

        for (let user of results.data) {
            if (user.type === UserType.PROVIDER) {
                user.providersInfo.total_vehicles = user.providersInfo.vehicles.length;
                delete user.providersInfo.vehicles;
            }
            users.push(await UserUtil.serialize(user));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', users, results.total, options));

    }

    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @Get('/administrators')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get Users Administrators' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    async findAllAdmin(@Req() request, @Res() res: Response) {
        let results, users = [];
        const token = request.headers.authorization.split(' ')[1];
        const userInfo: any = jwt.decode(token);
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            request.query.area_uuid = userInfo.area_uuid || '';
            request.query.user_id = userInfo.id || '';
            results = await this.usersService.findAllAdmin(per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
        }

        for (let user of results.data) {
            users.push(await UserUtil.serialize(user));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', users, results.total, options));
    }

    @Get('/customers')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get Users Customers' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    async findAllCustomer(@Req() request, @Res() res: Response) {
        let results, users = [];
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.usersService.findAllCustomer(per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Trang hiện tại không được lớn hơn tổng số trang ', null));
        }

        for (let user of results.data) {
            users.push(await UserUtil.serialize(user));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', users, results.total, options));
    }

    @Get('/drivers')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get Users Drivers' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'role', description: 'role driver', required: false, type: 'number', enum: [2, 5, 6, 7] })
    @ApiImplicitQuery({ name: 'provider_id', description: 'Provider ID', required: false, type: 'number' })
    async findAllDrivers(@Req() request, @Res() res: Response) {
        let results, users = [];
        const token = request.headers.authorization.split(' ')[1];
        const userInfo: any = jwt.decode(token);
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        let role = Number(request.query.role).valueOf();
        const offset = (page - 1) * per_page;
        if (role && (role !== UserType.DRIVER_BUS && role !== UserType.DRIVER_BIKE_CAR && role !== UserType.DRIVER_VAN_BAGAC && role !== UserType.DRIVER_TOUR)) {
            return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', [], 0, {}));
        }
        try {
            request.query.area_uuid = userInfo.area_uuid || request.query.area_uuid;
            const query_options = { ...request.query, ...{ user_info: request.user } };
            results = await this.usersService.findAllDrivers(per_page, offset, query_options);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Trang hiện tại không được lớn hơn tổng số trang ', null));
        }

        for (let user of results.data) {
            users.push(await UserUtil.serialize(user));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    @Get('/drivers-bus')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get Users Drivers Bus' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'provider_id', description: 'Provider ID', required: false, type: 'number' })
    async findAllDriverBus(@Req() request, @Res() res: Response) {
        let results, users = [];
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            const query_options = { ...request.query, ...{ user_info: request.user } };
            results = await this.usersService.findAllDriverBus(per_page, offset, query_options);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Trang hiện tại không được lớn hơn tổng số trang ', null));
        }

        for (let user of results.data) {
            users.push(await UserUtil.serialize(user));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', users, results.total, options));
    }

    @Get('/drivers-bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get Users Drivers Bike Car' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    async findAllDriverBikeCar(@Req() request, @Res() res: Response) {
        let results, users = [];
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.usersService.findAllDriverBikeCarOrVanBagac(UserType.DRIVER_BIKE_CAR, per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Trang hiện tại không được lớn hơn tổng số trang ', null));
        }

        for (let user of results.data) {
            users.push(await UserUtil.serialize(user));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', users, results.total, options));
    }

    @Get('/get-all-the-driver-bike-car-list-without-vehicle')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all the driver bike car list without vehicle' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getAllDriverBikeCarWithoutVehicle(@Req() request, @Res() res: Response) {
        let results, users = [];
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.usersService.findAllDriverBikeCarOrVanBagacWithoutVehicle(UserType.DRIVER_BIKE_CAR, per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Trang hiện tại không được lớn hơn tổng số trang ', null));
        }

        for (let user of results.data) {
            users.push(await UserUtil.serialize(user));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', users, results.total, options));
    }

    @Get('/get-all-the-driver-van-bagac-list-without-vehicle')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all the driver van bagac list without vehicle' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getAllDriverVanBagacWithoutVehicle(@Req() request, @Res() res: Response) {
        let results, users = [];
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.usersService.findAllDriverBikeCarOrVanBagacWithoutVehicle(
                UserType.DRIVER_VAN_BAGAC, per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Trang hiện tại không được lớn hơn tổng số trang ', null));
        }

        for (let user of results.data) {
            users.push(await UserUtil.serialize(user));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', users, results.total, options));
    }

    @Get('/drivers-van-bagac')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get Users Drivers Van Bagac' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    async findAllDriverVanBagac(@Req() request, @Res() res: Response) {
        let results, users = [];
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.usersService.findAllDriverBikeCarOrVanBagac(UserType.DRIVER_VAN_BAGAC, per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Trang hiện tại không được lớn hơn tổng số trang ', null));
        }

        for (let user of results.data) {
            users.push(await UserUtil.serialize(user));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', users, results.total, options));
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @Get('/providers')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get Users Providers' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    async findAllProvider(@Req() request, @Res() res: Response) {
        let results, users = [];
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.usersService.findAllProvider(per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Trang hiện tại không được lớn hơn tổng số trang ', null));
        }

        for (let user of results.data) {
            if (user.type === UserType.PROVIDER) {
                user.providersInfo.total_vehicles = user.providersInfo.vehicles.length;
                delete user.providersInfo.vehicles;
            }
            users.push(await UserUtil.serialize(user));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', users, results.total, options));
    } 
    @Get('driver_wallet_report')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get list tour order with off set limit' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'from', description: 'Filter Date From', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'to', description: 'Filter Date To', required: false, type: 'date' }) 
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    async driverWalletReport(@Req() request, @Res() res: Response) {  
        try { 
            let results;
            // ===============Query offset + per_page=============
            let per_page = Number(request.query.per_page).valueOf();
            if (isNaN(per_page)) {
                per_page = 10;
            }
    
            let page = Number(request.query.page).valueOf();
            if (isNaN(page)) {
                page = 1;
            }
            const offset = (page - 1) * per_page;
            results = await this.usersService.driverWalletReport(per_page, offset, request.query); 
         
            if (results && results.status) {
                results = results.data;
                let options = {
                    current_page: page,
                    next_page: page,
                    total_page: Math.ceil(results.total / per_page),
                };
                options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;
        
                if (options.total_page > 0 && options.next_page > options.total_page) {
                    return res.status(HttpStatus.BAD_REQUEST).
                        send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
                }
                return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
            }
            else return res.status(HttpStatus.BAD_REQUEST).send(dataError(results.message || 'Lỗi BC số dư thợ', null, results.error)); 
        } catch (e) {
            console.log(e)
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi BC số dư tài xế', null));
        } 
    }
    @Get('/export_driver_wallet_report')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'DriverWalletReport' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'from', description: 'Filter Date From', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'to', description: 'Filter Date To', required: false, type: 'date' })   
    async exportDriverWalletReport(@Req() request, @Res() res: Response) {  
        try {   
            let results;
            results = await this.usersService.exportDriverWalletReport(request.query);
            if (results && results.status) {  
                let xl = require('excel4node');
                let wb = new xl.Workbook();
                let ws = wb.addWorksheet('Báo cáo số dư ví tài xế'); 

                let data = []; 
                (results.data || []).map((item, index) => { 
                    let numberIndex = index  + 1 
                    data.push({
                        "STT": numberIndex,
                        "Số điện thoại Tài Xế" : item.az_users_phone || "",
                        "Họ và Tên Tài Xế": item.driver_profile_full_name || "",
                        "Số chứng minh Thư/Thẻ căn cước": item.driver_profile_cmnd || "",
                        "Email": item.az_users_email || "",
                        "Số Tài khoản ngân hàng": item.account_account_bank_number || "",
                        "Tên ngân hàng (Chi nhánh)": item.account_account_bank_name ? item.account_account_bank_name + `${item.account_account_bank_branch ? `(${item.account_account_bank_branch})` : ''}` : '',
                        "Tên Chủ Tài Khoản": item.account_account_bank_user_name || "",
                        "Fee (Hiện tại)": item.walletEntity_fee_wallet_balance || "",
                        "Cash (Hiện tại)": item.walletEntity_cast_wallet_balance || "",
                        "Đơn vị tiền": "VND",
                    });
                });
                const headingColumnNames = [
                    "STT",
                    "Số điện thoại tx",
                    "Tên Tài Xế",
                    "Số chứng minh Thư/Thẻ căn cước",
                    "Email",
                    "Số Tài khoản ngân hàng",
                    "Tên ngân hàng (Chi nhánh)",
                    "Tên Chủ Tài Khoản",
                    "Fee (Hiện tại)",
                    "Cash (Hiện tại)",
                    "Đơn vị tiền",
                ]
                let from = request.query.from ? changeFormatDate(request.query.from) : "";
                let to = request.query.to ?  changeFormatDate(request.query.to) : "";
                ws.cell(1, 1)
                .string('CÔNG TY CỔ PHẦN AZGO').style({font: {name: 'Times New Roman', size: 12, bold: true}})
                ws.cell(2, 1)
                .string((from && to) ? `BẢNG TỔNG HỢP SỐ DƯ VÍ TÀI  XẾ TỪ ${from} ĐẾN ${to}` : 'BẢNG TỔNG HỢP SỐ DƯ TÀI  XẾ ').style({font: {name: 'Times New Roman', size: 16, bold: true}})
                ws.cell(3, 1)
                .string(``).style({font: {name: 'Times New Roman', size: 11, bold: false}})
                // Write Column Title in Excel file
                let headingColumnIndex = 1;
                headingColumnNames.forEach(heading => {
                    ws.cell(4, headingColumnIndex++)
                        .string(heading).style({font: {name: 'Times New Roman', size: 11, bold: true},  alignment: {wrapText: true, vertical: 'top'}});
                });

                // Write Data in Excel file
                let rowIndex = 5;
                data.forEach( record => {
                    let columnIndex = 1;
                    Object.keys (record).forEach(columnName => {
                        if (columnIndex === 1 || columnIndex === 9|| columnIndex === 10)
                            ws.cell(rowIndex,columnIndex++)
                            .number(Number(record [columnName])).style({font: {name: 'Times New Roman', size: 11}})
                        else 
                            ws.cell(rowIndex,columnIndex++)
                                .string(record [columnName]).style({font: {name: 'Times New Roman', size: 11}})
                    });
                    rowIndex++;
                });  
                ws.column(1).setWidth(10);
                ws.column(2).setWidth(20);
                ws.column(3).setWidth(50);
                ws.column(4).setWidth(50);
                ws.column(5).setWidth(50);
                ws.column(6).setWidth(30);
                ws.column(7).setWidth(30);
                ws.column(8).setWidth(30);
                ws.column(9).setWidth(50);
                ws.column(10).setWidth(20);
                ws.column(11).setWidth(50);
                wb.write('Report.xlsx', res);  
                // return res.status(HttpStatus.OK).send(dataSuccess('oK', res));
                return res; 
            }
            else return null;
        } catch (e) {
            console.log(e)
            return null;
        }
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Get('/:user_id')
    @ApiOperation({ title: 'Detail User' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'user_id', description: 'User UUID', required: true, type: 'string' })
    async detail(@Param('user_id') user_id, @Res() res: Response) {
        try {
            const result = await this.usersService.findOne(user_id);
            const user = await UserUtil.serialize(result);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', user));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/detail_by_id/:user_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Detail user by id' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad requests' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'user_id', description: 'User ID', required: true, type: 'number' })
    @ApiImplicitQuery({ name: 'role_id', description: 'Role ID (Example: 1: Master Admin, 2:Plant Owner, 3:Project Manager )', required: false, type: 'number' })
    async detailUserById(@Param('user_id') user_id, @Req() request, @Res() res: Response) {
        try {
            const result = await this.usersService.findByIdOptions(user_id, request.query);
            if (!result) {
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Tài khoản không tìm thấy', null));
            }
            delete result.password;
            delete result.user_id;
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/detail_by_email/:email')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Detail user by email' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'email', description: 'User email', required: true, type: 'string' })
    async detailUserByEmail(@Param('email') email, @Res() res: Response) {
        try {
            const result = await this.usersService.findOneByEmail(email);
            delete result.password;
            delete result.user_id;
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('user-register')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiOperation({ title: 'User register' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async userRegister(@Req() request, @Body(CustomValidationPipe) body: CreateUserMobileDto, @Res() res: Response) {
        // check role
        let role = Number(body.role).valueOf();
        if (body.role && isNaN(role))
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Vai trò phải là số', null));

        if (body.role && (role < RolesType.Master_Admin || role > RolesType.Project_Mechanic))
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Role should be 1, 2, 3, 4, 5, 6, 7, 8 or 9', null));

        let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(body.phone, body.country_code);

        if (!data_phone_number) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('SĐiện thoại không đúng định dạng', null));
        }

        body.country_code = data_phone_number.country_code;
        body.phone = (data_phone_number.national_number).toString();
        body.roles = [UserType.CUSTOMER];

        try {
            const user = await this.usersService.createUserMobile(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', user));
        } catch (error) {
            console.log("userRegister - error", error)
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Post('')
    @ApiOperation({ title: 'Admin create user' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createUser(@Req() request, @Body(CustomValidationPipe) body: CreateUserDto, @Res() res: Response) {

        let country_code = body.country_code || config.countryCode;
        let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(body.phone, country_code);

        if (!data_phone_number) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('SĐiện thoại không đúng định dạng', null));
        }

        body.country_code = data_phone_number.country_code;
        body.phone = (data_phone_number.national_number).toString();
        body.roles = !!body.roles ? body.roles : [body.type || UserType.CUSTOMER];

        try {
            const result = await this.usersService.createUser(body, request.user.user_id_str);
            if (result.status) {
                return res.status(HttpStatus.CREATED).send(dataSuccess('Bạn đã tạo tài khoản thành công', result.data));
            } else {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message, null, result.error));
            }
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/driver')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Admin create user driver' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createUserDriver(@Req() request, @Body(CustomValidationPipe) body: CreateUserDriverDto, @Res() res: Response) {
        let country_code = body.country_code || config.countryCode;
        let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(body.phone, country_code);

        if (!data_phone_number) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Số điện thoại sai định dang', null));
        }

        body.country_code = data_phone_number.country_code;
        body.phone = (data_phone_number.national_number).toString();
        body.roles = !!body.roles ? body.roles : [body.type || UserType.DRIVER_BIKE_CAR];
        try {
            const result = await this.usersService.createUser(body, request.user.user_id_str);
            if (result.status) {
                return res.status(HttpStatus.CREATED).send(dataSuccess('Bạn đã tạo tài khoản thành công', result.data));
            } else {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message, null, result.error));
            }
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/provider')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Admin create user provider' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createUserProvider(@Req() request, @Body(CustomValidationPipe) body: CreateUserProviderDto, @Res() res: Response) {
        let country_code = body.country_code || config.countryCode;
        let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(body.phone, country_code);

        if (!data_phone_number) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('SĐiện thoại không đúng định dạng', null));
        }

        body.country_code = data_phone_number.country_code;
        body.phone = (data_phone_number.national_number).toString();
        body.roles = !!body.roles ? body.roles : [body.type || UserType.PROVIDER];

        try {
            const result = await this.usersService.createUser(body, request.user.user_id_str);
            if (result.status) {
                return res.status(HttpStatus.CREATED).send(dataSuccess('Bạn đã tạo tài khoản thành công', result.data));
            } else {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message, null, result.error));
            }
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/customer')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Admin create user customer' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createUserCustomer(@Req() request, @Body(CustomValidationPipe) body: CreateUserCustomerDto, @Res() res: Response) {
        let country_code = body.country_code || config.countryCode;
        let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(body.phone, country_code);

        if (!data_phone_number) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('SĐiện thoại không đúng định dạng', null));
        }

        body.country_code = data_phone_number.country_code;
        body.phone = (data_phone_number.national_number).toString();
        body.roles = !!body.roles ? body.roles : [body.type || UserType.CUSTOMER];

        try {
            const result = await this.usersService.createUser(body, request.user.user_id_str);
            if (result.status) {
                return res.status(HttpStatus.CREATED).send(dataSuccess('Bạn đã tạo tài khoản thành công', result.data));
            } else {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message, null, result.error));
            }
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Post('/administrator')
    @ApiOperation({ title: 'Admin create user administrator' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createUserAdministrator(@Req() request, @Body(CustomValidationPipe) body: CreateUserAdminDto, @Res() res: Response) {
        let country_code = body.country_code || config.countryCode;
        let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(body.phone, country_code);

        if (data_phone_number) {
            body.country_code = data_phone_number.country_code;
            body.phone = (data_phone_number.national_number).toString();
        }
        // body.roles = !!body.roles ? body.roles : [body.type || UserType.ADMIN];
        body.roles = [UserType.ADMIN];

        try {
            const result = await this.usersService.createAdmin(body, request.user.user_id_str);
            if (result.status) {
                return res.status(HttpStatus.CREATED).send(dataSuccess('Bạn đã tạo tài khoản thành công', result.data));
            } else {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message, null, result.error));
            }
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Put('/:user_id')
    @ApiOperation({ title: 'Update user' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiImplicitParam({ name: 'user_id', description: 'User UUID', required: true, type: 'string' })
    async updateUser(@Param('user_id') user_id, @Req() request, @Body() body: UpdateUserDto, @Res() res: Response) {
        let user;
        try {
            user = await this.usersService.findOne(user_id);
            if (!user)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Tài khoản không tồn tại', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let data = _.omit(body, ['id', 'created_at', 'user_id']);
        try {
            user = await this.usersService.update(data, user);
            if (!user)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Cập nhật thất bại', null));
            user = UserUtil.serialize(user);
            delete user.user_roles;
            return res.status(HttpStatus.OK).send(dataSuccess('Cập nhật thành công', user));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:user_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete user roles' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'user_role', description: 'Use roles to delete', required: false, type: 'string' })
    @ApiImplicitParam({ name: 'user_id', description: 'User UUID', required: true, type: 'string' })
    async deleteUserAccount(@Param('user_id') user_id, @Req() request, @Res() res: Response) {
        let user;
        let user_role;
        try {
            user_role = Number(request.query.user_role).valueOf();
            if (isNaN(user_role)) {
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Role không hợp lệ', null));
            }
            user = await this.usersService.findOne(user_id);
            if (!user)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Tài khoản không tìm thấy', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        try {
            let result = await this.usersService.deleteRolesUser(user, user_role);
            if (!result.success) return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Xóa tài khoản thành công', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Xóa tài khoản thành công', result.success));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/delete-account/:user_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete user account' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'user_id', description: 'User UUID', required: true, type: 'string' })
    async deleteAccount(@Param('user_id') user_id, @Req() request, @Res() res: Response) {
        let user;
        try {
            user = await this.usersService.findOne(user_id);
            if (!user)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Tài khoản không tìm thấy', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        try {
            let result = await this.usersService.deleteUser(user);

            return res.status(HttpStatus.OK).send(dataSuccess('Xóa tài khoản thành công', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/block/:user_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Block user account' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'user_id', description: 'User UUID', required: true, type: 'string' })
    async blockUserAccount(@Param('user_id') user_id: string, @Req() request, @Res() res: Response) {
        let user;
        try {
            user = await this.usersService.findOne(user_id);
            if (!user)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Tài khoản không tìm thấy', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        try {
            let result = await this.usersService.blockUserAccount(user, true);

            return res.status(HttpStatus.OK).send(dataSuccess('Tài khoản đã khóa thành công', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/unblock/:user_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'UnBlock user account' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'user_id', description: 'User UUID', required: true, type: 'string' })
    async unBlockUserAccount(@Param('user_id') user_id, @Req() request, @Res() res: Response) {
        let user;
        try {
            user = await this.usersService.findOne(user_id);
            if (!user)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Tài khoản không tìm thấy', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        try {
            let result = await this.usersService.blockUserAccount(user, false);

            return res.status(HttpStatus.OK).send(dataSuccess('Đã mở khoản tài khoản thành công', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @Post('/forgot-password/')
    @ApiOperation({ title: 'Forgot password send OTP with phone' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async forgotPassword(@Req() request, @Body() body: ForgotPasswordSendOTPDto, @Res() res: Response) {
        let country_code = body.country_code || config.countryCode;
        // check phone
        let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(body.phone, country_code);

        if (!data_phone_number) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Số điện thoại không đúng định dạng', null));
        }

        body.country_code = data_phone_number.country_code;
        body.phone = (data_phone_number.national_number).toString();

        try {
            let result = await this.usersService.forgotPasswordSendOTP(body);

            return res.status(HttpStatus.OK).send(dataSuccess('Đã gửi mã OTP thành công', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/validate-forgot-password/')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiOperation({ title: 'Validate forgot password' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async validateForgotPassword(@Req() request, @Body() body: ValidateOTPForgotPasswordDto, @Res() res: Response) {
        let country_code = body.country_code || config.countryCode;
        // check phone
        let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(body.phone, country_code);

        if (!data_phone_number) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Số điện thoại không đúng định dạng', null));
        }

        body.country_code = data_phone_number.country_code;
        body.phone = (data_phone_number.national_number).toString();

        try {
            let result = await this.usersService.validateForgotPassword(body);

            return res.status(HttpStatus.OK).send(dataSuccess('Thay đổi mật khẩu thành công', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/change-password/:user_id')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Change password user account' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'user_id', description: 'User UUID', required: true, type: 'string' })
    async changePasswordUser(@Param('user_id') user_id, @Body() body: ChangePasswordUserDto, @Req() request, @Res() res: Response) {
        let user;
        try {
            user = await this.usersService.findOne(user_id);
            if (!user)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Tài khoản không tìm thấy', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        if (!user.checkIfUnencryptedPasswordIsValid(body.password_old))
            return res.status(HttpStatus.UNAUTHORIZED).send(dataError('Mật khẩu cũ không đúng', null));

        try {
            let result = await this.usersService.changePassword(user, body);

            return res.status(HttpStatus.OK).send(dataSuccess('Thay đổi mật khẩu thành công', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/check-user-by-phone/:phone')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Check user by phone' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'phone', description: 'Phone number', required: true, type: 'string' })
    async checkUserByPhone(@Param('phone') phone, @Req() request, @Res() res: Response) {
        try {
            let country_code = config.countryCode;
            let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(phone, country_code);

            if (!data_phone_number) {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Số điện thoại không đúng định dạng', null));
            }

            let result = await this.usersService.findOneByPhone((data_phone_number.national_number).toString());

            if (!result) {
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Tài khoản không tìm thấy', null));
            }
            return res.status(HttpStatus.OK).send(dataSuccess('Tài khoản đã tồn tại', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/send-otp')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @ApiOperation({ title: 'Send OTP' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async sendOTP(@Req() request, @Body(CustomValidationPipe) body: SendOTPDto, @Res() res: Response) {
        let country_code = body.country_code || config.countryCode;

        let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(body.phone, country_code);

        if (!data_phone_number) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Số điện thoại không đúng định dạng', null));
        }

        body.country_code = data_phone_number.country_code;
        body.phone = (data_phone_number.national_number).toString();

        try {
            let result = await this.usersService.sendOTP(body, false);

            return res.status(HttpStatus.OK).send(dataSuccess('Đã gửi mã OTP thành công', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @Post('/validate-otp')
    @ApiOperation({ title: 'Validate OTP' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async validateOTP(@Req() request, @Body(CustomValidationPipe) body: ValidateOTPDto, @Res() res: Response) {
        let country_code = body.country_code || config.countryCode;

        let data_phone_number = PhoneNumberUtil.getCountryCodeAndNationalNumber(body.phone, country_code);

        if (!data_phone_number) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Số điện thoại không đúng định dạng', null));
        }

        body.country_code = data_phone_number.country_code;
        body.phone = (data_phone_number.national_number).toString();

        try {
            let result = await this.usersService.validateOTP(body);

            return res.status(HttpStatus.OK).send(dataSuccess('Mã OTP họp lệ', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
