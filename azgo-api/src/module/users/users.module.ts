import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { UsersEntity } from './users.entity';
import { RolesModule } from '../roles/roles.module';
import { UsersRepository } from './users.repository';
import { AdminProfileModule } from '../admin-profile/admin-profile.module';
import { AccountsModule } from '../accounts/accounts.module';
import { DriverProfileModule } from '../driver-profile/driver-profile.module';
import { CustomerProfileModule } from '../customer-profile/customer-profile.module';
import { UserProfileModule } from '../user-profile/user-profile.module';
import { ProvidersInfoModule } from '../providers-info/providers-info.module';
import { VehicleModule } from '../vehicle/vehicle.module';
import { OtpModule } from '@azgo-module/core/dist/common/otp/otp.module';
import { VehicleRepository } from '../vehicle/vehicle.repository';
import { VehicleDriverRepository } from '../vehicle-driver/vehicle-driver.repository';
import { AdminProfileRepository } from '../admin-profile/admin-profile.repository';
import { DriverProfileRepository } from '../driver-profile/driver-profile.repository';
import { CustomerProfileRepository } from '../customer-profile/customer-profile.repository';
import { ProvidersInfoRepository } from '../providers-info/providers-info.repository';
import { AccountsRepository } from '../accounts/accounts.repository';
import { RatingDriverModule } from '../rating-driver/rating-driver.module';
import { VehicleDriverModule } from '../vehicle-driver/vehicle-driver.module';
import { RatingCustomerModule } from '../rating-customer/rating-customer.module';
import { UploadAWSModule } from '../upload-aws/upload-aws.module';
import { WalletModule } from '../wallet/wallet.module';
import { WalletRepository } from '../wallet/wallet.repository';
@Module({
    imports: [
        TypeOrmModule.forFeature([
            UsersRepository,
            VehicleRepository,
            VehicleDriverRepository,
            AdminProfileRepository,
            DriverProfileRepository,
            CustomerProfileRepository,
            ProvidersInfoRepository,
            AccountsRepository,
            WalletRepository
        ]),
        RolesModule,
        AdminProfileModule, 
        DriverProfileModule,
        CustomerProfileModule,
        UserProfileModule,
        ProvidersInfoModule,
        VehicleModule,
        UploadAWSModule,
        OtpModule,
        forwardRef(() => WalletModule),
        forwardRef(() => AccountsModule),
        forwardRef(() => RatingDriverModule),
        forwardRef(() => RatingCustomerModule),
        VehicleDriverModule,

    ],
    exports: [
        UsersService,
    ],
    providers: [UsersService],
    controllers: [UsersController],
})
export class UsersModule { }
