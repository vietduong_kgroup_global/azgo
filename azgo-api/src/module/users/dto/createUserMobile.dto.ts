import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '@azgo-module/core/dist/utils/blank.util';

export class CreateUserMobileDto {

    @ApiModelProperty({ example: '01229125354' })
    phone: string;

    @ApiModelProperty({ example: 84 })
    country_code: number;

    @IsNotEmpty({ message: 'Password is require' })
    @IsNotBlank('password', { message: 'Is not white space' })
    @ApiModelProperty({ format: 'password', example: '123456' })
    password: string;

    @IsInt()
    @IsNotEmpty({ message: 'Type is require' })
    @ApiModelProperty({ example: 2, description: '2:driver, 3:customer, 5:driver-bike-car, 6:driver-van-bagac', required: true })
    type: number;

    @IsInt()
    @IsNotEmpty({ message: 'Platform is require' })
    @ApiModelProperty({ example: '1:Web, 2:Customer App, 3:Driver App', required: true })
    platform: number;

    @ApiModelProperty({ example: '2030110833758824', required: false })
    fb_id: string;

    @ApiModelProperty({ example: '106118625832609922317', required: false })
    gg_id: string;

    @ApiModelProperty({ example: '2: Customer, 3: Driver'  })
    role: number;

    @ApiModelProperty({ example: '106118625832609922317', required: false })
    token: string;

    @ApiModelProperty({ example: [3]  })
    roles: object;
}
