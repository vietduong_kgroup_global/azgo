import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '@azgo-module/core/dist/utils/blank.util';

export class CreateUserCustomerDto {

    @ApiModelProperty({ example: 'admin@gmail.com', required: false })
    email: string;

    @ApiModelProperty({ example: '01229125354' })
    phone: string;

    @ApiModelProperty({ example: 84 })
    country_code: number;

    @ApiModelProperty({ example: 'Alex', required: false })
    first_name: string;

    @ApiModelProperty({ example: 'John', required: false })
    last_name: string;

    @ApiModelProperty({ example: '0: other, 1: male, 2: female', required: false  })
    gender: number;

    @IsNotEmpty({ message: 'Password is require' })
    @IsNotBlank('password', { message: 'Is not white space' })
    @ApiModelProperty({ format: 'password', example: '123456' })
    password: string;

    @ApiModelProperty({ example: '1991-01-01', required: false })
    birthday: string;

    @ApiModelProperty({ example: '123456789', required: false })
    cmnd: string;

    @ApiModelProperty({ example: '46 Bach Dang', required: false })
    address: string;

    @ApiModelProperty({ example: {file_path: 'assets/images', file_name: '1583745101578.jpg'}, required: false })
    image_profile: object;

    @ApiModelProperty({ example: {file_path: 'assets/images', file_name: '1583745101578.jpg'}, required: false })
    front_of_cmnd: object;

    @ApiModelProperty({ example: {file_path: 'assets/images', file_name: '1583745101578.jpg'}, required: false })
    back_of_cmnd: object;

    @IsInt()
    @IsNotEmpty({ message: 'Type is require' })
    @ApiModelProperty({ example: 3, required: true })
    type: number;

    @IsInt()
    @IsNotEmpty({ message: 'Platform is require' })
    @ApiModelProperty({ example: 2, required: true })
    platform: number;

    @ApiModelProperty({ example: '1: Admin'  })
    role: number;

    @ApiModelProperty({ example: [3]  })
    roles: object;
}
