import { ApiModelProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty } from 'class-validator';

export class UpdateUserDto {

    @ApiModelProperty({ example: 'admin@gmail.com' })
    email: string;

    @IsNotEmpty({ message: 'Phone is require' })
    @ApiModelProperty({ example: '975716503', required: true })
    phone: string;

    @IsNotEmpty({ message: 'Country code is require' })
    @ApiModelProperty({ example: 84 })
    country_code: number;

    @ApiModelProperty({ example: 'Alex', required: false })
    first_name: string;

    @ApiModelProperty({ example: 'John', required: false })
    last_name: string;

    @ApiModelProperty({ example: '0: other, 1: male, 2: female', required: false  })
    gender: number;

    @IsInt()
    @IsNotEmpty({ message: 'Platform is require' })
    @ApiModelProperty({ example: '1:Web', required: true })
    platform: number;

    @ApiModelProperty({ example: '26/3/1999', required: false })
    birthday: string;

    @ApiModelProperty({ example: '123456789', required: false })
    cmnd: string;

    @ApiModelProperty({ example: '46 Bach Dang', required: false })
    address: string;

    @ApiModelProperty({ example: {file_path: 'assets/images', file_name: '1583745101578.jpg'}, required: false })
    image_profile: object;

    @ApiModelProperty({ example: {file_path: 'assets/images', file_name: '1583745101578.jpg'}, required: false })
    front_of_cmnd: object;

    @ApiModelProperty({ example: {file_path: 'assets/images', file_name: '1583745101578.jpg'}, required: false })
    back_of_cmnd: object;

    @ApiModelProperty({ example: {file_path: 'assets/images', file_name: '1583745101578.jpg'}})
    front_driving_license: object;

    @ApiModelProperty({ example: {file_path: 'assets/images', file_name: '1583745101578.jpg'}})
    back_driving_license: object;

    @ApiModelProperty({ example: 'Phương Trang', required: false })
    provider_name: string;

    @ApiModelProperty({ example: 'Steve', required: false })
    representative: string;

    @ApiModelProperty({ example: '0837760601', required: false })
    phone_representative: string;

    @ApiModelProperty({ example: '2030110833758824', required: false })
    fb_id: string;

    @ApiModelProperty({ example: '106118625832609922317', required: false })
    gg_id: string;

    @ApiModelProperty({ example: '[2, 7]'  })
    roles: object;

    @IsInt()
    @IsNotEmpty({ message: 'Type is require' })
    @ApiModelProperty({ example: '2:driver-bus, 5:driver-bike-car, 6:driver-van-bagac', required: true })
    type: number;
}
