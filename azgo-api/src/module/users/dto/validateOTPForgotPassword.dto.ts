import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsNotBlank } from '@azgo-module/core/dist/utils/blank.util';

export class ValidateOTPForgotPasswordDto {

    @IsNotEmpty({ message: 'Phone is require' })
    @IsNotBlank('phone', { message: 'Is not white space' })
    @ApiModelProperty({ example: '0975716503', required: true })
    phone: string;

    @ApiModelProperty({ example: 84, required: false })
    country_code: number;

    @IsNotEmpty({ message: 'Password is require' })
    @IsNotBlank('password', { message: 'Is not white space' })
    @ApiModelProperty({ format: 'password', example: '123456' })
    password: string;

    @IsNotEmpty({ message: 'Password confirm is require' })
    @IsNotBlank('password', { message: 'Is not white space' })
    @ApiModelProperty({ format: 'password', example: '123456' })
    password_confirm: string;

    @IsNotEmpty({ message: 'OTP is require' })
    @ApiModelProperty({ example: '1234', required: true })
    otp: string;

    @IsNotBlank('token', { message: 'Is not white space' })
    @IsNotEmpty({ message: 'Token is require' })
    @ApiModelProperty({ example: '1234', required: true })
    token: string;
}
