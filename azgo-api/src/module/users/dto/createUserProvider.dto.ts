import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '@azgo-module/core/dist/utils/blank.util';

export class CreateUserProviderDto {

    @ApiModelProperty({ example: 'admin@gmail.com', required: false })
    email: string;

    @ApiModelProperty({ example: '01229125354' })
    phone: string;

    @ApiModelProperty({ example: 84 })
    country_code: number;

    @IsNotEmpty({ message: 'Password is require' })
    @IsNotBlank('password', { message: 'Is not white space' })
    @ApiModelProperty({ format: 'password', example: '123456' })
    password: string;

    @ApiModelProperty({ example: '46 Bach Dang', required: false })
    address: string;

    @ApiModelProperty({ example: 'Phương Trang', required: false })
    provider_name: string;

    @ApiModelProperty({ example: 'Steve', required: false })
    representative: string;

    @ApiModelProperty({ example: '0837760601', required: false })
    phone_representative: string;

    @IsInt()
    @IsNotEmpty({ message: 'Type is require' })
    @ApiModelProperty({ example: 4, required: true })
    type: number;

    @IsInt()
    @IsNotEmpty({ message: 'Platform is require' })
    @ApiModelProperty({ example: 1, required: true })
    platform: number;

    @ApiModelProperty({ example: '1: Admin'  })
    role: number;

    @ApiModelProperty({ example: {file_path: 'assets/images', file_name: '1583745101578.jpg'}, required: false })
    image_profile: object;

    @ApiModelProperty({ example: [4]  })
    roles: object;
}
