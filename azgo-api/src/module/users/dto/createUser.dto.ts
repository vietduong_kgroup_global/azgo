import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '@azgo-module/core/dist/utils/blank.util';
import {ImageDto} from '../../../general-dtos/image.dto';

export class CreateUserDto {

    @ApiModelProperty({ example: 'admin@gmail.com', required: false })
    email: string;

    @ApiModelProperty({ example: '357739234' })
    phone: string;

    @ApiModelProperty({ example: 84 })
    country_code: number;

    @ApiModelProperty({ example: 'Alex', required: false })
    first_name: string;

    @ApiModelProperty({ example: 'John', required: false })
    last_name: string;

    @ApiModelProperty({ example: '0: other, 1: male, 2: female', required: false  })
    gender: number;

    @IsNotEmpty({ message: 'Password is require' })
    @IsNotBlank('password', { message: 'Is not white space' })
    @ApiModelProperty({ format: 'password', example: '123456' })
    password: string;

    @ApiModelProperty({ example: '31/12/2000', required: false })
    birthday: string;

    @ApiModelProperty({ example: '123456789', required: false })
    cmnd: string;

    @ApiModelProperty({ example: '46 Bach Dang', required: false })
    address: string;

    @ApiModelProperty({ type: ImageDto, required: false })
    image_profile: ImageDto;

    @ApiModelProperty({ type: ImageDto, required: false })
    front_of_cmnd: ImageDto;

    @ApiModelProperty({ type: ImageDto, required: false })
    back_of_cmnd: ImageDto;

    @ApiModelProperty({ example: 'Phương Trang', required: false })
    provider_name: string;

    @ApiModelProperty({ example: 'Steve', required: false })
    representative: string;

    @ApiModelProperty({ example: '0837760601', required: false })
    phone_representative: string;

    @IsInt()
    @IsNotEmpty({ message: 'Type is require' })
    @ApiModelProperty({ example: '1:Admin, 2:driver-bus, 3:customer, 4:provider, 5:driver-bike-car, 6:driver-van-bagac', required: true })
    type: number;

    @IsInt()
    @IsNotEmpty({ message: 'Platform is require' })
    @ApiModelProperty({ example: '1:Web, 2:Customer App, 3:Driver App', required: true })
    platform: number;

    @ApiModelProperty({ example: '2030110833758824', required: false })
    fb_id: string;

    @ApiModelProperty({ example: '106118625832609922317', required: false })
    gg_id: string;

    @ApiModelProperty({ example: '1: Admin'  })
    role: number;

    @ApiModelProperty({ example: [1]  })
    roles: object;
}
