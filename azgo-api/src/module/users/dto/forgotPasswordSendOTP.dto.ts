import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsNotBlank } from '@azgo-module/core/dist/utils/blank.util';

export class ForgotPasswordSendOTPDto {

    @IsNotEmpty({ message: 'Phone is require' })
    @IsNotBlank('phone', { message: 'Is not white space' })
    @ApiModelProperty({ example: '0975716503', required: true })
    phone: string;

    @ApiModelProperty({ example: 84, required: false })
    country_code: number;
}
