import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { IsNotBlank } from '@azgo-module/core/dist/utils/blank.util';

export class SendOTPDto {

    @IsNotEmpty({ message: 'Phone is require' })
    @IsNotBlank('phone', { message: 'Is not white space' })
    @ApiModelProperty({ example: '0975716503', required: true })
    phone: string;

    @ApiModelProperty({ example: 84, required: false })
    country_code: number;

    @IsInt()
    @IsNotEmpty({ message: 'Platform is require' })
    @ApiModelProperty({ example: '1:Web', required: true })
    platform: number;
}
