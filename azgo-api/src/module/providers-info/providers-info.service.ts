import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProvidersInfoRepository } from './providers-info.repository';
import { PaginateType } from '@azgo-module/core/dist/utils/enum.ultis';
import { ProvidersInfoEntity } from './providers-info.entity';

@Injectable()
export class ProvidersInfoService {
    constructor(
        @InjectRepository(ProvidersInfoRepository)
        private readonly providersInfoRepository: ProvidersInfoRepository,
    ) { }

    /**
     * This function get list providers info by limit & offset
     * @param limit
     * @param offset
     * @param options
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        options.limit = limit;
        options.offset = offset;
        return await this.providersInfoRepository.getAll(PaginateType.PAGE, options);
    }

    async getAll(options?: any) {
        let provider_name = options.provider_name ? options.provider_name.trim() : null;
        let query = await this.providersInfoRepository.createQueryBuilder('az_providers_info');
        // query not delete
        query.where('deleted_at is null');
        if (provider_name) {
            query.andWhere(`az_providers_info.provider_name like "%${provider_name}%"`);
        }
        return query.getMany();
    }

    /**
     * This is function get providers info by user_id
     * @param user_id
     */
    async findByUserId(user_id: number): Promise<ProvidersInfoEntity> {
        return await this.providersInfoRepository.findByUserId(user_id);
    }

    /**
     * This is function get provider info by id
     * @param id
     */
    async findById(id): Promise<ProvidersInfoEntity> {
        return await this.providersInfoRepository.findOne(id);
    }
}
