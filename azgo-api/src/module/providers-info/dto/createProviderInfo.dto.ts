import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, MaxLength } from 'class-validator';
import { IsNotBlank } from '@azgo-module/core/dist/utils/blank.util';

export class CreateProviderInfoDto {

    @MaxLength(255, { message: 'You have reached your maximum limit of characters allowed' })
    @IsNotBlank('expire_date', { message: 'Is not white space' })
    @IsNotEmpty({ message: 'Name is required' })
    @ApiModelProperty({ example: 'Nha xe Thanh Buoi', required: true })
    name: string;

    @IsNotBlank('expire_date', { message: 'Is not white space' })
    @IsNotEmpty({ message: 'Phone is required' })
    @ApiModelProperty({ example: '01229125354', required: true })
    phone: string;

    @ApiModelProperty({ example: 84, required: false })
    country_code: number;

    @ApiModelProperty({ example: 1, required: false })
    status: number;

    @ApiModelProperty({ example: 1, required: false })
    vehicle_quantity: number;

    @IsNumber()
    @IsNotEmpty({ message: 'User id is required' })
    @ApiModelProperty({ example: 1, required: true })
    user_id: number;
}
