import { EntityRepository, Repository } from 'typeorm';
import { ProvidersInfoEntity } from './providers-info.entity';
import { PaginateType } from '@azgo-module/core/dist/utils/enum.ultis';

@EntityRepository(ProvidersInfoEntity)
export class ProvidersInfoRepository extends Repository<ProvidersInfoEntity> {
    /**
     * This is function get all providers
     * @param paginate_type
     * @param options
     */
    async getAll(paginate_type: number, options?: any) {
        let is_load_more = false;
        let query_limit = !isNaN(Number(options.limit).valueOf()) ? Number(options.limit).valueOf() : 10;

        let query = this.createQueryBuilder('az_providers_info');

        if (paginate_type === PaginateType.PAGE) {
            // limit + offset
            query.orderBy('id', 'DESC')
                .limit(options.limit)
                .offset(options.offset);

            const results = await query.getManyAndCount();

            return {
                success: true,
                data: results[0],
                total: results[1],
            };
        } else if (paginate_type === PaginateType.LOAD_MORE) {
            // count list providers info
            let count_query = query.getCount();

            // pagination
            if (options.last_id)
                query.andWhere('id < :last_id', { last_id: options.last_id });

            // list providers info
            let results_query = query.orderBy('id', 'DESC')
                .take(query_limit + 1)
                .getMany();

            let results = await Promise.all([
                count_query,
                results_query,
            ]);

            if (results[1].length > query_limit) {
                is_load_more = true;
                results[1].pop();
            }

            return {
                count: results[0],
                users: results[1],
                load_more: is_load_more,
            };
        } else {
            return query.getMany();
        }
    }

    /**
     * This is function find by user id
     * @param user_id
     */
    async findByUserId(user_id: number): Promise<ProvidersInfoEntity> {
        return await this.createQueryBuilder('az_providers_info')
            .where('user_id = :user_id', { user_id })
            .andWhere('deleted_at is null')
            .getOne();
    }
}
