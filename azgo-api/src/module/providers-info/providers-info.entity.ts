import {Entity, Column, PrimaryGeneratedColumn, OneToMany, OneToOne, JoinColumn} from 'typeorm';
import { Exclude } from 'class-transformer';
import { UsersEntity } from '../users/users.entity';
import { VehicleEntity } from '../vehicle/vehicle.entity';
import {VehicleTypesEntity} from '../vehicle-types/vehicle-types.entity';
import {SeatPatternEntity} from '../seat-pattern/seat-pattern.entity';
import {RoutesEntity} from '../routes/routes.entity';
import {VehicleScheduleEntity} from '../vehicle-schedule/vehicleSchedule.entity';

@Entity('az_providers_info')
export class ProvidersInfoEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '255',
        nullable: true,
    })
    provider_name: string;

    @Column({
        type: 'varchar',
        length: '255',
        nullable: true,
    })
    representative: string;

    @Column({
        type: 'varchar',
        length: '255',
        nullable: true,
    })
    phone_representative: string;

    @Column({
        type: 'smallint',
        default: 1,
    })
    gender: number;

    @Column({
        nullable: true,
    })
    birthday: Date;

    @Column({
        type: 'int',
        nullable: true,
    })
    vehicle_quantity: number;

    @Column({
        type: 'varchar',
        length: '50',
        nullable: true,
    })
    cmnd: string;

    @Column({
        type: 'text',
        nullable: true,
    })
    address: string;

    @Column({
        type: 'json',
        nullable: true,
    })
    image_profile: object;

    @Column({
        type: 'json',
        nullable: true,
    })
    front_of_cmnd: object;

    @Column({
        type: 'json',
        nullable: true,
    })
    back_of_cmnd: object;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
    })
    deleted_at: Date;

    @OneToOne(type => UsersEntity, user => user.providersInfo)
    @JoinColumn({
        name: 'user_id',
    })
    users: UsersEntity;

    @OneToMany(type => SeatPatternEntity, seat_patterns => seat_patterns.provider)
    seat_patterns: SeatPatternEntity[];

    @OneToMany(type => VehicleEntity, vehicles => vehicles.provider)
    vehicles: VehicleEntity[];

    @OneToMany(type => RoutesEntity, routes => routes.provider)
    routes: RoutesEntity[];

    @OneToMany(type => VehicleScheduleEntity, vehicle_schedule => vehicle_schedule.provider)
    vehicleSchedule: VehicleScheduleEntity[];

    @OneToMany(type => VehicleTypesEntity, vehicleType => vehicleType.providersInfo)
    vehicleTypes: VehicleTypesEntity[];
}
