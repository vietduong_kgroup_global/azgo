import { Module } from '@nestjs/common';
import { ProvidersInfoController } from './providers-info.controller';
import { ProvidersInfoService } from './providers-info.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProvidersInfoRepository } from './providers-info.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([ProvidersInfoRepository]),
  ],
  exports: [
    ProvidersInfoService,
  ],
  controllers: [ProvidersInfoController],
  providers: [ProvidersInfoService],
})
export class ProvidersInfoModule {}
