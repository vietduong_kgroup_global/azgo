import { Controller, Get, HttpStatus, Req, Res, UseGuards } from '@nestjs/common';
import { ProvidersInfoService } from './providers-info.service';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import {
    ApiBearerAuth,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey  } from '@azgo-module/core/dist/common/guards/roles.guards';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@ApiUseTags('Providers')
@Controller('providers')
export class ProvidersInfoController {
    constructor(
        private readonly providersInfoService: ProvidersInfoService,
    ) { }

    @Get()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get Providers' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'provider_name', description: 'Provider name', required: false, type: 'string'})
    async detail(@Req() request, @Res() res: Response) {
        try {
            const result = await this.providersInfoService.getAll(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
