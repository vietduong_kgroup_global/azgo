import { Injectable } from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {RoutesEntity} from '../routes/routes.entity';
import {PromotionRepository} from './promotion.repository';
import {CreatePromotionDto} from './dto/create-promotion.dto';
import {
    ERROR_TOUR_ORDER_DRIVER_DONT_HAVE_ENOUGHT_FEE_WALLET,
    ERROR_UNKNOWN,
} from '../../constants/error.const';

@Injectable()
export class PromotionsService {
    constructor(
        @InjectRepository(PromotionRepository)
        private readonly promotionRepository: PromotionRepository,
    ) {}

    async findAll(limit: number, offset: number, options?: any){
        return await this.promotionRepository.getAll(limit, offset, options);
    }

    async findOne(id: number){
        return await this.promotionRepository.findOne({id });
    }

    async findOneByUuid(uuid: string){
        return await this.promotionRepository.findOne({ uuid });
    }

    async findOneByCoupon(coupon: string){
        return await this.promotionRepository.findOne({coupon});
    }

    async checkValidateByCoupon(coupon: string){
        return await this.promotionRepository.findCouponValidate(coupon);
    }

    async applyCounpon(coupon: string){
        try {
            let promotion = await this.promotionRepository.findOne({coupon});
            promotion.count -= 1;
            this.promotionRepository.save(promotion);
            return {
                status: true,
                data: null,
                error: null,
                message: null,
            };
        } catch (error) {
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message || 'Update promotion false'
            };
        }
    }

    async removeCounpon(coupon: string){
        try {
            let promotion = await this.promotionRepository.findOne({coupon});
            promotion.count += 1;
            this.promotionRepository.save(promotion);
            return {
                status: true,
                data: null,
                error: null,
                message: null,
            };
        } catch (error) {
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message || 'Update promotion false'
            };
        }
    }

    async delete(id: number){
        const obj = await this.promotionRepository.findOne({id });
        if (!obj)
            return false;
        obj.deleted_at = new Date();
        return await this.promotionRepository.save(obj);
    }
    async create(body: CreatePromotionDto[]) {
        try {
            const route = new RoutesEntity();
            const dataSave = body.map(item => { 
                return {...route, ...item}
            });
            return await this.promotionRepository.save(dataSave);
        } catch (error) {
            console.log(error)
            return error
        }
    }
    async update(id: number, body?: any) {
        const route = await this.promotionRepository.findOne({id});
        if (!route) throw { message: 'Promotion not found', status: 404 };
        const dataSave = {...route, ...body};
        return await this.promotionRepository.save(dataSave);
    }
}
