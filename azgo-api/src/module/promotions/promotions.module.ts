import { Module } from '@nestjs/common';
import { PromotionsService } from './promotions.service';
import { PromotionsController } from './promotions.controller';
import {TypeOrmModule} from '@nestjs/typeorm';
import {PromotionRepository} from './promotion.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([PromotionRepository]),
  ],
  providers: [PromotionsService],
  controllers: [PromotionsController],
})
export class PromotionsModule {}
