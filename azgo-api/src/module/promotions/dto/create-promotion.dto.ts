import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsString, MaxLength, IsNumber} from 'class-validator';

export class CreatePromotionDto {
    @IsNotEmpty({message: 'Coupon is require'})
    @ApiModelProperty({example: 'Phiếu mua hàng', required: false})
    @IsString()
    @MaxLength(255, {message: 'Chuỗi không quá 255 ký tự'})
    coupon: string;

    @ApiModelProperty({example: 1234, required: false})
    @IsNumber()
    percent_discount: number;

    @ApiModelProperty({example: 1234, required: false})
    @IsNumber()
    price_discount: number;
}
