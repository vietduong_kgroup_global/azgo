import {Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Req, Res, UseGuards} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { AuthGuard } from '@nestjs/passport';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import { PromotionsService } from './promotions.service';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { Response } from 'express';
import * as jwt from 'jsonwebtoken';
import { ERROR_COUPON_QUANTITY_CANNOT_EMPTY } from '../../constants/error.const';

@ApiUseTags('Promotions')
@Controller('promotions')
export class PromotionsController {
    constructor(
        private readonly promotionService: PromotionsService,
    ) {
    }

    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Add new promotion' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    async create(@Req() request, @Body() body: any, @Res() res: Response) {
        try {
            let result;
            if(body.quantity){
                let arrCoupon = []
                for (let index = 1; index <= body.quantity; index++) {
                    let element = {...body, coupon: `${Math.round(new Date().getTime()).toString(16).toLocaleUpperCase()}${index}`};
                    element.coupon = element.coupon.toLowerCase();
                    arrCoupon.push(element)
                }
                result = await this.promotionService.create(arrCoupon);
            }
            else if(body.coupon){
                body.coupon = body.coupon.toLowerCase();
                body.coupon = body.coupon.trim();
                result = await this.promotionService.create([body]);
            }
            else{
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Mã khuyến mãi và số lượng không thể cùng thiếu', null, ERROR_COUPON_QUANTITY_CANNOT_EMPTY));
            }
            return res.status(HttpStatus.OK).send(dataSuccess('Create success', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:uuid')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update promotion' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiImplicitParam({ name: 'uuid', description: 'Promotion uuid', required: true, type: 'number' })
    async update(@Req() request, @Body() body: CreatePromotionDto, @Res() res: Response) {
        try {
            const promotion = await this.promotionService.findOneByUuid(request.params.uuid);
            if(!promotion)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Promotion not found', null));
            body.coupon = body.coupon.toLowerCase();
            body.coupon = body.coupon.trim();
            const result = await this.promotionService.update(promotion.id, body);
            return res.status(HttpStatus.OK).send(dataSuccess('Update success', result));
        }catch (error) {
            console.log('update: ', error)
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get promotions' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiImplicitQuery({ name: 'company', description: 'Filter by company name', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    async getRoutes(@Req() request, @Res() res: Response) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.promotionService.findAll(per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    @Get('/get-list-for-company')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get promotions' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    async getListForCompany(@Req() request, @Res() res: Response) {
        let results;
        const token = request.headers.authorization.split(' ')[1];
        const userInfo: any =  jwt.decode(token);
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            request.query.company = userInfo.company;
            console.log(request.query)
            results = await this.promotionService.findAll(per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    @Get('/:uuid')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Detail promotion' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'uuid', description: 'Promotion uuid', required: true, type: 'number' })
    async detail(@Param('uuid') uuid, @Res() res: Response) {
        try {
            const result = await this.promotionService.findOneByUuid(uuid);
            if (!result)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Promotion not found', null));
            return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:uuid')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete promotion' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'uuid', description: 'Promotion Id', required: true, type: 'number' })
    async deleteRoute(@Param('uuid') uuid, @Res() res: Response) {
        try {
            const promotion = await this.promotionService.findOneByUuid(uuid);
            if(!promotion)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Promotion not found', null));
            const result = await this.promotionService.delete(promotion.id);
            if (result)
                return res.status(HttpStatus.OK).send(dataSuccess('Delete success', result));
            return res.status(HttpStatus.OK).send(dataError('Delete error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
