import {Column, Entity, Generated,  OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {IsDecimal, IsNotEmpty, MaxLength} from 'class-validator';
import {VehicleGroupsEntity} from '../vehicle-groups/vehicle-groups.entity';
import {ApiModelProperty} from '@nestjs/swagger';

@Entity('az_promotions')
export class PromotionsEntity {

    @Exclude()
    @PrimaryGeneratedColumn({ type: 'bigint', unsigned: true })
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    @Generated('uuid')
    @IsNotEmpty()
    uuid: string;

    @Column('varchar')
    @MaxLength(255)
    @IsNotEmpty({message: 'Coupon is require'})
    @ApiModelProperty({example: 'Phiếu mua hàng', required: false})
    coupon: string;

    @Column('float')
    @IsDecimal()
    @ApiModelProperty({example: 1234, required: false})
    percent_discount: number;

    @Column('float')
    @IsDecimal()
    @ApiModelProperty({example: 1234, required: false})
    price_discount: number;

    @Column('int')
    @IsDecimal()
    @ApiModelProperty({example: 1, required: false})
    count: number;

    @Column('varchar')
    @MaxLength(255)
    @ApiModelProperty({example: 'Tên công ty', required: false})
    company: string;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @OneToMany(type => VehicleGroupsEntity, vehicleGroup => vehicleGroup.promotion)
    vehicleGroups: VehicleGroupsEntity[];

}
