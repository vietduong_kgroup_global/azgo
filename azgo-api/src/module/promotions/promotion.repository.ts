import {Brackets, EntityRepository, Repository} from 'typeorm';
import {PromotionsEntity} from './promotions.entity';

@EntityRepository(PromotionsEntity)
export class PromotionRepository extends Repository<PromotionsEntity>{
    async getAll(limit: number, offset: number, options?: any){
        // create new query
        let query = this.createQueryBuilder('promotion');
        // search by keyword
        if (options.keyword && options.keyword.trim()) {
            query.andWhere(new Brackets(qb => {
                qb.where('promotion.coupon like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('promotion.percent_discount like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('promotion.price_discount like :keyword', { keyword: '%' + options.keyword.trim() + '%' });
            }));
        }
        if(options.company){
            query.andWhere('promotion.company = :company', {company: options.company})
        }
        query.andWhere('promotion.deleted_at is null')
            .orderBy('promotion.id', 'DESC')
            .take(limit)
            .skip(offset);

        const results = await query.getManyAndCount();
        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }
    async findCouponValidate(coupon){
        // create new query
        let query = this.createQueryBuilder('az_promotions');
        query.andWhere('az_promotions.coupon = :coupon', { coupon: coupon });
        query.andWhere('az_promotions.count > 0');
        query.andWhere('az_promotions.deleted_at is null');
        return await query.getOne();
    }
}
