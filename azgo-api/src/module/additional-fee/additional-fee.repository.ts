import { Brackets, EntityRepository, Repository } from 'typeorm';
import { AdditionalFeeEntity } from './additional-fee.entity';

@EntityRepository(AdditionalFeeEntity)
export class AdditionalFeeRepository extends Repository<AdditionalFeeEntity> {
    /**
     * This function get list fee by limit & offset
     * @param limit
     * @param offset
     * @param options
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        let query = this.createQueryBuilder('az_additional_fee');
        query.leftJoinAndSelect('az_additional_fee.areaInfo', 'areaInfo');
        // search by keyword
        if (options.keyword && options.keyword.trim()) {
            query.orWhere(`az_additional_fee.name like :name`, { name: '%' + options.keyword.trim() + '%' });
        }
         // Filter by type tour
        if (!!options.type) {
            query.andWhere(`az_additional_fee.type = :type`, { type: options.type });
        }
        
        // not delete_at
        query.andWhere('az_additional_fee.deleted_at is null');
        // limit + offset
        query.orderBy('az_additional_fee.id', 'DESC')
            .limit(limit)
            .offset(offset);

        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }
    /**
     * This is function get tour by id
     * @param id
     */
    async findById(id: number) {
        return await this.findOne({
            where: { id, deleted_at: null },
            relations: [
                'areaInfo'
            ],
        });
    }

    /**
     * This is function get tour by id
     * @param id
     */
    async findByUuid(uuid: string) {
        return await this.findOne({ 
            where: { uuid: uuid, deleted_at: null },
            relations: [
                'areaInfo'
            ],
        });
    }

    /**
     * This is function get tour by id
     * @param id
     */
    async findByCode(code: string) {
        return await this.findOne({ 
            where: { code: code, deleted_at: null },
            relations: [
                'areaInfo'
            ],
        });
    }
}