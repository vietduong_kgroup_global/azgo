import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityId } from 'typeorm/repository/EntityId';
import { AdditionalFeeRepository } from './additional-fee.repository';
import {
    ERROR_UNKNOWN,
    ERROR_CAN_NOT_FIND_ADITION_FEE
} from '../../constants/error.const';

@Injectable()
export class AdditionalFeeService {
    constructor(
        @InjectRepository(AdditionalFeeRepository)
        private readonly repository: AdditionalFeeRepository,
    ) {
    }

    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        return await this.repository.getAllByOffsetLimit(limit, offset, options);
    }

    /**
     * This function get area by tour uuis
     * @param area_uuid
     */
    async findByUuid(uuid: string){
        return await this.repository.findByUuid(uuid);
    }

    /**
     * This function get area by tour uuis
     * @param area_uuid
     */
    async findByCode(code: string){
        return await this.repository.findByCode(code);
    }

    async create(body: any) {
        return await this.repository.save(body);
    }
    
    async updateById(id: EntityId, data: any){
        const rs = await this.repository.findOneOrFail(id);
        const dataUpdate = { ...rs, ...data };
        return await this.repository.save(dataUpdate);
    }

    async delete(id: number){
        try {
            const obj = await this.repository.findOne({id});
            if (!obj){
                return {
                    status: false,
                    data: null,
                    error: ERROR_CAN_NOT_FIND_ADITION_FEE,
                    message: 'Không tìm thấy phí phụ thu'
                };
            }
            obj.deleted_at = new Date();
            await this.repository.save(obj);
            return {
                status: true,
                data: null,
                error: null,
                message: null
            };
        } catch (error) {
            console.log("error deleteTour: ", error);
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message || 'Error in count order fee exist'
            };
        }
    }
}
