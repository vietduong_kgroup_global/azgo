import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdditionalFeeController } from './additional-fee.controller';
import { AdditionalFeeService } from './additional-fee.service';
import { AdditionalFeeRepository } from './additional-fee.repository';

@Module({
  imports: [TypeOrmModule.forFeature([AdditionalFeeRepository])],
  controllers: [AdditionalFeeController],
  providers: [AdditionalFeeService],
  exports: [AdditionalFeeService],
})
export class AdditionalFeeModule { }