import { ApiModelProperty } from "@nestjs/swagger";
import {IsInt, IsNotEmpty, IsString, MaxLength} from "class-validator";
export class AdditionalFeeDto {
    @IsNotEmpty()
    @ApiModelProperty({example: "Cần Thơ", required: true, description: "Tên phí phụ thu" })
    @IsString()
    @MaxLength(255, {message: "Name max length is 255"})
    name: string;

    @ApiModelProperty({ example: "AZGOFEE", required: true, description: "Mã phí phụ thu" })
    code: string;

    @ApiModelProperty({ example: "enum[flat, percent]", required: false, description: "Loại phí phụ thu" })
    type: string;

    @ApiModelProperty({ example: 12000, required: false, description: "Khi chọn loại phí flat" })
    flat: number;

    @ApiModelProperty({ example: 0.5, required: false, description: "Khi chọn loại phí percent" })
    percent: number;

    @ApiModelProperty({ example: "7bb9-4154-b45660a18e28", required: false, description: "Mã khu vực" })
    area_uuid: string;
}
