import { Test, TestingModule } from '@nestjs/testing';
import { AdditionalFeeController } from './additional-fee.controller';

describe('AdditionalFee Controller', () => {
  let controller: AdditionalFeeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AdditionalFeeController],
    }).compile();

    controller = module.get<AdditionalFeeController>(AdditionalFeeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
