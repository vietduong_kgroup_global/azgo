import { Body, Controller, Get, HttpStatus, Param, Post, Put, Delete, Req, Res, UseGuards } from '@nestjs/common';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { AuthGuard } from '@nestjs/passport';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import { AdditionalFeeDto } from './dto/additional-fee.dto';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiResponse,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiUseTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import { AdditionalFeeService } from './additional-fee.service';
import {
    ERROR_ADDITINAL_FEE_EXISTED_CODE,
    ERROR_UNKNOWN,
    ERROR_CAN_NOT_FIND_ADITION_FEE
} from '../../constants/error.const';

@ApiUseTags('Additional Fee')
@Controller('additional-fee')
export class AdditionalFeeController {
    constructor(
        private readonly additionalFeeService: AdditionalFeeService,
    ) {
    }
    @Get('')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All})
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get list fee with offset limit' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword search by name fee', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'type', description: 'Filter by type fee', required: false, type: 'string' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    async getListTour(@Req() request,  @Res() res: Response) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.additionalFeeService.getAllByOffsetLimit(per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).
            send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    @Get('/:fee_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Detail fee' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'fee_uuid', description: 'Fee Uuid', required: true, type: 'string' })
    async detail(@Req() request, @Param('fee_uuid') fee_uuid, @Res() res: Response) {
        try {
            const fee = await this.additionalFeeService.findByUuid(fee_uuid);
            if (!fee)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy phí phụ thu', null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', fee));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create new additional fee' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async create(@Req() request, @Body() body: AdditionalFeeDto, @Res() res: Response) {
        try {
            let checkExisted = await this.additionalFeeService.findByCode(body.code)
            if(checkExisted) return res.status(HttpStatus.NOT_FOUND).send(dataError('Mã phí đã tồn tại', null, ERROR_ADDITINAL_FEE_EXISTED_CODE));
            if(body.type === 'percent') {
                body.percent = body.percent ? (body.percent > 1 ? Number(body.percent)/100 : Number(body.percent)) : 0;
                body.flat = null;
            }
            else {
                body.flat = body.flat? body.flat : 0;
                body.percent = null;
            }
            const result = await this.additionalFeeService.create(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:uuid')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update one additional fee' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'uuid', description: 'Area uuid', required: true, type: 'string' })
    async update(@Param('uuid') uuid, @Body() body: AdditionalFeeDto, @Res() res: Response) {
        let fee;
        try {
            fee = await this.additionalFeeService.findByUuid(uuid);
            if (!fee)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy phí phụ thu', null, ERROR_CAN_NOT_FIND_ADITION_FEE));

            let checkExisted = await this.additionalFeeService.findByCode(body.code)
            if(checkExisted && checkExisted.uuid !== uuid) return res.status(HttpStatus.NOT_FOUND).send(dataError('Mã phí đã tồn tại', null, ERROR_ADDITINAL_FEE_EXISTED_CODE));
            
            if(body.type === 'percent') {
                body.percent = body.percent ? (body.percent > 1 ? Number(body.percent)/100 : Number(body.percent)) : 0;
                body.flat = null;
            }
            else {
                body.flat = body.flat? body.flat : 0;
                body.percent = null;
            }
            const result = await this.additionalFeeService.updateById(fee.id, body);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:uuid')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update one additional fee' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'uuid', description: 'Area uuid', required: true, type: 'string' })
    async delete(@Param('uuid') uuid, @Body() body: AdditionalFeeDto, @Res() res: Response) {
        let fee;
        try {
            fee = await this.additionalFeeService.findByUuid(uuid);
            if (!fee)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy phí phụ thu', null, ERROR_CAN_NOT_FIND_ADITION_FEE));
            
            const result = await this.additionalFeeService.delete(fee.id);
            if (result && result.status) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result.data));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Xoá phí phụ thu lỗi', null, result.error || ERROR_UNKNOWN));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
