import { Test, TestingModule } from '@nestjs/testing';
import { AdditionalFeeService } from './additional-fee.service';

describe('AdditionalFeeService', () => {
  let service: AdditionalFeeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AdditionalFeeService],
    }).compile();

    service = module.get<AdditionalFeeService>(AdditionalFeeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
