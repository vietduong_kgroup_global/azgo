import {Entity, PrimaryGeneratedColumn, Generated, Column, JoinColumn, ManyToOne, OneToOne} from 'typeorm';
import { Exclude } from 'class-transformer';
import {IsInt, IsNotEmpty} from 'class-validator';
import { AreaEntity } from '../area/area.entity';

@Entity('az_additional_fee')
export class AdditionalFeeEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    @Generated('uuid')
    @IsNotEmpty()
    uuid: string;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    code: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    name: string;

    @Column({
        type: 'enum',
        enum: ['flat', 'percent'],
        nullable: true,
    })
    type: string;

    @Column({
        type: 'float',
        nullable: true,
    })
    flat: number;

    @Column({
        type: 'float',
        nullable: true,
    })
    percent: number;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    area_uuid: string;
    @ManyToOne(type => AreaEntity, area => area.additionalFee)
    @JoinColumn({ name: 'area_uuid', referencedColumnName: 'uuid' })
    areaInfo: AreaEntity;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;
}
