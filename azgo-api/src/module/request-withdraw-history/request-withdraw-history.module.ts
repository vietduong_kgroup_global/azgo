import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RequestWithdrawHistoryService } from './request-withdraw-history.service';
import { RequestWithdrawHistoryController } from './request-withdraw-history.controller';
import { RequestWithdrawHistoryRepository } from './request-withdraw-history.repository';

@Module({
  imports: [TypeOrmModule.forFeature([RequestWithdrawHistoryRepository])],
  controllers: [RequestWithdrawHistoryController],
  providers: [RequestWithdrawHistoryService],
  exports: [RequestWithdrawHistoryService],
})
export class RequestWithdrawHistoryModule {}
