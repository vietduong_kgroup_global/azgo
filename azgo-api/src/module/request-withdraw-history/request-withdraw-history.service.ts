import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityId } from 'typeorm/repository/EntityId';
import { RequestWithdrawHistoryRepository } from './request-withdraw-history.repository';

@Injectable()
export class RequestWithdrawHistoryService {
    constructor(
        @InjectRepository(RequestWithdrawHistoryRepository)
        private readonly repository: RequestWithdrawHistoryRepository,
    ) {
    }
    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        return await this.repository.getAllByOffsetLimit(limit, offset, options);
    }

    /**
     * This function get area by tour uuis
     * @param area_uuid
     */
    async findByUuid(uuid: string){
        return await this.repository.findByUuid(uuid);
    }

    async create(body: any) {
        return await this.repository.save(body);
    }
    
    async updateById(id: EntityId, data: any){
        const rs = await this.repository.findOneOrFail(id);
        const dataUpdate = { ...rs, ...data };
        return await this.repository.save(dataUpdate);
    }
}
