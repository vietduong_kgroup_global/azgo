import { Brackets, EntityRepository, Repository } from 'typeorm';
import { RequestWithdrawHistoryEntity } from './request-withdraw-history.entity';

@EntityRepository(RequestWithdrawHistoryEntity)
export class RequestWithdrawHistoryRepository extends Repository<RequestWithdrawHistoryEntity> {
    /**
     * This function get list fee by limit & offset
     * @param limit
     * @param offset
     * @param options
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        let query = this.createQueryBuilder('az_request_withdraw_history');
        query.leftJoinAndSelect('az_request_withdraw_history.userUpdate', 'userUpdate'); 
        // query.leftJoinAndSelect('az_request_withdraw_history.driverInfo', 'driverInfo');
        
        // not delete_at
        query.andWhere('az_request_withdraw_history.deleted_at is null');
        // limit + offset
        query.orderBy('az_request_withdraw_history.id', 'DESC')
            .limit(limit)
            .offset(offset);

        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }
    /**
     * This is function get tour by id
     * @param id
     */
    async findById(id: number) {
        return await this.findOne({
            where: { id, deleted_at: null },
            // relations: [
            //     'requestHitory',
            // ],
        });
    }

    /**
     * This is function get tour by id
     * @param id
     */
    async findByUuid(uuid: string) {
        return await this.findOne({ 
            where: { uuid: uuid, deleted_at: null },
            // relations: [
            //     'requestHitory',
            // ],
        });
    }
}