
import { Entity, PrimaryGeneratedColumn, Generated, JoinColumn, ManyToOne, OneToOne, Column, BaseEntity } from 'typeorm';
import { UsersEntity } from '../users/users.entity';
import { RequestWithdrawEntity } from '../request-withdraw/request-withdraw.entity';
import { Exclude } from 'class-transformer';
import { IsInt, IsNotEmpty } from 'class-validator';

@Entity('az_request_withdraw_history')
export class RequestWithdrawHistoryEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    @Generated('uuid')
    @IsNotEmpty()
    uuid: string;

    @Column({
        type: 'int',
        nullable: true,
    })
    user_update_id : number;

    @ManyToOne(type => UsersEntity, user => user.requestUpdate)
    @JoinColumn({
        name: 'user_update_id',
    })
    userUpdate: UsersEntity;

    @Column({
        type: 'int',
        nullable: true,
    })
    request_id : number;

    @ManyToOne(type => RequestWithdrawEntity, request => request.requestHitory)
    @JoinColumn({ name: 'request_id', referencedColumnName: 'id' })
    requestDetail: RequestWithdrawEntity;

    @Column({
        type: 'varchar',
    })
    type: string;

    @Column({
        type: 'json',
        comment: 'Thông tin bị thay đổi',
    })
    data: object;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;
}