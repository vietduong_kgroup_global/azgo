import { Injectable } from '@nestjs/common';
import { BaseService } from '../../base.service';
import {BankRepository} from './bank.repository';
import {BankEntity} from './bank.entity';
import {CreateBankDto} from './dto/create-bank.dto';
import { EntityId } from 'typeorm/repository/EntityId';

@Injectable()
export class BankService  extends BaseService<BankEntity, BankRepository> {
    constructor(repository: BankRepository ) {
        super(repository); 
    } 
    async findAll(options?: any){
        let query = await this.repository.createQueryBuilder('bank');
        if (!options.keyword === false) {
            query.where('bank.short_name like :keyword', {keyword: '%' + options.keyword + '%' })
            .orWhere('bank.vn_name like :keyword', {keyword: '%' + options.keyword + '%' })
            .orWhere('bank.en_name like :keyword', {keyword: '%' + options.keyword + '%' })
            .orWhere('bank.bank_code like :keyword', {keyword: '%' + options.keyword + '%' });
        }
        // if (!options.type === false) {
        //     query.andWhere('bank.type = :type', {type: options.keyword});
        // }
        // query.andWhere('bank.deleted_at is null');
        // query.orderBy('bank.id', 'DESC');
        return await query.getMany();
    }

    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        return await this.repository.getAllByOffsetLimit(limit, offset, options);
    }


    async findById(id: number) {
        return await this.repository.findOneOrFail(id);
    }

    /**
     * This function get bank by tour uuis
     * @param bank_uuid
     */
    async findByUuid(uuid: string){
        return await this.repository.findByUuid(uuid);
    }
    async checkExist(bankCode: string){
        let res = await this.repository.findOne({bank_code: bankCode})
        return res;
    }

    async findByName(name: string) {
        console.log(name)
        return await this.repository.createQueryBuilder('bank')
            .where('bank.name like :name', {name: '%' + name + '%'})
            .getOne();
    }

    async createBank(body: any) { 
        return await this.repository.save(body);
    }

    async deleteBank(uuid: string){
        const obj = await this.repository.findOne({uuid});
        if (!obj)
            return false;
        await this.repository.delete(obj.id);
        return true;
    }

    // async updateBank(bank_uuid: string, body?: any) {
    //     console.log('bank_uuid',bank_uuid)
    //     const bank = await this.repository.findByUuid(bank_uuid);
    //     console.log('bank',bank)
    //     if (!bank) throw { message: 'Khu vực không tìm thấy', status: 404 };
    //     const dataSave = {...bank, ...body};
    //     console.log('body',body)
    //     console.log('dataSave',dataSave)
    //     return await this.update(bank.id, dataSave);

    // }
    async updateById(id: EntityId, data: CreateBankDto){ 
        return await this.update(id, data);
    }
}
