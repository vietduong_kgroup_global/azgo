import { Brackets, EntityRepository, Repository } from 'typeorm';
import { BankEntity } from './bank.entity';

@EntityRepository(BankEntity)
export class BankRepository extends Repository<BankEntity> {
    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     * @param options
     */ 
    /**
     * This is function get tour by id
     * @param id
     */
    async findById(id: number) {
        return await this.findOne({ where: { id, deleted_at: null } });
    }

    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        let query = this.createQueryBuilder('az_bank');
        // query.leftJoinAndSelect('az_bank.areaInfo', 'areaInfo');
        // search by keyword
        if (options.keyword && options.keyword.trim()) {
            query.orWhere(`az_bank.vn_name like :name`, { name: '%' + options.keyword.trim() + '%' });
            query.orWhere(`az_bank.en_name like :name`, { name: '%' + options.keyword.trim() + '%' });
            query.orWhere(`az_bank.short_name like :name`, { name: '%' + options.keyword.trim() + '%' });
            query.orWhere(`az_bank.bank_code like :name`, { name: '%' + options.keyword.trim() + '%' });
        } 
         
        query.orderBy('az_bank.updated_at', 'DESC')
            .limit(limit)
            .offset(offset);

        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }
    
    /**
     * This is function get tour by id
     * @param id
     */
    async findByUuid(uuid: string) {
        return await this.findOne({ where: {uuid: uuid} });
    }

}
