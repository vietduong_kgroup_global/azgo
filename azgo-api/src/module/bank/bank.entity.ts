import {BaseEntity, Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, Generated, OneToMany} from 'typeorm';
import {UserAccountEntity} from '../user-account/user-account.entity';
import {AccountsEntity} from '../accounts/accounts.entity'
@Entity('az_bank')
export class BankEntity   extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'binary',
        length: 36,
    })
    @Generated('uuid')
    uuid: string;

    @Column({
        type: 'varchar',
        length: '250',
    })
    en_name: string;
 
    @Column({
        type: 'varchar',
        length: '250',
    })
    vn_name: string;

    @Column({
        type: 'varchar',
        length: '50',
    })
    bank_id: string;

    @Column({
        type: 'varchar',
        length: '50',
    })
    atm_bin: string;

    @Column({
        type: 'varchar',
        length: '50',
    })
    card_length: string;

    @Column({
        type: 'varchar',
        length: '50',
    })
    short_name: string;

    @Column({
        type: 'varchar',
        length: '50',
    })
    bank_code: string;

    @Column({
        type: 'varchar',
        length: '50',
    })
    type: string;

    @Column({
        type: 'boolean',
        default: 0,
    })
    napas_supported: number;

    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;
 
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date; 

    @OneToMany(type => AccountsEntity, id => id.bankInfo)
    bankAccount: AccountsEntity[];
   
}
