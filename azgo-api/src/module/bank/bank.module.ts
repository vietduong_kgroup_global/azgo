import { Module } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {BankRepository} from './bank.repository'; 
import {BankService} from './bank.service';
import {BankController} from './bank.controller'; 

@Module({
    imports: [
        TypeOrmModule.forFeature([BankRepository]),
    ],
    providers: [BankService],
    controllers: [BankController],
    exports: [BankService],
})
export class BankModule { 

}
