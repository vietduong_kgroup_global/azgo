import {
    Body,
    Controller,
    Delete, Get,
    HttpStatus,
    Param,
    Post,
    Put,
    Req,
    Res,
    UseGuards,
} from '@nestjs/common';
import {BankService} from './bank.service';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags
} from '@nestjs/swagger';
import {Roles} from '@azgo-module/core/dist/common/decorators/roles.decorator';
import {FunctionKey, ActionKey} from '@azgo-module/core/dist/common/guards/roles.guards';
import {CreateBankDto} from './dto/create-bank.dto';
import {AuthGuard} from '@nestjs/passport';
import {CustomValidationPipe} from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { Response } from 'express';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@Controller('bank')
export class BankController {
    constructor(public service: BankService) { 

    }

    @Get('/getAllBankByAdmin')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Retrieve many BankEntity By Admin' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Search keyword by short_name, vn_name, en_name, bank_code', required: false, type: 'string' })
    // @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    // @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    // @ApiImplicitQuery({name: 'type', description: 'Type', required: false, type: 'number'})
    async getAllBankByAdmin(@Req() request, @Res() res: Response) { 
        try { 
            const results = await this.service.findAll(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', results));
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }
    }

    @Get('/getAllBankByMobile')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Retrieve many BankEntity By Mobile' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Search keyword by short_name, vn_name, en_name, bank_code', required: false, type: 'string' })
    // @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    // @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    // @ApiImplicitQuery({name: 'type', description: 'Type', required: false, type: 'number'})
    async getAllBankByMobile(@Req() request, @Res() res: Response) { 
        try { 
            const results = await this.service.findAll(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', results));
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }
    }

    @Get('/getAllByOffsetLimit')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get list bank' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'type', description: 'Type', required: false, type: 'number'})
    async getAllByOffsetLimit(@Req() request, @Res() res: Response) { 
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.service.getAllByOffsetLimit(per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).
            send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    @Get('/:bank_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Detail bank' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'bank_uuid', description: 'bank Uuid', required: true, type: 'string' })
    async detail(@Req() request, @Param('bank_uuid') bank_uuid, @Res() res: Response) {
        try {
            console.log('bank_uuid', bank_uuid);
            // const token = request.headers.authorization.split(' ')[1];
            // const userInfo: any =  jwt.decode(token);
            const bank = await this.service.findByUuid(bank_uuid);
            if (!bank)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy bank', null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', bank));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

   

    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create one BankEntity' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async create(@Req() request, @Body(CustomValidationPipe) body: CreateBankDto, @Res() res: Response) {
        try { 
            const resultExist = await this.service.checkExist(body.bank_code);
            if (!resultExist)
            {
                const result = await this.service.createBank(body);
                return res.status(HttpStatus.CREATED).send(dataSuccess('oK', result));
            }
            else  return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bank code exist', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:uuid')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update one BankEntity' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'uuid', description: 'Bank uuid', required: true, type: 'string' })
    async update(@Param('uuid') id, @Body() body: CreateBankDto, @Res() res: Response) {
        let bank;
        try {
            bank = await this.service.findByUuid(id);
            if (!bank)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy bank', null));
            
            const resultExist = await this.service.checkExist(body.bank_code); 
            if (bank.bank_code === body.bank_code || (bank.bank_code !== body.bank_code && !resultExist))
            {
                const result = await this.service.updateById(bank.id, body);
                if (result) {
                    return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
                }
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
            }
            else return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bank code exist', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete one BankEntity' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'id', description: 'Bank Id', required: true, type: 'number' })
    async delete(@Param('id') id, @Res() res: Response) {
        try {
            await this.service.deleteBank(id);
            return res.status(HttpStatus.OK).send(dataSuccess('Delete success', null));
            // return res.status(HttpStatus.OK).send(dataError('Delete error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
