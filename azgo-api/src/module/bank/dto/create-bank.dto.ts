import { ApiModelProperty } from '@nestjs/swagger';
import {IsBoolean, IsInt, IsNotEmpty, IsString, MaxLength} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';
// import {BankType} from '../../../enums/type.enum';
export class CreateBankDto {
 
    @ApiModelProperty({example: 'Asia Commercial Bank'})
    @IsString()
    @MaxLength(255, {message: 'Name max length is 255'})
    en_name: string;

    @ApiModelProperty({example: 'Ngân hàng Á Châu'})
    @IsString()
    @MaxLength(255, {message: 'Name max length is 255'})
    vn_name: string;

    @ApiModelProperty({example: 'ACB'})
    @IsString()
    @MaxLength(255, {message: 'Name max length is 255'})
    short_name: string;

    @ApiModelProperty({example: '123456'})
    @IsString()
    @MaxLength(50, {message: 'Name max length is 50'})
    bank_code: string;

    @ApiModelProperty({example: true})
    @IsBoolean() 
    napas_supported: boolean;

}
