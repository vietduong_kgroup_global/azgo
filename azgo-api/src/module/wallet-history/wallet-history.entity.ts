import {Column, Entity, JoinColumn, OneToOne, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {UsersEntity} from '../users/users.entity';

@Entity('az_wallet_histories')
export class WalletHistoryEntity {
    @PrimaryGeneratedColumn({ type: 'bigint', unsigned: true })
    id: number;

    @Column({
        type: 'int',
        nullable: false,
    })
    driver_id: number;

    @Column({
        type: 'int',
    })
    user_update_id: number;

    @Column('double')
    amount: number;

    @Column({
        type: 'smallint',
        default: 1,
    })
    payment_method: number;

    @Column({
        type: 'smallint',
        default: 1,
    })
    payment_status: number;

    @Column({
        type: 'varchar',
        default: 1,
    })
    content: string;

    @Column({
        type: 'varchar',
    })
    payment_code: string;

    @Column({
        type: 'varchar',
    })
    order_code: string;

    @Column({
        type: 'smallint',
        default: 1,
    })
    type: number;

    @Column({
        type: 'varchar',
    })
    tag: string;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @ManyToOne(type => UsersEntity, driver => driver.walletHistoryEntity)
    @JoinColumn({
        name: 'driver_id',
    })
    usersEntity: UsersEntity;

    @ManyToOne(type => UsersEntity, driver => driver.walletHistoryEntity)
    @JoinColumn({
        name: 'user_update_id',
    })
    userUpdateEntity: UsersEntity;
}
