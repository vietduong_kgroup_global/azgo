import { forwardRef, Module } from '@nestjs/common';
import { WalletHistoryController } from './wallet-history.controller';
import { WalletHistoryService } from './wallet-history.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {WalletHistoryRepository} from './wallet-history.repository';
import {UsersModule} from '../users/users.module';

@Module({
  imports: [
      TypeOrmModule.forFeature([WalletHistoryRepository]),
      forwardRef(() => UsersModule)
  ],
  controllers: [WalletHistoryController],
  providers: [WalletHistoryService],
  exports: [WalletHistoryService],
})
export class WalletHistoryModule {}
