import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { WalletHistoryRepository } from './wallet-history.repository';
import { WalletHistoryEntity } from './wallet-history.entity';
import {
    ERROR_UNKNOWN,
} from '../../constants/error.const';

@Injectable()
export class WalletHistoryService {
    constructor(
        @InjectRepository(WalletHistoryRepository)
        private readonly walletHistoryRepository: WalletHistoryRepository,
    ) {
    }

    async getAll(driver_id: number, limit: number, offset: number, options?: any) {
        return await this.walletHistoryRepository.getAll(driver_id, limit, offset, options);
    }
    async countOrderFeeExist(order_code: string, driver_id?: number, tag?: string[]) {
        try {
            let countFee =  await this.walletHistoryRepository.countOrderFeeExist(order_code, driver_id, tag);
            return {
                status: countFee.status,
                data: countFee.data,
                error: countFee.error,
                message: countFee.message
            };
        } catch (error) {
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message || 'Error in count order fee exist'
            };
        }
    }
    async countOrderFeeReturn(order_code: string, driver_id?: number, tag?: string[]) {
        try {
            let countFee =  await this.walletHistoryRepository.countOrderFeeReturn(order_code, driver_id, tag);
            return {
                status: countFee.status,
                data: countFee.data,
                error: countFee.error,
                message: countFee.message
            };
        } catch (error) {
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message || 'Error in count order fee exist'
            };
        }
    }

    async create(body: WalletHistoryEntity) {
        return await this.walletHistoryRepository.save(body);
    }
}
