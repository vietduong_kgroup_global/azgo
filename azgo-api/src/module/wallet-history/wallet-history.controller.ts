import {Controller, Get, HttpStatus, Req, Res, UseGuards, Param} from '@nestjs/common';
import {WalletHistoryService} from './wallet-history.service';
import {AuthGuard} from '@nestjs/passport';
import {
    ApiBearerAuth,
    ApiImplicitQuery,
    ApiImplicitParam,
    ApiOperation,
    ApiResponse,
    ApiUseTags
} from '@nestjs/swagger';
import * as jwt from 'jsonwebtoken';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { Response } from 'express';
import { VehicleGroup, UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import { UsersService } from '../users/users.service';

@Controller('wallet-history')
@ApiUseTags('Wallet history')
export class WalletHistoryController {
    constructor(
        private readonly walletHistoryService: WalletHistoryService,
        private readonly usersService: UsersService,
    ) {
    }

    @Get()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all wallet history' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: Number})
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: Number})
    @ApiImplicitQuery({
        name: 'month', description: 'Number month',
        required: false, type: String, enum: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
    })
    @ApiImplicitQuery({name: 'year', description: 'Number year', required: true, type: String})
    async getAll( @Req() request, @Res() res: Response) {
        // ===============Query offset + per_page=============
        let results;
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        try {
            const token = request.headers.authorization.split(' ')[1];
            const driverInfo: any = jwt.decode(token);
            results = await this.walletHistoryService.getAll(driverInfo.id, per_page, offset, request.query);
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    @Get('/:driver_uuid')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get one wallet by driver' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: Number})
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: Number})
    @ApiImplicitParam({ name: 'driver_uuid', description: 'User uuid of driver', required: true, type: 'string' })
    async getOneByDriverUuid(@Param('driver_uuid') driver_uuid, @Req() request, @Res() res: Response) {
        // ===============Query offset + per_page=============
        let results;
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        try {
            const user = await this.usersService.findOne(driver_uuid);
            if(!user) return res.status(HttpStatus.BAD_REQUEST).send(dataError('Không tìm thấy tài xế', null));
            results = await this.walletHistoryService.getAll(user.id, per_page, offset, request.query);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).
                send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
        
    }

}
