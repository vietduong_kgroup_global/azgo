import { Brackets, EntityRepository, Repository } from 'typeorm';
import { WalletHistoryEntity } from './wallet-history.entity';
import {
    ERROR_UNKNOWN,
} from '../../constants/error.const';

@EntityRepository(WalletHistoryEntity)
export class WalletHistoryRepository extends Repository<WalletHistoryEntity> {
    async getAll(driver_id: number, limit: number, offset: number, options?: any) {
        // create new query
        let query = this.createQueryBuilder('az_wallet_histories');
        query.leftJoinAndSelect('az_wallet_histories.userUpdateEntity', 'userUpdateEntity');
        query.andWhere('az_wallet_histories.driver_id = :driver_id', { driver_id });
        query.andWhere('az_wallet_histories.deleted_at is null');

        // limit + offset
        query.orderBy('az_wallet_histories.id', 'DESC')
            .take(limit)
            .skip(offset);
        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }
    /**
     * This function count fee minus driver
     * @param order_code
     * @param driver_id
     * @param tag
     */
    async countOrderFeeExist(order_code: string, driver_id?: number, tag?: string[]) {
        try {
            // create new query
            let query = this.createQueryBuilder('az_wallet_histories');
            query.andWhere('az_wallet_histories.order_code = :order_code', { order_code });
            query.andWhere('az_wallet_histories.amount < 0');
            query.andWhere('az_wallet_histories.deleted_at is null');
            if(driver_id){
                query.andWhere('az_wallet_histories.driver_id = :driver_id', { driver_id });
            }
            if(tag){
                query.andWhere('az_wallet_histories.tag IN (:tag)', { tag });
            }
            const results = await query.getMany();
            // return results.length;
            return {
                status: true,
                data: results.length,
                error: null,
                message: null
            };
        } catch (error) {
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message || 'Error in count order fee exist'
            };
        }
    }
    /**
     * This function count fee return driver
     * @param order_code
     * @param driver_id
     * @param tag
     */
    async countOrderFeeReturn(order_code: string, driver_id?: number, tag?: string[]) {
        try {
            // create new query
            let query = this.createQueryBuilder('az_wallet_histories');
            query.andWhere('az_wallet_histories.order_code = :order_code', { order_code });
            query.andWhere('az_wallet_histories.amount > 0');
            query.andWhere('az_wallet_histories.deleted_at is null');
            if(driver_id){
                query.andWhere('az_wallet_histories.driver_id = :driver_id', { driver_id });
            }
            if(tag){
                query.andWhere('az_wallet_histories.tag IN (:tag)', { tag });
            }
            const results = await query.getMany();
            // return results.length;
            return {
                status: true,
                data: results.length,
                error: null,
                message: null
            };
        } catch (error) {
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message || 'Error in count order fee exist'
            };
        }
    }
}
