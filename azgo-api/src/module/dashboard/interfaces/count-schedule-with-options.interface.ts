export interface CountScheduleWithOptionsInterface {
    user_type: number;
    provider_id?: number;
    driver_id?: number;
    from_date?: string;
    to_date?: string;
    is_in_month?: string;
    status?: number;
}
