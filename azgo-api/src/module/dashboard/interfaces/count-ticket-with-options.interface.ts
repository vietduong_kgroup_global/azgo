export interface CountTicketWithOptionsInterface {
    user_type: number;
    provider_id?: number;
    from_date?: string;
    to_date?: string;
    is_in_month?: string;
}
