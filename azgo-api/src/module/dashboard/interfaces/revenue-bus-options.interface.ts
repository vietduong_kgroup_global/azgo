export interface RevenueBusOptionsInterface {
    user_type: number;
    user_id: number;
    driver_id?: number;
    provider_id?: number;
    is_in_month?: string;
    from_date?: string;
    to_date?: string;

}
