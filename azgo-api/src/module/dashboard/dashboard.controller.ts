import {
    Controller,
    Get,
    HttpStatus,
    Req,
    Res,
    UseGuards,
} from '@nestjs/common';
import { DashboardService} from './dashboard.service';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import * as jwt from 'jsonwebtoken';
import { AuthGuard} from '@nestjs/passport';
import { Response } from 'express';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { VehicleGroup } from '@azgo-module/core/dist/utils/enum.ultis';
import { ApiBearerAuth, ApiImplicitQuery, ApiOperation, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import { CustomChargeService } from '../custom-charge/custom-charge.service';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import { ProvidersInfoService } from '../providers-info/providers-info.service';
import {UsersService} from '../users/users.service';

@Controller('dashboard')
@ApiUseTags('Dashboard')
export class DashboardController {
    constructor(
        private readonly dashboardService: DashboardService,
        private readonly customChageService: CustomChargeService,
        private readonly providersInfoService: ProvidersInfoService,
        private readonly userService: UsersService,

    ) {
    }

    @Get('count-amount-schedule')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Count amount schedule' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'from_date', description: '31/3/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'to_date', description: '31/4/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'is_in_month', description: 'Get schedule in month', required: false, type: Boolean})
    @ApiImplicitQuery({name: 'status', description: 'Get schedule by status', required: false, type: Number})
    @ApiImplicitQuery({name: 'driver_id', description: 'driver id', required: false, type: Number})
    @ApiImplicitQuery({name: 'provider_id', description: 'Provider id', required: false, type: Number})
    async countAmountScheduleInMonthByBus(@Req() request, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const userInfo =  jwt.decode(token);
            const usersEntity = await this.userService.findOneByUserInfo(userInfo); // uuid

            let options = {user_type: request.user.type, ...request.query};
            const providerEntity = await this.providersInfoService.findByUserId(usersEntity.id);
            if (providerEntity) {
                options.provider_id = providerEntity.id;
            }
            const result = await this.dashboardService.countAmountScheduleInMonthByBus(options);
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('revenue-bus')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Revenue bus' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'is_in_month', description: 'Get order in month', required: false, type: Boolean})
    @ApiImplicitQuery({name: 'from_date', description: '31/3/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'to_date', description: '31/4/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'driver_id', description: 'Driver id', required: false, type: Number})
    @ApiImplicitQuery({name: 'provider_id', description: 'Provider id', required: false, type: Number})
    async revenueInMonthByBus(@Req() request, @Res() res: Response) {
        try {
            const options = {...request.query, ...{user_type: request.user.type, user_id: request.user.id}};
            const result: {total_price: number} = await this.dashboardService.revenueByBus(options);
            const customCharge = await this.customChageService.getOneByVehicleGroup(VehicleGroup.BUS);
            let revenueBusWithCharge = result.total_price;
            if (request.user.type === 1 && customCharge) {
                const rate_of_charge = customCharge.rate_of_charge;
                if (customCharge.rate_of_charge === 0) {
                    return res.status(HttpStatus.BAD_REQUEST).send(dataError('rate_of_charge right thaner 0', null));
                }
                revenueBusWithCharge = Math.ceil((result.total_price * rate_of_charge) / 100);
            }
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', revenueBusWithCharge));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('line-chart-revenue-by-admin')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Line chart revenue by admin' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'year', description: '2020', required: true, type: Number})
    @ApiImplicitQuery({name: 'month', description: '1', required: false, type: Number})
    async lineChartRevenueByAdmin(@Req() request, @Res() res: Response) {
        try {
            const result = await this.dashboardService.lineChartRevenueTotalByAdmin(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('line-chart-revenue-bus-by-provider')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.PROVIDER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Line chart revenue bus by provider' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'year', description: '2020', required: true, type: Number})
    @ApiImplicitQuery({name: 'month', description: '1', required: false, type: Number})
    async lineChartRevenueBusByProvider(@Req() request, @Res() res: Response) {
        try {
            const provider = await this.providersInfoService.findByUserId(request.user.id);
            const result = await this.dashboardService.lineChartRevenueBusTotalByProvider(provider.id, request.query); // 4 = hoàn thành
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
