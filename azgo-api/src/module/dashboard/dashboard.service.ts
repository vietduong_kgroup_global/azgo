import {HttpStatus, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {TicketRepository} from '../ticket/ticket.repository';
import {VehicleScheduleRepository} from '../vehicle-schedule/vehicleSchedule.repository';
import * as moment from 'moment';
import {ProvidersInfoService} from '../providers-info/providers-info.service';
import {BikeCarOrderRepository} from '../bike-car-order/bikeCarOrder.repository';
import {CountScheduleWithOptionsInterface} from './interfaces/count-schedule-with-options.interface';
import {RevenueBusOptionsInterface} from './interfaces/revenue-bus-options.interface';
import {VanBagacOrdersRepository} from '../van-bagac-orders/van-bagac-orders.repository';
import * as _ from 'lodash';
import { VehicleGroup } from '@azgo-module/core/dist/utils/enum.ultis';
import {CustomChargeService} from '../custom-charge/custom-charge.service';

@Injectable()
export class DashboardService {
    constructor(
        @InjectRepository(TicketRepository)
        private readonly ticketRepository: TicketRepository,
        @InjectRepository(VehicleScheduleRepository)
        private readonly vehicleScheduleRepository: VehicleScheduleRepository,
        @InjectRepository(BikeCarOrderRepository)
        private readonly bikeCarOrderRepository: BikeCarOrderRepository,
        @InjectRepository(VanBagacOrdersRepository)
        private readonly vanBagacOrdersRepository: VanBagacOrdersRepository,
        private readonly providersInfoService: ProvidersInfoService,
        private readonly userService: ProvidersInfoService,
        private readonly customChageService: CustomChargeService,

    ) {
    }
    // Admin - bus
    async countAmountScheduleInMonthByBus(options: CountScheduleWithOptionsInterface) {
        let query = await this.vehicleScheduleRepository.createQueryBuilder('schedule');
        if (options.provider_id) {
            query.andWhere('schedule.provider_id = :provider_id', {provider_id: options.provider_id});
        }
        if (options && options.status) {
            query.andWhere('schedule.status = :schedule_status', {schedule_status: options.status});
        }
        if (options && options.is_in_month === 'true') {
            const currentDate = moment(new Date(), 'YYYY-MM').format('YYYY-MM').toString();
            query.andWhere('schedule.start_date like :created_at', {created_at: '%' + currentDate + '%'});
        }

        if (options && options.from_date) {
            const from_date_tostring = await this.formatDate(options.from_date) + ' 00:00:00';
            query.andWhere('schedule.start_date >= :from_date_tostring', {from_date_tostring});

        }
        if (options && options.to_date) {
            const to_date_tostring = await this.formatDate(options.to_date) + ' 23:59:59';
            query.andWhere('schedule.start_date <= :to_date_tostring', {to_date_tostring});
        }
        if (options && options.driver_id) {
            query.andWhere('schedule.user_id = :user_id', {user_id: options.driver_id});
        }
        return query.getCount();
    }

    async revenueByBus(options: RevenueBusOptionsInterface) {
        let query = await this.ticketRepository.createQueryBuilder('ticket')
            .leftJoin('ticket.vehicleSchedule', 'vehicleSchedule')
            .leftJoin('vehicleSchedule.bookShippings', 'bookShippings');
        let provider_id = options.provider_id;
        if (options.user_type === 4 ) {
            const providerEntity = await this.providersInfoService.findByUserId(options.user_id);
            if (!providerEntity) {
                throw {message: 'Not found provider info', status: HttpStatus.BAD_REQUEST};
            }
            options.provider_id = providerEntity.id;
        }
        if (options) {
            if (options.provider_id) {
                query.andWhere('vehicleSchedule.provider_id = :provider_id', {provider_id: options.provider_id});
            }
            if (options.is_in_month === 'true') {
                const currentDate = moment(new Date(), 'YYYY-MM').format('YYYY-MM').toString();
                query.where('vehicleSchedule.start_date like :start_date', {start_date: '%' + currentDate + '%'});
            }

            if (options.from_date) {
                const from_date_tostring = await this.formatDate(options.from_date) + ' 00:00:00';
                query.andWhere('vehicleSchedule.start_date >= :from_date_tostring', {from_date_tostring});
            }
            if (options.to_date) {
                const to_date_tostring = await this.formatDate(options.to_date) + ' 23:59:59';
                query.andWhere('vehicleSchedule.start_date <= :to_date_tostring', {to_date_tostring});
            }

            if (options.driver_id) {
                query.andWhere('vehicleSchedule.user_id = :driver_id', {driver_id: options.driver_id});
            }
        }

        return  query.select('(SUM(ticket.price) + SUM(bookShippings.price)) as total_price').getRawOne();
    }
    async lineChartRevenueBusTotalByProvider(provider_id: number, options: {year: number, month?: number}) {
        // get revenue bus
        let revenueBus;
        if (typeof options.month === 'undefined') {
            // Choose Year, return month
            revenueBus = await this.ticketRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.vehicleSchedule', 'vehicleSchedule')
                .select('YEAR(ticket.created_at) as year, MONTH(ticket.created_at) as month,' +
                    ' SUM(price) as value')
                .where('YEAR(ticket.created_at) = :year', {year: options.year})
                .andWhere('vehicleSchedule.provider_id = :provider_id', {provider_id})
                .groupBy('month')
                .getRawMany();
        } else {
            // Choose month return day
            revenueBus = await this.ticketRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.vehicleSchedule', 'vehicleSchedule')
                .select('YEAR(ticket.created_at) as year, MONTH(ticket.created_at) as month,' +
                    ' DAY(ticket.created_at) as day, SUM(price) as value')
                .where('YEAR(ticket.created_at) = :year', {year: options.year})
                .andWhere('MONTH(ticket.created_at) = :month', {month: options.month})
                .andWhere('vehicleSchedule.provider_id = :provider_id', {provider_id})
                .groupBy('day')
                .getRawMany();
        }
        return revenueBus;
    }
    async lineChartRevenueTotalByAdmin(options: {year: number, month?: number}) {
        // get revenue bus
        let revenueBus, revenueBike, revenueCar, revenueVan, revenueBagac;
        let flagExistMonth = false;
        const customChargeBus = await this.customChageService.getOneByVehicleGroup(VehicleGroup.BUS);
        if (typeof options.month === 'undefined') {
            // Choose Year, return month
            revenueBus = await this.ticketRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.vehicleSchedule', 'vehicleSchedule')
                .select('MONTH(vehicleSchedule.start_date) as month, YEAR(vehicleSchedule.start_date) as year, ' +
                    ' SUM(price) as value')
                .where('YEAR(vehicleSchedule.start_date) = :year', {year: options.year})
                .groupBy('year, month')
                .getRawMany();
            revenueBus.map((v) => {
                v.value = (v.value * customChargeBus.rate_of_charge) / 100.0;
            });

            // get revenue bike/car
            revenueBike = await this.bikeCarOrderRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.customCharge', 'customCharge')
                .select('MONTH(ticket.created_at) as month, YEAR(ticket.created_at) as year,' +
                    ' SUM(price) * customCharge.rate_of_charge / 100.0 as value')
                .where('YEAR(ticket.created_at) = :year', {year: options.year})
                .andWhere('ticket.vehicle_group_id = :vehicle_group_id', {vehicle_group_id: VehicleGroup.BIKE })
                .groupBy('year, month')
                .getRawMany();
            revenueCar = await this.bikeCarOrderRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.customCharge', 'customCharge')
                .select('MONTH(ticket.created_at) as month, YEAR(ticket.created_at) as year, ' +
                    ' SUM(price) * customCharge.rate_of_charge / 100.0 as value')
                .where('YEAR(ticket.created_at) = :year', {year: options.year})
                .andWhere('ticket.vehicle_group_id = :vehicle_group_id', {vehicle_group_id: VehicleGroup.CAR })
                .groupBy('year, month')
                .getRawMany();

            // get revenue van/bagac
            revenueVan = await this.vanBagacOrdersRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.customCharge', 'customCharge')
                .select('DISTINCT MONTH(ticket.created_at) as month,  YEAR(ticket.created_at) as year, ' +
                    ' SUM(price) * customCharge.rate_of_charge / 100.0 as value')
                .where('YEAR(ticket.created_at) = :year', {year: options.year})
                .andWhere('ticket.vehicle_group_id = :vehicle_group_id', {vehicle_group_id: VehicleGroup.VAN })
                .groupBy('year, month')
                .getRawMany();
            revenueBagac = await this.vanBagacOrdersRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.customCharge', 'customCharge')
                .select('MONTH(ticket.created_at) as month, YEAR(ticket.created_at) as year, ' +
                    ' SUM(price) * customCharge.rate_of_charge / 100.0 as value')
                .where('YEAR(ticket.created_at) = :year', {year: options.year})
                .andWhere('ticket.vehicle_group_id = :vehicle_group_id', {vehicle_group_id: VehicleGroup.BAGAC })
                .groupBy('year, month')
                .getRawMany();
        } else {

            flagExistMonth = true;
            // Choose month return day
            revenueBus = await this.ticketRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.vehicleSchedule', 'vehicleSchedule')
                .select('YEAR(vehicleSchedule.start_date) as year, MONTH(vehicleSchedule.start_date) as month,' +
                    ' DAY(vehicleSchedule.start_date) as day, SUM(price) as value')
                .where('YEAR(vehicleSchedule.start_date) = :year', {year: options.year})
                .andWhere('MONTH(vehicleSchedule.start_date) = :month', {month: options.month})
                .groupBy('day')
                .getRawMany();
            // Tính tỷ lệ doanh thu %
            revenueBus.map((v) => {
                v.value = (v.value * customChargeBus.rate_of_charge) / 100.0;
            });
            // get revenue bike/car
            revenueBike = await this.bikeCarOrderRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.customCharge', 'customCharge')
                .select('YEAR(ticket.created_at) as year, MONTH(ticket.created_at) as month,' +
                    ' DAY(ticket.created_at) as day, SUM(price) * customCharge.rate_of_charge / 100.0 as value')
                .where('YEAR(ticket.created_at) = :year', {year: options.year})
                .andWhere('MONTH(ticket.created_at) = :month', {month: options.month})
                .andWhere('ticket.vehicle_group_id = :vehicle_group_id', {vehicle_group_id: VehicleGroup.BIKE })
                .groupBy('day')
                .getRawMany();
            revenueCar = await this.bikeCarOrderRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.customCharge', 'customCharge')
                .select('YEAR(ticket.created_at) as year, MONTH(ticket.created_at) as month,' +
                    ' DAY(ticket.created_at) as day, SUM(price) * customCharge.rate_of_charge / 100.0 as value')
                .where('YEAR(ticket.created_at) = :year', {year: options.year})
                .andWhere('MONTH(ticket.created_at) = :month', {month: options.month})
                .andWhere('ticket.vehicle_group_id = :vehicle_group_id', {vehicle_group_id: VehicleGroup.CAR })
                .groupBy('day')
                .getRawMany();
            // get revenue van/bagac
            revenueVan = await this.vanBagacOrdersRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.customCharge', 'customCharge')
                .select('YEAR(ticket.start_date) as year, MONTH(ticket.start_date) as month,' +
                    ' DAY(ticket.start_date) as day, SUM(price) * customCharge.rate_of_charge / 100.0 as value')
                .where('YEAR(ticket.start_date) = :year', {year: options.year})
                .andWhere('MONTH(ticket.start_date) = :month', {month: options.month})
                .andWhere('ticket.vehicle_group_id = :vehicle_group_id', {vehicle_group_id: VehicleGroup.VAN })
                .groupBy('day')
                .getRawMany();
            revenueBagac = await this.vanBagacOrdersRepository.createQueryBuilder('ticket')
                .leftJoinAndSelect('ticket.customCharge', 'customCharge')
                .select('YEAR(ticket.start_date) as year, MONTH(ticket.start_date) as month,' +
                    ' DAY(ticket.start_date) as day, SUM(price) * customCharge.rate_of_charge / 100.0 as value')
                .where('YEAR(ticket.start_date) = :year', {year: options.year})
                .andWhere('MONTH(ticket.start_date) = :month', {month: options.month})
                .andWhere('ticket.vehicle_group_id = :vehicle_group_id', {vehicle_group_id: VehicleGroup.BAGAC })
                .groupBy('day')
                .getRawMany();
        }
        const sumBusBike = await this.sumArrayObject(revenueBus, revenueBike, flagExistMonth);
        const sumBusBikeCar = await this.sumArrayObject(sumBusBike, revenueCar, flagExistMonth);
        const sumBusBikeCarVan = await this.sumArrayObject(sumBusBikeCar, revenueVan, flagExistMonth);
        const sumBusBikeCarVanBagac = await this.sumArrayObject(sumBusBikeCarVan, revenueBagac, flagExistMonth);
        return sumBusBikeCarVanBagac;
    }
    async sumArrayObject(a, b, flagExistMonth: boolean) {
        let c = [];
        if (a.length > b.length) {
            a.map((value) => {
                const rs = flagExistMonth ? _.find(b, {day: value.day}) : _.find(b, {month: value.month});
                if (rs !== undefined) {
                    value.value += rs.value;
                }
            });
            c = a;
        } else {
            b.map((value) => {
                const rs = flagExistMonth ? _.find(b, {day: value.day}) : _.find(a, {month: value.month});
                if (rs !== undefined) {
                    value.value += rs.value;
                }
            });
            c = b;
        }
        return c;
    }
    /*
    * Input format: DD/MM/YYYY
    *
    * */
    formatDate(date: string) {
        const dateSplit = date.split('/');
        // month is 0-based, that's why we need dataParts[1] - 1
        const dateObject = new Date(+dateSplit[2],  +dateSplit[1] - 1, +dateSplit[0]);
        const formatDate = moment(dateObject, 'YYYY-MM-DD');
        return formatDate.format('YYYY-MM-DD').toString();
    }
}
