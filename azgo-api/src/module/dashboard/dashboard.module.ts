import { Module } from '@nestjs/common';
import { DashboardController } from './dashboard.controller';
import { DashboardService } from './dashboard.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {TicketRepository} from '../ticket/ticket.repository';
import {VehicleScheduleRepository} from '../vehicle-schedule/vehicleSchedule.repository';
import {ProvidersInfoModule} from '../providers-info/providers-info.module';
import {BikeCarOrderRepository} from '../bike-car-order/bikeCarOrder.repository';
import {CustomChargeModule} from '../custom-charge/custom-charge.module';
import {VanBagacOrdersRepository} from '../van-bagac-orders/van-bagac-orders.repository';
import {UsersModule} from '../users/users.module';

@Module({
  imports: [
      TypeOrmModule.forFeature([TicketRepository, VehicleScheduleRepository, BikeCarOrderRepository, VanBagacOrdersRepository]),
      ProvidersInfoModule,
      CustomChargeModule,
      UsersModule,
  ],
  controllers: [DashboardController],
  providers: [DashboardService],
})
export class DashboardModule {}
