import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm';
import {VehicleEntity} from '../vehicle/vehicle.entity';
import {DriverProfileEntity} from '../driver-profile/driver-profile.entity';
import {IsInt, IsNotEmpty} from 'class-validator';

@Entity('az_vehicle_driver')
export class VehicleDriverEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
    })
    @IsNotEmpty()
    @IsInt()
    vehicle_id: number;

    @Column({
        type: 'int',
    })
    @IsNotEmpty()
    @IsInt()
    driver_id: number;

    @ManyToOne(type => VehicleEntity, vehicle => vehicle.id)
    @JoinColumn({name: 'vehicle_id', referencedColumnName: 'id'})
    vehicle: VehicleEntity;

    @ManyToOne(type => DriverProfileEntity, driver => driver.user_id)
    @JoinColumn({name: 'driver_id', referencedColumnName: 'user_id'})
    driverProfile: DriverProfileEntity;
}
