import { Module } from '@nestjs/common';
import { VehicleDriverController } from './vehicle-driver.controller';
import { VehicleDriverService } from './vehicle-driver.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {VehicleDriverRepository} from './vehicle-driver.repository';

@Module({
  imports: [
      TypeOrmModule.forFeature([VehicleDriverRepository]),
  ],
  exports: [VehicleDriverService],
  controllers: [VehicleDriverController],
  providers: [VehicleDriverService],
})
export class VehicleDriverModule {}
