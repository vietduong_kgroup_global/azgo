import { Injectable } from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {VehicleDriverRepository} from './vehicle-driver.repository';
import {CreateVehicleDriverDto} from './dto/create-vehicle-driver.dto';
import {VehicleDriverEntity} from './vehicle-driver.entity';

@Injectable()
export class VehicleDriverService {
    constructor(
        @InjectRepository(VehicleDriverRepository)
        private readonly vehicleDriverRepository: VehicleDriverRepository,
    ) {
    }

    /**
     * This is function find by driver and vehicle
     * @param driver_id
     */
    async findByDriver(driver_id: number): Promise<VehicleDriverEntity> {
        return await this.vehicleDriverRepository.findOne({
            where: {
                driver_id,
            },
            relations: ['vehicle'],
        });
    }

    /**
     * This is function find by vehicle
     * @param vehicle_id
     */
    async findByVehicle(vehicle_id: number): Promise<VehicleDriverEntity> {
        return await this.vehicleDriverRepository.findOne({
            where: {
                vehicle_id,
            },
        });
    }

    async save(data: CreateVehicleDriverDto): Promise<VehicleDriverEntity> {
        return await this.vehicleDriverRepository.save(data);
    }

    async updateById(id: number, data: CreateVehicleDriverDto): Promise<any> {
        const rs = await this.vehicleDriverRepository.findOneOrFail(id);
        if (rs) {
            return await this.vehicleDriverRepository.update(id, data);
        }
        return rs;
    }

    async deleteById(id: number): Promise<any> {
        const rs = await this.vehicleDriverRepository.findOneOrFail(id);
        if (rs) {
            return  await this.vehicleDriverRepository.delete(id);
        }
        return rs;
    }

    async deleteByVehicle(vehicle_id: number): Promise<any> {
        const rs = await this.vehicleDriverRepository.find({
            vehicle_id,
        });
        if (rs) {
            return  await this.vehicleDriverRepository.delete({
                vehicle_id,
            });
        }
        // Return true b/c bus not choose driver before as bike car or van ba gac
        return true;
    }

    async getAllDriver() {
        const rs = await this.vehicleDriverRepository.find();
        let arrDriverId = [];
        rs.forEach((v) => {
            if (arrDriverId.indexOf(v.driver_id) < 0) {
                arrDriverId.push(v.driver_id);
            }
        });
        return arrDriverId;

    }

    async getAllDriverWithTotalSeat(options?: any) {
        const rs = await this.vehicleDriverRepository.getAllVehicleWithTotalSeat(options);
        let arrDriverId = [];
        rs.forEach((v) => {
            if (arrDriverId.indexOf(v.driver_id) < 0) {
                arrDriverId.push(v.driver_id);
            }
        });
        return arrDriverId;

    }
}
