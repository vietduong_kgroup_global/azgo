import { Brackets, EntityRepository, Repository } from 'typeorm';
import {VehicleDriverEntity} from './vehicle-driver.entity';

@EntityRepository(VehicleDriverEntity)
export class VehicleDriverRepository extends Repository<VehicleDriverEntity> {
    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     * @param options
     */
    async getAllVehicleWithTotalSeat(options?: any) {
        let query = this.createQueryBuilder('az_vehicle_driver');
        query.leftJoinAndSelect('az_vehicle_driver.vehicle', 'vehicle')
        .leftJoinAndSelect('vehicle.vehicleType', 'vehicleType')
        // .leftJoinAndSelect('vehicle.vehicleType', 'vehicleType')
        // not delete_at

        // search by keyword
        if (!!options.total_seat) {
            query.andWhere('vehicleType.total_seat  = :total_seat', {total_seat: options.total_seat});
        }
        // limit + offset
        query.orderBy('az_vehicle_driver.id', 'DESC')

        const results = await query.getManyAndCount();
        // results[0].forEach(element => {
        // });
        return results[0];
    }

}
