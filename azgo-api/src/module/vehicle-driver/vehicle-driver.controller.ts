import {Body, Controller, Delete, HttpStatus, Param, Post, Put, Res, UseGuards} from '@nestjs/common';
import {VehicleDriverService} from './vehicle-driver.service';
import { Response } from 'express';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {CreateVehicleDriverDto} from './dto/create-vehicle-driver.dto';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@ApiUseTags('Vehicle driver')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('vehicle-driver')
export class VehicleDriverController {
    constructor(private readonly vehicleDriverService: VehicleDriverService) {
    }

    @Post('/')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @ApiOperation({ title: 'Add vehicle driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async saveVehicleDiver(@Body() body: CreateVehicleDriverDto, @Res() res: Response) {
        try {
            const result = await this.vehicleDriverService.save(body);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:vehicle_driver_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @ApiOperation({ title: 'Edit vehicle driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'vehicle_driver_id', description: 'vehicle driver id', required: true, type: 'number' })
    async updateVehicleSchedule(@Param('vehicle_driver_id') vehicle_driver_id,  @Body() body: CreateVehicleDriverDto, @Res() res: Response) {
        try {
            const result = await this.vehicleDriverService.updateById(vehicle_driver_id, body);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:vehicle_driver_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @ApiOperation({ title: 'Delete vehicle driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'vehicle_driver_id', description: 'Vehicle driver id', required: true, type: 'number' })
    async deleteVehicleDriver(@Param('vehicle_driver_id') vehicle_driver_id, @Res() res: Response) {
        try {
            const result = await this.vehicleDriverService.deleteById(vehicle_driver_id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
