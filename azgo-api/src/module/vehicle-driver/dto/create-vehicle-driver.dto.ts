import {ApiModelProperty} from '@nestjs/swagger';
import {IsInt, IsNotEmpty, Validate} from 'class-validator';
import {DriverProfileEntityExists} from '../../validator-module/entity-driver-profile-exists.constraint';
import {VehicleEntityExists} from '../../validator-module/entity-vehicle-exists.constraint';

export class CreateVehicleDriverDto {

    @IsNotEmpty()
    @ApiModelProperty({example: 1, required: true})
    @IsInt()
    @Validate(DriverProfileEntityExists, {message: 'Driver profile doesn`t exists'})
    driver_id: number;

    @IsNotEmpty()
    @ApiModelProperty({example: 1, required: true})
    @IsInt()
    @Validate(VehicleEntityExists, {message: 'Vehicle doesn`t exists'})
    vehicle_id: number;

}
