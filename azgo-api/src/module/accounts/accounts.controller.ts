import { Controller, Get, Post, Put, HttpStatus, Req, Res, UseGuards, Param, Body } from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
    ApiImplicitParam,
} from '@nestjs/swagger';
import { Response } from 'express';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { AccountsService } from './accounts.service';
import { UserAccountService } from '../user-account/user-account.service';
import { UsersService } from '../users/users.service';
import { AuthGuard } from '@nestjs/passport';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { CreateAccountDto } from './dto/create-accounts.dto';
import * as jwt from 'jsonwebtoken';
@ApiUseTags('Accounts')
@Controller('accounts')
export class AccountsController {
    constructor(
        private readonly accountService: AccountsService,
        private readonly userService: UsersService,

    ) { }

    @Put('/createUpdateAccount/:user_uuid')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update account' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiImplicitParam({ name: 'user_uuid', description: 'User UUID', required: true, type: 'string' })
    async createUpdateAccount(@Req() request, @Param('user_uuid') user_uuid, @Body() body: CreateAccountDto, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const data_user: any = jwt.decode(token);
            const result = await this.accountService.createAccount(body, user_uuid, data_user);
            if (result.success) {
                return res.status(HttpStatus.CREATED).send(dataSuccess('oK', result.data));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.error.message || 'Bad requests', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
    @Get('/getAccount/:user_uuid')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN] })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update account' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiImplicitParam({ name: 'user_uuid', description: 'Get account by user UUID', required: true, type: 'string' })
    async get(@Param('user_uuid') user_uuid, @Res() res: Response) {
        try {
            let result = await this.accountService.getAccount(user_uuid);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
