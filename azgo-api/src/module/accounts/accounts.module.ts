import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountsController } from './accounts.controller';
import { AccountsService } from './accounts.service';
import { AccountsRepository } from './accounts.repository';
import { CommonService } from '../../helpers/common.service';
import { UserAccountService } from '../user-account/user-account.service';
import { UserAccountModule } from '../user-account/user-account.module';
import { UserAccountRepository } from '../user-account/user-account.repository';
import { UsersModule } from '../users/users.module';
 
@Module({
    imports: [TypeOrmModule.forFeature([AccountsRepository, UserAccountRepository]), UserAccountModule,
    forwardRef(() => UsersModule),
    ],
    providers: [AccountsService, CommonService, UserAccountService],
    exports: [AccountsService],
    controllers: [AccountsController],
})
export class AccountsModule { }
