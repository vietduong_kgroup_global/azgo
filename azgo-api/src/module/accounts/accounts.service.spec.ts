import { Test, TestingModule } from '@nestjs/testing';
import { AccountsController } from './accounts.controller';
 
describe('AccountsController', () => {
  let service: AccountsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AccountsController],
    }).compile();

    service = module.get<AccountsController>(AccountsController);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
