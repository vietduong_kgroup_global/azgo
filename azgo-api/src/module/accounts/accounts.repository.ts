import { EntityRepository, Repository, getManager } from 'typeorm';
import { AccountsEntity } from './accounts.entity';

@EntityRepository(AccountsEntity)
export class AccountsRepository extends Repository<AccountsEntity> {
    /**
     * This function get role by id
     * @param id
     */
    async findById(id: number): Promise<AccountsEntity> {
        let res = await this.findOne(id)
        return res;
    }
    async updateByAccountId(account_id: number, data_update) {
        await this.update({ id: account_id }, data_update); 
        return await this.findOne({ id: account_id })
    }  
    async getAccount(id: number) {
        // let res = await this.findById(id)
        let query = this.createQueryBuilder('az_accounts');
        query.leftJoinAndSelect('az_accounts.bankInfo', 'bankInfo');
        query.andWhere(`az_accounts.id = :account_id`, { account_id: id });
        const result = await query.getOne();
        return result;
    }
     
}