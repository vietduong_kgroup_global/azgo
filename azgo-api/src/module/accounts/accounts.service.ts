import { Injectable, Inject, forwardRef} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, ObjectID } from 'typeorm';
import { AccountsEntity } from './accounts.entity';
import { UserAccountEntity } from '../user-account/user-account.entity';
import { UserAccountService } from '../user-account/user-account.service';
import { AccountsRepository } from './accounts.repository';
import { BaseService } from '../../base.service';
import { UsersService } from '../users/users.service';
import { UserAccountLogEntity } from '../user-account-log/user-account-log.entity';

@Injectable()
export class AccountsService extends BaseService<AccountsEntity, AccountsRepository> {
    constructor(
        repository: AccountsRepository,
        @Inject(forwardRef(() => UsersService))
        private readonly usersService: UsersService,
        private readonly userAccountService: UserAccountService,

    ) {
        super(repository);
    }

    /**
     * This function get role by id
     * @param id
     */
    async findOne(id: number): Promise<AccountsEntity> {
        return this.repository.findById(id);
    }
    async findById(id: number) {
        return await this.repository.findOneOrFail(id);
    }
    async createAccount(body: any, user_uuid: string, data_user: any) {
        try {
            let account_res;
            let accountId;
            await getManager().transaction(async transactionalEntityManager => {
                const user_data = await this.usersService.findByUuid(user_uuid);
                const check_user_account = await this.userAccountService.findByUserId(user_data.id);
                if (check_user_account) {
                    accountId = check_user_account.account_id;
                    let data_update = {};
                    if (body.hasOwnProperty("account_bank_branch")) data_update = {...data_update, account_bank_branch: body.account_bank_branch};
                    if (body.hasOwnProperty("account_bank_name") ) data_update = {...data_update, account_bank_name: body.account_bank_name};
                    if (body.hasOwnProperty("account_bank_code"))  data_update = {...data_update, account_bank_code: body.account_bank_code};
                    if (body.hasOwnProperty("account_bank_number"))  data_update = {...data_update, account_bank_number: body.account_bank_number};
                    if (body.hasOwnProperty("account_bank_user_name")) data_update = {...data_update, account_bank_user_name: body.account_bank_user_name};
                    if (body.hasOwnProperty("is_active"))  data_update = {...data_update, is_active: (body.is_active === true || body.is_active === 1) ? 1 : 0};  
             
                    if (Object.entries(data_update) && Object.entries(data_update).length)
                        await transactionalEntityManager.update(
                            AccountsEntity,  // entity
                            {id: check_user_account.account_id}, // condition update
                            data_update, // params update
                        );
                    // account_res = await this.repository.updateByAccountId(check_user_account.account_id, data_update);
                    let user_account_log = {
                        account_id: accountId,
                        user_id: user_data.id,
                        user_action_id: data_user.id,
                        type: 'UPDATE',
                        data: data_update,
                    };
                    await transactionalEntityManager.save(UserAccountLogEntity, user_account_log);
                }
                else {
                    account_res = await transactionalEntityManager.save(AccountsEntity, body);
                    accountId = account_res.id;
                    let account_data = {
                        id: accountId,
                        account_bank_branch: body.account_bank_branch,
                        account_bank_name: body.account_bank_name,
                        account_bank_code: body.account_bank_code,
                        account_bank_number: body.account_bank_number,
                        account_bank_user_name: body.account_bank_user_name,
                        is_active: (body.is_active === true || body.is_active === 1) ? 1 : 0,
                    };
                    let user_account = {
                        user: user_data,
                        account: account_data,
                    };
                    let user_account_log = {
                        account_id: accountId,
                        user_id: user_data.id,
                        user_action_id: data_user.id,
                        type: 'CREATE',
                        data: user_account,
                    }
                    await transactionalEntityManager.save(UserAccountLogEntity, user_account_log);
                    await transactionalEntityManager.save(UserAccountEntity, user_account);
                }

            }); 
            account_res = await this.findOne(accountId);
            return {
                success: true,
                data: accountId ? account_res : {},
            };
        } catch (error) {
            console.log("error", error)
            return {
                success: false,
                error: error
            }
        }
    }
    async getAccount(user_uuid: string) {
        try {
            const user_data = await this.usersService.findByUuid(user_uuid);
            if (user_data) {
                const user_account = await this.userAccountService.findByUserId(user_data.id);
                if (user_account) return await this.repository.getAccount(user_account.account_id);
                return null;
            }
            return null;
        } catch (error) {
            console.log("error", error);
            return null;
        }
    }

    async getAccountByUserId(user_id: number) {
        try {
            const user_data = await this.usersService.findById(user_id);
            if (user_data) {
                const user_account = await this.userAccountService.findByUserId(user_data.id);
                if (user_account) return await this.repository.getAccount(user_account.account_id);
                return null;
            }
            return null;
        } catch (error) {
            console.log("error", error);
            return null;
        }
    }
}
