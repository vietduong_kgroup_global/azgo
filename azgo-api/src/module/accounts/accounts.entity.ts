import { BaseEntity, Entity, Column, PrimaryGeneratedColumn, OneToOne, OneToMany, ManyToOne, JoinColumn} from 'typeorm';
import { UserAccountEntity } from '../user-account/user-account.entity';
import { UserAccountLogEntity } from '../user-account-log/user-account-log.entity';
import { BankEntity } from '../bank/bank.entity';

@Entity('az_accounts')
export class AccountsEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '50',
    })
    account_bank_number: string;

    @Column({
        type: 'varchar',
        length: '50',
    })
    account_bank_name: string;

    @Column({
        type: 'varchar',
        length: '50',
    })
    account_bank_code: string;

    @Column({
        type: 'varchar',
        length: '50',
    })
    account_bank_branch: string;

    @Column({
        type: 'varchar',
        length: '50',
    })
    account_bank_user_name: string;

    @Column({
        type: 'smallint',
        default: 0,
    })
    is_active: number;

    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @OneToOne(() => UserAccountEntity, user => user.account)
    userAccount: UserAccountEntity;

    @OneToMany(type => UserAccountLogEntity, user_account_log => user_account_log.accountProfile)
    userAccountLog: UserAccountLogEntity[];

    @ManyToOne(type => BankEntity, id => id.bankAccount)
    @JoinColumn({ name: 'account_bank_code', referencedColumnName: 'bank_code' })
    bankInfo: AccountsEntity;
}
