import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt, IsNotEmpty, IsString, MaxLength} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';
// import {AccountName} from '../../../enums/type.enum';
export class CreateAccountDto {
    @IsNotEmpty() 
    @IsString()
    @ApiModelProperty({example: 'Field bank_code in bankEntity', description: 'bank_code in bankEntity'})
    // @ApiModelProperty({type: AccountName, required: true, default: 1})
    account_bank_code: string;

    @IsNotEmpty() 
    @IsString()
    @ApiModelProperty({example: 'Agribank'}) 
    account_bank_name: string;

    @IsNotEmpty() 
    @IsString()
    @ApiModelProperty({example: 'Phu nhuan'}) 
    account_bank_branch: string;

    @IsNotEmpty() 
    @IsString()
    @ApiModelProperty({example: 'Nguyen Minh Thong'}) 
    account_bank_user_name: string;

    @IsNotEmpty() 
    @IsString()
    @ApiModelProperty({example: '123456789'}) 
    account_bank_number: string; 

    @IsInt()
    @ApiModelProperty({example: 0}) 
    is_active: number; 
    
}
