import {forwardRef, Inject, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {RatingScheduleEntity} from './rating-schedule.entity';
import {RatingScheduleRepository} from './rating-schedule.repository';
import {VehicleScheduleService} from '../vehicle-schedule/vehicleSchedule.service';

@Injectable()
export class RatingScheduleService {
    constructor(
        @InjectRepository(RatingScheduleEntity)
        private readonly ratingScheduleRepository: RatingScheduleRepository,
        @Inject(forwardRef(() => VehicleScheduleService))
        private readonly scheduleService: VehicleScheduleService,
    ) {
    }

    async getAll(limit: number, offset: number, options?: any) {
        let query = this.ratingScheduleRepository.createQueryBuilder('rating')
                .leftJoinAndSelect('rating.scheduleEntity', 'scheduleEntity')
                .leftJoinAndSelect('rating.customerProfile', 'customerProfile');
        // search by keyword
        if (options.provider_id) {
           query.where('scheduleEntity.provider_id = :provider_id', {provider_id: options.provider_id});
        }
        // limit + offset
        query.orderBy('rating.created_at', 'DESC')
            .limit(limit)
            .offset(offset);
        const results = await query.getManyAndCount();
        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }
    async customerRating(data) {
        return await this.ratingScheduleRepository.save(data);
    }

    async avgRatingByListScheduleId(list_id: Array<number>) {
        return await this.ratingScheduleRepository.createQueryBuilder('rating')
            .where('rating.schedule_id in(:list_id)', {list_id})
            .select('AVG(rating.point)', 'avg')
            .getRawOne();
    }
    async avgRatingDriverByDriverId(driver_id: number) {
        // Get list schedule id
        const scheduleEntity = await this.scheduleService.getAllByDriver(driver_id, 3); // 3 => chuyen da hoan thanh
        const listIdSchedule = [];
        await Promise.all(scheduleEntity.map((item) => {
            listIdSchedule.push(item.id);
        }));
        return  await this.avgRatingByListScheduleId(listIdSchedule);

    }

    async avgRatingByScheduleId(schedule_id: number) {
        return await this.ratingScheduleRepository.createQueryBuilder('rating')
            .where('rating.schedule_id = :schedule_id', {schedule_id})
            .select('AVG(rating.point)', 'avg')
            .getRawOne();
    }

    async getAvgRatingByProvider(provider_id: number) {
        // Get list vehicle schedule by provider
        const scheduleEntity = await this.scheduleService.getAllByProvider(provider_id, 3); // 3 => chuyen da hoan thanh
        const listIdSchedule = [];
        await Promise.all(scheduleEntity.map((item) => {
            listIdSchedule.push(item.id);
        }));
        return  await this.avgRatingByListScheduleId(listIdSchedule);
    }
}
