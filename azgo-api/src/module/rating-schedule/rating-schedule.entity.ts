import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {IsInt, IsNotEmpty, IsString, Validate} from 'class-validator';
import {CustomerProfileEntityExists} from '../validator-module/entity-customer-profile-exists.constraint';
import {ApiModelProperty} from '@nestjs/swagger';
import {CustomerProfileEntity} from '../customer-profile/customer-profile.entity';
import {VehicleScheduleEntity} from '../vehicle-schedule/vehicleSchedule.entity';

@Entity('az_rating_schedules')
export class RatingScheduleEntity {
    @Exclude()
    @PrimaryGeneratedColumn({type: 'bigint', unsigned: true})
    id: number;

    @Column('int')
    @IsInt({message: 'Schedule info must be a integer'})
    @ApiModelProperty({example: 1, required: true})
    schedule_id: number;

    @Validate(CustomerProfileEntityExists, {message: 'Customer profile doesn`t exists'})
    @Column('int')
    @IsInt({message: 'Customer profile must be a integer'})
    @ApiModelProperty({example: 1, required: true})
    customer_id: number;

    @Column('text')
    @IsString({message: 'Content must be a string'})
    @IsNotEmpty({message: 'Content is require'})
    @ApiModelProperty({example: 'Xe ngồi thoải mái, êm...', required: true})
    content: string;

    @Column('decimal')
    @IsNotEmpty({message: 'Point map is require'})
    @ApiModelProperty({example: '4.5', required: true})
    point: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @ManyToOne(type => VehicleScheduleEntity, schedule => schedule.ratingScheduleEntities)
    @JoinColumn({name: 'schedule_id'})
    scheduleEntity: VehicleScheduleEntity;

    @ManyToOne(type => CustomerProfileEntity, customer => customer.user_id)
    @JoinColumn({name: 'customer_id', referencedColumnName: 'user_id'})
    customerProfile: CustomerProfileEntity;
}
