import {forwardRef, Module} from '@nestjs/common';
import { RatingScheduleService } from './rating-schedule.service';
import { RatingScheduleController } from './rating-schedule.controller';
import {TypeOrmModule} from '@nestjs/typeorm';
import {RatingScheduleEntity} from './rating-schedule.entity';
import {UsersModule} from '../users/users.module';
import {VehicleScheduleModule} from '../vehicle-schedule/vehicleSchedule.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([RatingScheduleEntity]),
    UsersModule,
    forwardRef(() => VehicleScheduleModule),
  ],
  providers: [RatingScheduleService],
  controllers: [RatingScheduleController],
  exports: [RatingScheduleService],
})
export class RatingScheduleModule {}
