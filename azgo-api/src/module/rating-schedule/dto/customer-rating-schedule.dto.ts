import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt, IsNotEmpty, IsString, Max, MaxLength, Min} from 'class-validator';
export class CustomerRatingScheduleDto {

    @IsNotEmpty()
    @ApiModelProperty({example: 1})
    @IsInt()
    schedule_id: number;

    @IsNotEmpty()
    @ApiModelProperty({example: 'Xe chay rat em'})
    @IsString()
    @MaxLength(500, {message: 'Content max length is 500'})
    content: string;

    @IsNotEmpty()
    @ApiModelProperty({example: 4})
    @Max(5)
    @Min(0)
    point: number;
}
