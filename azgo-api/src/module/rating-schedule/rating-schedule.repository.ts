import {EntityRepository, Repository} from 'typeorm';
import {RatingScheduleEntity} from './rating-schedule.entity';
@EntityRepository(RatingScheduleEntity)
export class RatingScheduleRepository extends Repository<RatingScheduleEntity>{

}
