import {Body, Controller, Get, HttpStatus, Param, Post, Req, Res, UseGuards} from '@nestjs/common';
import { AuthGuard} from '@nestjs/passport';
import {ApiBearerAuth, ApiImplicitQuery, ApiOperation, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey  } from '@azgo-module/core/dist/common/guards/roles.guards';
import { RatingScheduleService } from './rating-schedule.service';
import { CustomerRatingScheduleDto } from './dto/customer-rating-schedule.dto';
import * as jwt from 'jsonwebtoken';
import { Response } from 'express';
import { UsersService } from '../users/users.service';
import { VehicleScheduleService } from '../vehicle-schedule/vehicleSchedule.service';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';

@Controller('rating-schedule')
@ApiUseTags('Rating schedule')
export class RatingScheduleController {
    constructor(
        private readonly ratingScheduleService: RatingScheduleService,
        private readonly userService: UsersService,
        private readonly scheduleService: VehicleScheduleService,
    ) { }
    @Get()
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All  })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get rating schedule' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'provider_id', description: 'Provider id (ProviderProfille entity)', required: false, type: 'number'})
    async findAll(@Req() request, @Res() res: Response) {
        let results;

        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.ratingScheduleService.getAll(per_page, offset, request.query);
        } catch (e) {
            console.error('[findAllUsers] error', e);
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).
            send(dataError('current_page is not greater than total_page', null));
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));

    }

    @Post('customer-rating-schedule')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Customer rating scheudle' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async customerRatingSchedule(@Req() request, @Body() body: CustomerRatingScheduleDto, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        // decode token
        const customerInfo: any =  jwt.decode(token);
        try {
            const dataInsert = {...body, ...{customer_id: customerInfo.id}};
            const result = await this.ratingScheduleService.customerRating(dataInsert);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('avg-rating-driver')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All,
        user_type: [UserType.DRIVER_BIKE_CAR, UserType.DRIVER_BUS, UserType.DRIVER_VAN_BAGAC ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Avg rating by driver bus' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async avgRatingDriver(@Req() request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        // decode token
        const driverInfo =  jwt.decode(token);
        const userProfile = await this.userService.findOneByUserInfo(driverInfo);
        // Get list schedule id
        const scheduleEntity = await this.scheduleService.getAllByDriver(userProfile.id);
        const listIdSchedule = [];
        await Promise.all(scheduleEntity.map((item) => {
            listIdSchedule.push(item.id);
        }));
        // Avg rating by schedule
        const ratingSchedule = await this.ratingScheduleService.avgRatingByListScheduleId(listIdSchedule);
        return res.status(HttpStatus.OK).send(dataSuccess('oK', Number(ratingSchedule.avg).toFixed(2)));

    }

    @Get('get-avg-rating-by-provider')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.PROVIDER]})
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get avg rating by provider' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getAvgRatingByProvider(@Req() request, @Res() res: Response){
        try {
            const token = request.headers.authorization.split(' ')[1];
            const providerInfo: any =  jwt.decode(token);
            const result = await this.ratingScheduleService.getAvgRatingByProvider(providerInfo.id);
            if (result){
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
