import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RequestWithdrawController } from './request-withdraw.controller';
import { RequestWithdrawService } from './request-withdraw.service';
import { RequestWithdrawRepository } from './request-withdraw.repository';
import { UsersModule } from '../users/users.module';
import { WalletModule } from '../wallet/wallet.module';
import { AccountsModule } from '../accounts/accounts.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([RequestWithdrawRepository]),
    UsersModule,
    WalletModule,
    AccountsModule
  ],
  controllers: [RequestWithdrawController],
  providers: [RequestWithdrawService],
  exports: [RequestWithdrawService],
})
export class RequestWithdrawModule {}
