
import { Entity, PrimaryGeneratedColumn, Generated, JoinColumn, ManyToOne, OneToMany, Column, BaseEntity } from 'typeorm';
import { UsersEntity } from '../users/users.entity';
import { RequestWithdrawHistoryEntity } from '../request-withdraw-history/request-withdraw-history.entity';
import { Exclude } from 'class-transformer';
import { IsInt, IsNotEmpty } from 'class-validator';

@Entity('az_request_withdraw')
export class RequestWithdrawEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    @Generated('uuid')
    @IsNotEmpty()
    uuid: string;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    request_code: string;

    @Column({
        type: 'int',
        nullable: false,
    })
    driver_id: number;

    @ManyToOne(type => UsersEntity, user => user.driverRequests)
    @JoinColumn({ name: 'driver_id', referencedColumnName: 'id' })
    driverInfo: UsersEntity;

    @Column({
        type: 'smallint',
    })
    type: number;

    @Column({
        type: 'varchar',
    })
    reason_cancel: string;

    @Column({
        type: 'varchar',
    })
    reason_decline: string;

    @Column({
        type: 'double',
    })
    amount: number;

    @Column({
        type: 'varchar',
        comment: 'Ma giao dịch',
    })
    payment_code: string;

    @Column({
        type: 'varchar',
        comment: 'Noi dung',
    })
    content: string;

    @Column({
        type: 'smallint',
        default: 1,
        comment: '1: Chờ xác nhận , 2: Tài xế xác nhận, 3: Khách hàng xác nhận, 4: Tài xế huỷ, 5: Khách hàng huỷ, 6: Hoàn tất',
    })
    @IsNotEmpty()
    status: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
    })
    transfer_time: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @OneToMany(type => RequestWithdrawHistoryEntity, history => history.requestDetail)
    requestHitory: RequestWithdrawHistoryEntity[];
}