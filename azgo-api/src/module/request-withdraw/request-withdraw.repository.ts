import { Brackets, EntityRepository, Repository } from 'typeorm';
import { RequestWithdrawEntity } from './request-withdraw.entity';
import {
    STATUS_REQUEST_WITHDRAW_ARR
} from '../../constants/secrets.const';

@EntityRepository(RequestWithdrawEntity)
export class RequestWithdrawRepository extends Repository<RequestWithdrawEntity> {
    /**
     * This function get list fee by limit & offset
     * @param limit
     * @param offset
     * @param options
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        let query = this.createQueryBuilder('az_request_withdraw');
        query.leftJoinAndSelect('az_request_withdraw.requestHitory', 'requestHitory');
        query.leftJoinAndSelect('az_request_withdraw.driverInfo', 'driverInfo');
        query.leftJoinAndSelect('driverInfo.walletEntity', 'walletEntity'); 
        query.leftJoinAndSelect('driverInfo.userAccount', 'userAccount'); 
        query.leftJoinAndSelect('userAccount.account', 'account'); 
        query.leftJoinAndSelect('driverInfo.driverProfile', 'driverProfile');
        
        // search by keyword
        if (options.keyword && options.keyword.trim()) {
            options.keyword = options.keyword.trim();
            if (options.keyword.startsWith('0'))
                options.keyword = options.keyword.substring(1, options.keyword.length);
            else if (options.keyword.startsWith('+84') || options.keyword.startsWith('84'))
                options.keyword = options.keyword.substring(3, options.keyword.length);
            query.andWhere(new Brackets(qb => {
                qb.where('driverInfo.phone like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere(`driverProfile.full_name like :name`, { name: '%' + options.keyword.trim() + '%' })
            }));
        }

        // Filter by Status
        try {
            if (!Array.isArray(options.status)) options.status = JSON.parse(options.status);
        } catch (error) {
            options.status = [];
        }
        if (options.status.length) {
            query.andWhere(`az_request_withdraw.status IN (:status)`, { status: options.status });
        }

        // Filter by date create
        if (!!options.from_date && !!options.to_date) {
            query.andWhere(`az_request_withdraw.created_at >= :from_date`, { from_date: options.from_date });
            query.andWhere(`az_request_withdraw.created_at <= :to_date`, { to_date: options.to_date });
        }
        
        // not delete_at
        query.andWhere('az_request_withdraw.deleted_at is null');
        // limit + offset
        query.orderBy('az_request_withdraw.id', 'DESC')
            .limit(limit)
            .offset(offset);

        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }
    /**
     * This is function get tour by id
     * @param id
     */
    async findById(id: number) {
        return await this.findOne({
            where: { id, deleted_at: null },
            relations: [
                'driverInfo',
                'driverInfo.driverProfile',
                'driverInfo.driverProfile.areaInfo',
                'driverInfo.walletEntity',
            ],
        });
    }

    /**
     * This is function get tour by id
     * @param id
     */
    async findByUuid(uuid: string) {
        return await this.findOne({ 
            where: { uuid: uuid, deleted_at: null },
            relations: [
                'requestHitory',
                'driverInfo',
                'driverInfo.driverProfile',
                'driverInfo.driverProfile.areaInfo',
                'driverInfo.walletEntity',
                'driverInfo.userAccount',
                'driverInfo.userAccount.account'
            ],
        });
    }

    async countRequestByStatus(){
        let query = this.createQueryBuilder('az_request_withdraw')
        .select("status" )
        .addSelect("COUNT(status) AS status_count" )
        .groupBy('status');

        const results = await query.getRawMany();
        return {
            status: true,
            data: results,
            error: null,
            message: null
        };
    }
}