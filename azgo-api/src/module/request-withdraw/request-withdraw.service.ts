import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityId } from 'typeorm/repository/EntityId';
import { getManager } from 'typeorm';
import { RequestWithdrawRepository } from './request-withdraw.repository';
import { WalletService } from "../wallet/wallet.service";
import { AccountsService } from '../accounts/accounts.service';
import { RequestWithdrawHistoryEntity } from '../request-withdraw-history/request-withdraw-history.entity';
import { RequestWithdrawEntity } from './request-withdraw.entity';
import { JwtPayload } from '../auth/interfaces/jwt-payload.interface';
import {
    ERROR_UNKNOWN,
    ERROR_CAN_NOT_FIND_REQUEST_WITHDRAW,
    ERROR_DRIVER_FEE_WALLET_CAN_NOT_SMALLER_ZERO,
    ERROR_CASH_WALLET_DO_NOT_ENOUGH_TO_WITHDRAW,
    ERROR_DRIVER_DO_NOT_HAVE_ACCOUNT_BANK,
    ERROR_AMOUNT_REQUEST_DO_NOT_ENOUGH_TO_WITHDRAW,
    ERROR_DRIVER_HAVE_REQUEST_WAITING_VERIFY
} from '../../constants/error.const';
import {
    STATUS_REQUEST_WITHDRAW_ARR
} from '../../constants/secrets.const';

@Injectable()
export class RequestWithdrawService {
    constructor(
        @InjectRepository(RequestWithdrawRepository)
        private readonly repository: RequestWithdrawRepository,
        private readonly walletService: WalletService,
        private readonly accountsService: AccountsService
    ) {
    }
    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        return await this.repository.getAllByOffsetLimit(limit, offset, options);
    }

    /**
     * This function get area by tour uuis
     * @param request_uuid
     */
    async findByUuid(uuid: string) {
        return await this.repository.findByUuid(uuid);
    }

    async countRequestByStatus(){
        const count = await this.repository.countRequestByStatus();
        return {
            status: true,
            data: count,
            error: null,
            message: null
        };
    }

    async create(data: any, user_info?: JwtPayload) {
        try {
            let data_log;
            let request;
            let checkValidate = await this.checkValidateDriverForRequest(data.driver_id, data)
            if (!checkValidate.status) return checkValidate;
            let responseTransaction = await getManager().transaction(async transactionalEntityManager => {
                data.request_code = Math.round(new Date().getTime()).toString(16).toLocaleUpperCase();
                delete data.created_at;
                delete data.updated_at;
                // create request
                request = await transactionalEntityManager.save(RequestWithdrawEntity, data);
                data_log = {
                    type: 'CREATE',
                    data: data,
                    request_id: request.id,
                    user_update_id: user_info.id
                };
                // create order log
                await transactionalEntityManager.save(RequestWithdrawHistoryEntity, data_log);
                return {
                    status: true,
                    data: null,
                    error: null,
                    message: null
                };
            })
            if (!responseTransaction.status) return responseTransaction;
            delete request.password
            return {
                status: true,
                data: request,
                error: null,
                message: null
            };
        } catch (error) {
            console.log('Error in create request', error);
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message,
            };
        }
    }

    async update(request_withdraw: any, data: any, user_info?: JwtPayload) {
        try {
            let _this = this;
            let data_log;
            let request;
            let responseTransaction = await getManager().transaction(async transactionalEntityManager => {
                // Handle request log
                data_log = {
                    type: 'UPDATE',
                    data: data,
                    request_id: request_withdraw.id,
                    user_update_id: user_info.id
                };
                if (data.status && request_withdraw.status !== data.status) {
                    data_log.type = 'UPDATE_STATUS';
                }
                // create order log
                await transactionalEntityManager.save(RequestWithdrawHistoryEntity, data_log);

                if (
                    data_log.type === 'UPDATE_STATUS' && // Update trạng thái
                    data.status === STATUS_REQUEST_WITHDRAW_ARR.DECLINE_ADMIN // 6: Decline
                ) {
                    let cancelRequest = await _this.walletService.cancelRequestWithdrawCashwallet(request, transactionalEntityManager);
                    if (!cancelRequest.status)
                        return {
                            status: cancelRequest.status,
                            data: cancelRequest.data,
                            error: cancelRequest.error,
                            message: cancelRequest.message
                        };
                }
                if (
                    data_log.type === 'UPDATE_STATUS' && // Update trạng thái
                    data.status === STATUS_REQUEST_WITHDRAW_ARR.ACCEPTED_ADMIN &&// Accept
                    request.status === STATUS_REQUEST_WITHDRAW_ARR.VERIFYING // Accept
                ) {
                    let confirmRequest = await _this.walletService.confirmRequestWithdrawCashwallet(request, transactionalEntityManager);
                    if (!confirmRequest.status)
                        return {
                            status: confirmRequest.status,
                            data: confirmRequest.data,
                            error: confirmRequest.error,
                            message: confirmRequest.message
                        };
                }
                // Handle request
                request = _this.repository.merge(new RequestWithdrawEntity(), request_withdraw, data);
                delete request.driver_id; // Không được đổi cho tài xế khác
                delete request.request_code;
                delete request.created_at;
                delete request.driverInfo;
                delete request.requestHitory;
                delete request.updated_at;

                if(
                    data_log.type === 'UPDATE_STATUS'
                    && request.status === STATUS_REQUEST_WITHDRAW_ARR.TRANSFERRED_ADMIN
                    && request.transfer_time === null
                ){
                    request.transfer_time = new Date().toISOString();
                }
                
                await transactionalEntityManager.save(RequestWithdrawEntity, request);
                return {
                    status: true,
                    data: null,
                    error: null,
                    message: null
                };
            })
            if (!responseTransaction.status) return responseTransaction;
            return {
                status: true,
                data: request,
                error: null,
                message: null
            };

        } catch (error) {
            console.log('Error in update request', error);
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message,
            };
        }

    }

    async updateById(id: EntityId, data: any) {
        const rs = await this.repository.findOneOrFail(id);
        const dataUpdate = { ...rs, ...data };
        return await this.repository.save(dataUpdate);
    }

    async delete(request_withdraw: any, user_info?: JwtPayload) {
        try {
            let data_log;
            let request;
            let responseTransaction = await getManager().transaction(async transactionalEntityManager => {
                request = await this.repository.findOne({ id: request_withdraw.id });
                if (!request) {
                    return {
                        status: false,
                        data: null,
                        error: ERROR_CAN_NOT_FIND_REQUEST_WITHDRAW,
                        message: 'Không tìm thấy yêu cầu rút tiền'
                    };
                }
                request.deleted_at = new Date();
                await transactionalEntityManager.save(RequestWithdrawEntity, request);

                // Handle request log
                data_log = {
                    type: 'DELETE',
                    data: request_withdraw,
                    request_id: request_withdraw.id,
                    user_update_id: user_info.id
                };
                // create order log
                await transactionalEntityManager.save(RequestWithdrawHistoryEntity, data_log);
                return {
                    status: true,
                    data: null,
                    error: null,
                    message: null
                };
            })
            if (!responseTransaction.status) return responseTransaction;
            return {
                status: true,
                data: null,
                error: null,
                message: null
            };
        } catch (error) {
            console.log("error delete request: ", error);
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message || 'Error in delete request'
            };
        }
    }

    async checkValidateDriverForRequest(driver_id: number, request: any) {
        let walletDriver = await this.walletService.createNewWalletAccount(driver_id);
        let accountDriver = await this.accountsService.getAccountByUserId(driver_id);
        let requestWithdrawWaiting = await this.repository.findOne({
            driver_id: driver_id,
            status: STATUS_REQUEST_WITHDRAW_ARR.VERIFYING,
            deleted_at: null
        })
        if (!accountDriver || !accountDriver.account_bank_number)
            return {
                status: false,
                data: null,
                error: ERROR_DRIVER_DO_NOT_HAVE_ACCOUNT_BANK,
                message: 'Tài xế chưa có tài khoản ngân hàng'
            };
        if (walletDriver.fee_wallet < 0)
            return {
                status: false,
                data: null,
                error: ERROR_DRIVER_FEE_WALLET_CAN_NOT_SMALLER_ZERO,
                message: 'Tài khoản thanh toán của tài xế không được nhỏ hơn 0'
            };
        if (walletDriver.cash_wallet < request.amount && request.amount <= 55000)
            return {
                status: false,
                data: null,
                error: ERROR_CASH_WALLET_DO_NOT_ENOUGH_TO_WITHDRAW,
                message: 'Tài khoản doanh thu của tài xế không đủ để thực hiện yêu cầu'
            };
        if (request.amount < 55000)
            return {
                status: false,
                data: null,
                error: ERROR_AMOUNT_REQUEST_DO_NOT_ENOUGH_TO_WITHDRAW,
                message: 'Số tiền yêu cầu rút phải lớn hơn hoặc bằng 55.000 VNĐ'
            };
        if (requestWithdrawWaiting)
            return {
                status: false,
                data: null,
                error: ERROR_DRIVER_HAVE_REQUEST_WAITING_VERIFY,
                message: 'Tài xế có yêu cầu đang chờ xác nhận, vui lòng chờ yêu cầu được xác nhận để tạo thêm'
            };
        return {
            status: true,
            data: null,
            error: null,
            message: null
        };
    }
}
