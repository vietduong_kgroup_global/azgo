import { ApiModelProperty } from "@nestjs/swagger";
import { timestamp } from "aws-sdk/clients/cloudfront";
import {IsInt, IsNotEmpty, IsString, MaxLength} from "class-validator";
export class UpdateRequestWithdrawDto {
    @ApiModelProperty({ example: "XYZ", required: false })
    content: string;

    @ApiModelProperty({ example: 1, required: false, description: "Trang thai" })
    status: number;

    @ApiModelProperty({ example: "123456", required: false, description: "Mat khau tai xe" })
    password: string;
    
    @ApiModelProperty({ example: "HDUO", required: false })
    payment_code: string;
    
    @ApiModelProperty({ example: "HDUO", required: false, description: "Ly do tu choi" })
    reason_decline: string;
    
    @ApiModelProperty({ example: "HDUO", required: false, description: "Ly do huy" })
    reason_cancel: string;
}
