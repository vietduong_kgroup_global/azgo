import { ApiModelProperty } from "@nestjs/swagger";
import { timestamp } from "aws-sdk/clients/cloudfront";
import {IsInt, IsNotEmpty, IsString, MaxLength} from "class-validator";
export class RequestWithdrawDto {
    @ApiModelProperty({ example: "AZGOFEE", required: true, description: "1: Cash Wallet, 2: orther" })
    type: number;

    @ApiModelProperty({ example: 10000, required: false, description: "So tien can rut" })
    amount: number;

    @ApiModelProperty({ example: "XYZ", required: false })
    content: string;

    @ApiModelProperty({ example: 1, required: false, description: "Trang thai" })
    status: number;

    @ApiModelProperty({ example: 1, required: false, description: "Trang thai" })
    transfer_time: timestamp;

    @ApiModelProperty({ example: "7bb9-4154-b45660a18e28", required: false })
    driver_id: string;

    @ApiModelProperty({ example: "123456", required: false, description: "Mat khau tai xe" })
    password: string;
    @ApiModelProperty({ example: "HDUO", required: false })
    payment_code: string;
}
