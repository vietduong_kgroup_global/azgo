import { Body, Controller, Get, HttpStatus, Param, Post, Put, Delete, Req, Res, UseGuards } from '@nestjs/common';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { AuthGuard } from '@nestjs/passport';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiResponse,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiUseTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import * as jwt from 'jsonwebtoken';
import { RequestWithdrawService } from './request-withdraw.service';
import { RequestWithdrawDto } from './dto/request-withdraw.dto';
import { UpdateRequestWithdrawDto } from './dto/update-request.dto';
import { UsersService } from '../users/users.service';
import {
    ERROR_UNKNOWN,
    ERROR_CAN_NOT_FIND_REQUEST_WITHDRAW,
    ERROR_PASSWORD_INCORRECT,
    ERROR_PLS_INSERT_PASSWORD,
    ERROR_DRIVER_CAN_NOT_CANCEL_REQUEST,
    ERROR_USER_NOT_PERMISSION_UPDATE_REQUEST
} from '../../constants/error.const';
import {
    STATUS_REQUEST_WITHDRAW_ARR
} from '../../constants/secrets.const';

@Controller('request-withdraw')
@ApiUseTags('Request Withdraw')
export class RequestWithdrawController {
    constructor(
        private readonly requestWithdrawService: RequestWithdrawService,
        private usersService: UsersService,
    ) {
    }
    @Get('')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All})
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get list request with offset limit' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword search by name, phone driver', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'status', description: 'Status Order', required: false, type: 'json' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({ name: 'from_date', description: 'Filter Date From (Created date)', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'to_date', description: 'Filter Date To (Created date)', required: false, type: 'date' })
    async getListTour(@Req() request,  @Res() res: Response) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.requestWithdrawService.getAllByOffsetLimit(per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).
            send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }
    
    @Get('/count-request-status')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Count request (Group by status)' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'request_uuid', description: 'Fee Uuid', required: true, type: 'string' })
    async countRequest(@Req() request, @Param('request_uuid') request_uuid, @Res() res: Response) {
        try {
            const request_count = await this.requestWithdrawService.countRequestByStatus();
            if (request_count && request_count.status) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', request_count.data));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(request_count.message || 'Tạo yêu cầu bị lỗi', null, request_count.error || ERROR_UNKNOWN));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/:request_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Detail request' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'request_uuid', description: 'Fee Uuid', required: true, type: 'string' })
    async detail(@Req() request, @Param('request_uuid') request_uuid, @Res() res: Response) {
        try {
            const request_withdraw = await this.requestWithdrawService.findByUuid(request_uuid);
            if (!request_withdraw)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy yêu cầu chuyển tiền', null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', request_withdraw));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post()
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create new request withdraw' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async create(@Req() request, @Body() body: RequestWithdrawDto, @Res() res: Response) {
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        try {
            if(data_user.current_role === 'driver') {
                if(!body.password) return res.status(HttpStatus.BAD_REQUEST).send(dataError('Vui lòng nhập khẩu không đúng', null, ERROR_PLS_INSERT_PASSWORD));
                const driver = await this.usersService.findOneByEmailOrPhone(data_user.phone);
                if (!driver.user.checkIfUnencryptedPasswordIsValid(body.password)) 
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Mật khẩu không đúng', null, ERROR_PASSWORD_INCORRECT));
                body.driver_id = data_user.id;
            }
            const result = await this.requestWithdrawService.create(body, data_user);
            if (result && result.status) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result.data));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Tạo yêu cầu bị lỗi', null, result.error || ERROR_UNKNOWN));
        } catch (error) {
            console.log(error);
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update one request withdraw' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'uuid', description: 'Request uuid', required: true, type: 'string' })
    async update(@Req() request, @Param('uuid') uuid, @Body() body: UpdateRequestWithdrawDto, @Res() res: Response) {
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        let request_withdraw;
        try {
            request_withdraw = await this.requestWithdrawService.findByUuid(uuid);
            if (!request_withdraw)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy yêu cầu chuyển tiền', null, ERROR_CAN_NOT_FIND_REQUEST_WITHDRAW));
            if(
                body.status &&
                request_withdraw.status !== body.status && // Cập nhật trạng thái
                body.status === STATUS_REQUEST_WITHDRAW_ARR.TRANSFERRED_ADMIN && // Admin cập nhật trạng thái ĐÃ CHUYỂN TIỀN
                !body.payment_code && !request_withdraw.payment_code // Không có Ma giao dich
            ){
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Tài xế không thể huỷ do yêu cầu đã được xác nhận', null, ERROR_DRIVER_CAN_NOT_CANCEL_REQUEST));
            }
            const result = await this.requestWithdrawService.update(request_withdraw, body, data_user);
            if (result && result.status) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result.data));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Cập nhật yêu cầu bị lỗi', null, result.error || ERROR_UNKNOWN));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/driver-update/:uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update one request withdraw for driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'uuid', description: 'Request uuid', required: true, type: 'string' })
    async updateForDriver(@Req() request, @Param('uuid') uuid, @Body() body: UpdateRequestWithdrawDto, @Res() res: Response) {
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        let request_withdraw;
        try {
            request_withdraw = await this.requestWithdrawService.findByUuid(uuid);
            if (!request_withdraw)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy yêu cầu chuyển tiền', null, ERROR_CAN_NOT_FIND_REQUEST_WITHDRAW));
            if(
                body.status &&
                request_withdraw.status !== body.status && // Cập nhật trạng thái
                body.status === STATUS_REQUEST_WITHDRAW_ARR.CANCELED_DRIVER && // Tài xế huỷ yêu cầu
                request_withdraw.status !== STATUS_REQUEST_WITHDRAW_ARR.VERIFYING // trạng thái cũ không phải CHỜ XÁC NHẬN
            ){
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Tài xế không thể huỷ do yêu cầu đã được xác nhận', null, ERROR_DRIVER_CAN_NOT_CANCEL_REQUEST));
            }
            if(request_withdraw.driver_id !== data_user.id)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Tài xế không thể Cập nhật yêu cầu không phải của mình', null, ERROR_USER_NOT_PERMISSION_UPDATE_REQUEST));

            const result = await this.requestWithdrawService.update(request_withdraw, body, data_user);
            if (result && result.status) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result.data));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Cập nhật yêu cầu bị lỗi', null, result.error || ERROR_UNKNOWN));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:uuid')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.REGION_MANAGER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update one request withdraw' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'uuid', description: 'Request uuid', required: true, type: 'string' })
    async delete(@Param('uuid') uuid, @Body() body: RequestWithdrawDto, @Res() res: Response) {
        let request_withdraw;
        try {
            request_withdraw = await this.requestWithdrawService.findByUuid(uuid);
            if (!request_withdraw)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy yêu cầu chuyển tiền', null, ERROR_CAN_NOT_FIND_REQUEST_WITHDRAW));
            
            const result = await this.requestWithdrawService.delete(request_withdraw);
            if (result && result.status) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result.data));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Xoá yêu cầu chuyển tiền lỗi', null, result.error || ERROR_UNKNOWN));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
