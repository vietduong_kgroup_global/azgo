import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FirebaseRepository } from './firebase.repository';
import { FirebaseEntity } from './firebase.entity';
import * as FCM from 'fcm-node';
import config from '../../../config/config';
const fcm = new FCM(config.env.fireBaseServerKey);

@Injectable()
export class FirebaseService {
    constructor(
        @InjectRepository(FirebaseRepository)
        private firebaseRepository: FirebaseRepository,
    ) {
    }
    /*
       @data: [ 'device_type' => 'ios, android, other', 'device_token' => 'device_token', 'user_id' => 1]
    *
    * */
    async saveFireBaseData(device_type, device_token, user_id, user_uuid) {

        if (device_type === 'ios') {
            let obj = await this.firebaseRepository.findOne({ user_id });
            if (!obj) {
                obj = new FirebaseEntity();
            }

            obj.user_id = user_id;
            obj.user_uuid = user_uuid;
            obj.apns_id = device_token;
            obj.token = null;
            return await this.firebaseRepository.save(obj);
        }
        else {
            let obj = await this.firebaseRepository.findOne({ user_id });
            if (!obj) {
                obj = new FirebaseEntity();
            }
            obj.user_id = user_id;
            obj.user_uuid = user_uuid;
            obj.token = device_token;
            obj.apns_id = null;
            return await this.firebaseRepository.save(obj);
        }
    }
    async removeDeviceTokenByUserUuid(user_uuid: string) {
        return await this.firebaseRepository.delete({ user_uuid });
    }
    /*
    * @param:
    * @tokens: ['device_token', 'apns_id', 'device_token']
    * @notification: { title: 'Title of your push notification',body: 'Body of your push notification'}
    * @data : { key_1: 'value',  key_2: 'value 2'}
    * */
    async pushNotificationWithTokens(tokens, notification, data = null) {
        let result = true;
        const message = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
            // to: 'CB9FDE3E6FA9E584',
            registration_ids: tokens,
            collapse_key: 'azgo_notification',
            notification,
            data,
        };

        await fcm.send(message, (err, response) => {
            if (err) {
                console.log('Something has gone wrong!', err);
                result = false;
            } else {
                console.log('Successfully sent with response: ', response);
                result = true;
            }
        });
        return result;
    }

    async pushNotificationWithUserIds(userIds, notification, data = null) {
        if (userIds) {
            userIds.forEach((userId) => {
                this.firebaseRepository.findOne({ user_id: userId }).then((fcmObj) => {
                    if (fcmObj) {
                        const token = fcmObj.token ? fcmObj.token : fcmObj.apns_id;
                        this.pushNotificationWithTokens([token], notification, data);
                    }
                });
            });
            return true;
        }
        return false;
    }

    async findOne(user_uuid: string) {
        return await this.firebaseRepository.findOne({ user_uuid });
    }

    async findOneByUserId(user_id: number) {
        return await this.firebaseRepository.findOne({ user_id });
    }
}
