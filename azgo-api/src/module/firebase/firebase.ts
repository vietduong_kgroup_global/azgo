import config from '../../../config/config';
import * as FCM from 'fcm-node';
const fcm = new FCM(config.env.fireBaseServerKey);

class FireBase {
    async pushNotification() {
        const message = { // this may vary according to the message type (single recipient, multicast, topic, et cetera)
            // to: 'CB9FDE3E6FA9E584',
            // 677388646851
            registration_ids: ['CB9FDE3E6FA9E584'],
            collapse_key: 'azgo_notification',

            notification: {
                title: 'Title of your push notification',
                body: 'Body of your push notification',
            },

            data: {  // you can send only notification or only data(or include both)
                my_key: 'my value',
                my_another_key: 'my another value',
            },
        };

        fcm.send(message,  (err, response) => {
            if (err) {
                console.log('Something has gone wrong!', err);
            } else {
                console.log('Successfully sent with response: ', response);
            }
        });
        return true;
    }
}
export default FireBase;
