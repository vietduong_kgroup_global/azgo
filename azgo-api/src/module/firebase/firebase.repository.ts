import {EntityRepository, Repository} from 'typeorm';
import {FirebaseEntity} from './firebase.entity';

@EntityRepository((FirebaseEntity))
export class FirebaseRepository extends Repository<FirebaseEntity>{

}
