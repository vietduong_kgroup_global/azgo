import {
    Controller,
    Body,
    Post,
    HttpStatus,
    Res,
    Req,
    UseGuards, Delete,
} from '@nestjs/common';
import {
    ApiUseTags,
    ApiResponse,
    ApiOperation,
    ApiBearerAuth,
} from '@nestjs/swagger';
import { Response } from 'express';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { FirebaseService } from './firebase.service';
import { PushNotificationDto } from './dto/pushNotification.dto';
import { AuthGuard } from '@nestjs/passport';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import * as jwt from 'jsonwebtoken';

@ApiUseTags('API Notification')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('firebase')
export class FirebaseController {
    constructor(
            private readonly firebaseService: FirebaseService,
    ) {
    }

    @Post('push-notification')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({title: 'Push notification token device'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    async pushNotification(@Req() request, @Body() body: PushNotificationDto, @Res() res: Response) {
        try {
            const checkPush = await this.firebaseService.pushNotificationWithTokens(body.device_tokens, body.message, body.data);
            console.log("checkPush: ", checkPush)
            if (checkPush)
                return res.status(HttpStatus.CREATED).send(dataSuccess('Push oke', null));

            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Push errors', null));
        }catch (e) {
            console.log(e)
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Push errors', e));
        }
    }

    @Delete('remove-device-token')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @ApiOperation({ title: 'Remove device token by user' })
    @ApiResponse({ status: 200, description: 'Remove success' })
    @ApiResponse({ status: 400, description: 'Bad request' })
    @ApiResponse({ status: 401, description: 'UNAUTHORIZED' })
    async logout(@Req() request, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const userInfo: any =  jwt.decode(token);
            const rs = await this.firebaseService.removeDeviceTokenByUserUuid(userInfo.user_id.toString());
            return res.status(HttpStatus.OK).send(dataSuccess('Remove success', rs));
        }catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Remove errors', e));
        }
    }
}
