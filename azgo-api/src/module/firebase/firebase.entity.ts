import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {UsersEntity} from '../users/users.entity';

@Entity('az_user_fcm_token')
export class FirebaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_id: number;

    @Column({
        type: 'varchar',
        length: '255',
    })
    user_uuid: string;

    @Column({
        type: 'varchar',
        length: '255',
    })
    token: string;

    @Column({
        type: 'varchar',
        length: '255',
    })
    apns_id: string;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @OneToOne(type => UsersEntity, user => user.fcmToken)
    @JoinColumn({
        name: 'user_id',
    })
    usersEntity: UsersEntity;
}
