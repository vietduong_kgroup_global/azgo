import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {FirebaseRepository} from './firebase.repository';
import {FirebaseService} from './firebase.service';
import {FirebaseController} from './firebase.controller';

@Module({
    imports: [
        TypeOrmModule.forFeature([FirebaseRepository]),
    ],
    exports: [
        FirebaseService,
    ],
    providers: [FirebaseService],
    controllers: [FirebaseController],
})
export class FirebaseModule { }
