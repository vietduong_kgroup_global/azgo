import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class PushNotificationDto {

    @IsNotEmpty()
    @ApiModelProperty({ example: { title: 'Title', body: ' Body demo ' } })
    message: object;

    @IsNotEmpty()
    @ApiModelProperty({ example: { action: 'request_bus_book_vehice', uuid: 'asxkj123jkbad1b2j' } })
    data: object;

    @IsNotEmpty()
    @ApiModelProperty({ example: ['device-token_1', 'device-token_2', 'device-token_3'] })
    device_tokens: object;
}
