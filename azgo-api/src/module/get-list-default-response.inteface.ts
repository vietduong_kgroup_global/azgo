export interface GetListDefaultResponse<T> {

    status: boolean;
    message: string;
    data: {
        count: number;
        results: T[];
        current_page: number;
        next_page: number;
        total_page: number;
    } | T[];

}
