import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    OneToOne,
    ManyToOne,
    JoinColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { UsersEntity } from '../users/users.entity';
import {ProvidersInfoEntity} from '../providers-info/providers-info.entity';
import { AreaEntity } from '../area/area.entity';

@Entity('az_driver_profile')
export class DriverProfileEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '50',
        nullable: true,
    })
    first_name: string;

    @Column({
        type: 'varchar',
        length: '50',
        nullable: true,
    })
    last_name: string;

    @Column({
        type: 'varchar',
        length: '50',
        nullable: true,
    })
    full_name: string;

    @Column({
        type: 'smallint',
        default: 1,
    })
    gender: number;

    @Column({
        nullable: true,
    })
    birthday: Date;

    @Column({
        type: 'varchar',
        length: '50',
        nullable: true,
    })
    cmnd: string;

    @Column({
        type: 'text',
        nullable: true,
    })
    address: string;

    @Column({
        type: 'text',
        nullable: true,
    })
    email: string;

    @Column({
        type: 'json',
        nullable: true,
    })
    image_profile: object;

    @Column({
        type: 'json',
        nullable: true,
    })
    front_of_cmnd: object;

    @Column({
        type: 'json',
        nullable: true,
    })
    back_of_cmnd: object;

    @Column({
        type: 'json',
        nullable: true,
    })
    front_driving_license: object;

    @Column({
        type: 'json',
        nullable: true,
    })
    back_driving_license: object;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    area_uuid: string;
    @ManyToOne(type => AreaEntity, area => area.driverProfile)
    @JoinColumn({ name: 'area_uuid', referencedColumnName: 'uuid' })
    areaInfo: AreaEntity;

    @Column({
        type: 'int',
    })
    user_id: number;

    @OneToOne(type => UsersEntity, user => user.driverProfile)
    @JoinColumn({
        name: 'user_id',
    })
    users: UsersEntity;

    @Column({
        type: 'int',
    })
    provider_id: number;

    @OneToOne(type => ProvidersInfoEntity, provider => provider.id)
    @JoinColumn({name: 'provider_id', referencedColumnName: 'id' })
    ProvidersInfoEntity: ProvidersInfoEntity;
}
