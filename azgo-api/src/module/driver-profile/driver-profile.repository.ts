import { EntityRepository, Repository } from 'typeorm';
import { DriverProfileEntity } from './driver-profile.entity';

@EntityRepository(DriverProfileEntity)
export class DriverProfileRepository extends Repository<DriverProfileEntity> {
    /**
     * This is function find by user id
     * @param user_id
     */
    async findByUserId(user_id: number): Promise<DriverProfileEntity> {
        return await this.createQueryBuilder('az_driver_profile')
            .where('az_driver_profile.user_id = :user_id', { user_id })
            .leftJoinAndSelect('az_driver_profile.users', 'users')
            .getOne();
    }

}
