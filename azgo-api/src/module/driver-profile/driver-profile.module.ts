import {forwardRef, Module} from '@nestjs/common';
import {DriverProfileController} from './driver-profile.controller';
import {TypeOrmModule} from '@nestjs/typeorm';
import {DriverProfileRepository} from './driver-profile.repository';
import {DriverProfileService} from './driver-profile.service';
import {VehicleModule} from '../vehicle/vehicle.module';
import {ProvidersInfoModule} from '../providers-info/providers-info.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([DriverProfileRepository]),
        forwardRef(() => VehicleModule), // B/c Circular dependency
        ProvidersInfoModule,
    ],
    exports: [
        DriverProfileService,
    ],
    controllers: [DriverProfileController],
    providers: [DriverProfileService],
})
export class DriverProfileModule {
}
