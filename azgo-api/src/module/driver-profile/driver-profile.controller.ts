import {Controller, Get, HttpStatus, Param, Req, Res, UseGuards} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import {DriverProfileService} from './driver-profile.service';
import {AuthGuard} from '@nestjs/passport';
import { Response } from 'express';
import {Roles} from '@azgo-module/core/dist/common/decorators/roles.decorator';
import {ActionKey, FunctionKey} from '@azgo-module/core/dist/common/guards/roles.guards';
import { dataSuccess, dataError} from '@azgo-module/core/dist/utils/jsonFormat';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import {ProvidersInfoService} from '../providers-info/providers-info.service';

@ApiUseTags('Driver')
@Controller('driver')
export class DriverProfileController {
    constructor(
        private readonly driverProfileService: DriverProfileService,
        private readonly providersInfoService: ProvidersInfoService,
    ) { }

    @Get('/get-all-driver-by-provider/:provider_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all drivers by provider' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'provider_id', description: 'Provider id', required: true, type: 'number'})
    async findAllDriverByProvider(@Req() request, @Res() res: Response, @Param('provider_id') provider_id: number) {
        if (request.user.type === UserType.PROVIDER) {
            try {
                const provider = await this.providersInfoService.findByUserId(request.user.id);
                if (!provider) return res.status(HttpStatus.BAD_REQUEST).send(dataError('Provider not found', null));
                provider_id = provider.id;
            } catch (e) {
                console.error('[findAllDriverByProvider] get provider: ', e);
                return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Bad requests', null));
            }
        }

        try {
            const result = await this.driverProfileService.getAllDriverByProvider(provider_id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (e) {
            console.error('[findAllDriverByProvider] get driver: ', e);
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Bad requests', null));
        }
    }

    @Get()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all driver' })
    @ApiImplicitQuery({name: 'full_name', description: 'Full name', required: false, type: 'string'})
    async getAll(@Req() request, @Res() res: Response) {
        try {
            let result = await this.driverProfileService.getAll(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }
    }

    @Get('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Find Driver By Id' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'number' })
    async findById(@Param('id') id, @Res() res: Response) {
        try {
            const result = await this.driverProfileService.findById(id);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
