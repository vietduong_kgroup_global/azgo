import {forwardRef, Inject, Injectable} from '@nestjs/common';
import { DriverProfileEntity } from './driver-profile.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DriverProfileRepository } from './driver-profile.repository';
import {VehicleService} from '../vehicle/vehicle.service';

@Injectable()
export class DriverProfileService {
    constructor(
        @InjectRepository(DriverProfileRepository)
        private readonly driverProfileRepository: DriverProfileRepository,
        @Inject(forwardRef(() => VehicleService))
        private readonly vehicleService: VehicleService,
    ) { }

    async getAllDriverByProvider(provider_id: number) {
        return this.driverProfileRepository.createQueryBuilder('driver')
            .leftJoinAndSelect('driver.users', 'users')
            .where('driver.provider_id = :provider_id', {provider_id})
            .andWhere('driver.deleted_at is null')
            .andWhere('users.deleted_at is null')
            .getManyAndCount();
    }
    /**
     * This is function get all driver
     * @param options
     */
    async getAll(options?: any) {
        return await this.driverProfileRepository.find({
            relations: ['users'],
        });
    }

    /**
     * This is function find by id
     * @param id
     */
    async findById(id: number) {
        return await this.driverProfileRepository.findOne(id);
    }

    /**
     * This is function get driver profile by user_id
     * @param user_id
     */
    async findByUserId(user_id: number): Promise<DriverProfileEntity> {
        return await this.driverProfileRepository.findByUserId(user_id);
    }

}
