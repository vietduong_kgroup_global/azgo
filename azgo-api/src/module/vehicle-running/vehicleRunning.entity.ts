import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {VehicleScheduleEntity} from '../vehicle-schedule/vehicleSchedule.entity';

@Entity('az_vehicle_running')
export class VehicleRunningEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    schedule_id: number;

    @OneToOne(type => VehicleScheduleEntity, schedule => schedule.id)
    @JoinColumn({name: 'schedule_id', referencedColumnName: 'id'})
    vehicleSchedule: VehicleScheduleEntity;

    @Column()
    vehicle_id: number;

    @Column()
    status: number;

    @Column({
        type: 'json',
    })
    router: object;

    @Column({
        type: 'json',
    })
    seat_map: object;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;
}
