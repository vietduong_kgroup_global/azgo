import {EntityRepository, Repository} from 'typeorm';
import {VehicleRunningEntity} from './vehicleRunning.entity';
@EntityRepository(VehicleRunningEntity)
export class VehicleRunningRepository extends Repository<VehicleRunningEntity>{
    async createData(data){
        const checkExit = await this.findOne({ schedule_id: data.schedule_id, vehicle_id: data.vehicle_id });
        if (checkExit)
            return false;
        const vehicleRunning = new VehicleRunningEntity();
        const newData = {...vehicleRunning, ...data};
        return await this.save(newData);
    }
}
