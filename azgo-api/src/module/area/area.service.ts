import { Injectable } from '@nestjs/common';
import {AreaEntity} from './area.entity';
import {InjectRepository} from '@nestjs/typeorm';
import { EntityId } from 'typeorm/repository/EntityId';
import {ObjectID, Repository} from 'typeorm';
import {CreateAreaDto} from './dto/create-area.dto';
import { BaseService } from '../../base.service';
import {AreaRepository} from './area.repository'
@Injectable()
export class AreaService extends BaseService<AreaEntity, AreaRepository> {
    // constructor(
    //     @InjectRepository(AreaEntity)
    //     private readonly areaRepository: AreaRepository,
    //  )
    // {}
    constructor(repository: AreaRepository ) {
        super(repository); 
    } 
    async findAll(options?: any){
        let query = await this.repository.createQueryBuilder('area');
        if (!options.keyword === false) {
            query.where('area.name like :keyword', {keyword: '%' + options.keyword + '%' });
        }
        if (!options.type === false) {
            query.andWhere('area.type = :type', {type: options.keyword});
        }
        query.andWhere('area.deleted_at is null');
        query.orderBy('area.id', 'DESC');
        return await query.getMany();
    }

    async findById(id: number) {
        return await this.repository.findOneOrFail(id);
    }

    /**
     * This function get area by tour uuis
     * @param area_uuid
     */
    async findByUuid(uuid: string){
        return await this.repository.findByUuid(uuid);
    }


    async findByName(name: string) {
        console.log(name)
        return await this.repository.createQueryBuilder('area')
            .where('area.name like :name', {name: '%' + name + '%'})
            .getOne();
    }
    async createArea(body: any) {
        return await this.repository.save(body);
    }

    async deleteArea(id: number){
        const obj = await this.repository.findOne({id});
        if (!obj)
            return false;
        obj.deleted_at = new Date();
        return await this.repository.save(obj);
    }

    // async updateArea(area_uuid: string, body?: any) {
    //     console.log('area_uuid',area_uuid)
    //     const area = await this.repository.findByUuid(area_uuid);
    //     console.log('area',area)
    //     if (!area) throw { message: 'Khu vực không tìm thấy', status: 404 };
    //     const dataSave = {...area, ...body};
    //     console.log('body',body)
    //     console.log('dataSave',dataSave)
    //     return await this.update(area.id, dataSave);

    // }
    async updateById(id: EntityId, data: CreateAreaDto){ 
        return await this.update(id, data);
    }

}
