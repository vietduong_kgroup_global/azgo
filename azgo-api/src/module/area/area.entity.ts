import { Column, Entity, PrimaryGeneratedColumn, OneToMany, JoinColumn, Generated, BaseEntity } from 'typeorm';
import { Exclude } from 'class-transformer';
import { IsNotEmpty, MaxLength} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';
import { AdditionalFeeEntity } from '../additional-fee/additional-fee.entity';
import { AdminProfileEntity } from '../admin-profile/admin-profile.entity';
import { DriverProfileEntity } from '../driver-profile/driver-profile.entity';
import { TourEntity } from '../tour/tour.entity';

@Entity('az_area')
export class AreaEntity  extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'binary',
        length: 36,
    })
    @Generated('uuid')
    uuid: string;

    @Column('varchar')
    // @MaxLength(255)
    @IsNotEmpty()
    @MaxLength(255)
    name: string;

    @Column({
        type: 'json',
    })
    coordinates: object;

    @Column('smallint')
    @IsNotEmpty({message: 'Type is require'})
    @ApiModelProperty({ example: 1, required: true, description: '1: Tỉnh - Thành Phố, 2: Quận - Huyện', default: 1 })
    type: number;

    @Column({
        type: 'json',
        nullable: false,
    })
    image: object;
    
    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @OneToMany(type => AdditionalFeeEntity, fee => fee.areaInfo)
    @JoinColumn({ name: 'area_uuid', referencedColumnName: 'uuid' })
    additionalFee: AdditionalFeeEntity[];

    @OneToMany(type => AdminProfileEntity, profile => profile.areaInfo)
    @JoinColumn({ name: 'area_uuid', referencedColumnName: 'uuid' })
    adminProfile: AdminProfileEntity[];

    @OneToMany(type => TourEntity, tour => tour.areaInfo)
    @JoinColumn({ name: 'area_uuid', referencedColumnName: 'uuid' })
    tourList: TourEntity[];

    @OneToMany(type => DriverProfileEntity, profile => profile.areaInfo)
    @JoinColumn({ name: 'area_uuid', referencedColumnName: 'uuid' })
    driverProfile: DriverProfileEntity[];
}
