import { Module } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {AreaEntity} from './area.entity';
import {AreaService} from './area.service';
import {AreaController} from './area.controller'; 
import {AreaRepository} from './area.repository'; 

@Module({
    imports: [
        TypeOrmModule.forFeature([AreaRepository]),
    ],
    providers: [AreaService],
    controllers: [AreaController],
    exports: [AreaService],
})
export class AreaModule {}
