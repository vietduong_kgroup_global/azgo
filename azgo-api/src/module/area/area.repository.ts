import { Brackets, EntityRepository, Repository } from 'typeorm';
import { AreaEntity } from './area.entity';

@EntityRepository(AreaEntity)
export class AreaRepository extends Repository<AreaEntity> {
    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     * @param options
     */ 
    /**
     * This is function get tour by id
     * @param id
     */
    async findById(id: number) {
        return await this.findOne({ where: { id, deleted_at: null } });
    }

    /**
     * This is function get tour by id
     * @param id
     */
    async findByUuid(uuid: string) {
        return await this.findOne({ where: {uuid: uuid} });
    }

}
