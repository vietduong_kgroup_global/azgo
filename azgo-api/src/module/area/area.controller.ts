import {
    Body,
    Controller,
    Delete, Get,
    HttpStatus,
    Param,
    Post,
    Put,
    Req,
    Res,
    UseGuards,
} from '@nestjs/common';
import {AreaService} from './area.service';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags
} from '@nestjs/swagger';
import {Roles} from '@azgo-module/core/dist/common/decorators/roles.decorator';
import {FunctionKey, ActionKey} from '@azgo-module/core/dist/common/guards/roles.guards';
import {CreateAreaDto} from './dto/create-area.dto';
import {AuthGuard} from '@nestjs/passport';
import {CustomValidationPipe} from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { Response } from 'express';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@ApiUseTags('Area')
@Controller('area')
export class AreaController {
    constructor(public service: AreaService) { 

    }

    @Get('/')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Retrieve many AreaEntity' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'type', description: 'Type', required: false, type: 'number'})
    async getRoutes(@Req() request, @Res() res: Response) { 
        try { 
            const results = await this.service.findAll(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', results));
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }
    }

    @Get('/:area_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Detail area' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'area_uuid', description: 'area Uuid', required: true, type: 'string' })
    async detail(@Req() request, @Param('area_uuid') area_uuid, @Res() res: Response) {
        try {
            console.log('area_uuid',area_uuid)
            // const token = request.headers.authorization.split(' ')[1];
            // const userInfo: any =  jwt.decode(token);
            const area = await this.service.findByUuid(area_uuid);
            if (!area)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy area', null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', area));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create one AreaEntity' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async create(@Req() request, @Body(CustomValidationPipe) body: CreateAreaDto, @Res() res: Response) {
        try { 
            const result = await this.service.createArea(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:uuid')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update one AreaEntity' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'uuid', description: 'Area uuid', required: true, type: 'string' })
    async update(@Param('uuid') id, @Body() body: CreateAreaDto, @Res() res: Response) {
        let area;
        try {
            area = await this.service.findByUuid(id);
            console.log('id',id)
            if (!area)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy area', null));
       
            const result = await this.service.updateById(area.id, body);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete one AreaEntity' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'id', description: 'Area Id', required: true, type: 'number' })
    async delete(@Param('id') id, @Res() res: Response) {
        try {
            const result = await this.service.deleteArea(id);
            if (result)
                return res.status(HttpStatus.OK).send(dataSuccess('Delete success', result));
            return res.status(HttpStatus.OK).send(dataError('Delete error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
