import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt, IsNotEmpty, IsString, MaxLength} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';
import {AreaType} from '../../../enums/type.enum';
export class CreateAreaDto {

    @IsNotEmpty()
    @ApiModelProperty({example: 'Cần Thơ'})
    @IsString()
    @MaxLength(255, {message: 'Name max length is 255'})
    name: string;

    @IsNotEmpty()
    @IsInt()
    @ApiModelProperty({type: AreaType, required: true, default: 1})
    type: AreaType;

    // @IsNotEmpty()
    @ApiModelProperty({type: CoordinatesDto, required: false, default: null})
    coordinates: CoordinatesDto;

    @ApiModelProperty({ example: {}, required: false, description: "Hình đại diện" })
    image: object;

}
