import { Injectable } from '@nestjs/common';
import {DistanceDto} from './dto/distance.dto';
let distance = require('google-distance-matrix');
import axios from 'axios';
import * as querystring from 'qs';

@Injectable()
export class GoogleMapService {
    constructor() {
    }
    /*
    * Not return distance yet
    * console.log then run
    * Way 1
    * */
    async getDistance(body: DistanceDto) {

        const origins = [body.origins.lat + ',' + body.origins.lng]; // Tọa độ điểm đi [lat,lng]
        const destinations = [body.destinations.lat + ',' + body.destinations.lng]; // Tọa độ điểm đến [lat,long]
        distance.key(process.env.GOOGLE_MAP_KEY || 'AIzaSyD1kD-DUmj9UsO2wVCGk2IqTOyZl9RysyE');
        distance.mode('driving');
        let abc = [];
        await distance.matrix(origins, destinations, (err, distances) => {
            if (!err) console.log(distances);
            return distances;
            for (let i = 0; i < origins.length; i++) {
                for (let j = 0; j < destinations.length; j++) {
                    const origin = distances.origin_addresses[i];
                    const destination = distances.destination_addresses[j];
                    if (distances.rows[0].elements[j].status === 'OK') {
                        distance = distances.rows[i].elements[j].distance.text;
                        console.log('Distance from ' + origin + ' to ' + destination + ' is ' + distance);
                        // Not push distance yet
                        abc.push(distance);
                    } else {
                        console.log(destination + ' is not reachable by land from ' + origin);
                    }
                }
            }
        });
        return distance;
    }
    /*
    * Way 2
    * Raw url gg map
    * */
    async rawUrlGetDistance(body: DistanceDto) {
        try {
            const domain = 'https://maps.googleapis.com/maps/api/distancematrix/json?';
            const origins = body.origins.lat + ',' + body.origins.lng; // Tọa độ điểm đi [lat,long]
            const destinations = body.destinations.lat + ',' + body.destinations.lng; // Tọa độ điểm đến [lat,long]
            const units = 'metric'; // km
            const key = process.env.GOOGLE_MAP_KEY ||'AIzaSyD1kD-DUmj9UsO2wVCGk2IqTOyZl9RysyE';
            const mode = body.mode;
            const paramObject = {
                origins,
                destinations,
                units,
                mode,
                key,
            };
            const signData = querystring.stringify(paramObject, { encode: true });
            return await axios.get(domain + signData );
        } catch (error) {
            console.error(error);
        }
        return false;
    }

    async getAzgoDistance(body: DistanceDto) {
        try {
            const domain = 'https://maps.googleapis.com/maps/api/distancematrix/json?';
            const origins = body.origins.lat + ',' + body.origins.lng; // Tọa độ điểm đi [lat,long]
            const destinations = body.destinations.lat + ',' + body.destinations.lng; // Tọa độ điểm đến [lat,long]
            const units = 'metric'; // km
            const key = process.env.GOOGLE_MAP_KEY ||'AIzaSyD1kD-DUmj9UsO2wVCGk2IqTOyZl9RysyE';
            const mode = body.mode;
            const paramObject = {
                origins,
                destinations,
                units,
                mode,
                key,
            };
            const signData = querystring.stringify(paramObject, { encode: true });
            let response= await axios.get(domain + signData );
            let data=response.data;
            return data;
        } catch (error) {
            console.error(error);
            return null;
        }

    }
}
