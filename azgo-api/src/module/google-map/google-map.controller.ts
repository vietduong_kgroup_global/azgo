import {Body, Controller, HttpStatus, Post,Get, Req, Res, UseGuards} from '@nestjs/common';
import {GoogleMapService} from './google-map.service';
import {AuthGuard} from '@nestjs/passport';
import {ApiBearerAuth, ApiOperation, ApiResponse} from '@nestjs/swagger';
import { Response } from 'express';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import * as jwt from 'jsonwebtoken';
import {DistanceDto} from './dto/distance.dto';

@Controller('google-map')
export class GoogleMapController {
    constructor(private readonly googleMapService: GoogleMapService) {
    }
    @Post('/test-distance-two-point-coordinates')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    // @UseGuards(AuthGuard('jwt'))
    // @ApiBearerAuth()
    @ApiOperation({title: 'Get distance'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    async pushNotification(@Req() request, @Body() body: DistanceDto, @Res() res: Response) {
        try {
            // const distance = await this.googleMapService.getDistance(body);
            const distance = await this.googleMapService.rawUrlGetDistance(body);
            if (distance)
                return res.status(HttpStatus.CREATED).send(dataSuccess('Ok', distance.data));
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('error', null));
        }catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('error', e));
        }
    }
}
