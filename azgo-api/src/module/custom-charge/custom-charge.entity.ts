import {Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {VehicleGroupsEntity} from '../vehicle-groups/vehicle-groups.entity';
import {VanBagacOrdersEntity} from '../van-bagac-orders/van-bagac-orders.entity';
import {BikeCarOrderEntity} from '../bike-car-order/bikeCarOrder.entity';

@Entity('az_custom_charges')
export class CustomChargeEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
    })
    vehicle_group_id: number;

    @OneToOne(type => VehicleGroupsEntity, vehicle_group => vehicle_group.customChargeEntity)
    @JoinColumn({
        name: 'vehicle_group_id',
    })
    vehicleGroupEntity: VehicleGroupsEntity;

    @OneToMany(type => VanBagacOrdersEntity, vanbagac => vanbagac.customCharge)
    vanBagacOrders: VanBagacOrdersEntity[];

    @OneToMany(type => BikeCarOrderEntity, bikeCar => bikeCar.customCharge)
    bikeCarOrders: VanBagacOrdersEntity[];

    @Column({
        type: 'float',
    })
    rate_of_charge: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;
}
