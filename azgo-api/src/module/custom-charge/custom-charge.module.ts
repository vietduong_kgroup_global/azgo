import { Module } from '@nestjs/common';
import { CustomChargeController } from './custom-charge.controller';
import { CustomChargeService } from './custom-charge.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {CustomChargeRepository} from './custom-charge.repository';

@Module({
     imports: [
          TypeOrmModule.forFeature([CustomChargeRepository]),
     ],
     controllers: [CustomChargeController],
     providers: [CustomChargeService],
     exports: [CustomChargeService],
})
export class CustomChargeModule {}
