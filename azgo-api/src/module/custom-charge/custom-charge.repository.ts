import { EntityRepository, Repository } from 'typeorm';
import {CustomChargeEntity} from './custom-charge.entity';

@EntityRepository(CustomChargeEntity)
export class CustomChargeRepository extends Repository<CustomChargeEntity> {
}
