import { Body, Controller, Get, HttpStatus, Param, Put, Req, Res, UseGuards } from '@nestjs/common';
import { CustomChargeService } from './custom-charge.service';
import { ApiBearerAuth, ApiImplicitParam, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import * as jwt from 'jsonwebtoken';
import { Response } from 'express';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { UpdateCustomChargeDto } from './dto/update-custom-charg.dto';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@Controller('custom-charge')
@ApiUseTags('Custom charge')
export class CustomChargeController {
    constructor(
        private  readonly customChargeService: CustomChargeService,
    ) {
    }

    @Get()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get all custom charge by admin' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getListActivityHistory(@Req() request,  @Res() res: Response) {
        try {
            const result = await this.customChargeService.getAll();
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Update custom charge by admin' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'id', description: 'Custom charge id', required: true, type: 'number'})
    async update(@Req() request, @Param('id') id: number, @Body() body: UpdateCustomChargeDto, @Res() res: Response) {
        try {
            const result = await this.customChargeService.update(id, body);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
