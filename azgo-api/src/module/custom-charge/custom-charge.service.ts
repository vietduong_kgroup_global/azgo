import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CustomChargeRepository } from './custom-charge.repository';
import { UpdateCustomChargeDto } from './dto/update-custom-charg.dto';

@Injectable()
export class CustomChargeService {
    constructor(
        @InjectRepository(CustomChargeRepository)
        private readonly customChargeRepository: CustomChargeRepository,
    ) {
    }

    async getAll() {
        return await this.customChargeRepository.find({ relations: ['vehicleGroupEntity'] });
    }

    async getByVehicleGroupName(arr_names?: any) {
        return await this.customChargeRepository.createQueryBuilder('customCharge')
            .leftJoinAndSelect('customCharge.vehicleGroupEntity', 'vehicleGroupEntity')
            .where('vehicleGroupEntity.name IN (:arr_names)', { arr_names })
            .getMany();
    }

    async getOneByVehicleGroupName(name: string) {
        return await this.customChargeRepository.createQueryBuilder('customCharge')
            .leftJoinAndSelect('customCharge.vehicleGroupEntity', 'vehicleGroupEntity')
            .where('vehicleGroupEntity.name = (:name)', { name })
            .getOne();
    }

    async getOneByVehicleGroup(vehicle_group_id: number) {
        return await this.customChargeRepository.createQueryBuilder('customCharge')
            .leftJoinAndSelect('customCharge.vehicleGroupEntity', 'vehicleGroupEntity')
            .where('customCharge.vehicle_group_id = :vehicle_group_id', { vehicle_group_id })
            .getOne();
    }

    async update(id: number, body: UpdateCustomChargeDto) {
        const rs = await this.customChargeRepository.findOneOrFail(id);
        const dataUpdate = { ...rs, ...body };
        return await this.customChargeRepository.save(dataUpdate);
    }

}
