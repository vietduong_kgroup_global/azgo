import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class UpdateCustomChargeDto {

    @IsNotEmpty()
    @ApiModelProperty({ example: 1 , required: true  })
    vehicle_group_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 5 , required: true  })
    rate_of_charge: number;
}
