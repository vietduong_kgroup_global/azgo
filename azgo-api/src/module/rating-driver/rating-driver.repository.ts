import {EntityRepository, Repository} from 'typeorm';
import {RatingDriverEntity} from './rating-driver.entity';
@EntityRepository(RatingDriverEntity)
export class RatingDriverRepository extends Repository<RatingDriverEntity>{

}
