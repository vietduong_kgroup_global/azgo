import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt, IsNotEmpty, Max, Min, Validate} from 'class-validator';
import {DriverProfileEntityExists} from '../../validator-module/entity-driver-profile-exists.constraint';
export class CreateRatingDriverDto {

    @Validate(DriverProfileEntityExists, {message: 'Driver profile doesn`t exists'})
    @IsNotEmpty()
    @ApiModelProperty({example: 74, required: true})
    @IsInt()
    driver_id: number;

    @ApiModelProperty({example: 'Xe chay rat em'})
    content: string;

    @IsNotEmpty()
    @ApiModelProperty({example: 1, required: true})
    @Max(5)
    @Min(0)
    point: number;

    @ApiModelProperty({example: 1, required: false})
    az_bike_car_order_id: number;

    @ApiModelProperty({example: 1, required: false})
    az_van_bagac_order_id: number;
}
