import {Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {IsInt, IsNotEmpty, IsString} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';
import {DriverProfileEntity} from '../driver-profile/driver-profile.entity';
import {CustomerProfileEntity} from '../customer-profile/customer-profile.entity';
import {BikeCarOrderEntity} from '../bike-car-order/bikeCarOrder.entity';
import {VanBagacOrdersEntity} from '../van-bagac-orders/van-bagac-orders.entity';

@Entity('az_rating_drivers')
export class RatingDriverEntity {
    @Exclude()
    @PrimaryGeneratedColumn({type: 'bigint', unsigned: true})
    id: number;

    @Column('int')
    @IsInt({message: 'Driver profile must be a integer'})
    @ApiModelProperty({example: 1, required: true})
    driver_id: number;

    @Column('int')
    @IsInt()
    @ApiModelProperty({example: 1, required: true})
    customer_id: number;

    @Column('int')
    @IsInt()
    @ApiModelProperty({example: 1, required: false})
    az_bike_car_order_id: number;

    @Column('int')
    @IsInt()
    @ApiModelProperty({example: 1, required: false})
    az_van_bagac_order_id: number;

    @Column('text')
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty({example: 'Xe ngồi thoải mái, êm...', required: false})
    content: string;

    @Column('decimal')
    @IsNotEmpty()
    @ApiModelProperty({example: 4.5, required: true})
    point: number;

    @ManyToOne(type => DriverProfileEntity, driver => driver.user_id)
    @JoinColumn({name: 'driver_id', referencedColumnName: 'user_id'})
    driverProfile: DriverProfileEntity;

    @ManyToOne(type => CustomerProfileEntity, customer => customer.user_id)
    @JoinColumn({name: 'customer_id', referencedColumnName: 'user_id'})
    customerProfile: CustomerProfileEntity;

    @OneToOne(type => BikeCarOrderEntity, order_bike_car => order_bike_car.id)
    @JoinColumn({name: 'az_bike_car_order_id', referencedColumnName: 'id'})
    bikeCarOrder: BikeCarOrderEntity;

    @OneToOne(type => VanBagacOrdersEntity, order_bagac => order_bagac.id)
    @JoinColumn({name: 'az_van_bagac_order_id', referencedColumnName: 'id'})
    vanBagacOrder: VanBagacOrdersEntity;
}
