import {Body,
    Controller,
    Get,
    HttpStatus,
    Param,
    Post,
    Req,
    Res,
    UseGuards,
} from '@nestjs/common';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { ApiBearerAuth, ApiImplicitParam, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RatingDriverService } from './rating-driver.service';
import { CreateRatingDriverDto } from './dto/create-rating-driver.dto';
import { Response } from 'express';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import * as jwt from 'jsonwebtoken';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@ApiUseTags('Rating driver')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('rating-driver')
export class RatingDriverController {
    constructor(
        public readonly service: RatingDriverService,
    ) {

    }

    @Post('customer-rating-driver')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @ApiOperation({ title: 'Customer rating driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async ratingDriver(@Req() request, @Body() body: CreateRatingDriverDto, @Res() res: Response){
        try {
            const token = request.headers.authorization.split(' ')[1];
            const customerInfo: any =  jwt.decode(token);
            const data = {...body, ...{customer_id: customerInfo.id}};
            const result = await this.service.createNew(data);
            if (result){
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found order', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('get-rating-by-driver/:driver_id')
    @Roles({function_key: FunctionKey.All, action_key: ActionKey.All})
    @ApiOperation({ title: 'Avg rating by driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'driver_id', required: true, type: 'number'})
    async getRatingDriverByDriver(@Param('driver_id') driver_id: number, @Res() res: Response){
        try {
            const result = await this.service.getRatingByDriver(driver_id);
            if (result){
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

}
