import {forwardRef, Module} from '@nestjs/common';
import { RatingDriverController } from './rating-driver.controller';
import { RatingDriverService } from './rating-driver.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {UsersModule} from '../users/users.module';
import {CustomerProfileModule} from '../customer-profile/customer-profile.module';
import {AdminProfileModule} from '../admin-profile/admin-profile.module';
import {DriverProfileModule} from '../driver-profile/driver-profile.module';
import {BikeCarOrderRepository} from '../bike-car-order/bikeCarOrder.repository';
import {RatingDriverRepository} from './rating-driver.repository';
import {VanBagacOrdersRepository} from '../van-bagac-orders/van-bagac-orders.repository';

@Module({
  imports: [
      TypeOrmModule.forFeature([RatingDriverRepository, BikeCarOrderRepository, VanBagacOrdersRepository]),
      forwardRef(() => UsersModule),
      CustomerProfileModule,
      AdminProfileModule,
      DriverProfileModule,
  ],
  exports: [RatingDriverService],
  controllers: [RatingDriverController],
  providers: [RatingDriverService],
})
export class RatingDriverModule {}
