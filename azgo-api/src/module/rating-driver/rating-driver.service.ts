import { Injectable} from '@nestjs/common';
import { InjectRepository} from '@nestjs/typeorm';
import { getRepository} from 'typeorm';
import { RatingDriverEntity} from './rating-driver.entity';
import { RatingDriverRepository} from './rating-driver.repository';
import { BikeCarOrderRepository} from '../bike-car-order/bikeCarOrder.repository';
import {VanBagacOrdersRepository} from '../van-bagac-orders/van-bagac-orders.repository';

@Injectable()
export class RatingDriverService {
    constructor(
        @InjectRepository(RatingDriverRepository)
        private readonly ratingRepository: RatingDriverRepository,
        @InjectRepository(BikeCarOrderRepository)
        private readonly bikeCarOrderRepository: BikeCarOrderRepository,
        @InjectRepository(VanBagacOrdersRepository)
        private readonly vanBagacOrdersRepository: VanBagacOrdersRepository,
    )
    {
    }

    async getRatingByDriver(driver_id: number){
        const countRating = await this.ratingRepository.find({
            where: {driver_id},
        });

        const sumRatingDriver = await getRepository(RatingDriverEntity)
            .createQueryBuilder('az_rating_bike_cars')
            .select('az_rating_bike_cars.driver_id')
            .addSelect('SUM(az_rating_bike_cars.point)', 'sum_rating')
            .where('az_rating_bike_cars.driver_id = :driver_id', { driver_id })
            .getRawMany();

        let avgRatingDriver = 0;
        sumRatingDriver.map(item => {
            avgRatingDriver = item.sum_rating / countRating.length;
        });

        const data = {avg_rating_driver: avgRatingDriver.toFixed(1)};
        return data;
    }
    async createNew(data: any ) {
        if (data.az_bike_car_order_id) {
            const order = await this.bikeCarOrderRepository.findOneOrFail({customer_id: data.customer_id, id: data.az_bike_car_order_id});
            if (!order) return false;
            order.flag_rating_driver = 2;
            await this.bikeCarOrderRepository.save(order);
            return await this.ratingRepository.save(data);
        }
        if (data.az_van_bagac_order_id) {
            const order = await this.vanBagacOrdersRepository.findOneOrFail({customer_id: data.customer_id, id: data.az_van_bagac_order_id});
            if (!order) return false;
            order.flag_rating_driver = 2;
            await this.vanBagacOrdersRepository.save(order);
            return await this.ratingRepository.save(data);
        }
        return false;
    }

    async avgRatingDriver(driver_id: number): Promise<number> {
        const sumRatingDriver = await this.ratingRepository
            .createQueryBuilder('ratingDriver')
            .select(['ratingDriver.driver_id as driver_id', 'AVG(ratingDriver.point) as avg_rating'])
            .where('ratingDriver.driver_id = :driver_id', { driver_id })
            .getRawOne();
        return sumRatingDriver.avg_rating;
    }
}
