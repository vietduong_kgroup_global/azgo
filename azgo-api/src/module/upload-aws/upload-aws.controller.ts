import {
    ApiBearerAuth,
    ApiUseTags,
    ApiImplicitFile,
    ApiImplicitParam
} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import { Controller, Post, Req, Res, UseGuards, Param, HttpStatus } from '@nestjs/common';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import {FilesInterceptor} from '@nestjs/platform-express';
import { UploadAWSService } from './upload-aws.service';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';

@ApiUseTags('Upload AWS')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('upload-aws')
export class UploadAWSController {
    constructor(private readonly uploadAWSService: UploadAWSService) {}
    @Post('images')
    @Roles({function_key: FunctionKey.All, action_key: ActionKey.All})
    @UseGuards(AuthGuard('jwt'))
    @ApiImplicitFile({name: 'images[]', description: 'Support files type jpg|jpeg|png|gif'})
    async create(@Req() request, @Res() response) {
        try {
            console.log(request)
            await this.uploadAWSService.fileupload(request, response);
        } catch (error) {
        return response
            .status(500)
            .json(`Failed to upload image file: ${error.message}`);
        }
    }
    @Post('deleteImg')
    @Roles({function_key: FunctionKey.All, action_key: ActionKey.All})
    @UseGuards(AuthGuard('jwt'))
    // @ApiImplicitFile({name: 'images[]', description: 'Support files type jpg|jpeg|png|gif'})
    async deleteImg(@Req() request, @Res() response) {
        try {
            let {file_name} = request.body; 
            await this.uploadAWSService.removeImg(file_name);
            return response.status(HttpStatus.OK).send(dataSuccess('OK', true));
            
        } catch (error) {
        return response
            .status(500)
            .json(`Failed to delete image file: ${error.message}`);
        }
    }
}