import { Module } from '@nestjs/common';
import { UploadAWSController } from './upload-aws.controller';
import { UploadAWSService } from './upload-aws.service';
@Module({
  controllers: [UploadAWSController],
  providers: [UploadAWSService],
  exports: [UploadAWSService],
})
export class UploadAWSModule {
    constructor() {
      console.log("UploadAWSModule");
    }

}