import { Req, Res, Injectable, UploadedFiles } from '@nestjs/common';
import * as multer from 'multer';
import * as AWS from 'aws-sdk';
import * as multerS3 from 'multer-s3';
import s3Storage = require("multer-sharp-s3");
// import * as sharp from 'sharp';
import config from '../../../config/config';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';

const s3 = new AWS.S3({
  region: config.awsSetting.regionKey, // Region
  accessKeyId: config.awsSetting.accessKey,
  secretAccessKey: config.awsSetting.secretKey,
});
// AWS.config.region = config.awsSetting.regionKey; // Region
// AWS.config.update({
//   region: config.awsSetting.regionKey, // Region
//   accessKeyId: config.awsSetting.accessKey,
//   secretAccessKey: config.awsSetting.secretKey,
// });
@Injectable()
export class UploadAWSService {
  constructor() { }

  async removeImg(key: any) {
    console.log('key ', key)
    try {
      s3.deleteObject({
        Bucket: config.awsSetting.s3Setting.public_bucket,
        Key: key,
      }, (err, data) => {
        if (err) {
          console.log('Failed to delete ', err);
        }
        else {
          console.log('Success delete ', data);
        }
      });
    } catch (error) {
      console.log(error);
      // return res.status(500).json(dataError(`Failed to upload image file: ${error}`, null));
    }
  }

  async fileupload(@Req() req, @Res() res) {
    const _this = this;
    console.log('****fileupload')
    try {
      this.upload(req, res, function (error) {
        if (error) {
          console.log(error);
          return res.status(404).json(`Failed to upload image file: ${error}`);
        }
        let data = req.files.map(file => {
          if(file.location){
            return {
              address: file.location,
              file_path: config.awsSetting.s3Setting.key,
              file_name: file.key.replace(config.awsSetting.s3Setting.key + '/', ''),
            };
          }
          return;
        });
        data = data.filter(img => !!img)
        return res.status(200).json(dataSuccess('Uploaded Success', data));
      });
      this.fileuploadImgThumbnail(req, res)
    } catch (error) {
      console.log(error);
      return res.status(500).json(dataError(`Failed to upload image file: ${error}`, null));
    }
  }

  async fileuploadImgThumbnail(@Req() req, @Res() res) {
    const _this = this;
    try {
      _this.uploadThumbnail(req, res, function(errorThumb){
        if (errorThumb) {
          console.log(errorThumb);
          return res.status(404).json(`Failed to upload image thumbnail: ${errorThumb}`);
        }
      })
    } catch (error) {
      console.log(error);
      return res.status(500).json(dataError(`Failed to upload image file: ${error}`, null));
    }

  }
  upload = multer({
    storage: multerS3({
      s3: s3,
      bucket: config.awsSetting.s3Setting.public_bucket,
      contentType: multerS3.AUTO_CONTENT_TYPE,
      acl: 'public-read',
      key: function (request, file, cb) {
        let folderUpload = request.query.type_image || 'assets';
        cb(null, `${folderUpload}/${Date.now().toString()}-${file.originalname}`);
      },
    }),
  }).array('images[]', 1);

  // Create thumbnail
  uploadThumbnail = multer({
    storage: s3Storage({
      s3: s3,
      Bucket: config.awsSetting.s3Setting.public_bucket,
      // ContentType: multerS3.AUTO_CONTENT_TYPE,
      ACL: 'public-read',
      Key: function(request, file, cb) {
        let folderUpload = request.query.type_image || 'assets';
        cb(null, `${folderUpload}/thumbnail/${Date.now().toString()}-${file.originalname}`);
      },
      resize: {
        width: config.uploadConfig.RESIZE_IMAGE_WIDTH,
      },
    }),
  }).array('images[]', 1);
}