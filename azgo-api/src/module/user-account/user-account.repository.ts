import { Brackets, EntityRepository, Repository } from 'typeorm';
import { UserAccountEntity } from './user-account.entity';

@EntityRepository(UserAccountEntity)
export class UserAccountRepository extends Repository<UserAccountEntity> {
    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     * @param options
     */ 
    /**
     * This is function get tour by id
     * @param id
     */
    async findById(id: number) {
        return await this.findOne({ where: { id, deleted_at: null } });
    }
    async findByUserId(user_id: number) {
        return await this.findOne({ where: { user_id } });
    }
     
    /**
     * This is function get tour by id
     * @param id
     */
    async findByUuid(uuid: string) {
        return await this.findOne({ where: {uuid: uuid} });
    }

}
