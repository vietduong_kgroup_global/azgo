import { Column, Entity, PrimaryGeneratedColumn, OneToOne, JoinColumn, Generated, BaseEntity } from 'typeorm';
import { Exclude } from 'class-transformer';
import { IsNotEmpty, MaxLength} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';
import { AccountsEntity } from '../accounts/accounts.entity';
import { UsersEntity } from '../users/users.entity';
// import { AdditionalFeeEntity } from '../additional-fee/additional-fee.entity';

@Entity('az_user_accounts')
export class UserAccountEntity  extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        nullable: false,
    }) 
    account_id: number;
    
    @Column({
        type: 'int',
        nullable: false,
    }) 
    user_id: number;

    @OneToOne(() => AccountsEntity, id => id.userAccount)
    @JoinColumn({
        name: 'account_id',
    })
    account: AccountsEntity;

    @OneToOne(() => UsersEntity, user => user.userAccount)
    @JoinColumn({
        name: 'user_id',
    })
    user: UsersEntity;
}
