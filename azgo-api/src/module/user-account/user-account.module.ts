import { Module } from '@nestjs/common';
import { UserAccountController } from './user-account.controller';
import { UserAccountService } from './user-account.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {UserAccountRepository} from './user-account.repository';  
// import {UserAccountService} from './user-account.service';  
 
@Module({
  imports: [
      TypeOrmModule.forFeature([UserAccountRepository]),
  ],
  controllers: [UserAccountController],
  providers: [UserAccountService],
  exports: [UserAccountService],
})
export class UserAccountModule {}
