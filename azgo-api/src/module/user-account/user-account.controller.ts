import {
    Body,
    Controller,
    Delete, Get,
    HttpStatus,
    Param,
    Post,
    Put,
    Req,
    Res,
    UseGuards,
} from '@nestjs/common'; 
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import {FunctionKey, ActionKey} from '@azgo-module/core/dist/common/guards/roles.guards';
import { Response } from 'express';
import {AuthGuard} from '@nestjs/passport';
import {Roles} from '@azgo-module/core/dist/common/decorators/roles.decorator';
@Controller('user-account')
export class UserAccountController {
    @Post('/')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update account' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' }) 
    async updateUserAccount(@Req() request, @Res() res: Response) { 
            
    }
}
