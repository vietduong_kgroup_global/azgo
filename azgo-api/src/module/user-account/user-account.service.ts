import { Injectable } from '@nestjs/common';
import { BaseService } from '../../base.service';
import {UserAccountEntity} from './user-account.entity';
import {UserAccountRepository} from './user-account.repository'
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserAccountService  
// extends BaseService<UserAccountEntity, UserAccountRepository>   
{
    constructor(
        @InjectRepository(UserAccountRepository)
        private readonly repository: UserAccountRepository,
    ) {
    }
    async findById(id: number) {
        return await this.repository.findOneOrFail(id);
    }

    /**
     * This function get area by tour uuis
     * @param area_uuid
     */
    async findByUuid(uuid: string){
        return await this.repository.findByUuid(uuid);
    }
    async findByUserId(user_id: number): Promise<UserAccountEntity> {
        
        return await this.repository.findByUserId(user_id);
    }
}
