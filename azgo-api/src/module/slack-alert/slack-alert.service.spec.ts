import { Test, TestingModule } from '@nestjs/testing';
import { SlackAlertService } from './slack-alert.service';

describe('SlackAlertService', () => {
  let service: SlackAlertService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SlackAlertService],
    }).compile();

    service = module.get<SlackAlertService>(SlackAlertService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
