import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import axios from 'axios';
import { TourOrderRepository } from '../tour-order/tour-order.repository';
import { TourOrderEntity } from '../tour-order/tour-order.entity';
import {
    STATUS_TOUR_ARR,
} from '../../constants/secrets.const';
@Injectable()
export class SlackAlertService {
    
    async alertSlackAboutTourOrder(creatorName: string,company:string,status:number,tourOrder:TourOrderEntity) {
        try{
            let channel=process.env.CHANNEL_ALERT_ORDER_CREATED;
            let from=(company!=null)?"("+company+")":"" ;
            let message=creatorName + from + " đã vừa tạo ra đơn hàng mã #" + tourOrder.order_code;
            if(status === STATUS_TOUR_ARR.VERIFYING){
                channel=process.env.CHANNEL_ALERT_ORDER_CREATED;
                message=creatorName + from + " đã vừa tạo ra đơn hàng mã #" + tourOrder.order_code;
            }else if(status === STATUS_TOUR_ARR.ACCEPTED_DRIVER){
                channel=process.env.CHANNEL_ALERT_DRIVER_ACCEPT;
                if(creatorName!=null){
                    message=creatorName + from + " đã gắn tài xế vào đơn hàng mã #" + tourOrder.order_code;
                }else{
                    message=creatorName + from + "Một tài xế vừa nhận đơn hàng mã #" + tourOrder.order_code;
                }
            }else if(status === STATUS_TOUR_ARR.ACCEPTED_CUSTOMER){
                channel=process.env.CHANNEL_ALERT_CUSTOMER_ACCEPT;
                if(creatorName!=null){
                    message=creatorName + from + " đã đổi trạng thái `Khách Hàng Chấp Nhận` vào đơn hàng mã #" + tourOrder.order_code;
                }else{
                    message="Khách hàng vừa chấp nhận đơn hàng mã #" + tourOrder.order_code;
                }
            }else if(status === STATUS_TOUR_ARR.CANCELED_DRIVER){
                channel=process.env.CHANNEL_ALERT_DRIVER_CANCEL;
                if(creatorName!=null){
                    message=creatorName + from + " đã đổi trạng thái `Tài Xế Huỷ` vào đơn hàng mã #" + tourOrder.order_code + " :exclamation::exclamation::exclamation:";
                }else{
                    message="Tài xế vừa huỷ đơn hàng mã #" + tourOrder.order_code + " :exclamation::exclamation::exclamation:";
                }
            }else if(status === STATUS_TOUR_ARR.CANCELED_CUSTOMER){
                channel=process.env.CHANNEL_ALERT_CUSTOMER_CANCEL;
                if(creatorName!=null){
                    message=creatorName + from + " đã đổi trạng thái `Khách Hàng Huỷ` vào đơn hàng mã #" + tourOrder.order_code + " :broken_heart::broken_heart::broken_heart:";
                }else{
                    message="Khách hàng vừa huỷ đơn hàng mã #" + tourOrder.order_code + " :broken_heart::broken_heart::broken_heart:";
                }
            }else if(status === STATUS_TOUR_ARR.DONE){
                channel=process.env.CHANNEL_ALERT_ORDER_SUCCESS;
                if(creatorName!=null){
                    message=":dart: :dart: " +creatorName + from + " đã đổi trạng thái `Hoàn Thành` vào đơn hàng mã #" + tourOrder.order_code + " :tada::tada::tada:";
                }else{
                    message=":dart: :dart: " + "Tài xế vừa hoàn thành đơn hàng mã #" + tourOrder.order_code + " :tada::tada::tada:";
                }
            }else if(status === STATUS_TOUR_ARR.ARRIVED_LOCATION_PICKED_UP_CUSTOMER){
                channel=process.env.CHANNEL_ALERT_ARRIVED_LOCATION_PICKED_UP_CUSTOMER;
                if(creatorName!=null){
                    message=":dart: :dart: " +creatorName + from + " đã đón khách của đơn hàng mã #" + tourOrder.order_code + " :tada::tada::tada:";
                }else{
                    message=":dart: :dart: " + "Tài xế đã đón khách của đơn hàng mã #" + tourOrder.order_code + " :tada::tada::tada:";
                }
            }
            console.log('Send notify via slack:' + process.env.WEB_HOOK_SLACK + ' and channel:' + channel);
            if(process.env.WEB_HOOK_SLACK){
                axios.post(process.env.WEB_HOOK_SLACK,{
                    'channel':channel,
                    'text': message
                  }).then(resp => {
                    console.log(resp);
                  })
            }else{
    
            }
        }catch(err){
            console.log(err);
        }
        


    }
    async alertSlack(channel: string, message: string) {
        if(process.env.WEB_HOOK_SLACK){
            console.log('process.env.WEB_HOOK_SLACK', process.env.WEB_HOOK_SLACK)
            console.log('channel', channel)
            console.log('message', message)
            if (process.env.WEB_HOOK_SLACK) {
                axios.post(process.env.WEB_HOOK_SLACK, {
                    'channel':  channel,
                    'text': message,
                  }).then(resp => {
                    console.log(`Push notification in ${channel} channel successful`);
                    // console.log(resp);
                  })
                  .catch((error) => {
                      console.log('error alert', error)
                  })
            }
           
        }
    }
}
