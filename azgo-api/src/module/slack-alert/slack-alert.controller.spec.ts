import { Test, TestingModule } from '@nestjs/testing';
import { SlackAlertController } from './slack-alert.controller';

describe('SlackAlert Controller', () => {
  let controller: SlackAlertController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SlackAlertController],
    }).compile();

    controller = module.get<SlackAlertController>(SlackAlertController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
