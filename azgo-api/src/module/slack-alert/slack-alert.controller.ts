import { Controller, Get, HttpStatus, Req, Res } from '@nestjs/common';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { dataSuccess } from '@azgo-module/core/dist/utils/jsonFormat';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { Response } from 'express';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiResponse,
} from '@nestjs/swagger';
import { SlackAlertService } from './slack-alert.service';

@Controller('slack-alert')
export class SlackAlertController {
    constructor(
        private readonly slackAlertService: SlackAlertService,
    ) {
    }
    @Get('test')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get list tour order with off set limit' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getListTour(@Req() request, @Res() res: Response) {
        this.slackAlertService.alertSlackAboutTourOrder(null,null,null,null);
        return res.status(HttpStatus.OK).send(dataSuccess('Test Alert', null));
    }
}
