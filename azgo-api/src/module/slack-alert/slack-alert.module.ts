import { Module } from '@nestjs/common';
import { SlackAlertController } from './slack-alert.controller';
import { SlackAlertService } from './slack-alert.service';

@Module({
  imports: [],
  controllers: [SlackAlertController],
  providers: [SlackAlertService],
  exports: [SlackAlertService],
})
export class SlackAlertModule {}