import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsInt, IsNumber, Max, Min} from 'class-validator';

export class CreateVehicleTypeForTheProviderDto {

    @IsNotEmpty({ message: 'Tên loại xe là bắt buộc' })
    @ApiModelProperty({ example: '16 chỗ', required: true  })
    name: string;

    @IsInt()
    @IsNotEmpty({ message: 'Nhà cung cấp là bắt buộc' })
    @ApiModelProperty({ example: 1})
    provider_id: number;

    @IsInt()
    @IsNotEmpty({ message: 'Trường total_seat là bắt buộc' })
    @Max(100, {message: 'Tổng số ghế không được lớn hơn 100'})
    @Min(1, {message: 'Tổng số ghế không được nhỏ hơn 1'})
    @ApiModelProperty({ example: 16, required: true})
    total_seat: number;

    @IsNotEmpty({ message: 'Giá mỗi km là bắt buộc' })
    @ApiModelProperty({ example: 8000, required: true })
    @Max(1000000000, {message: 'Tổng số ghế không được lớn hơn 1,000,000,000'})
    @Min(1, {message: 'Tổng số ghế không được nhỏ hơn 1000'})
    price_per_km: number;

    @IsNotEmpty({ message: 'Giá cơ bản cơ bản là bắt buộc' })
    @ApiModelProperty({ example: 10000, required: true })
    @Max(1000000000, {message: 'Tổng số ghế không được lớn hơn 1,000,000,000'})
    @Min(1, {message: 'Tổng số ghế không được nhỏ hơn 1000'})
    base_price: number;

}
