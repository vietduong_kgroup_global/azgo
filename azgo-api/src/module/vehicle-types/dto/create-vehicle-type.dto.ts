import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsNumber, Max, Min} from 'class-validator';

export class CreateVehicleTypeDto {

    @IsNotEmpty({ message: 'Vehicle Type name is require' })
    @ApiModelProperty({ example: '4 chỗ', required: true  })
    name: string;

    @IsNotEmpty({ message: 'Total seat new is require' })
    @ApiModelProperty({example: 4, required: true })
    @IsNumber()
    @Min(1)
    // @Max(7)
    total_seat: number;

    @IsNotEmpty()
    @IsNumber()
    @ApiModelProperty({ example: 2 , description: '1: bus, 2: Bike, 3: Car, 4: Van, 5: Bagac, 6: Tour', required: true})
    vehicle_group_id: number;

    @IsNotEmpty({ message: 'Price per km is require' })
    @ApiModelProperty({ example: 8000, required: true })
    @IsNumber()
    price_per_km: number;

    @IsNotEmpty({ message: 'Base price is require' })
    @ApiModelProperty({ example: 10000, required: true })
    @IsNumber()
    base_price: number;
}
