import {forwardRef, Module} from '@nestjs/common';
import { VehicleTypesController } from './vehicle-types.controller';
import { VehicleTypesService } from './vehicle-types.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {VehicleTypesRepository} from './vehicle-types.repository';
import { ProvidersInfoModule } from '../providers-info/providers-info.module';
import {VehicleGroupsModule} from '../vehicle-groups/vehicle-groups.module';
import {UsersModule} from '../users/users.module';

@Module({
  imports: [ TypeOrmModule.forFeature([VehicleTypesRepository]),
      ProvidersInfoModule,
      VehicleGroupsModule,
      forwardRef(() => UsersModule),
  ],
  exports: [
    VehicleTypesService,
  ],
  controllers: [VehicleTypesController],
  providers: [VehicleTypesService],
})
export class VehicleTypesModule {}
