import {
    Controller,
    Get,
    HttpStatus,
    Param,
    Post,
    UseGuards,
    Put,
    Req,
    Res,
    Body,
    Delete,
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { VehicleTypesService } from './vehicle-types.service';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { Response } from 'express';
import { CreateVehicleTypeForTheProviderDto } from './dto/createVehicleTypeForTheProvider.dto';
import { CreateVehicleTypeDto } from './dto/create-vehicle-type.dto';
import { VehicleGroup, UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import {ProvidersInfoService} from '../providers-info/providers-info.service';
import * as jwt from 'jsonwebtoken';

@ApiUseTags('Vehicle types')
@Controller('vehicle-types')
export class VehicleTypesController {
    constructor(
        private readonly service: VehicleTypesService,
        private readonly providersInfoService: ProvidersInfoService,
    ) { }

    /**
     * Get all vehicle type by vehicle group
     * Return number
     * @param request
     * @param res
     */
    @Get('/provider')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @ApiOperation({ title: 'Get all vehicle type by provider' })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'provider_id', description: 'Provider ID', required: false, type: 'number'})
    async getVehicleTypesForTheProvider(
        @Req() request,
        @Res() res: Response,
    ) {
        if (request.user.type === UserType.PROVIDER) {
            try {
                const provider = await this.providersInfoService.findByUserId(request.user.id);
                if (!provider) return res.status(HttpStatus.BAD_REQUEST).send(dataError('Provider not found', null));
                request.query.provider_id = provider.id;
            } catch (e) {
                return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Bad requests', null));
            }
        }

        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.service.getAllByVehicleGroup([VehicleGroup.BUS], per_page, offset, request.query);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    /**
     * Get all vehicle type by vehicle group
     * Return number
     * @param vehicle_group_id
     * @param res
     */
    @Get('/van-bagac')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.CUSTOMER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all vehicle type by VAN/BAGAC' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    async getVehicleTypeByVanBaGac(
        @Req() request,
        @Res() res: Response,
    ) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.service.getAllByVehicleGroup([VehicleGroup.VAN, VehicleGroup.BAGAC], per_page, offset, request.query);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    /**
     * Get all vehicle type by vehicle group
     * Return number
     * @param res
     */
    @Get('/vehicle')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.CUSTOMER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all vehicle type by Bike Car' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: String })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: Number})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: Number})
    @ApiImplicitQuery({name: 'vehicle_group', description: 'vehicle group name', required: true, type: String})
    async getVehicleType(
        @Req() request,
        @Res() res: Response,
    ) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        try {
            let vehicleGroup = Object.keys(VehicleGroup)
            results = await this.service.getAllByVehicleGroup([...vehicleGroup.map(item => item)], per_page, offset, request.query);

        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }
    /**
     * Get all vehicle type by vehicle group
     * Return number
     * @param res
     */
    @Get('/bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.CUSTOMER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all vehicle type by Bike Car' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: String })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: Number})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: Number})
    @ApiImplicitQuery({name: 'vehicle_group', description: 'vehicle group name', required: true, type: String})
    async getVehicleTypeByBikeCar(
        @Req() request,
        @Res() res: Response,
    ) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        try {
             results = await this.service.getAllByVehicleGroup([VehicleGroup.BIKE, VehicleGroup.CAR], per_page, offset, request.query);

        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }
    /**
     * Create vehicle type for the provider
     * Return number
     * @param request
     * @param body
     * @param res
     */
    @Post('provider')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create vehicle type of provider' })
    @ApiResponse({ status: 201, description: 'Create' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createVehicleTypeForTheProvider(
        @Req() request,
        @Body(CustomValidationPipe) body: CreateVehicleTypeForTheProviderDto,
        @Res() res: Response,
    ) {
        try {
            const vehicle_types_for_the_provider = await this.service.createForTheProvider(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', vehicle_types_for_the_provider));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
    /**
     * Create vehicle type
     * Return number
     * @param request
     * @param body
     * @param res
     */

    @Post('vehicle')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create vehicle type of bike car' })
    @ApiResponse({ status: 201, description: 'Create' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createVehicleType(
        @Req() request,
        @Body(CustomValidationPipe) body: CreateVehicleTypeDto,
        @Res() res: Response,
    ) {
        try {
            const vehicle_types_for_the_provider = await this.service.createVehicleType(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', vehicle_types_for_the_provider));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create vehicle type of bike car' })
    @ApiResponse({ status: 201, description: 'Create' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createVehicleTypeForBikeCar(
        @Req() request,
        @Body(CustomValidationPipe) body: CreateVehicleTypeDto,
        @Res() res: Response,
    ) {
        try {
            const vehicle_types_for_the_provider = await this.service.createVehicleType(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', vehicle_types_for_the_provider));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('van-bagac')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create vehicle type of van bagac' })
    @ApiResponse({ status: 201, description: 'Create' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async createVehicleTypeForVanBagac(
        @Req() request,
        @Body(CustomValidationPipe) body: CreateVehicleTypeDto,
        @Res() res: Response,
    ) {
        try {
            const vehicle_types_for_the_provider = await this.service.createVehicleType(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', vehicle_types_for_the_provider));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('provider/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update vehicle type of provider' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'string' })
    async updateVehicleTypeForTheProvider(
        @Param('id') id,
        @Req() request,
        @Body(CustomValidationPipe) body: CreateVehicleTypeForTheProviderDto,
        @Res() res: Response,
    ) {
        try {
            const vehicle_types_for_the_provider = await this.service.updateForTheProvider(id, body);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', vehicle_types_for_the_provider));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
    /**
     * Update vehicle type
     * Return number
     * @param id
     * @param request
     * @param body
     * @param res
     */

    @Put('vehicle/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update vehicle type of bike car' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'string' })
    async updateVehicleType(
        @Param('id') id,
        @Req() request,
        @Body(CustomValidationPipe) body: CreateVehicleTypeDto,
        @Res() res: Response,
    ) {
        try {
            const vehicle_types = await this.service.updateVehicleType(id, body);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', vehicle_types));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('bike-car/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update vehicle type of bike car' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'string' })
    async updateVehicleTypeForBikeCar(
        @Param('id') id,
        @Req() request,
        @Body(CustomValidationPipe) body: CreateVehicleTypeDto,
        @Res() res: Response,
    ) {
        try {
            const vehicle_types_for_the_provider = await this.service.updateVehicleType(id, body);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', vehicle_types_for_the_provider));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('van-bagac/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update vehicle type of van bagac' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'string' })
    async updateVehicleTypeForVanBagac(
        @Param('id') id,
        @Req() request,
        @Body(CustomValidationPipe) body: CreateVehicleTypeDto,
        @Res() res: Response,
    ) {
        try {
            const vehicle_types_for_the_provider = await this.service.updateVehicleType(id, body);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', vehicle_types_for_the_provider));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get one by id vehicle type' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getDetail(
        @Param('id') id: number,
        @Res() res: Response,
        @Req() req,
    ) {
        try {
            const vehicle_types = await this.service.findById(id, req.user);
            if (!vehicle_types) return res.status(HttpStatus.NOT_FOUND).send(dataSuccess('Vehicle type not found', null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', vehicle_types));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    /**
     * Get all vehicle type by provider
     * @param provider_id vehicle types
     * Return number
     * @param res
     */
    @Get('/find-by-provider/:provider_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all by provider' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getVehicleTypeByProvider(
        @Param('provider_id') provider_id: number,
        @Res() res: Response,
    ) {
        try {
            const vehicle_types = await this.service.findByProvider(provider_id);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', vehicle_types));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    /**
     * Get all vehicle type by vehicle group
     * Return number
     * @param vehicle_group_id
     * @param res
     */
    @Get('/find-by-vehicle-group/:vehicle_group_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all by vehicle group' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getVehicleTypeByVehicleGroup(
        @Param('vehicle_group_id') vehicle_group_id: number,
        @Res() res: Response,
    ) {
        try {
            const vehicle_types = await this.service.findByVehicleGroup(vehicle_group_id);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', vehicle_types));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete vehicle type' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'string' })
    async delete(@Param('id') id, @Req() request, @Res() res: Response) {
        try {
            const vehicle_type = await this.service.delete(id);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', vehicle_type));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
