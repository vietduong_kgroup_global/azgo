import { EntityRepository, Repository } from 'typeorm';
import { VehicleTypesEntity } from './vehicle-types.entity';

@EntityRepository(VehicleTypesEntity)
export class VehicleTypesRepository extends Repository<VehicleTypesEntity> {
    /**
     * This is function find by id
     * @param id
     */
    async findById(id: number): Promise<VehicleTypesEntity> {
        let query = await this.createQueryBuilder('az_vehicle_types');
        query.leftJoinAndSelect('az_vehicle_types.vehicleGroup', 'vehicleGroup');
        query.leftJoinAndSelect('az_vehicle_types.providersInfo', 'providersInfo');
        query.where('az_vehicle_types.id = :id', {id});
        query.andWhere('az_vehicle_types.deleted_at is null');
        return query.getOne();
    }
}
