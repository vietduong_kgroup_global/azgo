import {Column, Entity, OneToMany, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {VehicleEntity} from '../vehicle/vehicle.entity';
import {IsInt, IsNotEmpty, IsString} from 'class-validator';
import { VehicleGroupsEntity } from '../vehicle-groups/vehicle-groups.entity';
import {BikeCarOrderEntity} from '../bike-car-order/bikeCarOrder.entity';
import {ProvidersInfoEntity} from '../providers-info/providers-info.entity';
import {TourEntity} from '../tour/tour.entity';

@Entity('az_vehicle_types')
export class VehicleTypesEntity {
    @PrimaryGeneratedColumn({ type: 'bigint', unsigned: true })
    id: number;

    @Column({
        type: 'varchar',
        length: '255',
        nullable: false,
    })
    @IsNotEmpty()
    @IsString()
    name: string;

    @Column({
        type: 'int',
        nullable: false,
    })
    @IsNotEmpty({message: 'Seat map is required'})
    @IsInt({message: 'Seat map must be a integer'})
    total_seat: number;

    @Column({
        type: 'float',
    })
    @IsNotEmpty()
    price_per_km: number;

    @Column({
        type: 'float',
    })
    @IsNotEmpty()
    base_price: number;

    @Column({
        type: 'int',
        nullable: true,
    })
    @IsNotEmpty()
    provider_id: number;

    @Column({
        type: 'bigint',
    })
    @IsNotEmpty()
    vehicle_group_id: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @OneToMany(type => VehicleEntity, vehicleEntity => vehicleEntity.vehicleType)
    vehicles: VehicleEntity[];

    @ManyToOne(type => VehicleGroupsEntity, vehicle_group => vehicle_group.vehicleTypes)
    @JoinColumn({
        name: 'vehicle_group_id',
    })
    vehicleGroup: VehicleGroupsEntity;

    @OneToMany(type => BikeCarOrderEntity, bike_car_order => bike_car_order.vehicleType)
    bikeCarService: BikeCarOrderEntity[];

    @ManyToOne(type => ProvidersInfoEntity, provider => provider.vehicleTypes)
    @JoinColumn({
        name: 'provider_id',
    })
    providersInfo: VehicleGroupsEntity;

    @OneToMany(type => TourEntity, tour => tour.vehicleType)
    listTour: TourEntity[];
}
