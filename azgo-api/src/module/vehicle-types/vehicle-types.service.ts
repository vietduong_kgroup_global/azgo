import { Injectable } from '@nestjs/common';
import {VehicleTypesEntity} from './vehicle-types.entity';
import {InjectRepository} from '@nestjs/typeorm';
import {Brackets} from 'typeorm';
import {CreateVehicleTypeForTheProviderDto} from './dto/createVehicleTypeForTheProvider.dto';
import {CreateVehicleTypeDto} from './dto/create-vehicle-type.dto';
import {ProvidersInfoService} from '../providers-info/providers-info.service';
import { getManager } from 'typeorm';
import {VehicleGroup, UserType} from '@azgo-module/core/dist/utils/enum.ultis';
import {VehicleTypesRepository} from './vehicle-types.repository';
import {VehicleGroupsService} from '../vehicle-groups/vehicle-groups.service';
import {UsersEntity} from '../users/users.entity';

@Injectable()
export class VehicleTypesService {
    constructor(
        @InjectRepository(VehicleTypesRepository)
        private readonly vehicleTypeRepository: VehicleTypesRepository,
        private readonly providerInfoService: ProvidersInfoService,
        private readonly vehicleGroupsService: VehicleGroupsService,
    )
    { }

    /**
     * This is function find by id
     * @param id
     * @param user
     */
    async findById(id: number, user?: UsersEntity): Promise<VehicleTypesEntity> {
        // return await this.vehicleTypeRepository.findById(id);
        let query = this.vehicleTypeRepository.createQueryBuilder('az_vehicle_types')
            .where('az_vehicle_types.id = :type_id', {type_id: id});

        if (user && user.type === UserType.PROVIDER) {
            query.leftJoin('az_vehicle_types.providersInfo', 'providersInfo')
                .leftJoin('providersInfo.users', 'user')
                .andWhere('user.id = :user_id', {user_id: user.id});
        }

        return await query.getOne();
    }

    /**
     * This is function find by id and vehicle group id
     * @param id
     * @param vehicle_group_id
     */
    async findByIdAndVehicleGroup(id: number, vehicle_group_ids: any): Promise<any> {
        return await this.vehicleTypeRepository.createQueryBuilder('vehicleType')
            .where('vehicleType.id = :id', {id})
            .andWhere('vehicleType.vehicle_group_id In(:vehicle_group_ids)', {vehicle_group_ids})
            .getOne();
    }

    /**
     * This is function find by provider
     * @param provider_id
     */
    async findByProvider(provider_id: number) {
        return await this.vehicleTypeRepository.createQueryBuilder('az_vehicle_types')
            .leftJoin('az_vehicle_types.providersInfo', 'providersInfo')
            .where('providersInfo.id = :provider_id', { provider_id })
            .andWhere('az_vehicle_types.deleted_at is null')
            .getMany();
    }

    /**
     * This is function find by vehicle group
     * @param vehicle_group_id
     */
    async findByVehicleGroup(vehicle_group_id: number) {
        return await this.vehicleTypeRepository.createQueryBuilder('az_vehicle_types')
            .leftJoinAndSelect('az_vehicle_types.vehicleGroup', 'vehicleGroup')
            .where('az_vehicle_types.vehicle_group_id = :vehicle_group_id', {vehicle_group_id})
            .andWhere('az_vehicle_types.deleted_at is null')
            .getMany();
    }

    /**
     * This is function find by provider
     * @param vehicle_group_id
     * @param limit
     * @param offset
     * @param options
     */
    async getAllByVehicleGroup(vehicle_group_ids: any, limit: number, offset: number, options?: any) {
        const provider_id = !isNaN(Number(options.provider_id).valueOf()) ? Number(options.provider_id).valueOf() : null;
        const query = await this.vehicleTypeRepository.createQueryBuilder('az_vehicle_types')
            .leftJoinAndSelect('az_vehicle_types.vehicleGroup', 'vehicleGroup')
            .leftJoinAndSelect('az_vehicle_types.providersInfo', 'providersInfo')
            .where('az_vehicle_types.vehicle_group_id in(:vehicle_group_ids)', {vehicle_group_ids})
            .andWhere('az_vehicle_types.deleted_at is null');
        if (provider_id) {
            query.andWhere('az_vehicle_types.provider_id = :provider_id', {provider_id});
        }
        // search by keyword
        if (!options.keyword === false) {
            query.andWhere(new Brackets(qb => {
                qb.where('az_vehicle_types.name like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('vehicleGroup.name like :keyword', { keyword: '%' + options.keyword.trim() + '%' });
            }));
        }
        // limit + offset
        if (options.vehicle_group && options.vehicle_group === 'BIKE') {
            query.orderBy('az_vehicle_types.vehicle_group_id', 'ASC');
        } else {
            query.orderBy('az_vehicle_types.vehicle_group_id', 'DESC');
        }
        query.take(limit)
            .skip(offset);

        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }

    /**
     * This is function create vehicle type for the provider
     * @param body
     */
    async createForTheProvider(body: CreateVehicleTypeForTheProviderDto): Promise<VehicleTypesEntity> {
        let vehicle_type = new VehicleTypesEntity();
        let provider = await this.providerInfoService.findById(body.provider_id);
        if (!provider) throw { message: 'Lôi, không tìm thấy nhà cung cấp', status: 404 };

        await getManager().transaction(async transactionalEntityManager => {
            vehicle_type.total_seat = body.total_seat;
            vehicle_type.name = body.name;
            vehicle_type.price_per_km = body.price_per_km;
            vehicle_type.base_price = body.base_price;
            vehicle_type.vehicle_group_id = VehicleGroup.BUS;
            vehicle_type.provider_id = body.provider_id;
            await transactionalEntityManager.save(vehicle_type);
        });

        return await this.vehicleTypeRepository.createQueryBuilder('vehicleType')
            .leftJoinAndSelect('vehicleType.providersInfo', 'providersInfo')
            .where('vehicleType.id = :id', {id: vehicle_type.id})
            .getOne();
    }

    /**
     * This is function create vehicle type for bike car or van bagac
     * @param vehicle_group_id
     * @param body
     */
    async createVehicleType(body: CreateVehicleTypeDto) {
        try {
            return await this.vehicleTypeRepository.save(body);
        } catch (error) {
            console.log("vehicle-types.services.createVehicleType: ", error);
        }
    }

    /**
     * This is function update vehicle type for bike car or van bagac
     * @param id
     * @param body
     */
    async updateVehicleType(id: number, body: CreateVehicleTypeDto) {
        let vehicle_type = await this.findById(id);
        const vehicleGroup = await this.vehicleGroupsService.findOne(body.vehicle_group_id);
        if (!vehicle_type) throw { message: 'Vehicle type not found', status: 404 };
        if (!vehicleGroup) throw { message: 'Vehicle Group not found', status: 404 };
        vehicle_type.name = body.name;
        vehicle_type.base_price = body.base_price;
        vehicle_type.price_per_km = body.price_per_km;
        vehicle_type.total_seat = body.total_seat;
        vehicle_type.vehicleGroup = vehicleGroup;
        return await this.vehicleTypeRepository.save(vehicle_type);
    }

    /**
     * This is function update vehicle type for the provider
     * @param id
     * @param body
     */
    async updateForTheProvider(id: number, body): Promise<VehicleTypesEntity>  {
        let vehicle_type = await this.findById(id);
        if (!vehicle_type) throw { message: 'Vehicle type not found', status: 404 };
        let provider = await this.providerInfoService.findById(body.provider_id);
        if (!provider) throw { message: 'Provider not found', status: 404 };

        await getManager().transaction(async transactionalEntityManager => {
            vehicle_type.total_seat = body.total_seat;
            vehicle_type.name = body.name;
            vehicle_type.base_price = body.base_price;
            vehicle_type.price_per_km = body.price_per_km;
            vehicle_type.provider_id = body.provider_id;
            await transactionalEntityManager.save(vehicle_type);
        });

        return vehicle_type;
    }

    /**
     * This is function delete vehicle type
     * @param id
     */
    async delete(id: number) {
        let vehicle_type = await this.vehicleTypeRepository.findOne(id);
        if (!vehicle_type) throw { message: 'Vehicle type not found', status: 404 };
        vehicle_type.deleted_at = new Date();
        await this.vehicleTypeRepository.save(vehicle_type);
    }
}
