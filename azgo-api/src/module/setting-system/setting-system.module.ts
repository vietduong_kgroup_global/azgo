import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SettingSystemController } from './setting-system.controller';
import { SettingSystemService } from './setting-system.service';
import { SettingSystemRepository } from './setting-system.repository';

@Module({
  imports: [TypeOrmModule.forFeature([SettingSystemRepository])],
  controllers: [SettingSystemController],
  providers: [SettingSystemService],
  exports: [SettingSystemService],
})
export class SettingSystemModule { }