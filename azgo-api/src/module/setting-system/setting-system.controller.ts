import {Roles} from '@azgo-module/core/dist/common/decorators/roles.decorator';
import {
    Body,
    Controller,
    Delete, Get,
    HttpStatus,
    Param,
    Post,
    Put,
    Req,
    Res,
    UseGuards,
} from '@nestjs/common';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import {CustomValidationPipe} from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import {AuthGuard} from '@nestjs/passport';
import {FunctionKey, ActionKey} from '@azgo-module/core/dist/common/guards/roles.guards';
import { Response } from 'express';
import {SettingSystemService} from './setting-system.service';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags
} from '@nestjs/swagger';
import {CreatesettingSystemDto} from './dto/create-setting-system.dto'
@Controller('setting-system')
export class SettingSystemController {
    constructor(public service: SettingSystemService) {

    }
    @Get('/versionDriver')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get driver version for google and apple, force update values' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    // @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    // @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    // @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    // @ApiImplicitQuery({name: 'type', description: 'Type', required: false, type: 'number'})
    async versionDriver(@Req() request, @Res() res: Response) {
        try {
            const results = await this.service.getOne(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', {
                apple_driver_version:  results.apple_driver_version,
                apple_driver_force_update:  results.apple_driver_force_update,
                google_driver_version:  results.google_driver_version,
                google_driver_force_update:  results.google_driver_force_update,
            }));
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }
    }

    @Get('/versionCustomer')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get customer version for google and apple, force update values' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    // @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    // @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    // @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    // @ApiImplicitQuery({name: 'type', description: 'Type', required: false, type: 'number'})
    async versionCustomer(@Req() request, @Res() res: Response) {
        try {
            const results = await this.service.getOne(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', {
                apple_customer_version:  results.apple_customer_version,
                apple_customer_force_update:  results.apple_customer_force_update,
                google_customer_version:  results.google_customer_version,
                google_customer_force_update:  results.google_customer_force_update,
            }));
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }
    }
    @Get('/')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Retrieve many settingSystemEntity' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    // @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    // @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    // @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    // @ApiImplicitQuery({name: 'type', description: 'Type', required: false, type: 'number'})
    async getOne(@Req() request, @Res() res: Response) {
        try {
            const results = await this.service.getOne(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', results));
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }
    } 


    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create one settingSystemEntity' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async create(@Req() request, @Body(CustomValidationPipe) body: CreatesettingSystemDto, @Res() res: Response) {
        try {
            const result = await this.service.create(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update one settingSystemEntity' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'id', description: 'Area Id', required: true, type: 'number' })
    async update(@Param('id') id, @Body() body: CreatesettingSystemDto, @Res() res: Response) {
        try {
            const result = await this.service.update(id, body);
            if (result)
                return res.status(HttpStatus.OK).send(dataSuccess('Update success', result));
            return res.status(HttpStatus.OK).send(dataError('Update error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
