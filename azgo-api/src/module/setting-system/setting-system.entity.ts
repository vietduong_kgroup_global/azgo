
import { Entity, PrimaryGeneratedColumn, Generated, Column, BaseEntity, OneToMany } from 'typeorm';
import { Exclude } from 'class-transformer';
import { IsInt, IsNotEmpty } from 'class-validator';

@Entity('az_setting_system')
export class SettingSystemEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    @Generated('uuid')
    @IsNotEmpty()
    uuid: string;

    @Column({
        type: 'float',
    })
    vat: number;

    @Column({
        type: 'float',
    })
    tax: number;

    @Column({
        type: 'int',
        default: () => 12,
    })
    hours_allow_cancel: number;

    @Column({
        type: 'int',
        default: () => 0.3,
    })
    minimum_percent_order: number;

    @Column({
        type: 'int',
        default: () => 3,
    })
    remind_time: number;

    @Column({
        type: 'varchar', 
    })
    apple_driver_version: string;

    @Column({
        type: 'varchar', 
    })
    google_driver_version: string;

    @Column({
        type: 'varchar', 
    })
    apple_customer_version: string;

    @Column({
        type: 'varchar', 
    })
    google_customer_version: string;

    @Column({
        type: 'boolean',
        default: () => false,
    })
    apple_driver_force_update: boolean;

    @Column({
        type: 'boolean',
        default: () => false,
    })
    google_driver_force_update: boolean;

    @Column({
        type: 'boolean',
        default: () => false,
    })
    apple_customer_force_update: boolean;

    @Column({
        type: 'boolean',
        default: () => false,
    })
    google_customer_force_update: boolean;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;
}