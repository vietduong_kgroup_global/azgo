import { ApiModelProperty } from '@nestjs/swagger';
import {IsInt, IsNotEmpty, IsString, MaxLength} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';
import {AreaType} from '../../../enums/type.enum';
export class CreatesettingSystemDto {

    @IsNotEmpty()
    @ApiModelProperty({example: '0.1'})
    // @IsInt() 
    vat: number;

    @IsNotEmpty()
    @ApiModelProperty({example: '10000'})
    // @IsInt() 
    tax: number;

    @IsNotEmpty()
    @ApiModelProperty({example: '1'})
    @IsInt() 
    hours_allow_cancel: number;
 
}
