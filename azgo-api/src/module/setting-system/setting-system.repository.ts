import { Brackets, EntityRepository, Repository } from 'typeorm';
import { SettingSystemEntity } from './setting-system.entity';

@EntityRepository(SettingSystemEntity)
export class SettingSystemRepository extends Repository<SettingSystemEntity> {
}
