import { Test, TestingModule } from '@nestjs/testing';
import { SettingSystemController } from './setting-system.controller';

describe('SettingSystem Controller', () => {
  let controller: SettingSystemController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SettingSystemController],
    }).compile();

    controller = module.get<SettingSystemController>(SettingSystemController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
