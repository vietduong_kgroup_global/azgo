import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SettingSystemRepository } from './setting-system.repository';

@Injectable()
export class SettingSystemService {
    constructor(
        @InjectRepository(SettingSystemRepository) 
        private readonly settingSystemRepository: SettingSystemRepository,
    ) {
    }
    async getOne(arr_names?: any) {
        return await this.settingSystemRepository.createQueryBuilder('az_setting_system')
            .where('az_setting_system.id = 1')
            .getOne();
    }
    async create(body: any) {
        return await this.settingSystemRepository.save(body);
    }

    async update(area_id: number, body?: any) {
        const area = await this.settingSystemRepository.findOne({id: area_id});
        if (!area) throw { message: 'Tuyến không tìm thấy', status: 404 };
        const dataSave = {...area, ...body};
        return await this.settingSystemRepository.save(dataSave);
    }
    async findAll(options?: any){
        let query = await this.settingSystemRepository.createQueryBuilder('area');
        if (!options.keyword === false) {
            query.where('area.name like :keyword', {keyword: '%' + options.keyword + '%' });
        }
        if (!options.type === false) {
            query.andWhere('area.type = :type', {type: options.keyword});
        }
        query.andWhere('area.deleted_at is null');
        query.orderBy('area.id', 'DESC');
        return await query.getMany();
    }

    async findById(id: number) {
        return await this.settingSystemRepository.findOneOrFail(id);
    }
}
