import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';

export class TypeOrmCrudServiceExtend<T> extends TypeOrmCrudService<T> {
    constructor(protected repository: Repository<T>)  {
        super(repository);
    }

    private get prototype(): T {
        return this.repository.create();
    }

    getEntityFromDTO(dto: any): T {
        return Object.assign(this.prototype, dto);
    }

}
