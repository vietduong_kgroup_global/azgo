import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {FirebaseService} from '../firebase/firebase.service';
import {FirebaseRepository} from '../firebase/firebase.repository';
import {BikeCarDriverService} from './bikeCarDriver.service';
import {BikeCarDriverController} from './bikeCarDriver.controller';
import {BikeCarOrderRepository} from '../bike-car-order/bikeCarOrder.repository';
import {CommonService} from '../../helpers/common.service';
import {UsersModule} from '../users/users.module';
import {DriverProfileModule} from '../driver-profile/driver-profile.module';
import {VehicleDriverModule} from '../vehicle-driver/vehicle-driver.module';
import {VehicleModule} from '../vehicle/vehicle.module';
import {RatingDriverModule} from '../rating-driver/rating-driver.module';
import {DriverLogsRepository} from '../driver-logs/driver-logs.repository';
import {DriverLogsModule} from '../driver-logs/driver-logs.module';
import {GoogleMapService} from '../google-map/google-map.service';
import {CustomChargeModule} from '../custom-charge/custom-charge.module';
import {WalletModule} from '../wallet/wallet.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            FirebaseRepository,
            BikeCarOrderRepository,
            DriverLogsRepository,
        ]),
        UsersModule,
        DriverProfileModule,
        RatingDriverModule,
        VehicleDriverModule,
        VehicleModule,
        DriverLogsModule,
        CustomChargeModule,
        WalletModule,
    ],
    providers: [BikeCarDriverService, FirebaseService, CommonService, GoogleMapService ],
    controllers: [BikeCarDriverController],
})
export class BikeCarDriverModule {

}
