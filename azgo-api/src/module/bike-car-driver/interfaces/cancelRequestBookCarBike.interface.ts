export interface CancelRequestBookCarBikeInterface {
    customer_uuid: string;
    service_id: number;
    route_info: object;
    book_car_bike_reference: string;
    price?: number;
}
