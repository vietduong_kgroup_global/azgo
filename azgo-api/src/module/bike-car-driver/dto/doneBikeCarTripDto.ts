import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class DoneBikeCarTripDto {
    @IsNotEmpty()
    @ApiModelProperty({ example: 'Reference of notification book' , required: true  })
    book_car_bike_reference: string;
}
