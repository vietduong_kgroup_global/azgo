import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class CancelBookCarBikeDto {

    @IsNotEmpty({ message: 'Reference of notification book' })
    @ApiModelProperty({ example: 'Reference of notification book' , required: true  })
    book_car_bike_reference: string;

}
