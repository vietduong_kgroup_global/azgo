import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class StartBikeCarTripDto {
    @IsNotEmpty()
    @ApiModelProperty({ example: 'Reference of notification book' , required: true  })
    book_car_bike_reference: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: '2: Đã đến nơi, 3: Khách lên xe' , required: true  })
    status: number;
}
