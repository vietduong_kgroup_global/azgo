import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';

export class ApproveBookCarBikeDto {

    @IsNotEmpty()
    @ApiModelProperty({ example: 1 , required: true  })
    vehicle_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: '5fdc191c-cc39-477b-b813-1d454c08f04c' , required: true  })
    customer_uuid: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: '4' , required: true  })
    service_id: number;

    @IsNotEmpty({ message: 'Route info' })
    @ApiModelProperty({ example: {
        start_position: CoordinatesDto, end_position: CoordinatesDto}, required: true  })
    route_info: object;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'Reference of notification book' , required: true  })
    book_car_bike_reference: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: 15000 , required: true  })
    price: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'BIKE or CAR' , required: true  })
    type_group: string;

}
