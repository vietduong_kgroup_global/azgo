import {Injectable} from '@nestjs/common';
import {BikeCarOrderRepository} from '../bike-car-order/bikeCarOrder.repository';
import {BikeCarOrderEntity} from '../bike-car-order/bikeCarOrder.entity';
import {InjectRepository} from '@nestjs/typeorm';
import {FirebaseRepository} from '../firebase/firebase.repository';
import {FirebaseService} from '../firebase/firebase.service';
import {RedisService} from '@azgo-module/core/dist/background-utils/Redis/redis.service';
import config from '@azgo-module/core/dist/config';
import {RedisUtil} from '@azgo-module/core/dist/utils/redis.util';
import {CommonService} from '../../helpers/common.service';
import message from '../../../config/message';
import {VehicleDriverService} from '../vehicle-driver/vehicle-driver.service';
import {VehicleService} from '../vehicle/vehicle.service';
import {RatingDriverService} from '../rating-driver/rating-driver.service';
import {DriverProfileService} from '../driver-profile/driver-profile.service';
import {DriverLogsRepository} from '../driver-logs/driver-logs.repository';
import {UsersService} from '../users/users.service';
import {DriverLogsService} from '../driver-logs/driver-logs.service';
import * as _ from 'lodash';
import {GoogleMapService} from '../google-map/google-map.service';
import {
    KEY_DRIVER_UPDATE_STATUS_THE_DRIVER_HAS_ARRIVED,
    KEY_ALREADY_HAVE_A_DRIVER_ACCEPT,
    KEY_CLICK_ACTION,
    KEY_DRIVER_ACCEPT_REQUEST_BOOK_BIKE_CAR_FROM_CUSTOMER, KEY_DRIVER_UPDATE_STATUS_CUSTOMER_ON_BIKE
} from '../../constants/keys.const';
import {CustomChargeService} from '../custom-charge/custom-charge.service';
import {WalletService} from '../wallet/wallet.service';

@Injectable()
export class BikeCarDriverService {
    private redisService;
    private redis;
    private keyRedisCustomerRequestBookCarBike = 'key_customer_request_book_car_bike';
    private fieldRedisCustomerRequestBookCarBike = 'field_customer_request_book_car_bike';

    // use for vehicle bus
    private keyRedisVehiclePositionBus = 'vehicle_positions_bus';
    private fieldRedisVehiclePositionBus = 'field_vehicle_positions_bus';
    // use for vehicle bike
    private keyRedisVehiclePositionBike = 'vehicle_positions_bike';
    private fieldRedisVehiclePositionBike = 'field_vehicle_positions_bike';
    // use for vehicle car
    private keyRedisVehiclePositionCar = 'vehicle_positions_car';
    private fieldRedisVehiclePositionCar = 'field_vehicle_positions_car';
    // use for vehicle van
    private keyRedisVehiclePositionVan = 'vehicle_positions_van';
    private fieldRedisVehiclePositionVan = 'field_vehicle_positions_van';
    // use for vehicle bagac
    private keyRedisVehiclePositionBagac = 'vehicle_positions_bagac';
    private fieldRedisVehiclePositionBagac = 'field_vehicle_positions_bagac';
    private  connect:any;
    constructor(
        @InjectRepository(BikeCarOrderRepository)
        private bikeCarOrderRepository: BikeCarOrderRepository,
        @InjectRepository(FirebaseRepository)
        private firebaseRepository: FirebaseRepository,
        @InjectRepository(DriverLogsRepository)
        private driverLogsRepository: DriverLogsRepository,
        private firebaseService: FirebaseService,
        private commonService: CommonService,
        private vehicleDriverService: VehicleDriverService,
        private vehicleService: VehicleService,
        private ratingDriverService: RatingDriverService,
        private driverProfileService: DriverProfileService,
        private userService: UsersService,
        private driverLogService: DriverLogsService,
        private readonly googleMapService: GoogleMapService,
        private readonly customChargeService: CustomChargeService,
        private readonly walletService: WalletService,

    ) {
        this.connect= {
            port: process.env.ENV_REDIS_PORT || 6379,
            host: process.env.ENV_REDIS_HOST || '127.0.0.1', 
            password: process.env.ENV_REDIS_PASSWORD || null,
        };
        console.log(" Redis constructor:",this.connect);
        this.redisService = RedisService.getInstance(this.connect);
        this.redis = RedisUtil.getInstance();
    }
    async approveRequestBookCarOrBike(data){
        let getListOrder, findReference, findReferenceIndex, listVehicle, vehicleDriverApprove;
        const getCacheOrder = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike);
        // check exist reference
        if (getCacheOrder.isValidate) {
            getListOrder = JSON.parse(getCacheOrder.value);
            findReferenceIndex =  _.findIndex(getListOrder, {book_car_bike_reference: data.book_car_bike_reference});
            findReference =  _.find(getListOrder, {book_car_bike_reference: data.book_car_bike_reference});
            if (!findReference) {
                throw {message: 'Reference not found', status: 404};
            }

        }
        const checkExit = await this.bikeCarOrderRepository
            .findOne({ book_car_bike_reference: data.book_car_bike_reference});
        if (checkExit) {
            throw {message: 'Reference existed', status: 409};
        }

        // Set receive request for driver not approve
        const redisField = await this.fieldRedisVehiclePosition(findReference.type_group);
        const redisKey = await this.keyRedisVehiclePosition(findReference.type_group);
        const getCacheVehicle = await this.redis.getInfoRedis(redisKey, redisField);
        if (getCacheVehicle.isValidate) {
            listVehicle = JSON.parse(getCacheVehicle.value);
            vehicleDriverApprove = _.find(listVehicle, {user_uuid: data.driver_uuid});
            const arrDriver = findReference.list_driver;
            const findDriverNotAccept = _.filter(listVehicle, vehicle => {
                return vehicle.user_uuid !==  data.driver_uuid && arrDriver.includes(vehicle.user_uuid);
            });
            if (findDriverNotAccept) {
                findDriverNotAccept.map((v) => {
                    v.receive_request = false;
                });
                // Update receive_request on list vehicle position
                await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listVehicle));
            }
        }
        // Get distance, time gg map by mode (driving)
        const distances: any = await Promise.all([
            this.googleMapService.rawUrlGetDistance({
                origins: {lat: vehicleDriverApprove.lat, lng: vehicleDriverApprove.lng},
                destinations: {lat: findReference.route_info.start_position.lat, lng: findReference.route_info.start_position.lng},
            }),
            this.googleMapService.rawUrlGetDistance({
                origins: {lat: findReference.route_info.start_position.lat, lng: findReference.route_info.start_position.lng},
                destinations: {lat: findReference.route_info.end_position.lat, lng: findReference.route_info.end_position.lng},
            }),
        ]);

        const distanceFromDriverToCustomer = distances[0].data.rows[0].elements[0].distance.text;
        const distanceFromStartPointToDestinations  = distances[1].data.rows[0].elements[0].distance.text;
        const timeLongFromDriverToCustomer  = distances[1].data.rows[0].elements[0].duration.text;
        const timeLongFromStartPointToDestinations  = distances[1].data.rows[0].elements[0].duration.text;

        // save driver log
        await this.driverLogsRepository.save({driver_id: data.driver_id});

        // Get first rate of charge by  vehicle group
        const rateOfChageByVehicleGroup = await this.customChargeService.getOneByVehicleGroup(data.vehicle_group_id);
        if (!rateOfChageByVehicleGroup) throw {message: 'Not found custom charge', status: 404};
        const order = new BikeCarOrderEntity();
        data.route_info = {...findReference.route_info, ...{
                distance_from_driver_to_customer: distanceFromDriverToCustomer,
                time_long_from_driver_to_customer: timeLongFromDriverToCustomer,
                distance_from_start_point_to_destinations: distanceFromStartPointToDestinations,
                time_long_from_start_point_to_destinations: timeLongFromStartPointToDestinations,
            }};
        const newOrder = {...order, ...data, ...{
            rate_of_charge: rateOfChageByVehicleGroup.rate_of_charge,
            fee_tax: Math.ceil((data.price * rateOfChageByVehicleGroup.rate_of_charge) / 100) }};
        const saveData = await this.bikeCarOrderRepository.save(newOrder);
        if (saveData) {
            // Emit event approve for per driver don't accept
            const dataNotiDriverNotAccept = {
                key: KEY_ALREADY_HAVE_A_DRIVER_ACCEPT,
                customer_user_uuid: data.customer_uuid,
                route_info: data.route_info,
                book_car_bike_reference: data.book_car_bike_reference,
                click_action: KEY_CLICK_ACTION,
            };
            (findReference.list_driver).map((driver_uuid) => {
                if (driver_uuid !== data.driver_uuid) {
                    this.redisService.triggerEventBikeCar({room: 'user_' + driver_uuid, message: dataNotiDriverNotAccept});
                }
            });
            // Update order bike car on redis
            findReference.driver_accept = true;
            findReference.driver_uuid_accept = data.driver_uuid;
            getListOrder[findReferenceIndex] = findReference;
            await this.redis.setInfoRedis(
                this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike, JSON.stringify(getListOrder));
            // Save driver log
            await this.driverLogService.createDriverLogs({driver_id: data.driver_id, status: 1});
            // send notification to driver
            const notification = {
                title:  message.bikeCarDriverToCustomer.acceptRequestBikeCar.title,
                body: message.bikeCarDriverToCustomer.acceptRequestBikeCar.body,
            };
            const driver_info = await this.userService.getOneDriver(data.driver_id);
            const dataNotification = {
                key: KEY_DRIVER_ACCEPT_REQUEST_BOOK_BIKE_CAR_FROM_CUSTOMER,
                customer_user_uuid: data.customer_uuid,
                driver_user_uuid: data.driver_uuid,
                route_info: data.route_info,
                book_car_bike_reference: data.book_car_bike_reference,
                time_to_go: timeLongFromDriverToCustomer, // time_long
                info_driver: driver_info,
                click_action: KEY_CLICK_ACTION,
            };
            // Emit event
            this.redisService.triggerEventBikeCar({room: 'user_' + data.customer_uuid, message: dataNotification});
            const userFCM = await this.firebaseRepository.findOne({user_uuid: data.customer_uuid});
            if (userFCM){
                const token = userFCM.token ? userFCM.token : userFCM.apns_id;
                await this.firebaseService.pushNotificationWithTokens([token], notification,  dataNotification);
            }
            return dataNotification;

        }
        return false;
    }

    async startTripBookCarOrBike(data){
        const allowArrayStatus = [2, 3];
        if (!allowArrayStatus.includes(Number(data.status))) {
            throw {message: 'Status not support', status: 400};
        }

        const order =  await this.bikeCarOrderRepository.findOne({driver_id: data.driverid, book_car_bike_reference: data.book_car_bike_reference });
        if (!order)
            return false ;
        const newOrder = {...order, status: data.status}; // Đã đến nơi
        let title, body, key;
        // status === 3 là khách lên xe, bắn notify cho customer, ẩn nút hủy
        if (data.status === 2) {
            title = 'Tài xế đã đến nơi';
            body = 'Tài xế đã đến nơi';
            key = KEY_DRIVER_UPDATE_STATUS_THE_DRIVER_HAS_ARRIVED;
        } else {
            title = 'Chúc mừng bạn đã lên xe';
            body = 'Chúc mừng bạn đã lên xe';
            key = KEY_DRIVER_UPDATE_STATUS_CUSTOMER_ON_BIKE;
        }
        const dataNotification = {
            key, // driver_update_status_customer_on_bike
            route_info: data.route_info,
            book_car_bike_reference: data.book_car_bike_reference,
            click_action: KEY_CLICK_ACTION,
        };
        // update request
        const getCacheOrder = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike);
        // check exist reference and update request
        if (getCacheOrder.isValidate) {
            const getListOrder = JSON.parse(getCacheOrder.value);
            const findReference = _.find(getListOrder, {book_car_bike_reference: data.book_car_bike_reference});
            const findReferenceIndex =  _.findIndex(getListOrder, {book_car_bike_reference: data.book_car_bike_reference});
            if (findReference) {
                findReference.status = data.status;
                getListOrder[findReferenceIndex] = findReference;
                await this.redis.setInfoRedis(
                    this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike, JSON.stringify(getListOrder) );
            }
            // Emit event for customer
            this.redisService.triggerEventBikeCar({room: 'user_' + findReference.customer_uuid, message: dataNotification});
        }
        const userEntity = await this.userService.findById(order.customer_id);
        if (!userEntity) {
            throw {message: 'User not found', status: 400};
        }

        // Send notify via firebase
        const userFCM = await this.firebaseService.findOne(userEntity.user_id);
        if (userFCM) {
            const token = userFCM.token ? userFCM.token : userFCM.apns_id;
            // send notification to customer
            const notification = {
                title,
                body,
            };
            await this.firebaseService.pushNotificationWithTokens([token], notification, dataNotification);
        }

        return await this.bikeCarOrderRepository.save(newOrder);
    }

    async doneTripBookCarOrBike(data){
        // update request
        let findReference;
        const getCacheOrder = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike);
        // check exist reference and update request
        if (getCacheOrder.isValidate) {
            const getListOrder = JSON.parse(getCacheOrder.value);
            findReference = _.find(getListOrder, {book_car_bike_reference: data.book_car_bike_reference});
            if (!findReference) throw {message: 'Reference not found!', status: 404};
            _.remove(getListOrder, {book_car_bike_reference: data.book_car_bike_reference});
            await this.redis.setInfoRedis(
                this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike, JSON.stringify(getListOrder));

        }
        // Update status order bike car
        const order = await this.bikeCarOrderRepository.findOne({driver_id: data.driverid, book_car_bike_reference: data.book_car_bike_reference });
        const newOrder = {...order, status: 4}; // Hoàn thành
        const saveData = await this.bikeCarOrderRepository.save(newOrder);
        const findUser = await this.firebaseRepository.findOne({user_id: order.customer_id});

        // Minus fee wallet by driver submit done schedule
        await this.walletService
            .minusFeeWalletByDriverId(data.driverid, data.driverid, order.fee_tax, order.rate_of_charge);

        // emit event to customer
        const dataNotification = {...saveData, ...{key: 'driver_submit_done_strip_book_bike_car_from_customer', click_action: 'FLUTTER_NOTIFICATION_CLICK'}};
        this.redisService.triggerEventBikeCar({room: 'user_' + findReference.customer_uuid, message: dataNotification});

        // Set receive request for driver approve
        const redisField = await this.fieldRedisVehiclePosition(findReference.type_group);
        const redisKey = await this.keyRedisVehiclePosition(findReference.type_group);
        const getCacheVehicle = await this.redis.getInfoRedis(redisKey, redisField);
        if (getCacheVehicle.isValidate) {
            let listVehicle = JSON.parse(getCacheVehicle.value);
            let findDriverAccept = _.find(listVehicle, vehicle => {
                return vehicle.user_uuid ===  findReference.driver_uuid_accept;
            });
            if (findDriverAccept) {
                findDriverAccept.receive_request = false;
                // Update receive_request on list vehicle position
                await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listVehicle));
            }
        }
        if (findUser) {
            let listDriverDeviceToken = [];
            const token = findUser.token ? findUser.token : findUser.apns_id;
            listDriverDeviceToken.push(token);
            const notification = {
                title: 'Chuyến đi kết thúc!',
                body: 'Hãy đánh giá tài xế!',
            };
            // Emit event
            await this.firebaseService.pushNotificationWithTokens(listDriverDeviceToken, notification,  dataNotification);
            return saveData;
        }
        return saveData;
    }

    async getActivityHistory(driver_id: number){
        return this.bikeCarOrderRepository.createQueryBuilder('bikeCar')
            .leftJoinAndSelect('bikeCar.customerProfile', 'customerProfile')
            .leftJoinAndSelect('customerProfile.users', 'users_customer')
            .leftJoinAndSelect('bikeCar.driverProfile', 'driverProfile')
            .leftJoinAndSelect('driverProfile.users', 'users_driver')
            .leftJoinAndSelect('bikeCar.vehicleType', 'vehicleType')
            .where('bikeCar.driver_id = :driver_id', {driver_id})
            .andWhere('bikeCar.status = :status', {status: 4}) // 4: Hoàn thành
            .orderBy('bikeCar.created_at', 'DESC')
            .getMany();
    }

    async getDetailActivityHistory(user_id: number, id: number){
        return await this.bikeCarOrderRepository.find({
            where: {id, user_id},
            relations: ['customerProfile', 'driverProfile', 'vehicleType'],
        });
    }

    async cancelRequestBookCarOrBike(book_car_bike_reference: string, driverInfo: any){
        // update request
        let findReference;
        const getCacheOrder = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike);
        // check exist reference and update request
        if (getCacheOrder.isValidate) {
            const getListOrder = JSON.parse(getCacheOrder.value);
            findReference = _.find(getListOrder, order => {
                return order.book_car_bike_reference === book_car_bike_reference && order.list_driver.includes(driverInfo.user_id.toString()) ;
            });
            if (!findReference) throw {message: 'Reference not found!', status: 404};
            // Update order list after remove driver uuid receive request
            findReference.list_driver = findReference.list_driver.filter(v => v !== driverInfo.user_id.toString());
            await this.redis.setInfoRedis(
                this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike, JSON.stringify(getListOrder));

            // Set receive request for driver
            const redisField = await this.fieldRedisVehiclePosition(findReference.type_group);
            const redisKey = await this.keyRedisVehiclePosition(findReference.type_group);
            const getCacheVehicle = await this.redis.getInfoRedis(redisKey, redisField);
            if (getCacheVehicle.isValidate) {
                let listVehicle = JSON.parse(getCacheVehicle.value);
                let findDriverNotAccept = _.find(listVehicle, vehicle => {
                    return vehicle.user_uuid !== driverInfo.user_id.toString();
                });
                if (findDriverNotAccept) {
                    findDriverNotAccept.receive_request = false;
                    // Update receive_request on list vehicle position
                    await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listVehicle));
                }
                return true;
            }
        }

        await this.driverLogsRepository.save({driver_id: driverInfo.id, status: 2});
        // const userFCM = await this.firebaseService.findOne(driverInfo.user_id);
        // if (userFCM){
        //     let listDriverDeviceToken = [];
        //     const token = userFCM.token ? userFCM.token : userFCM.apns_id;
        //     listDriverDeviceToken.push(token);
        //     // send notification to customer
        //     const notification = {
        //         title: 'Từ chối yêu cầu thành công!',
        //         body: 'Bạn đã từ chối yêu cầu đặt xe thành công',
        //     };
        //     const dataNotification = {
        //         key: 'driver_cancle_request_book_bike_car_from_customer',
        //         customer_user_uuid: data.customer_uuid,
        //         driver_user_uuid: driverInfo.driver_uuid,
        //         route_info: data.route_info,
        //         book_car_bike_reference: data.book_car_bike_reference,
        //         click_action: 'FLUTTER_NOTIFICATION_CLICK',
        //     };
        //     await this.driverLogService.createDriverLogs({driver_id: userFCM.user_id, status: 2});
        //     await this.firebaseService.pushNotificationWithTokens(listDriverDeviceToken, notification,  dataNotification);
        //     return true;
        // }
        return false;
    }

    async getDetailVehicleOfDriver(id: number) {
        const vehicleDriver = await this.vehicleDriverService.findByDriver(id);
        if (vehicleDriver) {
            return  await this.vehicleService.findById(vehicleDriver.vehicle_id);
        }
        return false;
    }

    async getListReports(driver_id: number) {
        const report = await this.driverLogService.report(driver_id);
        const rating = await this.ratingDriverService.getRatingByDriver(driver_id);
        return {...report, ...rating};
    }

    async fieldRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.fieldRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.fieldRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.fieldRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.fieldRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.fieldRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    async keyRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.keyRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.keyRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.keyRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.keyRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.keyRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }
}
