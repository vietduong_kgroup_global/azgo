import { Body, Controller, Get, HttpStatus, Param, Post, Req, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { ApproveBookCarBikeDto } from './dto/approveBookCarBikeDto';
import { BikeCarDriverService } from './bikeCarDriver.service';
import * as jwt from 'jsonwebtoken';
import { StartBikeCarTripDto } from './dto/startBikeCarTripDto';
import { DoneBikeCarTripDto } from './dto/doneBikeCarTripDto';
import { UsersService } from '../users/users.service';
import { CancelBookCarBikeDto } from './dto/cancelBookCarBikeDto';
import { VehicleGroup } from '@azgo-module/core/dist/utils/enum.ultis';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@ApiUseTags('Bike Car Driver')
@Controller('bike-car-driver')
export class BikeCarDriverController {

    constructor(
        private readonly userService: UsersService,
        private readonly bikeCarDriverService: BikeCarDriverService,
    ) {
    }

    @Post('/approve-request-book-bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BIKE_CAR]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Approve Request Book To All Car Or Bike' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async approveRequestBookCarOrBike(@Req() request, @Body() body: ApproveBookCarBikeDto, @Res() res: Response) {
        try {
            const findcustomer = await this.userService.findOne(body.customer_uuid);
            // get infor driver
            const token = request.headers.authorization.split(' ')[1];
            const driverInfo =  jwt.decode(token);
            const findUserByUuid = await this.userService.findOneByUserInfo(driverInfo);
            const vehicle_group_id = body.type_group === 'BIKE' ? VehicleGroup.BIKE :  VehicleGroup.CAR;
            const addData = {customer_id: findcustomer.id,
                driver_id: findUserByUuid.id, vehicle_group_id, driver_uuid: findUserByUuid.user_id.toString()  };
            const data = { ...body, ...addData};
            const result = await this.bikeCarDriverService.approveRequestBookCarOrBike(data);
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Book car or bike exited! ', null));
            return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/start-strip-book-bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BIKE_CAR]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Driver send request start moving ' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async startTripBookCarOrBike(@Req() request, @Body() body: StartBikeCarTripDto , @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const driverInfo =  jwt.decode(token);
            const findUserByUuid = await this.userService.findOneByUserInfo(driverInfo);
            const addData = {driverid: findUserByUuid.id};
            const data = { ...body, ...addData};
            const result = await this.bikeCarDriverService.startTripBookCarOrBike(data);
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Errors! ', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/done-trip-book-bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BIKE_CAR]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Driver send request done trip' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async doneTripBookCarOrBike(@Req() request, @Body() body: DoneBikeCarTripDto , @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const driverInfo =  jwt.decode(token);
            const findUserByUuid = await this.userService.findOneByUserInfo(driverInfo);
            const addData = {driverid: findUserByUuid.id};
            const data = { ...body, ...addData};
            const result = await this.bikeCarDriverService.doneTripBookCarOrBike(data);
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Errors! ', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('get-activity-history')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BIKE_CAR]})
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get activity history' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getListActivityHistory(@Req() request,  @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        // decode token
        const driverInfo =  jwt.decode(token);
        const findUserByUuid = await this.userService.findOneByUserInfo(driverInfo);
        if (findUserByUuid) {
            try {
                const result = await this.bikeCarDriverService.getActivityHistory(findUserByUuid.id);
                if (result) {
                    return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
                }
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
            } catch (error) {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
            }
        }
        return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));

    }

    @Get('get-detail-activity-history/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BIKE_CAR]})
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get detail activity history' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'id', type: 'integer', description: 'Activity ID'})
    async getDetailActivityHistory(@Req() request,  @Res() res: Response, @Param('id') id) {
        const token = request.headers.authorization.split(' ')[1];
        // decode token
        const customerInfo =  jwt.decode(token);
        const findUserByUuid = await this.userService.findOneByUserInfo(customerInfo); // uuid
        if (findUserByUuid) {
            try {
                const result = await this.bikeCarDriverService.getDetailActivityHistory(findUserByUuid.id, id);
                if (result) {
                    return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
                }
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
            } catch (error) {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
            }
        }
        return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));
    }

    @Post('/cancel-request-book-bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BIKE_CAR]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Cancel Request Book To All Car Or Bike By Driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async cancelRequestBookCarOrBike(@Req() request, @Body() body: CancelBookCarBikeDto, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const driverInfo =  jwt.decode(token);
            const findUserByUuid = await this.userService.findOneByUserInfo(driverInfo); // uuid
            const result = await this.bikeCarDriverService.cancelRequestBookCarOrBike(body.book_car_bike_reference, findUserByUuid);
            if (result)
                return res.status(HttpStatus.OK).send(dataSuccess('OK', null));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('get-detail-vehicle-of-driver-bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BIKE_CAR]})
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get detail vehicle of driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getDetailVehicle(@Req() request,  @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        // decode token
        const driverInfo =  jwt.decode(token);
        const findUserByUuid = await this.userService.findOneByUserInfo(driverInfo); // uuid
        if (findUserByUuid) {
            try {
                const result = await this.bikeCarDriverService.getDetailVehicleOfDriver(findUserByUuid.id);
                if (result) {
                    return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
                }
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
            } catch (error) {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
            }
        }
        return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));
    }

    @Get('get-reports-of-driver')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BIKE_CAR]})
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get reports of driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getReports(@Req() request,  @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        // decode token
        const driverInfo =  jwt.decode(token);
        const findUserByUuid = await this.userService.findOneByUserInfo(driverInfo); // uuid
        if (findUserByUuid) {
            try {
                const result = await this.bikeCarDriverService.getListReports(findUserByUuid.id);
                if (result) {
                    return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
                }
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
            } catch (error) {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
            }
        }
        return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));
    }
}
