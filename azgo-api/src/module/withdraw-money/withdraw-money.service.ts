import {forwardRef, Inject, Injectable} from '@nestjs/common';
import {WithdrawMoneyRepository} from './withdraw-money.repository';
import {OptionsInterface} from './dto/options.interface';
import {getManager} from 'typeorm';
import {WithdrawMoneyEntity} from './withdraw-money.entity';
import {WalletEntity} from '../wallet/wallet.entity';
import {StatusRequestWithdraw} from '../../enums/status.enum';
import {WalletService} from '../wallet/wallet.service';

@Injectable()
export class WithdrawMoneyService {
    constructor(
        private readonly withdrawMoneyReponsitory: WithdrawMoneyRepository,
        @Inject(forwardRef(() => WalletService))
        private readonly walletService: WalletService,
    ) {
    }

    async getAll(options: OptionsInterface) {
        return await this.withdrawMoneyReponsitory.getAll(options);
    }

    async requestWithdrawBydriver(body: {driver_id: number, amount: number}, findWalletByDriver: WalletEntity) {
        await getManager().transaction(async transactionalEntityManager => {
            // create history wallet
            const dataHistory = new WithdrawMoneyEntity();
            dataHistory.amount = body.amount;
            dataHistory.driver_id = body.driver_id;
            dataHistory.content = 'Yêu cầu rút tiền mặt';
            await transactionalEntityManager.save(dataHistory);

            // update wallet
            findWalletByDriver.driver_id = body.driver_id;
            findWalletByDriver.cash_wallet -= body.amount;
            await transactionalEntityManager.save(findWalletByDriver);
        });
        return findWalletByDriver;
    }

    async update(id: number, status: number) {
        const findOne = await this.withdrawMoneyReponsitory.findOneOrFail(id);
        if (findOne.status >= status ||  status > 3) throw {message: 'Status value not accept'};
        let dataUpdate: any = {status};
        if (status === StatusRequestWithdraw.CANCEL) dataUpdate.deleted_at = new Date();
        await this.withdrawMoneyReponsitory.update(id, dataUpdate);
        await getManager().transaction(async transactionalEntityManager => {
            // update status withdraw money
            findOne.status = status;
            await transactionalEntityManager.save(findOne);

            // update wallet when admin cancel
            if (status === StatusRequestWithdraw.CANCEL) {
                const wallet = await this.walletService.getOneByDriverId(findOne.driver_id);
                wallet.cash_wallet += findOne.amount;
                await transactionalEntityManager.save(wallet);
            }
        });
        return findOne;
    }
}
