import {ApiModelProperty} from '@nestjs/swagger';
import {IsNumber} from 'class-validator';

export class UpdateDto {
    @ApiModelProperty({
        example: 1,
        description: '2: Approved, 3: Cancel',
        enum: [2, 3],
        type: Number, required: true,
    })
    @IsNumber()
    status: number;
}
