import {Brackets, EntityRepository, Repository} from 'typeorm';
import {WithdrawMoneyEntity} from './withdraw-money.entity';
import {OptionsInterface} from './dto/options.interface';

@EntityRepository(WithdrawMoneyEntity)
export class WithdrawMoneyRepository extends Repository<WithdrawMoneyEntity> {
    async getAll(options: OptionsInterface){
        let setDate;
        if (options.year) setDate = options.year;
        if (options.month) setDate += '-' + options.month;

        // create new query
        let query = this.createQueryBuilder('withdraw')
            .leftJoinAndSelect('withdraw.usersEntity', 'usersEntity')
            .leftJoinAndSelect('usersEntity.driverProfile', 'driverProfile')
            .leftJoinAndSelect('usersEntity.vehicles', 'vehicles')
            .leftJoinAndSelect('vehicles.vehicleType', 'vehicleType');

        if (options.driver_id && setDate) {
            query.where('withdraw.created_at like :created_at', { created_at: '%' + setDate + '%' })
            .andWhere('withdraw.driver_id = :driver_id', { driver_id: options.driver_id });
        }
        if (options.keyword) {
            query.andWhere(new Brackets(qb => {
                qb.where('usersEntity.phone like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('usersEntity.email like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('driverProfile.full_name like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('vehicles.license_plates like :keyword', { keyword: '%' + options.keyword.trim() + '%' });
            }));
        }
        query.orderBy('withdraw.id', 'DESC')
            .take(Number(options.per_page))
            .skip(Number(options.offset));
        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }
}
