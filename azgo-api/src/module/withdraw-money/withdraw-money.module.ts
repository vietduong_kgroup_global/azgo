import {forwardRef, Module} from '@nestjs/common';
import { WithdrawMoneyController } from './withdraw-money.controller';
import { WithdrawMoneyService } from './withdraw-money.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {WithdrawMoneyRepository} from './withdraw-money.repository';
import {WalletModule} from '../wallet/wallet.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([WithdrawMoneyRepository]),
    forwardRef(() => WalletModule),
  ],
  controllers: [WithdrawMoneyController],
  providers: [WithdrawMoneyService],
  exports: [WithdrawMoneyService],
})
export class WithdrawMoneyModule {}
