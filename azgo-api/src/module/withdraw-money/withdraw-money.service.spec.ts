import { Test, TestingModule } from '@nestjs/testing';
import { WithdrawMoneyService } from './withdraw-money.service';

describe('WithdrawMoneyService', () => {
  let service: WithdrawMoneyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WithdrawMoneyService],
    }).compile();

    service = module.get<WithdrawMoneyService>(WithdrawMoneyService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
