import { Test, TestingModule } from '@nestjs/testing';
import { WithdrawMoneyController } from './withdraw-money.controller';

describe('WithdrawMoney Controller', () => {
  let controller: WithdrawMoneyController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WithdrawMoneyController],
    }).compile();

    controller = module.get<WithdrawMoneyController>(WithdrawMoneyController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
