import {Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {UsersEntity} from '../users/users.entity';

@Entity('az_withdraw_money')
export class WithdrawMoneyEntity {
    @PrimaryGeneratedColumn({ type: 'bigint', unsigned: true })
    id: number;

    @Column({
        type: 'int',
        nullable: false,
    })
    driver_id: number;

    @Column('double')
    amount: number;

    @Column({
        type: 'smallint',
        default: 1,
    })
    status: number;

    @Column({
        type: 'varchar',
    })
    content: string;

    @Column({
        type: 'smallint',
        default: 1,
    })
    type: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @ManyToOne(type => UsersEntity, driver => driver.withDrawMoney)
    @JoinColumn({
        name: 'driver_id',
    })
    usersEntity: UsersEntity;
}
