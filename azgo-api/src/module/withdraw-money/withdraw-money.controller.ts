import {Body, Controller, Get, HttpStatus, Param, Put, Req, Res, UseGuards} from '@nestjs/common';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { Response } from 'express';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import * as jwt from 'jsonwebtoken';
import {WithdrawMoneyService} from './withdraw-money.service';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import {UpdateDto} from './dto/update.dto';
import { UserUtil } from '@azgo-module/core/dist/utils/user.util';

@Controller('withdraw-money')
@ApiUseTags('Withdraw money')
export class WithdrawMoneyController {
    constructor(
        private readonly withdrawMoneyService: WithdrawMoneyService,
    ) {
    }

    @Get()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all withdraw money' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({
        name: 'month', description: 'Number month',
        required: false, type: String, enum: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
    })
    @ApiImplicitQuery({name: 'year', description: 'Number year', required: true, type: String})
    async getAll(@Req() request, @Res() res: Response) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            const token = request.headers.authorization.split(' ')[1];
            const driverInfo: any =  jwt.decode(token);
            let driver_id = null;
            if (driverInfo.type !== 1) {
                driver_id =  driverInfo.id;
            }
            results = await this.withdrawMoneyService.getAll({
                per_page,
                offset,
                keyword: request.query.keyword,
                month: request.query.month,
                year: request.query.year,
                driver_id,
            });
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }
        for (let i = 0; i < results.data.length ; i++) {
            results.data[i].usersEntity = await UserUtil.serialize(results.data[i].usersEntity);
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    @Put(':id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update status request withdraw money by admin' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    @ApiImplicitParam({ name: 'id', description: 'Withdraw id', required: true, type: Number })
    async update(@Body() body: UpdateDto, @Req() request, @Res() res: Response, @Param('id') id: number) {
        try {
            const result = await this.withdrawMoneyService.update(id, body.status);
            if (result) return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad request', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
