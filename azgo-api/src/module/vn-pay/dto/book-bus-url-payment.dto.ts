import {IsNotEmpty} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';

export class BookBusUrlPaymentDto {

    @IsNotEmpty()
    @ApiModelProperty({example: 'NCB', description: 'Mã ngân hàng', required: true })
    vnp_BankCode: string;

    @IsNotEmpty()
    @ApiModelProperty({example: '2c6259d3-fd0d-460a-abd7-22d921e7ade5_uv2pys62cm', description: 'Book bus reference', required: true })
    book_bus_reference: string;
}
