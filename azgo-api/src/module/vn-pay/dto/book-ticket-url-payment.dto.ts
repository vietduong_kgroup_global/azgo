import {IsNotEmpty, IsNumber, IsString} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';

export class BookTicketUrlPaymentDto {

    @IsNotEmpty()
    @ApiModelProperty({ example: '1188cbac-94f9-4c04-8d96-17f5bac71530', description: 'Customer uuid', type: String, required: true })
    customer_uuid: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'Mã chuyến', type: Number, required: true })
    schedule_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: ['01'], description: 'Array name', type: [String], required: true})
    seat_name: [string];

    @IsNotEmpty()
    @ApiModelProperty({ example: 1, type: Number, required: true, description: 'Tầng xe'})
    @IsNumber()
    floor: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 10000, description: 'Gia mỗi vé', type: Number, required: true})
    @IsNumber()
    price: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: '13.160.92.202', description: 'Địa chỉ IP của khách hàng thực hiện giao dịch', type: String, required: true})
    @IsNumber()
    vnp_IpAddr: string;

    @IsNotEmpty()
    @ApiModelProperty({example: 'NCB', description: 'Mã ngân hàng', required: true })
    vnp_BankCode: string;
}
