import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class CreatePaymentUrl {

    @IsNotEmpty()
    @ApiModelProperty({ example: 10000, required: true  })
    vnp_Amount: number;

    @IsNotEmpty()
    @ApiModelProperty({example: 'NCB', description: 'Mã ngân hàng', required: true })
    vnp_BankCode: string;

    @IsNotEmpty()
    @ApiModelProperty({example: 'Thanh toan cuoc xe (Go tieng viet khong dau)', required: true })
    vnp_OrderInfo: string;

    @IsNotEmpty()
    @ApiModelProperty({example: '240000: Xe cộ phương tiện, 250007: Vé máy bay, 240000', required: true })
    vnp_OrderType: string;

    @IsNotEmpty()
    @ApiModelProperty({example: 'Ticket_code or book reference', required: true })
    vnp_TxnRef: string;

}
