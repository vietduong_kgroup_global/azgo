import { Body, Controller, Get, HttpStatus, Post, Req, Res, UseGuards } from '@nestjs/common';
import { AuthGuard} from '@nestjs/passport';
import {ApiBearerAuth, ApiImplicitQuery, ApiOperation, ApiResponse} from '@nestjs/swagger';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import * as jwt from 'jsonwebtoken';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { CreatePaymentUrl} from './dto/create-payment-url.dto';
import * as  sha256 from 'sha256';
import * as querystring from 'qs';
import { VnPayService } from './vn-pay.service';
@Controller('vn-pay')
export class VnPayController {
    constructor(private readonly vnPayService: VnPayService) {
    }

    @Post('/create-payment-url')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create payment url' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async createPaymentUrl(@Req() request, @Res() res, @Body() body: CreatePaymentUrl) {
        /*
        * Get IP address
        * */
        const vnp_IpAddr = request.headers['x-forwarded-for'] ||
            request.connection.remoteAddress ||
            request.socket.remoteAddress ||
            request.connection.socket.remoteAddress;

        body.vnp_Amount = body.vnp_Amount * 100; // Khử số thập phân, vn pay bắt buộc nhân 100
        try {
            const vnpUrl = await this.vnPayService.createPaymentUrl(body, vnp_IpAddr);
            return res.status(HttpStatus.OK).send(dataSuccess('OK', vnpUrl));
        }catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(e.message, null));
        }
    }

    @Get('/call-back-payment')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @ApiBearerAuth()
    @ApiOperation({ title: 'Callback payment' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async callback(@Req() request, @Res() res) {
        try {
            const result = await this.vnPayService.callBackPayment(request.query);
            if (result.status)  return res.status(HttpStatus.OK).send(dataSuccess(result.message, null));
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message, null));
        }catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(e.message, null));
        }
    }

    @Get('/vnpay-ipn')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Vnpay ipn, Cung cấp URL này cho VN PAY, dùng để check trạng thái và update đơn hàng' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'vnp_Amount', description: 'vnp_Amount', required: true, type: Number})
    @ApiImplicitQuery({name: 'vnp_BankCode', description: 'vnp_BankCode', required: true, type: String})
    @ApiImplicitQuery({name: 'vnp_OrderInfo', description: 'vnp_OrderInfo (Tieng viet khong dau)', required: true, type: String})
    @ApiImplicitQuery({name: 'vnp_OrderType', description: 'vnp_OrderType', required: true, type: String})
    @ApiImplicitQuery({name: 'vnp_TxnRef', description: 'vnp_TxnRef', required: true, type: String})
    async vnpayIpn(@Req() request, @Res() res) {
        const vnp_Params = request.query;
        const secureHash = vnp_Params.vnp_SecureHash;
        delete vnp_Params.vnp_SecureHash;
        delete vnp_Params.vnp_SecureHashType;
        const vnp_ParamsSort =  await this.vnPayService.sortObject(vnp_Params);
        const secretKey = process.env.vnp_HashSecret;
        const signData = secretKey + querystring.stringify(vnp_ParamsSort, { encode: false });
        const checkSum = sha256(signData);

        if (secureHash === checkSum){
            console.log('VN PAY đã xác thực payment')
            const orderId = vnp_Params.vnp_TxnRef;
            const rspCode = vnp_Params.vnp_ResponseCode;
            // Kiem tra du lieu co hop le khong, cap nhat trang thai don hang va gui ket qua cho VNPAY theo dinh dang duoi
            res.status(200).json({RspCode: '00', Message: 'success'});
        }
        else {
            res.status(200).json({RspCode: '97', Message: 'Fail checksum'});
        }

    }
}
