import { Injectable } from '@nestjs/common';
import * as moment from 'moment';
import * as  sha256 from 'sha256';
import * as querystring from 'qs';
import {CreatePaymentUrlInterface} from './interfaces/create-payment-url.interface';
import {CreatePaymentUrl} from './dto/create-payment-url.dto';
import {InjectRepository} from '@nestjs/typeorm';
import {BikeCarOrderRepository} from '../bike-car-order/bikeCarOrder.repository';
import {TicketRepository} from '../ticket/ticket.repository';
import {VanBagacOrdersRepository} from '../van-bagac-orders/van-bagac-orders.repository';
import {BookShippingRepository} from '../book-shipping/book-shipping.repository';
import {SECRET_BOOK_BUS, SECRET_BOOK_SHIPPING, SECRET_BOOK_TICKET} from '../../constants/secrets.const';
import {StatusPayment, StatusPaymentMethod} from '../../enums/status.enum';
import {CommonService} from '../../helpers/common.service';

@Injectable()
export class VnPayService {
    constructor(
        @InjectRepository(BikeCarOrderRepository)
        private readonly bikeCarOrderRepository: BikeCarOrderRepository,
        @InjectRepository(TicketRepository)
        private readonly ticketRepository: TicketRepository,
        @InjectRepository(VanBagacOrdersRepository)
        private readonly vanBagacOrdersRepository: VanBagacOrdersRepository,
        @InjectRepository(BookShippingRepository)
        private readonly bookShippingRepository: BookShippingRepository,
        private readonly commonService: CommonService,
    ) {
    }
    async createPaymentUrl(body: CreatePaymentUrl, vnp_IpAddr: string) {
        /* Get config */
        const vnp_TmnCode = process.env.vnp_TmnCode;
        const secretKey = process.env.vnp_HashSecret;
        const vnp_ReturnUrl = process.env.vnp_ReturnUrl;
        let vnpUrl = process.env.vnp_Url;
        // Get date and format
        const date = new Date();
        const vnp_CreateDate = moment(date).format('YYYYMMDDHHmmss');
        // Sort object
        const dataStatic = {
            vnp_Version: '2',
            vnp_Command: 'pay',
            vnp_Locale: 'vn',
            vnp_CurrCode: 'VND',
        };
        const vnp_Params: CreatePaymentUrlInterface = {...body, ...dataStatic, ...{vnp_CreateDate, vnp_TmnCode, vnp_ReturnUrl, vnp_IpAddr}};
        const vnp_ParamsSort = await this.sortObject(vnp_Params);
        // Covert to query string
        const signData = secretKey + querystring.stringify(vnp_ParamsSort, { encode: false });
        // Create signature
        const vnp_SecureHashType =  'SHA256';
        const vnp_SecureHash = sha256(signData);
        // Concat object
        const vnp_Params_data = {...vnp_ParamsSort, ...{vnp_SecureHashType, vnp_SecureHash}};
        vnpUrl += '?' + querystring.stringify(vnp_Params_data,  { encode: true });
        return vnpUrl;
    }

    /*
    * Return result payment
    *
    * */
    async callBackPayment(query: any) {
        /* Get config */
        const secretKey = process.env.vnp_HashSecret;
        /* Get signature */
        const secureHash = query.vnp_SecureHash;
        delete query.vnp_SecureHash;
        delete query.vnp_SecureHashType;
        const vnp_ParamsSort: any = await this.sortObject(query);
        // Covert to query string
        const signData = secretKey + querystring.stringify(vnp_ParamsSort, { encode: false });
        // Create signature
        const checkSum = sha256(signData);
        if (secureHash === checkSum) {
            // Kiem tra xem du lieu trong db co hop le hay khong va thong bao ket qua
            // const reference = vnp_ParamsSort.vnp_TxnRef;
            // if (reference.indexOf(SECRET_BOOK_BUS) > -1 || reference.indexOf(SECRET_BOOK_TICKET) > -1) {
            //     await this.ticketRepository.createQueryBuilder('ticket')
            //         .where('book_bus_reference = :reference', {reference})
            //         .update()
            //         .set({payment_status: StatusPayment.PAID, payment_method: StatusPaymentMethod.BANK})
            //         .execute();
            // }
            // if (reference.indexOf(SECRET_BOOK_SHIPPING) > -1) {
            //     await this.bookShippingRepository.createQueryBuilder('ticket')
            //         .where('book_shipping_reference = :reference', {reference})
            //         .update()
            //         .set({payment_status: StatusPayment.PAID, payment_method: StatusPaymentMethod.BANK})
            //         .execute();
            // }
            return await this.commonService.vnp_ResponseCode(query.vnp_ResponseCode.toString());
        }
        return {status: false, message: 'Giao dịch không thành công'};
    }

    /*
    * Sort list params
    *
    * */
    sortObject(o) {
        let sorted = {},
            key, a = [];

        for (key in o) {
            if (o.hasOwnProperty(key)) {
                a.push(key);
            }
        }

        a.sort();
        for (key = 0; key < a.length; key++) {
            sorted[a[key]] = o[a[key]];
        }
        return sorted;
    }
}
