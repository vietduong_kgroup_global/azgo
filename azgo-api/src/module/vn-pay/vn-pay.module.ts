import { Module } from '@nestjs/common';
import { VnPayController } from './vn-pay.controller';
import { VnPayService } from './vn-pay.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {BikeCarOrderRepository} from '../bike-car-order/bikeCarOrder.repository';
import {TicketRepository} from '../ticket/ticket.repository';
import {BookShippingRepository} from '../book-shipping/book-shipping.repository';
import {VanBagacOrdersRepository} from '../van-bagac-orders/van-bagac-orders.repository';
import {CommonService} from '../../helpers/common.service';

@Module({
  imports: [
      TypeOrmModule.forFeature([BikeCarOrderRepository, TicketRepository, BookShippingRepository, VanBagacOrdersRepository]),
  ],
  controllers: [VnPayController],
  providers: [VnPayService, CommonService],
  exports: [VnPayService],
})
export class VnPayModule {}
