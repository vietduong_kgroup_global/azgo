import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsInt} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';

export class FindVanOrBagacNearCustomer {

    @IsNotEmpty()
    @ApiModelProperty({ type: CoordinatesDto , required: true  })
    start_point: CoordinatesDto;

    @IsNotEmpty()
    @ApiModelProperty({ type: CoordinatesDto, required: true  })
    end_point: CoordinatesDto;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'ngã 5 bình thái ...', required: true  })
    address_received_good: string;

    @IsNotEmpty({ message: 'Thông tin hàng hóa' })
    @ApiModelProperty({ example: '1 Balo lap top' , required: false  })
    good_info: string;

    @IsNotEmpty({ message: 'Khối lượng hàng hóa, đơn vị G' })
    @ApiModelProperty({ example: 1000 , required: false  })
    good_weight: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 10 , required: false  })
    good_length: number;
}
