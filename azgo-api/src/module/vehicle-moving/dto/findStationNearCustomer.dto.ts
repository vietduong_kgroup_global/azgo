import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';

export class FindStationNearCustomerDto {

    @IsNotEmpty({ message: 'Start station, 46 Bach Dang' })
    @ApiModelProperty({
        example: {
            lat: '10.814243',
            lng: '106.672924',
            address: '54 Bạch Đằng, phường 2, quận Tân Bình, TP. Hồ Chí Minh',
            short_address: '54 Bạch Đằng',
        },
        required: true,
        type: CoordinatesDto,
    })
    start_point: CoordinatesDto;

    @IsNotEmpty({ message: 'Area id' })
    @ApiModelProperty({ example: 1, required: true  })
    end_point: number;

    @IsNotEmpty({ message: 'Number seat' })
    @ApiModelProperty({ example: 1 , required: true, minimum: 1  })
    number_seat: number;

    @IsNotEmpty({ message: 'Thông tin hành lý' })
    @ApiModelProperty({ example: '1 Balo lap top' , required: false  })
    note: string;

    /* For filter*/
    @ApiModelProperty({ example: 1 , required: false  })
    provider_id: number;

    @ApiModelProperty({ example: 10000 , required: false  })
    price: number;
}
