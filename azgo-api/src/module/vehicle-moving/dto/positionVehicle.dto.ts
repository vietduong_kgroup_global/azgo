import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class PositionVehicleDto {

    @IsNotEmpty()
    @ApiModelProperty({ example: 1 , required: true  })
    vehicle_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: '10.814243' , required: true  })
    lat: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: '106.672924' , required: true  })
    lng: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'BUS, BIKE, CAR, VAN, BAGAC' , required: true  })
    type_group: string;
}
