import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';

export class TestDistanceDto {

    @IsNotEmpty()
    @ApiModelProperty({
        type: CoordinatesDto,
        required: true  })
    start_point: CoordinatesDto;

    @IsNotEmpty()
    @ApiModelProperty({
        type: CoordinatesDto,
        required: true  })
    end_point: CoordinatesDto;

}
