import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsNumber} from 'class-validator';

export class RemoveVehiclePositionDto {

    @IsNotEmpty({ message: 'Vehicle id' })
    @ApiModelProperty({ example: 1 , required: true  })
    @IsNumber({allowInfinity: false})
    vehicle_id: number;

    @IsNotEmpty({ message: 'Type : bus, bike_car, van_bagac' })
    @ApiModelProperty({ example: 'BUS, BIKE, CAR, VAN, BAGAC' , required: true  })
    type_group: string;
}
