import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsInt} from 'class-validator';

export class RemoveStationDto {

    @IsNotEmpty()
    @IsInt()
    @ApiModelProperty({ example: 1, required: true  })
    vehicle_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: '10.8148238' , required: true  })
    lat: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: '106.710817', required: true  })
    lng: string;
}
