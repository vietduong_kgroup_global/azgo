import {forwardRef, Module} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {VehicleMovingController} from './vehicleMoving.controller';
import {VehicleMovingService} from './vehicleMoving.service';
import {VehicleMovingRepository} from './vehicleMoving.repository';
import {RoutesRepository} from '../routes/routes.repository';
import {VehicleScheduleRepository} from '../vehicle-schedule/vehicleSchedule.repository';
import {VehicleRunningRepository} from '../vehicle-running/vehicleRunning.repository';
import {ProvidersInfoRepository} from '../providers-info/providers-info.repository';
import {VehicleRepository} from '../vehicle/vehicle.repository';
import {TicketRepository} from '../ticket/ticket.repository';
import {FirebaseRepository} from '../firebase/firebase.repository';
import {FirebaseService} from '../firebase/firebase.service';
import {CommonService} from '../../helpers/common.service';
import {VehicleScheduleModule} from '../vehicle-schedule/vehicleSchedule.module';
import {TicketModule} from '../ticket/ticket.module';
import {UsersModule} from '../users/users.module';
import {VehicleModule} from '../vehicle/vehicle.module';
import {AreaModule} from '../area/area.module';
import {RatingDriverModule} from '../rating-driver/rating-driver.module';
import {GoogleMapService} from '../google-map/google-map.service';
import {WalletModule} from '../wallet/wallet.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            VehicleMovingRepository,
            RoutesRepository,
            VehicleScheduleRepository,
            VehicleRunningRepository,
            ProvidersInfoRepository,
            VehicleRepository,
            TicketRepository,
            FirebaseRepository,
        ]),
        forwardRef(() => VehicleScheduleModule),
        forwardRef(() => TicketModule),
        UsersModule,
        VehicleModule,
        AreaModule,
        RatingDriverModule,
        WalletModule,
    ],
    providers: [VehicleMovingService, CommonService, FirebaseService, GoogleMapService],
    controllers: [VehicleMovingController],
    exports: [VehicleMovingService],
})
export class VehicleMovingModule {}
