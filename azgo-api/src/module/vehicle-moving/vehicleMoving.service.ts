import {InjectRepository} from '@nestjs/typeorm';
import {VehicleMovingRepository} from './vehicleMoving.repository';
import {Injectable} from '@nestjs/common';
import {RoutesRepository} from '../routes/routes.repository';
import {VehicleScheduleRepository} from '../vehicle-schedule/vehicleSchedule.repository';
import {VehicleRunningRepository} from '../vehicle-running/vehicleRunning.repository';
import {RedisService} from '@azgo-module/core/dist/background-utils/Redis/redis.service';
import config from '@azgo-module/core/dist/config';
import {RedisUtil} from '@azgo-module/core/dist/utils/redis.util';
import {ProvidersInfoRepository} from '../providers-info/providers-info.repository';
import {VehicleRepository} from '../vehicle/vehicle.repository';
import {TicketRepository} from '../ticket/ticket.repository';
import {FirebaseRepository} from '../firebase/firebase.repository';
import {CommonService} from '../../helpers/common.service';
import {VehicleRunningEntity} from '../vehicle-running/vehicleRunning.entity';
import {RemoveVehiclePositionDto} from './dto/removeVehiclePosition.dto';
import * as moment from 'moment';
import {FirebaseService} from '../firebase/firebase.service';
import {AreaService} from '../area/area.service';
import * as _ from 'lodash';
import {GoogleMapService} from '../google-map/google-map.service';
import {UsersService} from '../users/users.service';
import {StatusCode, StatusVehicleSchedule, StatusVehicleScheduleRunning} from '../../enums/status.enum';
import {VehicleGroup} from '../../enums/group.enum';
import {
    KEY_CLICK_ACTION,
    KEY_DRIVER_SUBMIT_DONE_SCHEDULE,
    KEY_DRIVER_UPDATE_POSITION,
} from '../../constants/keys.const';

@Injectable()
export class VehicleMovingService {
    private redisService: RedisService;
    private redis: RedisUtil;
    // Book shipping
    private keyRedisCustomerRequestBookShipping = 'key_customer_request_book_shipping';
    private fieldRedisCustomerRequestBookShipping = 'field_customer_request_book_shipping';
    // Book bus
    private keyRedisCustomerRequestBookBus = 'key_customer_request_book_bus';
    private fieldRedisCustomerRequestBookBus = 'field_customer_request_book_bus';
    // use for vehicle bus
    private keyRedisVehiclePositionBus = 'vehicle_positions_bus';
    private fieldRedisVehiclePositionBus = 'field_vehicle_positions_bus';
    // use for vehicle bike
    private keyRedisVehiclePositionBike = 'vehicle_positions_bike';
    private fieldRedisVehiclePositionBike = 'field_vehicle_positions_bike';
    // use for vehicle car
    private keyRedisVehiclePositionCar = 'vehicle_positions_car';
    private fieldRedisVehiclePositionCar = 'field_vehicle_positions_car';
    // use for vehicle van
    private keyRedisVehiclePositionVan = 'vehicle_positions_van';
    private fieldRedisVehiclePositionVan = 'field_vehicle_positions_van';
    // use for vehicle bagac
    private keyRedisVehiclePositionBagac = 'vehicle_positions_bagac';
    private fieldRedisVehiclePositionBagac = 'field_vehicle_positions_bagac';
    // user request bike car
    private keyRedisCustomerRequestBookCarBike = 'key_customer_request_book_car_bike';
    private fieldRedisCustomerRequestBookCarBike = 'field_customer_request_book_car_bike';

    private keyRedisVehicleRunning = 'vehicle_running';
    private fieldRedisVehicleRunning = 'field_vehicle';
    private  connect:any;
    constructor(
        @InjectRepository(VehicleMovingRepository)
        @InjectRepository(RoutesRepository)
        @InjectRepository(VehicleScheduleRepository)
        @InjectRepository(VehicleRunningRepository)
        @InjectRepository(ProvidersInfoRepository)
        @InjectRepository(VehicleRepository)
        @InjectRepository(TicketRepository)
        @InjectRepository(FirebaseRepository)
        private readonly vehicleMovingRepository: VehicleMovingRepository,
        private readonly vehicleScheduleRepository: VehicleScheduleRepository,
        private readonly vehicleRunningRepository: VehicleRunningRepository,
        private readonly routesRepository: RoutesRepository,
        private readonly providersInfoRepository: ProvidersInfoRepository,
        private readonly vehicleRepository: VehicleRepository,
        private readonly ticketRepository: TicketRepository,
        private readonly firebaseRepository: FirebaseRepository,
        private readonly firebaseService: FirebaseService,
        private commonService: CommonService,
        private readonly areaService: AreaService,
        private readonly googleMapService: GoogleMapService,
        private readonly userService: UsersService,

    ) {
        this.connect= {
            port: process.env.ENV_REDIS_PORT || 6379,
            host: process.env.ENV_REDIS_HOST || '127.0.0.1', 
            password: process.env.ENV_REDIS_PASSWORD || null,
        };
        console.log(" Redis constructor:",this.connect);
        this.redisService = RedisService.getInstance(this.connect);
        this.redis = RedisUtil.getInstance();
    }

    async saveVehicleStartMoving(schedule_id: number, driver_id: number) {
        const today = moment().format('YYYY-MM-DD HH:mm:ss');
        // 1:Chua khoi hanh, 2: Da khoi hanh, 3 Ket thuc
        const checkScheduleStarted = await this.vehicleScheduleRepository.createQueryBuilder('schedule')
            .where('schedule.user_id = :driver_id', {driver_id})
            .andWhere('schedule.status = :status', {status: StatusVehicleSchedule.STARTING})
            .getOne();
        if (checkScheduleStarted) {
            throw {
                message: `Bạn có chuyến ${checkScheduleStarted.id} đang chạy, vui lòng kết thúc chuyến đó trước`,
                status: 409 };
        }

        let schedule = await this.vehicleScheduleRepository.createQueryBuilder('schedule')
            .where('id = :schedule_id', {schedule_id} )
            .andWhere('user_id = :driver_id', {driver_id})
            .andWhere('start_date <= :today', {today})
            .andWhere('status = :status', {status: StatusVehicleSchedule.UNSTART})
            .getOne();
        if (schedule) {
            const route = await this.routesRepository.findOne(schedule.route_id);
            if (route) {
                const dataRunning = {
                    vehicle_id: schedule.vehicle_id,
                    schedule_id,
                    status: StatusVehicleScheduleRunning.STARTING,
                    router: route.router,
                    seat_map: schedule.seat_map,
                };
                const createDataRunningSuccess = await this.vehicleRunningRepository.createData(dataRunning);
                if (createDataRunningSuccess) {

                    schedule.status = StatusVehicleSchedule.STARTING;
                    await this.vehicleScheduleRepository.update(schedule.id, schedule);
                    return createDataRunningSuccess;
                }
            }
        }
        return false;
    }

    async removeStationOnRedis(data) {
        let getCache = await this.redis.getInfoRedis(this.keyRedisVehicleRunning, this.fieldRedisVehicleRunning);
        if (getCache.isValidate) {
            const listLatLngRedis = JSON.parse(getCache.value);
            _.remove(listLatLngRedis, (n: any) => {
                if (
                    n && parseInt(n.vehicle_id, 10) === parseInt(data.vehicle_id, 10)
                    && n.lat === data.lat
                    && n.lng === data.lng
                ) {
                    return true;
                }
            });
            await this.redis.setInfoRedis(this.keyRedisVehicleRunning, this.fieldRedisVehicleRunning, JSON.stringify(listLatLngRedis));
            return listLatLngRedis;
        }
        return false;
    }

    async removeVehiclePositionRedis(data: RemoveVehiclePositionDto, driver_uuid: string) {
        const redisField = await this.fieldRedisVehiclePosition(data.type_group);
        const redisKey = await this.keyRedisVehiclePosition(data.type_group);
        let getCache = await this.redis.getInfoRedis(redisKey, redisField);
        if (getCache.isValidate) {
            const listPositionRedis = JSON.parse(getCache.value);
            _.remove(listPositionRedis, (item: any) => {
                return Number(item.vehicle_id) === Number(data.vehicle_id) && item.user_uuid === driver_uuid;
            });
            await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listPositionRedis));
            return true;
        }
        return false;
    }
    async removeScheduleOnRedis(schedule_id: number) {
        const redisKey = await this.keyRedisVehiclePosition('BUS');
        const redisField = await this.fieldRedisVehiclePosition('BUS');
        let getCache = await this.redis.getInfoRedis(redisKey, redisField);
        if (getCache.isValidate) {
            const listLatLngRedis = JSON.parse(getCache.value);
            _.remove(listLatLngRedis, (item: any) => {
                return Number(item.schedule_id) === Number(schedule_id);
            });
            await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listLatLngRedis));
        }
        return true;
    }
    async findStationsNearCustomer(data) {
        const today = moment().format('x');

        // get cache vehicle position
        let listScheduleChoose = [];
        const unit = 'K';

        // Get vehicle position by driver
        const redisKey = await this.keyRedisVehiclePosition('BUS');
        const redisField = await this.fieldRedisVehiclePosition('BUS');
        let getCache = await this.redis.getInfoRedis(redisKey, redisField);

        if (getCache.isValidate) {
            const listVehicleBus = JSON.parse(getCache.value);
            /* Filter end point customer with end point schedule*/
            const findScheduleWithEndPoint = _.filter(listVehicleBus, (vehicle) => {
                const end_hour_schedule = moment(vehicle.end_hour, 'HH:mm DD-MM-YYYY').format('x');
                return Number(vehicle.end_point) === (data.end_point)
                    && Number(data.number_seat) <= Number(vehicle.available_seat)
                    && end_hour_schedule > today;
            });
            if (findScheduleWithEndPoint.length > 0) {
                // get list station approve about distance
                await Promise.all(findScheduleWithEndPoint.map(async (val) => {
                    if (val.router.additional_station.length > 0) {
                        // filter station with flag flase
                        const filterAdditionalStation = await _.filter( val.router.additional_station , {flag: false});
                        let distance;
                        let long_time = 0,
                          tempDistanceCustomerToStation = null,
                          distance_station_to_end_point = 0,
                          distance_customer_to_station = 0;
                        let station_address = null;
                        // Find station approve about distance
                        if (filterAdditionalStation.length > 0) {

                            /* Get coordinates of end point*/
                            const areaEntity = await this.areaService.findById(Number(val.end_point));
                            if (!areaEntity) throw {message: 'coordinates end point not found!', status: StatusCode.NOT_FOUND};
                            const coordinatesEndPoint: any = areaEntity.coordinates;
                            /* Loop stations and calc distance*/
                            await Promise.all(filterAdditionalStation.map(
                              async (station, index) => {
                                // Distance customer to station
                                const getDistances: any = await Promise.all([
                                    this.googleMapService.rawUrlGetDistance({
                                            origins: {lat: data.start_point.lat, lng: data.start_point.lng},
                                            destinations: {lat: station.lat, lng: station.lng},
                                    }),
                                    this.googleMapService.rawUrlGetDistance({
                                        origins: {lat: val.lat, lng: val.lng},
                                        destinations: {lat: station.lat, lng: station.lng},
                                    }),

                                    this.googleMapService.rawUrlGetDistance({
                                        origins: {lat: station.lat, lng: station.lng},
                                        destinations: {lat: coordinatesEndPoint.lat, lng: coordinatesEndPoint.lng},
                                    }),
                                    new Promise((resolved) => {
                                        resolved(station);
                                    }),
                                ]);

                                if (getDistances) {
                                    // Distance from customer to station, unit: meters
                                    const distance_pickup_point = getDistances[0].data.rows[0].elements[0].distance.value;

                                    // Distance from vehicle to station
                                    const distance_vehicle_to_station = getDistances[1].data.rows[0].elements[0].distance.text;
                                    const distance_vehicle_to_station_value = getDistances[1].data.rows[0].elements[0].distance.value;

                                    // Distance from station to end point
                                    const distance_station_end_point = getDistances[2].data.rows[0].elements[0].distance.text;

                                    // Compare distance approve
                                    const conditionFilterStation =
                                      Number(distance_pickup_point) <= (Number(process.env.BUS_DISTANCE_APPROVED) || 50000)
                                      && (Number(distance_vehicle_to_station_value) / 3) >= Number(distance_pickup_point);
                                    if (conditionFilterStation) {
                                        //  Get station nearest by customer
                                        const stationChoosed = tempDistanceCustomerToStation === null ||
                                          tempDistanceCustomerToStation >= distance_pickup_point;

                                        if (stationChoosed) {
                                            tempDistanceCustomerToStation = distance_pickup_point;
                                            distance = distance_vehicle_to_station;
                                            distance_station_to_end_point = distance_station_end_point;
                                            distance_customer_to_station =  getDistances[0].data.rows[0].elements[0].distance.text;
                                            station_address = getDistances[3];
                                            long_time = getDistances[1].data.rows[0].elements[0].duration.text;
                                        }
                                    }
                                }
                            }));
                            val.distance = distance;
                            val.distance_station_to_end_point = distance_station_to_end_point;
                            val.distance_customer_to_station = distance_customer_to_station;
                            val.unit = unit;
                            val.price = (Number(String(distance_station_to_end_point).match(/\d+/)[0]) * val.price_per_km).toFixed(1);
                            val.pickup_point = station_address;
                            val.long_time = long_time;
                        }
                        if (distance !== undefined) {
                            listScheduleChoose.push(val);
                        }

                    }

                }));

                if (data.provider_id !== undefined) {
                    listScheduleChoose = _.filter(listScheduleChoose, {provider_id: data.provider_id});

                }
                if (data.price !== undefined) {
                    listScheduleChoose = _.filter(listScheduleChoose, schedule => {
                        return Number(schedule.price) <= Number(data.price);
                    });
                }
                return listScheduleChoose;
            }
        }
        return null;
    }

    calculateDistance(lat1, lon1, lat2, lon2, unit) {
        return this.commonService.calculateDistance(lat1, lon1, lat2, lon2, unit);
    }

    async calculateLongTime(vehicleId, lat, lng) {
        return await this.commonService.calculateLongTime(vehicleId, lat, lng);
    }

    async updatePositionVehicleToRedis(data: any) {
        let findVehicleOnRedis;
        // @ts-ignore
        const userFCM = await this.firebaseRepository.findOne({user_uuid: data.user_uuid});
        if (userFCM) {
            data.device_token = userFCM.token ? userFCM.token : userFCM.apns_id;
            // Get vehicle position by driver
            const redisKey = await this.keyRedisVehiclePosition(data.type_group);
            const redisField = await this.fieldRedisVehiclePosition(data.type_group);
            let getCache = await this.redis.getInfoRedis(redisKey, redisField);
            if (getCache.isValidate) {
                let vehiclePositions = JSON.parse(getCache.value);
                const findIndexVehicle = _.findIndex(vehiclePositions, {vehicle_id: Number(data.vehicle_id)});
                findVehicleOnRedis = _.find(vehiclePositions, {vehicle_id: Number(data.vehicle_id)});
                if (!findVehicleOnRedis) {
                    vehiclePositions.push(data);
                } else {
                    if (data.type_group === VehicleGroup.BUS) {
                        // exit vehicle , replace data
                        // Tính khoản cách của trạm đoán phụ (flag = false) với vị trí của xe với bán kính <= 1km
                        /* sort list additional_station*/
                        let additionalStation = findVehicleOnRedis.router.additional_station.sort(function compare(a, b) {
                            return a.id - b.id;
                        });
                        // get first with flag flase
                        const findIndexAdditionalStation = _.findIndex(additionalStation, {flag: false});
                        if (findIndexAdditionalStation > -1) {
                            let distanceVehicleToStation = await this.calculateDistance(data.lat, data.lng,
                                additionalStation[findIndexAdditionalStation].lat, additionalStation[findIndexAdditionalStation].lng, 'K');
                            if (distanceVehicleToStation <= (Number(process.env.DISTANCE_VEHICLE_TO_STATION) || 0.5)) {
                                additionalStation[findIndexAdditionalStation].flag = true;
                            }
                        }
                        // Set additional_station
                        findVehicleOnRedis.router.additional_station = additionalStation;
                        data.router = findVehicleOnRedis.router;
                        data.seat_map = vehiclePositions[findIndexVehicle].seat_map;
                        data.available_seat = vehiclePositions[findIndexVehicle].available_seat;
                    }
                    vehiclePositions[findIndexVehicle] = data;

                }
                await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(vehiclePositions));
            } else {
                let vehiclePositions = [];
                vehiclePositions.push(data);
                await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(vehiclePositions));
            }
            // Push socket for customers receive approve from driver
            await this.pushSocketIOForCustomerByTypeGroup({driver_uuid: userFCM.user_uuid, type_group: data.type_group, findVehicleOnRedis });
            return true;
        }
        return false;
    }
    async pushSocketIOForCustomerByTypeGroup(data: {driver_uuid: string, type_group: string, findVehicleOnRedis: any}) {
        let getCacheOrder, getOrderApprove;
        const redisKey = await this.keyRedisRequestBooking(data.type_group);
        const redisField = await this.fieldRedisRequestBooking(data.type_group);
        getCacheOrder = await this.redis.getInfoRedis(redisKey, redisField);
        if (getCacheOrder.isValidate) {
            const cacheValue = JSON.parse(getCacheOrder.value);
            getOrderApprove = _.find(cacheValue, item => {
                return item.driver_accept === true && item.driver_uuid_accept === data.driver_uuid;
            });
        }
        if (getOrderApprove && data.findVehicleOnRedis) {
            const dataNotification = {
                key: KEY_DRIVER_UPDATE_POSITION,
                type_group: data.type_group,
                click_action: KEY_CLICK_ACTION,
                driver_position: {lat: data.findVehicleOnRedis.lat, lng: data.findVehicleOnRedis.lng},
            };
            switch (data.type_group) {
                case VehicleGroup.BUS:
                    this.redisService.triggerEventBookBus({room: 'user_' + getOrderApprove.customer_uuid, message: dataNotification});
                    break;
                case VehicleGroup.BIKE:
                case VehicleGroup.CAR:
                    this.redisService.triggerEventBikeCar({room: 'user_' + getOrderApprove.customer_uuid, message: dataNotification});
                    break;
                case VehicleGroup.VAN:
                case VehicleGroup.BAGAC:
                    this.redisService.triggerEventVanBagac({room: 'user_' + getOrderApprove.customer_uuid, message: dataNotification});
                    break;
            }
        }
        return true;
    }
    async findCarOrBikeNearCustomer(data: {
        customer_position: {lat: string, lng: string},
        type_group: string, service_id: number, convertPriceToPercentApproved: number,
    }): Promise<any> {

        const redisField = await this.fieldRedisVehiclePosition(data.type_group);
        const redisKey = await this.keyRedisVehiclePosition(data.type_group);
        let getCache = await this.redis.getInfoRedis(redisKey, redisField);
        if (getCache.isValidate) {
            const listLatLng = JSON.parse(getCache.value);
            // Filter vehicle by vehicle type & receive_request = false
            const findVehicleType = _.filter(listLatLng, item => {
                return item.vehicle_type_id === data.service_id
                    && item.receive_request === false
                    && item.fee_wallet >= data.convertPriceToPercentApproved;
            });
            if (findVehicleType.length <= 0) return false;
            const lat1 = parseFloat(data.customer_position.lat);
            const lon1 = parseFloat(data.customer_position.lng);
            const unit = 'K';
            let listLatLngChoose = [];
            if (findVehicleType) {
                // get list station approve about distance
                findVehicleType.forEach((val) => {
                    const lat2 = parseFloat(val.lat);
                    const lon2 = parseFloat(val.lng);
                    const distance = this.calculateDistance(lat1, lon1, lat2, lon2, unit);
                    // if (distance <= distanceApprove && val.receive_request === false) {
                    if (Number(distance) <= Number(process.env.BIKE_CAR_DISTANCE_APPROVED) || 2 ) {
                        if (listLatLngChoose.length <= 2) {
                            val.distance = distance;
                            val.unit = unit;
                            listLatLngChoose.push(val);
                            val.receive_request = true;
                            delete val.distance;
                            delete val.unit;
                        }
                    }
                });
                // Update receive_request into list vehicle
                await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listLatLng));
                return listLatLngChoose;
            }
        }
        return false;
    }

    async findVanOrBagacNearCustomer(data: {
        customer_position: {lat: string, lng: string},
        type_group: string, service_id: number}) {
        const redisField = await this.fieldRedisVehiclePosition(data.type_group);
        const redisKey = await this.keyRedisVehiclePosition(data.type_group);
        let getCache = await this.redis.getInfoRedis(redisKey, redisField);
        if (getCache.isValidate) {
            const listLatLng = JSON.parse(getCache.value);
            const lat1 = parseFloat(data.customer_position.lat);
            const lon1 = parseFloat(data.customer_position.lng);
            const unit = 'K';
            let listLatLngChoose = [];
            if (listLatLng) {
                // Filter vehicle by vehicle type
                const findVehicleType = _.filter(listLatLng, { vehicle_type_id: data.service_id, receive_request: false });
                if (findVehicleType.length <= 0) return false;
                // get list station approve about distance
                findVehicleType.forEach((val) => {
                    const lat2 = parseFloat(val.lat);
                    const lon2 = parseFloat(val.lng);
                    const distance = this.calculateDistance(lat1, lon1, lat2, lon2, unit);
                    if (Number(distance) <= Number(process.env.BIKE_CAR_DISTANCE_APPROVED) || 2) {
                        // Get 3 driver
                        if (listLatLngChoose.length <= 3) {
                            val.distance = distance;
                            val.unit = unit;
                            listLatLngChoose.push(val);
                        }
                    }
                });
                return listLatLngChoose;
            }
        }
        return false;
    }

    async getDetailVehicleRunning(schedule_id: number) {
        return await this.vehicleRunningRepository.findOne({schedule_id});
    }

    async updateSeatMap(vehicle_schedule_id: number, seatMapNew: object, available_seat?: number) {
        // update seat map vehicle schedule
        let dataUpdate = {
            seat_map: seatMapNew,
        };
        if (available_seat) {
            dataUpdate = {...dataUpdate, ...{available_seat}};
        }
        await this.vehicleScheduleRepository.update({
            id: vehicle_schedule_id,
        }, dataUpdate);

        return await this.vehicleRunningRepository.update({
            schedule_id: vehicle_schedule_id,
        }, {seat_map: seatMapNew});
    }

    async fieldRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.fieldRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.fieldRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.fieldRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.fieldRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.fieldRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    async keyRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.keyRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.keyRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.keyRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.keyRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.keyRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    async fieldRedisRequestBooking(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.fieldRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.fieldRedisCustomerRequestBookCarBike;
                break;
            case 'CAR':
                redisField = this.fieldRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.fieldRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.fieldRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    async keyRedisRequestBooking(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.keyRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.keyRedisCustomerRequestBookCarBike;
                break;
            case 'CAR':
                redisField = this.keyRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.keyRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.keyRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    async submitEndSchedule(schedule_id: number, driver_uuid: string): Promise<VehicleRunningEntity> {
        // Remove request book shipping on redis by driver uuid
        const getCacheRequestBookShipping = await this.redis.getInfoRedis(
            this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping);
        if (getCacheRequestBookShipping.isValidate) {
            let listRequestBookShipping = JSON.parse(getCacheRequestBookShipping.value);
            _.remove(listRequestBookShipping, (req: any ) => {
                return req.driver_uuid === driver_uuid;
            });
            // Update request booking
            await this.redis.setInfoRedis(
                this.keyRedisCustomerRequestBookShipping, this.fieldRedisCustomerRequestBookShipping, JSON.stringify(listRequestBookShipping));
        }

        // Remove request book bus on redis by driver uuid
        const getCacheRequestBookBus = await this.redis.getInfoRedis(
            this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus);
        if (getCacheRequestBookBus.isValidate) {
            let listRequestBookBus = JSON.parse(getCacheRequestBookBus.value);
            _.remove(listRequestBookBus, (req : any) => {
                return req.driver_uuid === driver_uuid;
            });
            // Update request booking
            await this.redis.setInfoRedis(
                this.keyRedisCustomerRequestBookBus, this.fieldRedisCustomerRequestBookBus, JSON.stringify(listRequestBookBus));
        }

        // Update status vehicle schedule
        const schedule = await this.vehicleScheduleRepository.findOne({id: schedule_id});
        schedule.status = 3; // Đã kết thúc
        await this.vehicleScheduleRepository.save(schedule);
        // Update status vehicle running
        const vehicleRunningEntity = await this.vehicleRunningRepository.findOne({schedule_id});
        vehicleRunningEntity.status = 2; // Đã kết thúc
        // Remove schedule on redis
        await this.removeVehiclePositionRedis({
            type_group: 'BUS',
            vehicle_id: vehicleRunningEntity.vehicle_id,
        }, driver_uuid);
        // Get all ticket with status =2 by schedule id
        const ticket = await this.ticketRepository.createQueryBuilder('ticket')
            .where('ticket.status = :status', {status: 2})
            .andWhere('ticket.schedule_id = :schedule_id', {schedule_id})
            .select('ticket.user_id')
            .getMany();
        let customer_list_id = [];
        ticket.map((v) => {
            customer_list_id.push(v.user_id);
        });
        if (customer_list_id.length !== 0) {
            const userFCM = await this.firebaseRepository.createQueryBuilder('fcm')
                .where('fcm.user_id in(:customer_list_id)', {customer_list_id})
                .getMany();

            const listCustomerDeviceToken = [];
            const notification = {
                title: 'Chuyến xe đã hoàn thành',
                body: 'Chúc mừng bạn chuyến xe đã hoàn thành',
            };

            userFCM.map((v) => {
                const token = v.token ? v.token : v.apns_id;
                listCustomerDeviceToken.push(token);
            });

            const notificationData = {
                click_action: 'FLUTTER_NOTIFICATION_CLICK',
                key: KEY_DRIVER_SUBMIT_DONE_SCHEDULE,
                schedule_id,
            };
            // Emit event
            const userEntity = await this.userService.getAllUserBYlistId(customer_list_id);
            if (userEntity) {
                userEntity.map((user) => {
                    this.redisService.triggerEventBookBus({room: 'user_' + user.user_id.toString(), message: notificationData});
                });
            }
            await this.firebaseService.pushNotificationWithTokens(listCustomerDeviceToken, notification, notificationData);
        }
        return await this.vehicleRunningRepository.save(vehicleRunningEntity);
    }

    async findOne(schedule_id: number) {
        return await this.vehicleRunningRepository.createQueryBuilder('vehicle_running')
            .leftJoinAndSelect('vehicle_running.vehicleSchedule', 'vehicleSchedule')
            .leftJoinAndSelect('vehicleSchedule.route', 'route')
            .leftJoinAndSelect('route.endPoint', 'endPoint')
            .where('vehicle_running.schedule_id = :schedule_id', {schedule_id})
            .getOne();
    }

    async updateBySchedule(vehicleMoving: VehicleRunningEntity) {
        return await this.vehicleRunningRepository.save(vehicleMoving);
    }

    async getVehiclePostionOnRedisByDriverUuid(driver_uuid: string, type_group: string): Promise<any> {
        const redisField = await this.fieldRedisVehiclePosition(type_group);
        const redisKey = await this.keyRedisVehiclePosition(type_group);
        let getCache = await this.redis.getInfoRedis(redisKey, redisField);
        if (getCache.isValidate) {
            const listVehicle = JSON.parse(getCache.value);
            const findVehicle = _.find(listVehicle, vehicle => {
                return vehicle.user_uuid === driver_uuid;
            });
            if (!findVehicle) return false;
            return findVehicle;
        }
        return false;
    }
}
