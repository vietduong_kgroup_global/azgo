import {Body, Controller, HttpStatus, Param, Post, Get, Res, UseGuards, Req } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiImplicitParam, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { Response } from 'express';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { VehicleMovingService } from './vehicleMoving.service';
import { TicketService } from '../ticket/ticket.service';
import { RemoveStationDto } from './dto/removeStation.dto';
import { FindStationNearCustomerDto } from './dto/findStationNearCustomer.dto';
import { PositionVehicleDto } from './dto/positionVehicle.dto';
import * as jwt from 'jsonwebtoken';
import { FirebaseService } from '../firebase/firebase.service';
import { FirebaseRepository } from '../firebase/firebase.repository';
import { VehicleScheduleService } from '../vehicle-schedule/vehicleSchedule.service';
import { UsersService } from '../users/users.service';
import { RemoveVehiclePositionDto } from './dto/removeVehiclePosition.dto';
import { VehicleService } from '../vehicle/vehicle.service';
import { TestDistanceDto } from './dto/testDistance.dto';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import {RatingDriverService} from '../rating-driver/rating-driver.service';
import {VehicleGroup} from '../../enums/group.enum';
import {WalletService} from '../wallet/wallet.service';

@ApiUseTags('Vehicle Moving - For book vehicle')
@Controller('vehicle-moving')
export class VehicleMovingController {

    constructor(
        private vehicleMovingService: VehicleMovingService,
        private vehicleScheduleService: VehicleScheduleService,
        private firebaseRepository: FirebaseRepository,
        private ticketService: TicketService,
        private userService: UsersService,
        private readonly firebaseService: FirebaseService,
        private readonly vehicleService: VehicleService,
        private readonly ratingDriverService: RatingDriverService,
        private readonly walletService: WalletService,

    ) {
    }

    @Post('/start-moving/:schedule_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BUS]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Vehicle start moving' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'schedule_id', description: 'schedule Id, ex : 26', required: true, type: 'number' })
    async startMoving(@Param('schedule_id') schedule_id, @Res() res: Response, @Req() request) {
        const token = request.headers.authorization.split(' ')[1];
        const driverInfo: any = jwt.decode(token);
        try {
            const result = await this.vehicleMovingService.saveVehicleStartMoving(schedule_id, driverInfo.id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Chuyến xe đã khởi hành hoặc chưa tới giờ chạy', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/remove-station-moving')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Remove station when Vehicle pass station' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async removeStation(@Body() body: RemoveStationDto, @Res() res: Response) {
        try {

            if (!body.vehicle_id || !body.lat || !body.lng)
                return res.status(HttpStatus.UNPROCESSABLE_ENTITY).send(dataError('Error', null));
            const result = await this.vehicleMovingService.removeStationOnRedis(body);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.OK).send(dataSuccess('Not station removed', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/remove-vehicle-position-redis')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All,
        user_type: [UserType.ADMIN, UserType.DRIVER_VAN_BAGAC, UserType.DRIVER_BIKE_CAR, UserType.DRIVER_BUS]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Remove vehicle position on redis' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async removeVehiclePositionRedis(@Req() request, @Body() body: RemoveVehiclePositionDto, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const driverInfo: any =  jwt.decode(token);
            const result = await this.vehicleMovingService.removeVehiclePositionRedis(body, driverInfo.user_id.toString());
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.OK).send(dataSuccess('Not station removed', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/find-vehicle-bus-station-near-customer')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Find vehicle bus station near customer or filter provider & price' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async findStationsNearCustomer(@Body() body: FindStationNearCustomerDto, @Res() res: Response) {
        try {
            const result = await this.vehicleMovingService.findStationsNearCustomer(body);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
            }
            return res.status(HttpStatus.OK).send(dataSuccess('Not found vehicle schedule', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/position-vehicle-schedule-by-driver')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All,
        user_type: [UserType.ADMIN, UserType.DRIVER_VAN_BAGAC, UserType.DRIVER_BIKE_CAR, UserType.DRIVER_BUS]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update position vehicle schedule by driver ' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async updatePositionVehicle(@Req() request, @Body() body: PositionVehicleDto, @Res() res: Response) {
        try {
            /* Check exist vehicle group */
            if (!(body.type_group in VehicleGroup)) {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Type group incorrect!', null));
            }
            /* Check exist vehicle*/
            const checkExistVehicle = await this.vehicleService.findById(body.vehicle_id);
            if (!checkExistVehicle) {
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Vehicle not found', null));
            }
            if (checkExistVehicle.vehicleType.vehicleGroup.name !== body.type_group) {
                return res.status(HttpStatus.NOT_FOUND).send(dataError('This vehicle is not in this group', null));
            }
            /* Get info user login */
            const token = request.headers.authorization.split(' ')[1];
            let driverInfo: any = jwt.decode(token);
            const usersEntity = await this.userService.findOneByUserInfo(driverInfo);

            /*Switch case by vehicle group*/
            if (body.type_group === VehicleGroup.BUS) {
                /* Check exist vehicle running */
                const checkVehicleRunning = await this.vehicleScheduleService.findVehicleRunning(body.vehicle_id);
                if (!checkVehicleRunning) {
                    return res.status(HttpStatus.OK).send(dataSuccess('Vehicle not running or schedule is overdue', null));
                }
                if (checkVehicleRunning.user_id !== usersEntity.id) {
                    return res.status(HttpStatus.OK).send(dataSuccess('This ride is not in your possession', null));
                }
                /* Set data to update on redis */
                // @ts-ignore
                body.user_uuid = usersEntity.user_id.toString();
                // rating provider
                const avgRatingProvider = await this.vehicleScheduleService.getAllScheduleByProvider(checkVehicleRunning.provider_id);
                const addData = {
                    end_point: checkVehicleRunning.route.end_point,
                    schedule_id: checkVehicleRunning.id,
                    start_date: checkVehicleRunning.start_date,
                    end_hour: checkVehicleRunning.end_hour,
                    seat_map: checkVehicleRunning.seat_map,
                    max_seat_of_horizontal: checkVehicleRunning.vehicle.seatPattern.max_seat_of_horizontal,
                    max_seat_of_vertical: checkVehicleRunning.vehicle.seatPattern.max_seat_of_vertical,
                    floor: checkVehicleRunning.vehicle.seatPattern.floor,
                    provider_name: checkVehicleRunning.provider.provider_name,
                    available_seat: checkVehicleRunning.available_seat,
                    license_plates: checkVehicleRunning.vehicle.license_plates,
                    pickup_points: checkVehicleRunning.route.router.pickup_points,
                    base_price: checkVehicleRunning.route.base_price,
                    price_per_km: checkVehicleRunning.route.price_per_km,
                    provider_id: checkVehicleRunning.provider_id,
                    logo: checkVehicleRunning.provider.image_profile,
                    router: checkVehicleRunning.route.router,
                    avg_rating: avgRatingProvider.avg ? parseFloat(avgRatingProvider.avg).toFixed(1) : '0.0',
                };
                const data = {...body, ...addData};
                const result = await this.vehicleMovingService.updatePositionVehicleToRedis(data);
                if (result) {
                    return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
                }
                return res.status(HttpStatus.OK).send(dataSuccess('Không tìm thấy xe trên redis', null));
            }
            else {
                // check condition submit start
                const walletBydriver = await this.walletService.getOneByDriverId(driverInfo.id);
                if (walletBydriver.fee_wallet < 50) {
                    return res.status(HttpStatus.BAD_REQUEST)
                        .send(dataError('Vui lòng nạp tiền thêm vào tài khoản doanh thu, tài khoản doanh thu phải >= 50k', null));
                }
                const vehicleEntity = await this.vehicleService.findOne(body.vehicle_id);
                const ratingDriver = await this.ratingDriverService.avgRatingDriver(usersEntity.id);
                const addData = {
                    license_plates: vehicleEntity.license_plates,
                    vehicle_type_id: vehicleEntity.vehicle_type_id,
                    vehicle_type_name: vehicleEntity.vehicleType.name,
                    user_uuid: usersEntity.user_id.toString(),
                    user_id: usersEntity.id,
                    full_name: usersEntity.driverProfile.full_name,
                    phone: usersEntity.phone,
                    image_profile: usersEntity.driverProfile.image_profile,
                    avg_rating: Number(ratingDriver).toFixed(1),
                    receive_request: false,
                    count_receive_request: 0,
                    fee_wallet: walletBydriver.fee_wallet,
                };
                const data = {...body, ...addData};
                const result = await this.vehicleMovingService.updatePositionVehicleToRedis(data);
                if (result) {
                    return res.status(HttpStatus.OK).send(dataSuccess('OK', null));
                }
                return res.status(HttpStatus.OK).send(dataSuccess('Không tìm thấy xe trên redis', null));
            }
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('vehicle-running/detail-follow-schedule/:schedule_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get detail vehicle running' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'schedule_id', description: 'Vehicle schedule Id', required: true, type: 'number' })
    async getDetailVehicleRunningBySchedule(@Param('schedule_id') schedule_id, @Res() res: Response) {
        try {
            const result = await this.vehicleMovingService.getDetailVehicleRunning(schedule_id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('the-end-of-schedule-by-driver/:schedule_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BUS]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'The end of schedule by driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'schedule_id', description: 'Vehicle schedule Id', required: true, type: 'number' })
    async endSchedule(@Req() request, @Param('schedule_id') schedule_id, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const driverInfo: any =  jwt.decode(token);

            const result = await this.vehicleMovingService.submitEndSchedule(schedule_id, driverInfo.user_id.toString());
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('get-a-schedule-first-of-the-day-compared-to-time-now-by-driver')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BUS]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get a schedule first of the day compared to time now' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getOneSchedulePrepareByDriver(@Req() request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const driverInfo: any = jwt.decode(token);
        try {
            const result = await this.vehicleScheduleService.getOneByDriver(driverInfo.id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Chưa tìm thấy chuyến, vui lòng liên hệ nhà xe!', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));
        }
    }

    @Post('test-distance-bettwen-two-points')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'test-distance-bettwen-two-points' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getDistace(@Req() request, @Res() res: Response, @Body() body: TestDistanceDto) {
        try {
            const result = await this.vehicleMovingService.calculateDistance(
                body.start_point.lat,
                body.start_point.lng,
                body.end_point.lat,
                body.end_point.lng,
                'K',
            );
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));
        }
    }
}
