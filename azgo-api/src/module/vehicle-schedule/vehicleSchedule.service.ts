import {forwardRef, Inject, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {VehicleScheduleRepository} from './vehicleSchedule.repository';
import {VehicleScheduleEntity} from './vehicleSchedule.entity';
import {RoutesService} from '../routes/routes.service';
import {CreateVehicleScheduleDto} from './dto/createVehicleSchedule';
import {VehicleService} from '../vehicle/vehicle.service';
import {TicketService} from '../ticket/ticket.service';
import {VehicleRunningRepository} from '../vehicle-running/vehicleRunning.repository';
import {RatingScheduleService} from '../rating-schedule/rating-schedule.service';
import * as moment from 'moment';
import {UsersService} from '../users/users.service';
import {BookShippingService} from '../book-shipping/book-shipping.service';
import {Brackets} from 'typeorm';
import {VehicleMovingService} from '../vehicle-moving/vehicleMoving.service';
import {FindRouteVehicleDto} from '../book-ticket/dto/find-route-vehicle.dto';
import {VehicleRunningEntity} from '../vehicle-running/vehicleRunning.entity';
import {UsersEntity} from '../users/users.entity';
import {UserType} from '@azgo-module/core/dist/utils/enum.ultis';
import {StatusVehicleSchedule} from '../../enums/status.enum';

@Injectable()
export class VehicleScheduleService  {
    constructor(
        @InjectRepository(VehicleScheduleRepository)
        private readonly vehicleScheduleRepository: VehicleScheduleRepository,
        @InjectRepository(VehicleRunningRepository)
        private readonly vehicleRunningRepository: VehicleRunningRepository,
        private readonly routeService: RoutesService,
        private readonly vehicleService: VehicleService,
        @Inject(forwardRef(() => TicketService))
        private readonly ticketService: TicketService,
        @Inject(forwardRef(() => RatingScheduleService))
        private readonly ratingScheduleService: RatingScheduleService,
        private readonly userService: UsersService,
        private readonly bookShippingService: BookShippingService,
        private readonly vehicleMovingService: VehicleMovingService,
    ) {
    }

    async cronJobCreateSchedule() {
        const today = moment().format('YYYY-MM-DD');

        /*
            Create vehicle schedule at 9 AM
            - Provider: Phương Trang - id = 1
            - Driver: Anh  - id = 25
            - Tuyến: Tuyến Sài Gòn - Cần Thơ - id = 1
        */
        const findRouteByPT = await this.routeService.findOne(1);
        if (findRouteByPT) {
            const findVehicleByPT = await this.vehicleService.findVehicleByProviderId(1);
            if (findVehicleByPT) {
                const start_date = moment(today + ' 09:00').toDate();
                const end_hour = moment(today + ' ' + findRouteByPT.duration).format( 'HH:mm DD-MM-YYYY');
                const dataInsert = {
                    provider_id: 1,
                    vehicle_type: findVehicleByPT.vehicle_type_id,
                    vehicle_id: findVehicleByPT.id,
                    route_id: 1,
                    user_id: 25,
                    sub_driver: 'Chân dài 2m',
                    end_hour,
                    start_date,
                    start_hour: '09:00',
                    platform: 1,
                };
                await this.create(dataInsert);
            }
        }

        /*
          Create vehicle schedule at 11 AM
          - Nhà xe:  Admin id = 3
          - Driver: Louis Thành - id = 18
          - Tuyến : Đà Lạ - Nha Trrang - id = 4
        */
        const findRouteByAdmin = await this.routeService.findOne(4);
        if (findRouteByAdmin) {
            const findVehicleByAdmin = await this.vehicleService.findVehicleByProviderId(3);
            if (findVehicleByAdmin) {
                const start_date = moment(today + ' 11:00').toDate();
                const end_hour = moment(today + ' ' + findRouteByAdmin.duration).format( 'HH:mm DD-MM-YYYY');
                const dataInsert = {
                    provider_id: 3,
                    vehicle_type: findVehicleByAdmin.vehicle_type_id,
                    vehicle_id: findVehicleByAdmin.id,
                    route_id: 4,
                    user_id: 18,
                    sub_driver: 'Chân ngắn 2m',
                    end_hour,
                    start_date,
                    start_hour: '11:00',
                    platform: 1,
                };
                console.log('created admin');
                await this.create(dataInsert);
            }
        }
        /*
          Create vehicle schedule at 14 PM
          - Provider: Thành Bưởi - id = 2 // id provider ìnfo
          - Driver: Sara Han - id = 10
          - Tuyến: Sài Gòn - Đà Lạt - id = 3
        */
        const findRouteByTB = await this.routeService.findOne(3);
        if (findRouteByTB) {
            const findVehicleByTB = await this.vehicleService.findVehicleByProviderId(2);
            if (findVehicleByTB) {
                const start_date = moment(today + ' 14:00').toDate();
                const end_hour = moment(today + ' ' + findRouteByTB.duration).format( 'HH:mm DD-MM-YYYY');
                const dataInsert = {
                    provider_id: 2,
                    vehicle_type: findVehicleByTB.vehicle_type_id,
                    vehicle_id: findVehicleByTB.id,
                    route_id: 3,
                    user_id: 10,
                    sub_driver: 'Thanh Buoi sub',
                    end_hour,
                    start_date,
                    start_hour: '14:00',
                    platform: 1,
                };
                console.log('created Thanh Buoi');
                await this.create(dataInsert);
            }
        }
    }

    async filterRouteSchedules(data: FindRouteVehicleDto) {
        return await this.vehicleScheduleRepository.filterRouteSchedules(data);
    }
    async create(data: CreateVehicleScheduleDto){
        const route = await this.routeService.findOne(data.route_id);
        const vehicle = await this.vehicleService.findById(data.vehicle_id);
        const vehicleSchedule = new VehicleScheduleEntity();
        if (route !== undefined && vehicle !== undefined) {
            const provider = {provider_id: route.provider.id};
            const seatPattern = {
                seat_map: vehicle.seatPattern.seat_map,
                available_seat: vehicle.seatPattern.total_seat,
            };
            const dataSave = { ...vehicleSchedule, ...data, ...provider, ...seatPattern };
            return await this.vehicleScheduleRepository.save(dataSave);
        }
        return false;
    }
    async update(id: number, data){
        const vehicleSchedule = await this.vehicleScheduleRepository.findOne({ id });
        // const dataSave = {..vehicleSchedule, ...data };
        vehicleSchedule.user_id = data.user_id;
        vehicleSchedule.vehicle_id = data.vehicle_id;
        vehicleSchedule.route_id = data.route_id;
        vehicleSchedule.start_date = data.start_date;
        vehicleSchedule.start_hour = data.start_hour;
        vehicleSchedule.end_hour = data.end_hour;
        vehicleSchedule.seat_map = data.seat_map;
        vehicleSchedule.available_seat = data.available_seat;
        return await this.vehicleScheduleRepository.save(vehicleSchedule);
    }
    async getDetailById(id: number) {
        return await this.vehicleScheduleRepository.findOneOrFail(id);
    }

    async updateEntity(vehicleSchedule: VehicleScheduleEntity) {
        return await this.vehicleScheduleRepository.save(vehicleSchedule);
    }

    async getDetail(id: number, user?: UsersEntity) {
        let query = this.vehicleScheduleRepository.createQueryBuilder('schedule')
            .leftJoinAndSelect('schedule.user', 'user')
            .leftJoinAndSelect('schedule.vehicle', 'vehicle')
            .leftJoinAndSelect('vehicle.seatPattern', 'seatPattern')
            .leftJoinAndSelect('schedule.route', 'route')
            .leftJoinAndSelect('route.startPoint', 'startPoint')
            .leftJoinAndSelect('route.endPoint', 'endPoint')
            .leftJoinAndSelect('schedule.provider', 'provider')
            .where('schedule.id = :id', {id});

        const getOneTicketBySchedule = await this.ticketService.findOneBySchedule(id);
        if (getOneTicketBySchedule) {
            query.leftJoinAndSelect('schedule.tickets', 'tickets')
            .leftJoinAndSelect('tickets.user', 'ticketsUser')
            .leftJoinAndSelect('ticketsUser.customerProfile', 'customerProfile')
            .andWhere('tickets.deleted_at is null');
        }
        if (user && user.type === UserType.PROVIDER) {
            query.leftJoin('provider.users', 'user_provider')
                .andWhere('user_provider.id = :user_id', {user_id: user.id});
        }

        return await query.getOne();
    }
    async getAllVehicleSchedule(limit: number, offset: number, options?: any){
        return await this.vehicleScheduleRepository.filterSchedules(limit, offset, options);
    }
    async deleteVehicleSchedule(schedule_id: number) {
        // remove schedule on redis
        await this.vehicleMovingService.removeScheduleOnRedis(schedule_id);
        const vehicleScheduleEntity = await this.vehicleScheduleRepository.findOne({ id: schedule_id });
        if (vehicleScheduleEntity) {
            await this.ticketService.deleteAllTicketBySchedule(schedule_id);
            await this.vehicleRunningRepository.createQueryBuilder('vehicleRunnung')
                .update(VehicleRunningEntity)
                .set({deleted_at: new Date()})
                .where('schedule_id = :schedule_id', {schedule_id})
                .execute();
            await this.bookShippingService.deleteAllBookShippingBySchedule(schedule_id);
            vehicleScheduleEntity.deleted_at = new Date();
            return await this.vehicleScheduleRepository.save(vehicleScheduleEntity);
        }
        return false;

    }

    async findVehicleRunning(vehicle_id: number): Promise<any> {
        const today = moment().format('YYYY-MM-DD').concat(' 00:00:00');
        return await this.vehicleScheduleRepository.createQueryBuilder('schedule')
            .leftJoinAndSelect('schedule.vehicle', 'vehicle')
            .leftJoinAndSelect('vehicle.seatPattern', 'seatPattern')
            .leftJoinAndSelect('schedule.provider', 'provider')
            .leftJoinAndSelect('schedule.route', 'route')
            .leftJoinAndSelect('route.endPoint', 'endPoint')
            .where('schedule.vehicle_id = :vehicle_id', {vehicle_id})
            .andWhere('schedule.status = :status', {status: StatusVehicleSchedule.STARTING})
            .andWhere('schedule.start_date >= :today', {today})
            .getOne();

    }

    async getAllScheduleRanByDriver(user_id: number, status: number) {
          return await this.vehicleScheduleRepository.createQueryBuilder('vehicleSchedule')
              .leftJoinAndSelect('vehicleSchedule.vehicle', 'vehicle')
              .leftJoinAndSelect('vehicle.vehicleType', 'vehicleType')
              .leftJoinAndSelect('vehicleSchedule.provider', 'provider')
              .leftJoinAndSelect('vehicleSchedule.route', 'route')
              .leftJoinAndSelect('route.endPoint', 'endPoint')
              .where('vehicleSchedule.user_id = :user_id', {user_id})
              .andWhere('vehicleSchedule.status = :status', {status})
              .orderBy('vehicleSchedule.start_date', 'DESC')
              .getMany();
    }

    async getCustomerOrderSchedule(schedule_id){
        const result = await this.ticketService.findBySchedule(schedule_id);
        if (result) {
            return  result;
        }
        return false;
    }

    async getCustomerOnBus(schedule_id){
        const result = await this.ticketService.findCustomerOnBus(schedule_id);
        if (result) {
            return  result;
        }
        return false;
    }

    async updateStatusDoneSchedule(id){
        const vehicleSchedule = await this.vehicleScheduleRepository.findOne({ id });
        const vehicleRunning = await this.vehicleRunningRepository.findOne({schedule_id: id});
        const dataSaveVehicleSchedule = { ...vehicleSchedule, ...{status: 3} };
        const dataSaveVehicleRunning = { ...vehicleRunning, ...{status: 2} };
        const updateVehicleSchedule = await this.vehicleScheduleRepository.save(dataSaveVehicleSchedule);
        const updateVehicleRunning = await this.vehicleRunningRepository.save(dataSaveVehicleRunning);
        if (updateVehicleSchedule && updateVehicleRunning) {
            const data = {vehicle_running: updateVehicleRunning, vehicle_schedule: updateVehicleSchedule};
            return data;
        }
        return false;
    }
    async getAllByDriver(user_id: number, status: number =  1) {
        return  await this.vehicleScheduleRepository.createQueryBuilder('schedule')
            .where('schedule.user_id = :user_id', {user_id})
            .where('schedule.status = :status', {status})
            .orderBy('schedule.start_date', 'DESC')
            .getMany();
    }
    async getAllByProvider(provider_id: number, status: number =  1) {
        return  await this.vehicleScheduleRepository.createQueryBuilder('schedule')
            .where('schedule.provider_id = :provider_id', {provider_id})
            .where('schedule.status = :status', {status})
            .orderBy('schedule.start_date', 'DESC')
            .getMany();
    }

    async getOneByDriver(user_id: number) {
        // Get all schedule by driver id
        const start_date = moment().format('YYYY-MM-DD');
        const scheduleStarting =  await this.vehicleScheduleRepository.createQueryBuilder('schedule')
            .leftJoinAndSelect('schedule.vehicle', 'vehicle')
            .leftJoinAndSelect('vehicle.seatPattern', 'seatPattern')
            .leftJoinAndSelect('schedule.route', 'route')
            .leftJoinAndSelect('route.startPoint', 'startPoint')
            .leftJoinAndSelect('route.endPoint', 'endPoint')
            .leftJoinAndSelect('schedule.provider', 'provider')
            .where('schedule.user_id = :user_id', {user_id})
            .andWhere('schedule.status = :status', {status: StatusVehicleSchedule.STARTING}) // Đẫ khởi hành
            // .andWhere('schedule.start_date like :start_date', {start_date: '%' + start_date + '%'})
            .orderBy('schedule.start_date', 'ASC')
            .getOne();
        if (scheduleStarting) {
            return scheduleStarting;
        } else {
            return await this.vehicleScheduleRepository.createQueryBuilder('schedule')
                .leftJoinAndSelect('schedule.vehicle', 'vehicle')
                .leftJoinAndSelect('vehicle.seatPattern', 'seatPattern')
                .leftJoinAndSelect('schedule.route', 'route')
                .leftJoinAndSelect('route.startPoint', 'startPoint')
                .leftJoinAndSelect('route.endPoint', 'endPoint')
                .leftJoinAndSelect('schedule.provider', 'provider')
                .where('schedule.user_id = :user_id', {user_id})
                .andWhere('schedule.status = :status', {status: StatusVehicleSchedule.UNSTART}) //  Chưa khởi hành
                .andWhere('schedule.start_date like :start_date', {start_date: '%' + start_date + '%'})
                .orderBy('schedule.start_date', 'ASC')
                .getOne();
        }

    }
    async getAllScheduleByProvider(provider_id: number, status: number = 1) {
        const result =  await this.vehicleScheduleRepository.createQueryBuilder('schedule')
            .select('schedule.id')
            .where('schedule.user_id = :provider_id', {provider_id})
            .andWhere('schedule.status = :status', {status})
            .getRawMany();
        const listIdSchedule = [];
        if (result.length > 0) {
            result.forEach(item => {
                listIdSchedule.push(item.schedule_id);
            });
            return await this.ratingScheduleService.avgRatingByListScheduleId(listIdSchedule);
        }
        return false;

    }

    async checkVehicleSchedule(shcedule_id: number, status: number) {
        const today = moment().format('YYYY-MM-DD HH:mm:ss');
        return await this.vehicleScheduleRepository.createQueryBuilder('schedule')
            .leftJoinAndSelect('schedule.vehicle', 'vehicle')
            .leftJoinAndSelect('schedule.route', 'route')
            .leftJoinAndSelect('vehicle.seatPattern', 'seatPattern')
            .where('schedule.id = :shcedule_id', {shcedule_id})
            .andWhere('schedule.status = :status', {status})
            .andWhere('schedule.start_date >= :today', {today})
            .getOne();
    }

    async getScheduleByStatus(options: {provider_id: number, status: number, limit: number}) {
        return await this.vehicleScheduleRepository.createQueryBuilder('schedule')
            .leftJoinAndSelect('schedule.vehicle', 'vehicle')
            .leftJoinAndSelect('vehicle.seatPattern', 'seatPattern')
            .leftJoinAndSelect('schedule.route', 'route')
            .leftJoinAndSelect('route.startPoint', 'startPoint')
            .leftJoinAndSelect('route.endPoint', 'endPoint')
            .leftJoinAndSelect('schedule.provider', 'provider')
            .where('schedule.status = :status', {status: options.status})
            .andWhere('schedule.provider_id = :provider_id', {provider_id: options.provider_id})
            .orderBy('schedule.start_date', 'DESC')
            .limit(Number(options.limit))
            .getMany();
    }

    async revenuePerSchedule(limit: number, offset: number,
                             options?: {from_date?: string, to_date?: string, provider_id: number, driver_id: number, keyword?: string }) {
        // create new query
        let query =  await this.vehicleScheduleRepository.createQueryBuilder('schedule')
            .leftJoinAndSelect('schedule.tickets', 'tickets')
            .leftJoinAndSelect('schedule.user', 'user')
            .leftJoinAndSelect('schedule.vehicle', 'vehicle')
            .leftJoinAndSelect('vehicle.vehicleType', 'vehicleType')
            .leftJoinAndSelect('schedule.provider', 'provider')
            .leftJoinAndSelect('schedule.route', 'route');
        if (!options.keyword === false) {
            query.andWhere(new Brackets(qb => {
                qb.where('provider.full_name like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('vehicle.license_plates like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('route.name like :keyword', { keyword: '%' + options.keyword.trim() + '%' });
            }));
        }
        if (!options.provider_id === false) {
            query.andWhere('schedule.provider_id = :provider_id', { provider_id: options.provider_id });
        }

        if (!options.driver_id === false) {
            query.andWhere('user.user_id = :driver_id', { driver_id: options.driver_id });
        }

        if (options.from_date) {
            const from_date_tostring = await this.formatDate(options.from_date) + ' 00:00:00';
            query.andWhere('schedule.start_date >= :from_date_tostring', {from_date_tostring});
        }
        if (options.to_date) {
            const to_date_tostring = await this.formatDate(options.to_date) + ' 23:59:59';
            query.andWhere('schedule.start_date <= :to_date_tostring', {to_date_tostring});
        }
        // // list
        query.select('vehicle.license_plates as license_plates,' +
            ' user.full_name as full_name,' +
            ' vehicleType.name as vehicle_type_name,' +
            ' schedule.start_date as start_date,' +
            ' route.name as route_name,' +
            ' SUM(tickets.price) as total_price,' +
            ' SUM(tickets.price) as total_price_azgo')
            .groupBy('schedule.id')
            .orderBy('schedule.id', 'DESC')
            .limit(limit)
            .offset(offset);

        const results = await query.getRawMany();
        const count =  await query.getCount();
        return {
            success: true,
            data: results,
            total: count,
        };
    }

    /*
    * Input format: DD/MM/YYYY
    *
    * */
    formatDate(date: string) {
        const dateSplit = date.split('/');
        // month is 0-based, that's why we need dataParts[1] - 1
        const dateObject = new Date(+dateSplit[2],  +dateSplit[1] - 1, +dateSplit[0]);
        const formatDate = moment(dateObject, 'YYYY-MM-DD');
        return formatDate.format('YYYY-MM-DD').toString();
    }
}
