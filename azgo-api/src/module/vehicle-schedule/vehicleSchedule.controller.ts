import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import {
    Body,
    Controller,
    HttpStatus,
    Param,
    Post,
    Res,
    UseGuards,
    Put,
    Get,
    Query,
    Delete,
    Req,
    Inject, forwardRef,
} from '@nestjs/common';
import { VehicleScheduleService } from './vehicleSchedule.service';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { CreateVehicleScheduleDto } from './dto/createVehicleSchedule';
import { UpdateVehicleScheduleDto } from './dto/updateVehicleSchedule.dto';
import * as jwt from 'jsonwebtoken';
import { UsersService } from '../users/users.service';
import {VanBagacOrdersService} from '../van-bagac-orders/van-bagac-orders.service';
import { RatingScheduleService } from '../rating-schedule/rating-schedule.service';
import { TicketService } from '../ticket/ticket.service';
import { BookShippingService } from '../book-shipping/book-shipping.service';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { ProvidersInfoService } from '../providers-info/providers-info.service';
import { VehicleGroup } from '@azgo-module/core/dist/utils/enum.ultis';
import { CustomChargeService } from '../custom-charge/custom-charge.service';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@ApiUseTags('Vehicle Schedule')
@Controller('vehicle-schedule')
export class VehicleScheduleController {
    constructor(
        private readonly vehicleScheduleService: VehicleScheduleService,
        private readonly userService: UsersService,
        @Inject(forwardRef(() => VanBagacOrdersService))
        private readonly vanBagacOrdersService: VanBagacOrdersService,
        @Inject(forwardRef(() => RatingScheduleService))
        private readonly ratingScheduleService: RatingScheduleService,
        @Inject(forwardRef(() => TicketService))
        private readonly ticketService: TicketService,
        @Inject(forwardRef(() => BookShippingService))
        private readonly bookShippingService: BookShippingService,
        private readonly providersInfoService: ProvidersInfoService,
        private readonly customChageService: CustomChargeService,

    ) {
    }

    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Add vehicle schedule' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async saveVehicleSchedule(@Body() body: CreateVehicleScheduleDto, @Res() res: Response) {
        try {
            const result = await this.vehicleScheduleService.create(body);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:vehicle_schedule_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Edit vehicle schedule' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'vehicle_schedule_id', description: 'Vehicle schedule Id', required: true, type: 'number' })
    async updateVehicleSchedule(@Param('vehicle_schedule_id') vehicle_schedule_id,  @Body() body: UpdateVehicleScheduleDto, @Res() res: Response) {
        try {
            const result = await this.vehicleScheduleService.update(vehicle_schedule_id, body);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/:vehicle_schedule_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER, UserType.DRIVER_BUS ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get detail vehicle schedule' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'vehicle_schedule_id', description: 'Vehicle schedule Id', required: true, type: 'number' })
    async detailVehicleSchedule(@Req() req, @Param('vehicle_schedule_id') vehicle_schedule_id, @Res() res: Response) {
        let result;
        try {
            result = await this.vehicleScheduleService.getDetail(vehicle_schedule_id, req.user);
            if (!result)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
        // Optimizie
        try {
            const results = await Promise.all([
                this.ratingScheduleService.avgRatingByScheduleId(result.id), // Avg rating
                this.ticketService.countTicketAndSumPrice(result.id, 2), // count order bus, get total price
                this.ticketService.countTicketAndSumPrice(result.id, 1), // count order ticket, get total price
                this.bookShippingService.countBookShippingAndSumPrice(result.id), // count shipping, get total price
            ]);
            const data = {...result, ...
                {avgRatingSchedule: results[0].avg ?  results[0].avg : 0 },
                ...{countTicketAndSumPriceBookBus: results[1], countBookShippingAndSumPriceBookTicket: results[2],
                    countTicketAndSumPriceBookTicket: results[3]},
            };
            return res.status(HttpStatus.OK).send(dataSuccess('oK', data));
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Bad requests', null));
        }
    }

    @Get('/')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all vehicle schedule' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'provider_id', description: 'Provider id, from provider info entity', required: false, type: 'number'})
    async getAllVehicleSchedule( @Req() request, @Res() res: Response) {
        if (request.user.type === UserType.PROVIDER) {
            try {
                const provider = await this.providersInfoService.findByUserId(request.user.id);
                if (!provider) return res.status(HttpStatus.BAD_REQUEST).send(dataError('Lỗi, không tìm thấy nhà cung cấp', null));
                request.query.provider_id = provider.id;
            } catch (e) {
                return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Bad requests', null));
            }
        }
        // ===============Query offset + per_page=============
        let results;
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        try {
            results = await this.vehicleScheduleService.getAllVehicleSchedule(per_page, offset, request.query);
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    @Delete('/:vehicle_schedule_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete vehicle schedule' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'vehicle_schedule_id', description: 'Vehicle schedule Id', required: true, type: 'number' })
    async deleteVehicleSchedule(@Param('vehicle_schedule_id') vehicle_schedule_id, @Res() res: Response) {
        try {
            const result = await this.vehicleScheduleService.deleteVehicleSchedule(vehicle_schedule_id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Lỗi', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('get-customer-order-bus/:vehicle_schedule_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.DRIVER_BUS]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get list customer order schedule' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'vehicle_schedule_id', description: 'Vehicle schedule Id', required: true, type: 'number' })
    async getCustomerOrderBus(@Param('vehicle_schedule_id') vehicle_schedule_id, @Res() res: Response){
        try {
            const result = await this.vehicleScheduleService.getCustomerOrderSchedule(vehicle_schedule_id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @Get('get-customer-on-bus/:vehicle_schedule_id')
    @ApiOperation({ title: 'Get list customer on schedule' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'vehicle_schedule_id', description: 'Vehicle schedule Id', required: true, type: 'number' })
    async getCustomerOnBus(@Param('vehicle_schedule_id') vehicle_schedule_id, @Res() res: Response){
        try {
            const result = await this.vehicleScheduleService.getCustomerOnBus(vehicle_schedule_id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('submit-done-schedule/:vehicle_schedule_id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BUS]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Submit done schedule' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'vehicle_schedule_id', description: 'Vehicle schedule Id', required: true, type: 'number' })
    async updateStatusDoneSchedule(@Param('vehicle_schedule_id') vehicle_schedule_id, @Res() res: Response) {
        try {
            const result = await this.vehicleScheduleService.updateStatusDoneSchedule(vehicle_schedule_id);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('Done', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
    @Get('history/get-list-schedule-by-driver-follow-status')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BUS]})
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get list schedule ran by driver follow status' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'status', description: '1:Chua khoi hanh, 2: Da khoi hanh, 3 Ket thuc'})
    async listTicketFollowDriver(@Req() request,  @Res() res: Response, @Query('status') status: number) {
        const token = request.headers.authorization.split(' ')[1];
        // decode token
        const customerInfo =  jwt.decode(token);
        const findUserByUuid = await this.userService.findOneByUserInfo(customerInfo); // uuid
        if (findUserByUuid) {
            try {

                const result = await this.vehicleScheduleService.getAllScheduleRanByDriver(findUserByUuid.id, status);
                if (result) {
                    return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
                }
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
            } catch (error) {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
            }
        }
        return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));

    }

    @Get('get-vehicle-schedule/nearest')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get vehicle schedule nearest' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'status', description: '1:Chua khoi hanh, 2: Da khoi hanh, 3 Ket thuc', type: Number, required: true})
    @ApiImplicitQuery({name: 'limit', description: 'Limit', required: true, type: Number})
    async getScheduleByStatus(@Req() request, @Res() res: Response){
        try {
            if (request.user.type !== 4) {
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Please login account provider', null));
            }
            const providerEntity = await this.providersInfoService.findByUserId(request.user.id);
            const options = {...request.query, ...{provider_id: providerEntity.id}};
            const rs = await this.vehicleScheduleService.getScheduleByStatus(options);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', rs));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));
        }
    }

    @Get('revenue/per-vehicle-schedule')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Revenue per vehicle schedule' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'from_date', description: '31/3/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'to_date', description: '31/4/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'provider_id', description: 'Provider id', required: false, type: Number})
    @ApiImplicitQuery({name: 'driver_id', description: 'Driver id', required: false, type: Number})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    async revenuePerVehicle(@Req() request, @Res() res: Response) {
        // ===============Query offset + per_page=============
        let results;
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        try {
            const token = request.headers.authorization.split(' ')[1];
            const userLogin =  jwt.decode(token);
            const usersEntity = await this.userService.findOneByUserInfo(userLogin);
            if (usersEntity.providersInfo) {
                request.query.provider_id = usersEntity.providersInfo.id;
            }
            results = await this.vehicleScheduleService.revenuePerSchedule(per_page, offset, request.query);
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }
        /* Tỷ lệ doanh thu azgo*/
        await Promise.all(results.data.map(async (value) => {
            const customCharge = await this.customChageService.getOneByVehicleGroup(VehicleGroup.BUS);
            if (customCharge.rate_of_charge === 0) {
                throw {message: 'rate_of_charge right thaner 0', status: HttpStatus.BAD_REQUEST};
            }
            value.total_price_azgo = Math.ceil((value.total_price * customCharge.rate_of_charge) / 100);
        }));
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }
}
