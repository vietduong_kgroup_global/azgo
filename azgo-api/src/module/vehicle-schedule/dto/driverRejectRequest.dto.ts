import {ApiModelProperty} from '@nestjs/swagger';
import {
    IsNotEmpty,
    IsNumber,
    IsString,
} from 'class-validator';

export class DriverRejectRequestDto {

    @IsNotEmpty()
    @ApiModelProperty({example: 'customer_uuid',  required: true})
    @IsString()
    customer_uuid: string;

    @ApiModelProperty({example: 'Content', required: true})
    reason_reject: string;

    @IsNotEmpty({message: 'Schedule id is require'})
    @ApiModelProperty({example: 1, required: true})
    schedule_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1000})
    price: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: ['A1', 'B2']})
    order_seat: string[];

    @IsNotEmpty()
    @ApiModelProperty({ example: 1})
    @IsNumber()
    floor: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'HCM'})
    pickup_point: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'Can Tho'})
    destination: string;
}
