import {ApiModelProperty} from '@nestjs/swagger';
import {
    IsNotEmpty,
    IsNumber,
    IsString,
} from 'class-validator';

export class CreateVehicleScheduleDto {

    @IsNotEmpty({message: 'Vehicle id is require'})
    @IsNumber()
    @ApiModelProperty({example: 1, required: true})
    vehicle_id: number;

    @IsNotEmpty({message: 'User id is require'})
    @ApiModelProperty({example: 1, required: true})
    @IsNumber()
    user_id: number;

    @IsNotEmpty({message: 'Route id is require'})
    @IsNumber()
    @ApiModelProperty({example: 1, required: true})
    route_id: number;

    @IsNotEmpty({message: 'Start date is require'})
    @ApiModelProperty({example: '31/3/2020', required: true})
    start_date: Date;

    @IsNotEmpty({message: 'Start hour is require'})
    @IsString()
    @ApiModelProperty({example: '20:30', required: true})
    start_hour: string;

    @IsNotEmpty({message: 'Start hour is require'})
    @IsString()
    @ApiModelProperty({example: '22:30', required: true})
    end_hour: string;

    @ApiModelProperty({example: 'Thị N', required: false})
    sub_driver: string;

}
