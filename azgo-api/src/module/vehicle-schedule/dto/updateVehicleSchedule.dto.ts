import {ApiModelProperty} from '@nestjs/swagger';
import {
    IsNotEmpty,
    IsInt,
} from 'class-validator';

export class UpdateVehicleScheduleDto {

    @ApiModelProperty({example: 1, required: false})
    vehicle_id: number;

    @ApiModelProperty({example: 1, required: false})
    user_id: number;

    @ApiModelProperty({example: 1, required: false})
    route_id: number;

    @ApiModelProperty({example: '31/3/2020', required: false})
    start_date: Date;

    @ApiModelProperty({example: '14:00', required: false})
    start_hour: string;

    @ApiModelProperty({example: '18:00', required: false})
    end_hour: string;

    @IsNotEmpty({message: 'Seat map'})
    @ApiModelProperty({
        example: {
            f1: [
                [
                    {
                        id: 11,
                        name: 11,
                        number: 1,
                        status: false,
                        is_reserved: false,
                        is_selected: true,
                    },
                    {
                        id: 12,
                        name: 12,
                        number: 2,
                        status: false,
                        is_reserved: false,
                        is_selected: true,
                    },
                ],
                [
                    {
                        id: 21,
                        name: 21,
                        number: 1,
                        status: false,
                        is_reserved: false,
                        is_selected: true,
                    },
                    {
                        id: 22,
                        name: 22,
                        number: 2,
                        status: false,
                        is_reserved: false,
                        is_selected: true,
                    },
                ],
                [
                    {
                        id: 31,
                        name: 31,
                        number: 1,
                        status: false,
                        is_reserved: false,
                        is_selected: true,
                    },
                    {
                        id: 32,
                        name: 32,
                        number: 2,
                        status: false,
                        is_reserved: false,
                        is_selected: true,
                    },
                ],
                [
                    {
                        id: 41,
                        name: 41,
                        number: 1,
                        status: false,
                        is_reserved: false,
                        is_selected: true,
                    },
                    {
                        id: 42,
                        name: 42,
                        number: 1,
                        status: false,
                        is_reserved: false,
                        is_selected: true,
                    },

                ],
            ],
            cols: 4,
            rows: 2,
        }, required: true,
    })
    seat_map: object;

    @IsNotEmpty({message: 'Available seat is require'})
    @IsInt()
    @ApiModelProperty({example: 1, required: true})
    available_seat: number;
}
