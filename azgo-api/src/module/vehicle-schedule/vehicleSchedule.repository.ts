import {Brackets, EntityRepository, Repository} from 'typeorm';
import {VehicleScheduleEntity} from './vehicleSchedule.entity';
import {VehicleEntity} from '../vehicle/vehicle.entity';
import {ProvidersInfoEntity} from '../providers-info/providers-info.entity';
import {FindRouteVehicleDto} from '../book-ticket/dto/find-route-vehicle.dto';
import * as moment from 'moment';
import {RoutesEntity} from '../routes/routes.entity';
import {SeatPatternEntity} from '../seat-pattern/seat-pattern.entity';
import {StatusVehicleSchedule} from '../../enums/status.enum';

@EntityRepository(VehicleScheduleEntity)
export class VehicleScheduleRepository extends Repository<VehicleScheduleEntity> {

    async filterRouteSchedules(body: FindRouteVehicleDto) {
        /*
        * Tìm chuyến theo ngày của customer chọn
        * Và thời gian tìm phải <= start_hour của chuyến
        * */
        try {
            const start_date = body.start_date.split('/');
            // month is 0-based, that's why we need dataParts[1] - 1
            const dateObject = new Date(+start_date[2],  +start_date[1] - 1, +start_date[0]);
            const recheckStartDateAfterToday = moment(dateObject, 'YYYY-MM-DD').isAfter(moment().format('YYYY-MM-DD'));

            // Compare start_date with today
            const plusTimeIntoStartDate = new Date(+start_date[2],  +start_date[1] - 1, +start_date[0]);
            const  now = new Date();
            plusTimeIntoStartDate.setHours(now.getHours());
            plusTimeIntoStartDate.setMinutes(now.getMinutes());
            plusTimeIntoStartDate.setSeconds(now.getSeconds());
            const startDatePlusTimeFormat = moment(plusTimeIntoStartDate).format('YYYY-MM-DD HH:mm:ss').toString();

            // if start_date is not  after today
            if (!recheckStartDateAfterToday) {
                const getTodayTime = new Date().toTimeString().split(':');
                dateObject.setHours(Number(getTodayTime[0]));
                dateObject.setMinutes(Number(getTodayTime[1]));
                dateObject.setSeconds(0);
            }
            const covertDateToString = moment(dateObject).format('YYYY-MM-DD HH:mm:ss').toString();
            const covertDateToStringLike = moment(dateObject).format('YYYY-MM-DD').toString().concat(' 23:59:59');
            let rs = await this.createQueryBuilder('vsche')
                .leftJoin(RoutesEntity, 'route', 'route.id = vsche.route_id')
                .leftJoin(ProvidersInfoEntity, 'provider', 'provider.id = route.provider_id')
                .leftJoin(VehicleEntity, 'vehicle', 'vehicle.id = vsche.vehicle_id')
                .leftJoin(SeatPatternEntity, 'seatpattern', 'seatpattern.id = vehicle.seat_pattern_id')
                .where('route.start_point = :startPoint', { startPoint: body.start_point})
                .andWhere('route.end_point = :endPoint', { endPoint: body.end_point})
                .andWhere('vsche.start_date > :startDate', { startDate: startDatePlusTimeFormat })
                .andWhere('vsche.start_date >= :startDate', { startDate: covertDateToString })
                .andWhere('vsche.start_date <= :endDate', { endDate: covertDateToStringLike})
                .andWhere('vsche.available_seat >= :availableSeat', { availableSeat: body.available_seat  })
                .andWhere('vsche.status = :status', { status: StatusVehicleSchedule.UNSTART }) // 1 => chưa khởi hạnh
                .select([
                    'route.name as name', 'route.distance as distance', 'route.router as router', 'route.base_price as base_price',
                    'route.price_per_km as price_per_km', 'route.duration as duration', 'route.route_code as route_code',
                    'provider.*',
                    'vsche.*',
                    'vehicle.license_plates',
                    'seatpattern.max_seat_of_vertical as max_seat_of_vertical',
                    'seatpattern.max_seat_of_horizontal as max_seat_of_horizontal',
                    'seatpattern.floor as floor']);

            if (body.base_price) {
                rs.andWhere(`route.base_price <= :price`, {price: body.base_price});
            }
            if (body.provider_id) {
                rs.andWhere(`provider.id = :provider`, {provider: body.provider_id});
            }
            return await rs.execute();
        }catch (e) {
            throw {message: e.message, status: e.status};
        }

    }

    async filterSchedules(limit: number, offset: number, options?: any){

        // create new query
        let query = this.createQueryBuilder('az_vehicle_schedules')
            .leftJoinAndSelect('az_vehicle_schedules.vehicle', 'vehicle')
            .leftJoinAndSelect('az_vehicle_schedules.provider', 'provider')
            .leftJoinAndSelect('az_vehicle_schedules.route', 'route')
            .where('az_vehicle_schedules.deleted_at is null');
        if (!options.keyword === false) {
            query.andWhere(new Brackets(qb => {
                qb.where('provider.provider_name like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('vehicle.license_plates like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('route.name like :keyword', { keyword: '%' + options.keyword.trim() + '%' });
            }));
        }
        if (!options.provider_id === false) {
            query.andWhere('az_vehicle_schedules.provider_id = :provider_id', { provider_id: options.provider_id });
        }
        // list
        query.orderBy('az_vehicle_schedules.id', 'DESC')
            .take(limit)
            .skip(offset);

        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }

    // async findOneVehicleSchedule(options?: any){
    //
    //     let query = this.createQueryBuilder('az_vehicle_schedules');
    //     query.where('vehicle_id', options.id );
    //     const result = await query.getOne();
    //     return result;
    // }
}
