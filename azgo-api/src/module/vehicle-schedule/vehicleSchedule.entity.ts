import {Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {ProvidersInfoEntity} from '../providers-info/providers-info.entity';
import {VehicleEntity} from '../vehicle/vehicle.entity';
import {RoutesEntity} from '../routes/routes.entity';
import {TicketEntity} from '../ticket/ticket.entity';
import {DriverProfileEntity} from '../driver-profile/driver-profile.entity';
import {RatingScheduleEntity} from '../rating-schedule/rating-schedule.entity';
import {BookShippingEntity} from '../book-shipping/book-shipping.entity';

@Entity('az_vehicle_schedules')
export class VehicleScheduleEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('int')
    user_id: number;

    @ManyToOne(type => DriverProfileEntity, user => user.user_id)
    @JoinColumn({name: 'user_id', referencedColumnName: 'user_id'})
    user: DriverProfileEntity;

    @Column('int')
    vehicle_id: number;

    @ManyToOne(type => VehicleEntity, vehicle => vehicle.vehicleSchedules)
    @JoinColumn({name: 'vehicle_id', referencedColumnName: 'id'})
    vehicle: VehicleEntity;

    @OneToMany(type => RatingScheduleEntity, rating => rating.scheduleEntity)
    ratingScheduleEntities: [RatingScheduleEntity];

    @Column('int')
    route_id: number;

    @ManyToOne(type => RoutesEntity, route => route.id)
    @JoinColumn({name: 'route_id', referencedColumnName: 'id'})
    route: RoutesEntity;

    @Column('int')
    provider_id: number;

    @ManyToOne(type => ProvidersInfoEntity, provider => provider.vehicleSchedule)
    @JoinColumn({name: 'provider_id', referencedColumnName: 'id'})
    provider: ProvidersInfoEntity;

    @Column({
        type: 'datetime',
    })
    start_date: Date;

    @Column({
        type: 'varchar',
    })
    start_hour: string;

    @Column({
        type: 'varchar',
    })
    end_hour: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    sub_driver: string;

    @Column({
        type: 'json',
        nullable: true,
    })
    seat_map: object;

    @Column({
        type: 'smallint',
        default: 1,
    })
    status: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Column('int')
    available_seat: number;

    @OneToMany(type => TicketEntity, ticket => ticket.vehicleSchedule)
    tickets: TicketEntity[];

    @OneToMany(type => BookShippingEntity, ticket => ticket.vehicleSchedule)
    bookShippings: BookShippingEntity[];
}
