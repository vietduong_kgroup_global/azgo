import {forwardRef, Module} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {VehicleScheduleRepository } from './vehicleSchedule.repository';
import {VehicleScheduleController} from './vehicleSchedule.controller';
import {VehicleScheduleService} from './vehicleSchedule.service';
import {RoutesModule} from '../routes/routes.module';
import {VehicleModule} from '../vehicle/vehicle.module';
import {TicketModule} from '../ticket/ticket.module';
import {UsersModule} from '../users/users.module';
import {VehicleRunningRepository} from '../vehicle-running/vehicleRunning.repository';
import {VanBagacOrdersModule} from '../van-bagac-orders/van-bagac-orders.module';
import {RatingScheduleModule} from '../rating-schedule/rating-schedule.module';
import {BookShippingModule} from '../book-shipping/book-shipping.module';
import {ProvidersInfoModule} from '../providers-info/providers-info.module';
import {CustomChargeModule} from '../custom-charge/custom-charge.module';
import {VehicleMovingModule} from '../vehicle-moving/vehicleMoving.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            VehicleScheduleRepository,
            VehicleRunningRepository,
        ]),
        RoutesModule, VehicleModule,
        UsersModule,
        forwardRef(() => RatingScheduleModule),
        forwardRef(() => BookShippingModule),
        forwardRef(() => TicketModule),
        forwardRef(() => VanBagacOrdersModule),
        ProvidersInfoModule,
        CustomChargeModule,
        forwardRef(() => VehicleMovingModule),
    ],
    providers: [VehicleScheduleService],
    controllers: [VehicleScheduleController],
    exports: [VehicleScheduleService],
})
export class VehicleScheduleModule {}
