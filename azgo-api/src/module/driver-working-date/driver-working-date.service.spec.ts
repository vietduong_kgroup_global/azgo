import { Test, TestingModule } from '@nestjs/testing';
import { DriverWorkingDateService } from './driver-working-date.service';

describe('DriverWorkingDateService', () => {
  let service: DriverWorkingDateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DriverWorkingDateService],
    }).compile();

    service = module.get<DriverWorkingDateService>(DriverWorkingDateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
