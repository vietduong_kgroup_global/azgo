import { Module } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {DriverWorkingDateEntity} from './driver-working-date.entity';
import {DriverWorkingDateService} from './driver-working-date.service';
import {DriverWorkingDateController} from './driver-working-date.controller';
@Module({
    imports: [
        TypeOrmModule.forFeature([DriverWorkingDateEntity]), 
    ],
    exports: [
        DriverWorkingDateService,
    ],
    controllers: [DriverWorkingDateController],
    providers: [DriverWorkingDateService],
})
export class DriverWorkingDateModule {
   
}
