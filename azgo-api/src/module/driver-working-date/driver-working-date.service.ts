import { Injectable } from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {DriverWorkingDateEntity} from './driver-working-date.entity';
import {Repository} from 'typeorm';
import {STATUS_TOUR_ARR} from '../../constants/secrets.const';
import { getManager } from 'typeorm';
import * as moment from 'moment';
import {
    ERROR_UNKNOWN,
    ERROR_TOUR_ORDER_STATUS_NOT_EXISTED
} from '../../constants/error.const';

@Injectable()
export class DriverWorkingDateService {
    constructor(
        @InjectRepository(DriverWorkingDateEntity)
        private readonly driverWorkingDateRepository: Repository<DriverWorkingDateEntity>,
     )
    {}

    async findAll(options?: any){
        let query = await this.driverWorkingDateRepository.createQueryBuilder('driverWorkingDate');
        query.orderBy('driverWorkingDate.createdAt', 'DESC');
        return await query.getMany();
    } 
    async create(body: any) {
        return await this.driverWorkingDateRepository.save(body);
    }

    async updateDriverWorking(body?: any, transactionalEntityManager?: any) {
        try {
            let {new_driverId, old_driverId, order = {}, orderCode = '', startTimeMilisecond} = body;
            if (order.status === STATUS_TOUR_ARR.ACCEPTED_CUSTOMER || 
                order.status === STATUS_TOUR_ARR.ACCEPTED_DRIVER  ||
                order.status === STATUS_TOUR_ARR.ARRIVED_LOCATION_PICKED_UP_CUSTOMER  
            ){ // don hangn duoc chap nhan boi tai xe hoac khach hang
                let start_time_utc = moment(startTimeMilisecond).valueOf();
                const driverWorkingDateEntity = new DriverWorkingDateEntity();
                driverWorkingDateEntity.driver_id = new_driverId;   
                driverWorkingDateEntity.working_date = start_time_utc;
                driverWorkingDateEntity.order_code =  orderCode;
                if (old_driverId && old_driverId !== new_driverId)
                await transactionalEntityManager.delete(DriverWorkingDateEntity, {driver_id: old_driverId, 
                    order_code: orderCode,
                }); 
                if (new_driverId) {
                    let findOne = await transactionalEntityManager.findOne(DriverWorkingDateEntity,  {driver_id: new_driverId, order_code: orderCode}); 
                    if (!findOne) await transactionalEntityManager.save(driverWorkingDateEntity); 
                    else 
                        await transactionalEntityManager.update(
                            DriverWorkingDateEntity,  // entity
                            {driver_id: new_driverId, order_code: orderCode}, // condition update
                            {working_date: driverWorkingDateEntity.working_date}, // params update
                        );
                }
                return {
                    status: true,
                    data: null,
                    error: null,
                    message: null
                };
            }
            else if (order.status === STATUS_TOUR_ARR.CANCELED_CUSTOMER ||
                order.status === STATUS_TOUR_ARR.CANCELED_DRIVER ||
                order.status === STATUS_TOUR_ARR.DONE ||
                order.status === STATUS_TOUR_ARR.VERIFYING
            ) { 

                if (new_driverId) await transactionalEntityManager.delete(DriverWorkingDateEntity, { driver_id: new_driverId });
                if (old_driverId && old_driverId !== new_driverId) 
                    transactionalEntityManager.delete(DriverWorkingDateEntity, {driver_id: old_driverId,
                        order_code: orderCode,
                    });   
                return {
                    status: true,
                    data: null,
                    error: null,
                    message: null
                };

            }
            return {
                status: false,
                data: null,
                error:  ERROR_TOUR_ORDER_STATUS_NOT_EXISTED,
                message: 'Trạng thái đơn hàng không hợp lệ'
            };

        } catch (error) {
            console.log('Error in updateDriverWorking', error);
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message || 'Error in update driver working'
            };
        }
    } 
}
