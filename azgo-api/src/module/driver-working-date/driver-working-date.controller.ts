import {Controller, Get, Post, Put, Delete, HttpStatus, Param, Req, Res, UseGuards, Body} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger'; 
import {AuthGuard} from '@nestjs/passport';
import { Response } from 'express';
import {Roles} from '@azgo-module/core/dist/common/decorators/roles.decorator';
import {ActionKey, FunctionKey} from '@azgo-module/core/dist/common/guards/roles.guards';
import { dataSuccess, dataError} from '@azgo-module/core/dist/utils/jsonFormat'; 
import {DriverWorkingDateService} from './driver-working-date.service'; 
@Controller('driver-working-date')
export class DriverWorkingDateController {
    constructor(
        private readonly driverWorkingDateService: DriverWorkingDateService, 
    ) { }

    @Get('/')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Retrieve many ' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'type', description: 'Type', required: false, type: 'number'})
    async getAll(@Req() request, @Res() res: Response) {
        try {
            const results = await this.driverWorkingDateService.findAll(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', results));
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }
    }
}