import {
    Entity,
    Column,
    ManyToOne,
    JoinColumn,
    PrimaryColumn,
} from 'typeorm';
import { UsersEntity } from '../users/users.entity'; 
@Entity('az_driver_working_date')
export class DriverWorkingDateEntity {
    @PrimaryColumn({
        type: 'int', 
        nullable: false,
    })
    driver_id: number;

    @ManyToOne(type => UsersEntity, user => user.workingDateDriver)
    @JoinColumn({ name: 'driver_id', referencedColumnName: 'id' })
    driverInfo: UsersEntity;

    @PrimaryColumn({
        type: 'varchar', 
        nullable: false,
    })
    order_code: string;

    @PrimaryColumn({
        type: 'bigint', 
        nullable: false,
    })
    working_date: number;
 
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;
     
}
