import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsInt} from 'class-validator';

export class CreateDriverWorkingDateDto {

    @IsNotEmpty({ message: 'id' })
    // @ApiModelProperty({ example: '1: Accept, 2:Cancel' , required: true, default: 1 })
    id: number;

    @IsNotEmpty()
    @IsInt() 
    driver_id: number;

    @IsNotEmpty() 
    order_code: string;

    @IsNotEmpty() 
    working_date: number;
     
    @IsNotEmpty() 
    created_at: Date; 
    
}
