import { Test, TestingModule } from '@nestjs/testing';
import { DriverWorkingDateController } from './driver-working-date.controller';

describe('DriverWorkingDate Controller', () => {
  let controller: DriverWorkingDateController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DriverWorkingDateController],
    }).compile();

    controller = module.get<DriverWorkingDateController>(DriverWorkingDateController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
