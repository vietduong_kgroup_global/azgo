import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsString} from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';
export class SendRequestBookCarOrBikeDto {

    @IsNotEmpty()
    @ApiModelProperty({type: CoordinatesDto  , required: true  })
    start_position: CoordinatesDto;

    @IsNotEmpty()
    @ApiModelProperty({ type: CoordinatesDto , required: true  })
    end_position: CoordinatesDto;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'BIKE or CAR' , required: true  })
    @IsString()
    type: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1 , required: true  })
    service_id: number;

    @IsNotEmpty({ message: 'price' })
    @ApiModelProperty({ example: 15000 , required: true  })
    price: number;

}
