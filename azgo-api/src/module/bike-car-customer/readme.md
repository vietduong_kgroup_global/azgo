================= Flow bike car ====================
#App customer
1. Chọn điểm đi, điểm đến từ mobile có dạng: 
{
    lat: 100001.1,
    long: 100001.1,
    address: 'Cần Thơ',
    short_address: 'tòa nhà VinCom',
} 
2. Chọn dịch vụ (Loại xe):
- Trả về những loại xe đã sẵn sàng chạy OR hoặc trả hết các loại xe (kể cả xe chưa nhấn bắt đầu) ?
- Tính khoản cách (Tích họp gg map dưới BE) * price_per_km = price loại xe
3.Khi click tìm xe: Limit Push notify cho 3 driver (Điều kiện driver đó chưa nhận dc request nào) với các thông tin như: (image_profile, full_name, phone, status, start_point, end_point,
vehicle_type, vehicle_brand, price, rating_customer)
Case 1: Nếu chưa có driver nào accept request => trong vòng 20s sẽ tự động clear request của customer, clear thông báo bên app driver (cần dùng socket.io) .
Case 2: Customer hủy find xe (dk: chưa có driver nào accept) => Tụ động clear request, clear thông báo bên app driver (cần dùng socket.io) 
Case 3: Có 1 trong 3 driver accept => thì tại thời điểm đó 2 driver còn lại sẽ clear thông báo (cần dùng socket.io)
- Đồng thời bên app driver sẽ push notify cho customer với các thông tin của driver (image_profile, full_name, phone, license_plates, vehicle_type, 
vehicle_brand, start_point, end_point, price, position_vehicle, long_time)  
- Tại case này sẽ có thêm các case sau: 
=> Case 1: Nếu customer hủy approve từ driver (Điều kiện customer chưa lên xe) => Xóa request và notify cho driver (socket.io)
=> Case 2: Nếu cusomer nhận thông báo từ driver update status (khách lên xe)  => Customer không được hủy chuyến (Nút hủy sẽ disable hoặc ẩn), 
đồng thời reponse về vị trí xe (Để vẽ cái map xe đang chạy đến điểm đến) 
4. Khi driver click hoàn thành chuyến:
=> App customer sẽ nhận được thông báo và xuất hiện màn hình rating driver (socket)
=> App driver: xuất hiện màn hình rating customer 
