export interface CountCustomerInterface {
    from_date?: string;
    to_date?: string;
    is_in_month?: string;
    driver_id?: number;
}
