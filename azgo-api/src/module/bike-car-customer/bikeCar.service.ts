import { VehicleMovingService } from '../vehicle-moving/vehicleMoving.service';
import {InjectRepository} from '@nestjs/typeorm';
import {forwardRef, Inject, Injectable} from '@nestjs/common';
import {FirebaseService} from '../firebase/firebase.service';
import {RedisService} from '@azgo-module/core/dist/background-utils/Redis/redis.service';
import {RedisUtil} from '@azgo-module/core/dist/utils/redis.util';
import {BikeCarOrderRepository} from '../bike-car-order/bikeCarOrder.repository';
import message from '../../../config/message';
import {UsersService} from '../users/users.service';
import {Brackets} from 'typeorm';
import {RatingDriverService} from '../rating-driver/rating-driver.service';
import {CountCustomerInterface} from './interfaces/count-customer.interface';
import * as moment from 'moment';
import {CountOrderVanBagacInteface} from '../van-bagac-orders/interfaces/count-order-van-bagac.inteface';
import * as _ from 'lodash';
import {BikeCarOrderEntity} from '../bike-car-order/bikeCarOrder.entity';
import {SendRequestBookCarOrBikeDto} from './dto/sendRequestBookCarOrBikeDto';
import {GoogleMapService} from '../google-map/google-map.service';
import {
    KEY_CANCEL_FIND_BOOK_BIKE_CAR_BY_CUSTOMER,
    KEY_CLICK_ACTION,
    KEY_CUSTOMER_CANCEL_APPROVE_BOOK_BIKE_CAR_TO_DRIVER,
    KEY_CUSTOMER_SEND_REQUEST_BOOK_BIKE_CAR_TO_DRIVER,
} from '../../constants/keys.const';
import {VehicleGroup} from '../../enums/group.enum';

@Injectable()
export class BikeCarService {
    private redisService;
    private redis;
    private keyRedisCustomerRequestBookCarBike = 'key_customer_request_book_car_bike';
    private fieldRedisCustomerRequestBookCarBike = 'field_customer_request_book_car_bike';
    // use for vehicle bus
    private keyRedisVehiclePositionBus = 'vehicle_positions_bus';
    private fieldRedisVehiclePositionBus = 'field_vehicle_positions_bus';
    // use for vehicle bike
    private keyRedisVehiclePositionBike = 'vehicle_positions_bike';
    private fieldRedisVehiclePositionBike = 'field_vehicle_positions_bike';
    // use for vehicle car
    private keyRedisVehiclePositionCar = 'vehicle_positions_car';
    private fieldRedisVehiclePositionCar = 'field_vehicle_positions_car';
    // use for vehicle van
    private keyRedisVehiclePositionVan = 'vehicle_positions_van';
    private fieldRedisVehiclePositionVan = 'field_vehicle_positions_van';
    // use for vehicle bagac
    private keyRedisVehiclePositionBagac = 'vehicle_positions_bagac';
    private fieldRedisVehiclePositionBagac = 'field_vehicle_positions_bagac';
    private  connect:any;
    constructor(
        @InjectRepository(BikeCarOrderRepository)
        private readonly bikeCarOrderRepository: BikeCarOrderRepository,
        private readonly vehicleMovingService: VehicleMovingService,
        private readonly firebaseService: FirebaseService,
        private readonly userService: UsersService,
        @Inject(forwardRef(() => RatingDriverService))
        private readonly ratingService: RatingDriverService,
        private readonly googleMapService: GoogleMapService,
    ) {
        this.connect= {
            port: process.env.ENV_REDIS_PORT || 6379,
            host: process.env.ENV_REDIS_HOST || '127.0.0.1', 
            password: process.env.ENV_REDIS_PASSWORD || null,
        };
        console.log(" Redis constructor:",this.connect);
        this.redisService = RedisService.getInstance(this.connect);
        this.redis = RedisUtil.getInstance();
    }

    async sendRequestBookCarOrBike(data: SendRequestBookCarOrBikeDto, user_uuid: string){
        let dataNotiNew;
        const convertPriceToPercentApproved = Math.ceil((data.price * Number(process.env.PERCENT_APPROVED || 10) / 100));
        const book_car_bike_reference = user_uuid + '_' + Math.random().toString(36).slice(2);
        const listBikeCartNearCustomer = await this.vehicleMovingService.findCarOrBikeNearCustomer({
            customer_position: {
                lat: data.start_position.lat,
                lng: data.start_position.lng,
            },
            type_group: data.type,
            service_id: data.service_id,
            convertPriceToPercentApproved,
        });
        if (listBikeCartNearCustomer) {
            // send notification to driver
            const notification = {
                title: message.bikeCarCustomerToDriver.requestBikeCar.title,
                body: message.bikeCarCustomerToDriver.requestBikeCar.body,
            };

            const userEntity = await this.userService.getDetailCustomer(user_uuid);
            if (!userEntity) throw {message: 'Not found customer profile', status: 404};
            const dataNotification = {
                key: KEY_CUSTOMER_SEND_REQUEST_BOOK_BIKE_CAR_TO_DRIVER,
                route_info: data,
                book_car_bike_reference,
                customerInfo: userEntity,
                customer_uuid: userEntity.customer_uuid.toString(),
                type_group: data.type,
                click_action: KEY_CLICK_ACTION,

            };
            // get list device token Driver
            const listDriverDeviceToken = [];
            const list_driver_uuid = [];
            for (const val of listBikeCartNearCustomer) {
                if (val.device_token) {
                    listDriverDeviceToken.push({token: val.device_token, field: book_car_bike_reference, lat: val.lat, lng: val.lng});
                }
                list_driver_uuid.push(val.user_uuid);
            }

            // Set array request order
            const dataObj = {
                driver_accept: false,
                status: 1, // request new
                route_info: data,
                book_car_bike_reference,
                customerInfo: userEntity,
                customer_uuid: userEntity.customer_uuid.toString(),
                type_group: data.type,
                list_driver: list_driver_uuid,
            };

            // Check request and push request
            const getCacheOrder = await this.redis.getInfoRedis(
                this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike);
            if (getCacheOrder.isValidate) {
                const cacheValue = JSON.parse(getCacheOrder.value);
                const checkReuqestBook = _.find(cacheValue, item => {
                    return item.customer_uuid === user_uuid;
                });
                if (checkReuqestBook) throw {message: 'Request existed!', status: 400};

                cacheValue.push(dataObj);
                await this.redis.setInfoRedis(
                    this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike, JSON.stringify(cacheValue));
            } else {
                await this.redis.setInfoRedis(
                    this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike, JSON.stringify([dataObj]));
            }

            // Push notify for driver
            if (listDriverDeviceToken) {
                for (const val of listDriverDeviceToken) {
                    let getCache = await this.redis.getInfoRedis(
                        this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike);
                    if (getCache.isValidate) {
                        const obj = JSON.parse(getCache.value);
                        if (!obj.driver_accept) {
                            // console.log(' Push notification   ', val);
                            // const distance_pickup_point = await this.vehicleMovingService
                            //     .calculateDistance(val.lat, val.long, data.start_position.lat, data.start_position.long, 'K');
                            // const distance_destination = await this.vehicleMovingService
                            //     .calculateDistance(val.lat, val.long, data.end_position.lat, data.end_position.long, 'K');
                            // Get distance from gg map by driving
                            const distance: any = await Promise.all([
                                 this.googleMapService.rawUrlGetDistance({
                                    origins: {lat: val.lat, lng: val.lng},
                                    destinations: {lat: data.start_position.lat, lng: data.start_position.lng},
                                 }),
                                 this.googleMapService.rawUrlGetDistance({
                                    origins: {lat:  data.start_position.lat, lng: data.start_position.lng},
                                    destinations: {lat: data.end_position.lat, lng: data.end_position.lng},
                                 }),
                            ]);
                            const distance_pickup_point = distance[0].data.rows[0].elements[0].distance.text;
                            const distance_destination  = distance[1].data.rows[0].elements[0].distance.text;
                            dataNotiNew = {...dataNotification,
                                ...{distance_pickup_point, distance_destination},
                            };
                            await this.firebaseService.pushNotificationWithTokens([val.token], notification, dataNotiNew);
                        } else {
                            console.log(' Push Cancel   ', val);
                            break;
                        }
                    }
                }
            }
            // Emit event to
            for (const val of listBikeCartNearCustomer) {
                if (val.user_uuid) {
                    this.redisService.triggerEventBikeCar({room: 'user_' + val.user_uuid, message: dataNotiNew});
                }
            }
            return true;
        }
        return false;
    }
    async updateFlagRatingDriver(customer_id: number) {
        const order = await this.bikeCarOrderRepository.findOneOrFail({customer_id});
        order.flag_rating_driver = 2;
        return await this.bikeCarOrderRepository.save(order);
    }

    async updateFlagRatingCustomer(driver_id: number) {
        const order = await this.bikeCarOrderRepository.findOneOrFail({driver_id});
        order.flag_rating_customer = 2;
        return await this.bikeCarOrderRepository.save(order);
    }

    async findOneBikeCarByCustomer(customer_id: number) {
        return await this.bikeCarOrderRepository.createQueryBuilder('order')
            .where('order.flag_rating_driver = 1')
            .andWhere('order.customer_id = :customer_id', {customer_id})
            .getMany();
    }
    async checkRequestOrderBikeCar(customer_uuid: string) {

        const getCacheOrder = await this.redis.getInfoRedis(
            this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike);
        if (getCacheOrder.isValidate) {
            const listRequestOrderBikeCar = JSON.parse(getCacheOrder.value);
            const findCustomerUuid = _.find(listRequestOrderBikeCar, request => {
                return request.customer_uuid === customer_uuid;
            });
            if (!findCustomerUuid) return false;
            // get caching vehcle position by driver uuid
            const getVehicle = await this.vehicleMovingService.getVehiclePostionOnRedisByDriverUuid(
                findCustomerUuid.driver_uuid_accept, findCustomerUuid.type_group);
            let driver_info_accept = null;
            if (getVehicle) {
                driver_info_accept = getVehicle;
            }
            findCustomerUuid.driver_info = driver_info_accept;
            return findCustomerUuid;
        }
        return false;
    }
    async checkApproveOrderBikeCar(driver_uuid: string) {
        // get caching vehcle position by driver uuid
        let getVehicle, findDriverApprove;
        getVehicle = await this.vehicleMovingService.getVehiclePostionOnRedisByDriverUuid(
            driver_uuid, VehicleGroup.BIKE) ||  await this.vehicleMovingService.getVehiclePostionOnRedisByDriverUuid(
            driver_uuid, VehicleGroup.CAR);

        if (!getVehicle) throw {message: 'You need update vehicle position', status: 404};
        const getCacheOrder = await this.redis.getInfoRedis(
            this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike);
        if (getCacheOrder.isValidate) {
            const listRequestOrderBikeCar = JSON.parse(getCacheOrder.value);
            findDriverApprove = _.find(listRequestOrderBikeCar, request => {
                return request.driver_uuid_accept === driver_uuid;
            });
        }
        return { ...findDriverApprove, ...{vehicle: getVehicle}};
    }
    async getListHistoryBookCarOrBike(customer_id: number, options?: any){
        // creuate new query
        let query = this.bikeCarOrderRepository.createQueryBuilder('order')
            .leftJoinAndSelect('order.customerProfile', 'customerProfile')
            .leftJoinAndSelect('order.ratingDriver', 'ratingDriver')
            .leftJoinAndSelect('order.vehicleType', 'services')
            .leftJoinAndSelect('services.vehicles', 'vehicles')
            .leftJoinAndSelect('vehicles.vehicleBrand', 'vehicleBrand')
            .leftJoinAndSelect('vehicles.vehicleType', 'vehicleType');

        let query_limit = !isNaN(Number(options.limit).valueOf()) ? Number(options.limit).valueOf() : 10;
        let page = Number(options.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * query_limit;
        // list
        query.andWhere('order.customer_id = :customer_id', { customer_id })
            .orderBy('order.id', 'DESC')
            .limit(query_limit)
            .offset(offset);
        const results = await query.getManyAndCount();
        // Loop result to add new record
        let dataNew = [];
        await Promise.all(results[0].map( async (item, index) => {
            const driverInfo = await this.userService.getOneDriver(item.driver_id);
            dataNew.push({...item, ... {driverInfo}});
        }));
        return {
            total: results[1],
            data: dataNew.sort((a, b) => Number(b.id) - Number(a.id)),
            current_page: page,
        };
    }
    async  getDetailOrderBookCarOrBike(id: number){
        const rs =  await this.bikeCarOrderRepository.createQueryBuilder('order')
            .leftJoinAndSelect('order.customerProfile', 'customerProfile')
            .leftJoinAndSelect('order.ratingDriver', 'ratingDriver')
            .leftJoinAndSelect('customerProfile.users', 'customerUsers')
            .leftJoinAndSelect('order.driverProfile', 'driverProfile')
            .leftJoinAndSelect('driverProfile.users', 'users')
            .leftJoinAndSelect('users.vehicles', 'vehicles')
            .leftJoinAndSelect('vehicles.vehicleBrand', 'vehicleBrand')
            .leftJoinAndSelect('vehicles.vehicleType', 'vehicleType')
            .where('order.id = :id', {id})
            .getOne();
        const ratingDriver = await this.ratingService.avgRatingDriver(rs.driverProfile.user_id);
        return {...rs, ...{avgRatingDriver: Number(ratingDriver).toFixed(1)}};
    }
    async sleep(number: number) {
        return new Promise(resolve => {
            setTimeout(resolve, number);
        });
    }

    async cancelApproveDriver(book_car_bike_reference, customerInfo: any){
        let listRequestOrderBikeCar, findReference, listVehicle;
        // get request order bike car on redis
        const getCacheOrder = await this.redis.getInfoRedis(
            this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike);
        if (getCacheOrder.isValidate) {
            listRequestOrderBikeCar = JSON.parse(getCacheOrder.value);
            findReference = _.find(listRequestOrderBikeCar, request => {
                return request.book_car_bike_reference === book_car_bike_reference && Number(request.status) <= 2;
            });
        }
        if (!findReference) throw {message: 'Not found request or customer on bike', status: 404};

        // Set receive request for driver not approve
        const redisField = await this.fieldRedisVehiclePosition(findReference.type_group);
        const redisKey = await this.keyRedisVehiclePosition(findReference.type_group);
        const getCacheVehicle = await this.redis.getInfoRedis(redisKey, redisField);
        if (getCacheVehicle.isValidate) {
            listVehicle = JSON.parse(getCacheVehicle.value);
            const findDriverAccept = _.find(listVehicle, vehicle => {
                return vehicle.user_uuid === findReference.driver_uuid_accept;
            });
            if (findDriverAccept) {
                findDriverAccept.receive_request = false;
                // Update vehicle position
                await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listVehicle));
            }
        }

        const userFCM = await this.firebaseService.findOne(findReference.driver_uuid_accept);
        if (userFCM){
            // soft delete and update status bike car order
            await this.bikeCarOrderRepository.createQueryBuilder('order')
                .update(BikeCarOrderEntity)
                .set({
                    status: 5, // huy chuyen
                    deleted_at: new Date(),
                })
                .where('book_car_bike_reference = :book_car_bike_reference', {book_car_bike_reference})
                .andWhere('driver_id = :driver_id', {driver_id: userFCM.user_id})
                .andWhere('customer_id = :customer_id', {customer_id: customerInfo.id})
                .execute();
            // get device token
            let listDriverDeviceToken = [];
            const token = userFCM.token ? userFCM.token : userFCM.apns_id;
            listDriverDeviceToken.push(token);
            // send notification to customer
            const notification = {
                title: 'Từ chối tài xế !',
                body: 'Khách hàng đã từ chối xe của bạn',
            };
            const dataNotification = {
                key: KEY_CUSTOMER_CANCEL_APPROVE_BOOK_BIKE_CAR_TO_DRIVER,
                customer_user_uuid: customerInfo.driver_uuid,
                driver_user_uuid: findReference.driver_uuid_accept,
                route_info: findReference.route_info,
                book_car_bike_reference: findReference.book_car_bike_reference,
                click_action: KEY_CLICK_ACTION,
            };
            // Emit event
            this.redisService.triggerEventBikeCar({room: 'user_' + findReference.driver_uuid_accept, message: dataNotification});
            await this.firebaseService.pushNotificationWithTokens(listDriverDeviceToken, notification,  dataNotification);
            // Remove request on redis
            _.remove(listRequestOrderBikeCar, (request: any)  => {
                return request.book_car_bike_reference === book_car_bike_reference;
            });
            await this.redis.setInfoRedis(
                this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike, JSON.stringify(listRequestOrderBikeCar));
            return true;
        }
        return false;
    }

    async getAllOrderBikeCar(
        limit: number = 0,
        offset: number = 10,
        options?: any,
    ) {

        // let license_plates = options.license_plates ? options.license_plates.trim() : null;
        let query = await this.bikeCarOrderRepository.createQueryBuilder('order')
            .leftJoinAndSelect('order.customerProfile', 'customerProfile')
            .leftJoinAndSelect('customerProfile.users', 'customerUsers')
            .leftJoinAndSelect('order.driverProfile', 'driverProfile')
            .leftJoinAndSelect('driverProfile.users', 'users')
            .leftJoinAndSelect('users.vehicles', 'vehicles')
            .leftJoinAndSelect('vehicles.vehicleBrand', 'vehicleBrand')
            .leftJoinAndSelect('vehicles.vehicleType', 'vehicleType')
            .orderBy('order.id', 'DESC');
        if (!options.keyword === false) {
            query.andWhere(new Brackets(qb => {
                qb.where('order.book_car_bike_reference like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('customerProfile.full_name like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('driverProfile.full_name like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere('vehicles.license_plates like :keyword', { keyword: '%' + options.keyword.trim() + '%' });
            }));
        }
        // limit + offset
        query.take(limit).skip(offset);

        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }

    async countAmountCustsomer(options?: CountCustomerInterface): Promise<string>{
        let query = await this.bikeCarOrderRepository.createQueryBuilder('order')
            .select('COUNT(DISTINCT order.customer_id) as amount_customer');
        if (options && options.is_in_month === 'true') {
            const currentDate = moment(new Date(), 'YYYY-MM').format('YYYY-MM').toString();
            query.andWhere('order.created_at like :created_at', {created_at: '%' + currentDate + '%'});
        }
        if (options && options.from_date) {
            const from_date_tostring = await this.formatDate(options.from_date);
            query.andWhere('order.created_at >= :from_date_tostring', {from_date_tostring});
        }
        if (options && options.to_date) {
            const to_date_tostring = await this.formatDate(options.to_date);
            query.andWhere('order.created_at <= :to_date_tostring', {to_date_tostring});
        }
        if (options && options.driver_id) {
            query.andWhere('order.driver_id = :driver_id', {driver_id: options.driver_id});

        }
        return query.getRawOne();
    }

    async revenueByBikeCar(options: {is_in_month?: string, from_date?: string, to_date?: string, driver_id?: number }) {
        let query = await this.bikeCarOrderRepository.createQueryBuilder('order');
        if (options) {
            if (options.is_in_month === 'true') {
                const currentDate = moment(new Date(), 'YYYY-MM').format('YYYY-MM').toString();
                query.where('order.created_at like :created_at', {created_at: '%' + currentDate + '%'});
            }
            if (options.from_date) {
                const from_date_tostring = await this.formatDate(options.from_date);
                query.andWhere('order.created_at >= :from_date_tostring', {from_date_tostring});
            }
            if (options.to_date) {
                const to_date_tostring = await this.formatDate(options.to_date);
                query.andWhere('order.created_at <= :to_date_tostring', {to_date_tostring});
            }
            if (options.driver_id) {
                query.andWhere('order.driver_id = :driver_id', {driver_id: options.driver_id});
            }
        }

        return  query.select('SUM(order.fee_tax) as total_price').getRawOne();
    }

    async revenuePerVehicleBikeCar( limit: number = 0, offset: number = 10,
                                    options: {is_in_month?: string, from_date?: string, to_date?: string, driver_id?: number }) {
        let query = await this.bikeCarOrderRepository.createQueryBuilder('order')
            .leftJoinAndSelect('order.vehicleBikeCar', 'vehicleBikeCar')
            .leftJoinAndSelect('order.vehicleType', 'vehicleType');

        if (options) {
            if (options.is_in_month === 'true') {
                const currentDate = moment(new Date(), 'YYYY-MM').format('YYYY-MM').toString();
                query.where('order.created_at like :created_at', {created_at: '%' + currentDate + '%'});
            }
            if (options.from_date) {
                const from_date_tostring = await this.formatDate(options.from_date);
                query.andWhere('order.created_at >= :from_date_tostring', {from_date_tostring});
            }
            if (options.to_date) {
                const to_date_tostring = await this.formatDate(options.to_date);
                query.andWhere('order.created_at <= :to_date_tostring', {to_date_tostring});
            }
            if (options.driver_id) {
                query.andWhere('order.driver_id = :driver_id', {driver_id: options.driver_id});
            }
        }
        query.select('DISTINCT(order.vehicle_id), order.vehicle_group_id as vehicle_group_id,' +
            'SUM(order.price) as total_price,' +
            'SUM(order.fee_tax) as total_price_azgo,' +
            ' vehicleBikeCar.license_plates as license_plates,' +
            ' vehicleType.name as vehicle_type_name,' +
            ' order.created_at as created_at')
            .groupBy('order.vehicle_id');
        const count = await query.getRawMany();
        const results = await query.limit(Number(limit)).offset(Number(offset)).getRawMany();
        return {
            success: true,
            data: results,
            total: count.length,
        };
    }
    /*
    * Count order in month with @param is_in_month = true
    * Count order done with @param status = 4
    * */
    async countOrderBikeCar(options?: CountOrderVanBagacInteface): Promise<number> {
        const currentDate = moment(new Date(), 'YYYY-MM').format('YYYY-MM').toString();
        let query = this.bikeCarOrderRepository.createQueryBuilder('order');
        if (options && options.is_in_month === 'true') {
            query.where('order.created_at like :created_at', {created_at: '%' + currentDate + '%'});
        }
        if (options && options.status) {
            query.andWhere('order.status = :status', {status: options.status });
        }

        if (options && options.from_date) {
            const from_date_tostring = await this.formatDate(options.from_date);
            query.andWhere('order.created_at >= :from_date_tostring', {from_date_tostring});
        }
        if (options && options.to_date) {
            const to_date_tostring = await this.formatDate(options.to_date);
            query.andWhere('order.created_at <= :to_date_tostring', {to_date_tostring});
        }
        if (options && options.driver_id) {
            query.andWhere('order.driver_id = :driver_id', {driver_id: options.driver_id});
        }
        return query.getCount();
    }

    async cancelFindDrivers(customerInfo: any) {
        let findBookBusReference, listVehicle;
        const getCacheOrder = await this.redis.getInfoRedis(this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike);
        if (getCacheOrder.isValidate) {
            const getListOrder = JSON.parse(getCacheOrder.value);
            findBookBusReference = _.find(getListOrder, item => {
                return item.customer_uuid === customerInfo.user_id.toString() && item.driver_accept === false;
            });
            if (!findBookBusReference) {
                // reference not found, or have driver approve
                return false;
            }
            // remove order by customer uuid
            _.remove(getListOrder, (item: any) => {
                return item.customer_uuid === customerInfo.user_id.toString();
            });
            await this.redis.setInfoRedis(
                this.keyRedisCustomerRequestBookCarBike, this.fieldRedisCustomerRequestBookCarBike, JSON.stringify(getListOrder));
            const listDriverUUID = findBookBusReference.list_driver;
            if (listDriverUUID) {
                const redisField = await this.fieldRedisVehiclePosition(findBookBusReference.type_group);
                const redisKey = await this.keyRedisVehiclePosition(findBookBusReference.type_group);
                const getCacheVehicle = await this.redis.getInfoRedis(redisKey, redisField);
                if (getCacheVehicle.isValidate) {
                    listVehicle = JSON.parse(getCacheVehicle.value);
                }
                await Promise.all(listDriverUUID.map(async (user_uuid) => {
                    // Set receive_request = flase
                    listVehicle.map((vehicle) => {
                        if (vehicle.user_uuid === user_uuid) {
                            vehicle.receive_request = false;
                        }
                    });
                    const userFCM = await this.firebaseService.findOne(user_uuid);
                    //  Push notify fo driver
                    if (userFCM) {
                        let listDriverDeviceToken = [];
                        const token = userFCM.token ? userFCM.token : userFCM.apns_id;
                        listDriverDeviceToken.push(token);
                        // send notification to driver
                        const notification = {
                            title: 'Khách hàng hủy tìm xe',
                            body: 'Khách hàng đã hủy tìm xe',
                        };
                        const dataNotification = {
                            key: KEY_CANCEL_FIND_BOOK_BIKE_CAR_BY_CUSTOMER,
                            customerInfo: {
                                customer_uuid: findBookBusReference.customer_uuid.toString(),
                                phone: findBookBusReference.customerInfo.phone,
                            },
                            type_group: VehicleGroup.CAR,
                            click_action: KEY_CLICK_ACTION,
                        };
                        this.redisService.triggerEventBikeCar({room: 'user_' + user_uuid, message: dataNotification});
                        await this.firebaseService.pushNotificationWithTokens(listDriverDeviceToken, notification,  dataNotification);
                    }
                }));
                // Update vehicle position
                await this.redis.setInfoRedis(redisKey, redisField, JSON.stringify(listVehicle));
            }

            return true;
        }
        return  false;
    }

    /*
    * Input format: DD/MM/YYYY
    *
    * */
    formatDate(date: string) {
        const dateSplit = date.split('/');
        // month is 0-based, that's why we need dataParts[1] - 1
        const dateObject = new Date(+dateSplit[2],  +dateSplit[1] - 1, +dateSplit[0]);
        const formatDate = moment(dateObject, 'YYYY-MM-DD');
        return formatDate.format('YYYY-MM-DD').toString();
    }

    async fieldRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.fieldRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.fieldRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.fieldRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.fieldRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.fieldRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }

    async keyRedisVehiclePosition(type_group: string) {
        let redisField = '';
        switch (type_group) {
            case 'BUS':
                redisField = this.keyRedisVehiclePositionBus;
                break;
            case 'BIKE':
                redisField = this.keyRedisVehiclePositionBike;
                break;
            case 'CAR':
                redisField = this.keyRedisVehiclePositionCar;
                break;
            case 'VAN':
                redisField = this.keyRedisVehiclePositionVan;
                break;
            case 'BAGAC':
                redisField = this.keyRedisVehiclePositionBagac;
                break;
        }
        return redisField;
    }
}
