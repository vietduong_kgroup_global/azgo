import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BikeCarController } from './bikeCar.controller';
import { BikeCarService } from './bikeCar.service';
import { CommonService } from '../../helpers/common.service';
import { BikeCarOrderRepository } from '../bike-car-order/bikeCarOrder.repository';
import { VehicleMovingModule } from '../vehicle-moving/vehicleMoving.module';
import { FirebaseModule } from '../firebase/firebase.module';
import { ProvidersInfoModule } from '../providers-info/providers-info.module';
import { RoutesModule } from '../routes/routes.module';
import { TicketModule } from '../ticket/ticket.module';
import { UsersRepository } from '../users/users.repository';
import { UsersModule } from '../users/users.module';
import { RatingDriverModule } from '../rating-driver/rating-driver.module';
import { CustomChargeModule } from '../custom-charge/custom-charge.module';
import {GoogleMapService} from '../google-map/google-map.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            BikeCarOrderRepository,
            UsersRepository,
        ]),
        VehicleMovingModule,
        CommonService,
        ProvidersInfoModule,
        RoutesModule,
        TicketModule,
        BikeCarModule,
        FirebaseModule,
        UsersModule,
        RatingDriverModule,
        CustomChargeModule,
    ],
    providers: [BikeCarService, GoogleMapService],
    controllers: [BikeCarController],
})
export class BikeCarModule {

}
