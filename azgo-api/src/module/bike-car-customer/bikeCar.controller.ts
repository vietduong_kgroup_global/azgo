import { Body, Controller, HttpStatus, Param, Post, Req, Res, UseGuards, Get, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { SendRequestBookCarOrBikeDto } from './dto/sendRequestBookCarOrBikeDto';
import { BikeCarService } from './bikeCar.service';
import * as jwt from 'jsonwebtoken';
import { VehicleMovingService } from '../vehicle-moving/vehicleMoving.service';
import { UsersService } from '../users/users.service';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { CustomChargeService } from '../custom-charge/custom-charge.service';
import { VehicleGroup } from '@azgo-module/core/dist/utils/enum.ultis';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@ApiUseTags('Bike Car Customer')
@Controller('bike-car-customer')
export class BikeCarController {
    constructor(
        private readonly bikeCarService: BikeCarService,
        private readonly vehicleMovingService: VehicleMovingService,
        private userService: UsersService,
        private readonly customChageService: CustomChargeService,
    ) {
    }
    /**
     * Get All vehicle by provider and vehicle type
     * Return number
     * @param res
     * @param request
     */
    @Get('/get-list-ticket-by-bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all ticket by bike car' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'keyword', description: 'search full name driver, customer, book car bike reference', required: false, type: 'string'})
    async getAllBikeCars(@Res() res: Response, @Req() request) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        try {
            results = await this.bikeCarService.getAllOrderBikeCar(per_page, offset, request.query);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    @Post('/send-request-book-bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Send request book to all Car or Bike' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async sendRequestBookCarOrBike(@Req() request, @Body() body: SendRequestBookCarOrBikeDto , @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const customerInfo: any =  jwt.decode(token);

            const result = await this.bikeCarService.sendRequestBookCarOrBike(body, customerInfo.user_id.toString());
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Not found vehicle or existed request', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/check-status-customer-rating-driver-bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Check status customer rating driver bike car' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async checkStatusCustomerRatingDriverBikeCar(@Req() request, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const customerInfo: any =  jwt.decode(token);
            const result = await this.bikeCarService.findOneBikeCarByCustomer(customerInfo.id);
            if (!result)
                return res.status(HttpStatus.NOT_FOUND).send(dataSuccess('Not found', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/check-status-request-order-bike-car-by-customer')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Check status request order bike/car by customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async checkRequestOrderBikeCar(@Req() request, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const customerInfo: any =  jwt.decode(token);
            const result = await this.bikeCarService.checkRequestOrderBikeCar(customerInfo.user_id.toString());
            if (!result)
                return res.status(HttpStatus.NOT_FOUND).send(dataSuccess('Not found request', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/check-status-approve-order-bike-car-by-driver')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.DRIVER_BIKE_CAR]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Check status approve order bike/car by driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async checkApproveOrderBikeCar(@Req() request, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const driver_info: any =  jwt.decode(token);
            const result = await this.bikeCarService.checkApproveOrderBikeCar(driver_info.user_id.toString());
            if (!result)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found approve', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/history-order-book-bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get history book Car Or Bike of customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'limit', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    async getOrderBookCarOrBikeOrCustomer(@Req() request, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const customerInfo: any =  jwt.decode(token);
            const result = await this.bikeCarService.getListHistoryBookCarOrBike(customerInfo.id, request.query);
            if (!result)
                 return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/detail-order-book-bike-car/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get Detail book Car Or Bike of customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'id', description: 'id ', required: true, type: 'string'})
    async getDetailOrderBookCarOrBikeOrCustomer(@Param('id') id , @Req() request, @Res() res: Response) {
        try {
            const result = await this.bikeCarService.getDetailOrderBookCarOrBike(id);
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Bad requests', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/cancel-approve-driver-by-customer/:book_car_bike_reference')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Cancel approve driver bike/car by customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({name: 'book_car_bike_reference', description: 'book_car_bike_reference', required: true, type: String})
    async cancelApproveDriver(@Req() request, @Param('book_car_bike_reference') book_car_bike_reference: string, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const customerInfo =  jwt.decode(token);
            const findUserByUuid = await this.userService.findOneByUserInfo(customerInfo);
            const result = await this.bikeCarService.cancelApproveDriver(book_car_bike_reference, findUserByUuid);
            if (result)
                return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/count-amount-customer-bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Count amount customer bike car' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'from_date', description: '31/3/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'to_date', description: '31/4/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'is_in_month', description: 'Get order in month', required: false, type: Boolean})
    @ApiImplicitQuery({name: 'driver_id', description: 'Driver id', required: false, type: Number})
    async countCustomer(@Req() request, @Res() res: Response) {
        try {
            const result = await this.bikeCarService.countAmountCustsomer(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('revenue-bike-car')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Revenue bike/car by admin' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'from_date', description: '31/3/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'to_date', description: '31/4/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'is_in_month', description: 'Get order in month', required: false, type: Boolean})
    @ApiImplicitQuery({name: 'driver_id', description: 'Driver id', required: false, type: Number})
    async revenueInMonthByBikeCar(@Req() request, @Res() res: Response) {
        try {
            const result = await this.bikeCarService.revenueByBikeCar(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result.total_price));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('revenue-per-vehicle')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Revenue bike/car by vehicle' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'from_date', description: '31/3/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'to_date', description: '31/4/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'is_in_month', description: 'Get order in month', required: false, type: Boolean})
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: Number})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: Number})
    @ApiImplicitQuery({name: 'driver_id', description: 'Driver id', required: false, type: Number})
    async revenuePerVehicle(@Req() request, @Res() res: Response) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        try {
            results = await this.bikeCarService.revenuePerVehicleBikeCar(per_page, offset, request.query);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    @Get('/count-order-bike-car-by-admin')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Count order bike/car by admin' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({name: 'is_in_month', description: 'Get order in month', required: false, type: Boolean})
    @ApiImplicitQuery({name: 'status', description: 'Get order by status', required: false, type: Number})
    @ApiImplicitQuery({name: 'from_date', description: '31/3/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'to_date', description: '31/4/2020', required: false, type: String})
    @ApiImplicitQuery({name: 'driver_id', description: 'Driver id', required: false, type: Number})
    async countOrderVanBagac(@Req() request, @Res() res: Response) {
        try {
            const result = await this.bikeCarService.countOrderBikeCar(request.query);
            return res.status(HttpStatus.OK).send(dataSuccess('OK', result));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/cancel-find-driver-bike-car-by-customer')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Cancel find driver bike car by customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async cancelFindDriver(@Req() request, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            // decode token
            const loginInfo = jwt.decode(token);
            const customerInfo = await this.userService.findOneByUserInfo(loginInfo);
            const result = await this.bikeCarService.cancelFindDrivers(customerInfo);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('OK', null));
            }
            return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found request', null));
        }
        catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
