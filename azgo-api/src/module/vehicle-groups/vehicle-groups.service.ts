import { Injectable } from '@nestjs/common';
import {TypeOrmCrudServiceExtend} from '../type-orm-crud-extend.service';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {VehicleGroupsEntity} from './vehicle-groups.entity';

@Injectable()
export class VehicleGroupsService extends TypeOrmCrudServiceExtend<VehicleGroupsEntity>{
    constructor(
        @InjectRepository(VehicleGroupsEntity)
        private readonly vehicleGroupRepository: Repository<VehicleGroupsEntity>)
    {
        super(vehicleGroupRepository);
    }
}
