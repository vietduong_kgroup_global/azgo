import { Module } from '@nestjs/common';
import { VehicleGroupsController } from './vehicle-groups.controller';
import { VehicleGroupsService } from './vehicle-groups.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {VehicleGroupsEntity} from './vehicle-groups.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([VehicleGroupsEntity]),
  ],
  controllers: [VehicleGroupsController],
  providers: [VehicleGroupsService],
  exports: [VehicleGroupsService],
})
export class VehicleGroupsModule {}
