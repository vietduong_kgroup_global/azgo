import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty, IsInt, IsString, MaxLength, Validate} from 'class-validator';
import {PromotionEntityExists} from '../../validator-module/entity-promotions-exists.constraint';

export class CreateVehicleGroupDto {
    @Validate(PromotionEntityExists, { message: 'Promotion doesn`t exits.' })
    @IsNotEmpty({message: 'Promotion is required'})
    @IsInt({message: 'Promotion must be a integer'})
    @ApiModelProperty({example: 1, required: true})
    promotion_id: number;

    @IsNotEmpty()
    @ApiModelProperty({example: 'Xe 69 chỗ', required: true})
    @IsString()
    @MaxLength(255)
    name: string;
}
