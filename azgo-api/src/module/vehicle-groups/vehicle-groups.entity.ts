import {Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {IsInt, IsNotEmpty, MaxLength} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';
import {PromotionsEntity} from '../promotions/promotions.entity';
import {VehicleTypesEntity} from '../vehicle-types/vehicle-types.entity';
import {CustomChargeEntity} from '../custom-charge/custom-charge.entity';

@Entity('az_vehicle_groups')
export class VehicleGroupsEntity {
    @Exclude()
    @PrimaryGeneratedColumn({ type: 'bigint', unsigned: true })
    id: number;

    @Column('int')
    @IsInt()
    @ApiModelProperty({example: 1, required: true})
    promotion_id: number;

    @Column('varchar')
    @MaxLength(255)
    @IsNotEmpty()
    @ApiModelProperty({example: 'Xe 69 chỗ', required: true})
    name: string;

    @ManyToOne(type => PromotionsEntity, promotion => promotion.vehicleGroups)
    @JoinColumn({name: 'promotion_id', referencedColumnName: 'id'})
    @ApiModelProperty({example: 'promotion'})
    promotion: PromotionsEntity;

    @OneToMany(type => VehicleTypesEntity, vehicle_types => vehicle_types.vehicleGroup)
    vehicleTypes: VehicleTypesEntity[];

    @OneToOne(type => CustomChargeEntity, custom_charge => custom_charge.vehicleGroupEntity)
    customChargeEntity: CustomChargeEntity;
}
