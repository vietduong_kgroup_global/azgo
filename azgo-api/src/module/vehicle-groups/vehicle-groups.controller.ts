import {Controller, UseGuards, UsePipes} from '@nestjs/common';
import {CreateManyDto, Crud, CrudController, CrudRequest, Override, ParsedBody, ParsedRequest} from '@nestjsx/crud';
import {ApiBearerAuth, ApiImplicitQuery, ApiUseTags} from '@nestjs/swagger';
import {GetListDefaultResponse} from '../get-list-default-response.inteface';
import {GetManyDefaultResponse} from '@nestjsx/crud/lib/interfaces';
import {Roles} from '@azgo-module/core/dist/common/decorators/roles.decorator';
import {FunctionKey, ActionKey} from '@azgo-module/core/dist/common/guards/roles.guards';
import {AuthGuard} from '@nestjs/passport';
import {VehicleGroupsEntity} from './vehicle-groups.entity';
import {VehicleGroupsService} from './vehicle-groups.service';
import {CreateVehicleGroupDto} from './dto/create-vehicle-group.dto';
import {CustomValidationPipe} from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@Crud({
    model: {
        type: VehicleGroupsEntity,
    },
    query: {
        join: {
            promotion: {},
        },

    },
})
@ApiUseTags('Vehicle groups')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('vehicle-groups')
export class VehicleGroupsController implements CrudController<VehicleGroupsEntity>{
    constructor(public service: VehicleGroupsService) {

    }

    get base(): CrudController<VehicleGroupsEntity> {
        return this;
    }

    @Override()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    async getMany(
        @ParsedRequest() req: CrudRequest,
    ): Promise<GetListDefaultResponse<VehicleGroupsEntity>> {
        let rs = await this.base.getManyBase(req);
        const data: GetListDefaultResponse<VehicleGroupsEntity> = {
            status: true,
            message: 'string',
            data: {
                count: null,
                results: null,
                current_page: null,
                next_page: null,
                total_page: null,
            },
        };
        /* B/c rs promise about Object or Array*/
        if (rs instanceof Array) {
            data.data = rs ;
        }else {
            const dataObj = rs as GetManyDefaultResponse<VehicleGroupsEntity>;
            data.data = {
                count: dataObj.count,
                results: dataObj.data,
                current_page: dataObj.page,
                next_page: dataObj.page + 1 ,
                total_page: dataObj.total,
            };
        }

        return data;
    }

    @Override('getOneBase')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    getOne(
        @ParsedRequest() req: CrudRequest,
    ) {
        return this.base.getOneBase(req);
    }

    /**
     * Create one
     * @param req
     * @param dto
     */
    @Override()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UsePipes(CustomValidationPipe)
    createOne(
        @ParsedRequest() req: CrudRequest,
        @ParsedBody() dto: CreateVehicleGroupDto,
    ) {
        const entity = this.service.getEntityFromDTO(dto);
        return this.service.createOne(req, entity);
    }

    @Override()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UsePipes(CustomValidationPipe)
    createMany(
        @ParsedRequest() req: CrudRequest,
        @ParsedBody() dto: CreateManyDto<VehicleGroupsEntity>) {
        return this.base.createManyBase(req, dto);
    }

    @Override('updateOneBase')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UsePipes(CustomValidationPipe)
    updateOneBase(
        @ParsedRequest() req: CrudRequest,
        @ParsedBody() dto: VehicleGroupsEntity,
    ) {
        // const entity = this.service.getEntityFromDTO(dto);
        return this.base.updateOneBase(req, dto);
    }

    /**
     * Update with method put one
     * @param req
     * @param dto
     */
    @Override('replaceOneBase')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UsePipes(CustomValidationPipe)
    updatePut(
        @ParsedRequest() req: CrudRequest,
        @ParsedBody() dto: VehicleGroupsEntity,
    ) {
        return this.base.replaceOneBase(req, dto);
    }

    @Override()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    async deleteOne(
        @ParsedRequest() req: CrudRequest,
    ) {
        return this.base.deleteOneBase(req);
    }
}
