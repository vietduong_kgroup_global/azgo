import {Injectable} from '@nestjs/common';
import {FindRouteVehicleDto} from './dto/find-route-vehicle.dto';
import {VehicleScheduleService} from '../vehicle-schedule/vehicleSchedule.service';

@Injectable()
export class BookTicketService {
    constructor(
        private readonly vehicleScheduleService: VehicleScheduleService,
    ) {
    }

    async filterRouteSchedules(data: FindRouteVehicleDto) {
        const rs = await this.vehicleScheduleService.filterRouteSchedules(data);
        if (rs) {
            const zero = 0;
            await Promise.all(rs.map(async (value) => {
                const avg_rating_provider = await this.vehicleScheduleService.getAllScheduleByProvider(value.provider_id, 3);
                value.avg_rating_provider = avg_rating_provider ? Number(avg_rating_provider.avg).toFixed(1) : zero.toFixed(1);
            }));
            return rs;
        }
        return null;
    }
}
