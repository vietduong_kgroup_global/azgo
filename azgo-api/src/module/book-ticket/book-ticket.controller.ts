import { Controller, Get, HttpStatus, Query, Res, UseGuards, UsePipes } from '@nestjs/common';
import { BookTicketService } from './book-ticket.service';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { FindRouteVehicleDto } from './dto/find-route-vehicle.dto';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import {
    ApiBearerAuth,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@ApiBearerAuth()
@ApiUseTags('Book ticket')
@Controller('book-ticket')
export class BookTicketController {
    constructor(
        private readonly bookTicketService: BookTicketService,
    ) {
    }

    @Get()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.CUSTOMER]})
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(CustomValidationPipe)
    @ApiOperation({ title: 'Looking for route vehicle' })
    @ApiImplicitQuery({ name: 'start_point', description: 'ID province', required: true, type: 'number' })
    @ApiImplicitQuery({ name: 'end_point', description: 'ID province', required: true, type: 'number' })
    @ApiImplicitQuery({ name: 'available_seat', description: 'Available seat', required: true, type: 'number' })
    @ApiImplicitQuery({ name: 'start_date', description: '20/2/2020', required: true, type: 'string' })
    @ApiImplicitQuery({ name: 'luggage_info', description: 'Luggage information', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'base_price', description: 'Base price', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'provider_id', description: 'Provider id', required: false, type: 'number' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async filterRouteSchedules(@Query() query: FindRouteVehicleDto, @Res() res: Response) {
        try {
            const result = await this.bookTicketService.filterRouteSchedules(query);
            if (!result)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Not found vehicle schedule! ', null));
            return res.status(HttpStatus.OK).send(dataSuccess('Ok', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

}
