import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class UpdateSeatMapDto {
    @IsNotEmpty({message: 'Seat map is require'})
    @ApiModelProperty({
        example: {
            f1: [
                [
                    {
                        id: 11,
                        name: 11,
                        number: 1,
                        status: false,
                        is_reserved: false,
                        is_selected: true,
                    },
                    {
                        id: 12,
                        name: 12,
                        number: 2,
                        status: false,
                        is_reserved: false,
                        is_selected: true,
                    },
                    {
                        id: 13,
                        name: 13,
                        number: 3,
                        status: false,
                        is_reserved: false,
                        is_selected: false,
                    },
                    {
                        id: 14,
                        name: 14,
                        number: 4,
                        status: false,
                        is_reserved: false,
                        is_selected: false,
                    },
                ],
                [
                    {
                        id: 11,
                        name: 11,
                        number: 1,
                        status: false,
                        is_reserved: false,
                        is_selected: true,
                    },
                    {
                        id: 12,
                        name: 12,
                        number: 2,
                        status: false,
                        is_reserved: false,
                        is_selected: true,
                    },
                    {
                        id: 13,
                        name: 13,
                        number: 3,
                        status: false,
                        is_reserved: false,
                        is_selected: false,
                    },
                    {
                        id: 14,
                        name: 14,
                        number: 4,
                        status: false,
                        is_reserved: false,
                        is_selected: false,
                    },
                ],
                [
                    {
                        id: 11,
                        name: 11,
                        number: 1,
                        status: false,
                        is_reserved: false,
                        is_selected: true,
                    },
                    {
                        id: 12,
                        name: 12,
                        number: 2,
                        status: false,
                        is_reserved: false,
                        is_selected: true,
                    },
                    {
                        id: 13,
                        name: 13,
                        number: 3,
                        status: false,
                        is_reserved: false,
                        is_selected: false,
                    },
                    {
                        id: 14,
                        name: 14,
                        number: 4,
                        status: false,
                        is_reserved: false,
                        is_selected: false,
                    },
                ],
            ],
            f2: [],
            f3: [],
            cols: 4,
            rows: 5,
        },
        required: true,
    })
    seat_map: object;
}
