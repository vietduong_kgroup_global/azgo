import {IsBoolean, IsIn, IsInt, IsNotEmpty, IsOptional, IsString, MaxLength} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';

export class SeatMapDto  {

    @IsNotEmpty({message: 'Id is require'})
    @ApiModelProperty({example: 1, required: true})
    @IsInt()
    id: number;

    @IsNotEmpty({message: 'Name is require'})
    @IsString({message: 'Name must be string'})
    @MaxLength(255, {message: 'Name không quá 255 ký tự'})
    @ApiModelProperty({example: 'A1', required: true})
    name: string;

    @IsNotEmpty({message: 'Position is require'})
    @ApiModelProperty({example: 1, required: true})
    @IsInt()
    position: number;

    @IsNotEmpty({message: 'Start date seat is require'})
    @ApiModelProperty({enum: ['NORMAL', 'SUB'], required: true})
    @IsOptional()
    @IsIn(['NORMAL', 'SUB'])
    type: string;

    @IsNotEmpty({message: 'Status is require'})
    @ApiModelProperty({example: true, required: true, default: false})
    @IsBoolean()
    status: boolean;

}
