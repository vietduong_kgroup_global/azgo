import { ApiModelProperty } from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class FindRouteVehicleDto {

    @IsNotEmpty()
    @ApiModelProperty({example: 1, description: 'Area id', required: true, type: Number})
    start_point: number;

    @IsNotEmpty()
    @ApiModelProperty({example: 2, description: 'Area id', type: Number, required: true})
    end_point: number;

    @IsNotEmpty()
    @ApiModelProperty({example: 20, description: 'Số ghế muốn đặt', type: Number, required: true})
    available_seat: number;

    @IsNotEmpty()
    @ApiModelProperty({example: '20/2/2020', description: 'Ngày muốn đặt vé, format theo DD/MM/YYYY', required: true})
    start_date: string;

    @ApiModelProperty({example: 'Balo', description: 'Thông tin hành lý', type: Number, required: false})
    luggage_info: string;

    @ApiModelProperty({example: 10000, description: 'Price muốn filter', type: Number, required: false})
    base_price: number;

    @ApiModelProperty({example: 1, description: 'Provider muốn filter', type: Number, required: false})
    provider_id: number;
}
