import { Module } from '@nestjs/common';
import { BookTicketController } from './book-ticket.controller';
import { BookTicketService } from './book-ticket.service';
import { VehicleScheduleModule } from '../vehicle-schedule/vehicleSchedule.module';

@Module({
  imports: [
    VehicleScheduleModule,
  ],
  controllers: [BookTicketController],
  providers: [BookTicketService],
})
export class BookTicketModule {}
