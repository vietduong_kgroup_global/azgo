import { EntityRepository, Repository, Brackets } from 'typeorm';
import {WalletEntity} from './wallet.entity';

@EntityRepository(WalletEntity)
export class WalletRepository extends Repository<WalletEntity> {
    /**
     * This function get list wallet by limit & offset
     * @param limit
     * @param offset
     * @param options
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        let query = this.createQueryBuilder('az_wallet');
        query.leftJoinAndSelect('az_wallet.usersEntity', 'usersEntity');
        query.andWhere('usersEntity.deleted_at is null');
        query.leftJoinAndSelect('usersEntity.driverProfile', 'driverProfile'); 
        query.andWhere('az_wallet.deleted_at is null');

        // search by keyword
        if (options.keyword && options.keyword.trim()) {
            options.keyword = options.keyword.trim();
            if (options.keyword.startsWith('0')) 
                options.keyword = options.keyword.substring(1, options.keyword.length);
            else if (options.keyword.startsWith('+84') || options.keyword.startsWith('84'))  
                options.keyword = options.keyword.substring(3, options.keyword.length);
            query.andWhere(new Brackets(qb => {
                qb.where(`usersEntity.phone like :keyword`, { keyword: '%' + options.keyword.trim() + '%' });
            }));

        }
        // filter by area_uuid in token
        if (options.area_uuid) {
            query.andWhere(`driverProfile.area_uuid = :area_uuid`, { area_uuid: options.area_uuid });
        }
        // limit + offset
        query.orderBy('az_wallet.id', 'DESC')
            .limit(limit)
            .offset(offset);

        const results = await query.getManyAndCount();
        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }
    /**
     * This function get find by driver id
     * @param tour_order_uuid
     */
    async findByDriverId(driver_id: any): Promise<WalletEntity> {
        return await this.findOne({
            where: { driver_id },
            relations: [
                'usersEntity',
                'usersEntity.driverProfile',
            ]
        });
    }
}
