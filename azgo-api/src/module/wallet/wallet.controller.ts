import {Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Req, Res, UseGuards} from '@nestjs/common';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { Response } from 'express';
import {ApiBearerAuth, ApiImplicitParam, ApiImplicitQuery, ApiOperation, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import {WalletService} from './wallet.service';
import { UsersService } from '../users/users.service';
import { TourOrderService } from '../tour-order/tour-order.service';
import {AuthGuard} from '@nestjs/passport';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import * as jwt from 'jsonwebtoken';
import {ConvertVnPayToFeeWallet} from './dto/convert-vnpay-to-wallet.dto';
import {GetUrlPaymentDto} from './dto/get-url-payment.dto';
import {ConvertCashWalletToFeeWalletDto} from './dto/convert-cash-wallet-to-fee-wallet.dto';
import { UserInfoDto } from './dto/user-info.dto';
import { ERROR_USER_EXITS_IN_SYSTEM, ERROR_WALLET_ORDER_CODE_IS_NOT_EXIST, ERROR_UNKNOWN } from '../../constants/error.const';

@Controller('wallet')
@ApiUseTags('Wallet')
export class WalletController {
    constructor(
        private readonly walletService: WalletService,
        private readonly usersService: UsersService,
        private readonly tourOrderService: TourOrderService,
    ) {
    }

    @Get()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all wallet' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword search by phone', required: false, type: 'string' })
    async getAll(@Req() request, @Res() res: Response) {
        try {
            let results;
            const token = request.headers.authorization.split(' ')[1];
            const userInfo: any = jwt.decode(token);
            // ===============Query offset + per_page=============
            let per_page = Number(request.query.per_page).valueOf();
            if (isNaN(per_page)) {
                per_page = 10;
            }
    
            let page = Number(request.query.page).valueOf();
            if (isNaN(page)) {
                page = 1;
            }
            const offset = (page - 1) * per_page;
    
            try {
                request.query.area_uuid = userInfo.area_uuid || '';
                results = await this.walletService.getAllByOffsetLimit(per_page, offset, request.query);
            } catch (e) {
                return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
            }
    
            let options = {
                current_page: page,
                next_page: page,
                total_page: Math.ceil(results.total / per_page),
            };
            options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;
    
            if (options.total_page > 0 && options.next_page > options.total_page) {
                return res.status(HttpStatus.BAD_REQUEST).
                    send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
            }
            return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
            // res.status(HttpStatus.OK).send(query);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('url-payment-vn-pay-to-fee-wallet-by-driver')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get url payment vn pay to fee wallet by driver' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    async getUrlVnPay(@Body() body: GetUrlPaymentDto, @Req() request, @Res() res: Response) {
        try {
            /* Get IP address */
            const vnp_IpAddr = request.headers['x-forwarded-for'] ||
                request.connection.remoteAddress ||
                request.socket.remoteAddress ||
                request.connection.socket.remoteAddress;

            const token = request.headers.authorization.split(' ')[1];
            const driverInfo: any = jwt.decode(token);
            const concatBody = {...body, ... {vnp_IpAddr}, ...{driverInfo}};
            const result = await this.walletService.getUrlPaymentVnpay(concatBody);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('convert-cash-wallet-to-fee-wallet-by-driver')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Convert cash wallet to fee wallet by driver' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    async convertCashToFeeWallet(@Body() body: ConvertCashWalletToFeeWalletDto, @Req() request, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const driverInfo: any = jwt.decode(token);
            const result = await this.walletService.convertCashToFeeWallet(driverInfo, body);
            if (result && result.status) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result.data));;
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Chuyển tiền bị lỗi', null, result.error || ERROR_UNKNOWN));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('request-withdraw-money-from-cash-wallet-by-driver')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Request withdraw money from cash wallet by driver' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    async withdrawMoneyFromCashWalletByDriver(@Body() body: ConvertCashWalletToFeeWalletDto, @Req() request, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const driverInfo: any = jwt.decode(token);
            const result = await this.walletService.withdrawMoneyFromCashWalletByDriver(driverInfo, body);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Convert vn pay to fee wallet by driver (dùng tạm, nữa chuyển qa IPN)' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    async update(@Body() body: ConvertVnPayToFeeWallet, @Req() request, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const driverInfo: any = jwt.decode(token);

            const result = await this.walletService.updateByDriverId(driverInfo.id, driverInfo.id, body);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('get-one-wallet-by-driver')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get one wallet by driver' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    async getOne(@Req() request, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const driverInfo: any = jwt.decode(token);

            const result = await this.walletService.getOneByDriverId(driverInfo.id);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Get('/:driver_uuid')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get one wallet by driver' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    @ApiImplicitParam({ name: 'driver_uuid', description: 'User uuid of driver', required: true, type: 'string' })
    async getOneByDriverUuid(@Param('driver_uuid') driver_uuid, @Req() request, @Res() res: Response) {
        try {
            const user = await this.usersService.findOne(driver_uuid);
            if(!user) return res.status(HttpStatus.BAD_REQUEST).send(dataError('Không tìm thấy tài xế', null));
            const result = await this.walletService.findByDriverId(user.id);
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:driver_uuid')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Convert vn pay to fee wallet by driver (dùng tạm, nữa chuyển qa IPN)' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    async updateByDriverUuid(@Param('driver_uuid') driver_uuid, @Body() body: any, @Req() request, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const data_user: any = jwt.decode(token);
            const user = await this.usersService.findOne(driver_uuid);
            if (!user) return res.status(HttpStatus.BAD_REQUEST).send(dataError('Không tìm thấy tài xế', null));
            if (!!body.order_code) {
                let orderExist = await this.tourOrderService.findByOrderCode(body.order_code)
                if(!orderExist) return res.status(HttpStatus.BAD_REQUEST).send(dataError('Mã Đơn hàng không đúng', null, ERROR_WALLET_ORDER_CODE_IS_NOT_EXIST));
            }
            let result;
            if (body.type === 'feeWallet')
                result = await this.walletService.updateFeeWalletForAmdin(data_user.id, user.id, body);
            else if (body.type === 'cashWallet')
                result = await this.walletService.updateCashWalletForAmdin(data_user.id, user.id, body);
            if (result) return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            else  return res.status(HttpStatus.BAD_REQUEST).send(dataError('Type null', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }


    @Post('create-wallet-for-driver')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'create one wallet by driver' })
    @ApiResponse({ status: 200, description: 'Ok' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not Found' })
    async createWalletForDriver(@Body() body: UserInfoDto,@Req() request, @Res() res: Response) {
        try {
            const user = await this.usersService.findOne(body.user_id);
            if(user){
                const result = await this.walletService.createNewWalletAccount(user.id);
                return res.status(HttpStatus.OK).send(dataSuccess('ok', result));
            }else{
                return res.status(HttpStatus.OK).send(dataError('Tài khoản này không tồn tại', null,ERROR_USER_EXITS_IN_SYSTEM));
            }

        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
