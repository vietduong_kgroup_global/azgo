import { forwardRef, Module } from '@nestjs/common';
import { WalletController } from './wallet.controller';
import { WalletService } from './wallet.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WalletRepository } from './wallet.repository';
import { VnPayModule } from '../vn-pay/vn-pay.module';
import { WalletHistoryModule } from '../wallet-history/wallet-history.module';
import { UsersModule } from '../users/users.module';
import { WithdrawMoneyModule } from '../withdraw-money/withdraw-money.module';
import { TourOrderModule } from '../tour-order/tour-order.module';
import { CustomChargeModule } from '../custom-charge/custom-charge.module';
import { SettingSystemModule } from '../setting-system/setting-system.module';
import { CommonService } from '../../helpers/common.service';
import { DriverProfileModule } from '../driver-profile/driver-profile.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([WalletRepository]),
        VnPayModule,
        CustomChargeModule,
        SettingSystemModule,
        forwardRef(() => DriverProfileModule),
        forwardRef(() => WalletHistoryModule),
        forwardRef(() => UsersModule),
        forwardRef(() => WithdrawMoneyModule),
        forwardRef(() => TourOrderModule), 
    ],
    controllers: [WalletController],
    providers: [
        WalletService,
        CommonService
    ],
    exports: [WalletService],
})
export class WalletModule {
}
