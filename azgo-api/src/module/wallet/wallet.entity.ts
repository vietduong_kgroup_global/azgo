import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Exclude} from 'class-transformer';
import {UsersEntity} from '../users/users.entity';

@Entity('az_wallets')
export class WalletEntity {
    @PrimaryGeneratedColumn({ type: 'bigint', unsigned: true })
    id: number;

    @Column({
        type: 'double',
        default: 0,
    })
    cash_wallet: number;

    @Column({
        type: 'double',
        default: 0,
    })
    fee_wallet: number;

    @Column({
        type: 'int',
        nullable: false,
    })
    driver_id: number;


    @Column({
        type: 'smallint',
        nullable: false,
    })
    status: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @OneToOne(type => UsersEntity, driver => driver.walletEntity)
    @JoinColumn({
        name: 'driver_id',
    })
    usersEntity: UsersEntity;
}
