import {ApiModelProperty} from '@nestjs/swagger';
import {IsNotEmpty, IsNumber} from 'class-validator';

export class ConvertCashWalletToFeeWalletDto {

    @IsNotEmpty()
    @ApiModelProperty({example: '123456', default: '123456', description: 'Password', required: true, type: String })
    password: string;

    @ApiModelProperty({
        example: 100000,
        description: 'Số tièn',
        default: 100000, type: Number, required: true,
    })
    @IsNumber()
    amount: number;

}
