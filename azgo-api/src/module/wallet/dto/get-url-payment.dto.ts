import {ApiModelProperty} from '@nestjs/swagger';
import {IsNotEmpty, IsNumber} from 'class-validator';

export class GetUrlPaymentDto {

    @IsNotEmpty()
    @ApiModelProperty({example: 'NCB', description: 'Mã ngân hàng', required: true })
    vnp_BankCode: string;

    @ApiModelProperty({
        example: 100000,
        description: 'Số tièn',
        default: 100000, type: Number, required: true,
    })
    @IsNumber()
    amount: number;

}
