import {ApiModelProperty} from '@nestjs/swagger';
import {IsNotEmpty, IsNumber} from 'class-validator';

export class UserInfoDto {
    @IsNotEmpty()
    user_id: string;
}
