import {ApiModelProperty} from '@nestjs/swagger';
import {IsNotEmpty, IsNumber} from 'class-validator';

export class ConvertVnPayToFeeWallet {

    @ApiModelProperty({
        example: 100000,
        description: 'Số tièn',
        default: 100000, type: Number, required: true,
    })
    @IsNumber()
    amount: number;

    @ApiModelProperty({
        example: 1,
        enum: [1, 2],
        description: 'Hình thức thanh toán, 1: Vn Pay, 2: Cash wallet',
        default: 1, type: Number, required: true,
    })
    @IsNumber()
    payment_method: number;

    @ApiModelProperty({
        example: 1,
        enum: [1, 2],
        description: 'Chuyển tiền vào, 1: Fee wallet, 2: Cash wallet',
        default: 1, type: Number, required: true,
    })
    @IsNumber()
    type: number;

}
