import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { WalletRepository } from './wallet.repository';
import { VnPayService } from '../vn-pay/vn-pay.service';
import * as moment from 'moment';
import { getManager } from 'typeorm';
import { WalletHistoryEntity } from '../wallet-history/wallet-history.entity';
import { WalletHistoryService } from '../wallet-history/wallet-history.service';
import { WalletEntity } from '../wallet/wallet.entity'
import { StatusCode } from '../../enums/status.enum';
import { ConvertCashWalletToFeeWalletDto } from './dto/convert-cash-wallet-to-fee-wallet.dto';
import { UsersService } from '../users/users.service';
import { WithdrawMoneyService } from '../withdraw-money/withdraw-money.service';
import { CustomChargeService } from '../custom-charge/custom-charge.service';
import { SettingSystemService } from '../setting-system/setting-system.service';
import { CommonService } from '../../helpers/common.service';
import { DriverProfileService } from '../driver-profile/driver-profile.service';
import {
    ERROR_UNKNOWN,
    ERROR_PASSWORD_INCORRECT,
    ERROR_WALLET_CASH_DO_NOT_ENOUGH
} from '../../constants/error.const';
import {
    TAG_HISTORY_WALLET_ARR,
    PAYMEN_METHOD_WALLET_HISTORY_ARR,
    TYPE_WALLET_HISTORY_ARR
} from '../../constants/secrets.const';
import { formatMoney, calcFeesOrder } from '../../helpers/utils';

@Injectable()
export class WalletService {
    constructor(
        @InjectRepository(WalletRepository)
        private readonly walletRepository: WalletRepository,
        private vnPayService: VnPayService,
        private readonly customChargeService: CustomChargeService,
        private walletHistoryService: WalletHistoryService,
        @Inject(forwardRef(() => WithdrawMoneyService))
        private readonly withdrawMoneyService: WithdrawMoneyService,
        @Inject(forwardRef(() => UsersService))
        private userService: UsersService,
        private readonly settingSystemService: SettingSystemService,
        private readonly commonService: CommonService,

    ) { }

    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        return await this.walletRepository.getAllByOffsetLimit(limit, offset, options);
    }

    async getOneByDriverId(driver_id: number) {
        return await this.walletRepository.findOneOrFail({ driver_id });
    }

    async createNewWalletAccount(user_id: number) {
        let findWalletByDriver = await this.walletRepository.findOne({ driver_id: user_id });
        if (!findWalletByDriver) { // tài xế chưa có
            findWalletByDriver = new WalletEntity()
            // create history wallet
            findWalletByDriver.fee_wallet = 0; // Số tiền nạp vào
            findWalletByDriver.cash_wallet = 0; // Số tiền của ví
            findWalletByDriver.driver_id = user_id;
            findWalletByDriver = await this.walletRepository.save(findWalletByDriver);
        }
        return findWalletByDriver;
    }

    async minusFeeWalletByDriverId(user_update_id: number, driver_id: number, fee_wallet: number, rate_of_charge: number) {
        const findWalletByDriver = await this.walletRepository.findOneOrFail({ driver_id });
        // Use transaction for data synchronization
        await getManager().transaction(async transactionalEntityManager => {
            // create history wallet
            const dataHistory = new WalletHistoryEntity();
            dataHistory.amount = - fee_wallet;
            dataHistory.driver_id = driver_id;
            dataHistory.user_update_id = user_update_id;
            dataHistory.type = TYPE_WALLET_HISTORY_ARR.FEE_WALLET; // Trừ tài khoản thanh toán
            dataHistory.payment_method = PAYMEN_METHOD_WALLET_HISTORY_ARR.OTHER; // Trừ hoàn thành đơn hàng
            dataHistory.content = `Đã khấu trừ ${rate_of_charge} % (${fee_wallet} vnd) vào tài khoản doanh thu vì đã hoàn thành chuyến`;
            await transactionalEntityManager.save(dataHistory);

            // update wallet
            findWalletByDriver.driver_id = driver_id;
            findWalletByDriver.fee_wallet -= fee_wallet;
            await transactionalEntityManager.save(findWalletByDriver);
        });
        return findWalletByDriver;
    }

    async updateByDriverId(user_update_id: number, driver_id: number, body: any) {
        const findWalletByDriver = await this.walletRepository.findOneOrFail({ driver_id });
        // Use transaction for data synchronization
        await getManager().transaction(async transactionalEntityManager => {
            // create history wallet
            if (body.payment_method === PAYMEN_METHOD_WALLET_HISTORY_ARR.VNPAY) {
                const dataHistory = new WalletHistoryEntity();
                dataHistory.amount = body.amount;
                dataHistory.driver_id = driver_id;
                dataHistory.user_update_id = user_update_id;
                dataHistory.type = body.type;
                dataHistory.payment_method = body.payment_method;
                dataHistory.content = 'Nạp tiền vào tài khoản doanh thu từ VNPAY';
                await transactionalEntityManager.save(dataHistory);
            }
            if (body.payment_method === PAYMEN_METHOD_WALLET_HISTORY_ARR.CASH_WALLET) {
                const dataHistory = new WalletHistoryEntity();
                dataHistory.amount = body.amount;
                dataHistory.driver_id = driver_id;
                dataHistory.user_update_id = user_update_id;
                dataHistory.type = TYPE_WALLET_HISTORY_ARR.FEE_WALLET; // cộng vao ví thanh toán
                dataHistory.payment_method = body.payment_method;
                dataHistory.content = 'Nạp tiền vào tài khoản doanh thu từ tài khoản thanh toán';
                await transactionalEntityManager.save(dataHistory);
                const dataHistory2 = new WalletHistoryEntity();
                dataHistory2.amount = -body.amount;
                dataHistory2.driver_id = driver_id;
                dataHistory.user_update_id = user_update_id;
                dataHistory2.type = TYPE_WALLET_HISTORY_ARR.CASH_WALLET; // trừ vào ví doanh thu
                dataHistory2.payment_method = body.payment_method;
                dataHistory2.content = 'Chuyển tiền từ tài khoản thanh toán vào tài khoản doanh thu';
                await transactionalEntityManager.save(dataHistory);
            }

            // update wallet
            findWalletByDriver.driver_id = driver_id;
            if (body.type === 1) {
                findWalletByDriver.fee_wallet += body.amount;
            } else {
                findWalletByDriver.cash_wallet += body.amount;

            }
            await transactionalEntityManager.save(findWalletByDriver);
        });
        return findWalletByDriver;
    }

    async updateFeeWalletForAmdin(user_update_id: number, driver_id: number, body: any) {
        const findWalletByDriver = await this.walletRepository.findOneOrFail({ driver_id });
        // Use transaction for data synchronization
        await getManager().transaction(async transactionalEntityManager => {
            // create history wallet
            const dataHistory = new WalletHistoryEntity();
            dataHistory.amount = body.fee_wallet; // Số tiền thay đổi
            dataHistory.driver_id = driver_id;
            dataHistory.user_update_id = user_update_id;
            dataHistory.type = TYPE_WALLET_HISTORY_ARR.FEE_WALLET; // Thay đổi ví thanh toán
            dataHistory.payment_method = PAYMEN_METHOD_WALLET_HISTORY_ARR.OTHER; // Thay đổi từ trang admin
            dataHistory.content = body.content;
            dataHistory.payment_code = body.payment_code;
            dataHistory.order_code = body.order_code;
            await transactionalEntityManager.save(dataHistory);
            // update wallet
            findWalletByDriver.driver_id = driver_id;
            findWalletByDriver.status = body.status;
            findWalletByDriver.fee_wallet += body.fee_wallet;
            await transactionalEntityManager.save(findWalletByDriver);
        });
        return findWalletByDriver;
    }
    async updateCashWalletForAmdin(user_update_id: number, driver_id: number, body: any) {
        const findWalletByDriver = await this.walletRepository.findOneOrFail({ driver_id });

        // Use transaction for data synchronization
        await getManager().transaction(async transactionalEntityManager => {
            // create history wallet
            const dataHistory = new WalletHistoryEntity();
            dataHistory.amount = body.cash_wallet; // Số tiền thay đổi
            dataHistory.driver_id = driver_id;
            dataHistory.user_update_id = user_update_id;
            dataHistory.type = TYPE_WALLET_HISTORY_ARR.CASH_WALLET; // Thay đổi ví doanh thu
            dataHistory.payment_method = PAYMEN_METHOD_WALLET_HISTORY_ARR.OTHER; // Thay đổi từ trang admin
            dataHistory.content = body.content;
            dataHistory.payment_code = body.payment_code;
            dataHistory.order_code = body.order_code;
            await transactionalEntityManager.save(dataHistory);
            // update wallet
            findWalletByDriver.driver_id = driver_id;
            findWalletByDriver.status = body.status;
            findWalletByDriver.cash_wallet += body.cash_wallet;
            await transactionalEntityManager.save(findWalletByDriver);
        });
        return findWalletByDriver;
    }
    async updateWalletForDriverFinishTour(tour_order: any, transactionalEntityManager: any) {
        try {
            let findWalletByDriver = await this.walletRepository.findOne({ driver_id: tour_order.driver_id });
            if (!findWalletByDriver) { // tài xế chưa có ví
                findWalletByDriver = await this.createNewWalletAccount(tour_order.driver_id);
            }
            let customCharge = await this.customChargeService.getOneByVehicleGroupName('TOUR');
            let azgo_fee = Number(customCharge.rate_of_charge / 100);
            let setting = await this.settingSystemService.getOne();
            let price = tour_order.order_price || 0;
            let fee_order = calcFeesOrder(price, setting, azgo_fee)
            // if (tour_order.payment_method === 'cash') { // Trả tiền mặt
            // Ktra phí TAX, VAT
            let countTaxVatFeeExist = await this.walletHistoryService.countOrderFeeExist(
                tour_order.order_code,
                tour_order.driver_id,
                [TAG_HISTORY_WALLET_ARR.TAX, TAG_HISTORY_WALLET_ARR.VAT]
            );
            if (!countTaxVatFeeExist.status)
                return {
                    status: countTaxVatFeeExist.status,
                    data: countTaxVatFeeExist.data,
                    error: countTaxVatFeeExist.error,
                    message: countTaxVatFeeExist.message
                };
            if (countTaxVatFeeExist.data === 0) { // Đơn hàng chưa trừ TAX, VAT
                // History wallet VAT
                const dataHistoryVAT = new WalletHistoryEntity();
                dataHistoryVAT.amount = -fee_order.vat_fee; // Số tiền thay đổi
                dataHistoryVAT.driver_id = tour_order.driver_id;
                dataHistoryVAT.type = TYPE_WALLET_HISTORY_ARR.FEE_WALLET; // Thay đổi ví thanh toán
                dataHistoryVAT.tag = TAG_HISTORY_WALLET_ARR.VAT; // định danh ý nghĩa cho history
                dataHistoryVAT.payment_method = PAYMEN_METHOD_WALLET_HISTORY_ARR.OTHER; // Thay đổi do hoàn thành chuyến đi
                dataHistoryVAT.content = `Đã khấu trừ ${setting.vat * 100}% (${formatMoney(fee_order.vat_fee)} vnđ) thuế VAT từ tài khoản doanh thu vì đã hoàn thành đơn hàng #${tour_order.order_code} (${formatMoney(tour_order.order_price)})`;
                dataHistoryVAT.order_code = tour_order.order_code;
                await transactionalEntityManager.save(dataHistoryVAT);

                // History wallet Tax
                const dataHistoryTAX = new WalletHistoryEntity();
                dataHistoryTAX.amount = -fee_order.tax_fee; // Số tiền thay đổi
                dataHistoryTAX.driver_id = tour_order.driver_id;
                dataHistoryTAX.type = TYPE_WALLET_HISTORY_ARR.FEE_WALLET; // Thay đổi ví thanh toán
                dataHistoryTAX.tag = TAG_HISTORY_WALLET_ARR.TAX; // định danh ý nghĩa cho history
                dataHistoryTAX.payment_method = PAYMEN_METHOD_WALLET_HISTORY_ARR.OTHER; // Thay đổi do hoàn thành chuyến đi
                dataHistoryTAX.content = `Đã khấu trừ ${setting.tax * 100}% (${formatMoney(fee_order.tax_fee)} vnđ) thuế TNCN vào tài khoản doanh thu vì đã hoàn thành đơn hàng #${tour_order.order_code} (${formatMoney(tour_order.order_price)})`;
                dataHistoryTAX.order_code = tour_order.order_code;
                await transactionalEntityManager.save(dataHistoryTAX);
                // Trừ VAT, TAX vào fee wallet của tài xế
                findWalletByDriver.fee_wallet -= (fee_order.vat_fee + fee_order.tax_fee);
            }

            // Ktra phí Azgo
            let countAzgoFeeExist = await this.walletHistoryService.countOrderFeeExist(
                tour_order.order_code,
                tour_order.driver_id,
                [TAG_HISTORY_WALLET_ARR.AZGO_FEE]
            );
            if (!countAzgoFeeExist.status)
                return {
                    status: countAzgoFeeExist.status,
                    data: countAzgoFeeExist.data,
                    error: countAzgoFeeExist.error,
                    message: countAzgoFeeExist.message
                };
            let countAzgoFeeReturn = await this.walletHistoryService.countOrderFeeReturn(
                tour_order.order_code,
                tour_order.driver_id,
                [TAG_HISTORY_WALLET_ARR.RETURN_AZGO_FEE]
            );
            if (!countAzgoFeeReturn.status)
                return {
                    status: countAzgoFeeReturn.status,
                    data: countAzgoFeeReturn.data,
                    error: countAzgoFeeReturn.error,
                    message: countAzgoFeeReturn.message
                };
            if (countAzgoFeeExist.data === countAzgoFeeReturn.data) { // Đơn hàng chưa trừ phí Azgo
                // History wallet Fee Azgo
                const dataHistoryFee = new WalletHistoryEntity();
                dataHistoryFee.amount = -fee_order.azgo_fee_order; // Số tiền thay đổi
                dataHistoryFee.driver_id = tour_order.driver_id;
                dataHistoryFee.type = TYPE_WALLET_HISTORY_ARR.FEE_WALLET; // Thay đổi ví thanh toán
                dataHistoryFee.tag = TAG_HISTORY_WALLET_ARR.AZGO_FEE; // định danh ý nghĩa cho history
                dataHistoryFee.payment_method = PAYMEN_METHOD_WALLET_HISTORY_ARR.OTHER; // Thay đổi do hoàn thành chuyến đi
                dataHistoryFee.content = `Đã khấu trừ ${azgo_fee * 100}% (${formatMoney(fee_order.azgo_fee_order)} vnđ) phí của AZGO từ tài khoản doanh thu vì đã hoàn thành đơn hàng #${tour_order.order_code} (${formatMoney(tour_order.order_price)})`;
                dataHistoryFee.order_code = tour_order.order_code;
                await transactionalEntityManager.save(dataHistoryFee);
                // Trừ phí azgo vào fee wallet của tài xế
                findWalletByDriver.fee_wallet -= fee_order.azgo_fee_order;
            }

            let countOrderCashReturn = await this.walletHistoryService.countOrderFeeReturn(
                tour_order.order_code,
                tour_order.driver_id,
                [TAG_HISTORY_WALLET_ARR.ORDER_CASH]
            );
            if (!countOrderCashReturn.status)
                return {
                    status: countOrderCashReturn.status,
                    data: countOrderCashReturn.data,
                    error: countOrderCashReturn.error,
                    message: countOrderCashReturn.message
                };
            if (tour_order.is_paid && countOrderCashReturn.data === 0) {
                const dataHistoryCashWallet = new WalletHistoryEntity();
                dataHistoryCashWallet.amount = price; // Số tiền thay đổi
                dataHistoryCashWallet.driver_id = tour_order.driver_id;
                dataHistoryCashWallet.type = TYPE_WALLET_HISTORY_ARR.CASH_WALLET; // Thay đổi ví doanh thu
                dataHistoryCashWallet.tag = TAG_HISTORY_WALLET_ARR.ORDER_CASH; // định danh ý nghĩa cho history
                dataHistoryCashWallet.payment_method = PAYMEN_METHOD_WALLET_HISTORY_ARR.OTHER; // Thay đổi do hoàn thành chuyến đi
                dataHistoryCashWallet.content = `Hoàn trả ${formatMoney(price)} vnđ cước phí đơn hàng #${tour_order.order_code} vào tài khoản thanh toán`;
                dataHistoryCashWallet.order_code = tour_order.order_code;
                // Trả lai tiền vào tài khoản thanh toán cho tài xế trong trường hợp đơn hàng đã được thanh toán
                findWalletByDriver.cash_wallet += price;
                await transactionalEntityManager.save(dataHistoryCashWallet);
            }
            await transactionalEntityManager.save(findWalletByDriver);
            return {
                status: true,
                data: null,
                error: null,
                message: null
            };
        } catch (error) {
            console.log('updateWalletForDriverFinishTour', error);
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message || 'Error in update wallet driver finish tour'
            };
        }
    }

    async minusAzgoFeeReceiveOrder(tour_order: any, driver_id: number) {
        try {
            let countAzgoFeeExist = await this.walletHistoryService.countOrderFeeExist(
                tour_order.order_code,
                driver_id,
                [TAG_HISTORY_WALLET_ARR.AZGO_FEE]
            );
            if (!countAzgoFeeExist.status)
                return {
                    status: countAzgoFeeExist.status,
                    data: countAzgoFeeExist.data,
                    error: countAzgoFeeExist.error,
                    message: countAzgoFeeExist.message
                };
            let countAzgoFeeReturn = await this.walletHistoryService.countOrderFeeReturn(
                tour_order.order_code,
                driver_id,
                [TAG_HISTORY_WALLET_ARR.RETURN_AZGO_FEE]
            );
            if (!countAzgoFeeReturn.status)
                return {
                    status: countAzgoFeeReturn.status,
                    data: countAzgoFeeReturn.data,
                    error: countAzgoFeeReturn.error,
                    message: countAzgoFeeReturn.message
                };
            if (countAzgoFeeExist.data === countAzgoFeeReturn.data) { // Đơn hàng chưa được trừ phí AZGO
                const findWalletByDriver = await this.createNewWalletAccount(driver_id);
                let customCharge = await this.customChargeService.getOneByVehicleGroupName('TOUR');
                let azgo_fee = Number(customCharge.rate_of_charge / 100);
                let setting = await this.settingSystemService.getOne();
                let price = tour_order.order_price || 0;
                let fee_order = calcFeesOrder(price, setting, azgo_fee);

                await getManager().transaction(async transactionalEntityManager => {
                    // History wallet Fee Azgo
                    const dataHistoryFee = new WalletHistoryEntity();
                    dataHistoryFee.amount = -fee_order.azgo_fee_order; // Số tiền thay đổi
                    dataHistoryFee.driver_id = driver_id;
                    dataHistoryFee.type = TYPE_WALLET_HISTORY_ARR.FEE_WALLET; // Thay đổi tài khoản doanh thu
                    dataHistoryFee.tag = TAG_HISTORY_WALLET_ARR.AZGO_FEE; // định danh ý nghĩa cho history
                    dataHistoryFee.payment_method = PAYMEN_METHOD_WALLET_HISTORY_ARR.OTHER; // Thay đổi do hoàn thành chuyến đi
                    dataHistoryFee.content = `Đã khấu trừ ${azgo_fee * 100}% (${formatMoney(fee_order.azgo_fee_order)} vnđ) phí của AZGO từ tài khoản doanh thu vì đã nhận đơn hàng #${tour_order.order_code} (${formatMoney(tour_order.order_price)})`;
                    dataHistoryFee.order_code = tour_order.order_code;
                    await transactionalEntityManager.save(dataHistoryFee);
                    findWalletByDriver.fee_wallet -= fee_order.azgo_fee_order;
                    await transactionalEntityManager.save(findWalletByDriver);
                    return {
                        status: true,
                        data: null,
                        error: null,
                        message: null
                    };
                })
            }
            return {
                status: true,
                data: null,
                error: null,
                message: null
            };
        } catch (error) {
            console.log('minusAzgoFeeReceiveOrder', error);
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message || 'Error in Azgo fee receive order'
            };
        }
    }

    async returnAzgoFeeReceiveOrder(tour_order: any, driver_id: number) {
        let _this = this;
        try {
            let countAzgoFeeExist = await _this.walletHistoryService.countOrderFeeExist(
                tour_order.order_code,
                driver_id,
                [TAG_HISTORY_WALLET_ARR.AZGO_FEE]
            );
            if (!countAzgoFeeExist.status)
                return {
                    status: countAzgoFeeExist.status,
                    data: countAzgoFeeExist.data,
                    error: countAzgoFeeExist.error,
                    message: countAzgoFeeExist.message
                };
            let countAzgoFeeReturn = await this.walletHistoryService.countOrderFeeReturn(
                tour_order.order_code,
                driver_id,
                [TAG_HISTORY_WALLET_ARR.RETURN_AZGO_FEE]
            );
            if (!countAzgoFeeReturn.status)
                return {
                    status: countAzgoFeeReturn.status,
                    data: countAzgoFeeReturn.data,
                    error: countAzgoFeeReturn.error,
                    message: countAzgoFeeReturn.message
                };
            if (countAzgoFeeExist.data > countAzgoFeeReturn.data) { // Đơn hàng đã trừ phí
                const findWalletByDriver = await _this.createNewWalletAccount(driver_id);
                let customCharge = await _this.customChargeService.getOneByVehicleGroupName('TOUR');
                let azgo_fee = Number(customCharge.rate_of_charge / 100);
                let setting = await _this.settingSystemService.getOne();
                let price = tour_order.order_price || 0;
                let fee_order = calcFeesOrder(price, setting, azgo_fee);

                await getManager().transaction(async transactionalEntityManager => {
                    // History wallet Fee Azgo
                    const dataHistoryFee = new WalletHistoryEntity();
                    dataHistoryFee.amount = fee_order.azgo_fee_order; // Số tiền thay đổi
                    dataHistoryFee.driver_id = driver_id;
                    dataHistoryFee.type = TYPE_WALLET_HISTORY_ARR.FEE_WALLET; // Thay đổi tài khoản doanh thu
                    dataHistoryFee.tag = TAG_HISTORY_WALLET_ARR.RETURN_AZGO_FEE; // định danh ý nghĩa cho history
                    dataHistoryFee.payment_method = PAYMEN_METHOD_WALLET_HISTORY_ARR.OTHER; // Thay đổi do hoàn thành chuyến đi
                    dataHistoryFee.content = `Hoàn trả ${azgo_fee * 100}% (${formatMoney(fee_order.azgo_fee_order)} vnđ) phí của AZGO vào tài khoản doanh thu vì đã huỷ đơn hàng #${tour_order.order_code} (${formatMoney(tour_order.order_price)})`;
                    dataHistoryFee.order_code = tour_order.order_code;
                    await transactionalEntityManager.save(dataHistoryFee);
                    findWalletByDriver.fee_wallet += fee_order.azgo_fee_order;
                    await transactionalEntityManager.save(findWalletByDriver);
                    return {
                        status: true,
                        data: null,
                        error: null,
                        message: null
                    };
                })
            }
            return {
                status: true,
                data: null,
                error: null,
                message: null
            };
        } catch (error) {
            console.log('minusAzgoFeeReceiveOrder', error);
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message || 'Error in Azgo fee receive order'
            };
        }
    }

    async confirmRequestWithdrawCashwallet(request: any, transactionalEntityManager: any) {
        let findWalletByDriver = await this.createNewWalletAccount(request.driver_id);
        // await getManager().transaction(async transactionalEntityManager => {
        const dataHistoryFee = new WalletHistoryEntity();
        dataHistoryFee.amount = -request.amount; // Số tiền thay đổi
        dataHistoryFee.driver_id = request.driver_id;
        dataHistoryFee.type = TYPE_WALLET_HISTORY_ARR.CASH_WALLET; // Thay đổi ví doanh thu
        dataHistoryFee.tag = TAG_HISTORY_WALLET_ARR.WITHDRAW_CASH_WALLET_TO_BANK; // định danh ý nghĩa cho history
        dataHistoryFee.payment_method = PAYMEN_METHOD_WALLET_HISTORY_ARR.CASH_WALLET; // Tiền từ ví doanh thu ví doanh thu
        dataHistoryFee.content = `Rút ${formatMoney(request.amount)} vnđ từ tài khoản doanh thu về ngân hàng`;
        dataHistoryFee.order_code = request.request_code;
        await transactionalEntityManager.save(dataHistoryFee);
        findWalletByDriver.cash_wallet -= request.amount;
        await transactionalEntityManager.save(findWalletByDriver);
        return {
            status: true,
            data: null,
            error: null,
            message: null
        };
        // })
    }

    async cancelRequestWithdrawCashwallet(request: any, transactionalEntityManager: any) {
        let findWalletByDriver = await this.createNewWalletAccount(request.driver_id);
        // await getManager().transaction(async transactionalEntityManager => {
        const dataHistoryFee = new WalletHistoryEntity();
        dataHistoryFee.amount = request.amount; // Số tiền thay đổi
        dataHistoryFee.driver_id = request.driver_id;
        dataHistoryFee.type = TYPE_WALLET_HISTORY_ARR.CASH_WALLET; // Thay đổi ví doanh thu
        dataHistoryFee.tag = TAG_HISTORY_WALLET_ARR.RETURN_MONEY_WITHDRAW_TO_CASH_WALLET; // định danh ý nghĩa cho history
        dataHistoryFee.payment_method = PAYMEN_METHOD_WALLET_HISTORY_ARR.OTHER; // Tiền hoàn lại do yêu cầu chuyển tiền bị huỷ
        dataHistoryFee.content = `Hoàn lại ${formatMoney(request.amount)} vnđ vào tài khoản doanh thu do yêu câu rút tiền bị huỷ`;
        dataHistoryFee.order_code = request.request_code;
        await transactionalEntityManager.save(dataHistoryFee);
        findWalletByDriver.cash_wallet += request.amount;
        await transactionalEntityManager.save(findWalletByDriver);
        // })
        return {
            status: true,
            data: null,
            error: null,
            message: null
        };
    }

    async withdrawMoneyFromCashWalletByDriver(driverInfo: any, body: ConvertCashWalletToFeeWalletDto) {
        // Check password
        const driver = await this.userService.findOneByEmailOrPhone(driverInfo.phone);
        if (!driver.user.checkIfUnencryptedPasswordIsValid(body.password)) throw { message: 'Password incorrect', status: StatusCode.BAD_REQUEST };
        // check cash wallet
        const findWalletByDriver = await this.walletRepository.createQueryBuilder('wallet')
            .where('driver_id = :driver_id', { driver_id: driverInfo.id })
            .where('cash_wallet >= :amount', { amount: body.amount })
            .getOne();
        if (!findWalletByDriver) throw { message: 'Error Cash wallet < amount input', status: StatusCode.BAD_REQUEST };
        // Use transaction for data synchronization
        return await this.withdrawMoneyService.requestWithdrawBydriver(
            { driver_id: driverInfo.id, amount: body.amount }, findWalletByDriver);
    }
    async convertCashToFeeWallet(driverInfo: any, body: ConvertCashWalletToFeeWalletDto) {
        // Check password
        const driver = await this.userService.findOneByEmailOrPhone(driverInfo.phone);
        if (!driver.user.checkIfUnencryptedPasswordIsValid(body.password))
            return {
                status: false,
                data: null,
                error: ERROR_PASSWORD_INCORRECT,
                message: 'Password incorrect'
            };

        // Check Wallet existed
        let findWalletByDriver = await this.walletRepository.findOne({ driver_id: driverInfo.id });
        if (!findWalletByDriver) { // tài xế chưa có ví
            findWalletByDriver = await this.createNewWalletAccount(driverInfo.id);
        }
        if (findWalletByDriver.cash_wallet < body.amount) {
            return {
                status: false,
                data: null,
                error: ERROR_WALLET_CASH_DO_NOT_ENOUGH,
                message: 'Error Cash wallet < amount input'
            };
        }
        // Use transaction for data synchronization
        await getManager().transaction(async transactionalEntityManager => {
            // create history wallet
            const dataHistory = new WalletHistoryEntity();
            dataHistory.amount = body.amount;
            dataHistory.driver_id = driverInfo.id;
            dataHistory.user_update_id = driverInfo.id;
            dataHistory.type = TYPE_WALLET_HISTORY_ARR.FEE_WALLET; // Cộng tiền vào ví thanh toán
            dataHistory.payment_method = PAYMEN_METHOD_WALLET_HISTORY_ARR.CASH_WALLET;
            dataHistory.content = 'Nạp tiền vào tài khoản doanh thu từ tài khoản thanh toán';
            await transactionalEntityManager.save(dataHistory);
            // update wallet
            findWalletByDriver.fee_wallet += body.amount;

            const dataHistory2 = new WalletHistoryEntity();
            dataHistory2.driver_id = driverInfo.id;
            dataHistory.user_update_id = driverInfo.id;
            dataHistory2.type = TYPE_WALLET_HISTORY_ARR.CASH_WALLET; // trừ tiền trên ví doanh thu
            dataHistory2.payment_method = PAYMEN_METHOD_WALLET_HISTORY_ARR.CASH_WALLET;
            dataHistory2.amount = -body.amount;
            dataHistory2.content = 'Chuyển tiền từ tài khoản thanh toán vào tài khoản doanh thu';
            await transactionalEntityManager.save(dataHistory2);
            // update wallet
            findWalletByDriver.cash_wallet -= body.amount;

            await transactionalEntityManager.save(findWalletByDriver);
        });
        // return findWalletByDriver;
        return {
            status: true,
            data: findWalletByDriver,
            error: null,
            message: null
        };
    }

    async getUrlPaymentVnpay(body: any) {
        const getTimeToday = moment().toDate().getTime();
        // const date = new Date(getTimeToday);
        const convert_reference = body.driverInfo.user_id.toString() + '_' + getTimeToday;

        const dataPost = {
            vnp_Amount: Number(body.amount) * 100,
            vnp_BankCode: body.vnp_BankCode,
            vnp_OrderInfo: 'Nap tien vao vi cuoc phi',
            vnp_OrderType: '250000', // Thanh toan hoa don
            vnp_TxnRef: convert_reference,
        };
        return await this.vnPayService.createPaymentUrl(dataPost, body.vnp_IpAddr);
    }
    /**
     * This function get wallet by driver_id
     * @param driver_id
     */
    async findByDriverId(driver_id: any) {
        return await this.walletRepository.findByDriverId(driver_id);
    }
}
