import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {VehicleEntity} from '../vehicle/vehicle.entity';
import {VehicleEntityExists} from './entity-vehicle-exists.constraint';
import {VehicleGroupsEntity} from '../vehicle-groups/vehicle-groups.entity';
import {VehicleTypesEntity} from '../vehicle-types/vehicle-types.entity';
import {ProvidersInfoEntity} from '../providers-info/providers-info.entity';
import {VehicleGroupsEntityExists} from './entity-vehicle-groups-exists.constraint';
import {VehicleTypesEntityExists} from './entity-vehicle-types-exists.constraint';
import {ProvidersInfoEntityExists} from './entity-providers-info-exists.constraint';
import {PromotionsEntity} from '../promotions/promotions.entity';
import {PromotionEntityExists} from './entity-promotions-exists.constraint';
import {DriverProfileEntityExists} from './entity-driver-profile-exists.constraint';
import {CustomerProfileEntityExists} from './entity-customer-profile-exists.constraint';
import {DriverProfileEntity} from '../driver-profile/driver-profile.entity';
import {CustomerProfileEntity} from '../customer-profile/customer-profile.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            VehicleEntity, VehicleGroupsEntity, VehicleTypesEntity,
            ProvidersInfoEntity, PromotionsEntity, DriverProfileEntity,
            CustomerProfileEntity,
        ])],
    providers: [
        VehicleEntityExists,
        VehicleGroupsEntityExists,
        VehicleTypesEntityExists,
        ProvidersInfoEntityExists,
        PromotionEntityExists,
        DriverProfileEntityExists,
        CustomerProfileEntityExists,
    ],
})
export class ValidatorModule {}
