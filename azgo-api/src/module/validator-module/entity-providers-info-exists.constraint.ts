import {
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments,
} from 'class-validator';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {ProvidersInfoEntity} from '../providers-info/providers-info.entity';

@ValidatorConstraint({ async: true })
@Injectable()
export class ProvidersInfoEntityExists implements ValidatorConstraintInterface {
    constructor(
        @InjectRepository(ProvidersInfoEntity) private repository: Repository<ProvidersInfoEntity>) {}

    async validate(id: number, args: ValidationArguments): Promise<boolean> {
        const entity = await this.repository.findOne(id);
        return entity !== undefined;
    }
}
