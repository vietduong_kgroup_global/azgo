import {
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments,
} from 'class-validator';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {CustomerProfileEntity} from '../customer-profile/customer-profile.entity';

@ValidatorConstraint({ async: true })
@Injectable()
export class CustomerProfileEntityExists implements ValidatorConstraintInterface {
    constructor(
        @InjectRepository(CustomerProfileEntity) private repository: Repository<CustomerProfileEntity>) {}

    async validate(id: number, args: ValidationArguments): Promise<boolean> {
        const entity = await this.repository.findOne(id);
        return entity !== undefined;
    }
}
