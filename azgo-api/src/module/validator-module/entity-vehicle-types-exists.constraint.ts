import {
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments,
} from 'class-validator';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {VehicleTypesEntity} from '../vehicle-types/vehicle-types.entity';

@ValidatorConstraint({ async: true })
@Injectable()
export class VehicleTypesEntityExists implements ValidatorConstraintInterface {
    constructor(
        @InjectRepository(VehicleTypesEntity) private repository: Repository<VehicleTypesEntity>) {}

    async validate(id: number, args: ValidationArguments): Promise<boolean> {
        const entity = await this.repository.findOne(id);
        return entity !== undefined;
    }
}
