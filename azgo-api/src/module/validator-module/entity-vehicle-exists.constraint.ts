import {
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments,
} from 'class-validator';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {VehicleEntity} from '../vehicle/vehicle.entity';

@ValidatorConstraint({ async: true })
@Injectable()
export class VehicleEntityExists implements ValidatorConstraintInterface {
    constructor(
        @InjectRepository(VehicleEntity) private repository: Repository<VehicleEntity>) {}

    async validate(id: number, args: ValidationArguments): Promise<boolean> {
        const entity = await this.repository.findOne(id);
        return entity !== undefined;
    }
}
