import {
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments,
} from 'class-validator';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {PromotionsEntity} from '../promotions/promotions.entity';

@ValidatorConstraint({ async: true })
@Injectable()
export class PromotionEntityExists implements ValidatorConstraintInterface {
    constructor(
        @InjectRepository(PromotionsEntity) private repository: Repository<PromotionsEntity>) {}

    async validate(id: number, args: ValidationArguments): Promise<boolean> {
        const entity = await this.repository.findOne(id);
        return entity !== undefined;
    }
}
