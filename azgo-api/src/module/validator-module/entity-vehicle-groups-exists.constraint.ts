import {
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments,
} from 'class-validator';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {VehicleGroupsEntity} from '../vehicle-groups/vehicle-groups.entity';

@ValidatorConstraint({ async: true })
@Injectable()
export class VehicleGroupsEntityExists implements ValidatorConstraintInterface {
    constructor(
        @InjectRepository(VehicleGroupsEntity) private repository: Repository<VehicleGroupsEntity>) {}

    async validate(id: number, args: ValidationArguments): Promise<boolean> {
        const entity = await this.repository.findOne(id);
        return entity !== undefined;
    }
}
