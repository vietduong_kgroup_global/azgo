import {
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments,
} from 'class-validator';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {DriverProfileEntity} from '../driver-profile/driver-profile.entity';

@ValidatorConstraint({ async: true })
@Injectable()
export class DriverProfileEntityExists implements ValidatorConstraintInterface {
    constructor(
        @InjectRepository(DriverProfileEntity)
        private repository: Repository<DriverProfileEntity>) {}

    async validate(id: number, args: ValidationArguments): Promise<boolean> {
        const entity = await this.repository.findOne(id);
        return entity !== undefined;
    }
}
