import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ResetPasswordService } from './reset-password.service';
import { ResetPasswordController } from './reset-password.controller';
import { UsersModule } from '../users/users.module';
import { ResetPasswordEntity } from './reset-password.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([ResetPasswordEntity]),
        UsersModule,
    ],
    providers: [ResetPasswordService],
    controllers: [ResetPasswordController],
})
export class ResetPasswordModule { }
