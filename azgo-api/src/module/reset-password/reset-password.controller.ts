import { Controller, Body, Post, HttpStatus, Res, Param, Put, Get } from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation, ApiImplicitParam } from '@nestjs/swagger';
import { Response } from 'express';
import { dataSuccess, dataError } from '@azgo-module/core/dist/utils/jsonFormat';
import { ResetPasswordService } from './reset-password.service';
import { RecoverDto } from './dto/recover.dto';
import { PasswordDto } from './dto/password.dto';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@ApiUseTags('Reset Password')
@Controller('reset-password')
export class ResetPasswordController {
    constructor(
        private resetPasswordService: ResetPasswordService,
    ) { }

    @Post()
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @ApiOperation({ title: 'Reset password' })
    @ApiResponse({ status: 200, description: 'Mail sent' })
    @ApiResponse({ status: 401, description: 'Unauthorized.' })
    async resetPassword(@Body(CustomValidationPipe) recoverDto: RecoverDto, @Res() res: Response) {
        try {
            await this.resetPasswordService.sendTokenMail(recoverDto.email);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
        return res.status(HttpStatus.OK).send(dataSuccess('Mail send complete', null));
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @Put('change_password/:token')
    @ApiOperation({ title: 'Update new password' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 401, description: 'Unauthorized.' })
    @ApiImplicitParam({ name: 'token', description: 'token email', required: true, type: 'string' })
    async changePassword(@Param('token') token: string, @Body(CustomValidationPipe) passwordDto: PasswordDto, @Res() res: Response) {
        try {
            await this.resetPasswordService.checkTokenAndUpdatePassword(token, passwordDto);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
        return res.status(HttpStatus.OK).send(dataSuccess('Change password is completed', null));
    }

    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @Get('create-new-password/:token')
    @ApiOperation({ title: 'Check token' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 401, description: 'Unauthorized.' })
    @ApiImplicitParam({ name: 'token', description: 'token email', required: true, type: 'string' })
    async checkToken(@Param('token') token: string, @Res() res: Response) {
        try {
            await this.resetPasswordService.checkToken(token);
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError('Token is invalid', null));
        }
        return res.status(HttpStatus.OK).send(dataSuccess('oK', null));
    }
}
