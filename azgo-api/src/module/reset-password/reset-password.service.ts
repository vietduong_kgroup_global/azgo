import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersService } from '../users/users.service';
import { StringHelper } from '@azgo-module/core/dist/utils/string.helper';
import { EmailUtil } from '@azgo-module/core/dist/utils/email.util';
import config from '../../../config/config';
import { PasswordDto } from './dto/password.dto';
import { ResetPasswordEntity } from './reset-password.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ResetPasswordService {
    constructor(
        @InjectRepository(ResetPasswordEntity)
        private resetPasswordRepository: Repository<ResetPasswordEntity>,
        private userService: UsersService,
    ) { }

    /**
     * Check token and email
     * @param token
     * @param email
     * @returns {Promise<any>}
     */
    async validateToken(token: string, email: string): Promise<any> {
        const check = await this.resetPasswordRepository.findOne({ token, email });
        if (!check) return false;
        return true;
    }

    /**
     * Delete row reset password when complete change password
     * @param email
     * @returns {Promise<any>}
     */
    async deleteResetPassword(email: string): Promise<any> {
        return await this.resetPasswordRepository.delete({ email });
    }

    /**
     * Send mail and token
     * @param email
     * @returns {Promise<any>}
     */
    async sendTokenMail(email: string): Promise<any> {

        // Check exists email
        const check = await this.userService.findOneByEmail(email);
        if (!check) throw { message: 'Email is not exsits' };

        // Generate token
        const generateToken = StringHelper.generateOtpToken();
        const resetPassword = {
            email,
            token: generateToken,
        };

        // Save to database token and email
        await this.resetPasswordRepository.save(resetPassword);

        // Encode URL and sendmail
        const encodeUrl = StringHelper.encodeBase64(JSON.stringify(resetPassword));
        let emailUtil = EmailUtil.getInstance();
        await emailUtil.sendEmail([email], 'Reset your new password', 'Reset your new password', `Click link to URL: ${config.urlRestPassword}${encodeUrl}`);
    }

    /**
     * Check token and update new password
     * @param encode_token
     * @param passwordDto
     * @returns {Promise<any>}
     */
    async checkTokenAndUpdatePassword(encode_token: string, passwordDto: PasswordDto): Promise<any> {

        const { password, password_confirmation } = passwordDto;

        // Compare password confirmation and password
        if (password_confirmation !== password) throw { message: 'Password confirmation is not same' };

        // Decode and check token
        const decode = StringHelper.decodeBase64(encode_token);
        if (!decode) throw { message: 'Token is invalid' };
        const jsonParse = await JSON.parse(decode);
        const { email, token } = jsonParse;
        const check = await this.validateToken(token, email);
        if (!check) throw { message: 'Token is invalid' };

        // Update password
        const user = await this.userService.findOneByEmail(email);
        if (!user) throw { message: 'Token is invalid' };
        user.password = password;
        user.hashPassword();
        await this.userService.updatePassword(user);
        await this.deleteResetPassword(email);
    }

    /**
     * Check token
     * @param encode_token
     * @returns {Promise<any>}
     */
    async checkToken(encode_token: string): Promise<any> {
        // Decode and check token
        const decode = StringHelper.decodeBase64(encode_token);
        if (!decode) throw { message: 'Token is invalid' };
        const jsonParse = await JSON.parse(decode);
        const { email, token } = jsonParse;
        const check = await this.validateToken(token, email);
        if (!check) throw { message: 'Token is invalid' };
    }
}
