import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsNotBlank } from '@azgo-module/core/dist/utils/blank.util';

export class PasswordDto {
    @ApiModelProperty({ format: 'password', example: '123456' })
    @IsNotEmpty({ message: 'Password is required' })
    @IsNotBlank('password', { message: 'Is not white space' })
    password: string;

    @ApiModelProperty({ required: true, example: '123456' })
    @IsNotEmpty({ message: 'Password is required' })
    @IsNotBlank('password_confirmation', { message: 'Is not white space' })
    password_confirmation: string;
}
