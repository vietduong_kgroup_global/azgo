import { Entity, Column, PrimaryGeneratedColumn, Index } from 'typeorm';

@Entity('az_password_resets')
export class ResetPasswordEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '225',
    })
    token: string;

    @Index('user_email_unique', { unique: true })
    @Column({ length: 255 })
    email: string;

    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;
}
