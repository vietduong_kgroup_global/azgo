import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TourOrderLogController } from './tour-order-log.controller';
import { TourOrderLogService } from './tour-order-log.service';
import { TourOrderLogRepository } from './tour-order-log.repository';

@Module({
  imports: [TypeOrmModule.forFeature([TourOrderLogRepository])],
  controllers: [TourOrderLogController],
  providers: [TourOrderLogService],
  exports: [TourOrderLogService],
})
export class TourOrderLogModule { }
