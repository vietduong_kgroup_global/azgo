import { Test, TestingModule } from '@nestjs/testing';
import { TourOrderLogService } from './tour-order-log.service';

describe('TourOrderLogService', () => {
  let service: TourOrderLogService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TourOrderLogService],
    }).compile();

    service = module.get<TourOrderLogService>(TourOrderLogService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
