
import { Entity, PrimaryGeneratedColumn, Generated, JoinColumn, OneToOne, Column, BaseEntity } from 'typeorm';
import { CustomerProfileEntity } from '../customer-profile/customer-profile.entity';
import { DriverProfileEntity } from '../driver-profile/driver-profile.entity';
import { UsersEntity } from '../users/users.entity';
import { TourOrderEntity } from '../tour-order/tour-order.entity';
import { Exclude } from 'class-transformer';
import { IsInt, IsNotEmpty } from 'class-validator';

@Entity('az_tour_order_logs')
export class TourOrderLogEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    @Generated('uuid')
    @IsNotEmpty()
    uuid: string;

    @Column({
        type: 'varchar',
    })
    company: string;
    
    @Column({
        type: 'int',
        nullable: false,
    })
    @IsNotEmpty()
    user_id: number;

    @OneToOne(type => UsersEntity, user => user.tourOrderLog)
    @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
    userProfile: UsersEntity;

    @Column({
        type: 'int',
        nullable: false,
    })
    @IsNotEmpty()
    tour_order_id: number;

    @OneToOne(type => TourOrderEntity, order => order.tourOrderLog)
    @JoinColumn({ name: 'tour_order_id', referencedColumnName: 'id' })
    tourOrderProfile: TourOrderEntity;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    type: string;

    @Column({
        type: 'json',
        comment: 'Thông tin bị thay đổi',
    })
    data: object;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;
}