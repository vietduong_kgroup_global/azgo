import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityId } from 'typeorm/repository/EntityId';
import { TourOrderLogRepository } from './tour-order-log.repository';

@Injectable()
export class TourOrderLogService {
    constructor(
        @InjectRepository(TourOrderLogRepository)
        private readonly repository: TourOrderLogRepository,
    ) {
    }

    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        return await this.repository.getAllByOffsetLimit(limit, offset, options);
    }

    /**
     * This function get user by tour id
     * @param tour_id
     */
    async findByUuid(tour_uuid: string) {
        return await this.repository.findByUuid(tour_uuid);
    }

    async save(data: any) {
        // return await this.store(data);
        return await this.repository.save(data);
    }

    async updateById(id: EntityId, data: any) {
        // return await this.update(id, data);
        return await this.repository.update(id, data);
    }
}
