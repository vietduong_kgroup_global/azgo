import { Brackets, EntityRepository, Repository } from 'typeorm';
import { TourOrderLogEntity } from './tour-order-log.entity';

@EntityRepository(TourOrderLogEntity)
export class TourOrderLogRepository extends Repository<TourOrderLogEntity> {
    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     * @param options
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        let query = this.createQueryBuilder('az_tour_order_logs');
        query.leftJoinAndSelect('az_tour_order_logs.userProfile', 'userProfile');
        query.leftJoinAndSelect('userProfile.customerProfile', 'customerProfile');
        query.leftJoinAndSelect('userProfile.adminProfile', 'adminProfile'); 
        query.leftJoinAndSelect('az_tour_order_logs.tourOrderProfile', 'tourOrderProfile');
        query.leftJoinAndSelect('tourOrderProfile.tourProfile', 'tourProfile');
        // not delete_at
        query.andWhere('az_tour_order_logs.deleted_at is null');

        // search by keyword
        if (options.keyword && options.keyword.trim()) {
            query.andWhere(new Brackets(qb => {
                qb.where(`tourProfile.name like :keyword`, { keyword: '%' + options.keyword.trim() + '%' })
                    .orWhere(`tourOrderProfile.order_code like :keyword`, { keyword: '%' + options.keyword.trim() + '%' })
            }));
        }
        // filter by company
        if (!!options.company) {
            query.andWhere(`az_tour_order_logs.company = :company`, { company: options.company });
        }
        // Filter by tour order
        if (!!options.tour_order_id) {
            query.andWhere(`az_tour_order_logs.tour_order_id = :tour_order_id`, { tour_order_id: options.tour_order_id });
        }
        // Filter by date create
        if (!!options.from_date && !!options.to_date) {
            query.andWhere(`az_tour_order_logs.created_at >= :from_date`, { from_date: options.from_date });
            query.andWhere(`az_tour_order_logs.created_at <= :to_date`, { to_date: options.to_date });
        }

        // limit + offset
        query.orderBy('az_tour_order_logs.id', 'DESC')
            .limit(limit)
            .offset(offset);

        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }

    /**
     * This is function get tour by id
     * @param id
     */
    async findByUuid(uuid: string) {
        return await this.findOne({
            where: { uuid, deleted_at: null },
            relations: [
                'userProfile',
                'tourOrderProfile',
                'tourOrderProfile.tourProfile',
            ],
        });
    }

}
