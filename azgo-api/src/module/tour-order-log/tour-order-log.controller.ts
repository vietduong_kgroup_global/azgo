import { Body, Controller, Get, HttpStatus, Param, Post, Put, Delete, Req, Res, UseGuards } from '@nestjs/common';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { AuthGuard } from '@nestjs/passport';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import * as jwt from 'jsonwebtoken';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiResponse,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiUseTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import { TourOrderLogService } from './tour-order-log.service';

@ApiUseTags('Tour Order Logs')
@Controller('tour-order-log')
export class TourOrderLogController {
    constructor(
        private readonly tourOrderLogService: TourOrderLogService,
    ) {
    }
    @Get('get-list')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get list tour order log with off set limit' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'from_date', description: 'Filter Date From', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'to_date', description: 'Filter Date To', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword search by tour name and order code', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'tour_order_id', description: 'Order ID', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    async getList(@Req() request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const userInfo: any = jwt.decode(token);
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        // let query = await this.tourService.getAll({});

        try {
            request.query.company = userInfo.company;
            results = await this.tourOrderLogService.getAllByOffsetLimit(per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).
                send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
        // res.status(HttpStatus.OK).send(query);
    }

    @Get('/:tour_order_log_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Detail Log' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'tour_order_log_uuid', description: 'Log Uuid', required: true, type: 'string' })
    async detail(@Param('tour_order_log_uuid') tour_order_log_uuid, @Res() res: Response) {
        try {
            const tour = await this.tourOrderLogService.findByUuid(tour_order_log_uuid);
            if (!tour)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy tour', null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', tour));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
  
    // @Post('/')
    // @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    // @ApiBearerAuth()
    // @UseGuards(AuthGuard('jwt'))
    // @ApiOperation({ title: 'Add tour order log' })
    // @ApiResponse({ status: 200, description: 'OK' })
    // @ApiResponse({ status: 404, description: 'Not found' })
    // async create(@Body() body: any, @Res() res: Response) {
    //     try {
    //         const result = await this.tourOrderLogService.save(body);
    //         if (result) {
    //             return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
    //         }
    //         return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
    //     } catch (error) {
    //         return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
    //     }
    // }

    // @Put('/:tour_order_log_uuid')
    // @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    // @ApiBearerAuth()
    // @UseGuards(AuthGuard('jwt'))
    // @ApiOperation({ title: 'Edit tour order log' })
    // @ApiResponse({ status: 200, description: 'OK' })
    // @ApiResponse({ status: 404, description: 'Not found' })
    // @ApiImplicitParam({ name: 'tour_order_log_uuid', description: 'Log Uuid', required: true, type: 'string' })
    // async update(@Param('tour_order_log_uuid') tour_order_log_uuid, @Body() body: any, @Res() res: Response) {
    //     let tour;
    //     try {
    //         tour = await this.tourOrderLogService.findByUuid(tour_order_log_uuid);
    //         if (!tour)
    //             return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy', null));
    //     } catch (error) {
    //         return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
    //     }
    //     try {
    //         const result = await this.tourOrderLogService.updateById(tour.id, body);
    //         if (result) {
    //             return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
    //         }
    //         return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
    //     } catch (error) {
    //         return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
    //     }
    // }
}
