import { Test, TestingModule } from '@nestjs/testing';
import { TourOrderLogController } from './tour-order-log.controller';

describe('TourOrderLog Controller', () => {
  let controller: TourOrderLogController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TourOrderLogController],
    }).compile();

    controller = module.get<TourOrderLogController>(TourOrderLogController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
