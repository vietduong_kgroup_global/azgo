import { Brackets, EntityRepository, Repository } from 'typeorm';
import { TourEntity } from './tour.entity';

@EntityRepository(TourEntity)
export class TourRepository extends Repository<TourEntity> {
    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     * @param options
     */
    async getAll(options?: any) {
        let query = this.createQueryBuilder('az_tour');
        // search by keyword
        if (options.keyword && options.keyword.trim()) {
            query.orWhere(`az_tour.name like :name`, { name: '%' + options.keyword.trim() + '%' });
        }
        // Filter by number of seat
        if (!!options.number_of_seat) {
            query.andWhere(`az_tour.number_of_seat = :number_of_seat`, { number_of_seat: options.number_of_seat });
        }
        // Filter by area uuid
        try {
            if (!Array.isArray(options.area)) options.area = JSON.parse(options.area);
        } catch (error) {
            options.area = [];
        }
        if (options.area.length) {
            query.andWhere(`az_tour.area_uuid IN (:area)`, { area: options.area });
        }
        // Filter tour pickup from airport
        if (typeof options.is_from_airport !== 'undefined') {
            query.andWhere(`az_tour.is_from_airport = :is_from_airport`, { is_from_airport: options.is_from_airport });
        }
        // Filter by price tour
        if (!!options.from_price && !!options.to_price) {
            query.andWhere(`az_tour.tour_price >= :from_price`, { from_price: options.from_price });
            query.andWhere(`az_tour.tour_price <= :to_price`, { to_price: options.to_price });
        }
        // filter by area_uuid in token
        if (options.area_uuid) {
            query.andWhere(`az_tour.area_uuid = :area_uuid`, { area_uuid: options.area_uuid });
        }
        // filter by company
        if (!!options.company) {
            query.andWhere(`az_tour.company = :company`, { company: options.company });
        }
        // Filter by time tour
        if (!!options.from_time && !!options.to_time) {
            query.andWhere(`az_tour.time >= :from_time`, { from_time: options.from_time });
            query.andWhere(`az_tour.time <= :to_time`, { to_time: options.to_time });
        }
        // not delete_at
        query.andWhere('az_tour.deleted_at is null');

        // limit + offset
        query.orderBy('az_tour.id', 'DESC');

        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }
    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     * @param options
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        let query = this.createQueryBuilder('az_tour');
        query.leftJoinAndSelect('az_tour.vehicleType', 'vehicleType');
        query.leftJoinAndSelect('az_tour.areaInfo', 'areaInfo');
        // search by keyword
        if (options.keyword && options.keyword.trim()) {
            query.orWhere(`az_tour.name like :name`, { name: '%' + options.keyword.trim() + '%' });
        }
        // Filter by number of seat
        if (!!options.number_of_seat) {
            query.andWhere(`az_tour.number_of_seat = :number_of_seat`, { number_of_seat: options.number_of_seat });
        }
        // Filter by vehicle type
        if (!!options.vehicle_type_id) {
            query.andWhere(`az_tour.vehicle_type_id = :vehicle_type_id`, { vehicle_type_id: options.vehicle_type_id });
        }
        // Filter by type tour
        if (!!options.type) {
            query.andWhere(`az_tour.type = :type`, { type: options.type });
        }
        // Filter by area uuid
        try {
            if (!Array.isArray(options.area)) options.area = JSON.parse(options.area);
        } catch (error) {
            options.area = [];
        }
        if (options.area.length) {
            query.andWhere(`az_tour.area_uuid IN (:area)`, { area: options.area });
        }
        // Filter tour hiden in mobile
        if (typeof options.hide_mobile !== 'undefined') {
            query.andWhere(`az_tour.hide_mobile = :hide_mobile`, { hide_mobile: options.hide_mobile });
        }
        // Filter tour pickup from airport
        if (typeof options.is_from_airport !== 'undefined') {
            query.andWhere(`az_tour.is_from_airport = :is_from_airport`, { is_from_airport: options.is_from_airport });
        }
        // Filter tour pickup from company 
        if (typeof options.is_from_company !== 'undefined' && options.company) {
            query.andWhere(`JSON_CONTAINS(company, '"${options.company}"') <> 0`);
        }
        try {
            options.company = JSON.parse(options.company);
        } catch (error) {
            options.company = [];
        }
        if (options.company.length) {
            options.company = options.company.map((e) => `"${e}"`)
            query.andWhere(`JSON_CONTAINS(company, '${options.company}') <> 0`);
        }
        // filter by area_uuid in token
        if (options.area_uuid) {
            query.andWhere(`az_tour.area_uuid = :area_uuid`, { area_uuid: options.area_uuid });
        }
        // filter by company
        // if (!!options.company) {
        //     query.andWhere(`az_tour.company = :company`, { company: options.company });
        // }

        // filter distance tour
        if (options.distance_rate && options.distance_rate === 'false') {
            query.andWhere(`az_tour.distance_rate = 0`);
        }
        // Filter by price tour
        if (!!options.from_price && !!options.to_price) {
            if (!!options.type && options.type === 'distance_price') {
                query.andWhere(`az_tour.distance_rate >= :from_price`, { from_price: options.from_price });
                query.andWhere(`az_tour.distance_rate <= :to_price`, { to_price: options.to_price });
            }
            else if (!!options.type && options.type === 'time_price') {
                query.andWhere(`az_tour.time_rate >= :from_price`, { from_price: options.from_price });
                query.andWhere(`az_tour.time_rate <= :to_price`, { to_price: options.to_price });
            }
            else {
                query.andWhere(`az_tour.tour_price >= :from_price`, { from_price: options.from_price });
                query.andWhere(`az_tour.tour_price <= :to_price`, { to_price: options.to_price });
            }
        }
        // Filter by time tour
        if (!!options.from_time && !!options.to_time) {
            query.andWhere(`az_tour.time >= :from_time`, { from_time: options.from_time });
            query.andWhere(`az_tour.time <= :to_time`, { to_time: options.to_time });
        }
        // not delete_at
        query.andWhere('az_tour.deleted_at is null');
        // limit + offset
        query.orderBy('az_tour.id', 'DESC')
            .limit(limit)
            .offset(offset);

        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }

    /**
     * This is function get tour by id
     * @param id
     */
    async findById(id: number) {
        return await this.findOne({
            where: { id, deleted_at: null },
            relations: [
                'vehicleType'
            ]
        });
    }

    /**
     * This is function get tour by id
     * @param id
     */
    async findByUuid(uuid: string) {
        return await this.findOne({
            where: { uuid: uuid, deleted_at: null },
            relations: [
                'vehicleType',
                'areaInfo'
            ]
        });
    }

}
