
import {Entity, PrimaryGeneratedColumn, Generated, Column, JoinColumn, BaseEntity, OneToMany, ManyToOne} from 'typeorm';
import {Exclude} from 'class-transformer';
import {IsInt, IsNotEmpty} from 'class-validator';
import {TourOrderEntity} from '../tour-order/tour-order.entity';
import {VehicleTypesEntity} from '../vehicle-types/vehicle-types.entity';
import { AreaEntity } from '../area/area.entity';

@Entity('az_tour')
export class TourEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    @Generated('uuid')
    @IsNotEmpty()
    uuid: string;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    @IsNotEmpty()
    name: string;

    @Column()
    @IsNotEmpty()
    area_uuid: string;
    @ManyToOne(type => AreaEntity, area => area.tourList)
    @JoinColumn({ name: 'area_uuid', referencedColumnName: 'uuid' })
    areaInfo: AreaEntity;

    @Column({
        type: 'int',
    })
    @IsInt()
    time: number;

    @Column({
        type: 'varchar',
        default: 'fix_price',
        comment: 'Kieu tour',
    })
    type: string;

    @Column({
        type: 'int',
        nullable: false,
    })
    @IsInt()
    @IsNotEmpty()
    number_of_seat: number;

    @Column({
        type: 'int',
    })
    tour_price: number;

    @Column({
        type: 'int',
    })
    distance_rate: number;

    @Column({
        type: 'int',
    })
    time_rate: number;

    @Column()
    vehicle_type_id: number;

    @ManyToOne(type => VehicleTypesEntity, vehiclesType => vehiclesType.listTour)
    @JoinColumn({ name: 'vehicle_type_id', referencedColumnName: 'id' })
    vehicleType: VehicleTypesEntity;

    @Column({
        type: 'json',
        nullable: false,
    })
    stations: object;

    @Column()
    extra_time_price: number;

    @Column({
        type: 'json',
        nullable: true,
    })
    additional_fee: object;

    @Column()
    description: string;

    @Column({
        type: 'json',
        nullable: false,
    })
    company: object;
    
    @Column()
    is_round_trip: number;

    @Column()
    is_from_airport: number;

    @Column({
        type: 'smallint',
        default: 0,
        comment: 'Tour nổi bật (1: Có , 0: Không)',
    })
    is_feature: number;

    @Column({
        type: 'smallint',
        default: 0,
        comment: 'An tren dien thoai (1: Có , 0: Không)',
    })
    hide_mobile: number;

    @Column({
        type: 'json',
        nullable: false,
    })
    image: object;

    @Column({
        type: 'json',
        nullable: false,
    })
    gallery: object;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @OneToMany(type => TourOrderEntity, tour_order => tour_order.tourProfile)
    tourInfo: TourOrderEntity[];

}