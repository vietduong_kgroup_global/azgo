import {ApiModelProperty} from '@nestjs/swagger';
import {IsInt, IsNotEmpty, Validate} from 'class-validator';

export class UpdateTourDto {
    @ApiModelProperty({ example: 'Tour Example', required: false })
    name: string;

    @ApiModelProperty({ example: 'company', required: false })
    company: string;
    
    @ApiModelProperty({ example: 1, required: false })
    area_uuid: string;

    @ApiModelProperty({ example: 'fix_price', required: false, description: "Kiểu tour [fix_price, distance_price, time_price]"  })
    type: string;

    @ApiModelProperty({ example: 180, required: false, description: "Thời gian tour theo phút" })
    time: number;

    @ApiModelProperty({ example: 4, required: false })
    number_of_seat: number;

    @ApiModelProperty({ example: 100000, required: false , description: "Giá tour" })
    tour_price: number;

    @ApiModelProperty({ example: 50000, required: false, description: "Gía tính theo km" })
    distance_rate: number;

    @ApiModelProperty({ example: 50000, required: false, description: "Gía tính theo ngày" })
    time_rate: number;

    @ApiModelProperty({ example: 1, required: false, description: "Loại xe khi chọn tour thuê xe" })
    vehicle_type_id: number;

    @ApiModelProperty({ example: [{ "lat": 1090398237, "long": 12098921839, "name": "a", "description": "text" }], required: false , description: "Các trạm ghé trong tour" })
    stations: object;

    @ApiModelProperty({ example: 180, required: false, description: "Tiền phụ thu theo giờ" })
    extra_time_price: number;

    @ApiModelProperty({ example: [{}], required: false, description: "Mã phí phụ thu" })
    additional_fee: object;

    @ApiModelProperty({ example: "Mô tả", required: false })
    description: string;

    @ApiModelProperty({ example: 1, required: false, description: "1: có, 0: không" })
    is_round_trip: number;

    @ApiModelProperty({ example: 1, required: false, description: "1: có, 0: không" })
    is_from_airport: number;

    @ApiModelProperty({ example: 1, required: false, description: "1: có, 0: không" })
    is_feature: number;

    @ApiModelProperty({ example: {}, required: false, description: "Hình đại diện" })
    image: object;

    @ApiModelProperty({ example: [], required: false, description: "Thư viện ảnh" })
    gallery: object;
}
