import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import {CoordinatesDto} from '../../../general-dtos/coordinates.dto';
export class DistanceDto {
    @IsNotEmpty()
    @ApiModelProperty({ example: { lat: '10.759416', lng: '106.657959' }, type: CoordinatesDto })
    origins: CoordinatesDto;
    @IsNotEmpty()
    @ApiModelProperty({ example: { lat: '10.046410', lng: '105.757045' }, type: CoordinatesDto })
    destinations: CoordinatesDto;
    @IsNotEmpty()
    @ApiModelProperty({ example: 'driving | walking | bicycling | transit', default: 'driving' })
    mode?: string;
}
