import { Injectable } from '@nestjs/common';
import { EntityId } from 'typeorm/repository/EntityId';
import {TourEntity} from './tour.entity';
import {CreateTourDto} from './dto/create-tour.dto';
import {UpdateTourDto} from './dto/update-tour.dto';
import {TourRepository} from './tour.repository';
import { BaseService } from '../../base.service';
import {
    ERROR_UNKNOWN,
} from '../../constants/error.const';
@Injectable()
export class TourService extends BaseService<TourEntity, TourRepository> {
    constructor(repository: TourRepository ) {
        super(repository); 
} 
    // async getAll(options?: any) {
    //    return this.index();
    // }
    /**
     * This function get user by tour id
     * @param tour_id
     */
    async findOne(tour_id: number){
        return await this.repository.findById(tour_id);
    }
    /**
     * This function get user by tour id
     * @param tour_id
     */
    async findByUuid(tour_uuid: string){
        return await this.repository.findByUuid(tour_uuid);
    }

    /**
     * This function get all tour
     */
    async getAll(options?: any) {
        return await this.repository.getAll(options);
    }

    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        return await this.repository.getAllByOffsetLimit(limit, offset, options);
    }

    async save(data: CreateTourDto){
        return await this.store(data);
    }

    async updateById(id: EntityId,data: UpdateTourDto){
        return await this.update(id, data);
    }

    /**
     * This is function delete user
     * @param user
     */
    async deleteTour(tour: TourEntity){
        try {
            await this.update(tour.id, {deleted_at: new Date()});
            return {
                status: true,
                data: null,
                error: null,
                message: null
            };
        } catch (error) {
            console.log("error deleteTour: ", error);
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message || 'Error in count order fee exist'
            };
        }
    }

     getTotalFeeAndFilterAdditionFee(tour: TourEntity,tourOriginalPrice:number){
        let totalFee=0;
        let activeAdditionalFee=[];
        if(tour.additional_fee){
            for(let i in tour.additional_fee) {
                let fee=tour.additional_fee[i];
                //If start is null
                if(!fee.start){
                    //If end is null
                    if(!fee.end){
                        activeAdditionalFee.push(fee);
                        if(fee.additional_fee.type=="flat"){
                                totalFee=totalFee+fee.additional_fee.flat;
                        }else{
                                totalFee=totalFee+fee.additional_fee.percent*tourOriginalPrice;
                        }
                    }else{
                    //If end is not null    
                        let endDate=new Date(fee.end).getTime();
                        let now=new Date().getTime();
                        if(now<=endDate){
                            activeAdditionalFee.push(fee);
                            if(fee.additional_fee.type=="flat"){
                                totalFee=totalFee+fee.additional_fee.flat;
                            }else{
                                totalFee=totalFee+fee.additional_fee.percent*tourOriginalPrice;
                            }
                        }
                    }

                }else{
                //If start is not null    
                    if(!fee.end){
                    //If end is null    
                        let startDate=new Date(fee.start).getTime();
                        let now=new Date().getTime();
                        if(startDate<=now){
                            activeAdditionalFee.push(fee);
                            if(fee.additional_fee.type=="flat"){
                                totalFee=totalFee+fee.additional_fee.flat;
                            }else{
                                totalFee=totalFee+fee.additional_fee.percent*tourOriginalPrice;
                            }
                        }
                    }else{
                        let startDate=new Date(fee.start).getTime();
                        let endDate=new Date(fee.end).getTime();
                        let now=new Date().getTime();
                        if(startDate<=now && now<=endDate){
                            activeAdditionalFee.push(fee);
                            if(fee.additional_fee.type=="flat"){
                                totalFee=totalFee+fee.additional_fee.flat;
                            }else{
                                totalFee=totalFee+fee.additional_fee.percent*tourOriginalPrice;
                            }
                        }
                    }
                }

            }
        }
        tour.additional_fee=activeAdditionalFee;
        return totalFee;
    }

}
