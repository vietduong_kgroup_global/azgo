import { Body, Controller, Get, HttpStatus, Param, Post, Put, Delete, Req, Res, UseGuards } from '@nestjs/common';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { AuthGuard } from '@nestjs/passport';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import * as jwt from 'jsonwebtoken';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiResponse,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiUseTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import { CreateTourDto } from './dto/create-tour.dto';
import { UpdateTourDto } from './dto/update-tour.dto';
import { DistanceDto } from './dto/distance.dto';
import { TourService } from './tour.service';
import { ERROR_TOUR_NO_RATE_DISTANCE, ERROR_TOUR_CAN_NOT_CAL_DISTANCE, ERROR_TOUR_CAN_NOT_FIND_DISTANCE_ON_GG } from '../../constants/error.const';
import { GoogleMapService } from '../google-map/google-map.service';
import { SettingSystemService } from '../setting-system/setting-system.service';
import { roundKmFromDistance, roundUpPriceToThousand } from '../../helpers/utils';
import {
    ERROR_UNKNOWN,
} from '../../constants/error.const';
@ApiUseTags('Tours')
@Controller('tour')
export class TourController {
    constructor(
        private readonly tourService: TourService,
        private readonly googleMapService: GoogleMapService,
        private readonly settingSystemService: SettingSystemService
        // private readonly bikeCarDriverService: BikeCarDriverService,
    ) {
    }
    @Get('get-all-list')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get list tour without offset limit' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword search by name tour', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'type', description: 'Filter type tour [fix_price, distance_price, time_price]', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'vehicle_type_id', description: 'Filter tour by vehicle type', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'number_of_seat', description: 'Number of seat', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'area', description: 'Filter by area (uuid)', required: false, type: 'string[]' })
    @ApiImplicitQuery({ name: 'is_from_airport', description: 'Tour pickup from airport', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'hide_mobile', description: 'Hide tour in mobile', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'from_price', description: 'Filter Price From', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'to_price', description: 'Filter Price To', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'from_time', description: 'Filter Time From', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'to_time', description: 'Filter Time To', required: false, type: 'number' })
    async getALlListTour(@Req() request, @Res() res: Response) {
        let results;
        try {
            results = await this.tourService.getAll(request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }
        return res.status(HttpStatus.OK).send(dataSuccess('oK', results.data));
    }

    @Get('get-list-tour')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get list tour with offset limit' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword search by name tour', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'type', description: 'Filter type tour [fix_price, distance_price, time_price]', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'vehicle_type_id', description: 'Filter type tour vehicle type', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'number_of_seat', description: 'Number of seat', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'area', description: 'Filter by area (uuid)', required: false, type: 'string[]' })
    @ApiImplicitQuery({ name: 'company', description: 'Filter by company', required: false, type: 'string[]' })
    @ApiImplicitQuery({ name: 'is_from_airport', description: 'Tour pickup from airport', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'hide_mobile', description: 'Hide tour in mobile', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'from_price', description: 'Filter Price From', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'to_price', description: 'Filter Price To', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'from_time', description: 'Filter Time From', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'to_time', description: 'Filter Time To', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    async getListTour(@Req() request, @Res() res: Response) {
        const token = request.headers.authorization.split(' ')[1];
        const userInfo: any = jwt.decode(token);
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        // let query = await this.tourService.getAll({});

        try {
            request.query.company = request.query.company || userInfo.company || '';
            request.query.area_uuid = userInfo.area_uuid || '';
            results = await this.tourService.getAllByOffsetLimit(per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).
                send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
        // res.status(HttpStatus.OK).send(query);
    }

    @Get('/:tour_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Detail tour' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'tour_uuid', description: 'Tour Uuid', required: true, type: 'string' })
    async detail(@Req() request, @Param('tour_uuid') tour_uuid, @Res() res: Response) {
        try {
            // const token = request.headers.authorization.split(' ')[1];
            // const userInfo: any =  jwt.decode(token);
            const tour = await this.tourService.findByUuid(tour_uuid);
            if (!tour)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy tour', null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', tour));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/get-price-tour')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get Price Tour' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async getPriceTour(@Req() request, @Body() body: DistanceDto, @Res() res: Response) {
        try {
            let tourUUID = request.query.tour_uuid;
            if (!tourUUID) {
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy param tourUUID', null));
            }
            const tour = await this.tourService.findByUuid(tourUUID);
            if (tour) {
                if (tour.distance_rate) {
                    const distance = await this.googleMapService.getAzgoDistance(body);
                    if(distance){
                        if(distance.rows && distance.rows[0] && distance.rows[0].elements && distance.rows[0].elements[0] && distance.rows[0].elements[0].distance){
                                let data: any={};
                                data.unit="km";
                                data.currency="vnd";
                                data.distance=roundKmFromDistance(distance.rows[0].elements[0].distance.value);
                                data.distance_rate=tour.distance_rate;
                                data.price=tour.distance_rate*data.distance;
                                data.originalPrice = roundUpPriceToThousand(data.price, "vnd");
                                let totalAdditionalFee=this.tourService.getTotalFeeAndFilterAdditionFee(tour,data.price);
                                data.totalAdditionalFee = roundUpPriceToThousand(totalAdditionalFee, "vnd");
                                data.price=data.originalPrice + data.totalAdditionalFee;
                                data.price = roundUpPriceToThousand(data.price, "vnd");
                                data.tour_uuid=tourUUID;
                                data.tour=tour;
                                return res.status(HttpStatus.OK).send(dataSuccess('OK', data));
                        }else{
                            return res.status(HttpStatus.NOT_FOUND).send(dataError('Google không tính được khoản cách', null,ERROR_TOUR_CAN_NOT_FIND_DISTANCE_ON_GG));
                        
                        }
                    } else {
                        return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy tour có giá theo km', null, ERROR_TOUR_CAN_NOT_CAL_DISTANCE));
                    }

                } else {
                    return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy tour có giá theo km', null, ERROR_TOUR_NO_RATE_DISTANCE));
                }
            } else {
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy tour', null));
            }
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Create tour' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async createTour(@Req() request, @Body() body: CreateTourDto, @Res() res: Response) {
        try {
            const token = request.headers.authorization.split(' ')[1];
            const userInfo: any = jwt.decode(token);
            // body.company = userInfo.company || '';
            const result = await this.tourService.save(body);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:tour_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Edit tour' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'tour_uuid', description: 'Tour Uuid', required: true, type: 'string' })
    async updateTour(@Param('tour_uuid') tour_uuid, @Body() body: UpdateTourDto, @Res() res: Response) {
        let tour;
        try {
            tour = await this.tourService.findByUuid(tour_uuid);
            if (!tour)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy tour', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
        try {
            const result = await this.tourService.updateById(tour.id, body);
            if (result) {
                return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('Error', null));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:tour_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete tour' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'tour_uuid', description: 'Tour Uuid', required: true, type: 'string' })
    async deleteUserAccount(@Param('tour_uuid') tour_uuid, @Req() request, @Res() res: Response) {
        let tour;
        try {
            tour = await this.tourService.findByUuid(tour_uuid);
            if (!tour)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy tour', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
        try {
            let result = await this.tourService.deleteTour(tour);
            if (!result.status) return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Xoá tour bị lỗi', null, result.error || ERROR_UNKNOWN));
            return res.status(HttpStatus.OK).send(dataSuccess('Xóa tour thành công', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
