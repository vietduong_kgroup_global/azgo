import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TourService } from './tour.service';
import { TourController } from './tour.controller';
import { TourRepository } from './tour.repository';
import { GoogleMapService } from '../google-map/google-map.service';
import { SettingSystemService } from '../setting-system/setting-system.service';
import { SettingSystemModule } from '../setting-system/setting-system.module';
import { SettingSystemRepository } from '../setting-system/setting-system.repository';
@Module({
  imports: [TypeOrmModule.forFeature([TourRepository]),TypeOrmModule.forFeature([SettingSystemRepository])],
  controllers: [TourController],
  providers: [TourService, GoogleMapService,SettingSystemService],
  exports: [TourService],
})
export class TourModule { }
