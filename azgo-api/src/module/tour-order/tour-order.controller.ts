import { Body, Controller, Get, HttpStatus, Param, Post, Put, Delete, Req, Res, UseGuards } from '@nestjs/common';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { AuthGuard } from '@nestjs/passport';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiResponse,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiUseTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import * as jwt from 'jsonwebtoken';
import { TourOrderService } from './tour-order.service';
import { CreateTourOrderDto } from './dto/create-tour-order.dto';
import { UpdateTourOrderDto } from './dto/update-tour-order.dto';
import { WalletService } from '../wallet/wallet.service';
import { PromotionsService } from '../promotions/promotions.service';
import { TourService } from '../tour/tour.service';
import { UsersService } from '../users/users.service';
import { SlackAlertService } from '../slack-alert/slack-alert.service';
import {
    ERROR_PROMOTION_NOT_FOUND,
    ERROR_TOUR_ORDER_NOT_FOUND,
    ERROR_USER_NOT_PERMISSION_UPDATE_ORDER,
    ERROR_TOUR_ID_UUID_CANNOT_EMPTY,
    ERROR_TOUR_NOT_FOUND,
    ERROR_DELETE_TOUR_ORDER,
    ERROR_UNKNOWN,
    ERROR_WALLET_ORDER_CODE_IS_NOT_EXIST,
    ERROR_CUSTOMER_CAN_NOT_UPDATE_ORDER,
    ERROR_REPORT_ORDER,
    ERROR_SEND_SMS_IN_TOUR_ORDER,
} from '../../constants/error.const';  

import {
    STATUS_TOUR_ARR, STATUS_TOUR_LABEL_ARRAY
} from '../../constants/secrets.const';
import { roundUpPriceToThousand, changeFormatDate, formatDateByTimeZone } from '../../helpers/utils';
@ApiUseTags('Tour Orders')
@Controller('tour-order')
export class TourOrderController {
    constructor(
        private readonly tourOrderService: TourOrderService,
        private readonly walletService: WalletService,
        private promotionsService: PromotionsService,
        private readonly tourService: TourService,
        private readonly slackAlertService: SlackAlertService,
        private usersService: UsersService,
    ) {
    }
    @Get('get-list')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get list tour order with off set limit' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'sort', description: 'Descend or Ascend', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'sort_by', description: 'Sort field (ex: created_at)', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'distance_rate', description: 'true: get all, false: get tour distance_rate=0', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'start_time', description: 'Filter from start_time to now', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'from_date', description: 'Filter Date From', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'to_date', description: 'Filter Date To', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'from_price', description: 'Filter Price From', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'to_price', description: 'Filter Price To', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword search by orde code and tour name', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'status', description: 'Status Order', required: false, type: 'json' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'number_of_seat', description: 'Number of seat', required: false, type: 'number' })
    async getListTour(@Req() request, @Res() res: Response) {
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        try {
            request.query.company = data_user.company || '';
            request.query.area_uuid = data_user.area_uuid || '';
            results = await this.tourOrderService.getAllByOffsetLimit(per_page, offset, request.query);
        } catch (e) {
            console.log(e)
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }
        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).
                send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
        // res.status(HttpStatus.OK).send(query);
    }
    @Get('revenue_order_report')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get list tour order with off set limit' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'from', description: 'Filter Date From', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'to', description: 'Filter Date To', required: false, type: 'date' }) 
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    async revenueOrderReport(@Req() request, @Res() res: Response) { 
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try { 
            results = await this.tourOrderService.revenueOrderReport(per_page, offset, request.query);
            if (results && results.status) {
                results = results.data;
                let options = {
                    current_page: page,
                    next_page: page,
                    total_page: Math.ceil(results.total / per_page),
                };
                options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;
        
                if (options.total_page > 0 && options.next_page > options.total_page) {
                    return res.status(HttpStatus.BAD_REQUEST).
                        send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
                }
                return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options)); 
            }
            else return res.status(HttpStatus.BAD_REQUEST).send(dataError((results.message) || 'Lỗi báo cáo chuyến xe HT', null, results.error || ERROR_REPORT_ORDER)) 
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

       
    }

    @Get('/export_revenue_order_report')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'revenueOrderReport' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'from', description: 'Filter Date From', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'to', description: 'Filter Date To', required: false, type: 'date' })   
    @ApiImplicitQuery({ name: 'timeZone', description: 'timeZone', required: false, type: 'number' })  
    async exportRevenueOrderReport(@Req() request, @Res() res: Response) {  
        try {  
            let results;
            results = await this.tourOrderService.exportRevenueOrderReport(request.query); 
            if (results && results.status) { 
                let xl = require('excel4node');
                let wb = new xl.Workbook();
                let ws = wb.addWorksheet('BC Chi tiết chuyến xe HT'); 

                let data = [];
            
                (results.data || []).map((item, index) => {
                    let timeZone = request.query.time_zone
                    let numberIndex = index  + 1
                    let resultStatus = STATUS_TOUR_LABEL_ARRAY.find((e) => e.value === item.status)  
                    let verhicleList = item.driverTour ? item.driverTour.vehicles : []
                    let number_of_seat = item.tourProfile && item.tourProfile.number_of_seat; 
                    // let vehicleName = "";
                    let vehicleName = `Xe ${number_of_seat} chổ `;
                    let customer_name = "";
                    verhicleList.map((verhicle) => {
                        if (verhicle.vehicleType) {
                            if (number_of_seat && verhicle.vehicleType.total_seat == number_of_seat) return vehicleName = `Xe ${verhicle.vehicleType.total_seat} chổ `
                        }
                    });
                    if(!!item.customer_first_name && !!item.customer_last_name){
                        customer_name = `${item.customer_last_name} ${item.customer_first_name}`
                      }
                    else if(!!item.customerTour && !!item.customerTour.customerProfile){
                        customer_name =  `${item.customerTour.customerProfile.full_name}`
                    }
                    data.push({
                        "STT": numberIndex,
                        "Họ và tên khách hàng": customer_name,
                        "Loại khách hàng": "",
                        "Điện thoại khách hàng": (item.driverTour && item.driverTour.phone) || "",
                        "Mã tài xế": "",
                        "Hình thức hoạt động của tài xế": "",
                        "Họ và tên Tài xế": (item.driverTour && item.driverTour.driverProfile && item.driverTour.driverProfile.full_name) || "",
                        "Điện thoại Tài xế": (item.driverTour && item.driverTour.phone) || "",
                        "Loại xe": vehicleName || "",
                        "Thời gian khách đặt chuyến": formatDateByTimeZone(item.created_at, timeZone) || "",
                        "Thời gian bắt đầu thực hiện chuyến xe": formatDateByTimeZone(item.start_time, timeZone) || "",
                        "Thời gian kết thúc chuyến xe": formatDateByTimeZone(item.finish_time, timeZone) || "",
                        "Mã chuyến xe": item.order_code || "",
                        "Điểm đón": item.address_pick || "",
                        "Điểm đến": item.address_destination || "",
                        "Tổng giá trị chuyến xe ước tính": (item.order_price && item.order_price >= 0 ) ? item.order_price : 0,
                        "Mã khuyến mãi": "", 
                        "Tổng tiền khuyến mãi": "", 
                        "Tình trạng chuyến xe": resultStatus ? resultStatus.label : "",
                        "Ghi chú": "",  
                    });
                });
                const headingColumnNames = [
                    "STT", 
                    "Họ và tên khách hàng",
                    "Loại khách hàng",
                    "Điện thoại khách hàng",
                    "Mã tài xế",
                    "Hình thức hoạt động của tài xế",
                    "Họ và tên Tài xế",
                    "Điện thoại Tài xế", 
                    "Loại xe",
                    "Thời gian khách đặt chuyến",
                    "Thời gian bắt đầu thực hiện chuyến xe",
                    "Thời gian kết thúc chuyến xe",
                    "Mã chuyến xe",
                    "Điểm đón",
                    "Điểm đến",
                    "Tổng giá trị chuyến xe ước tính",
                    "Mã khuyến mãi", 
                    "Tổng tiền khuyến mãi", 
                    "Tình trạng chuyến xe", 
                    "Ghi chú", 
                ]
                let from = request.query.from ? changeFormatDate(request.query.from) : "";
                let to = request.query.to ?  changeFormatDate(request.query.to) : "";
                ws.cell(1, 1)
                .string('CÔNG TY CỔ PHẦN AZGO').style({font: {name: 'Times New Roman', size: 12, bold: true}})
                ws.cell(2, 1)
                .string((from && to) ? `BÁO CÁO CHI TIẾT CHUYẾN XE VÀ TIỀN THU CHUYẾN XE ${from} ĐẾN ${to}` : 'BÁO CÁO CHI TIẾT CHUYẾN XE VÀ TIỀN THU CHUYẾN XE').style({font: {name: 'Times New Roman', size: 16, bold: true}});
                ws.cell(3, 1)
                .string(``).style({font: {name: 'Times New Roman', size: 11, bold: false}})
                // Write Column Title in Excel file
                let headingColumnIndex = 1;
                headingColumnNames.forEach(heading => {
                    ws.cell(4, headingColumnIndex++)
                        .string(heading).style({font: {name: 'Times New Roman', size: 11, bold: true},  alignment: {wrapText: true, vertical: 'top'}});
                });

                // Write Data in Excel file
                let rowIndex = 5;
                data.forEach( record => {
                    let columnIndex = 1;
                    Object.keys (record).forEach(columnName => {
                        if (columnIndex === 1 || columnIndex === 16)
                            ws.cell(rowIndex,columnIndex++)
                            .number(record [columnName]).style({font: {name: 'Times New Roman', size: 11}})
                        // else if(columnIndex === 14 || columnIndex === 15) 
                        //     ws.cell(rowIndex,columnIndex++)
                        //     .string(record [columnName]).style({font: {name: 'Times New Roman', size: 11} ,  alignment: {wrapText: true, vertical: 'top'}})
                        else 
                            ws.cell(rowIndex,columnIndex++)
                                .string(record [columnName]).style({font: {name: 'Times New Roman', size: 11}})
                    });
                    rowIndex++;
                });  
                ws.column(1).setWidth(10);
                ws.column(2).setWidth(50);
                ws.column(3).setWidth(20);
                ws.column(4).setWidth(30);
                ws.column(5).setWidth(20);
                ws.column(6).setWidth(30);
                ws.column(7).setWidth(30);
                ws.column(8).setWidth(20);
                ws.column(9).setWidth(30);
                ws.column(10).setWidth(25);
                ws.column(11).setWidth(25);
                ws.column(12).setWidth(25);
                ws.column(13).setWidth(20);
                ws.column(14).setWidth(30);
                ws.column(15).setWidth(20);
                ws.column(16).setWidth(30);
                ws.column(17).setWidth(20);
                ws.column(18).setWidth(30);
                ws.column(19).setWidth(20);
                ws.column(20).setWidth(50);
                wb.write('Report.xlsx', res);  
                // return res.status(HttpStatus.OK).send(dataSuccess('oK', res));
                return res; 
            }
            else return null;
        } catch (e) {
            console.log(e)
            return null;
        }
    }

    @Get('driver_get-list')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get list tour order with off set limit' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'from_date', description: 'Filter Date From', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'to_date', description: 'Filter Date To', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'distance_rate', description: 'true: get all, false: get tour distance_rate=0', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'from_price', description: 'Filter Price From', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'to_price', description: 'Filter Price To', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword search by orde code and tour name', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'status', description: 'Status Order', required: false, type: 'json' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    async getListTourForDriver(@Req() request, @Res() res: Response) {
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            request.query.company = data_user.company || '';
            request.query.driver_id = data_user.id;
            results = await this.tourOrderService.getAllByOffsetLimit(per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).
                send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
        // res.status(HttpStatus.OK).send(query);
    }

    @Get('/:area_uuid/get-list')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get list tour order with off set limit' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'from_date', description: 'Filter Date From', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'to_date', description: 'Filter Date To', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'distance_rate', description: 'true: get all, false: get tour distance_rate=0', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'from_price', description: 'Filter Price From', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'to_price', description: 'Filter Price To', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword search by orde code and tour name', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'status', description: 'Status Order', required: false, type: 'json' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    async getListTourForDriverArea(@Param('area_uuid') area_uuid, @Req() request, @Res() res: Response) {
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;
        try {
            request.query.company = data_user.company || '';
            request.query.area_uuid = area_uuid || '';
            results = await this.tourOrderService.getAllByOffsetLimit(per_page, offset, request.query);
        } catch (e) {
            console.log(e)
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }
        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).
                send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
        // res.status(HttpStatus.OK).send(query);
    }

    @Get('customer_get-list')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Get list tour order with off set limit' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'from_date', description: 'Filter Date From', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'to_date', description: 'Filter Date To', required: false, type: 'date' })
    @ApiImplicitQuery({ name: 'distance_rate', description: 'true: get all, false: get tour distance_rate=0', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'from_price', description: 'Filter Price From', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'to_price', description: 'Filter Price To', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword search by orde code and tour name', required: false, type: 'string' })
    @ApiImplicitQuery({ name: 'status', description: 'Status Order', required: false, type: 'json' })
    @ApiImplicitQuery({ name: 'per_page', description: 'Number transactions per page', required: false, type: 'number' })
    @ApiImplicitQuery({ name: 'page', description: 'Page to query', required: false, type: 'number' })
    async getListTourForCustomer(@Req() request, @Res() res: Response) {
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }
        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            request.query.company = data_user.company || '';
            request.query.customer_id = data_user.id;
            results = await this.tourOrderService.getAllByOffsetLimit(per_page, offset, request.query);
        } catch (e) {
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Lỗi', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).
                send(dataError('Trang hiện tại không được lớn hơn tổng số trang', null));
        }
        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
        // res.status(HttpStatus.OK).send(query);
    }

    @Get('/:tour_order_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Detail tour order' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'tour_order_uuid', description: 'Order Uuid', required: true, type: 'string' })
    async detail(@Param('tour_order_uuid') tour_order_uuid, @Res() res: Response) {
        try {
            const tour = await this.tourOrderService.findByUuid(tour_order_uuid);
            if (!tour)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy tour', null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', tour));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
    @Post('/sendSMS')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'sendSMS to driver or customer in tour order' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    // @ApiImplicitParam({ name: 'tour_order_uuid', description: 'Order Uuid', required: true, type: 'string' })
    async sendSMS(@Body() body: any, @Req() request, @Res() res: Response) {
        try {
            let result = await this.tourOrderService.sendSMS(body); 
            if (result && result.status)
                return res.status(HttpStatus.OK).send(dataSuccess('oK', null)); 
            else
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Lỗi khi gửi SMS', null, result.error || ERROR_SEND_SMS_IN_TOUR_ORDER));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null, ERROR_SEND_SMS_IN_TOUR_ORDER));
        }
    }
    @Post('/')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Create tour order' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async createTourOrder(@Body() body: any, @Req() request, @Res() res: Response) {
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        data_user.current_role = data_user.current_role.toLowerCase();
        try {
            body.company = data_user.company || '';
            if (body.promotion_code) {
                body.promotion_code = body.promotion_code.toLowerCase();
                let promotion = await this.promotionsService.checkValidateByCoupon(body.promotion_code)
                if (!promotion) return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy mã khuyến mãi trong hệ thống', null, ERROR_PROMOTION_NOT_FOUND));
            }
            if (!body.tour_id) {
                if (!body.tour_uuid) return res.status(HttpStatus.NOT_FOUND).send(dataError('Tour id và tour uuid không thể cùng thiếu', null, ERROR_TOUR_ID_UUID_CANNOT_EMPTY));
                let tour = await this.tourService.findByUuid(body.tour_uuid)
                if (!tour) return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy tour', null, ERROR_TOUR_NOT_FOUND));
                body.tour_id = tour.id
            }
            // Calculate price prder
            let res_calc_price = await this.tourOrderService.calcOrderTourPrice(body);
            if (!res_calc_price.status) return res.status(HttpStatus.BAD_REQUEST).send(dataError(res_calc_price.message || 'Tạo đơn hàng bị lỗi', null, res_calc_price.error || null));
            body.order_price = roundUpPriceToThousand(res_calc_price.data.order_price, "vnd");
            body.extra_money = roundUpPriceToThousand(res_calc_price.data.extra_money, "vnd");

            if (body.start_time.indexOf("Z") == -1) {
                //it means that start_time has format 2021-01-29 22:00:00 GMT+0700
                if (body.start_time.indexOf("GMT+") == -1) {
                    body.start_time = body.start_time + " GMT+0700";
                }
            } else {
                //By default it must be UTC time
            }
            // Create order
            const result = await this.tourOrderService.save(body, data_user);
            if (result && result.status) {
                //Alert slack 
                let data_order = result.data;
                let creatorName = (data_user.email != null) ? data_user.email : data_user.phone;
                let company = data_user.company || null;
                this.slackAlertService.alertSlackAboutTourOrder(creatorName, company, 1, data_order);
                return res.status(HttpStatus.OK).send(dataSuccess('oK', data_order));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Tạo đơn hàng bị lỗi', null, result.error || ERROR_UNKNOWN));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null, ERROR_UNKNOWN));
        }
    }

    @Post('partner/create')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Create tour order from partner' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async createTourOrderFromPartner(@Body() body: any, @Req() request, @Res() res: Response) {
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        data_user.current_role = data_user.current_role.toLowerCase();
        try {
            body.company = data_user.company || '';
            if (body.promotion_code) {
                body.promotion_code = body.promotion_code.toLowerCase();
                let promotion = await this.promotionsService.checkValidateByCoupon(body.promotion_code)
                if (!promotion) return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy mã khuyến mãi trong hệ thống', null, ERROR_PROMOTION_NOT_FOUND));
            }
            if (!body.tour_id) {
                if (!body.tour_uuid) return res.status(HttpStatus.NOT_FOUND).send(dataError('Tour id và tour uuid không thể cùng thiếu', null, ERROR_TOUR_ID_UUID_CANNOT_EMPTY));
                let tour = await this.tourService.findByUuid(body.tour_uuid)
                if (!tour) return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy tour', null, ERROR_TOUR_NOT_FOUND));
                body.tour_id = tour.id
            }
            // Calculate price prder
            let res_calc_price = await this.tourOrderService.calcOrderTourPrice(body);
            if (!res_calc_price.status) return res.status(HttpStatus.BAD_REQUEST).send(dataError(res_calc_price.message || 'Tạo đơn hàng bị lỗi', null, res_calc_price.error || null));
            body.order_price = roundUpPriceToThousand(res_calc_price.data.order_price, "vnd");
            body.extra_money = roundUpPriceToThousand(res_calc_price.data.extra_money, "vnd");

            if (body.start_time.indexOf("Z") == -1) {
                //it means that start_time has format 2021-01-29 22:00:00 GMT+0700
                if (body.start_time.indexOf("GMT+") == -1) {
                    body.start_time = body.start_time + " GMT+0700";
                }
            } else {
                //By default it must be UTC time
            }
            // Create order
            const result = await this.tourOrderService.save(body, data_user);
            let listTourOrders=[];
            let isSuccess=false;
            if (result && result.status) {
                //Alert slack 
                let data_order = result.data;
                let creatorName = (data_user.email != null) ? data_user.email : data_user.phone;
                let company = data_user.company || null;
                this.slackAlertService.alertSlackAboutTourOrder(creatorName, company, 1, data_order);
                listTourOrders.push(data_order);
                isSuccess=true;
            }else{
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Tạo đơn hàng bị lỗi', null, result.error || ERROR_UNKNOWN));
            }

            if(!!body.start_time_round_trip){
                if (body.start_time_round_trip.indexOf("Z") == -1) {
                    //it means that start_time has format 2021-01-29 22:00:00 GMT+0700
                    if (body.start_time_round_trip.indexOf("GMT+") == -1) {
                        body.start_time_round_trip = body.start_time_round_trip + " GMT+0700";
                    }
                } else {
                }    
                //swap location and time
                body.start_time=body.start_time_round_trip;
                let tempLatPickLocation=body.lat_pick_location;
                let tempLngPickLocation=body.long_pick_location;
                let tempAddressPick=body.address_pick;
                let tempAddressDestination=body.address_destination
                body.lat_pick_location=body.lat_destination_location;
                body.long_pick_location=body.long_destination_location;
                body.lat_destination_location=tempLatPickLocation;
                body.long_destination_location=tempLngPickLocation;
                body.address_pick=tempAddressDestination;
                body.address_destination=tempAddressPick;
                const result = await this.tourOrderService.save(body, data_user);
                //Alert slack 
                if (result && result.status) {
                    //Alert slack 
                    let data_order = result.data;
                    let creatorName = (data_user.email != null) ? data_user.email : data_user.phone;
                    let company = data_user.company || null;
                    this.slackAlertService.alertSlackAboutTourOrder(creatorName, company, 1, data_order);
                    listTourOrders.push(data_order);
                    isSuccess=true;
                }else{
                    return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Tạo đơn hàng bị lỗi', null, result.error || ERROR_UNKNOWN));
                }
            }
            
            if(isSuccess){
                return res.status(HttpStatus.OK).send(dataSuccess('oK', listTourOrders));
            }else{
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Tạo đơn hàng bị lỗi', null, result.error || ERROR_UNKNOWN));
            }

        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null, ERROR_UNKNOWN));
        }
    }


    @Post('/customer-create-order')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Create tour order for customer' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    async createTourOrderForApp(@Body() body: CreateTourOrderDto, @Req() request, @Res() res: Response) {
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        body.customer_id = data_user.id;
        try {
            if (body.promotion_code) {
                body.promotion_code = body.promotion_code.toLowerCase();
                let promotion = await this.promotionsService.checkValidateByCoupon(body.promotion_code)
                if (!promotion) return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy mã khuyến mãi trong hệ thống', null, ERROR_PROMOTION_NOT_FOUND));
            }
            // Calculate price prder
            let res_calc_price = await this.tourOrderService.calcOrderTourPrice(body);
            if (!res_calc_price.status) return res.status(HttpStatus.BAD_REQUEST).send(dataError(res_calc_price.message || 'Tạo đơn hàng bị lỗi', null, res_calc_price.error || null));
            body.order_price = roundUpPriceToThousand(res_calc_price.data.order_price, "vnd");
            body.extra_money = roundUpPriceToThousand(res_calc_price.data.extra_money, "vnd");

            // Create order
            let customer_info = await this.usersService.findOne(data_user.user_id);
            if (!customer_info) return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy khách hàng trong hệ thống', null, null));
            body.customer_mobile = customer_info.phone;
            body.customer_first_name = customer_info.customerProfile ? customer_info.customerProfile.first_name : '';
            body.customer_last_name = customer_info.customerProfile ? customer_info.customerProfile.last_name : '';
            body.customer_gender = customer_info.customerProfile ? customer_info.customerProfile.gender : 0;
            const result = await this.tourOrderService.save(body, data_user);
            if (result && result.status) {
                //Alert slack 
                let data_order = result.data;
                let creatorName = (data_user.email != null) ? data_user.email : data_user.phone;
                let company = data_user.company || null;
                this.slackAlertService.alertSlackAboutTourOrder(creatorName, company, 1, data_order);
                return res.status(HttpStatus.OK).send(dataSuccess('oK', data_order));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Tạo đơn hàng bị lỗi', null, result.error || ERROR_UNKNOWN));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null, ERROR_UNKNOWN));
        }
    }

    @Put('/:tour_order_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Edit tour order' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'tour_order_uuid', description: 'Order Uuid', required: true, type: 'string' })
    async updateTourOrder(@Param('tour_order_uuid') tour_order_uuid, @Body() body: UpdateTourOrderDto, @Req() request, @Res() res: Response) {
        let tour_order;
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        try {
            tour_order = await this.tourOrderService.findByUuid(tour_order_uuid);
            if (!tour_order)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy order', null, ERROR_TOUR_ORDER_NOT_FOUND));
            else if (data_user.company && tour_order.company !== data_user.company)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Đơn hàng không thuộc về công ty', null, ERROR_USER_NOT_PERMISSION_UPDATE_ORDER));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
        try {
            if (body.promotion_code && tour_order.promotion_code !== body.promotion_code) {
                body.promotion_code = body.promotion_code.toLowerCase();
                let promotion = await this.promotionsService.checkValidateByCoupon(body.promotion_code)
                if (!promotion) return res.status(HttpStatus.NOT_FOUND).send(dataError('Không tìm thấy mã khuyến mãi trong hệ thống', null, ERROR_PROMOTION_NOT_FOUND));
            }
            // Calculate price prder
            let data_calc_price = await this.tourOrderService.mergeData(tour_order, body)
            let res_calc_price = await this.tourOrderService.calcOrderTourPrice(data_calc_price);
            if (!res_calc_price.status) return res.status(HttpStatus.BAD_REQUEST).send(dataError(res_calc_price.message || 'Tạo đơn hàng bị lỗi', null, res_calc_price.error || null));
            body.order_price = res_calc_price.data.order_price;
            body.extra_money = res_calc_price.data.extra_money;
            // Update order
            const result = await this.tourOrderService.update(tour_order, body, data_user);
            if (result && result.status) {
                let dataOrder = result.data;
                // Alert slack 
                let creatorName = (data_user.email != null) ? data_user.email : data_user.phone;
                let company = data_user.company || null;
                this.slackAlertService.alertSlackAboutTourOrder(creatorName, company, dataOrder.status, dataOrder);
                return res.status(HttpStatus.OK).send(dataSuccess('oK', dataOrder));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Chỉnh sửa đơn hàng bị lỗi', null, result.error || ERROR_UNKNOWN));
        } catch (error) {
            console.log("error - updateTourOrder: ", error);
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null, ERROR_UNKNOWN));
        }
    }

    @Put('/driver-update/:tour_order_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOperation({ title: 'Edit tour order for driver' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'tour_order_uuid', description: 'Order Uuid', required: true, type: 'string' })
    async updateTourOrderForDriver(@Param('tour_order_uuid') tour_order_uuid, @Body() body: UpdateTourOrderDto, @Req() request, @Res() res: Response) {
        let tour_order;
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        try {
            tour_order = await this.tourOrderService.findByUuid(tour_order_uuid);
            if (!tour_order)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Không tìm thấy order', null, ERROR_TOUR_ORDER_NOT_FOUND));
            else if (data_user.id && tour_order.driver_id && ((tour_order.status !== STATUS_TOUR_ARR.CANCELED_CUSTOMER && tour_order.status !== STATUS_TOUR_ARR.CANCELED_DRIVER) && tour_order.driver_id !== data_user.id))
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Đơn hàng không thuộc về tài xế', null, ERROR_USER_NOT_PERMISSION_UPDATE_ORDER));
            else if (data_user.current_role === 'customer' && tour_order.finish_time) // Đơn hàng được cập nhật bởi KH và có finish_time => đã hoàn thành
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Đơn hàng đã hoàn tất. Không thể cập nhật thêm', null, ERROR_CUSTOMER_CAN_NOT_UPDATE_ORDER));
            else if (data_user.current_role === 'customer' && tour_order.status === STATUS_TOUR_ARR.CANCELED_CUSTOMER) // Đơn hàng được cập nhật bởi KH và có đã bị huỷ bởi KH
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Đơn hàng đã bị huỷ. Không thể cập nhật thêm', null, ERROR_CUSTOMER_CAN_NOT_UPDATE_ORDER));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
        try {
            // Calculate price prder
            let data_calc_price = await this.tourOrderService.mergeData(tour_order, body)
            let res_calc_price = await this.tourOrderService.calcOrderTourPrice(data_calc_price);
            if (!res_calc_price.status) return res.status(HttpStatus.BAD_REQUEST).send(dataError(res_calc_price.message || 'Tạo đơn hàng bị lỗi', null, res_calc_price.error || null));
            let order_price = res_calc_price.data.order_price;
            // Cập nhật status thành ACCEPTED_DRIVER
            if (body.status && body.status !== tour_order.status && body.status === STATUS_TOUR_ARR.ACCEPTED_DRIVER) {
                // Check Fee Wallet Driver
                let checkFeeWallet = await this.tourOrderService.checkFeeWalletToReceiveOrder(order_price, tour_order.order_code, data_user.id);
                if (!checkFeeWallet.data) return res.status(HttpStatus.BAD_REQUEST).send(dataError(checkFeeWallet.message || 'Tài xế không đủ tiền để nhận đơn hàng này', null, checkFeeWallet.error || null));

                // Minus Azgo fee for driver
                tour_order.order_price = order_price
                let minusAzgoFeeReceiveOrder = await this.walletService.minusAzgoFeeReceiveOrder(tour_order, data_user.id)
                if (!minusAzgoFeeReceiveOrder.status) return res.status(HttpStatus.BAD_REQUEST).send(dataError(minusAzgoFeeReceiveOrder.message || null, null, minusAzgoFeeReceiveOrder.error || null));
            }
            // Cập nhật status thành CANCELED_DRIVER hoặc CANCELED_CUSTOMER
            if (body.status && body.status !== tour_order.status && (body.status === STATUS_TOUR_ARR.CANCELED_DRIVER || body.status === STATUS_TOUR_ARR.CANCELED_CUSTOMER)) {
                // Minus Azgo fee for driver
                tour_order.order_price = order_price
                let returnAzgoFeeReceiveOrder = await this.walletService.returnAzgoFeeReceiveOrder(tour_order, data_user.id)
                if (!returnAzgoFeeReceiveOrder.status) return res.status(HttpStatus.BAD_REQUEST).send(dataError(returnAzgoFeeReceiveOrder.message || null, null, returnAzgoFeeReceiveOrder.error || null));
            }

            // Update order
            body.driver_id = data_user.id;
            const result = await this.tourOrderService.update(tour_order, body, data_user);
            if (result && result.status) {
                let dataOrder = result.data;
                // Alert slack 
                let creatorName = (data_user.email != null) ? data_user.email : data_user.phone;
                let company = data_user.company || null;
                this.slackAlertService.alertSlackAboutTourOrder(creatorName, company, dataOrder.status, dataOrder);
                return res.status(HttpStatus.OK).send(dataSuccess('oK', dataOrder));
            }
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(result.message || 'Chỉnh sửa đơn hàng bị lỗi', null, result.error || ERROR_UNKNOWN));
        } catch (error) {
            console.log("error - updateTourOrder: ", error);
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null, ERROR_UNKNOWN));
        }
    }

    @Delete('/:tour_order_uuid')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete tour order' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'tour_order_uuid', description: 'Order Uuid', required: true, type: 'string' })
    async deleteTourOrder(@Param('tour_order_uuid') tour_order_uuid, @Req() request, @Res() res: Response) {
        let tour_order;
        let data_user;
        const token = request.headers.authorization.split(' ')[1];
        data_user = jwt.decode(token);
        try {
            tour_order = await this.tourOrderService.findByUuid(tour_order_uuid);
            if (!tour_order)
                return res.status(HttpStatus.NOT_FOUND).send(dataError('Tài khoản không tìm thấy', null));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
        try {
            let result = await this.tourOrderService.deleteTourOrder(tour_order, data_user);
            if (!result.success)
                return res.status(HttpStatus.NOT_FOUND).send(dataError(result.error.message || 'Xoá đơn hàng bị lỗi', null, ERROR_DELETE_TOUR_ORDER));
            return res.status(HttpStatus.OK).send(dataSuccess('Xóa đơn hàng thành công', result));
        } catch (error) {
            console.log('deleteTourOrder controller: ', error)
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null, ERROR_DELETE_TOUR_ORDER));
        }
    }

    @Post('/checkFeeWalletToReceiveOrder')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'check friver fee wallet' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    // @ApiImplicitParam({ name: 'tour_order_uuid', description: 'Order Uuid', required: true, type: 'string' })
    async checkFeeWalletToReceiveOrder(@Body() body: any, @Req() request, @Res() res: Response) {
        try {
            let res_calc_price = await this.tourOrderService.calcOrderTourPrice(body);
            if (!res_calc_price.status)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(res_calc_price.message || 'Đơn hàng bị lỗi', null, res_calc_price.error || null));
            let order_price = res_calc_price.data && res_calc_price.data.order_price
            let checkFeeWalletToReceiveOrder = await this.tourOrderService.checkFeeWalletToReceiveOrder(order_price, body.order_code, body.driver_id);
            if (!checkFeeWalletToReceiveOrder.status)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError(checkFeeWalletToReceiveOrder.message || 'Kiểm tra ví tài xế bị lỗi', null, checkFeeWalletToReceiveOrder.error || null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', checkFeeWalletToReceiveOrder));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post('/findByOrderCode')
    @Roles({ function_key: FunctionKey.All, action_key: ActionKey.All })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'check tour order by order code' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    // @ApiImplicitParam({ name: 'tour_order_uuid', description: 'Order Uuid', required: true, type: 'string' })
    async findByOrderCode(@Body() body: any, @Req() request, @Res() res: Response) {
        try {
            let result = await this.tourOrderService.findByOrderCode(body);
            if (!result)
                return res.status(HttpStatus.BAD_REQUEST).send(dataError('Mã Đơn hàng không đúng', null, ERROR_WALLET_ORDER_CODE_IS_NOT_EXIST));
            else
                return res.status(HttpStatus.OK).send(dataSuccess('oK', res));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

 
     

}
