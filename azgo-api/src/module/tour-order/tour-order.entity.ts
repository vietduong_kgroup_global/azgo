
import { Entity, PrimaryGeneratedColumn, Generated, JoinColumn, ManyToOne, OneToOne, Column, BaseEntity } from 'typeorm';
import { CustomerProfileEntity } from '../customer-profile/customer-profile.entity';
import { DriverProfileEntity } from '../driver-profile/driver-profile.entity';
import { UsersEntity } from '../users/users.entity';
import { TourOrderLogEntity } from '../tour-order-log/tour-order-log.entity';
import { TourEntity } from '../tour/tour.entity';
import { Exclude } from 'class-transformer';
import { IsInt, IsNotEmpty } from 'class-validator';

@Entity('az_tour_orders')
export class TourOrderEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    @Generated('uuid')
    @IsNotEmpty()
    uuid: string;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    order_code: string;

    @Column({
        type: 'int',
        nullable: false,
    })
    @IsNotEmpty()
    tour_id: number;

    @ManyToOne(type => TourEntity, tour => tour.id)
    @JoinColumn({ name: 'tour_id', referencedColumnName: 'id' })
    tourProfile: TourEntity;

    @Column({
        type: 'int',
        nullable: true,
    })
    driver_id: number;

    @OneToOne(type => UsersEntity, user => user.tourDriver)
    @JoinColumn({ name: 'driver_id', referencedColumnName: 'id' })
    driverTour: UsersEntity;

    @Column({
        type: 'int',
        nullable: true,
    })
    customer_id: string;

    @OneToOne(type => UsersEntity, user => user.tourCustomer)
    @JoinColumn({ name: 'customer_id', referencedColumnName: 'id' })
    customerTour: UsersEntity;

    @Column({
        type: 'varchar',
    })
    customer_mobile: string;

    @Column({
        type: 'varchar',
    })
    customer_first_name: string;

    @Column({
        type: 'varchar',
    })
    customer_last_name: string;

    @Column({
        type: 'int',
        comment: 'MALE: 1, FEMALE: 2',
    })
    customer_gender: number;

    @Column({
        type: 'varchar',
    })
    fly_number: string;

    @Column({
        type: 'varchar',
    })
    company: string;

    @Column({
        type: 'varchar',
    })
    payment_code: string;

    @Column({
        type: 'varchar',
    })
    external_code: string;

    @Column({
        type: 'varchar',
    })
    promotion_code: string;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
    })
    start_time: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
    })
    finish_time: Date;

    @Column({
        type: 'double',
    })
    lat_pick_location: number;

    @Column({
        type: 'double',
    })
    long_pick_location: number;

    @Column({
        type: 'varchar',
    })
    address_pick: string;

    @Column({
        type: 'double',
    })
    lat_destination_location: number;

    @Column({
        type: 'double',
    })
    long_destination_location: number;

    @Column({
        type: 'varchar',
    })
    address_destination: string;

    @Column({
        type: 'double',
    })
    order_price: number;

    @Column({
        type: 'json',
        nullable: false,
    })
    additional_fee: object;


    @Column({
        type: 'double',
    })
    distance: number;

    @Column({
        type: 'int',
        default: 0,
    })
    extra_time: number;

    @Column({
        type: 'int',
        default: 0,
    })
    time: number;

    @Column({
        type: 'int',
        default: 0,
    })
    extra_money: number;

    @Column({
        type: 'varchar',
    })
    payment_method: string;

    @Column({
        type: 'smallint',
        default: 0,
    })
    auto_send_notification: number;

    @Column({
        type: 'smallint',
        default: 0,
    })
    auto_send_sms: number;

    @Column({
        type: 'smallint',
        default: 0,
        comment: 'Đã thanh toán (0: chưa, 1: có)',
    })
    is_paid: number;

    @Column({
        type: 'varchar',
    })
    driver_cancel_reason: string;

    @Column({
        type: 'varchar',
    })
    customer_cancel_reason: string;

    @Column({
        type: 'smallint',
        default: 1,
        comment: '1: Chờ xác nhận , 2: Tài xế xác nhận, 3: Khách hàng xác nhận, 4: Tài xế huỷ, 5: Khách hàng huỷ, 6: Hoàn tất',
    })
    @IsNotEmpty()
    status: number;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @OneToOne(type => TourOrderLogEntity, tour_order_log => tour_order_log.tourOrderProfile)
    tourOrderLog: TourOrderLogEntity;
}