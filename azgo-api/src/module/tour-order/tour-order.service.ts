import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityId } from 'typeorm/repository/EntityId';
import { OtpService } from "@azgo-module/core/dist/common/otp/otp.service";
import { getManager } from 'typeorm';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import { JwtPayload } from '../auth/interfaces/jwt-payload.interface';
import { TourOrderEntity } from './tour-order.entity';
import { TourOrderLogEntity } from '../tour-order-log/tour-order-log.entity';
import { NotificationEntity } from '../notification/notification.entity';
import { PromotionsService } from '../promotions/promotions.service';
import { TourOrderRepository } from './tour-order.repository';
import { FirebaseService } from '../firebase/firebase.service';
import { UsersService } from '../users/users.service';
import { WalletHistoryService } from '../wallet-history/wallet-history.service';
import { WalletService } from '../wallet/wallet.service';
import { DriverWorkingDateService } from '../driver-working-date/driver-working-date.service';
import { TourService } from '../tour/tour.service';
import { GoogleMapService } from '../google-map/google-map.service';
import { SlackAlertService } from '../slack-alert/slack-alert.service';
import { SettingSystemService } from '../setting-system/setting-system.service';
import { ResultDto } from '../../general-dtos/result.dto';
import {ERROR_SEND_SMS_IN_TOUR_ORDER } from '../../constants/error.const';
import {
    ERROR_TOUR_ORDER_DRIVER_DONT_HAVE_ENOUGHT_FEE_WALLET,
    ERROR_UNKNOWN,
} from '../../constants/error.const';
import { BaseService } from '../../base.service';
import {
    ERROR_TOUR_NOT_FOUND,
    ERROR_TOUR_DO_NOT_HAVE_PRICE,
    ERROR_TOUR_DO_NOT_HAVE_PRICE_DISTANCE,
    ERROR_TOUR_DO_NOT_HAVE_PRICE_TIME,
    ERROR_TOUR_ORDER_DO_NOT_HAVE_COORDINATES,
    ERROR_TOUR_CAN_NOT_FIND_DISTANCE_ON_GG,
} from '../../constants/error.const';
import {
    STATUS_TOUR_ARR,
    TAG_HISTORY_WALLET_ARR
} from '../../constants/secrets.const';
import { throws } from 'assert';
import { roundKmFromDistance, roundUpPriceToThousand } from '../../helpers/utils';
import { TourEntity } from '../tour/tour.entity';
@Injectable()
export class TourOrderService {
    // export class TourOrderService extends BaseService<TourOrderEntity, TourOrderRepository> {
    constructor(
        @InjectRepository(TourOrderRepository)
        private readonly repository: TourOrderRepository,
        private firebaseService: FirebaseService,
        @Inject(forwardRef(() => UsersService))
        private usersService: UsersService,
        private walletHistoryService: WalletHistoryService,
        @Inject(forwardRef(() => WalletService))
        private walletService: WalletService,
        private promotionsService: PromotionsService,
        private driverWorkingDateService: DriverWorkingDateService,
        private readonly tourService: TourService,
        private readonly googleMapService: GoogleMapService,
        private readonly settingSystemService: SettingSystemService, 
        private readonly slackAlertService: SlackAlertService,
        private readonly otpService: OtpService,
        ) {
        // super(repository);
    }
    /**
     * This function get tour orders by tour id
     * @param tour_id
     */
    async mergeData(old_data: TourOrderEntity, data: any) {
        return this.repository.merge(new TourOrderEntity(), old_data, data);
    }
    /**
     * This function get tour orders by tour id
     * @param tour_id
     */
    async findOne(tour_id: number) {
        return await this.repository.findById(tour_id);
    }
    /**
     * This function get tour orders by tour id
     * @param tour_id
     */
    async findByOrderCode(order_code: string) {
        return await this.repository.findByOrderCode(order_code);
    }

     /**
     * This function get tour orders by order code
     * @param tour_id
     */
    async findByUuid(tour_uuid: string) {
        return await this.repository.findByUuid(tour_uuid);
    }

    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        return await this.repository.getAllByOffsetLimit(limit, offset, options);
    }

    async save(data: any, user_info?: JwtPayload) {
        try {
            let notification;
            let tour_order;
            let start_time_milisecond = data.start_time_milisecond;
            let data_log;
            let _this = this;
            let order;
            // Use transaction for data synchronization
            await getManager().transaction(async transactionalEntityManager => {
                data_log = {
                    user_id: user_info.id,
                    type: 'CREATE',
                    data: data,
                    company: user_info.company || '',
                };
                data.order_code = Math.round(new Date().getTime()).toString(16).toLocaleUpperCase();
                data.start_time = data.start_time ? new Date(data.start_time).toISOString() : new Date().toISOString();
                // Get data Additional Fee
                let tour_info = await transactionalEntityManager.findOne(TourEntity,data.tour_id);
                if(!!tour_info.additional_fee){
                    data.additional_fee = tour_info.additional_fee;
                }
                // create order
                let order = this.repository.merge(new TourOrderEntity(), data);
                tour_order = await transactionalEntityManager.save(order);

                // create order log
                await transactionalEntityManager.save(TourOrderLogEntity, { ...data_log, tour_order_id: tour_order.id });
                notification = {
                    title: 'Đơn hàng được tạo',
                    message: '',
                    object_id: tour_order.uuid,
                    type_object: 'TOUR_ORDER',
                    type: 'CREATE_ORDER',
                };

                // update promotion 
                if (data.promotion_code) {
                    let applyPromotion = await this.promotionsService.applyCounpon(data.promotion_code);
                    if (!applyPromotion.status) throw { message: 'Update promotion false', status: 400 };
                }
                // if(!tour_order.driver_id){ // Tạo đơn hàng ko có tài xế => gửi thông baó cho tất cả tài xế 
                //     let customer = !!tour_order.customer_id ? await this.usersService.findById(tour_order.customer_id) : null;
                //     let listDriver = await this.usersService.findAllDriversAvaiable({role: UserType.DRIVER_TOUR, start_time: data.start_time});
                //     listDriver.forEach(async element => {
                //         let notification_driver = await transactionalEntityManager.save(
                //             NotificationEntity,
                //             {
                //                 ...notification,
                //                 user_id: element.id || '',
                //                 user_action_id: user_info.id || '',
                //                 user_action_role: user_info.current_role || '',
                //                 message: `Có đơn hàng #${tour_order.order_code} mới tạo bởi khách hàng ${(customer && customer.customerProfile) ? customer.customerProfile.full_name : `${tour_order.customer_first_name} ${tour_order.customer_last_name}`}`,
                //                 data: {
                //                     customer: {
                //                         first_name: (customer && customer.customerProfile) ? customer.customerProfile.first_name : tour_order.customer_first_name,
                //                         last_name: (customer && customer.customerProfile) ? customer.customerProfile.last_name : tour_order.customer_last_name,
                //                         full_name: (customer && customer.customerProfile) ? customer.customerProfile.full_name : `${tour_order.customer_first_name} ${tour_order.customer_last_name}`,
                //                         phone: customer ? customer.phone : tour_order.customer_mobile,
                //                         avatar: (customer && customer.customerProfile) ? customer.customerProfile.image_profile : {},
                //                     }
                //                 },
                //             },
                //         );
                //         let driver = await _this.firebaseService.findOneByUserId(element.id);
                //         let token = (driver && driver.token) ? driver.token : (driver && driver.apns_id) ? driver.apns_id: null;
                //         _this.firebaseService.pushNotificationWithTokens(
                //             [token],
                //             {
                //                 title: 'Đơn hàng mới',
                //                 body: `Có đơn hàng #${tour_order.order_code} mới tạo bởi khách hàng ${(customer && customer.customerProfile) ? customer.customerProfile.full_name : `${tour_order.customer_first_name} ${tour_order.customer_last_name}`}`,
                //             },
                //             {
                //                 object_id: tour_order.uuid,
                //                 type_object: 'TOUR_ORDER',
                //                 notification_uuid: notification_driver.uuid
                //             }
                //         );
                //     });
                // }
                // Nếu có id tài xế và trạng thái đã chấp nhận thì gửi thông báo 
                if (!!tour_order.driver_id && data.status === STATUS_TOUR_ARR.ACCEPTED_DRIVER) {
                    let customer = !!tour_order.customer_id ? await this.usersService.findById(tour_order.customer_id) : null;
                    let notification_driver = await transactionalEntityManager.save(
                        NotificationEntity,
                        {
                            ...notification,
                            user_id: tour_order.driver_id || '',
                            user_action_id: user_info.id || '',
                            user_action_role: user_info.current_role || '',
                            message: `Bạn vừa nhận được đơn hàng #${tour_order.order_code}`,
                            data: {
                                customer: {
                                    first_name: (customer && customer.customerProfile) ? customer.customerProfile.first_name : tour_order.customer_first_name,
                                    last_name: (customer && customer.customerProfile) ? customer.customerProfile.last_name : tour_order.customer_last_name,
                                    full_name: (customer && customer.customerProfile) ? customer.customerProfile.full_name : `${tour_order.customer_first_name} ${tour_order.customer_last_name}`,
                                    phone: customer ? customer.phone : tour_order.customer_mobile,
                                    avatar: (customer && customer.customerProfile) ? customer.customerProfile.image_profile : {},
                                }
                            },
                        },
                    );
                    const driverFcm = await _this.firebaseService.findOneByUserId(tour_order.driver_id);
                    if (driverFcm) {
                        const tokenDriver = driverFcm.token ? driverFcm.token : driverFcm.apns_id;
                        this.firebaseService.pushNotificationWithTokens(
                            [tokenDriver],
                            {
                                title: 'Đơn hàng mới',
                                body: `Bạn vừa nhận được đơn hàng #${tour_order.order_code}`,
                            },
                            {
                                object_id: tour_order.uuid,
                                type_object: 'TOUR_ORDER',
                                notification_uuid: notification_driver.uuid
                            }
                        );
                    }
                }

                // Nếu có id khách hàng thì gửi thông báo 
                if (!!tour_order.customer_id) {
                    let driver = !!tour_order.driver_id ? await this.usersService.findById(tour_order.driver_id) : null;
                    let notification_customer = await transactionalEntityManager.save(
                        NotificationEntity,
                        {
                            ...notification,
                            user_id: tour_order.customer_id || '',
                            user_action_id: user_info.id || '',
                            user_action_role: user_info.current_role || '',
                            message: `Đơn hàng #${tour_order.order_code} của bạn vừa được tạo`,
                            data: {
                                driver: {
                                    first_name: (driver && driver.driverProfile) ? driver.driverProfile.first_name : "",
                                    last_name: (driver && driver.driverProfile) ? driver.driverProfile.last_name : "",
                                    full_name: (driver && driver.driverProfile) ? driver.driverProfile.full_name : "",
                                    phone: driver ? driver.phone : "",
                                    avatar: (driver && driver.driverProfile) ? driver.driverProfile.image_profile : {},
                                }
                            },
                        },
                    );
                    const customerFcm = await _this.firebaseService.findOneByUserId(tour_order.customer_id);
                    if (customerFcm) {
                        const tokenCustomer = customerFcm.token ? customerFcm.token : customerFcm.apns_id;
                        _this.firebaseService.pushNotificationWithTokens(
                            [tokenCustomer],
                            {
                                title: 'Đơn hàng mới',
                                body: `Đơn hàng #${tour_order.order_code} của bạn vừa được tạo`,
                            },
                            {
                                object_id: tour_order.uuid,
                                type_object: 'TOUR_ORDER',
                                notification_uuid: notification_customer.uuid
                            }
                        );
                    }
                }
                if (start_time_milisecond) {
                    let resultDriverWorkingDate = await this.driverWorkingDateService.updateDriverWorking({
                        order: data,
                        new_driverId: data.driver_id,
                        orderCode: tour_order.order_code,
                        startTimeMilisecond: start_time_milisecond,
                    }, transactionalEntityManager);
                    if (!resultDriverWorkingDate.status) throw { message: 'Error in resultDriverWorkingDate' }
                }

            });
            let createdTourOrder = await this.repository.findById(tour_order.id);
            return {
                status: true,
                data: createdTourOrder,
            }; 
        } catch (error) { 
            console.log('Error in save tour order ', error);
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message,
            }; 
        }
    }

    async update(tour_order: any, data: any, user_info?: JwtPayload) {
        try {
            let notification;
            let _this = this;
            let data_log;
            let order;
            // Use transaction for data synchronization
            let old_driver_id = tour_order.driver_id;
            let new_driver_id = data.driver_id;
            let start_time_milisecond = data.start_time_milisecond;
            delete data.start_time_milisecond;
            let responseTransaction = await getManager().transaction(async transactionalEntityManager => {
                data_log = {
                    user_id: user_info.id,
                    tour_order_id: tour_order.id,
                    type: 'UPDATE',
                    data: data, // luu data order vào log
                    company: tour_order.company || '',
                };
                if (data.status && tour_order.status !== data.status) {
                    data_log.type = 'UPDATE_STATUS';
                }
                // update promotion 
                if (data.promotion_code && tour_order.promotion_code !== data.promotion_code) { // Nhập mã promotion khác
                    let applyPromotion = await this.promotionsService.applyCounpon(data.promotion_code);
                    if (!applyPromotion.status)
                        return {
                            status: applyPromotion.status,
                            data: applyPromotion.data,
                            error: applyPromotion.error,
                            message: applyPromotion.message
                        };
                     
                    if (tour_order.promotion_code) { // Remove Promotion cũ
                        let removeCounpon = await this.promotionsService.removeCounpon(tour_order.promotion_code);
                        if (!removeCounpon.status)
                            return {
                                status: removeCounpon.status,
                                data: removeCounpon.data,
                                error: removeCounpon.error,
                                message: removeCounpon.message
                            };
                    }
                }
                // Get data Additional Fee
                if (data.tour_id && data.tour_id !== tour_order.tour_id){
                    let tour_info = await this.tourService.findOne(data.tour_id);
                    if (!!tour_info.additional_fee){
                        data.additional_fee = tour_info.additional_fee;
                    }
                    else{
                        data.additional_fee = null;
                    }
                }
                order = _this.repository.merge(tour_order, data);
                // create order log
                await transactionalEntityManager.save(TourOrderLogEntity, data_log);
                notification = {
                    title: 'Đơn hàng được cập nhật',
                    message: '',
                    object_id: order.uuid,
                    type_object: 'TOUR_ORDER',
                    type: 'UPDATE_ORDER',
                };

                // Nếu có id tài xế thì gửi thông báo 
                if (!!order.driver_id) {
                    let customer = !!order.customer_id ? await _this.usersService.findById(order.customer_id) : null;
                    let message_driver = `Đơn hàng #${order.order_code} của bạn vừa được cập nhật`;
                    if (data_log.type === 'UPDATE_STATUS') {
                        // check status order
                        switch (order.status) {
                            case STATUS_TOUR_ARR.ACCEPTED_DRIVER: 
                                if (user_info.current_role === 'admin') message_driver = `Bạn đã nhận được đơn hàng #${order.order_code} bới admin`;
                                else if (user_info.current_role === 'driver') message_driver = `Bạn đã xác nhận đơn hàng #${order.order_code}`;
                                break;
                            case STATUS_TOUR_ARR.ACCEPTED_CUSTOMER:
                                if (user_info.current_role === 'admin') message_driver = `Đơn hàng #${order.order_code} của bạn đã được xác nhận bởi admin`;
                                else if (user_info.current_role === 'customer') message_driver = `Khách hàng ${(customer && customer.customerProfile) ? customer.customerProfile.full_name : `${order.customer_first_name} ${order.customer_last_name}`} đã xác nhận đơn hàng #${order.order_code} của bạn`;
                                break;
                            case STATUS_TOUR_ARR.CANCELED_DRIVER:
                                if (user_info.current_role === 'admin') message_driver = `Bạn đã bị huỷ đơn hàng #${order.order_code} bới admin`;
                                else if (user_info.current_role === 'driver') message_driver = `Bạn đã huỷ đơn hàng #${order.order_code}`;
                                // order.driver_id = null; // Set driver null
                                break;
                            case STATUS_TOUR_ARR.CANCELED_CUSTOMER:
                                if (user_info.current_role === 'admin') message_driver = `Bạn đã bị huỷ đơn hàng #${order.order_code} bới admin`;
                                else if (user_info.current_role === 'customer') message_driver = `Khách hàng ${(customer && customer.customerProfile) ? customer.customerProfile.full_name : `${order.customer_first_name} ${order.customer_last_name}`} đã huỷ đơn hàng #${order.order_code} của bạn`;
                                break;
                            case STATUS_TOUR_ARR.DONE:
                                message_driver = `Đơn hàng #${order.order_code} của bạn đã hoàn tất`;
                                break;
                            default:
                                if (user_info.current_role === 'admin')  message_driver = `Đơn hàng #${order.order_code} của bạn vừa được cập nhật bởi admin`;
                                else if (user_info.current_role === 'customer') message_driver = `Khách hàng ${(customer && customer.customerProfile) ? customer.customerProfile.full_name : `${order.customer_first_name} ${order.customer_last_name}`} đã cập nhật đơn hàng #${order.order_code} của bạn`;
                                else message_driver = `Đơn hàng #${order.order_code} của bạn vừa được cập nhật`;
                        }
                    }
                    let notification_driver = await transactionalEntityManager.save(
                        NotificationEntity,
                        {
                            ...notification,
                            user_id: order.driver_id || '',
                            user_action_id: user_info.id || '',
                            user_action_role: user_info.current_role || '',
                            message: message_driver,
                            data: {
                                customer: {
                                    first_name: (customer && customer.customerProfile) ? customer.customerProfile.first_name : order.customer_first_name,
                                    last_name: (customer && customer.customerProfile) ? customer.customerProfile.last_name : order.customer_last_name,
                                    full_name: (customer && customer.customerProfile) ? customer.customerProfile.full_name : `${order.customer_first_name} ${order.customer_last_name}`,
                                    phone: customer ? customer.phone : order.customer_mobile,
                                    avatar: (customer && customer.customerProfile) ? customer.customerProfile.image_profile : {},
                                }
                            },
                        },
                    );
                    const driverFcm = await _this.firebaseService.findOneByUserId(order.driver_id);
                    if (driverFcm) {
                        const tokenDriver = driverFcm.token ? driverFcm.token : driverFcm.apns_id;
                        await _this.firebaseService.pushNotificationWithTokens(
                            [tokenDriver],
                            {
                                title: 'Đơn hàng cập nhật',
                                body: message_driver,
                            },
                            {
                                object_id: order.uuid,
                                type_object: 'TOUR_ORDER',
                                notification_uuid: notification_driver.uuid
                            }
                        );
                    }
                }

                // Nếu có id khách hàng thì gửi thông báo
                if (!!order.customer_id) {
                    let driver = !!order.driver_id ? await _this.usersService.findById(order.driver_id) : null;
                    let message_customer = `Đơn hàng #${order.order_code} của bạn vừa được cập nhật`;
                    if (data_log.type === 'UPDATE_STATUS') {
                        // check status order
                        switch (order.status) {
                            case STATUS_TOUR_ARR.VERIFYING:
                                message_customer = `Đơn hàng #${order.order_code} của bạn đang chờ xác nhận`;
                                break;
                            case STATUS_TOUR_ARR.ACCEPTED_DRIVER:
                                if (user_info.current_role === 'admin') message_customer = `Đơn hàng #${order.order_code} của bạn đã được thay đổi tài xế ${(driver && driver.driverProfile) ? driver.driverProfile.full_name : ""}`;
                                else if (user_info.current_role === 'driver') message_customer = `Tài xế ${(driver && driver.driverProfile) ? driver.driverProfile.full_name : ""} đã xác nhận đơn hàng #${order.order_code} của bạn`;
                                break;
                            case STATUS_TOUR_ARR.ACCEPTED_CUSTOMER:
                                if (user_info.current_role === 'admin') message_customer = `Đơn hàng #${order.order_code} của bạn đã được xác nhận bởi admin`;
                                else if (user_info.current_role === 'customer') message_customer = `Bạn đã xác nhận đơn hàng #${order.order_code}`;
                                break;
                            case STATUS_TOUR_ARR.CANCELED_DRIVER:
                                if (user_info.current_role === 'admin') message_customer = `Đơn hàng #${order.order_code} của bạn đã bị huỷ bởi admin`;
                                else if (user_info.current_role === 'driver') message_customer = `Tài xế ${(driver && driver.driverProfile) ? driver.driverProfile.full_name : ""} đã huỷ đơn hàng #${order.order_code} của bạn`;
                                // order.driver_id = null; // Set driver null
                                break;
                            case STATUS_TOUR_ARR.CANCELED_CUSTOMER:
                                if (user_info.current_role === 'admin') message_customer = `Đơn hàng #${order.order_code} của bạn đã bị huỷ bởi admin`;
                                else if (user_info.current_role === 'customer') message_customer = `Bạn đã huỷ đơn hàng #${order.order_code}`;
                                break;
                            case STATUS_TOUR_ARR.DONE:
                                message_customer = `Đơn hàng #${order.order_code} của bạn đã hoàn tất`;
                                break;
                            default:
                                message_customer = `Đơn hàng #${order.order_code} của bạn vừa được cập nhật`;
                        }
                    }
                    let notification_customer = await transactionalEntityManager.save(
                        NotificationEntity,
                        {
                            ...notification,
                            user_id: order.customer_id || '',
                            user_action_id: user_info.id || '',
                            user_action_role: user_info.current_role || '',
                            message: message_customer,
                            data: {
                                driver: {
                                    first_name: (driver && driver.driverProfile) ? driver.driverProfile.first_name : "",
                                    last_name: (driver && driver.driverProfile) ? driver.driverProfile.last_name : "",
                                    full_name: (driver && driver.driverProfile) ? driver.driverProfile.full_name : "",
                                    phone: driver ? driver.phone : "",
                                    avatar: (driver && driver.driverProfile) ? driver.driverProfile.image_profile : {},
                                }
                            },
                        },
                    );
                    const customerFcm = await _this.firebaseService.findOneByUserId(order.customer_id);
                    if (customerFcm) {
                        const tokenCustomer = customerFcm.token ? customerFcm.token : customerFcm.apns_id;
                        await _this.firebaseService.pushNotificationWithTokens(
                            [tokenCustomer],
                            {
                                title: 'Đơn hàng cập nhật',
                                body: message_customer,
                            },
                            {
                                object_id: order.uuid,
                                type_object: 'TOUR_ORDER',
                                notification_uuid: notification_customer.uuid
                            }
                        );
                    }
                }

                if (
                    data_log.type === 'UPDATE_STATUS' && // Update trạng thái
                    order.status === STATUS_TOUR_ARR.DONE && // 6: complete
                    order.driver_id // đơn hàng có tài xế
                ) {
                    let updateWallet = await _this.walletService.updateWalletForDriverFinishTour(order, transactionalEntityManager);
                    if (!updateWallet.status)
                        return {
                            status: updateWallet.status,
                            data: updateWallet.data,
                            error: updateWallet.error,
                            message: updateWallet.message
                        };
                    
                    // Chưa có finish_time =>Cập nhật thời gian hoàn thành  chuyến đi
                    if(!order.finish_time) order.finish_time = new Date().toISOString();
                }
                if (start_time_milisecond) {
                    let resultDriverWorkingDate = await this.driverWorkingDateService.updateDriverWorking({
                        order: order,
                        old_driverId: old_driver_id,
                        new_driverId: new_driver_id,
                        orderCode: order.order_code,
                        startTimeMilisecond: start_time_milisecond,
                    }, transactionalEntityManager);
                    if (!resultDriverWorkingDate.status)
                        return {
                            status: resultDriverWorkingDate.status,
                            data: resultDriverWorkingDate.data,
                            error: resultDriverWorkingDate.error,
                            message: resultDriverWorkingDate.message
                        };
                }
                // Update order
                order.start_time = order.start_time ? new Date(order.start_time).toISOString() : new Date().toISOString();
                delete order.customerTour;
                delete order.driverTour;
                delete order.tourProfile;
                await transactionalEntityManager.save(TourOrderEntity, order);
                return {
                    status: true,
                    data: null,
                    error: null,
                    message: null
                };
            });
            if (!responseTransaction.status) return responseTransaction;
            let updatedTourOrder = await this.repository.findById(order.id); 
            return {
                status: true,
                data: updatedTourOrder,
                error: null,
                message: null
            };
        } catch (error) {
            console.log('Error in update tour order ', error);
            return {
                status: false,
                data: null,
                error: error.error || ERROR_UNKNOWN,
                message: error.message,
            }; 
        }
    }

    /**
     * This is function check % fee wallet compare to order values
     * @param order_price
     * @param order_code
     * @param driver_id 
     */
    async checkFeeWalletToReceiveOrder(order_price: any, order_code: any, driver_id: any) {
        try {
            // Fee walet driver 
            let walletDriver = await this.walletService.getOneByDriverId(driver_id);  
            let fee_wallet = walletDriver.fee_wallet;

            // minimum_percent_order in Setting System
            let settingSystem = await this.settingSystemService.getOne(); 
            let minimum_percent_order = settingSystem.minimum_percent_order; 
            
            if (fee_wallet < order_price  * minimum_percent_order) 
                return {
                    status: true,
                    data: null,
                    message: 'Tài xế không đủ tiền để nhận đơn hàng này',
                    error: ERROR_TOUR_ORDER_DRIVER_DONT_HAVE_ENOUGHT_FEE_WALLET,
                };
            return {
                status: true,
                data: true,
                message: null,
                error: null,

            };

        } catch (error) {
            console.log(error)
            return {
                status: false,
                data: null,
                error: error,
                message: error.message
            };

        }
    }


    /**
     * This is function delete order
     * @param order
     */
    async deleteTourOrder(tour_order: any, user_info?: JwtPayload) {
        try {
            let data_log;
            let _this = this;
            // Use transaction for data synchronization
            await getManager().transaction(async transactionalEntityManager => {
                data_log = {
                    user_id: user_info.id,
                    tour_order_id: tour_order.id,
                    type: 'DELETE',
                    data: tour_order,
                    company: tour_order.company || '',
                };
                let data = {
                    deleted_at: new Date(),
                };
                tour_order = this.repository.merge(tour_order, data)
                await transactionalEntityManager.save(TourOrderEntity, tour_order);
                // create order log
                await transactionalEntityManager.save(TourOrderLogEntity, data_log);
            });
            return {
                success: true,
                error: null
            };
        } catch (error) {
            console.log('deleteTourOrder - error: ', error);
            return {
                success: false,
                error: error
            };
        }
    }

    /**
     * This is function delete order
     * @param order
     */
    async calcOrderTourPrice(tour_order: any) {
        try {
            let order_price = 0;
            let extra_money = 0;
            let tour_info = await this.tourService.findOne(tour_order.tour_id);
            let result;
            if (!tour_info) {
                return {
                    status: false,
                    data: null,
                    message: 'Tour không tồn tại, vui lòng kiểm tra lại',
                    error: ERROR_TOUR_NOT_FOUND,
                };
            }

            switch (tour_info.type) {
                case 'fix_price':
                    if (tour_info.tour_price) {
                        let extra_time = tour_order.extra_time ? Number(tour_order.extra_time) / 60 : 0; // Đổi gian theo phút => giờ
                        extra_money = (extra_time && tour_info.extra_time_price) ? (extra_time * tour_info.extra_time_price) : 0;
                        extra_money = roundUpPriceToThousand(extra_money, "vnd");
                        order_price = roundUpPriceToThousand(tour_info.tour_price + extra_money, "vnd");
                        result = {
                            status: true,
                            data: { order_price, extra_money },
                            message: null,
                            error: null,
                        };
                    }
                    else { // tour chưa cập nhật giá
                        result = {
                            status: false,
                            data: null,
                            message: 'Tour chưa cập nhật giá',
                            error: ERROR_TOUR_DO_NOT_HAVE_PRICE
                        };
                    }
                    break;
                case 'distance_price':
                    if (!tour_info.distance_rate) {
                        result = {
                            status: false,
                            data: null,
                            message: 'Tour chưa cập nhật giá theo km',
                            error: ERROR_TOUR_DO_NOT_HAVE_PRICE_DISTANCE
                        };
                    }
                    else if ( // Không đủ toạ độ
                        tour_info.distance_rate &&
                        (!tour_order.lat_pick_location ||
                            !tour_order.long_pick_location ||
                            !tour_order.long_destination_location ||
                            !tour_order.lat_destination_location)
                    ) {
                        result = {
                            status: false,
                            data: null,
                            message: 'Đơn hàng thiếu toạ độ điểm đón, điểm đến',
                            error: ERROR_TOUR_ORDER_DO_NOT_HAVE_COORDINATES,
                        };
                    }
                    else { // tour chưa cập nhật giá
                        let coordinates = {
                            origins: {
                                lat: String(tour_order.lat_pick_location),
                                lng: String(tour_order.long_pick_location)
                            },
                            destinations: {
                                lat: String(tour_order.lat_destination_location),
                                lng: String(tour_order.long_destination_location)
                            }
                        }
                        const distance = await this.googleMapService.getAzgoDistance(coordinates);
                        if (distance) {
                            if (distance.rows && distance.rows[0] && distance.rows[0].elements && distance.rows[0].elements[0] && distance.rows[0].elements[0].distance) {
                                tour_order.distance = roundKmFromDistance(distance.rows[0].elements[0].distance.value);
                                order_price = roundUpPriceToThousand(tour_order.distance * tour_info.distance_rate, "vnd");
                                result = {
                                    status: true,
                                    data: { order_price, extra_money },
                                    message: null,
                                    error: null
                                };
                            } else {
                                result = {
                                    status: false,
                                    data: null,
                                    message: 'Google không tính được khoảng cách',
                                    error: ERROR_TOUR_CAN_NOT_FIND_DISTANCE_ON_GG,
                                };
                            }
                        } else {
                            result = {
                                status: false,
                                data: null,
                                message: 'Google không tính được khoảng cách',
                                error: ERROR_TOUR_CAN_NOT_FIND_DISTANCE_ON_GG
                            };
                        }
                    }
                    break;
                case 'time_price':
                    if (tour_info.time_rate) {
                        order_price = roundUpPriceToThousand(tour_order.time * tour_info.time_rate, "vnd");
                        result = {
                            status: true,
                            data: { order_price, extra_money },
                            message: null,
                            error: null
                        };
                    }
                    else { // tour chưa cập nhật giá
                        result = {
                            status: false,
                            data: null,
                            message: 'Tour chưa cập nhật giá theo ngày',
                            code: ERROR_TOUR_DO_NOT_HAVE_PRICE_TIME
                        };
                    }
                    break;
                default: break
            }

            return result;
        } catch (error) {
            console.log('deleteTourOrder - error: ', error);
            return {
                success: false,
                error: error
            };
        }
    }

    /**
     *  Push những đơn hàng chưa được xác nhận và chưa có tài xế trong vòng 24h
     * @param order
     */
    async checkTourOrder() {
        try {
            // Push đến những đơn hàng chưa có tài xế
            let queryAllOrders = await this.repository.createQueryBuilder('az_tour_orders')
                .andWhere(`az_tour_orders.status NOT IN (${STATUS_TOUR_ARR.CANCELED_CUSTOMER}, ${STATUS_TOUR_ARR.CANCELED_DRIVER})`) 
                .andWhere('az_tour_orders.deleted_at is null')
                .andWhere('az_tour_orders.driver_id is null')
                .andWhere('az_tour_orders.created_at >= :from_date_tostring', {from_date_tostring: new Date((new Date().getTime() - 24 * 60 * 60 * 1000))});
            let allOrder = await queryAllOrders.getMany(); 
            allOrder.map((e) => {
                // if (new Date(e.created_at).getTime() + 24 * 60 * 60 * 1000 >= new Date().getTime()) {
                    this.slackAlertService.alertSlack(process.env.CHANNEL_ALERT_TOUR_ORDER, `Đơn hàng ${e.order_code} chưa có tài xế` );
                // }
            }); 



            // Push đến những đơn hàng da co tx, đang chờ khách hàng xác nhận 
            let queryAllOrders_have_driver = await this.repository.createQueryBuilder('az_tour_orders')
            .andWhere(`az_tour_orders.status IN (${STATUS_TOUR_ARR.ACCEPTED_DRIVER}, ${STATUS_TOUR_ARR.VERIFYING})`) 
            .andWhere('az_tour_orders.deleted_at is null')
            .andWhere('az_tour_orders.driver_id is not null')
            .andWhere('az_tour_orders.created_at >= :from_date_tostring', {from_date_tostring: new Date((new Date().getTime() - 24 * 60 * 60 * 1000))});
            let allOrder_have_driver = await queryAllOrders_have_driver.getMany(); 
            allOrder_have_driver.map((e) => {
                this.slackAlertService.alertSlack(process.env.CHANNEL_ALERT_TOUR_ORDER, `Đơn hàng ${e.order_code} đang chờ khách hàng xác nhận` );
            }); 
            return {
                status: true, 
            }; 

        } catch (error) {
            console.log('checkTourOrder - error: ', error);
            return {
                status: false, 
            };
        }
    }
    async revenueOrderReport(limit: number, offset: number, options?: any) {  
        let result =  await this.repository.revenueOrderReport(limit, offset, options); 
        return result;
    }
    async exportRevenueOrderReport(options?: any) {  
        let result =  await this.repository.exportRevenueOrderReport(options); 
        return result;
    }
    async sendSMS(options?: any) {
        try {
            console.log('option send sms:', options)
            const rsSendOtp = await this.otpService.sendOtp({
                Phone: options.phone,
                Content: options.content,
                SmsType: 2,
                Brandname: process.env.OTP_ESMS_BRAND_NAME || "AZGO", 
            });
            console.log('*****Result send SMS*******', rsSendOtp)
            
            let result = new ResultDto<any>();
            result.error = null; 
            result.status = true;
            result.message = null;
            result.data =  null;
            if (Number(rsSendOtp.CodeResult).valueOf() !== 100) {
                result.status = false;
                result.error = ERROR_SEND_SMS_IN_TOUR_ORDER;
                result.message = rsSendOtp.ErrorMessage;
            } 
            return result; 
             
        } catch (error) {
            let result = new ResultDto<any>();
            result.error = ERROR_SEND_SMS_IN_TOUR_ORDER; 
            result.status = true;
            result.message = error.toString();
            result.data =  null;
            return result;  
        }
     }
 
}
