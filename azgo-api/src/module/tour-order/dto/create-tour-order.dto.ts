import {ApiModelProperty} from '@nestjs/swagger';
import {IsInt, IsNotEmpty, Validate} from 'class-validator';

export class CreateTourOrderDto {
    @ApiModelProperty({ example: 1, required: true })
    tour_id: number;

    @ApiModelProperty({ example: 'company', required: false })
    company: string;
    
    @ApiModelProperty({ example: 1, required: false })
    driver_id: number;

    @ApiModelProperty({ example: 1, required: false })
    customer_id: number;

    @ApiModelProperty({ example: "2021-01-12 16:57:22", required: false })
    start_time: string;

    @ApiModelProperty({ example: 60, required: false, description: "Thời  gian thêm tính theo giờ" })
    extra_time: number;

    @ApiModelProperty({ example: 60, required: false, description: 'So ngay  thue xe'  })
    time: number;

    @ApiModelProperty({ example: 180000, required: false })
    extra_money: number;

    @ApiModelProperty({ example: 10.864588601846245, required: false })
    lat_pick_location: number;

    @ApiModelProperty({ example: 106.73446572639331, required: false })
    long_pick_location: number;

    @ApiModelProperty({ example: "Chợ đêm", required: false })
    address_pick: string;

    @ApiModelProperty({ example: 10.864588601846245, required: false })
    lat_destination_location: number;

    @ApiModelProperty({ example: 106.73446572639331, required: false })
    long_destination_location: number;

    @ApiModelProperty({ example: "Chợ đêm", required: false })
    address_destination: string;

    @ApiModelProperty({ example: '0969807203', required: false })
    customer_mobile: string;

    @ApiModelProperty({ example: 'Họ', required: false })
    customer_first_name: string;

    @ApiModelProperty({ example: 'Tên', required: false })
    customer_last_name: string;

    @ApiModelProperty({ example: 1, required: false, description: 'MALE: 1, FEMALE: 2' })
    customer_gender: number;

    @ApiModelProperty({ example: 'VJ901', required: false })
    fly_number: string;

    @ApiModelProperty({ example: '#879871A', required: false })
    payment_code: string;

    @ApiModelProperty({ example: 'INDIGO', required: false })
    external_code: string;

    @ApiModelProperty({ example: 'FLASHSALE', required: false })
    promotion_code: string;

    @ApiModelProperty({ example: 100000, required: false })
    order_price: number;

    @ApiModelProperty({ example: 18, required: false })
    distance: number;
}
