import { Brackets, EntityRepository, Repository } from 'typeorm';
import { TourOrderEntity } from './tour-order.entity';
import {
    STATUS_TOUR_ARR
} from '../../constants/secrets.const';
import { ResultDto } from '../../general-dtos/result.dto';
import {ERROR_REPORT_ORDER } from '../../constants/error.const';
@EntityRepository(TourOrderEntity)
export class TourOrderRepository extends Repository<TourOrderEntity> {
    /**
     * This function get list users by limit & offset
     * @param limit
     * @param offset
     * @param options
     */
    async getAllByOffsetLimit(limit: number, offset: number, options?: any) {
        try {
            let query = this.createQueryBuilder('az_tour_orders');
            query.leftJoinAndSelect('az_tour_orders.customerTour', 'customerTour');
            query.leftJoinAndSelect('customerTour.customerProfile', 'customerProfile');
            query.leftJoinAndSelect('az_tour_orders.driverTour', 'driverTour');
            query.leftJoinAndSelect('driverTour.driverProfile', 'driverProfile');
            query.leftJoinAndSelect('driverTour.vehicles', 'vehicles');
            query.leftJoinAndSelect('vehicles.vehicleType', 'vehicleType');
            query.leftJoinAndSelect('vehicles.vehicleBrand', 'vehicleBrand');
            query.leftJoinAndSelect('az_tour_orders.tourProfile', 'tourProfile');
            query.leftJoinAndSelect('tourProfile.vehicleType', 'vehicleTypeTour');
            // not delete_at
            query.andWhere('az_tour_orders.deleted_at is null');

            // search by keyword
            if (options.keyword && options.keyword.trim()) {
                options.keyword = options.keyword.trim();
                if (options.keyword.startsWith('0'))
                    options.keyword = options.keyword.substring(1, options.keyword.length);
                else if (options.keyword.startsWith('+84') || options.keyword.startsWith('84'))
                    options.keyword = options.keyword.substring(3, options.keyword.length);
                query.andWhere(new Brackets(qb => {
                    qb.where(`az_tour_orders.order_code like :order_code`, { order_code: '%' + options.keyword.trim() + '%' })
                        .orWhere('tourProfile.name like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                        .orWhere('customerTour.phone like :keyword', { keyword: '%' + options.keyword.trim() + '%' })
                        .orWhere('driverTour.phone like :keyword', { keyword: '%' + options.keyword.trim() + '%' });
                }));
            }
            // Filter by price tour
            if (!!options.from_price && !!options.to_price) {
                query.andWhere(new Brackets(qb => {
                    qb.where(`tourProfile.tour_price >= :from_price`, { from_price: options.from_price })
                        .andWhere(`tourProfile.tour_price <= :to_price`, { to_price: options.to_price });
                }));
            }

            // Filter by number of seat
            if (!!options.number_of_seat && !!options.number_of_seat) {
                query.andWhere(new Brackets(qb => {
                    qb.where(`tourProfile.number_of_seat = :number_of_seat`, { number_of_seat: options.number_of_seat })
                }));
            }

            // Filter by date create
            if (!!options.from_date && !!options.to_date) {
                query.andWhere(`az_tour_orders.created_at >= :from_date`, { from_date: options.from_date });
                query.andWhere(`az_tour_orders.created_at <= :to_date`, { to_date: options.to_date });
            }

            // Filter by start time
            if (!!options.start_time) {
                query.andWhere(`az_tour_orders.start_time >= :start_time`, { start_time: options.start_time });
            }
            // Filter by company
            if (!!options.company) { // 1 account company đang gọi api
                query.andWhere(`az_tour_orders.company = :company`, { company: options.company });
            }
            // filter by area_uuid
            if (options.area_uuid) {
                query.andWhere(`tourProfile.area_uuid = :area_uuid`, { area_uuid: options.area_uuid });
            }
            else if (!!options.list_company) { // filter by admin
                try {
                    options.list_company = JSON.parse(options.list_company);
                } catch (error) {
                    options.list_company = [];
                }
                if (options.list_company.length) {
                    query.andWhere(`az_tour_orders.company IN (:list_company)`, { list_company: options.list_company });
                }
            }


            // Filter by driver
            if (!!options.driver_id) {
                query.andWhere(`az_tour_orders.driver_id = :driver_id`, { driver_id: options.driver_id });
            }

            // Filter by customer
            if (!!options.customer_id) {
                query.andWhere(`az_tour_orders.customer_id = :customer_id`, { customer_id: options.customer_id });
            }

            // Filter by customer
            if (!!options.is_from_airport && options.is_from_airport === 'true') {
                query.andWhere(`az_tour_orders.tourProfile.is_from_airport = 1`);
            }

            // Filter by Status
            try {
                if (!Array.isArray(options.status)) options.status = JSON.parse(options.status);
            } catch (error) {
                options.status = [];
            }
            if (options.status.length) {
                query.andWhere(`az_tour_orders.status IN (:status)`, { status: options.status });
            }
            // Sort 
            if (!!options.sort && !!options.sort_by) {
                query.orderBy(`az_tour_orders.${options.sort_by}`, options.sort === 'ascend' ? 'ASC' : 'DESC');
            }
            else {
                query.orderBy('az_tour_orders.id', 'DESC');
            }
            // limit + offset
            query.limit(limit)
                .offset(offset);

            const results = await query.getManyAndCount();

            return {
                success: true,
                data: results[0],
                total: results[1],
            };
        } catch (error) {
            console.log('getAllByOffsetLimit error: ', error)
            return {
                success: false,
                data: [],
                total: 0,
            };
        }
    }
    /**
     * This function get find by tour order uuid
     * @param tour_order_uuid
     */
    async findByUuid(uuid: string): Promise<TourOrderEntity> {
        try {
            return await this.findOne({
                where: { uuid, deleted_at: null },
                relations: [
                    'customerTour',
                    'customerTour.customerProfile',
                    'driverTour',
                    'driverTour.driverProfile',
                    'driverTour.vehicles',
                    'driverTour.vehicles.vehicleType',
                    'driverTour.vehicles.vehicleBrand',
                    'tourProfile',
                    'tourProfile.vehicleType',
                    'tourProfile.areaInfo',
                ],
            });
        } catch (error) {
            console.log('findByUuid error: ', error)
            return
        }
    }

    /**
     * This is function get tour order by id
     * @param id
     */
    async findById(id: number) {
        return await this.findOne({ where: { id, deleted_at: null } });
    }

    /**
     * This is function get tour order by order code
     * @param id
     */
    async findByOrderCode(order_code: string) {
        console.log('order_code', order_code)
        return await this.findOne({ where: { order_code, deleted_at: null } });
    }

    /**
     * This is function get tour order by uuid
     * @param id
     */
    // async findByUuid(uuid: string) {
    //     return await this.findOne({ where: { uuid, deleted_at: null }});
    // }
    async revenueOrderReport(limit: number, offset: number, options?: any) { 
        try { 
            let query = this.createQueryBuilder('az_tour_orders')
            query.leftJoinAndSelect('az_tour_orders.customerTour', 'customerTour');
            query.leftJoinAndSelect('customerTour.customerProfile', 'customerProfile');
            query.leftJoinAndSelect('az_tour_orders.driverTour', 'driverTour');
            query.leftJoinAndSelect('driverTour.driverProfile', 'driverProfile');
            query.leftJoinAndSelect('driverTour.vehicles', 'vehicles');
            query.leftJoinAndSelect('vehicles.vehicleType', 'vehicleType'); 
            query.leftJoinAndSelect('az_tour_orders.tourProfile', 'tourProfile'); 
            query.andWhere('az_tour_orders.deleted_at is null');
            query.andWhere(`az_tour_orders.status = ${STATUS_TOUR_ARR.DONE}`);

            // Filter by date create
            if (!!options.from && !!options.to) {
                query.andWhere(`az_tour_orders.finish_time >= :from`, { from: options.from });
                query.andWhere(`az_tour_orders.finish_time <= :to`, { to: options.to });
            } 
             // Filter by driver id
            if (!!options.driver_id) {
                query.andWhere(`az_tour_orders.driver_id = :driver_id`, { driver_id: options.driver_id });
            } 
            query.limit(limit)
            .offset(offset);
            let results = await query.getManyAndCount();
            // let results = await query.getMany(); 
            let result = new ResultDto<any>();
            result.error = null;
            result.status = true;
            result.message = null;
            result.data = {
                data: results[0],
                total: results[1],
            };
            return result;    
        } catch (error) {
            console.log("error order: ", error)
            let result = new ResultDto<any>();
            result.error = ERROR_REPORT_ORDER;
            result.status = false;
            result.message = 'Lỗi báo cáo chuyến xe HT';
            result.data = null;
            return result;  
        }  
    }

    async exportRevenueOrderReport(options?: any) { 
        try { 
            let query = this.createQueryBuilder('az_tour_orders')
            query.leftJoinAndSelect('az_tour_orders.customerTour', 'customerTour');
            query.leftJoinAndSelect('customerTour.customerProfile', 'customerProfile');
            query.leftJoinAndSelect('az_tour_orders.driverTour', 'driverTour');
            query.leftJoinAndSelect('driverTour.driverProfile', 'driverProfile');
            query.leftJoinAndSelect('driverTour.vehicles', 'vehicles');
            query.leftJoinAndSelect('vehicles.vehicleType', 'vehicleType'); 
            query.leftJoinAndSelect('az_tour_orders.tourProfile', 'tourProfile'); 
            query.andWhere('az_tour_orders.deleted_at is null');
            query.andWhere(`az_tour_orders.status = ${STATUS_TOUR_ARR.DONE}`);

            // Filter by date create
            if (!!options.from && !!options.to) {
                query.andWhere(`az_tour_orders.finish_time >= :from`, { from: options.from });
                query.andWhere(`az_tour_orders.finish_time <= :to`, { to: options.to });
            } 
             // Filter by driver id
            if (!!options.driver_id) {
                query.andWhere(`az_tour_orders.driver_id = :driver_id`, { driver_id: options.driver_id });
            } 
            let results = await query.getMany(); 
            let result = new ResultDto<any>();
            result.error = null;
            result.status = true;
            result.message = null;
            result.data = results;
            return result;   
        } catch (error) {
            console.log("order Report: ", error)
            let result = new ResultDto<any>();
            result.error = ERROR_REPORT_ORDER;
            result.status = false;
            result.message = null;
            return result; 
        }
    }
}
