import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TourOrderController } from './tour-order.controller';
import { TourOrderService } from './tour-order.service';
import { TourOrderRepository } from './tour-order.repository';
import { TourOrderLogModule } from '../tour-order-log/tour-order-log.module';
import { NotificationModule } from '../notification/notification.module';
import { TourModule } from '../tour/tour.module';
import { UsersModule } from '../users/users.module';
import { FirebaseModule } from '../firebase/firebase.module';
import { TourService } from '../tour/tour.service';
import { TourRepository } from '../tour/tour.repository';
import { OtpModule } from '@azgo-module/core/dist/common/otp/otp.module';
import { PromotionsModule } from '../promotions/promotions.module';
import { PromotionRepository } from '../promotions/promotion.repository';
import { PromotionsService } from '../promotions/promotions.service';
import { WalletHistoryModule } from '../wallet-history/wallet-history.module';
import { WalletModule } from '../wallet/wallet.module';
import { DriverWorkingDateModule } from '../driver-working-date/driver-working-date.module';
import { SlackAlertModule } from '../slack-alert/slack-alert.module';
import { GoogleMapService } from '../google-map/google-map.service';
import {SettingSystemModule} from '../setting-system/setting-system.module'
@Module({
  imports: [
    TypeOrmModule.forFeature([
      TourOrderRepository,
      PromotionRepository,
      TourRepository,
    ]),
    FirebaseModule,
    TourOrderLogModule,
    NotificationModule,
    PromotionsModule,
    TourModule,

    WalletHistoryModule, 
    DriverWorkingDateModule,
    SlackAlertModule,
    SettingSystemModule,
    OtpModule,
    forwardRef(() => UsersModule),
    forwardRef(() => WalletModule), 
  ],
  controllers: [TourOrderController],
  providers:
    [
      TourOrderService,
      PromotionsService,
      TourService,
      GoogleMapService,
    ],
  exports: [TourOrderService],
})
export class TourOrderModule { }
