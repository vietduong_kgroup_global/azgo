import { Test, TestingModule } from '@nestjs/testing';
import { TourOrderController } from './tour-order.controller';

describe('TourOrder Controller', () => {
  let controller: TourOrderController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TourOrderController],
    }).compile();

    controller = module.get<TourOrderController>(TourOrderController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
