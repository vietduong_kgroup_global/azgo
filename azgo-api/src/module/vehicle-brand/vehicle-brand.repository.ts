import { EntityRepository, Repository } from 'typeorm';
import { VehicleBrandEntity } from './vehicle-brand.entity';

@EntityRepository(VehicleBrandEntity)
export class VehicleBrandRepository extends Repository<VehicleBrandEntity> {
    async getAll(limit: number, offset: number, options?: any) {
        let query = await this.createQueryBuilder('az_vehicle_brand');
        if (!options.keyword === false) {
            query.where('az_vehicle_brand.name like :keyword', {keyword: '%' + options.keyword + '%'});
        }
        query.andWhere('az_vehicle_brand.deleted_at is null');
        query.orderBy('az_vehicle_brand.id', 'DESC')
            .take(limit)
            .skip(offset);
        const results = await query.getManyAndCount();

        return {
            success: true,
            data: results[0],
            total: results[1],
        };
    }
}
