import { Module } from '@nestjs/common';
import { VehicleBrandController } from './vehicle-brand.controller';
import { VehicleBrandService } from './vehicle-brand.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {VehicleBrandRepository} from './vehicle-brand.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([VehicleBrandRepository]),
  ],
  exports: [
    VehicleBrandService,
  ],
  controllers: [VehicleBrandController],
  providers: [VehicleBrandService],
})
export class VehicleBrandModule {}
