import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { IsNotBlank } from '@azgo-module/core/dist/utils/blank.util';

export class CreateVehicleBrandDto {
    @IsNotEmpty({ message: 'Name is require' })
    @IsNotBlank('name', { message: 'Is not white space' })
    @ApiModelProperty({ format: 'name', example: 'honda' })
    name: string;
}
