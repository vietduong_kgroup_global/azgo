import { Injectable } from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {VehicleBrandRepository} from './vehicle-brand.repository';
import {VehicleBrandEntity} from './vehicle-brand.entity';
import {CreateVehicleBrandDto} from './dto/createVehicleBrand.dto';

@Injectable()
export class VehicleBrandService {
    constructor(
        @InjectRepository(VehicleBrandRepository)
        private readonly vehicleBrandRepository: VehicleBrandRepository,
    ) { }

    /**
     * This is function get all vehicle brand
     * @param limit
     * @param offset
     * @param options
     */
    async getAll(limit: number, offset: number, options?: any){
        return await this.vehicleBrandRepository.getAll(limit, offset, options);
    }

    /**
     * This is function find by id
     * @param id
     */
    async findById(id: number): Promise<VehicleBrandEntity> {
        return await this.vehicleBrandRepository.findOne(id, {
                where: 'deleted_at is null',
            },
        );
    }

    /**
     * This is function create vehicle brand
     * @param body
     */
    async create(body: CreateVehicleBrandDto): Promise<VehicleBrandEntity> {
        let vehicle_brand = new VehicleBrandEntity();
        await this.vehicleBrandRepository.merge(vehicle_brand, body);
        return await this.vehicleBrandRepository.save(vehicle_brand);
    }

    /**
     * This is function update vehicle brand
     * @param id
     * @param body
     */
    async update(id: number, body: CreateVehicleBrandDto): Promise<VehicleBrandEntity> {
        let vehicle_brand = await this.findById(id);
        if (!vehicle_brand) throw { message: 'Vehicle brand not found', status: 404 };
        await this.vehicleBrandRepository.merge(vehicle_brand, body);
        return await this.vehicleBrandRepository.save(vehicle_brand);
    }

    /**
     * This is function delete vehicle brand
     * @param id
     */
    async delete(id: number): Promise<boolean> {
        let vehicle_brand = await this.findById(id);
        if (!vehicle_brand) throw { message: 'Vehicle brand not found', status: 404 };
        vehicle_brand.deleted_at = new Date();
        await this.vehicleBrandRepository.save(vehicle_brand);
        return true;
    }
}
