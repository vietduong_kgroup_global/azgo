import {Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Req, Res, UseGuards} from '@nestjs/common';
import { AuthGuard} from '@nestjs/passport';
import {
    ApiBearerAuth,
    ApiImplicitParam,
    ApiImplicitQuery,
    ApiOperation,
    ApiResponse,
    ApiUseTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import { Roles } from '@azgo-module/core/dist/common/decorators/roles.decorator';
import { FunctionKey, ActionKey } from '@azgo-module/core/dist/common/guards/roles.guards';
import { dataSuccess, dataError, getPaginationDataWithLimitAndOffset } from '@azgo-module/core/dist/utils/jsonFormat';
import { CreateVehicleBrandDto } from './dto/createVehicleBrand.dto';
import { VehicleBrandService } from './vehicle-brand.service';
import { CustomValidationPipe } from '@azgo-module/core/dist/common/pipe/customValidation.pipe';
import { UserType } from '@azgo-module/core/dist/utils/enum.ultis';

@ApiUseTags('Vehicle brand')
@Controller('vehicle-brand')
export class VehicleBrandController {
    constructor(
        private readonly vehicleBrandService: VehicleBrandService,
    ) { }

    @Get()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get vehicle brands' })
    @ApiResponse({ status: 200, description: 'Success' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitQuery({ name: 'keyword', description: 'Keyword', required: false, type: 'string' })
    @ApiImplicitQuery({name: 'per_page', description: 'Number transactions per page', required: false, type: 'number'})
    @ApiImplicitQuery({name: 'page', description: 'Page to query', required: false, type: 'number'})
    async getAll(@Req() request, @Res() res: Response) {
        let results;
        // ===============Query offset + per_page=============
        let per_page = Number(request.query.per_page).valueOf();
        if (isNaN(per_page)) {
            per_page = 10;
        }

        let page = Number(request.query.page).valueOf();
        if (isNaN(page)) {
            page = 1;
        }
        const offset = (page - 1) * per_page;

        try {
            results = await this.vehicleBrandService.getAll(per_page, offset, request.query);
        } catch (e) {
            console.error('[findAllUsers] error', e);
            return res.status(e.status || HttpStatus.BAD_REQUEST).send(dataError(e.message || 'Error', null));
        }

        let options = {
            current_page: page,
            next_page: page,
            total_page: Math.ceil(results.total / per_page),
        };
        options.next_page = options.current_page < options.total_page ? options.current_page + 1 : options.current_page;

        if (options.total_page > 0 && options.next_page > options.total_page) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError('current_page is not greater than total_page', null));
        }

        return res.status(HttpStatus.OK).send(getPaginationDataWithLimitAndOffset('OK', results.data, results.total, options));
    }

    @Get('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN, UserType.PROVIDER ]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Detail Vehicle brand' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'number' })
    async detail(@Param('id') id, @Res() res: Response) {
        try {
            const result = await this.vehicleBrandService.findById(id);
            if (!result) return res.status(HttpStatus.NOT_FOUND).send(dataError('Vehicle brand not found', null));
            return res.status(HttpStatus.OK).send(dataSuccess('oK', result));
        } catch (error) {
            return res.status(HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Post()
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create vehicle brand' })
    @ApiResponse({ status: 201, description: 'Created' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    async create(@Req() request, @Body(CustomValidationPipe) body: CreateVehicleBrandDto, @Res() res: Response) {
        try {
            let vehicle = await this.vehicleBrandService.create(body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', vehicle));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Put('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update vehicle brand' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'id', description: 'Id', required: true, type: 'number' })
    async update(@Param('id') id, @Body(CustomValidationPipe) body: CreateVehicleBrandDto, @Res() res: Response) {
        try {
            let vehicle_brand = await this.vehicleBrandService.update(id, body);
            return res.status(HttpStatus.CREATED).send(dataSuccess('oK', vehicle_brand));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }

    @Delete('/:id')
    @Roles({ function_key: FunctionKey.User_Type, action_key: ActionKey.All, user_type: [UserType.ADMIN]})
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete vehicle brand' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: 'Not found' })
    @ApiImplicitParam({ name: 'id', description: 'ID', required: true, type: 'number' })
    async deleteVehicleBrand(@Param('id') id, @Req() request, @Res() res: Response) {
        try {
            let result = await this.vehicleBrandService.delete(id);

            return res.status(HttpStatus.OK).send(dataSuccess('Deleted vehicle brand success', result));
        } catch (error) {
            return res.status(error.status || HttpStatus.BAD_REQUEST).send(dataError(error.message || 'Bad requests', null));
        }
    }
}
