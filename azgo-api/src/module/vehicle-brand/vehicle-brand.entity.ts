import {Entity, Column, PrimaryGeneratedColumn, OneToMany} from 'typeorm';
import {Exclude} from 'class-transformer';
import {VehicleEntity} from '../vehicle/vehicle.entity';

@Entity('az_vehicle_brand')
export class VehicleBrandEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: '255',
    })
    name: string;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @Exclude()
    @Column({
        type: 'timestamp',
        precision: null,
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @Column({
        type: 'timestamp',
        nullable: true,
    })
    deleted_at: Date;

    @OneToMany(type => VehicleEntity, vehicle => vehicle.vehicleBrand)
    vehicles: VehicleEntity[];
}
