import {Controller, Body, Post, HttpStatus, Res, Req, Param, Put, UseInterceptors, UploadedFile} from '@nestjs/common';
import {
    ApiUseTags,
    ApiResponse,
    ApiOperation,
    ApiImplicitParam,
    ApiImplicitBody,
    ApiConsumes,
    ApiImplicitFile
} from '@nestjs/swagger';
import {Response} from 'express';
import {dataSuccess, dataError} from '@azgo-module/core/dist/utils/jsonFormat';
import {Roles} from '@azgo-module/core/dist/common/decorators/roles.decorator';
import {FunctionKey, ActionKey} from '@azgo-module/core/dist/common/guards/roles.guards';
import {RedisService} from '@azgo-module/core/dist/background-utils/Redis/redis.service';
import config from '@azgo-module/core/dist/config';
import {RedisUtil} from '@azgo-module/core/dist/utils/redis.util';
import * as _ from 'lodash';

@ApiUseTags('API test')
@Controller('test')
export class TestController {
    private redisService;
    private redis;
    private  connect:any;

    constructor() {
        this.connect= {
            port: process.env.ENV_REDIS_PORT || 6379,
            host: process.env.ENV_REDIS_HOST || '127.0.0.1', 
            password: process.env.ENV_REDIS_PASSWORD || null,
        };
        console.log(" Redis constructor:",this.connect);
        this.redisService = RedisService.getInstance(this.connect);
        this.redis = RedisUtil.getInstance();
    }

    @Roles({function_key: FunctionKey.All, action_key: ActionKey.All})
    @Post()
    @ApiOperation({title: 'Test socket IO'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    async test(@Req() request, @Body() body, @Res() res: Response) {
        this.redisService.triggerEventDemo({room: 'user_123456', message: 'test socket'});
        return res.status(HttpStatus.CREATED).send(dataSuccess('Push oke', null));
    }

    @Roles({function_key: FunctionKey.All, action_key: ActionKey.All})
    @Post('/redis-cache')
    @ApiOperation({title: 'Test redis cache'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    async testRedisCahe(@Req() request, @Body() body, @Res() res: Response) {
        // get cache
        let getCache = await this.redis.getInfoRedis('vehicle_running', 'field_vehicle');
        if (getCache.isValidate) {
            const listLatLng = JSON.parse(getCache.value)
            // 46 bach dang street
            const lat1 = 10.814243
            const lon1 = 106.6693821
            const unit = 'K'
            const distanceApprove = 1
            console.log(listLatLng)
            let listLatLngChoose = [];
            if (listLatLng) {
                listLatLng.forEach((val) => {
                    const lat2 = val.lat;
                    const lon2 = val.lng;
                    const distance = this.calculateDistance(lat1, lon1, lat2, lon2, unit)
                    if (distance <= distanceApprove) {
                        val.distance = distance;
                        val.unit = unit;
                        listLatLngChoose.push(val);
                    }
                });
                // if exit list lat long, that is approve
                // if (listLatLngChoose.length > 0){
                //     listLatLngChoose.forEach((val, indx) => {
                //         // remove from array
                //         _.remove(listLatLngChoose, function(n) {
                //             return n.distance > val.distance && val.ma_xe === n.ma_xe ;
                //         });
                //     });
                //
                // }
                return res.status(HttpStatus.OK).send(dataSuccess('List Lat Long APPROVED', listLatLngChoose));
            }

            return res.status(HttpStatus.OK).send(dataSuccess('oK', JSON.parse(getCache.value)));
        }

        return res.status(HttpStatus.OK).send(dataSuccess('Data empty', null));
    }

    @Roles({function_key: FunctionKey.All, action_key: ActionKey.All})
    @Post('/redis-cache-register')
    @ApiOperation({title: 'Test redis cache'})
    @ApiResponse({status: 200, description: 'Success'})
    @ApiResponse({status: 400, description: 'Bad Request'})
    async testRegisterRedisCahe(@Req() request, @Body() body, @Res() res: Response) {
        // get cache
        // let getCache = await this.redis.getInfoRedis('vehicle_running', 'field_vehicle', true);
        // let getCache = await this.redis.getInfoRedis('vehicle_running', 'field_vehicle');
        //  if (getCache.isValidate)
        //      return res.status(HttpStatus.OK).send(dataSuccess('oK', JSON.parse(getCache.value)));

        // {string} data
        let data = [
            {
                ma_xe: '11-99999',
                lat: '10.8153868',
                lng: '106.672924',
                diem_den: '126 Bach Dang',
            },
            {
                ma_xe: '11-99999',
                lat: '10.814243',
                lng: '106.6693821',
                diem_den: '46 Bach Dang',
            },
            {
                ma_xe: '22-99999',
                lat: '10.9128536',
                lng: '106.8193789',
                diem_den: 'Du lịch Sinh thái Thủy Châu',
            },
            {
                ma_xe: '22-99999',
                lat: '10.9128536',
                lng: '106.8193789',
                diem_den: 'Khu Du Lịch Suối Mơ Quận 9',
            },
            {
                ma_xe: '22-99999',
                lat: '10.8153868',
                lng: '106.672924',
                diem_den: '126 Bach Dang',
            },
            {
                ma_xe: '22-99999',
                lat: '10.814243',
                lng: '106.6693821',
                diem_den: '46 Bach Dang',
            },
            {
                ma_xe: '62H1-99999',
                lat: '10.823803',
                lng: '106.6665848',
                diem_den: 'HCM',
            },
            {
                ma_xe: '62H1-99999',
                lat: '10.809471',
                lng: '106.666499',
                diem_den: 'Dong Nai',
            },
            {
                ma_xe: '63B1-99999',
                lat: '10.809471',
                lng: '106.666499',
                diem_den: 'HCM',
            },
            {
                ma_xe: '63G2-99999',
                lat: '10.809471',
                lng: '106.6565427',
                diem_den: 'Binh Duong',
            },
        ];
        console.log('start');
        await this.redis.setInfoRedis('vehicle_running', 'field_vehicle', JSON.stringify(data));
        console.log('success')
        return res.status(HttpStatus.OK).send(dataSuccess('Set success', null));
    }

    calculateDistance(lat1, lon1, lat2, lon2, unit) {
        let r = 0
        if ((lat1 == lat2) && (lon1 == lon2)) {
            r = 0;
        }
        else {
            var radlat1 = Math.PI * lat1 / 180;
            var radlat2 = Math.PI * lat2 / 180;
            var theta = lon1 - lon2;
            var radtheta = Math.PI * theta / 180;
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            if (unit === 'K') {
                dist = dist * 1.609344
            }
            // if (unit === 'N') {
            //     dist = dist * 0.8684
            // }
            r = dist;
        }
        return r;
    }
}
