import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColArea1614320949837 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_area` ADD COLUMN `image` TEXT DEFAULT NULL AFTER `uuid`"); 
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
