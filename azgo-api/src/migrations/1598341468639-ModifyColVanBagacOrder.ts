import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class ModifyColVanBagacOrder1598341468639 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('az_van_bagac_orders', 'receive_and_delivery_distance');
        await queryRunner.addColumns('az_van_bagac_orders', [
            new TableColumn({
                name: 'receive_and_delivery_distance',
                type: 'varchar',
                isNullable: true,
                comment: 'Khoản cách từ điểm nhận đến điểm trả',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
