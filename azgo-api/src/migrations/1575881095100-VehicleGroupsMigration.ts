import {MigrationInterface, QueryRunner, Table, TableForeignKey} from 'typeorm';

export class VehicleGroupsMigration1575881095100 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_vehicle_groups',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'vehicle_id',
                    type: 'int',
                },
                {
                    name: 'name',
                    type: 'varchar',
                },
            ],
        }), true);
        await queryRunner.createForeignKey('az_vehicle_groups', new TableForeignKey({
            columnNames: ['vehicle_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_vehicle',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_vehicle_groups');
        const roleForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('vehicle_id') !== -1);
        await queryRunner.dropForeignKey('az_vehicle_groups', roleForeignKey);
        await queryRunner.dropTable('az_vehicle_groups');
    }

}
