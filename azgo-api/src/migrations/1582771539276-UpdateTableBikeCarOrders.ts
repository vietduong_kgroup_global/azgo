import {MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey} from 'typeorm';

export class UpdateTableBikeCarOrders1582771539276 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_bike_car_orders');
        await queryRunner.createTable(new Table({
            name: 'az_bike_car_orders',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'customer_id',
                    type: 'int',
                },
                {
                    name: 'driver_id',
                    type: 'int',
                },
                {
                    name: 'service_id',
                    type: 'int',
                },
                {
                    name: 'route_info',
                    type: 'json',
                    isNullable: true,
                },
                {
                    name: 'book_car_bike_reference',
                    type: 'varchar',
                },
                {
                    name: 'status',
                    type: 'smallint',
                    default: 1,
                    comment: '1: Dang den, 2: Dang di, 3: Hoan thanh, 4: Da huy',

                },
                {
                    name: 'price',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);
        await queryRunner.createForeignKey('az_bike_car_orders', new TableForeignKey({
            columnNames: ['driver_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_users',
            onDelete: 'CASCADE',
        }));
        await queryRunner.createForeignKey('az_bike_car_orders', new TableForeignKey({
            columnNames: ['customer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_users',
            onDelete: 'CASCADE',
        }));
        await queryRunner.createForeignKey('az_bike_car_orders', new TableForeignKey({
            columnNames: ['service_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_vehicle_types',
            onDelete: 'CASCADE',
        }));

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_bike_car_orders');
    }

}
