import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColNotification1612499617750 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_notifications` ADD COLUMN `user_action_role` VARCHAR(255) NULL AFTER `user_action_id`");
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
