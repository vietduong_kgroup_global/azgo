import {getRepository, MigrationInterface, QueryRunner, TableForeignKey} from 'typeorm';

export class UpdateDataVehicleGroup1583250913589 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        const table_types = await queryRunner.getTable('az_vehicle_types');
        const vehicleTypeForeignKey = await table_types.foreignKeys.find(fk => fk.columnNames.indexOf('vehicle_group_id') !== -1);
        if (typeof vehicleTypeForeignKey !== 'undefined') {
            await queryRunner.dropForeignKey('az_vehicle_types', vehicleTypeForeignKey);
        }

        const table_groups = await queryRunner.getTable('az_vehicle_groups');
        const promotionForeignKey = await table_groups.foreignKeys.find(fk => fk.columnNames.indexOf('promotion_id') !== -1);
        if (typeof promotionForeignKey !== 'undefined') {
            await queryRunner.dropForeignKey('az_vehicle_groups', promotionForeignKey);
        }
        await getRepository('az_vehicle_groups').clear();
        await getRepository('az_vehicle_groups').save([
            {
                name: 'BUS',
            },
            {
                name: 'BIKE',
            },
            {
                name: 'CAR',
            },
            {
                name: 'VAN',
            },
            {
                name: 'BAGAC',
            },
        ]);

        await queryRunner.createForeignKey('az_vehicle_types', new TableForeignKey({
            columnNames: ['vehicle_group_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_vehicle_groups',
            onDelete: 'CASCADE',
        }));

        await queryRunner.createForeignKey('az_vehicle_groups', new TableForeignKey({
            columnNames: ['promotion_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_promotions',
            onDelete: 'CASCADE',
        }));

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
