import { MigrationInterface, QueryRunner, Table } from 'typeorm';
export class CreateTableWorkingDate1612426022270 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE IF EXISTS `az_driver_working_date`");
        await queryRunner.createTable(new Table({
            name: 'az_driver_working_date',
            columns: [
                {
                    name: 'driver_id',
                    type: 'int',
                    isNullable: false,
                    isPrimary: true,
                },
                {
                    name: 'working_date',
                    type: 'bigint',
                    isNullable: false,
                    isPrimary: true,
                },
                {
                    name: 'order_code',
                    type: 'varchar',
                    isNullable: false,
                    isPrimary: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                }
            ],
        }), true);
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE IF EXISTS `az_driver_working_date`");
    }
}
