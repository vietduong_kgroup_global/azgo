import {MigrationInterface, QueryRunner} from 'typeorm';

export class SetNullFieldPromotionTableVehicleGroup1577176306220 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `az_vehicle_groups` MODIFY COLUMN `promotion_id` int(11) NULL');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
