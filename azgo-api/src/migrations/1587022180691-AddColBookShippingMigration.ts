import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColBookShippingMigration1587022180691 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_book_shipping', [
            new TableColumn({
                name: 'qr_code_status',
                type: 'smallint',
                default: 1,
            }),
            new TableColumn({
                name: 'qr_code',
                type: 'text',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
