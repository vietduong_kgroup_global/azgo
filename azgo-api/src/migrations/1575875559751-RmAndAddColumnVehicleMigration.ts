import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from 'typeorm';

export class RmAndAddColumnVehicleMigration1575875559751 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_vehicle');
        const roleForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('user_id') !== -1);
        const userForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('vehicle_owner') !== -1);
        await queryRunner.dropForeignKey('az_vehicle', roleForeignKey);
        await queryRunner.dropForeignKey('az_vehicle', userForeignKey);
        await queryRunner.dropColumn('az_vehicle',  'user_id');
        await queryRunner.dropColumn('az_vehicle',  'vehicle_owner');
        await queryRunner.addColumns('az_vehicle', [
            new TableColumn({
                name: 'vehicle_type_id',
                type: 'int',
                isNullable: false,
            }),
            new TableColumn({
                name: 'provider_id',
                type: 'int',
                isNullable: false,
            }),
        ]);
        await queryRunner.createForeignKey('az_vehicle', new TableForeignKey({
            columnNames: ['vehicle_type_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_vehicle_types',
            onDelete: 'CASCADE',
        }));
        await queryRunner.createForeignKey('az_vehicle', new TableForeignKey({
            columnNames: ['provider_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_providers_info',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {

    }

}
