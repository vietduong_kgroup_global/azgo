import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTableTicket1580973331052 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_ticket', [
            new TableColumn({
                name: 'pickup_point',
                type: 'varchar',
                isNullable: true,
            }),
            new TableColumn({
                name: 'destination',
                type: 'varchar',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
