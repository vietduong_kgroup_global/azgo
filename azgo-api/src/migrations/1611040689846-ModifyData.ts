import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class ModifyData1611040689846 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_tour_orders', [
            new TableColumn({
                name: 'address_pick',
                type: 'varchar',
                isNullable: true,
                comment: 'Địa chỉ đón khách',
            }),
            new TableColumn({
                name: 'reason_cancel',
                type: 'varchar',
                isNullable: true,
                comment: 'Lý do huỷ chuyến',
            }),
        ]);
        await queryRunner.query('ALTER TABLE `az_tour` MODIFY COLUMN `area_uuid` VARCHAR(255) NULL');
        await queryRunner.query('ALTER TABLE `az_tour` MODIFY COLUMN `time` INT(11) DEFAULT 0 NULL');
        await queryRunner.query('ALTER TABLE `az_tour` MODIFY COLUMN `number_of_seat` INT(11) DEFAULT 4 NULL');
        await queryRunner.query('ALTER TABLE `az_tour` MODIFY COLUMN `tour_price` INT(11) DEFAULT 0 NULL');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
