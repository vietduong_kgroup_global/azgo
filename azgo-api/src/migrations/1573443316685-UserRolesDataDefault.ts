import {MigrationInterface, QueryRunner} from 'typeorm';

export class UserRolesDataDefault1573443316685 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("INSERT INTO `az_user_roles` (`permission`, `user_id`, `role_id`) VALUES ('{\"functional\": [{\"users\": {\"read\": 1, \"create\": 1, \"delete\": 1, \"update\": 1}}, {\"plants\": {\"read\": 1, \"create\": 1, \"delete\": 1, \"update\": 1}}, {\"plant_category\": {\"read\": 1, \"create\": 1, \"delete\": 1, \"update\": 1}}, {\"plant_type\": {\"read\": 1, \"create\": 1, \"delete\": 1, \"update\": 1}}, {\"roles\": {\"read\": 1, \"create\": 1, \"delete\": 1, \"update\": 1}}, {\"user_roles\": {\"read\": 1, \"create\": 1, \"delete\": 1, \"update\": 1}}, {\"function_settings\": {\"read\": 1, \"create\": 1, \"delete\": 1, \"update\": 1}}]}', 1, 1)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DELETE FROM `az_user_roles` WHERE `id`=1');
    }

}
