import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateDataAdminProfile1610519467558 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('INSERT INTO `az_admin_profile` (`user_id`) ' +
            'VALUES (1)');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DELETE FROM `az_admin_profile` WHERE `user_id`=1');
    }

}
