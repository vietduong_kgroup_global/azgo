import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class createTableVehicleShedule1575865999778 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_vehicle_schedules',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'user_id',
                    type: 'int',
                },
                {
                    name: 'vehicle_id',
                    type: 'int',
                },
                {
                    name: 'start_date',
                    type: 'datetime',
                    isNullable: true,
                },
                {
                    name: 'end_date',
                    type: 'datetime',
                    isNullable: true,
                },
                {
                    name: 'start_hour',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'end_hour',
                    type: 'varchar',
                    isNullable: true,
                },

                {
                    name: 'seat_map',
                    type: 'json',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_vehicle_schedules');
    }

}
