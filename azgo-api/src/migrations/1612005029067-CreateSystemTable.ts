import { MigrationInterface, QueryRunner, Table } from 'typeorm';
export class CreateSystemTable1612005029067 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        // add data table custom_charge
        await queryRunner.query('INSERT INTO `az_custom_charges` (`vehicle_group_id`, `rate_of_charge`) VALUES (6, 1)');
        // Create table setting_system
        await queryRunner.createTable(new Table({
            name: 'az_setting_system',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'uuid',
                    type: 'varchar',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: 'vat',
                    type: 'float',
                    comment: 'Thuế giá trị gia tăng',
                },
                {
                    name: 'tax',
                    type: 'float',
                    comment: 'thuế thu nhập cá nhân',
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                },
            ],
        }), true);
        await queryRunner.query(
            'INSERT INTO `az_setting_system` (`uuid`, `vat`, `tax`) VALUES (\'43bf25f9-4a76-4fb2-8d69-86d2b5f03ea6\', 0.1, 0.015)'
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
