import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTour1609381192666 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_tour', [
            new TableColumn({
                name: 'is_from_airport',
                type: 'smallint',
                default: 0,
                isNullable: true,
                comment: '1: Co, 0: Khong',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
