import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class ChangeTypeColumnTicketMigration1576208932800 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn('az_ticket', 'qr_code', new TableColumn({
            name: 'qr_code',
            type: 'text',
            isNullable: true,
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
