import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
export class CreateTableAdditionalFee1614856055037 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_area` MODIFY COLUMN `uuid` VARCHAR(255) AFTER `id`");
        await queryRunner.query("ALTER TABLE `az_area` ADD INDEX( `uuid`)");

        await queryRunner.query("DROP TABLE IF EXISTS `az_additional_fee`");
        await queryRunner.createTable(new Table({
            name: 'az_additional_fee',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'uuid',
                    type: 'varchar',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: 'code',
                    type: 'varchar',
                    length: '50',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'name',
                    type: 'varchar',
                    length: '255',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'type',
                    type: 'enum',
                    enum: ['flat', 'percent'],
                    default: "'flat'",
                    isNullable: true,
                },
                {
                    name: 'flat',
                    type: 'float',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'percent',
                    type: 'float',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'area_uuid',
                    type: 'varchar',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                },
            ],
        }), true);
        await queryRunner.createForeignKey('az_additional_fee', new TableForeignKey({
            columnNames: ['area_uuid'],
            referencedColumnNames: ['uuid'],
            referencedTableName: 'az_area',
            onDelete: 'CASCADE',
        }));
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
