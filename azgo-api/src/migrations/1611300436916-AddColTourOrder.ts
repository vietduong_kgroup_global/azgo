import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTourOrder1611300436916 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("ALTER TABLE `az_tour` ADD COLUMN `distance_rate` INT(11) DEFAULT 0 NULL COMMENT 'Gia tinh theo km' AFTER `tour_price`");
        queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `external_code` VARCHAR(255) NULL COMMENT 'Ma lien ket cho cty doi tac ngoai he thong' AFTER `payment_code`");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
