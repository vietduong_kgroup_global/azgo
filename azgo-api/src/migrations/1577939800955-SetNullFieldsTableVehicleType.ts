import {MigrationInterface, QueryRunner} from 'typeorm';

export class SetNullFieldsTableVehicleType1577939800955 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `az_vehicle_types` MODIFY COLUMN `max_seat_of_horizontal` int(11) NULL');
        await queryRunner.query('ALTER TABLE `az_vehicle_types` MODIFY COLUMN `max_seat_of_vertical` int(11) NULL');
        await queryRunner.query('ALTER TABLE `az_vehicle_types` MODIFY COLUMN `total_seat` int(11) NULL');
        await queryRunner.query('ALTER TABLE `az_vehicle_types` MODIFY COLUMN `seat_map` json NULL');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
