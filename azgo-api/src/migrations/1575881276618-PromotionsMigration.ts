import {MigrationInterface, QueryRunner, Table, TableForeignKey} from 'typeorm';

export class PromotionsMigration1575881276618 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_promotions',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'vehicle_group_id',
                    type: 'int',
                },
                {
                    name: 'coupon',
                    type: 'varchar',
                },
                {
                    name: 'percent_discount',
                    type: 'float',
                },
                {
                    name: 'price_discount',
                    type: 'float',
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },

            ],
        }), true);
        await queryRunner.createForeignKey('az_promotions', new TableForeignKey({
            columnNames: ['vehicle_group_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_vehicle_groups',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_promotions');
        const roleForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('vehicle_group_id') !== -1);
        await queryRunner.dropForeignKey('az_promotions', roleForeignKey);
        await queryRunner.dropTable('az_promotions');
    }

}
