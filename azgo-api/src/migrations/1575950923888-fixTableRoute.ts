import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class fixTableRoute1575950923888 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const check = await queryRunner.hasColumn('az_routes', 'vehicle_id');
        if (check){
            await queryRunner.dropColumn('az_routes',  'vehicle_id');
        }

        await queryRunner.addColumn('az_routes',
            new TableColumn({
                name: 'duration',
                type: 'varchar',
                default: null,
                isNullable: true,
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
