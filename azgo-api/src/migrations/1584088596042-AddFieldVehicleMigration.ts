import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddFieldVehicleMigration1584088596042 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_vehicle', [
            new TableColumn({
                name: 'images',
                type: 'longtext',
                isNullable: true,
            }),
            new TableColumn({
                name: 'front_car_insurance',
                type: 'longtext',
                isNullable: true,
            }),
            new TableColumn({
                name: 'back_car_insurance',
                type: 'longtext',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
