import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColBikeCarOrderMigration1586320463524 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_bike_car_orders', [
            new TableColumn({
                name: 'vehicle_id',
                type: 'int',
                isNullable: true,
            }),
            new TableColumn({
                name: 'vehicle_group_id',
                type: 'int',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
