import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColSettingSystem1615519873558 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_setting_system` ADD COLUMN `minimum_percent_order` FLOAT DEFAULT 0.3 AFTER `hours_allow_cancel`"); 
        await queryRunner.query("ALTER TABLE `az_setting_system` ADD COLUMN `remind_time` SMALLINT(3) DEFAULT 30 AFTER `minimum_percent_order`"); 
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
