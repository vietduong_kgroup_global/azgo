import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddFieldDriverProfileMigration1584088909517 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_driver_profile', [
            new TableColumn({
                name: 'front_driving_license',
                type: 'longtext',
                isNullable: true,
            }),
            new TableColumn({
                name: 'back_driving_license',
                type: 'longtext',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
