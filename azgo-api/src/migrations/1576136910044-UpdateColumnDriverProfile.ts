import {MigrationInterface, QueryRunner} from 'typeorm';

export class UpdateColumnDriverProfile1576136910044 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `az_driver_profile` MODIFY COLUMN `first_name` VARCHAR(50)');
        await queryRunner.query('ALTER TABLE `az_driver_profile` MODIFY COLUMN `last_name` VARCHAR(50)');
        await queryRunner.query('ALTER TABLE `az_driver_profile` MODIFY COLUMN `gender` SMALLINT(6)');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
