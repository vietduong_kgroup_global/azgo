import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColProfile1617595638521 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_driver_profile` ADD COLUMN `email` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Dia chi email' AFTER `address`");
        await queryRunner.query("ALTER TABLE `az_customer_profile` ADD COLUMN `email` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Dia chi email' AFTER `address`");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_driver_profile` DROP COLUMN `email`");
        await queryRunner.query("ALTER TABLE `az_customer_profile` DROP COLUMN `email`");
    }

}