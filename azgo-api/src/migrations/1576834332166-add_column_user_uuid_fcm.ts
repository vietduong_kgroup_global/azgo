import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class addColumnUserUuidFcm1576834332166 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('az_user_fcm_token',
            new TableColumn({
                name: 'user_uuid',
                type: 'varchar',
                isNullable: true,
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
