import {MigrationInterface, QueryRunner} from 'typeorm';

export class AddColDeleteAtTableVehicleType1578025029830 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `az_vehicle_types` ADD COLUMN `deleted_at` timestamp NULL');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
