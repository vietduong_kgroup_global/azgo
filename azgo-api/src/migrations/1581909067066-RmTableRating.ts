import {MigrationInterface, QueryRunner} from 'typeorm';

export class RmTableRating1581909067066 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_rating');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
