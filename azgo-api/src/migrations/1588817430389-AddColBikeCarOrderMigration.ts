import {MigrationInterface, QueryRunner, Table, TableColumn} from 'typeorm';

export class AddColBikeCarOrderMigration1588817430389 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_bike_car_orders', [
            new TableColumn({
                name: 'payment_method',
                type: 'smallint',
                isNullable: true,
            }),
            new TableColumn({
                name: 'payment_status',
                type: 'smallint',
                default: 1
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
