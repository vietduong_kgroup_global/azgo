import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class ReplaceTextWalletHistory1620577086800 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("UPDATE `az_wallet_histories` SET `content` = REPLACE(`content`, 'Ví tiền mặt', 'tài khoản thanh toán') WHERE INSTR(`content`, 'Ví tiền mặt') > 0");
        await queryRunner.query("UPDATE `az_wallet_histories` SET `content` = REPLACE(`content`, 'Ví cuốc phí', 'tài khoản doanh thu') WHERE INSTR(`content`, 'Ví cuốc phí') > 0");
        await queryRunner.query("UPDATE `az_wallet_histories` SET `content` = REPLACE(`content`, 'ví cước phí', 'tài khoản doanh thu') WHERE INSTR(`content`, 'ví cước phí') > 0");
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
