import {MigrationInterface, QueryRunner} from 'typeorm';

export class RmTableBusOrderMigration1586748456156 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_bus_orders');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
