import {MigrationInterface, QueryRunner} from 'typeorm';

export class AddForeignKeyTableRoute1578453241670 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `az_routes` \n' +
            'ADD CONSTRAINT `FK_route_provider` FOREIGN KEY (`provider_id`) REFERENCES `az_providers_info` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
