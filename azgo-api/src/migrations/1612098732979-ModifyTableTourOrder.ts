import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';
export class ModifyTableTourOrder1612098732979 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `auto_send_sms` SMALLINT(6) DEFAULT 0 AFTER `payment_method`");
        await queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `auto_send_notification` SMALLINT(6) DEFAULT 0 AFTER `payment_method`");

        await queryRunner.query("ALTER TABLE `az_tour_orders` MODIFY COLUMN `order_price` DOUBLE NULL DEFAULT 0 COMMENT 'Gia don hang'");
        await queryRunner.query("ALTER TABLE `az_tour_orders` MODIFY COLUMN `distance` DOUBLE NULL DEFAULT 0 COMMENT 'So km'");
        await queryRunner.query("ALTER TABLE `az_tour_orders` MODIFY COLUMN `start_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Gio khoi hanh'");
        await queryRunner.query("ALTER TABLE `az_tour_orders` MODIFY COLUMN `lat_pick_location` DOUBLE NULL DEFAULT 0");
        await queryRunner.query("ALTER TABLE `az_tour_orders` MODIFY COLUMN `long_pick_location` DOUBLE NULL DEFAULT 0");
        await queryRunner.query("ALTER TABLE `az_tour_orders` MODIFY COLUMN `lat_destination_location` DOUBLE NULL DEFAULT 0");
        await queryRunner.query("ALTER TABLE `az_tour_orders` MODIFY COLUMN `long_destination_location` DOUBLE NULL DEFAULT 0");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
