import {MigrationInterface, QueryRunner} from 'typeorm';

export class RemoveColAreaMigration1609225674906 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        if (await queryRunner.hasColumn('az_tour', 'is_deleted')) {
            await queryRunner.dropColumn('az_tour',  'is_deleted');
        }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}