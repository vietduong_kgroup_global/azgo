import {MigrationInterface, QueryRunner} from 'typeorm';

export class RemoveColAreaMigration1587367386511 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        if (await queryRunner.hasColumn('az_area', 'provider_id')) {
            await queryRunner.dropColumn('az_area',  'provider_id');
        }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
