import {MigrationInterface, QueryRunner, Table} from 'typeorm';

export class CreateTableVanBaGacOrders1582514492294 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_van_bagac_orders',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'customer_id',
                    type: 'int',
                },
                {
                    name: 'driver_id',
                    type: 'int',
                },
                {
                    name: 'service_id',
                    type: 'int',
                },
                {
                    name: 'service_other_id',
                    type: 'int',
                },
                {
                    name: 'route_info',
                    type: 'json',
                },
                {
                    name: 'receiver_info',
                    type: 'json',
                },
                {
                    name: 'luggage_info',
                    type: 'varchar',
                    length: '255',
                },
                {
                    name: 'start_date',
                    type: 'date',
                },
                {
                    name: 'start_hour',
                    type: 'varchar',
                    length: '255',
                },
                {
                    name: 'images',
                    type: 'json',
                },
                {
                    name: 'status',
                    type: 'smallint',
                    isNullable: true,
                },
                {
                    name: 'price',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
