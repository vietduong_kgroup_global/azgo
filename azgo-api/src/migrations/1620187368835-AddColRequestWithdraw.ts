import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColRequestWithdraw1620187368835 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_request_withdraw` ADD COLUMN `request_code` VARCHAR(255) AFTER `uuid`");
        await queryRunner.query("ALTER TABLE `az_request_withdraw` ADD COLUMN `reason_cancel` VARCHAR(255) COMMENT 'Ly do huy (Neu huy)' AFTER `type`");
        await queryRunner.query("ALTER TABLE `az_request_withdraw` ADD COLUMN `reason_decline` VARCHAR(255) COMMENT 'Ly do tu choi (Neu tu choi)' AFTER `type`");
        await queryRunner.query("ALTER TABLE `az_request_withdraw` DROP COLUMN `reson_cancel`");
        await queryRunner.query("ALTER TABLE `az_request_withdraw` DROP COLUMN `reson_decline`");
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
