import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColVehicleScheduleMigration1587365431482 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_vehicle_schedules', [
            new TableColumn({
                name: 'end_hour',
                type: 'varchar',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
