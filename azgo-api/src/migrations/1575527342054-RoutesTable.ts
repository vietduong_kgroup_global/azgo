import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class RoutesTable1575527342054 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_routes',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'name',
                    type: 'varchar',
                },
                {
                    name: 'distance',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'start_point',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'end_point',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'provider_id',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'router',
                    type: 'json',
                    isNullable: true,
                },
                {
                    name: 'vehicle_type_id',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'base_price',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'price_per_km',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_routes');
    }

}
