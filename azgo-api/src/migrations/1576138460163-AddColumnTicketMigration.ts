import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColumnTicketMigration1576138460163 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const checkseatName = await queryRunner.hasColumn('az_ticket', 'seat_name');
        const checkqrCode = await queryRunner.hasColumn('az_ticket', 'qr_code');
        const checkqrCodeStatus = await queryRunner.hasColumn('az_ticket', 'qr_code_status');
        if (!checkseatName){
            await queryRunner.addColumn('az_ticket',
                new TableColumn({
                    name: 'seat_name',
                    type: 'varchar',
                    isNullable: true,
                }),
            );
        }

        if (!checkseatName){
            await queryRunner.addColumn('az_ticket',
                new TableColumn({
                    name: 'qr_code',
                    type: 'varchar',
                    isNullable: true,
                }),
            );
        }

        if (!checkseatName){
            await queryRunner.addColumn('az_ticket',
                new TableColumn({
                    name: 'qr_code_status',
                    type: 'smallint',
                    default: 1,
                    isNullable: true,
                }),
            );
        }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
