import { MigrationInterface, QueryRunner } from 'typeorm';
export class ModifyTour1611824510663 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("ALTER TABLE `az_tour` MODIFY COLUMN `company` json NULL");
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
