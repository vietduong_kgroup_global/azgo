import { MigrationInterface, QueryRunner } from 'typeorm';

export class ModifyTableTourOrderLog1611300743060 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query('ALTER TABLE `az_tour_order_logs` ADD COLUMN `company` VARCHAR(255) NULL COMMENT "ten cong ty" AFTER `data`');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
