import {MigrationInterface, QueryRunner, Table} from 'typeorm';

export class CreateTableBannerManagementMigration1590133398328 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_banner_management',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'banner',
                    type: 'varchar',
                },
                {
                    name: 'image',
                    type: 'json',
                },
                {
                    name: 'status',
                    type: 'smallint',
                    default: 1,
                    comment: '1: Active, 2: Inactive',
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
