import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class AddColumnToRoutes1575864255679 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const check = await queryRunner.hasColumn('az_routes', 'vehicle_type_id');
        if (check){
            await queryRunner.dropColumn('az_routes',  'vehicle_type_id');
        }

        await queryRunner.addColumn('az_routes',
            new TableColumn({
                name: 'vehicle_id',
                type: 'int',
                default: null,
                isNullable: true,
            }),
        );

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
