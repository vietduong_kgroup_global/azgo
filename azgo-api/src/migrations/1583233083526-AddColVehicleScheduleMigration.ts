import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColVehicleScheduleMigration1583233083526 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_vehicle_schedules', [
            new TableColumn({
                name: 'sub_driver',
                type: 'varchar',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
