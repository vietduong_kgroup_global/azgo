import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
export class CreateTableBank1615275314353 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        // await queryRunner.query("ALTER TABLE `az_area` MODIFY COLUMN `uuid` VARCHAR(255) AFTER `id`");
        // await queryRunner.query("ALTER TABLE `az_area` ADD INDEX( `uuid`)");

        await queryRunner.query("DROP TABLE IF EXISTS `az_bank`");
        await queryRunner.createTable(new Table({
            name: 'az_bank',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                }, 
                {
                    name: 'uuid',
                    type: 'varchar',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: 'en_name',
                    type: 'varchar',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'vn_name',
                    type: 'varchar', 
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'bank_id',
                    type: 'varchar',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'atm_bin',
                    type: 'varchar',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'card_length',
                    type: 'int',
                    default: 0,
                    isNullable: true,
                },
                {
                    name: 'short_name',
                    type: 'varchar',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'bank_code',
                    type: 'varchar', 
                    isNullable: true,
                },
                {
                    name: 'type',
                    type: 'varchar',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'napas_supported',
                    type: 'boolean',
                    default: false,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE IF EXISTS `az_bank`");
    }
}
