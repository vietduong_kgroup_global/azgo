import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColTableHistoryWallet1616655908210 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_wallet_histories` ADD COLUMN `tag` VARCHAR(255) NULL DEFAULT NULL COMMENT 'đinh danh y nghĩa cua giao dich' AFTER `type`");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_wallet_histories` DROP COLUMN `tag`");
    }

}
