import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColOrderVanBagac1600076729617 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_van_bagac_orders', [
            new TableColumn({
                name: 'rate_of_charge',
                type: 'float',
                default: 0,
                comment: 'Tỷ lệ phần % doanh thu của AZGO',
            }),

            new TableColumn({
                name: 'fee_tax',
                type: 'float',
                default: 0,
                comment: 'Chiết khấu % từ total price order',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
