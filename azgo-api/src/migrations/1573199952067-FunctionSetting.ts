import {MigrationInterface, QueryRunner, Table } from 'typeorm';

export class FunctionSetting1573199952067 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_function_settings',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'name',
                    type: 'varchar',
                    length: '50',
                },
                {
                    name: 'key',
                    type: 'varchar',
                    length: '50',
                    isUnique: true,
                },
                {
                    name: 'action',
                    type: 'json',
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_function_settings');
    }

}
