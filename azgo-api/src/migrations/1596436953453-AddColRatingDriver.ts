import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColRatingDriver1596436953453 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_rating_drivers', [
            new TableColumn({
                name: 'az_bike_car_order_id',
                type: 'int',
                isNullable: true,
            }),
            new TableColumn({
                name: 'az_van_bagac_order_id',
                type: 'int',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
