
import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColTableAccount1615789002674 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_accounts` DROP COLUMN `account_number`"); 
        await queryRunner.query("ALTER TABLE `az_accounts` DROP COLUMN `account_user_name`"); 
        await queryRunner.query("ALTER TABLE `az_accounts` DROP COLUMN `account_branch`"); 
        await queryRunner.query("ALTER TABLE `az_accounts` DROP COLUMN `account_name`"); 

        await queryRunner.query("ALTER TABLE `az_accounts` ADD COLUMN `account_bank_number` VARCHAR(50) NULL AFTER `id`"); 
        await queryRunner.query("ALTER TABLE `az_accounts` ADD COLUMN `account_bank_user_name` VARCHAR(50) NULL AFTER `id`"); 
        await queryRunner.query("ALTER TABLE `az_accounts` ADD COLUMN `account_bank_branch` VARCHAR(50) NULL AFTER `id`"); 
        await queryRunner.query("ALTER TABLE `az_accounts` ADD COLUMN `account_bank_name` VARCHAR(50) NULL AFTER `id`"); 
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
