import {getRepository, MigrationInterface, QueryRunner} from 'typeorm';

export class DataVehicleGroup1577176534251 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await getRepository('az_vehicle_groups').save([
            {
                name: 'BUS',
            },
            {
                name: 'BIKE/CAR',
            },
            {
                name: 'VAN/BAGAC',
            },
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
