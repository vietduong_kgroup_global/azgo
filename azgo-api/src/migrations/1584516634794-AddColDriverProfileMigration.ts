import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColDriverProfileMigration1584516634794 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_driver_profile', [
            new TableColumn({
                name: 'provider_id',
                type: 'int',
                isNullable: true,
                comment: 'id provider info',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
