import { MigrationInterface, QueryRunner, getRepository } from 'typeorm';

export class ModifyDataUser1609814702686 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('UPDATE `az_users` SET type=1, roles=\'[1]\' WHERE user_id=\'5fdc191c-cc39-477b-b813-1d454c08f04c\'');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DELETE FROM `az_users` WHERE `email`=\'admin@gmail.com\'');
    }

}
