import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColTourOrder1616040424509 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_tour_orders` DROP COLUMN `reason_cancel`");
        await queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `customer_cancel_reason` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Ly do huy cua khach hang' AFTER `is_paid`");
        await queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `driver_cancel_reason` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Ly do huy cua tai xe' AFTER `is_paid`");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_tour_orders` DROP COLUMN `customer_cancel_reason`");
        await queryRunner.query("ALTER TABLE `az_tour_orders` DROP COLUMN `driver_cancel_reason`");
    }

}
