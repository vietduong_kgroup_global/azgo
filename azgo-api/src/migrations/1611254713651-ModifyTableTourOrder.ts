import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class ModifyTableTourOrder1611254713651 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `distance` DOUBLE NULL COMMENT 'Số km' AFTER `customer_id`");
        queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `order_price` DOUBLE NULL COMMENT 'Giá đơn hàng' AFTER `customer_id`");
        queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `payment_code` VARCHAR(255) NULL COMMENT 'Mã giao dịch' AFTER `customer_id`");
        queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `company` VARCHAR(255) NULL COMMENT 'Tên/Mã công ty' AFTER `customer_id`");
        queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `fly_number` VARCHAR(255) NULL COMMENT 'Mã số chuyến bay' AFTER `customer_id`");
        queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `customer_gender` INT(11) NULL COMMENT 'Giới tính khách hàng (MALE: 1, FEMALE: 2)' AFTER `customer_id`");
        queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `customer_last_name` VARCHAR(255) NULL COMMENT 'Tên khách hàng' AFTER `customer_id`");
        queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `customer_first_name` VARCHAR(255) NULL COMMENT 'Họ khách hàng' AFTER `customer_id`");
        queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `customer_mobile` VARCHAR(255) NULL COMMENT 'Số điện thoại khách hàng' AFTER `customer_id`");
        queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `address_destination` VARCHAR(255) NULL COMMENT 'Họ khách hàng' AFTER `long_pick_location`");
        queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `lat_destination_location` DOUBLE NULL COMMENT 'Giới tính khách hàng (MALE: 1, FEMALE: 2)' AFTER `long_pick_location`");
        queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `long_destination_location` DOUBLE NULL COMMENT 'Tên khách hàng' AFTER `long_pick_location`");
        queryRunner.query("ALTER TABLE `az_tour_orders` MODIFY COLUMN `status` INT(11) NULL DEFAULT 1 COMMENT '1: Chờ xác nhận , 2: Tài xế xác nhận, 3: Khách hàng xác nhận, 4: Tài xế huỷ, 5: Khách hàng huỷ, 6: Hoàn tất'");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
