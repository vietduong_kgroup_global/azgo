import {MigrationInterface, QueryRunner} from 'typeorm';

export class AddColVehicleBrandIdTableVehicle1577243824379 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `az_vehicle` ADD COLUMN `vehicle_brand_id` int(11) NULL, ADD CONSTRAINT `FK_vehicle_brand_vehicle` FOREIGN KEY (`vehicle_brand_id`) REFERENCES `az_vehicle_brand` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
