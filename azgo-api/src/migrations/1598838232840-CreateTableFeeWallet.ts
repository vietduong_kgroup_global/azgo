import {MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey} from 'typeorm';

export class CreateTableFeeWallet1598838232840 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_wallets',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'driver_id',
                    type: 'int',
                },
                {
                    name: 'cash_wallet',
                    type: 'double',
                    default: 0,
                },
                {
                    name: 'fee_wallet',
                    type: 'double',
                    default: 0,
                },
                {
                    name: 'status',
                    type: 'smallint',
                    default: 1,
                    comment: '1: Active, 2: Inactive',
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                },
            ],
        }), true);
        await queryRunner.createForeignKey('az_wallets', new TableForeignKey({
            columnNames: ['driver_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_users',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
