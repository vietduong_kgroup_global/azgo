import {MigrationInterface, QueryRunner} from 'typeorm';

export class UpdateColumnEmailUser1576135746882 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `az_users` MODIFY COLUMN `email` VARCHAR(255)');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
