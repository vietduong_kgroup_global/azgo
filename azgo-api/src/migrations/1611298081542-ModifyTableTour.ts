import {MigrationInterface, QueryRunner} from 'typeorm';

export class ModifyTableTour1611298081542 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query('ALTER TABLE `az_tour` ADD COLUMN `company` json NULL COMMENT "ten cong ty" AFTER `description`');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
