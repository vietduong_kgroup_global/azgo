import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class RmAndAddColumnProvidersInfoMigration1576045711547 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('az_providers_info', 'name');
        await queryRunner.dropColumn('az_providers_info', 'phone');
        await queryRunner.dropColumn('az_providers_info', 'country_code');
        await queryRunner.dropColumn('az_providers_info', 'status');
        await queryRunner.dropColumn('az_providers_info', 'user_id');
        await queryRunner.dropColumn('az_providers_info', 'vehicle_quantity');
        await queryRunner.dropColumn('az_providers_info', 'created_at');
        await queryRunner.dropColumn('az_providers_info', 'updated_at');
        await queryRunner.dropColumn('az_providers_info', 'deleted_at');
        await queryRunner.addColumns('az_providers_info', [
            new TableColumn({
                name: 'user_id',
                type: 'int',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'first_name',
                type: 'varchar',
                length: '50',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'last_name',
                type: 'varchar',
                length: '50',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'full_name',
                type: 'varchar',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'provider_name',
                type: 'varchar',
                length: '255',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'representative',
                type: 'varchar',
                length: '255',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'phone_representative',
                type: 'varchar',
                length: '255',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'gender',
                type: 'smallint',
                default: 1,
            }),
            new TableColumn({
                name: 'birthday',
                type: 'date',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'vehicle_quantity',
                type: 'int',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'cmnd',
                type: 'varchar',
                length: '50',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'address',
                type: 'text',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'image_profile',
                type: 'json',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'front_of_cmnd',
                type: 'json',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'back_of_cmnd',
                type: 'json',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'created_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
            }),
            new TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
                onUpdate: 'CURRENT_TIMESTAMP',
            }),
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
        ]);
        await queryRunner.query('ALTER TABLE `az_providers_info` ADD CONSTRAINT `FK_providers_info_user` FOREIGN KEY (`user_id`) REFERENCES `az_users`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
