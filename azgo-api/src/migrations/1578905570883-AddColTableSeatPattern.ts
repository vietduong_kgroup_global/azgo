import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTableSeatPattern1578905570883 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_seat_pattern', [
            new TableColumn({
                name: 'floor',
                type: 'int',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
