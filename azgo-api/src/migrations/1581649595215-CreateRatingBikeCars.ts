import {MigrationInterface, QueryRunner, Table, TableForeignKey} from 'typeorm';

export class CreateRatingBikeCars1581649595215 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_rating_bike_cars',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'driver_id',
                    type: 'int',
                },
                {
                    name: 'customer_id',
                    type: 'int',
                },
                {
                    name: 'content',
                    type: 'text',
                },
                {
                    name: 'point',
                    type: 'decimal',
                    isNullable: false,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);

        await queryRunner.createForeignKey('az_rating_bike_cars', new TableForeignKey({
            columnNames: ['driver_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_driver_profile',
            onDelete: 'CASCADE',
        }));

        await queryRunner.createForeignKey('az_rating_bike_cars', new TableForeignKey({
            columnNames: ['customer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_customer_profile',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
