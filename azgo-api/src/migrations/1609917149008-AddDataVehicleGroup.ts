import {getRepository, MigrationInterface, QueryRunner, TableForeignKey} from 'typeorm';

export class AddDataVehicleGroup1609917149008 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await getRepository('az_vehicle_groups').save([
            {
                name: 'TOUR',
            },
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
