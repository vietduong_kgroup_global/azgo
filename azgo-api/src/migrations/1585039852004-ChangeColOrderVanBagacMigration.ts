import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class ChangeColOrderVanBagacMigration1585039852004 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('az_van_bagac_orders', 'images');
        await queryRunner.addColumns('az_van_bagac_orders', [
            new TableColumn({
                name: 'images',
                type: 'json',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
