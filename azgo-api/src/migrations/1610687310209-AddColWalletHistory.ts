import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from 'typeorm';

export class AddColWalletHistory1610687310209 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_wallet_histories', [
            new TableColumn({
                name: 'user_update_id',
                type: 'int',
                isNullable: true,
                comment: '',
            }),
        ]);
        await queryRunner.createForeignKey('az_wallet_histories', new TableForeignKey({
            columnNames: ['user_update_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_users',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
