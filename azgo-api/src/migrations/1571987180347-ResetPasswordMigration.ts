import { MigrationInterface, QueryRunner } from 'typeorm';

export class ResetPasswordMigration1571987180347 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            'CREATE TABLE `az_password_resets` (' +
            '`id` int NOT NULL AUTO_INCREMENT,' +
            '`token` varchar(255) DEFAULT NULL,' +
            '`email` varchar(255) NOT NULL,' +
            '`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,' +
            '`updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY (`id`))',
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DROP TABLE IF EXISTS `az_password_resets`');
    }

}
