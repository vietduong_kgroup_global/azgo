import {MigrationInterface, QueryRunner} from "typeorm";

export class AlterUserTableWithName1616140316528 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_customer_profile` MODIFY COLUMN `first_name` varchar(100)"); 
        await queryRunner.query("ALTER TABLE `az_customer_profile` MODIFY COLUMN `last_name` varchar(100)"); 
        await queryRunner.query("ALTER TABLE `az_customer_profile` MODIFY COLUMN `last_name` varchar(100)"); 
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
