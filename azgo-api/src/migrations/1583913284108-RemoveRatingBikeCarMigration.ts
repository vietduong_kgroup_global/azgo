import {MigrationInterface, QueryRunner} from 'typeorm';

export class RemoveRatingBikeCarMigration1583913284108 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_rating_bike_cars');
        const roleForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('driver_id') !== -1);
        const userForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('customer_id') !== -1);
        await queryRunner.dropForeignKey('az_rating_bike_cars', roleForeignKey);
        await queryRunner.dropForeignKey('az_rating_bike_cars', userForeignKey);
        await queryRunner.dropTable('az_rating_bike_cars');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
