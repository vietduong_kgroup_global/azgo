import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class ChangeColContentRatingDriver1596513937410 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        if (await queryRunner.hasColumn('az_rating_drivers', 'content')) {
            await queryRunner.dropColumn('az_rating_drivers',  'content');
        }
        await queryRunner.addColumns('az_rating_drivers', [
            new TableColumn({
                name: 'content',
                type: 'varchar',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
