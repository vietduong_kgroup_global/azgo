import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class UpdateTableVehicleTypes1582513989445 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_vehicle_types', [
            new TableColumn({
                name: 'price_per_km',
                type: 'float',
            }),
            new TableColumn({
                name: 'base_price',
                type: 'float',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
