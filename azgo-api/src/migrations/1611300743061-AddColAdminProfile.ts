import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddColAdminProfile1611300743061 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query('ALTER TABLE `az_admin_profile` ADD COLUMN `company` VARCHAR(255) NULL COMMENT "ten cong ty" AFTER `address`');
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
