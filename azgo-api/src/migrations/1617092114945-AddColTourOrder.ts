import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColTourOrder1617092114945 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `finish_time` TIMESTAMP NULL DEFAULT NULL COMMENT 'Thi gian ket thuc chuyen' AFTER `start_time`");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_tour_orders` DROP COLUMN `finish_time`");
    }

}
