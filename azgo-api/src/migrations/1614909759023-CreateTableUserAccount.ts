import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
export class CreateTableUserAccount1614909759023 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        // await queryRunner.query("ALTER TABLE `az_area` MODIFY COLUMN `uuid` VARCHAR(255) AFTER `id`");
        // await queryRunner.query("ALTER TABLE `az_area` ADD INDEX( `uuid`)");

        await queryRunner.query("DROP TABLE IF EXISTS `az_user_accounts`");
        await queryRunner.createTable(new Table({
            name: 'az_user_accounts',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                }, 
                {
                    name: 'user_id',
                    type: 'int',
                },
                {
                    name: 'account_id',
                    type: 'int',
                },
            ],
        }), true);
        await queryRunner.createForeignKey('az_user_accounts', new TableForeignKey({
            columnNames: ['account_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_accounts',
            onDelete: 'CASCADE',
        }));

        await queryRunner.createForeignKey('az_user_accounts', new TableForeignKey({
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_users',
            onDelete: 'CASCADE',
        }));
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE IF EXISTS `az_user_accounts`");
    }
}
