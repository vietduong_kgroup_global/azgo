import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class createTableVehicleRunning1575866008287 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_vehicle_running',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'schedule_id',
                    type: 'int',
                },
                {
                    name: 'vehicle_id',
                    type: 'int',
                },
                {
                    name: 'status',
                    type: 'smallint',
                    isNullable: true,
                    default: 1,
                },
                {
                    name: 'router',
                    type: 'json',
                    isNullable: true,
                },
                {
                    name: 'seat_map',
                    type: 'json',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_vehicle_running');
    }

}
