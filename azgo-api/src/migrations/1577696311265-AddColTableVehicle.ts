import {MigrationInterface, QueryRunner} from 'typeorm';

export class AddColTableVehicle1577696311265 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `az_vehicle` ADD COLUMN `seat_pattern_id` int(11) NULL, ADD CONSTRAINT `FK_seat_pattern_vehicle` FOREIGN KEY (`seat_pattern_id`) REFERENCES `az_seat_pattern` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
