import {MigrationInterface, QueryRunner} from 'typeorm';

export class AlterTableUser1610445609209 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `az_tour_orders` MODIFY COLUMN `lat_pick_location` DOUBLE NULL');
        await queryRunner.query('ALTER TABLE `az_tour_orders` MODIFY COLUMN `lat_pick_location` DOUBLE NULL');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
