import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColDeleteAtAllEntityMigration1587353449516 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_admin_profile', [
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
        ]);
        await queryRunner.addColumns('az_area', [
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
        ]);
        await queryRunner.addColumns('az_bike_car_orders', [
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
            new TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
                onUpdate: 'CURRENT_TIMESTAMP',
            }),
        ]);
        await queryRunner.addColumns('az_book_shipping', [
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
            new TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
                onUpdate: 'CURRENT_TIMESTAMP',
            }),
        ]);
        await queryRunner.addColumns('az_customer_profile', [
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
        ]);
        await queryRunner.addColumns('az_custom_charges', [
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
            new TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
                onUpdate: 'CURRENT_TIMESTAMP',
            }),
        ]);
        await queryRunner.addColumns('az_driver_profile', [
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
        ]);
        await queryRunner.addColumns('az_promotions', [
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
            new TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
                onUpdate: 'CURRENT_TIMESTAMP',
            }),
        ]);
        await queryRunner.addColumns('az_service_others', [
            new TableColumn({
                name: 'created_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
            }),
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
            new TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
                onUpdate: 'CURRENT_TIMESTAMP',
            }),
        ]);
        await queryRunner.addColumns('az_ticket', [
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
            new TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
                onUpdate: 'CURRENT_TIMESTAMP',
            }),
        ]);
        await queryRunner.addColumns('az_routes', [
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
            new TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
                onUpdate: 'CURRENT_TIMESTAMP',
            }),
        ]);
        await queryRunner.addColumns('az_van_bagac_orders', [
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
            new TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
                onUpdate: 'CURRENT_TIMESTAMP',
            }),
        ]);
        await queryRunner.addColumns('az_vehicle', [
            new TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
                onUpdate: 'CURRENT_TIMESTAMP',
            }),
        ]);
        await queryRunner.addColumns('az_vehicle_running', [
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
            new TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
                onUpdate: 'CURRENT_TIMESTAMP',
            }),
        ]);
        await queryRunner.addColumns('az_vehicle_schedules', [
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
            new TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
                onUpdate: 'CURRENT_TIMESTAMP',
            }),
        ]);
        await queryRunner.addColumns('az_vehicle_types', [
            new TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
                onUpdate: 'CURRENT_TIMESTAMP',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
