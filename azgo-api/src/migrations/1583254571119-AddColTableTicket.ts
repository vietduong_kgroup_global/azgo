import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTableTicket1583254571119 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_ticket', [
            new TableColumn({
                name: 'ticket_code',
                type: 'varchar',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
