import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddAreaDriverAdmin1617701295534 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_driver_profile` ADD COLUMN `area_uuid` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Khu vuc' AFTER `user_id`");
        await queryRunner.query("ALTER TABLE `az_admin_profile` ADD COLUMN `area_uuid` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Khu vuc' AFTER `user_id`");
        await queryRunner.createForeignKey('az_driver_profile', new TableForeignKey({
            columnNames: ['area_uuid'],
            referencedColumnNames: ['uuid'],
            referencedTableName: 'az_area',
            onDelete: 'CASCADE',
        }));
        await queryRunner.createForeignKey('az_admin_profile', new TableForeignKey({
            columnNames: ['area_uuid'],
            referencedColumnNames: ['uuid'],
            referencedTableName: 'az_area',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_driver_profile` DROP COLUMN `email`");
        await queryRunner.query("ALTER TABLE `az_admin_profile` DROP COLUMN `email`");
    }

}