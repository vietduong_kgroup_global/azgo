import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColAreaMigration1586773879522 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_area', [
            new TableColumn({
                name: 'provider_id',
                type: 'int',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
