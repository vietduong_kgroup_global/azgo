import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTicketMigration1581413094297 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_ticket', [
            new TableColumn({
                name: 'floor',
                type: 'int',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
