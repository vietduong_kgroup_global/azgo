import {MigrationInterface, QueryRunner, Table, TableForeignKey} from 'typeorm';

export class VehicleDriverMigration1577330035628 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_vehicle_driver',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'driver_id',
                    type: 'int',
                },
                {
                    name: 'vehicle_id',
                    type: 'int',
                },
                // {
                //     name: 'created_at',
                //     type: 'timestamp',
                //     isNullable: true,
                //     default: 'CURRENT_TIMESTAMP',
                // },
                // {
                //     name: 'updated_at',
                //     type: 'timestamp',
                //     isNullable: true,
                //     default: 'CURRENT_TIMESTAMP',
                //     onUpdate: 'CURRENT_TIMESTAMP',
                // },
                // {
                //     name: 'deleted_at',
                //     type: 'timestamp',
                //     isNullable: true,
                // },
            ],
        }), true);

        // await queryRunner.createForeignKey('az_vehicle_driver', new TableForeignKey({
        //     columnNames: ['driver_id'],
        //     referencedColumnNames: ['id'],
        //     referencedTableName: 'az_driver_profile',
        //     onDelete: 'CASCADE',
        // }));
        // await queryRunner.createForeignKey('az_vehicle_driver', new TableForeignKey({
        //     columnNames: ['vehicle_id'],
        //     referencedColumnNames: ['id'],
        //     referencedTableName: 'az_vehicle',
        //     onDelete: 'CASCADE',
        // }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_vehicle_driver');
        const dirverForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('driver_id') !== -1);
        const vehicleForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('vehicle_id') !== -1);
        await queryRunner.dropForeignKey('az_vehicle_driver', dirverForeignKey);
        await queryRunner.dropForeignKey('az_vehicle_driver', vehicleForeignKey);
        await queryRunner.dropTable('az_vehicle_driver');

    }

}
