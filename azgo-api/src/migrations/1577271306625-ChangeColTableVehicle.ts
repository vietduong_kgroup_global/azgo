import {MigrationInterface, QueryRunner} from 'typeorm';

export class ChangeColTableVehicle1577271306625 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `az_vehicle` MODIFY COLUMN `start_point` varchar(255) NULL');
        await queryRunner.query('ALTER TABLE `az_vehicle` MODIFY COLUMN `end_point` varchar(255) NULL');
        await queryRunner.query('ALTER TABLE `az_vehicle` MODIFY COLUMN `pick_up_point` json NULL');
        await queryRunner.query('ALTER TABLE `az_vehicle` MODIFY COLUMN `status` smallint NULL');
        await queryRunner.query('ALTER TABLE `az_vehicle` MODIFY COLUMN `point` json NULL');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
