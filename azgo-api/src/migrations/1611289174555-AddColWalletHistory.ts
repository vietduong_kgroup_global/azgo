import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddColWalletHistory1611289174555 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("ALTER TABLE `az_wallet_histories` ADD COLUMN `order_code` VARCHAR(255) NULL COMMENT 'Mã đơn hàng' AFTER `payment_status`");
        queryRunner.query("ALTER TABLE `az_wallet_histories` ADD COLUMN `payment_code` VARCHAR(255) NULL COMMENT 'Mã giao dịch' AFTER `payment_status`");
        queryRunner.query("ALTER TABLE `az_tour` ADD COLUMN `is_feature` SMALLINT(6) DEFAULT 0 NULL COMMENT 'Tour nổi bật (1: Có , 0: Không)' AFTER `is_round_trip`");
        queryRunner.query("ALTER TABLE `az_tour_orders` DROP COLUMN `is_feature`");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
