import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColBikeCarOrder1596418761024 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_bike_car_orders', [
            new TableColumn({
                name: 'flag_rating_customer',
                type: 'smallint',
                default: 1,
                comment: '1: Not yet rated, 2: Have evaluated',
            }),
            new TableColumn({
                name: 'flag_rating_driver',
                type: 'smallint',
                default: 1,
                comment: '1: Not yet rated, 2: Have evaluated',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
