import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTableVehicle1578630796907 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_vehicle', [
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
