import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class addParentIdForUserTable1573531769004 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('az_users', new TableColumn({
            name: 'parent_id',
            type: 'int',
            isNullable: true,
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('az_users', 'parent_id');
    }

}
