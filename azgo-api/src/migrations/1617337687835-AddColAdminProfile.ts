import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColAdminProfile1617337687835 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_admin_profile` ADD COLUMN `phone` VARCHAR(255) NULL DEFAULT NULL COMMENT 'So dien thoai' AFTER `address`");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_admin_profile` DROP COLUMN `phone`");
    }

}