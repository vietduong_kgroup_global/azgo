import { MigrationInterface, QueryRunner } from 'typeorm';
export class ModifyNotification1611817414769 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("ALTER TABLE `az_notifications` MODIFY COLUMN `object_id` VARCHAR(255) NULL");

    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
