import {MigrationInterface, QueryRunner, Table, TableForeignKey} from 'typeorm';

export class SeatPatternMigration1575946861674 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_seat_pattern',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'provider_id',
                    type: 'int',
                },
                {
                    name: 'name',
                    type: 'varchar',
                },
                {
                    name: 'seat_map',
                    type: 'json',
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },

            ],
        }), true);
        await queryRunner.createForeignKey('az_seat_pattern', new TableForeignKey({
            columnNames: ['provider_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_providers_info',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_seat_pattern');
        const roleForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('provider_id') !== -1);
        await queryRunner.dropForeignKey('az_seat_pattern', roleForeignKey);
        await queryRunner.dropTable('az_seat_pattern');
    }

}
