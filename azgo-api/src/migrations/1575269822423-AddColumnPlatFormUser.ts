import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColumnPlatFormUser1575269822423 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_users', [
            new TableColumn({
                name: 'platform',
                type: 'smallint',
                default: null,
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumns('az_users', [
            new TableColumn({
                name: 'platform',
                type: 'smallint',
                default: null,
                isNullable: true,
            }),
        ]);
    }

}
