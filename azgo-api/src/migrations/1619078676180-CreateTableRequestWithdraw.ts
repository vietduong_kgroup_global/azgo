import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
export class CreateTableRequestWithdraw1619078676180 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_request_withdraw',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'uuid',
                    type: 'varchar',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: 'driver_id',
                    type: 'int',
                    default: null,
                    isNullable: false,
                },
                {
                    name: 'type',
                    type: 'smallint',
                    default: 1,
                    isNullable: true,
                    comment: "1: cash wallet, 2: orther",
                },
                {
                    name: 'reson_cancel',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                    comment: "Ly do huy (Neu huy)",
                },
                {
                    name: 'reson_decline',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                    comment: "Ly do tu choi (Neu tu choi)",
                },
                {
                    name: 'amount',
                    type: 'double',
                    default: 0,
                    isNullable: true,
                },
                {
                    name: 'payment_code',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                    comment: "Ma giao dich",
                },
                {
                    name: 'content',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                    comment: "Noi dung",
                },
                {
                    name: 'status',
                    type: 'smallint',
                    default: 1,
                    isNullable: true,
                },
                {
                    name: 'transfer_time',
                    type: 'timestamp',
                    isNullable: true,
                    comment: "Thoi gian chuyen tien",
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                },
            ],
        }), true);
        await queryRunner.createForeignKey('az_request_withdraw', new TableForeignKey({
            columnNames: ['driver_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_users',
            onDelete: 'CASCADE',
        }));
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE IF EXISTS `az_request_withdraw`");
    }
}
