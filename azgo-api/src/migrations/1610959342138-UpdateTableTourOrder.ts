import {MigrationInterface, QueryRunner} from 'typeorm';

export class UpdateTableTourOrder1610959342138 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `az_tour_orders` MODIFY COLUMN `driver_id` INT NULL');
        await queryRunner.query('ALTER TABLE `az_tour_orders` MODIFY COLUMN `customer_id` INT NULL');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
