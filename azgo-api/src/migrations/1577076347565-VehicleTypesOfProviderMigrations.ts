import {MigrationInterface, QueryRunner, Table} from 'typeorm';

export class VehicleTypesOfProviderMigrations1577076347565 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_vehicle_types_of_providers',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'provider_id',
                    type: 'int',
                },
                {
                    name: 'vehicle_type_id',
                    type: 'int',
                },
            ],
            indices: [
                {
                    columnNames: ['provider_id'],
                },
                {
                    columnNames: ['vehicle_type_id'],
                },
            ],
            foreignKeys: [
                {
                    columnNames: ['provider_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'az_providers_info',
                },
                {
                    columnNames: ['vehicle_type_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'az_vehicle_types',
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
