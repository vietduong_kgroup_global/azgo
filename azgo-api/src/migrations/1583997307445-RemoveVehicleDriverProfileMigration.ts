import {MigrationInterface, QueryRunner} from 'typeorm';

export class RemoveVehicleDriverProfileMigration1583997307445 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_driver_profile');
        const roleForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('vehicle_id') !== -1);
        await queryRunner.dropForeignKey('az_driver_profile', roleForeignKey);
        await queryRunner.dropColumn('az_driver_profile', 'vehicle_id');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
