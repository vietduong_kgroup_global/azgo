import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColVanBagacOrder1599709977192 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_van_bagac_orders', [
            new TableColumn({
                name: 'total_price',
                type: 'float',
                default: 0,
                comment: 'price + price services other ...',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
