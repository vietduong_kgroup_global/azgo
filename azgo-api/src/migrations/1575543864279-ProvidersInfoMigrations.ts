import {MigrationInterface, QueryRunner, Table} from 'typeorm';

export class ProvidersInfoMigrations1575543864279 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_providers_info',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'name',
                    type: 'varchar',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'phone',
                    type: 'varchar',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'country_code',
                    type: 'int',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'status',
                    type: 'smallint',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'vehicle_quantity',
                    type: 'int',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'user_id',
                    type: 'int',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_providers_info');
    }

}
