import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddColumnBlockUser1573617150666 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_users', [
            new TableColumn({
                name: 'block',
                type: 'datetime',
                default: null,
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumns('az_users', [
            new TableColumn({
                name: 'block',
                type: 'datetime',
                default: null,
                isNullable: true,
            }),
        ]);
    }

}
