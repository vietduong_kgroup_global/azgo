import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class RmColumnAndAddColumnVehicleType1575620495359 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('az_vehicle_types',  'created_at');
        await queryRunner.addColumn('az_vehicle_types',
            new TableColumn({
                name: 'created_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
