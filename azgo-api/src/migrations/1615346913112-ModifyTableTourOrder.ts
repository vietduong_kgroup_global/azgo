import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class ModifyTableTourOrder1615346913112 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `time` INT NULL DEFAULT 0 COMMENT 'so ngay thue' AFTER `extra_time`");
        await queryRunner.query("ALTER TABLE `az_tour` ADD COLUMN `vehicle_type_id` INT NULL DEFAULT NULL AFTER `time_rate`");
        await queryRunner.createForeignKey('az_tour', new TableForeignKey({
            columnNames: ['vehicle_type_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_vehicle_types',
            onDelete: 'CASCADE',
        }));

        // Add comment table
        await queryRunner.query("ALTER TABLE `az_tour` MODIFY COLUMN `type` VARCHAR(255) NULL DEFAULT 'fix_price' COMMENT 'Kiểu tour [fix_price, distance_price, time_price]';");
        await queryRunner.query("ALTER TABLE `az_tour` MODIFY COLUMN `time` INT(11) NULL DEFAULT 0 COMMENT 'Thời gian tour theo phút';");
        await queryRunner.query("ALTER TABLE `az_tour` MODIFY COLUMN `extra_time_price` INT(11) NULL DEFAULT 0 COMMENT 'Tiền phụ thu theo giờ';");
        await queryRunner.query("ALTER TABLE `az_tour_orders` MODIFY COLUMN `extra_time` INT(11) NULL DEFAULT 0 COMMENT 'Tiền phụ thu theo giờ';");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_tour_orders` DROP COLUMN `time`");
        const table = await queryRunner.getTable('az_tour');
        const feeForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('vehicle_type_id') !== -1);
        await queryRunner.dropForeignKey('az_tour', feeForeignKey);
        await queryRunner.query("ALTER TABLE `az_tour` DROP COLUMN `vehicle_type_id`");
    }

}
