import {MigrationInterface, QueryRunner, Table} from 'typeorm';

export class CreateBookShipping1581567978249 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_book_shipping',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'user_id',
                    type: 'int',
                },
                {
                    name: 'schedule_id',
                    type: 'int',
                },
                {
                    name: 'status',
                    type: 'smallint',
                    isNullable: false,
                    default: 1,
                },
                {
                    name: 'payment_method',
                    type: 'smallint',
                    isNullable: true,
                },
                {
                    name: 'payment_status',
                    type: 'smallint',
                    isNullable: true,
                },
                {
                    name: 'price',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'address_ship',
                    type: 'varchar',
                    length: '255',
                },
                {
                    name: 'receiver',
                    type: 'varchar',
                    length: '255',
                },
                {
                    name: 'phone',
                    type: 'varchar',
                    length: '255',
                },
                {
                    name: 'commodity_information',
                    type: 'json',
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'qr_code',
                    type: 'varchar',
                    length: '255',
                },
                {
                    name: 'qr_code_status',
                    type: 'smallint',
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
