import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColBookShippingMigration1587439087329 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_book_shipping', [
            new TableColumn({
                name: 'destination',
                type: 'varchar',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
