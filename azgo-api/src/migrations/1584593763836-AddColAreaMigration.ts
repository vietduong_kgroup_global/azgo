import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColAreaMigration1584593763836 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_area', [
            new TableColumn({
                name: 'coordinates',
                type: 'json',
                isNullable: true,
                comment: 'lat, long',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

