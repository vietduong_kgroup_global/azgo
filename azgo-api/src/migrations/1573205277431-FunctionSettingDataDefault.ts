import {getRepository, MigrationInterface, QueryRunner} from 'typeorm';

export class FunctionSettingDataDefault1573205277431 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await getRepository('az_function_settings').save(
            [
                {
                    name: 'Users',
                    key: 'users',
                    action: {read: 0, create: 0, delete: 0, update: 0},
                },
                {
                    name: 'Roles',
                    key: 'roles',
                    action: {read: 0, create: 0, delete: 0, update: 0},
                },
                {
                    name: 'User Roles',
                    key: 'user_roles',
                    action: {read: 0, create: 0, delete: 0, update: 0},
                },
                {
                    name: 'Function settings',
                    key: 'function_settings',
                    action: {read: 0, create: 0, delete: 0, update: 0},
                },
            ],
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DELETE FROM `az_function_settings`');
    }

}
