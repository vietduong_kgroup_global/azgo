import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class createTableTicket1576045720172 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_ticket',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'user_id',
                    type: 'int',
                },
                {
                    name: 'schedule_id',
                    type: 'int',
                },
                {
                    name: 'status',
                    type: 'smallint',
                    isNullable: false,
                    default: 1,
                },
                {
                    name: 'payment_method',
                    type: 'smallint',
                    isNullable: true,
                },
                {
                    name: 'payment_status',
                    type: 'smallint',
                    isNullable: true,
                },
                {
                    name: 'price',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_ticket');
    }

}
