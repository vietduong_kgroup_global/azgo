import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColVehicleGroupIdTableVehicleType1577174693556 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `az_vehicle_types` ADD COLUMN `vehicle_group_id` int(11) NULL, ADD CONSTRAINT `FK_vehicle_type_vehicle_groups` FOREIGN KEY (`vehicle_group_id`) REFERENCES `az_vehicle_groups` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
