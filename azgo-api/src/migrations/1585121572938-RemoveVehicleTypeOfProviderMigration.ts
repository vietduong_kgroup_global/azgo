import {MigrationInterface, QueryRunner} from 'typeorm';

export class RemoveVehicleTypeOfProviderMigration1585121572938 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_vehicle_types_of_providers');
        const providerF = await table.foreignKeys.find(fk => fk.columnNames.indexOf('provider_id') !== -1);
        const vehicleTypeF = await table.foreignKeys.find(fk => fk.columnNames.indexOf('vehicle_type_id') !== -1);
        await queryRunner.dropForeignKey('az_vehicle_types_of_providers', providerF);
        await queryRunner.dropForeignKey('az_vehicle_types_of_providers', vehicleTypeF);
        await queryRunner.dropTable('az_vehicle_types_of_providers');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
