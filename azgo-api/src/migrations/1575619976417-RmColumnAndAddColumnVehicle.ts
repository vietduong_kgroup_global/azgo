import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class RmColumnAndAddColumnVehicle1575619976417 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('az_vehicle',  'price');
        await queryRunner.dropColumn('az_vehicle',  'created_at');
        await queryRunner.addColumn('az_vehicle',
            new TableColumn({
                name: 'created_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_vehicle', [
            new TableColumn({
                name: 'price',
                type: 'float',
                isNullable: false,
            }),
        ]);
    }

}
