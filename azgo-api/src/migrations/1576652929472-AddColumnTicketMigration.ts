import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColumnTicketMigration1576652929472 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        /* Add column az_ticket */
        await queryRunner.addColumn('az_ticket',
            new TableColumn({
                name: 'luggage_info',
                type: 'varchar',
                isNullable: true,
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
