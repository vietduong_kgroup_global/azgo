import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTourOrder1609820952561 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_tour_orders', [
            new TableColumn({
                name: 'order_code',
                type: 'varchar',
                isNullable: true,
                comment: '',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
