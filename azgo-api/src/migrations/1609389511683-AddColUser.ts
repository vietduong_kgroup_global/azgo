import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColUser1609389511683 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_users', [
            new TableColumn({
                name: 'roles',
                type: 'json',
                isNullable: true,
                comment: 'mang type user',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
