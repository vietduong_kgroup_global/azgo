import {MigrationInterface, QueryRunner, Table} from 'typeorm';

export class DriverProfileMigration1575952422782 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_driver_profile',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'user_id',
                    type: 'int',
                },
                {
                    name: 'vehicle_id',
                    type: 'int',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'first_name',
                    type: 'varchar',
                    length: '50',
                },
                {
                    name: 'last_name',
                    type: 'varchar',
                    length: '50',
                },
                {
                    name: 'full_name',
                    type: 'varchar',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'gender',
                    type: 'smallint',
                    default: 1,
                },
                {
                    name: 'birthday',
                    type: 'date',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'cmnd',
                    type: 'varchar',
                    length: '50',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'address',
                    type: 'text',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'image_profile',
                    type: 'json',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'front_of_cmnd',
                    type: 'json',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'back_of_cmnd',
                    type: 'json',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
            ],
            indices: [
                {
                    columnNames: ['user_id'],
                },
                {
                    columnNames: ['vehicle_id'],
                },
            ],
            foreignKeys: [
                {
                    columnNames: ['user_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'az_users',
                },
                {
                    columnNames: ['vehicle_id'],
                    referencedColumnNames: ['id'],
                    referencedTableName: 'az_vehicle',
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DROP TABLE IF EXISTS `az_driver_profile`');
    }

}
