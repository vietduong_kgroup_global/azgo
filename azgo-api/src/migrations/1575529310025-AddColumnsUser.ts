import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColumnsUser1575529310025 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumns('az_users', [
            new TableColumn({
                name: 'middle_name',
                type: 'varchar',
                default: null,
                isNullable: true,
            }),
        ]);
        await queryRunner.addColumns('az_users', [
            new TableColumn({
                name: 'full_name',
                type: 'varchar',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'cmnd',
                type: 'varchar',
                length: '50',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'address',
                type: 'text',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'image_profile',
                type: 'json',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'front_of_cmnd',
                type: 'json',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'back_of_cmnd',
                type: 'json',
                default: null,
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_users', [
            new TableColumn({
                name: 'middle_name',
                type: 'varchar',
                default: null,
                isNullable: true,
            }),
        ]);
        await queryRunner.dropColumns('az_users', [
            new TableColumn({
                name: 'full_name',
                type: 'varchar',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'cmnd',
                type: 'varchar',
                length: '50',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'address',
                type: 'text',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'image_profile',
                type: 'json',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'front_of_cmnd',
                type: 'json',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'back_of_cmnd',
                type: 'json',
                default: null,
                isNullable: true,
            }),
        ]);
    }

}
