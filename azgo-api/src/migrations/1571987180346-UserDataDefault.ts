import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserDataDefault1571987180346 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('INSERT INTO `az_users` (`user_id`, `phone`, `country_code`, `email`, `password`,`status`, `first_name`, `last_name`) ' +
            'VALUES (\'5fdc191c-cc39-477b-b813-1d454c08f04c\',\'975716503\',\'84\', \'admin@gmail.com\',\'$2a$08$G99bKZbBj0xpnnLYroccHObRE.gSxy3P7FYQMRWUHXWBsSx3QQ6.G\',\'1\',\'User\',\'Admin\')');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DELETE FROM `az_users` WHERE `email`=\'admin@gmail.com\'');
    }

}
