import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColumnCompanyUser1574677758454 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_users', [
            new TableColumn({
                name: 'company',
                type: 'varchar',
                default: null,
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumns('az_users', [
            new TableColumn({
                name: 'company',
                type: 'varchar',
                default: null,
                isNullable: true,
            }),
        ]);
    }

}
