import { truncateSync } from 'fs';
import {MigrationInterface, QueryRunner, Table, TableForeignKey} from 'typeorm';

export class CreateTableTourOrder1609409107612 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_tour_orders',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'uuid',
                    type: 'varchar',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: 'tour_id',
                    type: 'int',
                    isNullable: false,
                },
                {
                    name: 'driver_id',
                    type: 'int',
                    isNullable: false,
                },
                {
                    name: 'customer_id',
                    type: 'int',
                    isNullable: false,
                },
                {
                    name: 'start_time',
                    type: 'timestamp',
                    default: null,
                    isNullable: true,
                },
                {
                    name: 'lat_pick_location',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'long_pick_location',
                    type: 'int',
                    isNullable: true,
                },
                {
                    name: 'extra_time',
                    type: 'int',
                    default: 0,
                    isNullable: true,
                },
                {
                    name: 'extra_money',
                    type: 'int',
                    default: 0,
                    isNullable: true,
                },
                {
                    name: 'payment_method',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'status',
                    type: 'smallint',
                    comment: '1: Chờ xác nhận , 2: Tài xế xác nhận, 3: Khách hàng xác nhận, 4: Tài xế huỷ, 5: Khách hàng huỷ, 6: Hoàn tất',
                    default: 1,
                    isNullable: false,
                },
                {
                    name: 'is_feature',
                    type: 'smallint',
                    default: 0,
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'CURRENT_TIMESTAMP',
                    isNullable: true,
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                },
            ],
        }), true);
        await queryRunner.createForeignKey('az_tour_orders', new TableForeignKey({
            columnNames: ['driver_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_users',
            onDelete: 'CASCADE',
        }));
        await queryRunner.createForeignKey('az_tour_orders', new TableForeignKey({
            columnNames: ['customer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_users',
            onDelete: 'CASCADE',
        }));
        await queryRunner.createForeignKey('az_tour_orders', new TableForeignKey({
            columnNames: ['tour_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_tour',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_tour_orders');
    }

}
