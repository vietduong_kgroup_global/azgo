import {MigrationInterface, QueryRunner, Table} from 'typeorm';

export class CreateTableBusOrders1577160266784 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_bus_orders',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'customer_uuid',
                    type: 'varchar',
                },
                {
                    name: 'driver_uuid',
                    type: 'varchar',
                },
                {
                    name: 'route_info',
                    type: 'json',
                    isNullable: true,
                },
                {
                    name: 'book_bus_reference',
                    type: 'varchar',
                },
                {
                    name: 'status',
                    type: 'smallint',
                    isNullable: false,
                    default: 1,
                },
                {
                    name: 'payment_method',
                    type: 'smallint',
                    isNullable: true,
                },
                {
                    name: 'payment_status',
                    type: 'smallint',
                    isNullable: true,
                },
                {
                    name: 'price',
                    type: 'float',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_bus_orders');
    }

}
