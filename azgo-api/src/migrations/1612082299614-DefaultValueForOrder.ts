import { MigrationInterface, QueryRunner } from 'typeorm';
export class DefaultValueForOrder1612082299614 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("ALTER TABLE `az_tour_orders` MODIFY COLUMN `payment_method` VARCHAR(255) NULL DEFAULT 'cash'");

    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
