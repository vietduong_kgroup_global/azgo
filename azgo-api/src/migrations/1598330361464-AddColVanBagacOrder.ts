import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColVanBagacOrder1598330361464 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_van_bagac_orders', [
            new TableColumn({
                name: 'start_time',
                type: 'varchar',
                isNullable: true,
                comment: 'Driver input giờ bắt đầu',
            }),
            new TableColumn({
                name: 'end_time',
                type: 'varchar',
                isNullable: true,
                comment: 'Driver input giờ kết thúc',
            }),
            new TableColumn({
                name: 'created_accept',
                type: 'timestamp',
                isNullable: true,
                comment: 'Thời gian approved',
            }),
            new TableColumn({
                name: 'created_moving',
                type: 'timestamp',
                isNullable: true,
                comment: 'Thời gian moving (khi driver nhấn bắt đầu)',
            }),
            new TableColumn({
                name: 'created_received',
                type: 'timestamp',
                isNullable: true,
                comment: 'Thời gian received',
            }),
            new TableColumn({
                name: 'created_deliveried',
                type: 'timestamp',
                isNullable: true,
                comment: 'Thời gian deliveried',
            }),
            new TableColumn({
                name: 'created_completed',
                type: 'timestamp',
                isNullable: true,
                comment: 'Thời gian completed',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
