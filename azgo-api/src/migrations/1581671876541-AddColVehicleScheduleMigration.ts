import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColVehicleScheduleMigration1581671876541 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_vehicle_schedules', [
            new TableColumn({
                name: 'status',
                type: 'int',
                default: 1,
                comment: 'Chua khoi hanh, 2: Da khoi hanh, 3 Ket thuc',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
