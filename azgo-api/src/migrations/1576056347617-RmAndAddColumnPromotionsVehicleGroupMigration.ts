import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from 'typeorm';

export class RmAndAddColumnPromotionsVehicleGroupMigration1576056347617 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        /* Remove FK and column promotions*/
        const table = await queryRunner.getTable('az_promotions');
        const roleForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('vehicle_group_id') !== -1);
        if (typeof roleForeignKey !== 'undefined') {
            await queryRunner.dropForeignKey('az_promotions', roleForeignKey);
        }

        if (await queryRunner.hasColumn('az_promotions', 'vehicle_group_id')) {
            await queryRunner.dropColumn('az_promotions',  'vehicle_group_id');
        }

        /* Add column vehicle groups*/
        await queryRunner.addColumn('az_vehicle_groups',
            new TableColumn({
                name: 'promotion_id',
                type: 'int',

            }),
        );
        await queryRunner.createForeignKey('az_vehicle_groups', new TableForeignKey({
            columnNames: ['promotion_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_promotions',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
