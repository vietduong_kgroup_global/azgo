import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColumnGGFBUser1575357055994 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_users', [
            new TableColumn({
                name: 'fb_id',
                type: 'varchar',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'gg_id',
                type: 'varchar',
                default: null,
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumns('az_users', [
            new TableColumn({
                name: 'fb_id',
                type: 'varchar',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'gg_id',
                type: 'varchar',
                default: null,
                isNullable: true,
            }),
        ]);
    }

}
