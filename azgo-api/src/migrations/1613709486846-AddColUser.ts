import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColUser1613709486846 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_users` ADD COLUMN `provider_deleted` timestamp NULL DEFAULT NULL AFTER `deleted_at`");
        await queryRunner.query("ALTER TABLE `az_users` ADD COLUMN `customer_deleted` timestamp NULL DEFAULT NULL AFTER `deleted_at`");
        await queryRunner.query("ALTER TABLE `az_users` ADD COLUMN `driver_deleted` timestamp NULL DEFAULT NULL AFTER `deleted_at`");
        await queryRunner.query("ALTER TABLE `az_users` ADD COLUMN `admin_deleted` timestamp NULL DEFAULT NULL AFTER `deleted_at`");
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
