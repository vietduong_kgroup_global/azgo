import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTableRoute1579072182916 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_routes', [
            new TableColumn({
                name: 'route_code',
                type: 'varchar',
                length: '30',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
