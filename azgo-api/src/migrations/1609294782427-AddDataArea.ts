import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddDataArea1609294782427 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("INSERT INTO az_area (uuid, name, type) VALUES ('5e2961f1-7bb9-4154-9701-b45660a18e28', 'Phú Quốc', 1)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
