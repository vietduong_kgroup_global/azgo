import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColOrderBikeCar1600076723761 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_bike_car_orders', [
            new TableColumn({
                name: 'rate_of_charge',
                type: 'float',
                default: 0,
                comment: 'Tỷ lệ phần % doanh thu của AZGO',
            }),

            new TableColumn({
                name: 'fee_tax',
                type: 'float',
                default: 0,
                comment: 'Chiết khấu % từ price order',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
