import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColVehicleTypeMigration1585121439577 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_vehicle_types', [
            new TableColumn({
                name: 'provider_id',
                type: 'int',
                isNullable: true,
            }),
        ]);
        await queryRunner.dropColumns('az_vehicle_types', [
            new TableColumn({
                name: 'seat_map',
                type: 'longtext',
                isNullable: true,
            }),
            new TableColumn({
                name: 'max_seat_of_vertical',
                type: 'int',
                isNullable: true,
            }),
            new TableColumn({
                name: 'max_seat_of_horizontal',
                type: 'int',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
