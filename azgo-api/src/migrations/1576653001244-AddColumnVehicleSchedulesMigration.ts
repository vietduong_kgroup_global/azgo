import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColumnVehicleSchedulesMigration1576653001244 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        /* Add column az_ticket */
        await queryRunner.addColumn('az_vehicle_schedules',
            new TableColumn({
                name: 'available_seat',
                type: 'int',
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
