import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColBookShippingMigration1584582325335 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_book_shipping', [
            new TableColumn({
                name: 'book_shipping_reference',
                type: 'varchar',
                comment: 'Generagte code',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
