import {MigrationInterface, QueryRunner, Table} from 'typeorm';

export class CreateTableHotlinePolicyMigration1584669855688 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_hotline_policy',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'hotline',
                    type: 'json',
                    isNullable: true,
                },
                {
                    name: 'policy',
                    type: 'longtext',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
