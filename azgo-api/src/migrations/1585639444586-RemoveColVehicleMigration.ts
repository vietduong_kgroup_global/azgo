import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class RemoveColVehicleMigration1585639444586 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        if (await queryRunner.hasColumn('az_vehicle', 'start_point')) {
            await queryRunner.dropColumn('az_vehicle',  'start_point');
        }
        if (await queryRunner.hasColumn('az_vehicle', 'end_point')) {
            await queryRunner.dropColumn('az_vehicle',  'end_point');
        }
        if (await queryRunner.hasColumn('az_vehicle', 'pick_up_point')) {
            await queryRunner.dropColumn('az_vehicle',  'pick_up_point');
        }
        if (await queryRunner.hasColumn('az_vehicle', 'status')) {
            await queryRunner.dropColumn('az_vehicle',  'status');
        }
        if (await queryRunner.hasColumn('az_vehicle', 'point')) {
            await queryRunner.dropColumn('az_vehicle',  'point');
        }
        await queryRunner.addColumns('az_vehicle', [
            new TableColumn({
                name: 'status',
                type: 'smallint',
                default: 1,
                comment: '1: Đang hoạt động, 2: Chưa hoạt động',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
