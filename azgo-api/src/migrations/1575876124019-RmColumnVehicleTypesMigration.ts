import {MigrationInterface, QueryRunner} from 'typeorm';

export class RmColumnVehicleTypesMigration1575876124019 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_vehicle_types');
        const roleForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('vehicle_id') !== -1);
        await queryRunner.dropForeignKey('az_vehicle_types', roleForeignKey);
        await queryRunner.dropColumn('az_vehicle_types',  'vehicle_id');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
