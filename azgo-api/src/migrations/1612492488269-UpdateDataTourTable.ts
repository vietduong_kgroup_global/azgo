import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class UpdateDataTourTable1612492488269 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('UPDATE `az_tour` SET type=\'distance_price\' WHERE distance_rate != 0 AND distance_rate IS NOT NULL'); 
        await queryRunner.query('UPDATE `az_tour` SET type=\'time_price\' WHERE time_rate != 0 AND time_rate IS NOT NULL'); 
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
