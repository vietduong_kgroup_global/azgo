import {MigrationInterface, QueryRunner, Table, TableColumn} from 'typeorm';

export class UpdateTypeColumnTableVanBagacOrder1583231500898 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('az_van_bagac_orders', 'service_other_id');
        await queryRunner.addColumns('az_van_bagac_orders', [
            new TableColumn({
                name: 'service_others',
                type: 'json',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
