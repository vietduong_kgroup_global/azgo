import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTableVanBagacOrders1582615685128 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_van_bagac_orders', [
            new TableColumn({
                name: 'payment_status',
                type: 'smallint',
                isNullable: true,
                default: 1,
            }),
            new TableColumn({
                name: 'payment_method',
                type: 'smallint',
                isNullable: true,
                default: 1,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
