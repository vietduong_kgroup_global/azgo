import {getRepository, MigrationInterface, QueryRunner} from 'typeorm';

export class UserRolesDefault1573182762037 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await getRepository('az_roles').save(
            [
                {
                    name: 'Master Admin',
                    permission: {functional: [
                                    {users: {read: 1, create: 1, delete: 1, update: 1}},
                                    {roles: {read: 1, create: 1, delete: 1, update: 1}},
                                    {user_roles: {read: 1, create: 1, delete: 1, update: 1}},
                                    {function_settings: {read: 1, create: 1, delete: 1, update: 1}},
                                ]},
                },
                {
                    name: 'Driver Bus',
                    permission: {functional: [
                            {users: {read: 1, create: 1, delete: 0, update: 1}},
                            {roles: {read: 1, create: 1, delete: 0, update: 1}},
                            {user_roles: {read: 1, create: 1, delete: 0, update: 1}},
                            {function_settings: {read: 1, create: 0, delete: 0, update: 0}},
                        ]},
                },
                {
                    name: 'Customer',
                    permission: {functional: [
                            {users: {read: 1, create: 1, delete: 0, update: 1}},
                            {roles: {read: 1, create: 1, delete: 0, update: 1}},
                            {user_roles: {read: 1, create: 1, delete: 0, update: 1}},
                            {function_settings: {read: 1, create: 0, delete: 0, update: 0}},
                        ]},
                },
                {
                    name: 'Provider',
                    permission: {functional: [
                            {users: {read: 1, create: 1, delete: 0, update: 1}},
                            {roles: {read: 1, create: 1, delete: 0, update: 1}},
                            {user_roles: {read: 1, create: 1, delete: 0, update: 1}},
                            {function_settings: {read: 1, create: 0, delete: 0, update: 0}},
                        ]},
                },
                {
                    name: 'Driver Bike Car',
                    permission: {functional: [
                            {users: {read: 1, create: 1, delete: 0, update: 1}},
                            {roles: {read: 1, create: 1, delete: 0, update: 1}},
                            {user_roles: {read: 1, create: 1, delete: 0, update: 1}},
                            {function_settings: {read: 1, create: 0, delete: 0, update: 0}},
                        ]},
                },
                {
                    name: 'Driver Van Bagac',
                    permission: {functional: [
                            {users: {read: 1, create: 1, delete: 0, update: 1}},
                            {roles: {read: 1, create: 1, delete: 0, update: 1}},
                            {user_roles: {read: 1, create: 1, delete: 0, update: 1}},
                            {function_settings: {read: 1, create: 0, delete: 0, update: 0}},
                        ]},
                },
            ],
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DELETE FROM `az_roles`');
    }

}
