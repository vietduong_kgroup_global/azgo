import {MigrationInterface, QueryRunner} from "typeorm";

export class ModifySystemAddCancelHours1612082426772 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_setting_system` ADD COLUMN `hours_allow_cancel` SMALLINT(6) DEFAULT 12");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
