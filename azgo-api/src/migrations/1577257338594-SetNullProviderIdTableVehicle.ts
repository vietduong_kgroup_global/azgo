import {MigrationInterface, QueryRunner} from 'typeorm';

export class SetNullProviderIdTableVehicle1577257338594 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `az_vehicle` MODIFY COLUMN `provider_id` int(11) NULL');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
