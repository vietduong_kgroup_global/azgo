import {MigrationInterface, QueryRunner} from 'typeorm';

export class UsersMigration1571986429582 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('CREATE TABLE `az_users` (`id` int NOT NULL AUTO_INCREMENT, `user_id` BINARY(36) DEFAULT NULL,' +
            '`phone` varchar(255) DEFAULT NULL, `country_code` int DEFAULT NULL,`email` varchar(255) NOT NULL,`birthdate` date NULL DEFAULT NULL,' +
            '`password` varchar(255) NOT NULL,`status` smallint NOT NULL DEFAULT 1,`first_name` varchar(50) NOT NULL,' +
            '`middle_name` varchar(50) DEFAULT NULL,`last_name` varchar(50) NOT NULL,`gender` smallint DEFAULT 1,' +
            '`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,' +
            '`deleted_at` timestamp NULL DEFAULT NULL, PRIMARY KEY (`id`), UNIQUE(`email`))');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DROP TABLE IF EXISTS `az_users`');
    }

}
