import {MigrationInterface, QueryRunner, Table, TableForeignKey} from 'typeorm';

export class CreateTableHistoryWallet1598856039746 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_wallet_histories',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'driver_id',
                    type: 'int',
                },
                {
                    name: 'amount',
                    type: 'double',
                },
                {
                    name: 'type',
                    type: 'smallint',
                    comment: '1: fee wallet, 2: cash wallet',
                },
                {
                    name: 'payment_method',
                    type: 'smallint',
                    default: 1,
                    comment: '1: Vn pay, 2: cash wallet',
                },
                {
                    name: 'payment_status',
                    type: 'smallint',
                    default: 1,
                    comment: '1: Paid, 2: Unpaid',
                },
                {
                    name: 'content',
                    type: 'varchar',
                    isNullable: true,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                },
            ],
        }), true);
        await queryRunner.createForeignKey('az_wallet_histories', new TableForeignKey({
            columnNames: ['driver_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_users',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
