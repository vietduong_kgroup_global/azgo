import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class RmColumnUser1575873204395 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('az_users', 'birthdate');
        await queryRunner.dropColumn('az_users', 'first_name');
        await queryRunner.dropColumn('az_users', 'last_name');
        await queryRunner.dropColumn('az_users', 'gender');
        await queryRunner.dropColumn('az_users', 'parent_id');
        await queryRunner.dropColumn('az_users', 'company');
        await queryRunner.dropColumn('az_users', 'full_name');
        await queryRunner.dropColumn('az_users', 'cmnd');
        await queryRunner.dropColumn('az_users', 'address');
        await queryRunner.dropColumn('az_users', 'image_profile');
        await queryRunner.dropColumn('az_users', 'front_of_cmnd');
        await queryRunner.dropColumn('az_users', 'back_of_cmnd');
        await queryRunner.addColumns('az_users', [
            new TableColumn({
                name: 'type',
                type: 'smallint',
                default: null,
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_users', [
            new TableColumn({
                name: 'birthdate',
                type: 'date',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'first_name',
                type: 'varchar',
            }),
            new TableColumn({
                name: 'last_name',
                type: 'varchar',
            }),
            new TableColumn({
                name: 'gender',
                type: 'smallint',
                default: 1,
            }),
            new TableColumn({
                name: 'parent_id',
                type: 'int',
                isNullable: true,
            }),
            new TableColumn({
                name: 'company',
                type: 'varchar',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'full_name',
                type: 'varchar',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'cmnd',
                type: 'varchar',
                length: '50',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'address',
                type: 'text',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'image_profile',
                type: 'json',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'front_of_cmnd',
                type: 'json',
                default: null,
                isNullable: true,
            }),
            new TableColumn({
                name: 'back_of_cmnd',
                type: 'json',
                default: null,
                isNullable: true,
            }),
        ]);
    }

}
