import { MigrationInterface, QueryRunner } from 'typeorm';
export class ModifyPromotionTourOrder1611722309587 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query('ALTER TABLE `az_promotions` ADD COLUMN `uuid` VARCHAR(255) NOT NULL AFTER `id`');
        queryRunner.query('ALTER TABLE `az_promotions` ADD COLUMN `company` VARCHAR(255) NULL COMMENT "ten cong ty" AFTER `price_discount`');
        queryRunner.query('ALTER TABLE `az_tour_orders` ADD COLUMN `promotion_code` VARCHAR(255) NULL COMMENT "ma khuyen mai" AFTER `external_code`');
        queryRunner.query("ALTER TABLE `az_promotions` MODIFY COLUMN `percent_discount` FLOAT NULL");
        queryRunner.query("ALTER TABLE `az_promotions` MODIFY COLUMN `price_discount` FLOAT NULL");

    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
