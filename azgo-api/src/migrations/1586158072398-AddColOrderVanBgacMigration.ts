import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColOrderVanBgacMigration1586158072398 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_van_bagac_orders', [
            new TableColumn({
                name: 'receive_and_delivery_distance',
                type: 'float',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
