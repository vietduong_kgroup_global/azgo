import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';
export class ModifyTableTour1615263774294 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_tour');
        const feeForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('additional_fee_uuid') !== -1);
        await queryRunner.dropForeignKey('az_tour', feeForeignKey);
        await queryRunner.query("ALTER TABLE `az_tour` DROP COLUMN `additional_fee_end`");
        await queryRunner.query("ALTER TABLE `az_tour` DROP COLUMN `additional_fee_start`");
        await queryRunner.query("ALTER TABLE `az_tour` DROP COLUMN `additional_fee_uuid`");
        await queryRunner.query("ALTER TABLE `az_tour` ADD COLUMN `additional_fee` json NULL DEFAULT NULL AFTER `extra_time_price`");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
