import {MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey} from 'typeorm';

export class ModifyRouteMigration1581308038117 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_area');
        await queryRunner.createTable(new Table({
            name: 'az_area',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'name',
                    type: 'varchar',
                },
                {
                    name: 'type',
                    type: 'smallint',
                    comment: '1: Tỉnh, 2: Quận/Huyện',
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);
        await queryRunner.query("INSERT INTO az_area (name, type) VALUES ('Cần Thơ', 1)");
        await queryRunner.query("INSERT INTO az_area (name, type) VALUES ('Hồ Chí Minh', 1)");
        if (await queryRunner.hasColumn('az_routes', 'start_point')) {
            await queryRunner.dropColumn('az_routes',  'start_point');
        }
        if (await queryRunner.hasColumn('az_routes', 'end_point')) {
            await queryRunner.dropColumn('az_routes',  'end_point');
        }
        await queryRunner.addColumns('az_routes', [
            new TableColumn({
                name: 'start_point',
                type: 'int',
            }),
            new TableColumn({
                name: 'end_point',
                type: 'int',
            }),
        ]);
        await queryRunner.query('UPDATE  az_routes SET start_point=1, end_point=2 WHERE id > 1 ');

        await queryRunner.createForeignKey('az_routes', new TableForeignKey({
            columnNames: ['start_point'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_area',
            onDelete: 'CASCADE',
        }));
        await queryRunner.createForeignKey('az_routes', new TableForeignKey({
            columnNames: ['end_point'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_area',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
