import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class UpdateUniQueWallet1617960612776 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE az_wallets ADD UNIQUE (`driver_id`)");
    } 
    public async down(queryRunner: QueryRunner): Promise<any> {
        
    }

}