import {MigrationInterface, QueryRunner} from 'typeorm';

export class RmColProviderInfoMigration1586748278760 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('az_providers_info', 'first_name');
        await queryRunner.dropColumn('az_providers_info', 'last_name');
        await queryRunner.dropColumn('az_providers_info', 'full_name');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
