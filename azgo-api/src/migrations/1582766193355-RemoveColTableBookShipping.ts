import {MigrationInterface, QueryRunner} from 'typeorm';

export class RemoveColTableBookShipping1582766193355 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('az_book_shipping', 'qr_code');
        await queryRunner.dropColumn('az_book_shipping', 'qr_code_status');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
