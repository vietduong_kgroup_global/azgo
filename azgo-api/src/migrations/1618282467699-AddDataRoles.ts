import { getRepository, MigrationInterface, QueryRunner } from 'typeorm';
export class AddDataRoles1618282467699 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await getRepository('az_roles').save(
            [
                {
                    id: 8,
                    name: 'Region Manager',
                    permission: {
                        functional: [
                            { users: { read: 1, create: 1, delete: 0, update: 1 } },
                            { roles: { read: 1, create: 1, delete: 0, update: 1 } },
                            { user_roles: { read: 1, create: 1, delete: 0, update: 1 } },
                            { function_settings: { read: 1, create: 0, delete: 0, update: 0 } },
                        ],
                        
                    },
                },
            ],
        );
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
