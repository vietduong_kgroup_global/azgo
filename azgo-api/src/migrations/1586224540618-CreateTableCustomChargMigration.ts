import { getConnection, getRepository, MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
import { VehicleGroupsEntity } from '../module/vehicle-groups/vehicle-groups.entity';
import { CustomChargeEntity } from '../module/custom-charge/custom-charge.entity';

export class CreateTableCustomChargMigration1586224540618 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_custom_charges',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'vehicle_group_id',
                    type: 'int',
                },
                {
                    name: 'rate_of_charge',
                    type: 'float',
                    comment: 'Tỷ lệ phần % doanh thu',
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);
        await queryRunner.createForeignKey('az_custom_charges', new TableForeignKey({
            columnNames: ['vehicle_group_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_vehicle_groups',
            onDelete: 'CASCADE',
        }));

        const vehicle_groups = await queryRunner.manager.getRepository(VehicleGroupsEntity)
            .createQueryBuilder('vehicle_group')
            .getMany();

        Promise.all(vehicle_groups.map(async (v, i) => {
            await queryRunner.query('INSERT INTO `az_custom_charges` (`vehicle_group_id`, `rate_of_charge`) VALUES (' + v.id + ', 1)');
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
