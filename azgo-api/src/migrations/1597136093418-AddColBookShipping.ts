import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColBookShipping1597136093418 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_book_shipping', [
            new TableColumn({
                name: 'ticket_code',
                type: 'varchar',
                isNullable: true,
            }),
            new TableColumn({
                name: 'pickup_point',
                type: 'json',
                isNullable: true,
                comment: '{ lat: 10.814243, long: 106.6693821, address: Can Tho, short_address: Toa nha vincom }',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
