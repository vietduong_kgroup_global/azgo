import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColOrderVanBaGacMigration1583651506292 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_van_bagac_orders', [
            new TableColumn({
                name: 'book_van_bagac_reference',
                type: 'varchar',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
