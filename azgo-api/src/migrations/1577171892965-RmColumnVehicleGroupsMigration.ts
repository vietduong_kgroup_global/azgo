import {MigrationInterface, QueryRunner} from 'typeorm';

export class RmColumnVehicleGroupsMigration1577171892965 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        /* Remove FK and column promotions*/
        const table = await queryRunner.getTable('az_vehicle_groups');
        const roleForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('vehicle_id') !== -1);
        if (typeof roleForeignKey !== 'undefined') {
            await queryRunner.dropForeignKey('az_vehicle_groups', roleForeignKey);
        }

        if (await queryRunner.hasColumn('az_vehicle_groups', 'vehicle_id')) {
            await queryRunner.dropColumn('az_vehicle_groups',  'vehicle_id');
        }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
