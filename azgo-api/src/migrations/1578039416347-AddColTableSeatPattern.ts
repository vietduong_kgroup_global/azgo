import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTableSeatPattern1578039416347 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_seat_pattern', [
            new TableColumn({
                name: 'total_seat',
                type: 'int',
                isNullable: true,
            }),
            new TableColumn({
                name: 'max_seat_of_vertical',
                type: 'int',
                isNullable: true,
            }),
            new TableColumn({
                name: 'max_seat_of_horizontal',
                type: 'int',
                isNullable: true,
            }),
            new TableColumn({
                name: 'updated_at',
                type: 'timestamp',
                isNullable: true,
                default: 'CURRENT_TIMESTAMP',
                onUpdate: 'CURRENT_TIMESTAMP',
            }),
            new TableColumn({
                name: 'deleted_at',
                type: 'timestamp',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
