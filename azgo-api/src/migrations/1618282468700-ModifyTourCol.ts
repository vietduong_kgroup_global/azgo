import {MigrationInterface, QueryRunner} from "typeorm";
export class ModifyTourCol1618282468700 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_tour` MODIFY COLUMN `description` TEXT");
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
