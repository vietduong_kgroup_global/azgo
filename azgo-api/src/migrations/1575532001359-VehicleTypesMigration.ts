import {MigrationInterface, QueryRunner, Table, TableForeignKey} from 'typeorm';

export class VehicleTypesMigration1575532001359 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_vehicle_types',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'vehicle_id',
                    type: 'int',
                },
                {
                    name: 'name',
                    type: 'varchar',
                },
                {
                    name: 'seat_map',
                    type: 'json',
                },
                {
                    name: 'total_seat',
                    type: 'int',
                },
                {
                    name: 'max_seat_of_vertical',
                    type: 'int',
                },
                {
                    name: 'max_seat_of_horizontal',
                    type: 'int',
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                },
            ],
        }), true);

        await queryRunner.createForeignKey('az_vehicle_types', new TableForeignKey({
            columnNames: ['vehicle_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_vehicle',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_vehicle_types');
        const vehicleForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('vehicle_id') !== -1);
        await queryRunner.dropForeignKey('az_vehicle', vehicleForeignKey);
        await queryRunner.dropTable('az_vehicle_types');
    }

}
