import {MigrationInterface, QueryRunner, Table, TableForeignKey} from 'typeorm';

export class RatingMigration1576483597409 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_rating',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'driver_id',
                    type: 'int',
                },
                {
                    name: 'customer_id',
                    type: 'int',
                },
                {
                    name: 'content',
                    type: 'text',
                },
                {
                    name: 'point',
                    type: 'decimal',
                    isNullable: false,
                },
                {
                    name: 'type',
                    type: 'smallint',
                    comment: '1: driver, 2: provider',
                    default: 1,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);

        await queryRunner.createForeignKey('az_rating', new TableForeignKey({
            columnNames: ['driver_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_driver_profile',
            onDelete: 'CASCADE',
        }));
        await queryRunner.createForeignKey('az_rating', new TableForeignKey({
            columnNames: ['customer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_customer_profile',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_rating');
        const driverForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('driver_id') !== -1);
        await queryRunner.dropForeignKey('az_rating', driverForeignKey);
        const customerForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('customer_id') !== -1);
        await queryRunner.dropForeignKey('az_rating', customerForeignKey);
        await queryRunner.dropTable('az_rating');

    }

}
