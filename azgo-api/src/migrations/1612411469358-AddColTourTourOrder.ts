import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';
export class AddColTourTourOrder1612411469358 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_tour` ADD COLUMN `hide_mobile` SMALLINT(6) DEFAULT 0 COMMENT 'An tren dien thoai' AFTER `is_feature`");
        await queryRunner.query("ALTER TABLE `az_tour` ADD COLUMN `type` VARCHAR(255) DEFAULT 'fix_price' COMMENT 'Kieu tour' AFTER `time`");
        await queryRunner.query("ALTER TABLE `az_tour` ADD COLUMN `time_rate` INT(11) DEFAULT 0 COMMENT 'Gia theo ngay' AFTER `distance_rate`");
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
