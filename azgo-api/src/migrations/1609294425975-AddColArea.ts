import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColArea1609294425975 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DELETE FROM az_area WHERE id=1");
        await queryRunner.query("DELETE FROM az_area WHERE id=2");
        await queryRunner.addColumns('az_area', [
            new TableColumn({
                name: 'uuid',
                type: 'varchar',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
