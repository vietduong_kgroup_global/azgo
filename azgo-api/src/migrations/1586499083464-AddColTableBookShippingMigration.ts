import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTableBookShippingMigration1586499083464 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_book_shipping', [
            new TableColumn({
                name: 'commodity_images',
                type: 'json',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
