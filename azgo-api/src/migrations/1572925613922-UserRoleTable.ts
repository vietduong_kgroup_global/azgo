import {MigrationInterface, QueryRunner, Table, TableIndex, TableColumn, TableForeignKey } from 'typeorm';

export class UserRoleTable1572925613922 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_user_roles',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'permission',
                    type: 'json',
                },
                {
                    name: 'user_id',
                    type: 'int',
                },
                {
                    name: 'role_id',
                    type: 'int',
                },
            ],
        }), true);

        await queryRunner.createForeignKey('az_user_roles', new TableForeignKey({
            columnNames: ['role_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_roles',
            onDelete: 'CASCADE',
        }));

        await queryRunner.createForeignKey('az_user_roles', new TableForeignKey({
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_users',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_user_roles');
        const roleForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf('role_id') !== -1);
        const userForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf('user_id') !== -1);
        await queryRunner.dropForeignKey('az_roles', roleForeignKey);
        await queryRunner.dropForeignKey('az_users', userForeignKey);
        await queryRunner.dropTable('az_user_roles');
    }

}
