import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTicketMigration1582257995572 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        new TableColumn({
            name: 'type',
            type: 'smallint',
            default: 1,
            comment: '1: Book ticket, 2: Book vehicle',
        });
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
