import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColProviderIdTableRatingDriver1581588909312 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_rating', [
            new TableColumn({
                name: 'provider_id',
                type: 'int',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
