import {MigrationInterface, QueryRunner, Table, TableForeignKey} from 'typeorm';

export class CreateRatingProviders1581649630187 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_rating_providers',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'provider_id',
                    type: 'int',
                },
                {
                    name: 'customer_id',
                    type: 'int',
                },
                {
                    name: 'content',
                    type: 'text',
                },
                {
                    name: 'point',
                    type: 'decimal',
                    isNullable: false,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true);

        await queryRunner.createForeignKey('az_rating_providers', new TableForeignKey({
            columnNames: ['provider_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_providers_info',
            onDelete: 'CASCADE',
        }));

        await queryRunner.createForeignKey('az_rating_providers', new TableForeignKey({
            columnNames: ['customer_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_customer_profile',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
