import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class RmAndAddColumnAreaMigration1576484495677 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        if (await queryRunner.hasColumn('az_area', 'type')) {
            await queryRunner.dropColumn('az_area',  'type');
        }

        /* Add column az_area */
        await queryRunner.addColumn('az_area',
            new TableColumn({
                name: 'type',
                type: 'smallint',
                default: 1,
                comment: '1: Tỉnh - Thành phố, 2: Quận -Huyện',
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
