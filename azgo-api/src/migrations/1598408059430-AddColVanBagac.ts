import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColVanBagac1598408059430 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_van_bagac_orders', [
            new TableColumn({
                name: 'disable_btn_cancel_approve',
                type: 'smallint',
                default: 0,
                comment: 'Dùng để disable btn approve at app customer',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
