import {MigrationInterface, QueryRunner} from 'typeorm';

export class DropRatingProviderMiration1588309170969 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_rating_providers');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

