import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class fixTableVehicleSchedule1575952617271 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const check = await queryRunner.hasColumn('az_vehicle_schedules', 'end_date');
        if (check){
            await queryRunner.dropColumn('az_vehicle_schedules',  'end_date');
            await queryRunner.dropColumn('az_vehicle_schedules',  'end_hour');
        }

        await queryRunner.addColumn('az_vehicle_schedules',
            new TableColumn({
                name: 'route_id',
                type: 'int',
                default: null,
                isNullable: true,
            }),
        );
        await queryRunner.addColumn('az_vehicle_schedules',
            new TableColumn({
                name: 'provider_id',
                type: 'int',
                default: null,
                isNullable: true,
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
