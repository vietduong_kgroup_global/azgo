import { MigrationInterface, QueryRunner, getRepository } from 'typeorm';
import { SocialType, UserStatus, UserType } from '@azgo-module/core/dist/utils/enum.ultis';
import * as moment from 'moment';
import { UsersEntity } from '../module/users/users.entity';

export class UpdateRolesUser1614139501799 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        const list_users = await queryRunner.manager.getRepository(UsersEntity)
            .createQueryBuilder('az_users')
            .andWhere('JSON_CONTAINS(roles, \':role\') = 0', { role: UserType.CUSTOMER })
            .getMany();
        const promises =  list_users.map(async (user, index) => {
            let data_profile, array_promise = [];
            let customer_profile = await queryRunner.query('SELECT * FROM `az_customer_profile` WHERE `user_id` = ' + user.id);
            if(!customer_profile.length && user.type === UserType.ADMIN){
                let birthday_fortmat;
                let admin_profile = await queryRunner.query('SELECT * FROM `az_admin_profile` WHERE `user_id` = ' + user.id);
                admin_profile = admin_profile[0]
                if (admin_profile.birthday) {
                    birthday_fortmat = moment(admin_profile.birthday).format('YYYY-MM-DD').toString();
                }
                data_profile = {
                    user_id: user.id,
                    first_name: admin_profile.first_name,
                    last_name: admin_profile.last_name,
                    full_name: admin_profile.last_name + ' ' + admin_profile.first_name,
                    gender: admin_profile.gender,
                    birthday: birthday_fortmat,
                    cmnd: admin_profile.cmnd,
                    address: admin_profile.address,
                    image_profile: admin_profile.image_profile,
                    front_of_cmnd: admin_profile.front_of_cmnd,
                    back_of_cmnd: admin_profile.back_of_cmnd,
                };
            }
            else if(!customer_profile.length && (user.type === UserType.DRIVER_BUS || user.type === UserType.DRIVER_BIKE_CAR || user.type === UserType.DRIVER_VAN_BAGAC || user.type === UserType.DRIVER_TOUR)){
                let birthday_fortmat;
                let driver_profile = await queryRunner.query('SELECT * FROM `az_driver_profile` WHERE `user_id` = ' + user.id);
                driver_profile = driver_profile[0]
                if (driver_profile.birthday) {
                    birthday_fortmat = moment(driver_profile.birthday).format('YYYY-MM-DD').toString();
                }
                data_profile = {
                    user_id: user.id,
                    first_name: driver_profile.first_name,
                    last_name: driver_profile.last_name,
                    full_name: driver_profile.last_name + ' ' + driver_profile.first_name,
                    gender: driver_profile.gender,
                    birthday: birthday_fortmat,
                    cmnd: driver_profile.cmnd,
                    address: driver_profile.address,
                    image_profile: driver_profile.image_profile,
                    front_of_cmnd: driver_profile.front_of_cmnd,
                    back_of_cmnd: driver_profile.back_of_cmnd,
                };
            }
            let user_roles = String(user.roles);
            user_roles.substring(1, user_roles.length -1)
            let roles = user_roles.split(',')
            roles = roles.concat([String(UserType.CUSTOMER)])
            roles = roles.filter(item => !!item)
            // if(data_profile) array_promise.push(getRepository('az_customer_profile').save([data_profile]));
            if(data_profile) {
                array_promise.push(
                    queryRunner.query(
                        "INSERT INTO `az_customer_profile` (`user_id`, `first_name`, `last_name`, `full_name`, `gender`, `birthday`, `cmnd`, `address`, `image_profile`, `front_of_cmnd`, `back_of_cmnd`) VALUES (" 
                        + data_profile.user_id + ","
                        + (data_profile.first_name? "'" + data_profile.first_name + "'" : null) + ","
                        + (data_profile.last_name? "'" + data_profile.last_name + "'" : null) + ","
                        + (data_profile.full_name? "'" + data_profile.full_name + "'" : null) + ","
                        + data_profile.gender + ","
                        + (data_profile.birthday? "'" + data_profile.birthday + "'" : null) + ","
                        + (data_profile.cmnd? "'" + data_profile.cmnd + "'" : null) + ","
                        + (data_profile.address? "'" + data_profile.address + "'" : null) + ","
                        + (data_profile.image_profile? "'" + data_profile.image_profile + "'" : null) + ","
                        + (data_profile.front_of_cmnd? "'" + data_profile.front_of_cmnd + "'" : null) + ","
                        + (data_profile.back_of_cmnd? "'" + data_profile.back_of_cmnd + "'" : null) + ");"
                    )
                );
            }
            array_promise.push(queryRunner.query('UPDATE `az_users` SET `roles` = \'[' + roles + ']\' WHERE `az_users`.`id` = ' + user.id));
            return array_promise
        })
        Promise.all(promises);
    }
    public async down(queryRunner: QueryRunner): Promise<any> {

    }
}
