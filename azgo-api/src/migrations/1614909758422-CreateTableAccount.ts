import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
export class CreateTableAccount1614909758422 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        // await queryRunner.query("ALTER TABLE `az_area` MODIFY COLUMN `uuid` VARCHAR(255) AFTER `id`");
        // await queryRunner.query("ALTER TABLE `az_area` ADD INDEX( `uuid`)");

        await queryRunner.query("DROP TABLE IF EXISTS `az_accounts`");
        await queryRunner.createTable(new Table({
            name: 'az_accounts',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                }, 
                {
                    name: 'account_number',
                    type: 'varchar',
                    length: '50',
                },
                {
                    name: 'account_user_name',
                    type: 'varchar',
                    length: '50',
                },
                {
                    name: 'account_branch',
                    type: 'varchar',
                    length: '50',
                },
                {
                    name: 'account_name',
                    type: 'varchar',
                    length: '50',
                },
                {
                    name: 'is_active',
                    type: 'smallint',
                    default: 0,
                    comment: '1: Active, 0: Inactive',
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
            ],
        }), true); 
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE IF EXISTS `az_accounts`");
    }
}
