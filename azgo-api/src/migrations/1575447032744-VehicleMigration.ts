import {MigrationInterface, QueryRunner, Table, TableForeignKey} from 'typeorm';

export class VehicleMigration1575447032744 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_vehicle',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'user_id',
                    type: 'int',
                },
                {
                    name: 'vehicle_owner',
                    type: 'int',
                },
                {
                    name: 'license_plates',
                    type: 'varchar',
                },
                {
                    name: 'start_point',
                    type: 'varchar',
                },
                {
                    name: 'end_point',
                    type: 'varchar',
                },
                {
                    name: 'pick_up_point',
                    type: 'json',
                },
                {
                    name: 'status',
                    type: 'smallint',
                },
                {
                    name: 'point',
                    type: 'json',
                },
                {
                    name: 'price',
                    type: 'float',
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                },
            ],
        }), true);

        await queryRunner.createForeignKey('az_vehicle', new TableForeignKey({
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_users',
            onDelete: 'CASCADE',
        }));
        await queryRunner.createForeignKey('az_vehicle', new TableForeignKey({
            columnNames: ['vehicle_owner'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_users',
            onDelete: 'CASCADE',
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_vehicle');
        const roleForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('user_id') !== -1);
        const userForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('vehicle_owner') !== -1);
        await queryRunner.dropForeignKey('az_users', roleForeignKey);
        await queryRunner.dropForeignKey('az_users', userForeignKey);
        await queryRunner.dropTable('az_vehicle');
    }

}
