import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColTourOrder1614856067689 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_additional_fee` ADD INDEX( `uuid`)");
        await queryRunner.query("ALTER TABLE `az_tour` ADD COLUMN `additional_fee_end` timestamp NULL DEFAULT NULL AFTER `extra_time_price`");
        await queryRunner.query("ALTER TABLE `az_tour` ADD COLUMN `additional_fee_start` timestamp NULL DEFAULT NULL AFTER `extra_time_price`");
        await queryRunner.query("ALTER TABLE `az_tour` ADD COLUMN `additional_fee_uuid` VARCHAR(255) NULL DEFAULT NULL AFTER `extra_time_price`");
        await queryRunner.createForeignKey('az_tour', new TableForeignKey({
            columnNames: ['additional_fee_uuid'],
            referencedColumnNames: ['uuid'],
            referencedTableName: 'az_additional_fee',
            onDelete: 'CASCADE',
        }));

        await queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `is_paid` smallint NULL DEFAULT 0 AFTER `auto_send_sms`");
        await queryRunner.query("ALTER TABLE `az_tour_orders` ADD COLUMN `additional_fee` json NULL DEFAULT NULL AFTER `order_price`");
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('az_tour');
        const feeForeignKey = await table.foreignKeys.find(fk => fk.columnNames.indexOf('additional_fee_uuid') !== -1);
        await queryRunner.dropForeignKey('az_additional_fee', feeForeignKey);
        
        await queryRunner.query("ALTER TABLE `az_tour` DROP COLUMN `additional_fee_end`");
        await queryRunner.query("ALTER TABLE `az_tour` DROP COLUMN `additional_fee_start`");
        await queryRunner.query("ALTER TABLE `az_tour` DROP COLUMN `additional_fee_uuid`");
        await queryRunner.query("ALTER TABLE `az_tour_orders` DROP COLUMN `is_paid`");
        await queryRunner.query("ALTER TABLE `az_tour_orders` DROP COLUMN `additional_fee`");
    }
}
