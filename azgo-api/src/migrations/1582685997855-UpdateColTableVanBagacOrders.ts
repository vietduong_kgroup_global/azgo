import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class UpdateColTableVanBagacOrders1582685997855 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('az_van_bagac_orders', 'driver_id');
        await queryRunner.addColumns('az_van_bagac_orders', [
            new TableColumn({
                name: 'driver_id',
                type: 'int',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {

    }

}
