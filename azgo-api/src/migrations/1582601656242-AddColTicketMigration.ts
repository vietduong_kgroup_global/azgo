import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTicketMigration1582601656242 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_ticket', [
            new TableColumn({
                name: 'type',
                type: 'smallint',
                default: 1,
                comment: '1: Book ticket, 2: Book vehicle',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
