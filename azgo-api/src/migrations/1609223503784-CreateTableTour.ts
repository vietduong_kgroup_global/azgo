import {MigrationInterface, QueryRunner, Table, TableForeignKey} from 'typeorm';

export class CreateTableTour1609223503784 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_tour',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'uuid',
                    type: 'varchar',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: 'name',
                    type: 'varchar',
                    isNullable: false,
                },
                {
                    name: 'area_uuid',
                    type: 'varchar',
                    isNullable: false,
                },
                {
                    name: 'time',
                    type: 'int',
                    default: 0,
                    isNullable: false,
                },
                {
                    name: 'number_of_seat',
                    type: 'int',
                    default: 4,
                    isNullable: false,
                    comment: 'So ghe',
                },
                {
                    name: 'tour_price',
                    type: 'int',
                    default: 4,
                    isNullable: false,
                    comment: 'Gia',
                },
                {
                    name: 'stations',
                    type: 'json',
                    isNullable: false,
                    comment: 'Cac tram se ghe',
                },
                {
                    name: 'extra_time_price',
                    type: 'int',
                    default: 0,
                    isNullable: true,
                },
                {
                    name: 'description',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                },
                {
                    name: 'is_round_trip',
                    type: 'smallint',
                    default: 1,
                    isNullable: true,
                    comment: '1: Khu hoi, 0: mot chieu',
                },
                {
                    name: 'is_deleted',
                    type: 'smallint',
                    default: 0,
                    isNullable: true,
                },
                {
                    name: 'image',
                    type: 'json',
                    isNullable: true,
                    comment: 'Hinh dai dien',
                },
                {
                    name: 'gallery',
                    type: 'json',
                    isNullable: true,
                    comment: 'Thu vien anh',
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'CURRENT_TIMESTAMP',
                    isNullable: true,
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                },
            ],
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('az_tour');
    }

}
