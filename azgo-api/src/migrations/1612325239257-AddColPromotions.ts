import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';
export class AddColPromotions1612325239257 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_promotions` ADD COLUMN `count` INT(11) DEFAULT 1 AFTER `price_discount`");
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
