import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColVanBagacOrderMigration1586744629375 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_van_bagac_orders', [
            new TableColumn({
                name: 'vehicle_id',
                type: 'int',
                isNullable: true,
            }),
            new TableColumn({
                name: 'vehicle_group_id',
                type: 'int',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
