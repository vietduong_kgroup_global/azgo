import {MigrationInterface, QueryRunner} from 'typeorm';

export class AddColVehicle1577265889525 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `az_vehicle` ADD COLUMN `front_registration_certificates` json NULL');
        await queryRunner.query('ALTER TABLE `az_vehicle` ADD COLUMN `back_registration_certificates` json NULL');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
