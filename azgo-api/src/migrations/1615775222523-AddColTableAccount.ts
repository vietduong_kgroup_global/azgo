import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColTableAccount1615775222523 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_accounts` ADD COLUMN `account_bank_code` VARCHAR(50) NULL AFTER `account_branch`"); 
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
