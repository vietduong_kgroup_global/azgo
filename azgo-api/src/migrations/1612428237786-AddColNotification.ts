import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColNotification1612428237786 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_notifications` ADD COLUMN `user_action_id` INT(11) NULL AFTER `user_id`");
        await queryRunner.createForeignKey('az_notifications', new TableForeignKey({
            columnNames: ['user_action_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_users',
            onDelete: 'CASCADE',
        }));
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
