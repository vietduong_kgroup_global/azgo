import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';
export class CreateTableRequestWithdraw1619078676180 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'az_request_withdraw_history',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'uuid',
                    type: 'varchar',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'uuid',
                },
                {
                    name: 'request_id',
                    type: 'int',
                    default: null,
                    isNullable: false,
                },
                {
                    name: 'user_update_id',
                    type: 'int',
                    default: null,
                    isNullable: false,
                },
                {
                    name: 'type',
                    type: 'varchar',
                    length: '255',
                    isNullable: true,
                    comment: "[CREATE, UPDATE, UPDATE_STATUS]",
                },
                {
                    name: 'data',
                    type: 'json',
                    isNullable: false,
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    isNullable: true,
                    default: 'CURRENT_TIMESTAMP',
                    onUpdate: 'CURRENT_TIMESTAMP',
                },
                {
                    name: 'deleted_at',
                    type: 'timestamp',
                    isNullable: true,
                },
            ],
        }), true);
        await queryRunner.createForeignKey('az_request_withdraw_history', new TableForeignKey({
            columnNames: ['request_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_request_withdraw',
            onDelete: 'CASCADE',
        }));
        await queryRunner.createForeignKey('az_request_withdraw_history', new TableForeignKey({
            columnNames: ['user_update_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'az_users',
            onDelete: 'CASCADE',
        }));
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE IF EXISTS `az_request_withdraw_history`");
    }
}
