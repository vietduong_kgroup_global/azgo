import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';
export class AddColSettingSystem1617678122046 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `az_setting_system` ADD COLUMN `apple_driver_version` VARCHAR(255) AFTER `remind_time`");  
        await queryRunner.query("ALTER TABLE `az_setting_system` ADD COLUMN `google_driver_version` VARCHAR(255) AFTER `remind_time`");  
        await queryRunner.query("ALTER TABLE `az_setting_system` ADD COLUMN `apple_driver_force_update` BOOLEAN DEFAULT FALSE AFTER `remind_time`");  
        await queryRunner.query("ALTER TABLE `az_setting_system` ADD COLUMN `google_driver_force_update` BOOLEAN DEFAULT FALSE AFTER `remind_time`");  

        await queryRunner.query("ALTER TABLE `az_setting_system` ADD COLUMN `apple_customer_version` VARCHAR(255) AFTER `remind_time`");  
        await queryRunner.query("ALTER TABLE `az_setting_system` ADD COLUMN `google_customer_version` VARCHAR(255) AFTER `remind_time`");  
        await queryRunner.query("ALTER TABLE `az_setting_system` ADD COLUMN `apple_customer_force_update` BOOLEAN DEFAULT FALSE AFTER `remind_time`");  
        await queryRunner.query("ALTER TABLE `az_setting_system` ADD COLUMN `google_customer_force_update` BOOLEAN DEFAULT FALSE AFTER `remind_time`");  
    }
    public async down(queryRunner: QueryRunner): Promise<any> {
    }
}
