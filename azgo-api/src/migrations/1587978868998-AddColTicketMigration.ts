import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddColTicketMigration1587978868998 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumns('az_ticket', [
            new TableColumn({
                name: 'time_pick_up',
                type: 'varchar',
                isNullable: true,
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
