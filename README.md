# README FIRST #

Put azgo.sql, install_mysql.sh, install_docker.sh start_azgo_all.sh to same root your azgo project folder

## GETTING START ##

### FILES ###

File ```before_start.sh```: run it after machine restarted

File ```docker-compose.yml```: all service of azgo in here

File ```clear_project.sh```: run it when build project failed. maybe it is failed because of cache

File ```import mysql.sh```: run it if this is the first time you run this on your machine. These script will install mysql8 and export current db in mysql of azgo, then import the default db (when we had it)

File ```install_docker.sh```: run it if this is the first time you run this project. This file will install docker for you.

File ```azgo.sql```: default azgo db

### READY YOUR DOCKER ###

If you have'nt install docker

```bash
sh install_docker.sh
```

### Start the project ###

```bash
sh before_start.sh
```

```bash
docker-compose up -d --build
```

## GIT SUBTREE ##

### Add subtree (DONT DO THIS UNLESS YOU WANT TO ADD NEW PROJECT FOR AZGO) ###

This is the script add added to this repository

```bash
git subtree add --prefix azgo-admin https://bitbucket.org/vietduong_kgroup_global/azgo-admin.git master --squash
```

```bash
git subtree add --prefix azgo-api https://bitbucket.org/vietduong_kgroup_global/azgo-api.git master --squash
```

```bash
git subtree add --prefix azgo-socketio https://bitbucket.org/vietduong_kgroup_global/azgo-socketio.git master --squash
```


### Pull new commit ###

You're doing this when want to get newer version of code from this repo.
Default in this api will automatically pull from master branch
In the first clone, you have already pull the subtree.
If something went wrong, check your git status:

Pull commit from admin (k-wiki-admin)

```bash
git status
```

Pull commit from backend (k-wiki-backend)

```bash
git subtree pull --prefix azgo-admin https://bitbucket.org/vietduong_kgroup_global/azgo-admin.git master --squash
```

```bash
git subtree pull --prefix azgo-api https://bitbucket.org/vietduong_kgroup_global/azgo-api.git master --squash
```

```bash
git subtree pull --prefix azgo-socketio https://bitbucket.org/vietduong_kgroup_global/azgo-socketio.git master --squash
```